﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Lean.Gui.LeanButton::set_MultiDown(System.Boolean)
extern void LeanButton_set_MultiDown_m88FBEE0CD166F2C801B580281761B9E446D926CD (void);
// 0x00000002 System.Boolean Lean.Gui.LeanButton::get_MultiDown()
extern void LeanButton_get_MultiDown_m47BCBC6F29CC4D8B2F73E08588225AB10AC54167 (void);
// 0x00000003 System.Void Lean.Gui.LeanButton::set_DragThreshold(System.Single)
extern void LeanButton_set_DragThreshold_m4272966CB8E5497F927D799F2B1611D5598CA53E (void);
// 0x00000004 System.Single Lean.Gui.LeanButton::get_DragThreshold()
extern void LeanButton_get_DragThreshold_mBD44FB32F34D13A3B10C3DC07552ABFC727968D0 (void);
// 0x00000005 Lean.Transition.LeanPlayer Lean.Gui.LeanButton::get_NormalTransitions()
extern void LeanButton_get_NormalTransitions_m9C8C3CC734085E74C1D2ACAE1FD119EF3BFF363C (void);
// 0x00000006 Lean.Transition.LeanPlayer Lean.Gui.LeanButton::get_DownTransitions()
extern void LeanButton_get_DownTransitions_mA4A9A782F82EFCBC00F3225F5338C7D4A47CD4BA (void);
// 0x00000007 Lean.Transition.LeanPlayer Lean.Gui.LeanButton::get_ClickTransitions()
extern void LeanButton_get_ClickTransitions_m071379DBBBB8C6E854209B5CC96FE44B8C2C24DD (void);
// 0x00000008 UnityEngine.Events.UnityEvent Lean.Gui.LeanButton::get_OnDown()
extern void LeanButton_get_OnDown_m33740945FF936DB88C32B45F504421A8767B4804 (void);
// 0x00000009 UnityEngine.Events.UnityEvent Lean.Gui.LeanButton::get_OnClick()
extern void LeanButton_get_OnClick_mB73976FE3D074A5A3B58DA3EA39EDD7239C1090A (void);
// 0x0000000A System.Void Lean.Gui.LeanButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void LeanButton_OnPointerDown_mECDCCBB5389A82D990DF2EF2F2739BF14ACFC6E6 (void);
// 0x0000000B System.Void Lean.Gui.LeanButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void LeanButton_OnPointerUp_mDDF96700B0E942F6CBA05CBF0D67E656E1EAF7D1 (void);
// 0x0000000C System.Void Lean.Gui.LeanButton::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanButton_OnBeginDrag_m8B8ECBF312B74FDBE46CE94E35850B7B8E8EDB8E (void);
// 0x0000000D System.Void Lean.Gui.LeanButton::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanButton_OnDrag_mBA4F214F9614BA1B7D3EF4B9C645BEE261CABDD0 (void);
// 0x0000000E System.Void Lean.Gui.LeanButton::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanButton_OnEndDrag_mC4597374EFE7619CFC80ECAB2CF8343B8530A96A (void);
// 0x0000000F System.Void Lean.Gui.LeanButton::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern void LeanButton_OnSubmit_m2725A979862EC1E063D689E1288DF5EA7E1CBC79 (void);
// 0x00000010 System.Void Lean.Gui.LeanButton::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void LeanButton_OnPointerExit_m880205D33903E0E58F59628338C08A5A52869D98 (void);
// 0x00000011 System.Void Lean.Gui.LeanButton::TryNormal()
extern void LeanButton_TryNormal_m3A16CC22331372778E7C3B11474943F06C8E827E (void);
// 0x00000012 System.Void Lean.Gui.LeanButton::DoClick()
extern void LeanButton_DoClick_m941A43BCDD37360C02BBA8E2348D3F592DCBC923 (void);
// 0x00000013 System.Void Lean.Gui.LeanButton::.ctor()
extern void LeanButton__ctor_mFD9F52500AFB708E18AFB9B8424EDA4DD614C8AA (void);
// 0x00000014 System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_Horizontal(System.Boolean)
extern void LeanConstrainAnchoredPosition_set_Horizontal_mFCBD63B7EF6BF5386438EC36AEC6A168B2BFFD42 (void);
// 0x00000015 System.Boolean Lean.Gui.LeanConstrainAnchoredPosition::get_Horizontal()
extern void LeanConstrainAnchoredPosition_get_Horizontal_m827DD3EA32C9E81ECC74322199CE502B3147929F (void);
// 0x00000016 System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_HorizontalPixelMin(System.Single)
extern void LeanConstrainAnchoredPosition_set_HorizontalPixelMin_m36A2E138FB241EAD7F7296F5D92D78EF8BF7EF7D (void);
// 0x00000017 System.Single Lean.Gui.LeanConstrainAnchoredPosition::get_HorizontalPixelMin()
extern void LeanConstrainAnchoredPosition_get_HorizontalPixelMin_m2BCFD5FE69A56D71302D30B5AFC9504A94CC5743 (void);
// 0x00000018 System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_HorizontalPixelMax(System.Single)
extern void LeanConstrainAnchoredPosition_set_HorizontalPixelMax_m619FBA8E956773382382D9BF3D8612CC2C603CD7 (void);
// 0x00000019 System.Single Lean.Gui.LeanConstrainAnchoredPosition::get_HorizontalPixelMax()
extern void LeanConstrainAnchoredPosition_get_HorizontalPixelMax_m4902FAD81D2EAFFA5BD1A58849CD27E7197224F3 (void);
// 0x0000001A System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_HorizontalRectMin(System.Single)
extern void LeanConstrainAnchoredPosition_set_HorizontalRectMin_m6964506D34113292DD54CBFF3C3FEAF2DEAE4F82 (void);
// 0x0000001B System.Single Lean.Gui.LeanConstrainAnchoredPosition::get_HorizontalRectMin()
extern void LeanConstrainAnchoredPosition_get_HorizontalRectMin_mD255528A73417AD16C0C551352573A0E6E9D706E (void);
// 0x0000001C System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_HorizontalRectMax(System.Single)
extern void LeanConstrainAnchoredPosition_set_HorizontalRectMax_mA83FD49711ADAED9F36CFC6F443D4D82534BF5D3 (void);
// 0x0000001D System.Single Lean.Gui.LeanConstrainAnchoredPosition::get_HorizontalRectMax()
extern void LeanConstrainAnchoredPosition_get_HorizontalRectMax_m419EAC037B88C3396B5D03E5D158D0A6D5E351F8 (void);
// 0x0000001E System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_HorizontalParentMin(System.Single)
extern void LeanConstrainAnchoredPosition_set_HorizontalParentMin_mDBD76ED80A0F403E207411A77AF2B4D5099E3138 (void);
// 0x0000001F System.Single Lean.Gui.LeanConstrainAnchoredPosition::get_HorizontalParentMin()
extern void LeanConstrainAnchoredPosition_get_HorizontalParentMin_m8429A46EF751112B14C7A29E26B6CAA006C07064 (void);
// 0x00000020 System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_HorizontalParentMax(System.Single)
extern void LeanConstrainAnchoredPosition_set_HorizontalParentMax_mFC61A84C13FE076128EDC264BE9798AFDE1155F9 (void);
// 0x00000021 System.Single Lean.Gui.LeanConstrainAnchoredPosition::get_HorizontalParentMax()
extern void LeanConstrainAnchoredPosition_get_HorizontalParentMax_m6DDC77CF0DD41115B9B39A3C97D9DBC3FE3CA947 (void);
// 0x00000022 System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_Vertical(System.Boolean)
extern void LeanConstrainAnchoredPosition_set_Vertical_mAFD18B1C4D813A3A044BB06C611CAC635870FC35 (void);
// 0x00000023 System.Boolean Lean.Gui.LeanConstrainAnchoredPosition::get_Vertical()
extern void LeanConstrainAnchoredPosition_get_Vertical_mC0D4011C134332554397247725AAB67AA69E064E (void);
// 0x00000024 System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_VerticalPixelMin(System.Single)
extern void LeanConstrainAnchoredPosition_set_VerticalPixelMin_m19BABF20CD07582E1953BFE2A20758C6F2DE941F (void);
// 0x00000025 System.Single Lean.Gui.LeanConstrainAnchoredPosition::get_VerticalPixelMin()
extern void LeanConstrainAnchoredPosition_get_VerticalPixelMin_m3E6965F8F447820E9A78E010CD7A85C5AFB0F7E4 (void);
// 0x00000026 System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_VerticalPixelMax(System.Single)
extern void LeanConstrainAnchoredPosition_set_VerticalPixelMax_mED120E9C4E676DFEADB521DC33381431BDD10BC0 (void);
// 0x00000027 System.Single Lean.Gui.LeanConstrainAnchoredPosition::get_VerticalPixelMax()
extern void LeanConstrainAnchoredPosition_get_VerticalPixelMax_m4A87CC280E37B2DDFB994DEF0A0C19C9EAEFB00F (void);
// 0x00000028 System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_VerticalRectMin(System.Single)
extern void LeanConstrainAnchoredPosition_set_VerticalRectMin_m888D42FD0962FDE5B57EEFC153BFD00E8E036959 (void);
// 0x00000029 System.Single Lean.Gui.LeanConstrainAnchoredPosition::get_VerticalRectMin()
extern void LeanConstrainAnchoredPosition_get_VerticalRectMin_m612C2B18F11D0B10B1FCCC73A09F0286D3513107 (void);
// 0x0000002A System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_VerticalRectMax(System.Single)
extern void LeanConstrainAnchoredPosition_set_VerticalRectMax_m48CF3FB36FA3013E30BD68F8C7E21FF4B9AFC102 (void);
// 0x0000002B System.Single Lean.Gui.LeanConstrainAnchoredPosition::get_VerticalRectMax()
extern void LeanConstrainAnchoredPosition_get_VerticalRectMax_m0B0F6FE4BABDAC9E8669BC625AEBBDF60BA66FEA (void);
// 0x0000002C System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_VerticalParentMin(System.Single)
extern void LeanConstrainAnchoredPosition_set_VerticalParentMin_mC7AA3D3418848EB8E661C02074DFCB491E66C208 (void);
// 0x0000002D System.Single Lean.Gui.LeanConstrainAnchoredPosition::get_VerticalParentMin()
extern void LeanConstrainAnchoredPosition_get_VerticalParentMin_mA091B49A38B2E23F9D63477E1605262905AA245F (void);
// 0x0000002E System.Void Lean.Gui.LeanConstrainAnchoredPosition::set_VerticalParentMax(System.Single)
extern void LeanConstrainAnchoredPosition_set_VerticalParentMax_mA33E2E88FE629955799303A392AA4A9A4EE69AA9 (void);
// 0x0000002F System.Single Lean.Gui.LeanConstrainAnchoredPosition::get_VerticalParentMax()
extern void LeanConstrainAnchoredPosition_get_VerticalParentMax_mC0F0D01A4CC7158A215D1B7A8082CB730CFFE41C (void);
// 0x00000030 UnityEngine.RectTransform Lean.Gui.LeanConstrainAnchoredPosition::get_CachedRectTransform()
extern void LeanConstrainAnchoredPosition_get_CachedRectTransform_m08862EA2EE4546212035C844BF4B8300D54798BF (void);
// 0x00000031 UnityEngine.Vector2 Lean.Gui.LeanConstrainAnchoredPosition::get_HorizontalRange()
extern void LeanConstrainAnchoredPosition_get_HorizontalRange_mC428E5CA6FF16D628F6092E275B6D25477088DB3 (void);
// 0x00000032 UnityEngine.Vector2 Lean.Gui.LeanConstrainAnchoredPosition::get_VerticalRange()
extern void LeanConstrainAnchoredPosition_get_VerticalRange_mF73D02AC90C2FF1FFFBB6F3FF84AE760C437D09D (void);
// 0x00000033 UnityEngine.Vector4 Lean.Gui.LeanConstrainAnchoredPosition::get_Sizes()
extern void LeanConstrainAnchoredPosition_get_Sizes_m044B16AC948F014D5A57BB7B9005A7952E19731C (void);
// 0x00000034 System.Void Lean.Gui.LeanConstrainAnchoredPosition::LateUpdate()
extern void LeanConstrainAnchoredPosition_LateUpdate_mD1375A90DDF52686C9BA380EF46E787A90124935 (void);
// 0x00000035 System.Void Lean.Gui.LeanConstrainAnchoredPosition::.ctor()
extern void LeanConstrainAnchoredPosition__ctor_mD89D09205A0E11B1469842015E759F108008C00F (void);
// 0x00000036 System.Void Lean.Gui.LeanConstrainToParent::set_Horizontal(System.Boolean)
extern void LeanConstrainToParent_set_Horizontal_m57758513D51D6C76DB7848C7F421EDA54E653D64 (void);
// 0x00000037 System.Boolean Lean.Gui.LeanConstrainToParent::get_Horizontal()
extern void LeanConstrainToParent_get_Horizontal_mBC8EC4AC96A75E60603846EAC0685BC57E4BA297 (void);
// 0x00000038 System.Void Lean.Gui.LeanConstrainToParent::set_Vertical(System.Boolean)
extern void LeanConstrainToParent_set_Vertical_mB87D3895A36C50E74D76E8FF16B1E74F8D53862D (void);
// 0x00000039 System.Boolean Lean.Gui.LeanConstrainToParent::get_Vertical()
extern void LeanConstrainToParent_get_Vertical_m3970837AB4550672A730CAE1C1CEBBC7C405586B (void);
// 0x0000003A System.Void Lean.Gui.LeanConstrainToParent::OnEnable()
extern void LeanConstrainToParent_OnEnable_mA4CE51E87E73608A62A2341F91B57C00305791D3 (void);
// 0x0000003B System.Void Lean.Gui.LeanConstrainToParent::LateUpdate()
extern void LeanConstrainToParent_LateUpdate_m1507F6FD0F0698C96E6EACA9B3945C067DC26035 (void);
// 0x0000003C System.Void Lean.Gui.LeanConstrainToParent::.ctor()
extern void LeanConstrainToParent__ctor_mCE1E59A53C030575BD3289F7F6357AADFAE52C56 (void);
// 0x0000003D System.Void Lean.Gui.LeanDrag::set_Target(UnityEngine.RectTransform)
extern void LeanDrag_set_Target_mC00850FAA844FAC25937EDC3B4260DAB2E325A36 (void);
// 0x0000003E UnityEngine.RectTransform Lean.Gui.LeanDrag::get_Target()
extern void LeanDrag_get_Target_mBE95448A4C9A464D31BDDFF050C5857929BDBDE4 (void);
// 0x0000003F System.Void Lean.Gui.LeanDrag::set_Horizontal(System.Boolean)
extern void LeanDrag_set_Horizontal_mFAF231CBDF1858F1764F8B02E4EFCCF1514EC52E (void);
// 0x00000040 System.Boolean Lean.Gui.LeanDrag::get_Horizontal()
extern void LeanDrag_get_Horizontal_mCC7C7739C6370BAC4D51CE7A1869BE71B853BA68 (void);
// 0x00000041 System.Void Lean.Gui.LeanDrag::set_Vertical(System.Boolean)
extern void LeanDrag_set_Vertical_m37A5938E8B820206F97AE40EEA1979E52EFA2662 (void);
// 0x00000042 System.Boolean Lean.Gui.LeanDrag::get_Vertical()
extern void LeanDrag_get_Vertical_m04B5A8EB0ED82C4C80F1DDB983E4EF886055CC45 (void);
// 0x00000043 Lean.Transition.LeanPlayer Lean.Gui.LeanDrag::get_BeginTransitions()
extern void LeanDrag_get_BeginTransitions_m99D0B79568C2687F56E796A8AE0C33930BA2D9C8 (void);
// 0x00000044 Lean.Transition.LeanPlayer Lean.Gui.LeanDrag::get_EndTransitions()
extern void LeanDrag_get_EndTransitions_m24E4C95291787BDB104972B5E402B8B7E2DBB456 (void);
// 0x00000045 UnityEngine.Events.UnityEvent Lean.Gui.LeanDrag::get_OnBegin()
extern void LeanDrag_get_OnBegin_m90FD0261E462CF3B9CFA9F4C1AD54B462C6D16EA (void);
// 0x00000046 UnityEngine.Events.UnityEvent Lean.Gui.LeanDrag::get_OnEnd()
extern void LeanDrag_get_OnEnd_m4464F9AAD3BA09E2692EC73DE94375DEC6793A44 (void);
// 0x00000047 System.Boolean Lean.Gui.LeanDrag::get_Dragging()
extern void LeanDrag_get_Dragging_mC14C1D74E81AAC231BC0C8B00EAA3D11A0F05635 (void);
// 0x00000048 UnityEngine.RectTransform Lean.Gui.LeanDrag::get_TargetTransform()
extern void LeanDrag_get_TargetTransform_mE8AC992B67B396EAD5DF3844E875C16C3852BDAE (void);
// 0x00000049 System.Void Lean.Gui.LeanDrag::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanDrag_OnBeginDrag_mF1698DF448DD87021AE3A69E323D534006256B40 (void);
// 0x0000004A System.Void Lean.Gui.LeanDrag::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanDrag_OnDrag_mB70E5D990780435DB2CA07861E0E293AA0DA5844 (void);
// 0x0000004B System.Void Lean.Gui.LeanDrag::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanDrag_OnEndDrag_mCB4809A84FE628285C050A81CF6AA03F32177814 (void);
// 0x0000004C System.Void Lean.Gui.LeanDrag::Start()
extern void LeanDrag_Start_m6506D225ABBC20C6D69446BAA1E7C6173CFD7817 (void);
// 0x0000004D System.Void Lean.Gui.LeanDrag::OnEnable()
extern void LeanDrag_OnEnable_m62AA2734C4393A61CE968BE9C26D8A12B0DAC909 (void);
// 0x0000004E System.Void Lean.Gui.LeanDrag::OnDisable()
extern void LeanDrag_OnDisable_mF57CE3A7208A57AE17512468F6FED7D088503E3E (void);
// 0x0000004F System.Void Lean.Gui.LeanDrag::DraggingCheck(System.Boolean&)
extern void LeanDrag_DraggingCheck_m9852D23A9BFA83A825337B0914265551C6E1EDA1 (void);
// 0x00000050 System.Boolean Lean.Gui.LeanDrag::MayDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanDrag_MayDrag_mBF7A864A793AE2AFF7F6D0E4AB3C07DFBCBC0518 (void);
// 0x00000051 System.Void Lean.Gui.LeanDrag::.ctor()
extern void LeanDrag__ctor_mF6C7412189947A073B4D011CFBFE3D428BD2840C (void);
// 0x00000052 System.Boolean Lean.Gui.LeanGui::get_IsDragging()
extern void LeanGui_get_IsDragging_mEE2B17A390A5CD62273BCBEF7F1236517C888D13 (void);
// 0x00000053 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Gui.LeanGui::RaycastGui(UnityEngine.Vector2,UnityEngine.LayerMask)
extern void LeanGui_RaycastGui_m333DFD08A73B8BBA1617DA656E757445EC3AE085 (void);
// 0x00000054 System.Boolean Lean.Gui.LeanGui::InvaidViewportPoint(UnityEngine.Camera,UnityEngine.Vector3)
extern void LeanGui_InvaidViewportPoint_m0E44D4F79AC23D9EDFE2CC441D2B4C93FA566E5E (void);
// 0x00000055 System.Void Lean.Gui.LeanGui::.cctor()
extern void LeanGui__cctor_mF746579453A85F92CA6F8925CD1DCF1CB5418B6D (void);
// 0x00000056 System.Void Lean.Gui.LeanHitbox::set_Threshold(System.Single)
extern void LeanHitbox_set_Threshold_m4755F63D4F6EA82F537A4BF2636D040B485492E8 (void);
// 0x00000057 System.Single Lean.Gui.LeanHitbox::get_Threshold()
extern void LeanHitbox_get_Threshold_mBB04198A8227F555B93E255274260CFCF945B8B2 (void);
// 0x00000058 UnityEngine.UI.Image Lean.Gui.LeanHitbox::get_CachedImage()
extern void LeanHitbox_get_CachedImage_m6441C8DAFB15691B705ABFAA4E80DD406D0857B3 (void);
// 0x00000059 System.Void Lean.Gui.LeanHitbox::UpdateThreshold()
extern void LeanHitbox_UpdateThreshold_m53243EB5C271C7A284EB48AFE53B19C09AB0D99C (void);
// 0x0000005A System.Void Lean.Gui.LeanHitbox::Start()
extern void LeanHitbox_Start_m4284551223D3CBFB17822B257FF0279130D7EC2B (void);
// 0x0000005B System.Void Lean.Gui.LeanHitbox::.ctor()
extern void LeanHitbox__ctor_mAEC33AA898A8F5D89989251784C102B7C7521335 (void);
// 0x0000005C System.Void Lean.Gui.LeanHover::set_MultiEnter(System.Boolean)
extern void LeanHover_set_MultiEnter_mA2149192813E77BA54B0AEC1D92C8DA7604EAE14 (void);
// 0x0000005D System.Boolean Lean.Gui.LeanHover::get_MultiEnter()
extern void LeanHover_get_MultiEnter_mF7C2693292745E7FD12898F433A0AE8CCAC29B3A (void);
// 0x0000005E System.Void Lean.Gui.LeanHover::set_MultiExit(System.Boolean)
extern void LeanHover_set_MultiExit_m351D724EB2AAA4E16322CF75714A4B2CE2987C33 (void);
// 0x0000005F System.Boolean Lean.Gui.LeanHover::get_MultiExit()
extern void LeanHover_get_MultiExit_m259265C8589CADC2A598F0720A9178EF74011EB5 (void);
// 0x00000060 Lean.Transition.LeanPlayer Lean.Gui.LeanHover::get_EnterTransitions()
extern void LeanHover_get_EnterTransitions_m6138EF62DE7D7B91127E8435193393F9AFDA27D5 (void);
// 0x00000061 Lean.Transition.LeanPlayer Lean.Gui.LeanHover::get_ExitTransitions()
extern void LeanHover_get_ExitTransitions_m43B20D269C0E2C136C5FC4EB92956DC881D2F8B8 (void);
// 0x00000062 UnityEngine.Events.UnityEvent Lean.Gui.LeanHover::get_OnEnter()
extern void LeanHover_get_OnEnter_m5CA209FF5497807F722E30043334107F94FC53DE (void);
// 0x00000063 UnityEngine.Events.UnityEvent Lean.Gui.LeanHover::get_OnExit()
extern void LeanHover_get_OnExit_m1B7ED0F63CEB95F8042A788485967D5B204A25FD (void);
// 0x00000064 System.Void Lean.Gui.LeanHover::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void LeanHover_OnPointerEnter_m5AA7963B478736C8264B97BE982A3617F64E19F8 (void);
// 0x00000065 System.Void Lean.Gui.LeanHover::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void LeanHover_OnPointerExit_m3FDD77856F9081D4FBBF7432F8515AB383592039 (void);
// 0x00000066 System.Void Lean.Gui.LeanHover::.ctor()
extern void LeanHover__ctor_m2A070491B3A59077049458F899543E6A5CD9F2EB (void);
// 0x00000067 System.Void Lean.Gui.LeanJoystick::set_Shape(Lean.Gui.LeanJoystick/ShapeType)
extern void LeanJoystick_set_Shape_mA2A3DFD6D0A99B052824298E06106E9BC103EFC2 (void);
// 0x00000068 Lean.Gui.LeanJoystick/ShapeType Lean.Gui.LeanJoystick::get_Shape()
extern void LeanJoystick_get_Shape_mF424DFD02627CD9D5400045CF7BA2808086D3189 (void);
// 0x00000069 System.Void Lean.Gui.LeanJoystick::set_Size(UnityEngine.Vector2)
extern void LeanJoystick_set_Size_m600D6BB07E0BB46C29D3174C2EEB7C9FB246D520 (void);
// 0x0000006A UnityEngine.Vector2 Lean.Gui.LeanJoystick::get_Size()
extern void LeanJoystick_get_Size_m3465D0A26DDE7EADBAB6E9094443451EFE0242C3 (void);
// 0x0000006B System.Void Lean.Gui.LeanJoystick::set_Radius(System.Single)
extern void LeanJoystick_set_Radius_m29DEBCAA0B41AF6DBDD76B98B328B43222145704 (void);
// 0x0000006C System.Single Lean.Gui.LeanJoystick::get_Radius()
extern void LeanJoystick_get_Radius_mD5B09B28270BE925DF72BEA1544E5D4CBD6104A3 (void);
// 0x0000006D System.Void Lean.Gui.LeanJoystick::set_Handle(UnityEngine.RectTransform)
extern void LeanJoystick_set_Handle_mCDDF26DEE13F80794940027CFD117ADDD5095C75 (void);
// 0x0000006E UnityEngine.RectTransform Lean.Gui.LeanJoystick::get_Handle()
extern void LeanJoystick_get_Handle_m4EE242C0053BFB4792655225A94BE54E89E52EC6 (void);
// 0x0000006F System.Void Lean.Gui.LeanJoystick::set_Damping(System.Single)
extern void LeanJoystick_set_Damping_m4B85CA59FB8F70B464739FB48417C51C35F6B715 (void);
// 0x00000070 System.Single Lean.Gui.LeanJoystick::get_Damping()
extern void LeanJoystick_get_Damping_m34B2FBB9B4FDE3250A185DDBBD11FEDF13AD0C39 (void);
// 0x00000071 System.Void Lean.Gui.LeanJoystick::set_SnapWhileHeld(System.Boolean)
extern void LeanJoystick_set_SnapWhileHeld_m46390CAF65321E9CB93F902D14E5216A246DC902 (void);
// 0x00000072 System.Boolean Lean.Gui.LeanJoystick::get_SnapWhileHeld()
extern void LeanJoystick_get_SnapWhileHeld_m548B60358931703E62CD140D220ECF1AA6292420 (void);
// 0x00000073 System.Void Lean.Gui.LeanJoystick::set_RelativeToOrigin(System.Boolean)
extern void LeanJoystick_set_RelativeToOrigin_m5E9922F4C07A529F1BA502C61E375E5048DF1145 (void);
// 0x00000074 System.Boolean Lean.Gui.LeanJoystick::get_RelativeToOrigin()
extern void LeanJoystick_get_RelativeToOrigin_m41F5054E7A186191599970C3D4141AFC324E688B (void);
// 0x00000075 System.Void Lean.Gui.LeanJoystick::set_RelativeRect(UnityEngine.RectTransform)
extern void LeanJoystick_set_RelativeRect_m138A59C9A91BA68C90A5714487D7ABF0264F5A6A (void);
// 0x00000076 UnityEngine.RectTransform Lean.Gui.LeanJoystick::get_RelativeRect()
extern void LeanJoystick_get_RelativeRect_m41E26464A04C45EA9670CDB9CB279016F90CF7BE (void);
// 0x00000077 UnityEngine.Vector2 Lean.Gui.LeanJoystick::get_ScaledValue()
extern void LeanJoystick_get_ScaledValue_mF788EDDE81F29D3BBCB8747C5C6713A8DBE2CEE2 (void);
// 0x00000078 Lean.Transition.LeanPlayer Lean.Gui.LeanJoystick::get_DownTransitions()
extern void LeanJoystick_get_DownTransitions_m552F95DBE24073702C1008BE5571478537D0CAEF (void);
// 0x00000079 Lean.Transition.LeanPlayer Lean.Gui.LeanJoystick::get_UpTransitions()
extern void LeanJoystick_get_UpTransitions_mD46900B046DB466E2EF15994695B3DD4CAB85D6B (void);
// 0x0000007A UnityEngine.Events.UnityEvent Lean.Gui.LeanJoystick::get_OnDown()
extern void LeanJoystick_get_OnDown_mA2B5772C35027984D6F7A94D77A56B2AE0637BE3 (void);
// 0x0000007B Lean.Gui.LeanJoystick/Vector2Event Lean.Gui.LeanJoystick::get_OnSet()
extern void LeanJoystick_get_OnSet_m187D9C7D3ECD841D9D4B17C4BFFB064E18754906 (void);
// 0x0000007C UnityEngine.Events.UnityEvent Lean.Gui.LeanJoystick::get_OnUp()
extern void LeanJoystick_get_OnUp_mF6F6C4306CBDEEA411478811CA555406722E448D (void);
// 0x0000007D UnityEngine.RectTransform Lean.Gui.LeanJoystick::get_CachedRectTransform()
extern void LeanJoystick_get_CachedRectTransform_m835D0AFC2C8602E00D0C39302F31315523E326CE (void);
// 0x0000007E System.Void Lean.Gui.LeanJoystick::Update()
extern void LeanJoystick_Update_m69DBFF4FFC81F959347A1DB54676B01BAE369785 (void);
// 0x0000007F System.Void Lean.Gui.LeanJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void LeanJoystick_OnPointerDown_m21ED494AA248149500589AD35A1BD5663F12E18C (void);
// 0x00000080 System.Void Lean.Gui.LeanJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void LeanJoystick_OnPointerUp_m4DE7D33B742901BF4A8A4A9247C7AE20C2B2EADF (void);
// 0x00000081 System.Void Lean.Gui.LeanJoystick::NullPointerNow()
extern void LeanJoystick_NullPointerNow_mB9E053FCD8FF7FB22085CE531CC799621DC0AAF5 (void);
// 0x00000082 System.Void Lean.Gui.LeanJoystick::.ctor()
extern void LeanJoystick__ctor_m7FAB6C3FD81E981524010D1E795FA6DA9D3AF057 (void);
// 0x00000083 System.Void Lean.Gui.LeanMoveToTop::set_Move(Lean.Gui.LeanMoveToTop/MoveType)
extern void LeanMoveToTop_set_Move_m64ED128FF817358F9C49C8ECB2D817C12B765AAD (void);
// 0x00000084 Lean.Gui.LeanMoveToTop/MoveType Lean.Gui.LeanMoveToTop::get_Move()
extern void LeanMoveToTop_get_Move_m9A5DEDE9906A19D1EB5E539E55C16CBED51EF301 (void);
// 0x00000085 System.Void Lean.Gui.LeanMoveToTop::set_Target(UnityEngine.Transform)
extern void LeanMoveToTop_set_Target_m389CCB4E17D82FD97328A4BA54A1DE2B4DBD3AEF (void);
// 0x00000086 UnityEngine.Transform Lean.Gui.LeanMoveToTop::get_Target()
extern void LeanMoveToTop_get_Target_mFBE8F64B64211ADE4230F65BDDC48D4DA5044A87 (void);
// 0x00000087 System.Void Lean.Gui.LeanMoveToTop::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void LeanMoveToTop_OnPointerDown_m940DB66AE47BF0209AFF741F469D45103D89CB58 (void);
// 0x00000088 System.Void Lean.Gui.LeanMoveToTop::.ctor()
extern void LeanMoveToTop__ctor_mAE0DEA74FAD21C93BBBAB195149859CF64DE2227 (void);
// 0x00000089 System.Void Lean.Gui.LeanMoveToTop::.cctor()
extern void LeanMoveToTop__cctor_m3D480D91196D27DA80155CCC7A3A8BEE2A25F19C (void);
// 0x0000008A System.Void Lean.Gui.LeanOrientation::set_AnchoredPosition(System.Boolean)
extern void LeanOrientation_set_AnchoredPosition_mBF72280249DCF0FA74109D5CA50DC821E49164B3 (void);
// 0x0000008B System.Boolean Lean.Gui.LeanOrientation::get_AnchoredPosition()
extern void LeanOrientation_get_AnchoredPosition_mD31FDBA9B5AED5F9F748D086895F1AC9B000ECEF (void);
// 0x0000008C System.Void Lean.Gui.LeanOrientation::set_AnchoredPositionL(UnityEngine.Vector2)
extern void LeanOrientation_set_AnchoredPositionL_m0B2521B042D90C72B8B974DE5F305AA8DB180D2B (void);
// 0x0000008D UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_AnchoredPositionL()
extern void LeanOrientation_get_AnchoredPositionL_mD26B26796A0AB6E5F97EB848DF9CAFA351699461 (void);
// 0x0000008E System.Void Lean.Gui.LeanOrientation::set_AnchoredPositionP(UnityEngine.Vector2)
extern void LeanOrientation_set_AnchoredPositionP_m9C2B2182E206D9553D1C8FFDB386B8C455F2B90E (void);
// 0x0000008F UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_AnchoredPositionP()
extern void LeanOrientation_get_AnchoredPositionP_m1824F9C063D218E8D142C1B57464E61783D10064 (void);
// 0x00000090 System.Void Lean.Gui.LeanOrientation::set_SizeDelta(System.Boolean)
extern void LeanOrientation_set_SizeDelta_m25C4719D9EB7B5777921D1704744FF2B98BC594D (void);
// 0x00000091 System.Boolean Lean.Gui.LeanOrientation::get_SizeDelta()
extern void LeanOrientation_get_SizeDelta_m4B855B2850F5E7583098868A2F8F3E5B45FC761F (void);
// 0x00000092 System.Void Lean.Gui.LeanOrientation::set_SizeDeltaL(UnityEngine.Vector2)
extern void LeanOrientation_set_SizeDeltaL_m048F5E354D6C95C1A93D780F5CE691D00DEEDCEF (void);
// 0x00000093 UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_SizeDeltaL()
extern void LeanOrientation_get_SizeDeltaL_m623145A673E9BA7E4FF4FF30AD01BB2747E2936C (void);
// 0x00000094 System.Void Lean.Gui.LeanOrientation::set_SizeDeltaP(UnityEngine.Vector2)
extern void LeanOrientation_set_SizeDeltaP_m043014EACF832D781B5B312C41C12050360AE55C (void);
// 0x00000095 UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_SizeDeltaP()
extern void LeanOrientation_get_SizeDeltaP_m5FB614D382C5D8DBC7B74FF0A500C7AA9C39264A (void);
// 0x00000096 System.Void Lean.Gui.LeanOrientation::set_AnchorMin(System.Boolean)
extern void LeanOrientation_set_AnchorMin_m936944BCA53F3E8223A4259B9B79DD5C9E0ECB4A (void);
// 0x00000097 System.Boolean Lean.Gui.LeanOrientation::get_AnchorMin()
extern void LeanOrientation_get_AnchorMin_mD920D658AEF0098AE0A2DBC171F876A5BB3CA958 (void);
// 0x00000098 System.Void Lean.Gui.LeanOrientation::set_AnchorMinL(UnityEngine.Vector2)
extern void LeanOrientation_set_AnchorMinL_m4519D0648B6127235228A6EDF1FF084625E0F875 (void);
// 0x00000099 UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_AnchorMinL()
extern void LeanOrientation_get_AnchorMinL_mD33516A757311839CA3E37A89DA259E1703FB385 (void);
// 0x0000009A System.Void Lean.Gui.LeanOrientation::set_AnchorMinP(UnityEngine.Vector2)
extern void LeanOrientation_set_AnchorMinP_mA1F763531B9374D2085FC2D5F9C1EC5E7533F39A (void);
// 0x0000009B UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_AnchorMinP()
extern void LeanOrientation_get_AnchorMinP_m70D9817AB5AD110CB32BBDCB42506499F0808294 (void);
// 0x0000009C System.Void Lean.Gui.LeanOrientation::set_AnchorMax(System.Boolean)
extern void LeanOrientation_set_AnchorMax_mFBA662DF88FF6FB5228ECB15697B69978FE716B4 (void);
// 0x0000009D System.Boolean Lean.Gui.LeanOrientation::get_AnchorMax()
extern void LeanOrientation_get_AnchorMax_m665B7CA01B040FD3B901C9C11D66868283C99D70 (void);
// 0x0000009E System.Void Lean.Gui.LeanOrientation::set_AnchorMaxL(UnityEngine.Vector2)
extern void LeanOrientation_set_AnchorMaxL_mA530DD1DFDA80B18A926E1591F6D6CE3B797552A (void);
// 0x0000009F UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_AnchorMaxL()
extern void LeanOrientation_get_AnchorMaxL_mF2C0EC7B72449FA46017256E4EA4C83A94AFDDE8 (void);
// 0x000000A0 System.Void Lean.Gui.LeanOrientation::set_AnchorMaxP(UnityEngine.Vector2)
extern void LeanOrientation_set_AnchorMaxP_m4122DCD1E35A9A7DD9708EE877539B4AE042BDF4 (void);
// 0x000000A1 UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_AnchorMaxP()
extern void LeanOrientation_get_AnchorMaxP_mAE3481879C349F40E19E45572D07A35088E07F12 (void);
// 0x000000A2 System.Void Lean.Gui.LeanOrientation::set_OffsetMin(System.Boolean)
extern void LeanOrientation_set_OffsetMin_m20173979405B7BF2929C0B33E8DAD4664F74E389 (void);
// 0x000000A3 System.Boolean Lean.Gui.LeanOrientation::get_OffsetMin()
extern void LeanOrientation_get_OffsetMin_m8A316B8EB235F14A465170FB7CDDE234790EA1E1 (void);
// 0x000000A4 System.Void Lean.Gui.LeanOrientation::set_OffsetMinL(UnityEngine.Vector2)
extern void LeanOrientation_set_OffsetMinL_m42BA9FB9FF28D73172407D5049E1592FFE777A31 (void);
// 0x000000A5 UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_OffsetMinL()
extern void LeanOrientation_get_OffsetMinL_mB19E52B435445A6D9B29DA98009B6ED3371F2AE7 (void);
// 0x000000A6 System.Void Lean.Gui.LeanOrientation::set_OffsetMinP(UnityEngine.Vector2)
extern void LeanOrientation_set_OffsetMinP_m5317B83211CA459CB6CCD9820A397292820438AF (void);
// 0x000000A7 UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_OffsetMinP()
extern void LeanOrientation_get_OffsetMinP_m3A812B6A086BB7477D3E45EE4BB505D857ACC3D6 (void);
// 0x000000A8 System.Void Lean.Gui.LeanOrientation::set_OffsetMax(System.Boolean)
extern void LeanOrientation_set_OffsetMax_m304CDB9931B5591FFD432B47D8F454E18C43CD18 (void);
// 0x000000A9 System.Boolean Lean.Gui.LeanOrientation::get_OffsetMax()
extern void LeanOrientation_get_OffsetMax_m15F19E534DF72CC5697C6533DAAF594EDEC6CE5C (void);
// 0x000000AA System.Void Lean.Gui.LeanOrientation::set_OffsetMaxL(UnityEngine.Vector2)
extern void LeanOrientation_set_OffsetMaxL_mA3280CC4FE3122AA93DC7C28E13736C0F3842F7E (void);
// 0x000000AB UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_OffsetMaxL()
extern void LeanOrientation_get_OffsetMaxL_mB336811C1E5E42878E4DDE27B3285F046FDDB27E (void);
// 0x000000AC System.Void Lean.Gui.LeanOrientation::set_OffsetMaxP(UnityEngine.Vector2)
extern void LeanOrientation_set_OffsetMaxP_mC7A0534F2FBC854A36C8B4221C40AC6B41B4EB9A (void);
// 0x000000AD UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_OffsetMaxP()
extern void LeanOrientation_get_OffsetMaxP_m61F7E366CDDA0CA9954E5C04D009D3A2A577E135 (void);
// 0x000000AE System.Void Lean.Gui.LeanOrientation::set_Pivot(System.Boolean)
extern void LeanOrientation_set_Pivot_m8CC8265C88AFC035A9FF4BBFFE75407A8C191F75 (void);
// 0x000000AF System.Boolean Lean.Gui.LeanOrientation::get_Pivot()
extern void LeanOrientation_get_Pivot_m2026E5E25CB2618C0EDFD4628E6C60B4EF77A5E3 (void);
// 0x000000B0 System.Void Lean.Gui.LeanOrientation::set_PivotL(UnityEngine.Vector2)
extern void LeanOrientation_set_PivotL_m5DC333A7F60E35B49156958CC25A7BD30FF707DC (void);
// 0x000000B1 UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_PivotL()
extern void LeanOrientation_get_PivotL_m2F91674CA1C9F6EA6891EEFBFEFAA59EADE73839 (void);
// 0x000000B2 System.Void Lean.Gui.LeanOrientation::set_PivotP(UnityEngine.Vector2)
extern void LeanOrientation_set_PivotP_mB602D20F984268BAB456E1438B01D6D278570CAE (void);
// 0x000000B3 UnityEngine.Vector2 Lean.Gui.LeanOrientation::get_PivotP()
extern void LeanOrientation_get_PivotP_m327DC9C33A272CF0B1CC8136BF7BF0D3C3CA5883 (void);
// 0x000000B4 System.Void Lean.Gui.LeanOrientation::set_LocalRotation(System.Boolean)
extern void LeanOrientation_set_LocalRotation_m4B61283C635511352C082D264D818B36B71BFA9E (void);
// 0x000000B5 System.Boolean Lean.Gui.LeanOrientation::get_LocalRotation()
extern void LeanOrientation_get_LocalRotation_m5D0F19573DDFE04416494FDD1A71F26E80E5B8FF (void);
// 0x000000B6 System.Void Lean.Gui.LeanOrientation::set_LocalRotationL(UnityEngine.Quaternion)
extern void LeanOrientation_set_LocalRotationL_mC63049B96E5380ADAE4BFC3817C821A7CB3637D9 (void);
// 0x000000B7 UnityEngine.Quaternion Lean.Gui.LeanOrientation::get_LocalRotationL()
extern void LeanOrientation_get_LocalRotationL_m233193D85ED057E5134B7964AE6274BA6CAA652A (void);
// 0x000000B8 System.Void Lean.Gui.LeanOrientation::set_LocalRotationP(UnityEngine.Quaternion)
extern void LeanOrientation_set_LocalRotationP_m19856CEA46C07D28BC17A57E7BDB2394E8D405BB (void);
// 0x000000B9 UnityEngine.Quaternion Lean.Gui.LeanOrientation::get_LocalRotationP()
extern void LeanOrientation_get_LocalRotationP_mA87F453CF74007D49AD851D20B6BEEA65A0D80D9 (void);
// 0x000000BA System.Void Lean.Gui.LeanOrientation::set_LocalScale(System.Boolean)
extern void LeanOrientation_set_LocalScale_mEB223AFB990C4E426D68A3BF01C22E9A81AC53CD (void);
// 0x000000BB System.Boolean Lean.Gui.LeanOrientation::get_LocalScale()
extern void LeanOrientation_get_LocalScale_m0C87709A27F58397E96D934FFA82B6BB2B094CF8 (void);
// 0x000000BC System.Void Lean.Gui.LeanOrientation::set_LocalScaleL(UnityEngine.Vector3)
extern void LeanOrientation_set_LocalScaleL_m2DF2CB69956460BEA1091C7FED43277C6FCFDBBA (void);
// 0x000000BD UnityEngine.Vector3 Lean.Gui.LeanOrientation::get_LocalScaleL()
extern void LeanOrientation_get_LocalScaleL_m592B86B8CA0049460C7C0EB60BEBE6CFFB8954D5 (void);
// 0x000000BE System.Void Lean.Gui.LeanOrientation::set_LocalScaleP(UnityEngine.Vector3)
extern void LeanOrientation_set_LocalScaleP_mA0B3B03531A2FA0387C5101B001D9C25DEF6132C (void);
// 0x000000BF UnityEngine.Vector3 Lean.Gui.LeanOrientation::get_LocalScaleP()
extern void LeanOrientation_get_LocalScaleP_m153C4741A363E8E2E39D2C61E7051FE8C69F1D9F (void);
// 0x000000C0 Lean.Transition.LeanPlayer Lean.Gui.LeanOrientation::get_LandscapeTransitions()
extern void LeanOrientation_get_LandscapeTransitions_m0DE2CFBFB4CF44C8C19C088E1BF4E827409F15F0 (void);
// 0x000000C1 Lean.Transition.LeanPlayer Lean.Gui.LeanOrientation::get_PortraitTransitions()
extern void LeanOrientation_get_PortraitTransitions_m703E227FE766A6DEFC26254E53491E64F1FEA42E (void);
// 0x000000C2 UnityEngine.Events.UnityEvent Lean.Gui.LeanOrientation::get_OnLandscape()
extern void LeanOrientation_get_OnLandscape_mD470123D8666FDD17260E9323AD39EBF9E53472B (void);
// 0x000000C3 UnityEngine.Events.UnityEvent Lean.Gui.LeanOrientation::get_OnPortrait()
extern void LeanOrientation_get_OnPortrait_m7DD9D9361D05A1DB92C089793CF5EDA4510ED80D (void);
// 0x000000C4 System.Void Lean.Gui.LeanOrientation::CopyToLandscape()
extern void LeanOrientation_CopyToLandscape_mDCE1FAFFC997411636EA132FC571FDB742F7DF10 (void);
// 0x000000C5 System.Void Lean.Gui.LeanOrientation::CopyToPortrait()
extern void LeanOrientation_CopyToPortrait_mA70B745547F2E6563D4E6780439BEA576F0C626D (void);
// 0x000000C6 System.Void Lean.Gui.LeanOrientation::UpdateOrientation()
extern void LeanOrientation_UpdateOrientation_m8771B0A8B77E15A56634BCDAA973B6C71BED7C1D (void);
// 0x000000C7 System.Void Lean.Gui.LeanOrientation::Update()
extern void LeanOrientation_Update_mD2F8E1948E1E9DC4B0009767FCE4DA1356ACE242 (void);
// 0x000000C8 System.Void Lean.Gui.LeanOrientation::UpdateCachedRectTransform()
extern void LeanOrientation_UpdateCachedRectTransform_m1CEFE711665CB88AD4A34108017D83D69A1A728E (void);
// 0x000000C9 System.Void Lean.Gui.LeanOrientation::.ctor()
extern void LeanOrientation__ctor_m709A21C653592E4B37BBFD14211973D09B5E5A80 (void);
// 0x000000CA System.Void Lean.Gui.LeanPulse::set_RemainingPulses(System.Int32)
extern void LeanPulse_set_RemainingPulses_mDA0D97B54B3A1288F88C6FDE8076B92620FC0D80 (void);
// 0x000000CB System.Int32 Lean.Gui.LeanPulse::get_RemainingPulses()
extern void LeanPulse_get_RemainingPulses_mEC41D116CDE3C939AD910268BE0F52AAB72F2E73 (void);
// 0x000000CC System.Void Lean.Gui.LeanPulse::set_RemainingTime(System.Single)
extern void LeanPulse_set_RemainingTime_mF8213DABCAE68B7AA3DCA08C59FD5D13142DA2E2 (void);
// 0x000000CD System.Single Lean.Gui.LeanPulse::get_RemainingTime()
extern void LeanPulse_get_RemainingTime_m9C223651B7E8DADEA0C0D7B4E6E1231DF65D305A (void);
// 0x000000CE System.Void Lean.Gui.LeanPulse::set_TimeInterval(System.Single)
extern void LeanPulse_set_TimeInterval_m6864792FDADDF52AB2735AA78D43797988B56AB7 (void);
// 0x000000CF System.Single Lean.Gui.LeanPulse::get_TimeInterval()
extern void LeanPulse_get_TimeInterval_m0C8311BA54351AB92F68E5BDC6DDE5EDDE6CF787 (void);
// 0x000000D0 System.Void Lean.Gui.LeanPulse::set_Timing(Lean.Transition.LeanTiming)
extern void LeanPulse_set_Timing_m209541C687D3F09D26873CFA0AB9E06991B75044 (void);
// 0x000000D1 Lean.Transition.LeanTiming Lean.Gui.LeanPulse::get_Timing()
extern void LeanPulse_get_Timing_mAD3DD01394F4C332BB06B55D886D06474CADA66E (void);
// 0x000000D2 Lean.Transition.LeanPlayer Lean.Gui.LeanPulse::get_PulseTransitions()
extern void LeanPulse_get_PulseTransitions_mD9109EADBB4D6544DBB17EFC1E73524B1A051D09 (void);
// 0x000000D3 UnityEngine.Events.UnityEvent Lean.Gui.LeanPulse::get_OnPulse()
extern void LeanPulse_get_OnPulse_m969808E602D622B1B60472B38F7B261A76F4AA0E (void);
// 0x000000D4 System.Void Lean.Gui.LeanPulse::TryPulse()
extern void LeanPulse_TryPulse_m8A51D4665DC573F87BA51310EEFDE4360DF72C59 (void);
// 0x000000D5 System.Void Lean.Gui.LeanPulse::Pulse()
extern void LeanPulse_Pulse_mE0FAF9917833656844D01C13DD71B4706FD7E8F8 (void);
// 0x000000D6 System.Void Lean.Gui.LeanPulse::TryPulseAll(System.String)
extern void LeanPulse_TryPulseAll_m818DA5CB969E2845983CCDE2EDB983A048BB5A5F (void);
// 0x000000D7 System.Void Lean.Gui.LeanPulse::PulseAll(System.String)
extern void LeanPulse_PulseAll_mA722EBF7664B67D48E2F581F7255B71A8861EE40 (void);
// 0x000000D8 System.Void Lean.Gui.LeanPulse::OnEnable()
extern void LeanPulse_OnEnable_mE7869351D21E6FD3A57C037610610F4F07CC2B94 (void);
// 0x000000D9 System.Void Lean.Gui.LeanPulse::OnDisable()
extern void LeanPulse_OnDisable_m20C1DC5C6ACA0126E2464214298C1AFCB43AD902 (void);
// 0x000000DA System.Void Lean.Gui.LeanPulse::Update()
extern void LeanPulse_Update_m4F4806B48CF230D95B122DA785C73BA076A46F42 (void);
// 0x000000DB System.Void Lean.Gui.LeanPulse::LateUpdate()
extern void LeanPulse_LateUpdate_mD5F19ADC0E4BA83FC7C7A2963E22E69B117100D4 (void);
// 0x000000DC System.Void Lean.Gui.LeanPulse::FixedUpdate()
extern void LeanPulse_FixedUpdate_m4956C4508DAA4E18A70820C170DAF36F334A8803 (void);
// 0x000000DD System.Void Lean.Gui.LeanPulse::UpdatePulse(System.Single)
extern void LeanPulse_UpdatePulse_m7E286E7AB1A7CC260CE7D4BDC9859B167F698AC2 (void);
// 0x000000DE System.Void Lean.Gui.LeanPulse::.ctor()
extern void LeanPulse__ctor_m5E8D0480BC0DB7ABF464A73D45A0EFE731FE1FCB (void);
// 0x000000DF System.Void Lean.Gui.LeanPulse::.cctor()
extern void LeanPulse__cctor_m46A41285366158E674A5FC5833779EB60C9C7AFF (void);
// 0x000000E0 System.Void Lean.Gui.LeanResize::set_Target(UnityEngine.RectTransform)
extern void LeanResize_set_Target_m37CF68AEC1600989896042D1F0973A55DE4B1842 (void);
// 0x000000E1 UnityEngine.RectTransform Lean.Gui.LeanResize::get_Target()
extern void LeanResize_get_Target_m3CD72140AA6E6014F91AF7BE8B9A2C78E391B7DE (void);
// 0x000000E2 System.Void Lean.Gui.LeanResize::set_Horizontal(System.Boolean)
extern void LeanResize_set_Horizontal_m101DE68BE2E206DA1C4242077C205D0E7C5584F9 (void);
// 0x000000E3 System.Boolean Lean.Gui.LeanResize::get_Horizontal()
extern void LeanResize_get_Horizontal_m983F402D16A8A2B4144D76EC69B4C441070F1514 (void);
// 0x000000E4 System.Void Lean.Gui.LeanResize::set_HorizontalScale(System.Single)
extern void LeanResize_set_HorizontalScale_m2818CCD6BF62FB337B8FF0D80B088FD810B7203E (void);
// 0x000000E5 System.Single Lean.Gui.LeanResize::get_HorizontalScale()
extern void LeanResize_get_HorizontalScale_mE7E6497F506D54D24A959EAA6083F84C25EE2477 (void);
// 0x000000E6 System.Void Lean.Gui.LeanResize::set_HorizontalClamp(System.Boolean)
extern void LeanResize_set_HorizontalClamp_m822EEC8214CEE6D195F10B38858D0250616C0F84 (void);
// 0x000000E7 System.Boolean Lean.Gui.LeanResize::get_HorizontalClamp()
extern void LeanResize_get_HorizontalClamp_m7BDB58755A8A5DEDAFC2CB3E7AB356037FBB6345 (void);
// 0x000000E8 System.Void Lean.Gui.LeanResize::set_HorizontalMin(System.Single)
extern void LeanResize_set_HorizontalMin_mED36E4AAC8054501F18FF086FFB5D6FAC80E6A30 (void);
// 0x000000E9 System.Single Lean.Gui.LeanResize::get_HorizontalMin()
extern void LeanResize_get_HorizontalMin_mE33EDB40CC7C1CB15801DD4A48F3CA385990AFCA (void);
// 0x000000EA System.Void Lean.Gui.LeanResize::set_HorizontalMax(System.Single)
extern void LeanResize_set_HorizontalMax_m43CA5032EFA0B1BFD3ACB9DA83D743C984C44B3A (void);
// 0x000000EB System.Single Lean.Gui.LeanResize::get_HorizontalMax()
extern void LeanResize_get_HorizontalMax_m7423942849170BE9EF7B9A09D5D5ABCF76AEBD62 (void);
// 0x000000EC System.Void Lean.Gui.LeanResize::set_Vertical(System.Boolean)
extern void LeanResize_set_Vertical_m3D71F060BA268BCFE67BE8082620C45FAEACDD34 (void);
// 0x000000ED System.Boolean Lean.Gui.LeanResize::get_Vertical()
extern void LeanResize_get_Vertical_m45552C0FFCD01EBAF1B2FCA065C7FBAE9C423FE6 (void);
// 0x000000EE System.Void Lean.Gui.LeanResize::set_VerticalScale(System.Single)
extern void LeanResize_set_VerticalScale_m899B64096A673C25A66785707B3599119D06AA8A (void);
// 0x000000EF System.Single Lean.Gui.LeanResize::get_VerticalScale()
extern void LeanResize_get_VerticalScale_mE78968FFF28DF52085309E6E2A9C8C7089466603 (void);
// 0x000000F0 System.Void Lean.Gui.LeanResize::set_VerticalClamp(System.Boolean)
extern void LeanResize_set_VerticalClamp_mDAC4229EAA714DD77FB406E38C5526B2CC8D40F4 (void);
// 0x000000F1 System.Boolean Lean.Gui.LeanResize::get_VerticalClamp()
extern void LeanResize_get_VerticalClamp_m891034820C4E1E91F2BAC20B367585CE68CCB15E (void);
// 0x000000F2 System.Void Lean.Gui.LeanResize::set_VerticalMin(System.Single)
extern void LeanResize_set_VerticalMin_m3B7DA3B4AE432139736CC77BC4F67F7EEB6F742E (void);
// 0x000000F3 System.Single Lean.Gui.LeanResize::get_VerticalMin()
extern void LeanResize_get_VerticalMin_mF5595892478CA81B4D1EA32515932CE4E28082FB (void);
// 0x000000F4 System.Void Lean.Gui.LeanResize::set_VerticalMax(System.Single)
extern void LeanResize_set_VerticalMax_m4D586C54DE1912FF0EC50DD5778D44AEF54C4D92 (void);
// 0x000000F5 System.Single Lean.Gui.LeanResize::get_VerticalMax()
extern void LeanResize_get_VerticalMax_mD63DB2E41C6D4F9504C1BBF349D95C580311F41C (void);
// 0x000000F6 Lean.Transition.LeanPlayer Lean.Gui.LeanResize::get_BeginTransitions()
extern void LeanResize_get_BeginTransitions_m82D916161080A25ABB5223D87517FF06D690A8E6 (void);
// 0x000000F7 Lean.Transition.LeanPlayer Lean.Gui.LeanResize::get_EndTransitions()
extern void LeanResize_get_EndTransitions_m0E4EC82D6F7A6DA15FEFB7E827ED64EB0391BD24 (void);
// 0x000000F8 UnityEngine.Events.UnityEvent Lean.Gui.LeanResize::get_OnBegin()
extern void LeanResize_get_OnBegin_mB7FDB4033A3A8C9725391DD8B0BB5330A5840E00 (void);
// 0x000000F9 UnityEngine.Events.UnityEvent Lean.Gui.LeanResize::get_OnEnd()
extern void LeanResize_get_OnEnd_mEF349A7898A5B478AC70945055BA5163919FAADB (void);
// 0x000000FA UnityEngine.RectTransform Lean.Gui.LeanResize::get_TargetTransform()
extern void LeanResize_get_TargetTransform_m3ED1D591A5EC18EC2215E2CF3FC08C996D8ABAD4 (void);
// 0x000000FB System.Void Lean.Gui.LeanResize::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanResize_OnBeginDrag_m0163BE2C4A3C241812A60C4D58DE293870911637 (void);
// 0x000000FC System.Void Lean.Gui.LeanResize::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanResize_OnDrag_m5E14A6D2D70D6BD4A2BF64BAABCA2FBE0049F535 (void);
// 0x000000FD System.Void Lean.Gui.LeanResize::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanResize_OnEndDrag_mCCADE879766066C949231EE4A964220B498DDD01 (void);
// 0x000000FE System.Void Lean.Gui.LeanResize::Start()
extern void LeanResize_Start_m2B09E124F35472D3FE4ED20536895FD5841148EF (void);
// 0x000000FF System.Void Lean.Gui.LeanResize::OnEnable()
extern void LeanResize_OnEnable_m7B96F62F0D981620AD82F210E95FA09D1176941F (void);
// 0x00000100 System.Void Lean.Gui.LeanResize::OnDisable()
extern void LeanResize_OnDisable_mE4A1A8C08E3964F1AFF1550EF6145EB38AD0C274 (void);
// 0x00000101 System.Void Lean.Gui.LeanResize::DraggingCheck(System.Boolean&)
extern void LeanResize_DraggingCheck_m59ED1E03C4A38006A79988280C4E28227B7EACE3 (void);
// 0x00000102 System.Boolean Lean.Gui.LeanResize::MayDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanResize_MayDrag_m68D871F80FCDD028A54E4F751DC6B3110385B0B3 (void);
// 0x00000103 System.Void Lean.Gui.LeanResize::.ctor()
extern void LeanResize__ctor_m16B644780CF9BD5EE9D70AAFEF945EC3D28AEC1F (void);
// 0x00000104 System.Void Lean.Gui.LeanSafeArea::set_Horizontal(System.Boolean)
extern void LeanSafeArea_set_Horizontal_mA9F8B558A36BC0E585359D27533C92F9A3ABE970 (void);
// 0x00000105 System.Boolean Lean.Gui.LeanSafeArea::get_Horizontal()
extern void LeanSafeArea_get_Horizontal_m7AC10F138D598C315C6238ED7B1173E6785C0F8B (void);
// 0x00000106 System.Void Lean.Gui.LeanSafeArea::set_HorizontalRange(UnityEngine.Vector2)
extern void LeanSafeArea_set_HorizontalRange_m5599CF5D8428010282BAEF899A8EA1F17EB7D471 (void);
// 0x00000107 UnityEngine.Vector2 Lean.Gui.LeanSafeArea::get_HorizontalRange()
extern void LeanSafeArea_get_HorizontalRange_m1D6125D3E4E46CAC4B7BAD5B4FD8B5D47F9D66F1 (void);
// 0x00000108 System.Void Lean.Gui.LeanSafeArea::set_Vertical(System.Boolean)
extern void LeanSafeArea_set_Vertical_mB560F44D46447E451DB44EBD4F5261C9EA7FB6E7 (void);
// 0x00000109 System.Boolean Lean.Gui.LeanSafeArea::get_Vertical()
extern void LeanSafeArea_get_Vertical_mDD73839F2CC3492ECCE1BE4634DD35AB6B4C1E8F (void);
// 0x0000010A System.Void Lean.Gui.LeanSafeArea::set_VerticalRange(UnityEngine.Vector2)
extern void LeanSafeArea_set_VerticalRange_m252C368DC0F4A24868493D3889A09948EB893B38 (void);
// 0x0000010B UnityEngine.Vector2 Lean.Gui.LeanSafeArea::get_VerticalRange()
extern void LeanSafeArea_get_VerticalRange_mDF5D7E438940918B2EFAF1D5118E6E903B56B42E (void);
// 0x0000010C System.Void Lean.Gui.LeanSafeArea::UpdateSafeArea()
extern void LeanSafeArea_UpdateSafeArea_mB68B4067F9B70E3F0A1027D8F1179323BAD44FF5 (void);
// 0x0000010D System.Void Lean.Gui.LeanSafeArea::Update()
extern void LeanSafeArea_Update_mE1D77F2157905DAB557EF0136E4F4F4DD270AF54 (void);
// 0x0000010E System.Void Lean.Gui.LeanSafeArea::.ctor()
extern void LeanSafeArea__ctor_m80FDE93F593ED87CD625008D01153F8F2A181DE0 (void);
// 0x0000010F System.Void Lean.Gui.LeanSelectable::set_interactable(System.Boolean)
extern void LeanSelectable_set_interactable_m6D1CC6A7880100D578F2A93534AEAD345255CE40 (void);
// 0x00000110 System.Boolean Lean.Gui.LeanSelectable::get_interactable()
extern void LeanSelectable_get_interactable_m894583386D9BB52A1CB7EBD512D2478CD5111F6F (void);
// 0x00000111 Lean.Transition.LeanPlayer Lean.Gui.LeanSelectable::get_InteractableTransitions()
extern void LeanSelectable_get_InteractableTransitions_m16790F3417F101DB7C8C95A5E63194D329477C6B (void);
// 0x00000112 Lean.Transition.LeanPlayer Lean.Gui.LeanSelectable::get_NonInteractableTransitions()
extern void LeanSelectable_get_NonInteractableTransitions_m13CE7828B89D4B4F61A761A83F205CE2E7777F92 (void);
// 0x00000113 UnityEngine.Events.UnityEvent Lean.Gui.LeanSelectable::get_OnInteractable()
extern void LeanSelectable_get_OnInteractable_mE61F46E49E6B2499EDE675E51835E38AE6585AFB (void);
// 0x00000114 UnityEngine.Events.UnityEvent Lean.Gui.LeanSelectable::get_OnNonInteractable()
extern void LeanSelectable_get_OnNonInteractable_m0D6E22E23D93D4E455813125193823FA415EEA7F (void);
// 0x00000115 System.Void Lean.Gui.LeanSelectable::OnCanvasGroupChanged()
extern void LeanSelectable_OnCanvasGroupChanged_m67AEDF3CBCF11C3863947FBC7D4D61E4630D8DF8 (void);
// 0x00000116 System.Void Lean.Gui.LeanSelectable::UpdateInteractable()
extern void LeanSelectable_UpdateInteractable_m1A5F989E955971C61F3383DE455F419B05D84FB7 (void);
// 0x00000117 System.Void Lean.Gui.LeanSelectable::.ctor()
extern void LeanSelectable__ctor_m290993EAD99C032A12080CAC34405380565EFAD7 (void);
// 0x00000118 Lean.Transition.LeanPlayer Lean.Gui.LeanSelection::get_SelectTransitions()
extern void LeanSelection_get_SelectTransitions_m956AA52A311138EBD7786EDFBABE4C2111266AAC (void);
// 0x00000119 Lean.Transition.LeanPlayer Lean.Gui.LeanSelection::get_DeselectTransitions()
extern void LeanSelection_get_DeselectTransitions_mA53C28E6741348A159502B598D895520BBDCEBE4 (void);
// 0x0000011A System.Void Lean.Gui.LeanSelection::Update()
extern void LeanSelection_Update_mDB92A416C59CAFAA239266C12B138B4A5737368E (void);
// 0x0000011B System.Void Lean.Gui.LeanSelection::.ctor()
extern void LeanSelection__ctor_m628AFFF5633C47EEF1156B71709A121FCB4356FF (void);
// 0x0000011C System.Void Lean.Gui.LeanSelectionHighlight::set_WorldCamera(UnityEngine.Camera)
extern void LeanSelectionHighlight_set_WorldCamera_mF3DAC7CFE5F9A38372AE54A3867274104F2FDC73 (void);
// 0x0000011D UnityEngine.Camera Lean.Gui.LeanSelectionHighlight::get_WorldCamera()
extern void LeanSelectionHighlight_get_WorldCamera_mD0A41EFBE967F78EF633D95CFAC0E46E0D1DA9CC (void);
// 0x0000011E Lean.Transition.LeanPlayer Lean.Gui.LeanSelectionHighlight::get_ShowTransitions()
extern void LeanSelectionHighlight_get_ShowTransitions_m693E6BEE8817F1E6C890211B06D51E0298B4C94B (void);
// 0x0000011F Lean.Transition.LeanPlayer Lean.Gui.LeanSelectionHighlight::get_HideTransitions()
extern void LeanSelectionHighlight_get_HideTransitions_m496951330663C63B9D0638C42D1948DAA71DF0F6 (void);
// 0x00000120 UnityEngine.Events.UnityEvent Lean.Gui.LeanSelectionHighlight::get_OnShow()
extern void LeanSelectionHighlight_get_OnShow_m9A7EFCA3438A1F3C1F7860949039CE91CFF0B9F8 (void);
// 0x00000121 UnityEngine.Events.UnityEvent Lean.Gui.LeanSelectionHighlight::get_OnHide()
extern void LeanSelectionHighlight_get_OnHide_m374415E7A4CEF5EC277B5C98ED360278D696B7C5 (void);
// 0x00000122 System.Void Lean.Gui.LeanSelectionHighlight::LateUpdate()
extern void LeanSelectionHighlight_LateUpdate_m8C6F3E3E8834DD1693E2581CFD88BAE7C0569038 (void);
// 0x00000123 System.Boolean Lean.Gui.LeanSelectionHighlight::UpdateRect(UnityEngine.RectTransform)
extern void LeanSelectionHighlight_UpdateRect_mF51DB8EAFF6B9E87157B6245A3EB53A9FCE0F4EF (void);
// 0x00000124 UnityEngine.Vector3 Lean.Gui.LeanSelectionHighlight::WorldToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3,System.Boolean)
extern void LeanSelectionHighlight_WorldToViewportPoint_m0054A8FE3150E2FA5CAC32FD33D8A6E40CDA167F (void);
// 0x00000125 System.Void Lean.Gui.LeanSelectionHighlight::.ctor()
extern void LeanSelectionHighlight__ctor_m006C3554CBB60AA561AA9B256126234D759FF792 (void);
// 0x00000126 System.Void Lean.Gui.LeanSelectionManager::set_ForceSelection(System.Boolean)
extern void LeanSelectionManager_set_ForceSelection_mFDB0980AF725DF77D7E72F125C6DF841795D6626 (void);
// 0x00000127 System.Boolean Lean.Gui.LeanSelectionManager::get_ForceSelection()
extern void LeanSelectionManager_get_ForceSelection_mFC0374C0AADE67AD8016096886B55D7C9E865E72 (void);
// 0x00000128 System.Collections.Generic.List`1<UnityEngine.UI.Selectable> Lean.Gui.LeanSelectionManager::get_SelectionHistory()
extern void LeanSelectionManager_get_SelectionHistory_mDEC760CDCC58CFBA86314EC7096A0C0772CCBB1A (void);
// 0x00000129 System.Boolean Lean.Gui.LeanSelectionManager::CanSelect(UnityEngine.UI.Selectable)
extern void LeanSelectionManager_CanSelect_m32D33DC066EA88D56E7B53A13BAC0B6A846F9B4C (void);
// 0x0000012A System.Void Lean.Gui.LeanSelectionManager::SelectFirstSelectable()
extern void LeanSelectionManager_SelectFirstSelectable_mE77857EFD5B3EC1EBA1157E3FDDE53DE8DD4E03C (void);
// 0x0000012B System.Void Lean.Gui.LeanSelectionManager::LateUpdate()
extern void LeanSelectionManager_LateUpdate_m8588AAD646EE92EAA67B77901DA5B457699007E8 (void);
// 0x0000012C System.Void Lean.Gui.LeanSelectionManager::.ctor()
extern void LeanSelectionManager__ctor_mED2519A2D3BC237A177AE03CDFD4FF3706E6993D (void);
// 0x0000012D System.Void Lean.Gui.LeanSelectionManager::.cctor()
extern void LeanSelectionManager__cctor_mB57A87BC30D120E3083327A41C2017AA3F4DACF1 (void);
// 0x0000012E System.Void Lean.Gui.LeanSelectionPriority::set_Priority(System.Single)
extern void LeanSelectionPriority_set_Priority_mD3525BA8F4C649AFC7C68C24CA4D70580844BD01 (void);
// 0x0000012F System.Single Lean.Gui.LeanSelectionPriority::get_Priority()
extern void LeanSelectionPriority_get_Priority_mAD0CF5F3BA87210C477FAC4A61BBC85DD5200E8C (void);
// 0x00000130 System.Void Lean.Gui.LeanSelectionPriority::.ctor()
extern void LeanSelectionPriority__ctor_m81AD5E6C6D573966BC4551B90EDB23DF55EE3266 (void);
// 0x00000131 System.Void Lean.Gui.LeanShake::set_Speed(System.Single)
extern void LeanShake_set_Speed_mAC52FAB8D55C72C0C04A417A0EC5A214F0A0B6F6 (void);
// 0x00000132 System.Single Lean.Gui.LeanShake::get_Speed()
extern void LeanShake_get_Speed_m8E285BB9408659052A55197570A4E6C530FA14CE (void);
// 0x00000133 System.Void Lean.Gui.LeanShake::set_Strength(System.Single)
extern void LeanShake_set_Strength_mEA6A3660983E5B5AB2D9C127472A27B7F7C8D017 (void);
// 0x00000134 System.Single Lean.Gui.LeanShake::get_Strength()
extern void LeanShake_get_Strength_mC3E0E29DF7254A2A7A102E74D18A88DDFF813310 (void);
// 0x00000135 System.Void Lean.Gui.LeanShake::set_Multiplier(System.Single)
extern void LeanShake_set_Multiplier_m0B0C580E8E470EA45C5D79D15BDBBBF48FF2D655 (void);
// 0x00000136 System.Single Lean.Gui.LeanShake::get_Multiplier()
extern void LeanShake_get_Multiplier_m737F8B7AB20DE5AB7070625171720AA6AFF8E2F9 (void);
// 0x00000137 System.Void Lean.Gui.LeanShake::set_Damping(System.Single)
extern void LeanShake_set_Damping_m40F7B8C31645911F9C0E404573D4EE9E5C5B612F (void);
// 0x00000138 System.Single Lean.Gui.LeanShake::get_Damping()
extern void LeanShake_get_Damping_m44160858D789AF8C04E47DB553A3E2BBCAA6FB99 (void);
// 0x00000139 System.Void Lean.Gui.LeanShake::set_Reduction(System.Single)
extern void LeanShake_set_Reduction_m1E38D1399DB9BB994389981F9D92F908BB4ADC73 (void);
// 0x0000013A System.Single Lean.Gui.LeanShake::get_Reduction()
extern void LeanShake_get_Reduction_m55D5DC214D560368C602B68DB80AC069481AD776 (void);
// 0x0000013B System.Void Lean.Gui.LeanShake::set_ShakePosition(UnityEngine.Vector3)
extern void LeanShake_set_ShakePosition_m1507AF7B8A597B7F22A12872A158DE3D699795EE (void);
// 0x0000013C UnityEngine.Vector3 Lean.Gui.LeanShake::get_ShakePosition()
extern void LeanShake_get_ShakePosition_m1F96678D473ABAD25189062B52330E9846C4DC06 (void);
// 0x0000013D System.Void Lean.Gui.LeanShake::set_ShakeRotation(UnityEngine.Vector3)
extern void LeanShake_set_ShakeRotation_mDC29518FF2A751390DA365EF43A0999FA3D249F4 (void);
// 0x0000013E UnityEngine.Vector3 Lean.Gui.LeanShake::get_ShakeRotation()
extern void LeanShake_get_ShakeRotation_mF08B7607237BAC5ABF2FAE9FB1469563FCE4CFE0 (void);
// 0x0000013F System.Void Lean.Gui.LeanShake::Shake(System.Single)
extern void LeanShake_Shake_mD771B10F37457F38F38ED9E63C288B6ADDE06C00 (void);
// 0x00000140 System.Void Lean.Gui.LeanShake::Start()
extern void LeanShake_Start_mC92BFB85D9963688D58F145400F875E7251C39A7 (void);
// 0x00000141 System.Void Lean.Gui.LeanShake::Update()
extern void LeanShake_Update_m7D50EED9A60A351E0896C2982FE3C9F49CD74AAF (void);
// 0x00000142 System.Single Lean.Gui.LeanShake::Sample(System.Single&,System.Single)
extern void LeanShake_Sample_m71C335DED563750C7B1C39E4E732CE08D43DF26E (void);
// 0x00000143 System.Void Lean.Gui.LeanShake::.ctor()
extern void LeanShake__ctor_m60C2F8CB24B98B1DF5449D76C786200CBEE05F08 (void);
// 0x00000144 System.Void Lean.Gui.LeanSizer::set_Target(UnityEngine.RectTransform)
extern void LeanSizer_set_Target_m24428E4E0454E19F3C8AB481B403FF09BD1BD994 (void);
// 0x00000145 UnityEngine.RectTransform Lean.Gui.LeanSizer::get_Target()
extern void LeanSizer_get_Target_m6CD6E55A24DB93CDC278CDDEB08746FADDBE7B9F (void);
// 0x00000146 System.Void Lean.Gui.LeanSizer::set_Scale(System.Boolean)
extern void LeanSizer_set_Scale_m4085F8918F5AA9195C3AC0B05E053663DB5E31ED (void);
// 0x00000147 System.Boolean Lean.Gui.LeanSizer::get_Scale()
extern void LeanSizer_get_Scale_m7776D8BD56834D164853E5E46014AB895B3EA373 (void);
// 0x00000148 System.Void Lean.Gui.LeanSizer::set_Horizontal(System.Boolean)
extern void LeanSizer_set_Horizontal_mC969AACE37166E014E0CDF62162D94237FC1AE75 (void);
// 0x00000149 System.Boolean Lean.Gui.LeanSizer::get_Horizontal()
extern void LeanSizer_get_Horizontal_mDA1B781AC14F0F8E1CCF606CFDF1E122749209D2 (void);
// 0x0000014A System.Void Lean.Gui.LeanSizer::set_HorizontalPadding(System.Single)
extern void LeanSizer_set_HorizontalPadding_m535C45EFCDDD361B686E77EBEDA62E2DE2E00275 (void);
// 0x0000014B System.Single Lean.Gui.LeanSizer::get_HorizontalPadding()
extern void LeanSizer_get_HorizontalPadding_mE1C22DDBC775F953BB319302E9C2864142B34AE0 (void);
// 0x0000014C System.Void Lean.Gui.LeanSizer::set_Vertical(System.Boolean)
extern void LeanSizer_set_Vertical_m74EF0335F7AEB2DE1538290239F6C7B0049017BB (void);
// 0x0000014D System.Boolean Lean.Gui.LeanSizer::get_Vertical()
extern void LeanSizer_get_Vertical_m6199C83420B725F0E00B2032B22EA4ED9141EB15 (void);
// 0x0000014E System.Void Lean.Gui.LeanSizer::set_VerticalPadding(System.Single)
extern void LeanSizer_set_VerticalPadding_m0269DAEC58E33328BE89681A13D2A355EC9A9727 (void);
// 0x0000014F System.Single Lean.Gui.LeanSizer::get_VerticalPadding()
extern void LeanSizer_get_VerticalPadding_m7C8BBC8F5F5702E112E244BA482E0B66A3BE08DF (void);
// 0x00000150 System.Void Lean.Gui.LeanSizer::UpdateSize()
extern void LeanSizer_UpdateSize_m8548DB7D5C69CA9222BB421B6AD125CB62CB0646 (void);
// 0x00000151 System.Void Lean.Gui.LeanSizer::OnRectTransformDimensionsChange()
extern void LeanSizer_OnRectTransformDimensionsChange_m24DEAAAB595DF1AE4A9F17682AE381987602B145 (void);
// 0x00000152 System.Void Lean.Gui.LeanSizer::.ctor()
extern void LeanSizer__ctor_m15319E43A6EA8AD5A9F693511C0E933072842690 (void);
// 0x00000153 System.Void Lean.Gui.LeanSnap::set_Horizontal(System.Boolean)
extern void LeanSnap_set_Horizontal_m1612E05431C3D6B01DB08B0381838C23A022051A (void);
// 0x00000154 System.Boolean Lean.Gui.LeanSnap::get_Horizontal()
extern void LeanSnap_get_Horizontal_m3386236ECA1E693E784E4014D338901B8CE0E484 (void);
// 0x00000155 System.Void Lean.Gui.LeanSnap::set_HorizontalOffset(System.Single)
extern void LeanSnap_set_HorizontalOffset_m1C559686DDE99D445CFD511699C003CFD5923311 (void);
// 0x00000156 System.Single Lean.Gui.LeanSnap::get_HorizontalOffset()
extern void LeanSnap_get_HorizontalOffset_m236ADAD7F6F5B0BCD06031D650B19A011DE82985 (void);
// 0x00000157 System.Void Lean.Gui.LeanSnap::set_HorizontalIntervalPixel(System.Single)
extern void LeanSnap_set_HorizontalIntervalPixel_mB1D54760CB2C7589465DA6E5FC163761A368B221 (void);
// 0x00000158 System.Single Lean.Gui.LeanSnap::get_HorizontalIntervalPixel()
extern void LeanSnap_get_HorizontalIntervalPixel_mB6C0AD01603BBF74B64CC72992FAD1341B3A1F90 (void);
// 0x00000159 System.Void Lean.Gui.LeanSnap::set_HorizontalIntervalRect(System.Single)
extern void LeanSnap_set_HorizontalIntervalRect_m93319A8B221350106AF9EBF9CCD33BD1CBE906CC (void);
// 0x0000015A System.Single Lean.Gui.LeanSnap::get_HorizontalIntervalRect()
extern void LeanSnap_get_HorizontalIntervalRect_m7B3128A68FBE9C3AECF576C33F5A75544D311DDB (void);
// 0x0000015B System.Void Lean.Gui.LeanSnap::set_HorizontalIntervalParent(System.Single)
extern void LeanSnap_set_HorizontalIntervalParent_m5AC87A8146810D01B0CEB0A5E85951A2B74580D2 (void);
// 0x0000015C System.Single Lean.Gui.LeanSnap::get_HorizontalIntervalParent()
extern void LeanSnap_get_HorizontalIntervalParent_m12EBFC281462BC6CBDAE76E806D26F080877C9E8 (void);
// 0x0000015D System.Void Lean.Gui.LeanSnap::set_HorizontalSpeed(System.Single)
extern void LeanSnap_set_HorizontalSpeed_mD3CA12169A79349DC6544DBC14651BC7DF4A5EE4 (void);
// 0x0000015E System.Single Lean.Gui.LeanSnap::get_HorizontalSpeed()
extern void LeanSnap_get_HorizontalSpeed_mA7E030B13DF4B9F1A02BCBB6A3DF1CA04F3C23D5 (void);
// 0x0000015F System.Void Lean.Gui.LeanSnap::set_Vertical(System.Boolean)
extern void LeanSnap_set_Vertical_mA1E48CC9AADEA2EF89D9DAB4522ACB68229B17A5 (void);
// 0x00000160 System.Boolean Lean.Gui.LeanSnap::get_Vertical()
extern void LeanSnap_get_Vertical_m745909C580D523F7265ABDE7B0356E6B3707D451 (void);
// 0x00000161 System.Void Lean.Gui.LeanSnap::set_VerticalOffset(System.Single)
extern void LeanSnap_set_VerticalOffset_m3D88D676B26A3133F73F8D522316FC8F60D53E26 (void);
// 0x00000162 System.Single Lean.Gui.LeanSnap::get_VerticalOffset()
extern void LeanSnap_get_VerticalOffset_mDB1021780B210103F47B6DB82BC262CE8CBEC361 (void);
// 0x00000163 System.Void Lean.Gui.LeanSnap::set_VerticalIntervalPixel(System.Single)
extern void LeanSnap_set_VerticalIntervalPixel_m0912D8B615CDCA1A18A8313E6EAF2B581F910A6B (void);
// 0x00000164 System.Single Lean.Gui.LeanSnap::get_VerticalIntervalPixel()
extern void LeanSnap_get_VerticalIntervalPixel_m73EBAF2E2169FEC447C16590D106BDC5865A1C66 (void);
// 0x00000165 System.Void Lean.Gui.LeanSnap::set_VerticalIntervalRect(System.Single)
extern void LeanSnap_set_VerticalIntervalRect_mB3C2003164BA4BA889586B8DF7803A40A8A2EDA4 (void);
// 0x00000166 System.Single Lean.Gui.LeanSnap::get_VerticalIntervalRect()
extern void LeanSnap_get_VerticalIntervalRect_mEB6DA53625BF1C65E6E9F8D58EDCE74AE1D1FAF3 (void);
// 0x00000167 System.Void Lean.Gui.LeanSnap::set_VerticalIntervalParent(System.Single)
extern void LeanSnap_set_VerticalIntervalParent_m333D3338C4BD15308BED916D8754146369DB3ADE (void);
// 0x00000168 System.Single Lean.Gui.LeanSnap::get_VerticalIntervalParent()
extern void LeanSnap_get_VerticalIntervalParent_m8B0DF6518343E56D149B82E8580F6CE8A4EF2A2E (void);
// 0x00000169 System.Void Lean.Gui.LeanSnap::set_VerticalSpeed(System.Single)
extern void LeanSnap_set_VerticalSpeed_m0C2679C96D5D71D72781A14D70583703EAC5C4EA (void);
// 0x0000016A System.Single Lean.Gui.LeanSnap::get_VerticalSpeed()
extern void LeanSnap_get_VerticalSpeed_mFA112173A02DD18542AC4E5132780E2FB9427FA2 (void);
// 0x0000016B System.Void Lean.Gui.LeanSnap::set_DisableWith(Lean.Gui.LeanDrag)
extern void LeanSnap_set_DisableWith_mF544A4B4A8B44E35488139947913C4E30E169D70 (void);
// 0x0000016C Lean.Gui.LeanDrag Lean.Gui.LeanSnap::get_DisableWith()
extern void LeanSnap_get_DisableWith_m0250D7CA3B2FC28E402A4B3969949030FCCCC44B (void);
// 0x0000016D UnityEngine.Vector2Int Lean.Gui.LeanSnap::get_Position()
extern void LeanSnap_get_Position_mEEAAC70FBF2B694AA9FA1531A13D4B7EC6069309 (void);
// 0x0000016E Lean.Gui.LeanSnap/Vector2IntEvent Lean.Gui.LeanSnap::get_OnPositionChanged()
extern void LeanSnap_get_OnPositionChanged_m4467EACA276C8C96EC2D2DED3C32B321FFDF5E45 (void);
// 0x0000016F System.Void Lean.Gui.LeanSnap::OnEnable()
extern void LeanSnap_OnEnable_mDDD4639BE0FED1539084E757363B4FFF8E59B67B (void);
// 0x00000170 System.Void Lean.Gui.LeanSnap::LateUpdate()
extern void LeanSnap_LateUpdate_m4A8F1B3A3E2F31DF8D430395BA9A39891BA9D20B (void);
// 0x00000171 UnityEngine.Vector2 Lean.Gui.LeanSnap::get_ParentSize()
extern void LeanSnap_get_ParentSize_m249AE75A44E04D0A879F6675EB5A92B305D293C6 (void);
// 0x00000172 System.Void Lean.Gui.LeanSnap::.ctor()
extern void LeanSnap__ctor_m4AB8A8392D65251F612171537630B859EA540678 (void);
// 0x00000173 System.Void Lean.Gui.LeanSnapEvent::set_Position(UnityEngine.Vector2Int)
extern void LeanSnapEvent_set_Position_m20103BEBA12A6BDF5B9F7FE608FFD38F3447221C (void);
// 0x00000174 UnityEngine.Vector2Int Lean.Gui.LeanSnapEvent::get_Position()
extern void LeanSnapEvent_get_Position_m728D59E3FD0CA120425396B57088E3CF35FFEC45 (void);
// 0x00000175 UnityEngine.Events.UnityEvent Lean.Gui.LeanSnapEvent::get_OnAction()
extern void LeanSnapEvent_get_OnAction_mBEB7B1FD51962DD2E972C0E58FEA70BDD7C19D21 (void);
// 0x00000176 System.Void Lean.Gui.LeanSnapEvent::.ctor()
extern void LeanSnapEvent__ctor_mE782D9F238EBAE8C2967E59FF5B5E2254FF17ABA (void);
// 0x00000177 System.Void Lean.Gui.LeanSwipe::set_MinimumDistance(System.Single)
extern void LeanSwipe_set_MinimumDistance_m3E6D0D1F7DE31A4168A4F5D5C024782C68C283C0 (void);
// 0x00000178 System.Single Lean.Gui.LeanSwipe::get_MinimumDistance()
extern void LeanSwipe_get_MinimumDistance_m4A42497392E411E4A4EF81D0573A5942D943C970 (void);
// 0x00000179 System.Void Lean.Gui.LeanSwipe::set_MaximumTime(System.Single)
extern void LeanSwipe_set_MaximumTime_mC5429CADAB6691EEC1A00A2D064C90AD61D174F4 (void);
// 0x0000017A System.Single Lean.Gui.LeanSwipe::get_MaximumTime()
extern void LeanSwipe_get_MaximumTime_m07448AAB68FE127FE9BA68412E81B47B1ACE91ED (void);
// 0x0000017B System.Void Lean.Gui.LeanSwipe::set_CheckAngle(System.Boolean)
extern void LeanSwipe_set_CheckAngle_m0140E24E5BE831646402A97B69D324D3331B36E7 (void);
// 0x0000017C System.Boolean Lean.Gui.LeanSwipe::get_CheckAngle()
extern void LeanSwipe_get_CheckAngle_mC9DF5C4A9D3B70DD003B3D4267BF73F2338FD870 (void);
// 0x0000017D System.Void Lean.Gui.LeanSwipe::set_DesiredAngle(System.Single)
extern void LeanSwipe_set_DesiredAngle_m8E0571DC50992F81AA99058FBA962C278D031666 (void);
// 0x0000017E System.Single Lean.Gui.LeanSwipe::get_DesiredAngle()
extern void LeanSwipe_get_DesiredAngle_mF46B743C71C0D0F05605B3F2CB0090D17F021D29 (void);
// 0x0000017F System.Void Lean.Gui.LeanSwipe::set_MaximumRange(System.Single)
extern void LeanSwipe_set_MaximumRange_m5D78653C6BDA538C6027949F0C72AA451B99C372 (void);
// 0x00000180 System.Single Lean.Gui.LeanSwipe::get_MaximumRange()
extern void LeanSwipe_get_MaximumRange_m65887249DB16B6F26E4A38B10FC5B8E1133B77DD (void);
// 0x00000181 Lean.Transition.LeanPlayer Lean.Gui.LeanSwipe::get_SwipeTransitions()
extern void LeanSwipe_get_SwipeTransitions_m52852A33053CC410FE126D09C6B5CB3CFC03EA54 (void);
// 0x00000182 UnityEngine.Events.UnityEvent Lean.Gui.LeanSwipe::get_OnSwipe()
extern void LeanSwipe_get_OnSwipe_m61B73E36D0044B1B5F66FFA042A2E14D4FE0C178 (void);
// 0x00000183 UnityEngine.RectTransform Lean.Gui.LeanSwipe::get_CachedRectTransform()
extern void LeanSwipe_get_CachedRectTransform_m6CCCB992CEF297167383F424F19807DF3A42AC96 (void);
// 0x00000184 System.Void Lean.Gui.LeanSwipe::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanSwipe_OnBeginDrag_mFA845AA39EC67879AEFE0DAA133C1FC87828DAC3 (void);
// 0x00000185 System.Void Lean.Gui.LeanSwipe::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanSwipe_OnDrag_m25B56910AB0A65B10525AADE25C1A1DFBF8CD123 (void);
// 0x00000186 System.Void Lean.Gui.LeanSwipe::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void LeanSwipe_OnEndDrag_m0C119C24918023E5EACC83263EA537C593C9E636 (void);
// 0x00000187 System.Void Lean.Gui.LeanSwipe::Update()
extern void LeanSwipe_Update_m35BF6547E1226E36D79B4375A7BE2243991DA518 (void);
// 0x00000188 System.Void Lean.Gui.LeanSwipe::.ctor()
extern void LeanSwipe__ctor_m7877E1537093B28A3D6CE66FC03643FD3BFA8621 (void);
// 0x00000189 System.Void Lean.Gui.LeanSwitch::set_State(System.Int32)
extern void LeanSwitch_set_State_mD1A1BD92D5CE9913AAC8F23D38F266ED6E62BC4E (void);
// 0x0000018A System.Int32 Lean.Gui.LeanSwitch::get_State()
extern void LeanSwitch_get_State_m35E7587B2AC53E5637D15808BC00D9DDA65A4756 (void);
// 0x0000018B System.Collections.Generic.List`1<Lean.Transition.LeanPlayer> Lean.Gui.LeanSwitch::get_States()
extern void LeanSwitch_get_States_m1A4899F92AF673D602736ACAFF700D05416FB801 (void);
// 0x0000018C Lean.Transition.LeanPlayer Lean.Gui.LeanSwitch::get_ChangedStateTransitions()
extern void LeanSwitch_get_ChangedStateTransitions_mE9EE889D6AECCDA3565795E71C5FBEDF5BFCE3E9 (void);
// 0x0000018D Lean.Gui.LeanSwitch/IntUnityEvent Lean.Gui.LeanSwitch::get_OnChangedState()
extern void LeanSwitch_get_OnChangedState_mEDE3567593994165A6E01A443885A0F9B6DD4921 (void);
// 0x0000018E System.Void Lean.Gui.LeanSwitch::Switch(System.Int32)
extern void LeanSwitch_Switch_m31E363EF53525993AC595F451936EC189E5216B8 (void);
// 0x0000018F System.Void Lean.Gui.LeanSwitch::SwitchAll(System.String,System.Int32)
extern void LeanSwitch_SwitchAll_mD69ED936744605AB38D0D3BC8B26CA08B1438993 (void);
// 0x00000190 System.Void Lean.Gui.LeanSwitch::OnEnable()
extern void LeanSwitch_OnEnable_m96DFA14D8006CA53D994716E8C7DD9AC4F5772A7 (void);
// 0x00000191 System.Void Lean.Gui.LeanSwitch::OnDisable()
extern void LeanSwitch_OnDisable_mD20C37155D2292EC974C5D1E482FF894E112EC42 (void);
// 0x00000192 System.Void Lean.Gui.LeanSwitch::.ctor()
extern void LeanSwitch__ctor_m6E3F1862418E9CDF75BC23DE2686EF200DC1B81A (void);
// 0x00000193 System.Void Lean.Gui.LeanSwitch::.cctor()
extern void LeanSwitch__cctor_mAE4E1BF0784C5804BA4FBEAD42910E297747A784 (void);
// 0x00000194 System.Void Lean.Gui.LeanToggle::set_On(System.Boolean)
extern void LeanToggle_set_On_mFE64A96EF5015D8458357CB9F6B016030748987E (void);
// 0x00000195 System.Boolean Lean.Gui.LeanToggle::get_On()
extern void LeanToggle_get_On_mEDB8C73D0A187FF10BD250792E8E9799069065AD (void);
// 0x00000196 System.Void Lean.Gui.LeanToggle::set_TurnOffSiblings(System.Boolean)
extern void LeanToggle_set_TurnOffSiblings_mDE31F8EE04CC6106E8D6CA31B83A1720F513F1D3 (void);
// 0x00000197 System.Boolean Lean.Gui.LeanToggle::get_TurnOffSiblings()
extern void LeanToggle_get_TurnOffSiblings_m7073CEE6576375F37B00700479138FAF2CFC7B18 (void);
// 0x00000198 Lean.Transition.LeanPlayer Lean.Gui.LeanToggle::get_OnTransitions()
extern void LeanToggle_get_OnTransitions_m63AB565AF90BDB8D2C1C54460764212687CBC3E2 (void);
// 0x00000199 Lean.Transition.LeanPlayer Lean.Gui.LeanToggle::get_OffTransitions()
extern void LeanToggle_get_OffTransitions_mBB0DB0A9AE04FAA33A56026B1540E2E7A7BF6CF2 (void);
// 0x0000019A UnityEngine.Events.UnityEvent Lean.Gui.LeanToggle::get_OnOn()
extern void LeanToggle_get_OnOn_m2D3FD3D3CA8D253E6A7D94BD52C5A4EC7E0F0C1F (void);
// 0x0000019B UnityEngine.Events.UnityEvent Lean.Gui.LeanToggle::get_OnOff()
extern void LeanToggle_get_OnOff_m19295E35EA64AD58F124A99E118731F819354F30 (void);
// 0x0000019C System.Void Lean.Gui.LeanToggle::Set(System.Boolean)
extern void LeanToggle_Set_m079B02A4C923E6A1E7E8A9CC7630E5263DC5434F (void);
// 0x0000019D System.Void Lean.Gui.LeanToggle::Toggle()
extern void LeanToggle_Toggle_m2624FBAF0DCA08674D336E7DF00D8BDDA53E91D2 (void);
// 0x0000019E System.Void Lean.Gui.LeanToggle::TurnOn()
extern void LeanToggle_TurnOn_m633A8F4B9607EAA607278681830E3D762D57F53E (void);
// 0x0000019F System.Void Lean.Gui.LeanToggle::TurnOff()
extern void LeanToggle_TurnOff_mBE8CA67BF290EAE616F9947073A1AB05165A9183 (void);
// 0x000001A0 System.Void Lean.Gui.LeanToggle::TurnOffSiblingsNow()
extern void LeanToggle_TurnOffSiblingsNow_m61F532DFD5C3AC6FF7442DDCB362ABFA2B9C9F4C (void);
// 0x000001A1 System.Void Lean.Gui.LeanToggle::TurnOffOthersNow()
extern void LeanToggle_TurnOffOthersNow_m1A31C50170E718E8B466226B90E8B6DF66C3076F (void);
// 0x000001A2 System.Boolean Lean.Gui.LeanToggle::AllOn(System.String)
extern void LeanToggle_AllOn_m60CA3A3AF9F232F47A9AA3DD15147E6B6CE933FC (void);
// 0x000001A3 System.Boolean Lean.Gui.LeanToggle::AllOff(System.String)
extern void LeanToggle_AllOff_m6F6B5E129F6208860830D675C3F565E6E0196175 (void);
// 0x000001A4 System.Void Lean.Gui.LeanToggle::SetAll(System.String,System.Boolean)
extern void LeanToggle_SetAll_mB2C6506EF8E5A7D5A7C154ECC30D21EC3B21E239 (void);
// 0x000001A5 System.Void Lean.Gui.LeanToggle::ToggleAll(System.String)
extern void LeanToggle_ToggleAll_m818318E289B5A4FB045E8326D7602A65B35CFE96 (void);
// 0x000001A6 System.Void Lean.Gui.LeanToggle::TurnOnAll(System.String)
extern void LeanToggle_TurnOnAll_m33B71D9012B6A7A27D112B4A78B5681334F0FBDE (void);
// 0x000001A7 System.Void Lean.Gui.LeanToggle::TurnOffAll(System.String)
extern void LeanToggle_TurnOffAll_m5D1391C5C285ED90F520DCD27C28F005902954AE (void);
// 0x000001A8 System.Void Lean.Gui.LeanToggle::TurnOnNow()
extern void LeanToggle_TurnOnNow_m9544DEA88291706FC2A6B2989BED7E1450A35AB3 (void);
// 0x000001A9 System.Void Lean.Gui.LeanToggle::TurnOffNow()
extern void LeanToggle_TurnOffNow_m01B0806241C4A76B1DCB568D965406C31D97698E (void);
// 0x000001AA System.Void Lean.Gui.LeanToggle::OnEnable()
extern void LeanToggle_OnEnable_mB71E2D18EF0F7C3A1064721F6BEDFBEE1EDC576C (void);
// 0x000001AB System.Void Lean.Gui.LeanToggle::OnDisable()
extern void LeanToggle_OnDisable_m3F1393116026CD390475807456023AC058432E4E (void);
// 0x000001AC System.Void Lean.Gui.LeanToggle::.ctor()
extern void LeanToggle__ctor_mE2497ACC87D76C668F0FFE2015E9ACFC3A86B5C8 (void);
// 0x000001AD System.Void Lean.Gui.LeanToggle::.cctor()
extern void LeanToggle__cctor_m95F73A98394D12E392CE774460F54DB4530BD072 (void);
// 0x000001AE System.Void Lean.Gui.LeanTooltip::set_Activation(Lean.Gui.LeanTooltip/ActivationType)
extern void LeanTooltip_set_Activation_mFA21C11F86BE0B019CC23F811A1D3C647F941F18 (void);
// 0x000001AF Lean.Gui.LeanTooltip/ActivationType Lean.Gui.LeanTooltip::get_Activation()
extern void LeanTooltip_get_Activation_mBACA925B51A492AA4F1D29C19840824F2412713D (void);
// 0x000001B0 System.Void Lean.Gui.LeanTooltip::set_ShowDelay(System.Single)
extern void LeanTooltip_set_ShowDelay_m2A854DE4D56EC7CDAAB5216D1DEB28BB9263B0FE (void);
// 0x000001B1 System.Single Lean.Gui.LeanTooltip::get_ShowDelay()
extern void LeanTooltip_get_ShowDelay_m9A62E40F0B45E3D8A51D8A50E4D4BE01EA9D54F7 (void);
// 0x000001B2 System.Void Lean.Gui.LeanTooltip::set_Move(System.Boolean)
extern void LeanTooltip_set_Move_mD8D1453EBC1199D3276E05B2D2D46ABEDE373EBB (void);
// 0x000001B3 System.Boolean Lean.Gui.LeanTooltip::get_Move()
extern void LeanTooltip_get_Move_mFDB3497A80C13ECF50C7AF378425747EF0CF8528 (void);
// 0x000001B4 System.Void Lean.Gui.LeanTooltip::set_Boundary(Lean.Gui.LeanTooltip/BoundaryType)
extern void LeanTooltip_set_Boundary_mD0F804E89207D1C6DB4D20BA50BB9A01B516162E (void);
// 0x000001B5 Lean.Gui.LeanTooltip/BoundaryType Lean.Gui.LeanTooltip::get_Boundary()
extern void LeanTooltip_get_Boundary_m009797289FDFA6B015E2F2E72E2C06BF9066CADC (void);
// 0x000001B6 Lean.Transition.LeanPlayer Lean.Gui.LeanTooltip::get_ShowTransitions()
extern void LeanTooltip_get_ShowTransitions_mA376B0AD5DF9181763250DBF482801010EFAECEF (void);
// 0x000001B7 Lean.Transition.LeanPlayer Lean.Gui.LeanTooltip::get_HideTransitions()
extern void LeanTooltip_get_HideTransitions_mB9C28E5D2751AC320D6602848E99A584B8100910 (void);
// 0x000001B8 Lean.Gui.LeanTooltip/UnityEventString Lean.Gui.LeanTooltip::get_OnShow()
extern void LeanTooltip_get_OnShow_mC7CF2F479342006D2DC2EB92696100EACD764910 (void);
// 0x000001B9 UnityEngine.Events.UnityEvent Lean.Gui.LeanTooltip::get_OnHide()
extern void LeanTooltip_get_OnHide_mED0228775499F97EE35EA1A1F3E80C30F1A4A00D (void);
// 0x000001BA System.Void Lean.Gui.LeanTooltip::Update()
extern void LeanTooltip_Update_mD79E9F3B7C56A203B36D0A51F0251C9A034829A2 (void);
// 0x000001BB System.Void Lean.Gui.LeanTooltip::Show()
extern void LeanTooltip_Show_m6597DA564F2641D1BD3A4D13D4FAAC431D29A406 (void);
// 0x000001BC System.Void Lean.Gui.LeanTooltip::Hide()
extern void LeanTooltip_Hide_m96DAE44ABA9A70FBD87AD4F898C16462441BB71E (void);
// 0x000001BD System.Void Lean.Gui.LeanTooltip::.ctor()
extern void LeanTooltip__ctor_mF6199DDA8BDB89FE64E18C85943E66F99BB087FF (void);
// 0x000001BE System.Void Lean.Gui.LeanTooltip::.cctor()
extern void LeanTooltip__cctor_m1DBAB3A5D65944AE5D83AE5FBDB51B1C2A182F2D (void);
// 0x000001BF System.Void Lean.Gui.LeanTooltipData::set_Selectable(UnityEngine.UI.Selectable)
extern void LeanTooltipData_set_Selectable_m128AAEE7910C7D612ACA08D49A312C411C73DC5D (void);
// 0x000001C0 UnityEngine.UI.Selectable Lean.Gui.LeanTooltipData::get_Selectable()
extern void LeanTooltipData_get_Selectable_m2E957AFC77CBB11E3D8BFEB0B28580C3A86195F9 (void);
// 0x000001C1 System.Void Lean.Gui.LeanTooltipData::set_Text(System.String)
extern void LeanTooltipData_set_Text_m0FE8D94A6FF527725321F59984282602FA1FE2D4 (void);
// 0x000001C2 System.String Lean.Gui.LeanTooltipData::get_Text()
extern void LeanTooltipData_get_Text_m5F1588BF423A2C0FAE956FC850D04F8DDEF877F7 (void);
// 0x000001C3 System.Void Lean.Gui.LeanTooltipData::Update()
extern void LeanTooltipData_Update_mF86765962FD24589E65E27BB2BB1DB7B5ABDF0EA (void);
// 0x000001C4 System.Void Lean.Gui.LeanTooltipData::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void LeanTooltipData_OnPointerEnter_mB2F7D5ADFA1D3473A55E0CB90E0EB1174DFCF5EA (void);
// 0x000001C5 System.Void Lean.Gui.LeanTooltipData::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void LeanTooltipData_OnPointerExit_m7BE6537264DCD78E9AA857B1451032A47B700C16 (void);
// 0x000001C6 System.Void Lean.Gui.LeanTooltipData::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void LeanTooltipData_OnPointerDown_m9500FCD6458E97B8DCA5CA20778843EFE0990231 (void);
// 0x000001C7 System.Void Lean.Gui.LeanTooltipData::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void LeanTooltipData_OnPointerUp_m90FE7A544F5B93CB142EA27DE049CDD31C7EA51D (void);
// 0x000001C8 System.Void Lean.Gui.LeanTooltipData::.ctor()
extern void LeanTooltipData__ctor_mB78F66093C2C9DE09FD7E440EE927534F02C6552 (void);
// 0x000001C9 System.Void Lean.Gui.LeanWindow::TurnOnNow()
extern void LeanWindow_TurnOnNow_mC3C318F9F6F7EDF4345799E2E0A7382B4C1E7355 (void);
// 0x000001CA System.Void Lean.Gui.LeanWindow::.ctor()
extern void LeanWindow__ctor_m402C38BCBF9C2696BFA82AA3020F08EC5D60850A (void);
// 0x000001CB System.Void Lean.Gui.LeanWindowCloser::set_CloseKey(UnityEngine.KeyCode)
extern void LeanWindowCloser_set_CloseKey_m08785222B351290AE923A91D7151908D6A2F6CFA (void);
// 0x000001CC UnityEngine.KeyCode Lean.Gui.LeanWindowCloser::get_CloseKey()
extern void LeanWindowCloser_get_CloseKey_m131E1C8734FBE95BF6FD042D7812BCCF4A5A043F (void);
// 0x000001CD System.Void Lean.Gui.LeanWindowCloser::set_EmptyWindow(Lean.Gui.LeanWindow)
extern void LeanWindowCloser_set_EmptyWindow_m3615BEE0DAEDAD02F75F4BF97F90F7646794F1B6 (void);
// 0x000001CE Lean.Gui.LeanWindow Lean.Gui.LeanWindowCloser::get_EmptyWindow()
extern void LeanWindowCloser_get_EmptyWindow_m2396DD6345A12984BB328CF8624743FF6F1CE2CB (void);
// 0x000001CF System.Collections.Generic.List`1<Lean.Gui.LeanWindow> Lean.Gui.LeanWindowCloser::get_WindowOrder()
extern void LeanWindowCloser_get_WindowOrder_m6A789ECC88A2EAC873506A4947648D220990E50F (void);
// 0x000001D0 System.Void Lean.Gui.LeanWindowCloser::Register(Lean.Gui.LeanWindow)
extern void LeanWindowCloser_Register_m18DF24AC193EA02DCFDB8F13BF580B573819E42D (void);
// 0x000001D1 System.Void Lean.Gui.LeanWindowCloser::CloseAll()
extern void LeanWindowCloser_CloseAll_m660AA5F682F2E35935CE86716DF03AB1EFE9EEB3 (void);
// 0x000001D2 System.Void Lean.Gui.LeanWindowCloser::CloseTopMost()
extern void LeanWindowCloser_CloseTopMost_m13990636891BA9907E6DA03C4D53F88F290EB601 (void);
// 0x000001D3 System.Void Lean.Gui.LeanWindowCloser::OnEnable()
extern void LeanWindowCloser_OnEnable_m1F1D62B9E6D38FE37933D4F02A7D8FBD5781D7DE (void);
// 0x000001D4 System.Void Lean.Gui.LeanWindowCloser::OnDisable()
extern void LeanWindowCloser_OnDisable_mADD098805D07A16913FFD5BDFE1CAD8236FF1E7C (void);
// 0x000001D5 System.Void Lean.Gui.LeanWindowCloser::Update()
extern void LeanWindowCloser_Update_m1ABF60BE1D3B91A6935F159FA4BA924FD489EFFF (void);
// 0x000001D6 System.Void Lean.Gui.LeanWindowCloser::RegisterNow(Lean.Gui.LeanWindow)
extern void LeanWindowCloser_RegisterNow_mF30AE11FE4D7CD5680FE98970EA1647C6FBA2889 (void);
// 0x000001D7 System.Void Lean.Gui.LeanWindowCloser::.ctor()
extern void LeanWindowCloser__ctor_mB6C5164E32BA14845A42297D76EF067F3DE7D7BA (void);
// 0x000001D8 System.Void Lean.Gui.LeanWindowCloser::.cctor()
extern void LeanWindowCloser__cctor_m4BCD98259579A2DD6D73566EB48A61D49AAC142C (void);
// 0x000001D9 System.Void Lean.Gui.LeanGui/DraggingDelegate::.ctor(System.Object,System.IntPtr)
extern void DraggingDelegate__ctor_m3CCA0D325CE92FB84EA7CB04F870A1F7A982FBAF (void);
// 0x000001DA System.Void Lean.Gui.LeanGui/DraggingDelegate::Invoke(System.Boolean&)
extern void DraggingDelegate_Invoke_m4A3D9E8E2DCF729A985FB9001DA6A1E36DF4EA8D (void);
// 0x000001DB System.IAsyncResult Lean.Gui.LeanGui/DraggingDelegate::BeginInvoke(System.Boolean&,System.AsyncCallback,System.Object)
extern void DraggingDelegate_BeginInvoke_m99240362B3A6C70C605CC6DE6E13A8BE4402DCCE (void);
// 0x000001DC System.Void Lean.Gui.LeanGui/DraggingDelegate::EndInvoke(System.Boolean&,System.IAsyncResult)
extern void DraggingDelegate_EndInvoke_m8CC7C74C4975D39BBCA39650E208C18433675C6F (void);
// 0x000001DD System.Void Lean.Gui.LeanJoystick/Vector2Event::.ctor()
extern void Vector2Event__ctor_m5A44A356A94CC25A973E1AA5188430862644C02F (void);
// 0x000001DE System.Void Lean.Gui.LeanSnap/Vector2IntEvent::.ctor()
extern void Vector2IntEvent__ctor_m8A2A3486B1A5719E1D957BE6BF008A86FEE5946F (void);
// 0x000001DF System.Void Lean.Gui.LeanSwitch/IntUnityEvent::.ctor()
extern void IntUnityEvent__ctor_mF83077FDA40BD8447B6CB8624D6C50B5988CE338 (void);
// 0x000001E0 System.Void Lean.Gui.LeanTooltip/UnityEventString::.ctor()
extern void UnityEventString__ctor_m04212A3A3F82FE3A43D33DE626360A5E082F34A0 (void);
static Il2CppMethodPointer s_methodPointers[480] = 
{
	LeanButton_set_MultiDown_m88FBEE0CD166F2C801B580281761B9E446D926CD,
	LeanButton_get_MultiDown_m47BCBC6F29CC4D8B2F73E08588225AB10AC54167,
	LeanButton_set_DragThreshold_m4272966CB8E5497F927D799F2B1611D5598CA53E,
	LeanButton_get_DragThreshold_mBD44FB32F34D13A3B10C3DC07552ABFC727968D0,
	LeanButton_get_NormalTransitions_m9C8C3CC734085E74C1D2ACAE1FD119EF3BFF363C,
	LeanButton_get_DownTransitions_mA4A9A782F82EFCBC00F3225F5338C7D4A47CD4BA,
	LeanButton_get_ClickTransitions_m071379DBBBB8C6E854209B5CC96FE44B8C2C24DD,
	LeanButton_get_OnDown_m33740945FF936DB88C32B45F504421A8767B4804,
	LeanButton_get_OnClick_mB73976FE3D074A5A3B58DA3EA39EDD7239C1090A,
	LeanButton_OnPointerDown_mECDCCBB5389A82D990DF2EF2F2739BF14ACFC6E6,
	LeanButton_OnPointerUp_mDDF96700B0E942F6CBA05CBF0D67E656E1EAF7D1,
	LeanButton_OnBeginDrag_m8B8ECBF312B74FDBE46CE94E35850B7B8E8EDB8E,
	LeanButton_OnDrag_mBA4F214F9614BA1B7D3EF4B9C645BEE261CABDD0,
	LeanButton_OnEndDrag_mC4597374EFE7619CFC80ECAB2CF8343B8530A96A,
	LeanButton_OnSubmit_m2725A979862EC1E063D689E1288DF5EA7E1CBC79,
	LeanButton_OnPointerExit_m880205D33903E0E58F59628338C08A5A52869D98,
	LeanButton_TryNormal_m3A16CC22331372778E7C3B11474943F06C8E827E,
	LeanButton_DoClick_m941A43BCDD37360C02BBA8E2348D3F592DCBC923,
	LeanButton__ctor_mFD9F52500AFB708E18AFB9B8424EDA4DD614C8AA,
	LeanConstrainAnchoredPosition_set_Horizontal_mFCBD63B7EF6BF5386438EC36AEC6A168B2BFFD42,
	LeanConstrainAnchoredPosition_get_Horizontal_m827DD3EA32C9E81ECC74322199CE502B3147929F,
	LeanConstrainAnchoredPosition_set_HorizontalPixelMin_m36A2E138FB241EAD7F7296F5D92D78EF8BF7EF7D,
	LeanConstrainAnchoredPosition_get_HorizontalPixelMin_m2BCFD5FE69A56D71302D30B5AFC9504A94CC5743,
	LeanConstrainAnchoredPosition_set_HorizontalPixelMax_m619FBA8E956773382382D9BF3D8612CC2C603CD7,
	LeanConstrainAnchoredPosition_get_HorizontalPixelMax_m4902FAD81D2EAFFA5BD1A58849CD27E7197224F3,
	LeanConstrainAnchoredPosition_set_HorizontalRectMin_m6964506D34113292DD54CBFF3C3FEAF2DEAE4F82,
	LeanConstrainAnchoredPosition_get_HorizontalRectMin_mD255528A73417AD16C0C551352573A0E6E9D706E,
	LeanConstrainAnchoredPosition_set_HorizontalRectMax_mA83FD49711ADAED9F36CFC6F443D4D82534BF5D3,
	LeanConstrainAnchoredPosition_get_HorizontalRectMax_m419EAC037B88C3396B5D03E5D158D0A6D5E351F8,
	LeanConstrainAnchoredPosition_set_HorizontalParentMin_mDBD76ED80A0F403E207411A77AF2B4D5099E3138,
	LeanConstrainAnchoredPosition_get_HorizontalParentMin_m8429A46EF751112B14C7A29E26B6CAA006C07064,
	LeanConstrainAnchoredPosition_set_HorizontalParentMax_mFC61A84C13FE076128EDC264BE9798AFDE1155F9,
	LeanConstrainAnchoredPosition_get_HorizontalParentMax_m6DDC77CF0DD41115B9B39A3C97D9DBC3FE3CA947,
	LeanConstrainAnchoredPosition_set_Vertical_mAFD18B1C4D813A3A044BB06C611CAC635870FC35,
	LeanConstrainAnchoredPosition_get_Vertical_mC0D4011C134332554397247725AAB67AA69E064E,
	LeanConstrainAnchoredPosition_set_VerticalPixelMin_m19BABF20CD07582E1953BFE2A20758C6F2DE941F,
	LeanConstrainAnchoredPosition_get_VerticalPixelMin_m3E6965F8F447820E9A78E010CD7A85C5AFB0F7E4,
	LeanConstrainAnchoredPosition_set_VerticalPixelMax_mED120E9C4E676DFEADB521DC33381431BDD10BC0,
	LeanConstrainAnchoredPosition_get_VerticalPixelMax_m4A87CC280E37B2DDFB994DEF0A0C19C9EAEFB00F,
	LeanConstrainAnchoredPosition_set_VerticalRectMin_m888D42FD0962FDE5B57EEFC153BFD00E8E036959,
	LeanConstrainAnchoredPosition_get_VerticalRectMin_m612C2B18F11D0B10B1FCCC73A09F0286D3513107,
	LeanConstrainAnchoredPosition_set_VerticalRectMax_m48CF3FB36FA3013E30BD68F8C7E21FF4B9AFC102,
	LeanConstrainAnchoredPosition_get_VerticalRectMax_m0B0F6FE4BABDAC9E8669BC625AEBBDF60BA66FEA,
	LeanConstrainAnchoredPosition_set_VerticalParentMin_mC7AA3D3418848EB8E661C02074DFCB491E66C208,
	LeanConstrainAnchoredPosition_get_VerticalParentMin_mA091B49A38B2E23F9D63477E1605262905AA245F,
	LeanConstrainAnchoredPosition_set_VerticalParentMax_mA33E2E88FE629955799303A392AA4A9A4EE69AA9,
	LeanConstrainAnchoredPosition_get_VerticalParentMax_mC0F0D01A4CC7158A215D1B7A8082CB730CFFE41C,
	LeanConstrainAnchoredPosition_get_CachedRectTransform_m08862EA2EE4546212035C844BF4B8300D54798BF,
	LeanConstrainAnchoredPosition_get_HorizontalRange_mC428E5CA6FF16D628F6092E275B6D25477088DB3,
	LeanConstrainAnchoredPosition_get_VerticalRange_mF73D02AC90C2FF1FFFBB6F3FF84AE760C437D09D,
	LeanConstrainAnchoredPosition_get_Sizes_m044B16AC948F014D5A57BB7B9005A7952E19731C,
	LeanConstrainAnchoredPosition_LateUpdate_mD1375A90DDF52686C9BA380EF46E787A90124935,
	LeanConstrainAnchoredPosition__ctor_mD89D09205A0E11B1469842015E759F108008C00F,
	LeanConstrainToParent_set_Horizontal_m57758513D51D6C76DB7848C7F421EDA54E653D64,
	LeanConstrainToParent_get_Horizontal_mBC8EC4AC96A75E60603846EAC0685BC57E4BA297,
	LeanConstrainToParent_set_Vertical_mB87D3895A36C50E74D76E8FF16B1E74F8D53862D,
	LeanConstrainToParent_get_Vertical_m3970837AB4550672A730CAE1C1CEBBC7C405586B,
	LeanConstrainToParent_OnEnable_mA4CE51E87E73608A62A2341F91B57C00305791D3,
	LeanConstrainToParent_LateUpdate_m1507F6FD0F0698C96E6EACA9B3945C067DC26035,
	LeanConstrainToParent__ctor_mCE1E59A53C030575BD3289F7F6357AADFAE52C56,
	LeanDrag_set_Target_mC00850FAA844FAC25937EDC3B4260DAB2E325A36,
	LeanDrag_get_Target_mBE95448A4C9A464D31BDDFF050C5857929BDBDE4,
	LeanDrag_set_Horizontal_mFAF231CBDF1858F1764F8B02E4EFCCF1514EC52E,
	LeanDrag_get_Horizontal_mCC7C7739C6370BAC4D51CE7A1869BE71B853BA68,
	LeanDrag_set_Vertical_m37A5938E8B820206F97AE40EEA1979E52EFA2662,
	LeanDrag_get_Vertical_m04B5A8EB0ED82C4C80F1DDB983E4EF886055CC45,
	LeanDrag_get_BeginTransitions_m99D0B79568C2687F56E796A8AE0C33930BA2D9C8,
	LeanDrag_get_EndTransitions_m24E4C95291787BDB104972B5E402B8B7E2DBB456,
	LeanDrag_get_OnBegin_m90FD0261E462CF3B9CFA9F4C1AD54B462C6D16EA,
	LeanDrag_get_OnEnd_m4464F9AAD3BA09E2692EC73DE94375DEC6793A44,
	LeanDrag_get_Dragging_mC14C1D74E81AAC231BC0C8B00EAA3D11A0F05635,
	LeanDrag_get_TargetTransform_mE8AC992B67B396EAD5DF3844E875C16C3852BDAE,
	LeanDrag_OnBeginDrag_mF1698DF448DD87021AE3A69E323D534006256B40,
	LeanDrag_OnDrag_mB70E5D990780435DB2CA07861E0E293AA0DA5844,
	LeanDrag_OnEndDrag_mCB4809A84FE628285C050A81CF6AA03F32177814,
	LeanDrag_Start_m6506D225ABBC20C6D69446BAA1E7C6173CFD7817,
	LeanDrag_OnEnable_m62AA2734C4393A61CE968BE9C26D8A12B0DAC909,
	LeanDrag_OnDisable_mF57CE3A7208A57AE17512468F6FED7D088503E3E,
	LeanDrag_DraggingCheck_m9852D23A9BFA83A825337B0914265551C6E1EDA1,
	LeanDrag_MayDrag_mBF7A864A793AE2AFF7F6D0E4AB3C07DFBCBC0518,
	LeanDrag__ctor_mF6C7412189947A073B4D011CFBFE3D428BD2840C,
	LeanGui_get_IsDragging_mEE2B17A390A5CD62273BCBEF7F1236517C888D13,
	LeanGui_RaycastGui_m333DFD08A73B8BBA1617DA656E757445EC3AE085,
	LeanGui_InvaidViewportPoint_m0E44D4F79AC23D9EDFE2CC441D2B4C93FA566E5E,
	LeanGui__cctor_mF746579453A85F92CA6F8925CD1DCF1CB5418B6D,
	LeanHitbox_set_Threshold_m4755F63D4F6EA82F537A4BF2636D040B485492E8,
	LeanHitbox_get_Threshold_mBB04198A8227F555B93E255274260CFCF945B8B2,
	LeanHitbox_get_CachedImage_m6441C8DAFB15691B705ABFAA4E80DD406D0857B3,
	LeanHitbox_UpdateThreshold_m53243EB5C271C7A284EB48AFE53B19C09AB0D99C,
	LeanHitbox_Start_m4284551223D3CBFB17822B257FF0279130D7EC2B,
	LeanHitbox__ctor_mAEC33AA898A8F5D89989251784C102B7C7521335,
	LeanHover_set_MultiEnter_mA2149192813E77BA54B0AEC1D92C8DA7604EAE14,
	LeanHover_get_MultiEnter_mF7C2693292745E7FD12898F433A0AE8CCAC29B3A,
	LeanHover_set_MultiExit_m351D724EB2AAA4E16322CF75714A4B2CE2987C33,
	LeanHover_get_MultiExit_m259265C8589CADC2A598F0720A9178EF74011EB5,
	LeanHover_get_EnterTransitions_m6138EF62DE7D7B91127E8435193393F9AFDA27D5,
	LeanHover_get_ExitTransitions_m43B20D269C0E2C136C5FC4EB92956DC881D2F8B8,
	LeanHover_get_OnEnter_m5CA209FF5497807F722E30043334107F94FC53DE,
	LeanHover_get_OnExit_m1B7ED0F63CEB95F8042A788485967D5B204A25FD,
	LeanHover_OnPointerEnter_m5AA7963B478736C8264B97BE982A3617F64E19F8,
	LeanHover_OnPointerExit_m3FDD77856F9081D4FBBF7432F8515AB383592039,
	LeanHover__ctor_m2A070491B3A59077049458F899543E6A5CD9F2EB,
	LeanJoystick_set_Shape_mA2A3DFD6D0A99B052824298E06106E9BC103EFC2,
	LeanJoystick_get_Shape_mF424DFD02627CD9D5400045CF7BA2808086D3189,
	LeanJoystick_set_Size_m600D6BB07E0BB46C29D3174C2EEB7C9FB246D520,
	LeanJoystick_get_Size_m3465D0A26DDE7EADBAB6E9094443451EFE0242C3,
	LeanJoystick_set_Radius_m29DEBCAA0B41AF6DBDD76B98B328B43222145704,
	LeanJoystick_get_Radius_mD5B09B28270BE925DF72BEA1544E5D4CBD6104A3,
	LeanJoystick_set_Handle_mCDDF26DEE13F80794940027CFD117ADDD5095C75,
	LeanJoystick_get_Handle_m4EE242C0053BFB4792655225A94BE54E89E52EC6,
	LeanJoystick_set_Damping_m4B85CA59FB8F70B464739FB48417C51C35F6B715,
	LeanJoystick_get_Damping_m34B2FBB9B4FDE3250A185DDBBD11FEDF13AD0C39,
	LeanJoystick_set_SnapWhileHeld_m46390CAF65321E9CB93F902D14E5216A246DC902,
	LeanJoystick_get_SnapWhileHeld_m548B60358931703E62CD140D220ECF1AA6292420,
	LeanJoystick_set_RelativeToOrigin_m5E9922F4C07A529F1BA502C61E375E5048DF1145,
	LeanJoystick_get_RelativeToOrigin_m41F5054E7A186191599970C3D4141AFC324E688B,
	LeanJoystick_set_RelativeRect_m138A59C9A91BA68C90A5714487D7ABF0264F5A6A,
	LeanJoystick_get_RelativeRect_m41E26464A04C45EA9670CDB9CB279016F90CF7BE,
	LeanJoystick_get_ScaledValue_mF788EDDE81F29D3BBCB8747C5C6713A8DBE2CEE2,
	LeanJoystick_get_DownTransitions_m552F95DBE24073702C1008BE5571478537D0CAEF,
	LeanJoystick_get_UpTransitions_mD46900B046DB466E2EF15994695B3DD4CAB85D6B,
	LeanJoystick_get_OnDown_mA2B5772C35027984D6F7A94D77A56B2AE0637BE3,
	LeanJoystick_get_OnSet_m187D9C7D3ECD841D9D4B17C4BFFB064E18754906,
	LeanJoystick_get_OnUp_mF6F6C4306CBDEEA411478811CA555406722E448D,
	LeanJoystick_get_CachedRectTransform_m835D0AFC2C8602E00D0C39302F31315523E326CE,
	LeanJoystick_Update_m69DBFF4FFC81F959347A1DB54676B01BAE369785,
	LeanJoystick_OnPointerDown_m21ED494AA248149500589AD35A1BD5663F12E18C,
	LeanJoystick_OnPointerUp_m4DE7D33B742901BF4A8A4A9247C7AE20C2B2EADF,
	LeanJoystick_NullPointerNow_mB9E053FCD8FF7FB22085CE531CC799621DC0AAF5,
	LeanJoystick__ctor_m7FAB6C3FD81E981524010D1E795FA6DA9D3AF057,
	LeanMoveToTop_set_Move_m64ED128FF817358F9C49C8ECB2D817C12B765AAD,
	LeanMoveToTop_get_Move_m9A5DEDE9906A19D1EB5E539E55C16CBED51EF301,
	LeanMoveToTop_set_Target_m389CCB4E17D82FD97328A4BA54A1DE2B4DBD3AEF,
	LeanMoveToTop_get_Target_mFBE8F64B64211ADE4230F65BDDC48D4DA5044A87,
	LeanMoveToTop_OnPointerDown_m940DB66AE47BF0209AFF741F469D45103D89CB58,
	LeanMoveToTop__ctor_mAE0DEA74FAD21C93BBBAB195149859CF64DE2227,
	LeanMoveToTop__cctor_m3D480D91196D27DA80155CCC7A3A8BEE2A25F19C,
	LeanOrientation_set_AnchoredPosition_mBF72280249DCF0FA74109D5CA50DC821E49164B3,
	LeanOrientation_get_AnchoredPosition_mD31FDBA9B5AED5F9F748D086895F1AC9B000ECEF,
	LeanOrientation_set_AnchoredPositionL_m0B2521B042D90C72B8B974DE5F305AA8DB180D2B,
	LeanOrientation_get_AnchoredPositionL_mD26B26796A0AB6E5F97EB848DF9CAFA351699461,
	LeanOrientation_set_AnchoredPositionP_m9C2B2182E206D9553D1C8FFDB386B8C455F2B90E,
	LeanOrientation_get_AnchoredPositionP_m1824F9C063D218E8D142C1B57464E61783D10064,
	LeanOrientation_set_SizeDelta_m25C4719D9EB7B5777921D1704744FF2B98BC594D,
	LeanOrientation_get_SizeDelta_m4B855B2850F5E7583098868A2F8F3E5B45FC761F,
	LeanOrientation_set_SizeDeltaL_m048F5E354D6C95C1A93D780F5CE691D00DEEDCEF,
	LeanOrientation_get_SizeDeltaL_m623145A673E9BA7E4FF4FF30AD01BB2747E2936C,
	LeanOrientation_set_SizeDeltaP_m043014EACF832D781B5B312C41C12050360AE55C,
	LeanOrientation_get_SizeDeltaP_m5FB614D382C5D8DBC7B74FF0A500C7AA9C39264A,
	LeanOrientation_set_AnchorMin_m936944BCA53F3E8223A4259B9B79DD5C9E0ECB4A,
	LeanOrientation_get_AnchorMin_mD920D658AEF0098AE0A2DBC171F876A5BB3CA958,
	LeanOrientation_set_AnchorMinL_m4519D0648B6127235228A6EDF1FF084625E0F875,
	LeanOrientation_get_AnchorMinL_mD33516A757311839CA3E37A89DA259E1703FB385,
	LeanOrientation_set_AnchorMinP_mA1F763531B9374D2085FC2D5F9C1EC5E7533F39A,
	LeanOrientation_get_AnchorMinP_m70D9817AB5AD110CB32BBDCB42506499F0808294,
	LeanOrientation_set_AnchorMax_mFBA662DF88FF6FB5228ECB15697B69978FE716B4,
	LeanOrientation_get_AnchorMax_m665B7CA01B040FD3B901C9C11D66868283C99D70,
	LeanOrientation_set_AnchorMaxL_mA530DD1DFDA80B18A926E1591F6D6CE3B797552A,
	LeanOrientation_get_AnchorMaxL_mF2C0EC7B72449FA46017256E4EA4C83A94AFDDE8,
	LeanOrientation_set_AnchorMaxP_m4122DCD1E35A9A7DD9708EE877539B4AE042BDF4,
	LeanOrientation_get_AnchorMaxP_mAE3481879C349F40E19E45572D07A35088E07F12,
	LeanOrientation_set_OffsetMin_m20173979405B7BF2929C0B33E8DAD4664F74E389,
	LeanOrientation_get_OffsetMin_m8A316B8EB235F14A465170FB7CDDE234790EA1E1,
	LeanOrientation_set_OffsetMinL_m42BA9FB9FF28D73172407D5049E1592FFE777A31,
	LeanOrientation_get_OffsetMinL_mB19E52B435445A6D9B29DA98009B6ED3371F2AE7,
	LeanOrientation_set_OffsetMinP_m5317B83211CA459CB6CCD9820A397292820438AF,
	LeanOrientation_get_OffsetMinP_m3A812B6A086BB7477D3E45EE4BB505D857ACC3D6,
	LeanOrientation_set_OffsetMax_m304CDB9931B5591FFD432B47D8F454E18C43CD18,
	LeanOrientation_get_OffsetMax_m15F19E534DF72CC5697C6533DAAF594EDEC6CE5C,
	LeanOrientation_set_OffsetMaxL_mA3280CC4FE3122AA93DC7C28E13736C0F3842F7E,
	LeanOrientation_get_OffsetMaxL_mB336811C1E5E42878E4DDE27B3285F046FDDB27E,
	LeanOrientation_set_OffsetMaxP_mC7A0534F2FBC854A36C8B4221C40AC6B41B4EB9A,
	LeanOrientation_get_OffsetMaxP_m61F7E366CDDA0CA9954E5C04D009D3A2A577E135,
	LeanOrientation_set_Pivot_m8CC8265C88AFC035A9FF4BBFFE75407A8C191F75,
	LeanOrientation_get_Pivot_m2026E5E25CB2618C0EDFD4628E6C60B4EF77A5E3,
	LeanOrientation_set_PivotL_m5DC333A7F60E35B49156958CC25A7BD30FF707DC,
	LeanOrientation_get_PivotL_m2F91674CA1C9F6EA6891EEFBFEFAA59EADE73839,
	LeanOrientation_set_PivotP_mB602D20F984268BAB456E1438B01D6D278570CAE,
	LeanOrientation_get_PivotP_m327DC9C33A272CF0B1CC8136BF7BF0D3C3CA5883,
	LeanOrientation_set_LocalRotation_m4B61283C635511352C082D264D818B36B71BFA9E,
	LeanOrientation_get_LocalRotation_m5D0F19573DDFE04416494FDD1A71F26E80E5B8FF,
	LeanOrientation_set_LocalRotationL_mC63049B96E5380ADAE4BFC3817C821A7CB3637D9,
	LeanOrientation_get_LocalRotationL_m233193D85ED057E5134B7964AE6274BA6CAA652A,
	LeanOrientation_set_LocalRotationP_m19856CEA46C07D28BC17A57E7BDB2394E8D405BB,
	LeanOrientation_get_LocalRotationP_mA87F453CF74007D49AD851D20B6BEEA65A0D80D9,
	LeanOrientation_set_LocalScale_mEB223AFB990C4E426D68A3BF01C22E9A81AC53CD,
	LeanOrientation_get_LocalScale_m0C87709A27F58397E96D934FFA82B6BB2B094CF8,
	LeanOrientation_set_LocalScaleL_m2DF2CB69956460BEA1091C7FED43277C6FCFDBBA,
	LeanOrientation_get_LocalScaleL_m592B86B8CA0049460C7C0EB60BEBE6CFFB8954D5,
	LeanOrientation_set_LocalScaleP_mA0B3B03531A2FA0387C5101B001D9C25DEF6132C,
	LeanOrientation_get_LocalScaleP_m153C4741A363E8E2E39D2C61E7051FE8C69F1D9F,
	LeanOrientation_get_LandscapeTransitions_m0DE2CFBFB4CF44C8C19C088E1BF4E827409F15F0,
	LeanOrientation_get_PortraitTransitions_m703E227FE766A6DEFC26254E53491E64F1FEA42E,
	LeanOrientation_get_OnLandscape_mD470123D8666FDD17260E9323AD39EBF9E53472B,
	LeanOrientation_get_OnPortrait_m7DD9D9361D05A1DB92C089793CF5EDA4510ED80D,
	LeanOrientation_CopyToLandscape_mDCE1FAFFC997411636EA132FC571FDB742F7DF10,
	LeanOrientation_CopyToPortrait_mA70B745547F2E6563D4E6780439BEA576F0C626D,
	LeanOrientation_UpdateOrientation_m8771B0A8B77E15A56634BCDAA973B6C71BED7C1D,
	LeanOrientation_Update_mD2F8E1948E1E9DC4B0009767FCE4DA1356ACE242,
	LeanOrientation_UpdateCachedRectTransform_m1CEFE711665CB88AD4A34108017D83D69A1A728E,
	LeanOrientation__ctor_m709A21C653592E4B37BBFD14211973D09B5E5A80,
	LeanPulse_set_RemainingPulses_mDA0D97B54B3A1288F88C6FDE8076B92620FC0D80,
	LeanPulse_get_RemainingPulses_mEC41D116CDE3C939AD910268BE0F52AAB72F2E73,
	LeanPulse_set_RemainingTime_mF8213DABCAE68B7AA3DCA08C59FD5D13142DA2E2,
	LeanPulse_get_RemainingTime_m9C223651B7E8DADEA0C0D7B4E6E1231DF65D305A,
	LeanPulse_set_TimeInterval_m6864792FDADDF52AB2735AA78D43797988B56AB7,
	LeanPulse_get_TimeInterval_m0C8311BA54351AB92F68E5BDC6DDE5EDDE6CF787,
	LeanPulse_set_Timing_m209541C687D3F09D26873CFA0AB9E06991B75044,
	LeanPulse_get_Timing_mAD3DD01394F4C332BB06B55D886D06474CADA66E,
	LeanPulse_get_PulseTransitions_mD9109EADBB4D6544DBB17EFC1E73524B1A051D09,
	LeanPulse_get_OnPulse_m969808E602D622B1B60472B38F7B261A76F4AA0E,
	LeanPulse_TryPulse_m8A51D4665DC573F87BA51310EEFDE4360DF72C59,
	LeanPulse_Pulse_mE0FAF9917833656844D01C13DD71B4706FD7E8F8,
	LeanPulse_TryPulseAll_m818DA5CB969E2845983CCDE2EDB983A048BB5A5F,
	LeanPulse_PulseAll_mA722EBF7664B67D48E2F581F7255B71A8861EE40,
	LeanPulse_OnEnable_mE7869351D21E6FD3A57C037610610F4F07CC2B94,
	LeanPulse_OnDisable_m20C1DC5C6ACA0126E2464214298C1AFCB43AD902,
	LeanPulse_Update_m4F4806B48CF230D95B122DA785C73BA076A46F42,
	LeanPulse_LateUpdate_mD5F19ADC0E4BA83FC7C7A2963E22E69B117100D4,
	LeanPulse_FixedUpdate_m4956C4508DAA4E18A70820C170DAF36F334A8803,
	LeanPulse_UpdatePulse_m7E286E7AB1A7CC260CE7D4BDC9859B167F698AC2,
	LeanPulse__ctor_m5E8D0480BC0DB7ABF464A73D45A0EFE731FE1FCB,
	LeanPulse__cctor_m46A41285366158E674A5FC5833779EB60C9C7AFF,
	LeanResize_set_Target_m37CF68AEC1600989896042D1F0973A55DE4B1842,
	LeanResize_get_Target_m3CD72140AA6E6014F91AF7BE8B9A2C78E391B7DE,
	LeanResize_set_Horizontal_m101DE68BE2E206DA1C4242077C205D0E7C5584F9,
	LeanResize_get_Horizontal_m983F402D16A8A2B4144D76EC69B4C441070F1514,
	LeanResize_set_HorizontalScale_m2818CCD6BF62FB337B8FF0D80B088FD810B7203E,
	LeanResize_get_HorizontalScale_mE7E6497F506D54D24A959EAA6083F84C25EE2477,
	LeanResize_set_HorizontalClamp_m822EEC8214CEE6D195F10B38858D0250616C0F84,
	LeanResize_get_HorizontalClamp_m7BDB58755A8A5DEDAFC2CB3E7AB356037FBB6345,
	LeanResize_set_HorizontalMin_mED36E4AAC8054501F18FF086FFB5D6FAC80E6A30,
	LeanResize_get_HorizontalMin_mE33EDB40CC7C1CB15801DD4A48F3CA385990AFCA,
	LeanResize_set_HorizontalMax_m43CA5032EFA0B1BFD3ACB9DA83D743C984C44B3A,
	LeanResize_get_HorizontalMax_m7423942849170BE9EF7B9A09D5D5ABCF76AEBD62,
	LeanResize_set_Vertical_m3D71F060BA268BCFE67BE8082620C45FAEACDD34,
	LeanResize_get_Vertical_m45552C0FFCD01EBAF1B2FCA065C7FBAE9C423FE6,
	LeanResize_set_VerticalScale_m899B64096A673C25A66785707B3599119D06AA8A,
	LeanResize_get_VerticalScale_mE78968FFF28DF52085309E6E2A9C8C7089466603,
	LeanResize_set_VerticalClamp_mDAC4229EAA714DD77FB406E38C5526B2CC8D40F4,
	LeanResize_get_VerticalClamp_m891034820C4E1E91F2BAC20B367585CE68CCB15E,
	LeanResize_set_VerticalMin_m3B7DA3B4AE432139736CC77BC4F67F7EEB6F742E,
	LeanResize_get_VerticalMin_mF5595892478CA81B4D1EA32515932CE4E28082FB,
	LeanResize_set_VerticalMax_m4D586C54DE1912FF0EC50DD5778D44AEF54C4D92,
	LeanResize_get_VerticalMax_mD63DB2E41C6D4F9504C1BBF349D95C580311F41C,
	LeanResize_get_BeginTransitions_m82D916161080A25ABB5223D87517FF06D690A8E6,
	LeanResize_get_EndTransitions_m0E4EC82D6F7A6DA15FEFB7E827ED64EB0391BD24,
	LeanResize_get_OnBegin_mB7FDB4033A3A8C9725391DD8B0BB5330A5840E00,
	LeanResize_get_OnEnd_mEF349A7898A5B478AC70945055BA5163919FAADB,
	LeanResize_get_TargetTransform_m3ED1D591A5EC18EC2215E2CF3FC08C996D8ABAD4,
	LeanResize_OnBeginDrag_m0163BE2C4A3C241812A60C4D58DE293870911637,
	LeanResize_OnDrag_m5E14A6D2D70D6BD4A2BF64BAABCA2FBE0049F535,
	LeanResize_OnEndDrag_mCCADE879766066C949231EE4A964220B498DDD01,
	LeanResize_Start_m2B09E124F35472D3FE4ED20536895FD5841148EF,
	LeanResize_OnEnable_m7B96F62F0D981620AD82F210E95FA09D1176941F,
	LeanResize_OnDisable_mE4A1A8C08E3964F1AFF1550EF6145EB38AD0C274,
	LeanResize_DraggingCheck_m59ED1E03C4A38006A79988280C4E28227B7EACE3,
	LeanResize_MayDrag_m68D871F80FCDD028A54E4F751DC6B3110385B0B3,
	LeanResize__ctor_m16B644780CF9BD5EE9D70AAFEF945EC3D28AEC1F,
	LeanSafeArea_set_Horizontal_mA9F8B558A36BC0E585359D27533C92F9A3ABE970,
	LeanSafeArea_get_Horizontal_m7AC10F138D598C315C6238ED7B1173E6785C0F8B,
	LeanSafeArea_set_HorizontalRange_m5599CF5D8428010282BAEF899A8EA1F17EB7D471,
	LeanSafeArea_get_HorizontalRange_m1D6125D3E4E46CAC4B7BAD5B4FD8B5D47F9D66F1,
	LeanSafeArea_set_Vertical_mB560F44D46447E451DB44EBD4F5261C9EA7FB6E7,
	LeanSafeArea_get_Vertical_mDD73839F2CC3492ECCE1BE4634DD35AB6B4C1E8F,
	LeanSafeArea_set_VerticalRange_m252C368DC0F4A24868493D3889A09948EB893B38,
	LeanSafeArea_get_VerticalRange_mDF5D7E438940918B2EFAF1D5118E6E903B56B42E,
	LeanSafeArea_UpdateSafeArea_mB68B4067F9B70E3F0A1027D8F1179323BAD44FF5,
	LeanSafeArea_Update_mE1D77F2157905DAB557EF0136E4F4F4DD270AF54,
	LeanSafeArea__ctor_m80FDE93F593ED87CD625008D01153F8F2A181DE0,
	LeanSelectable_set_interactable_m6D1CC6A7880100D578F2A93534AEAD345255CE40,
	LeanSelectable_get_interactable_m894583386D9BB52A1CB7EBD512D2478CD5111F6F,
	LeanSelectable_get_InteractableTransitions_m16790F3417F101DB7C8C95A5E63194D329477C6B,
	LeanSelectable_get_NonInteractableTransitions_m13CE7828B89D4B4F61A761A83F205CE2E7777F92,
	LeanSelectable_get_OnInteractable_mE61F46E49E6B2499EDE675E51835E38AE6585AFB,
	LeanSelectable_get_OnNonInteractable_m0D6E22E23D93D4E455813125193823FA415EEA7F,
	LeanSelectable_OnCanvasGroupChanged_m67AEDF3CBCF11C3863947FBC7D4D61E4630D8DF8,
	LeanSelectable_UpdateInteractable_m1A5F989E955971C61F3383DE455F419B05D84FB7,
	LeanSelectable__ctor_m290993EAD99C032A12080CAC34405380565EFAD7,
	LeanSelection_get_SelectTransitions_m956AA52A311138EBD7786EDFBABE4C2111266AAC,
	LeanSelection_get_DeselectTransitions_mA53C28E6741348A159502B598D895520BBDCEBE4,
	LeanSelection_Update_mDB92A416C59CAFAA239266C12B138B4A5737368E,
	LeanSelection__ctor_m628AFFF5633C47EEF1156B71709A121FCB4356FF,
	LeanSelectionHighlight_set_WorldCamera_mF3DAC7CFE5F9A38372AE54A3867274104F2FDC73,
	LeanSelectionHighlight_get_WorldCamera_mD0A41EFBE967F78EF633D95CFAC0E46E0D1DA9CC,
	LeanSelectionHighlight_get_ShowTransitions_m693E6BEE8817F1E6C890211B06D51E0298B4C94B,
	LeanSelectionHighlight_get_HideTransitions_m496951330663C63B9D0638C42D1948DAA71DF0F6,
	LeanSelectionHighlight_get_OnShow_m9A7EFCA3438A1F3C1F7860949039CE91CFF0B9F8,
	LeanSelectionHighlight_get_OnHide_m374415E7A4CEF5EC277B5C98ED360278D696B7C5,
	LeanSelectionHighlight_LateUpdate_m8C6F3E3E8834DD1693E2581CFD88BAE7C0569038,
	LeanSelectionHighlight_UpdateRect_mF51DB8EAFF6B9E87157B6245A3EB53A9FCE0F4EF,
	LeanSelectionHighlight_WorldToViewportPoint_m0054A8FE3150E2FA5CAC32FD33D8A6E40CDA167F,
	LeanSelectionHighlight__ctor_m006C3554CBB60AA561AA9B256126234D759FF792,
	LeanSelectionManager_set_ForceSelection_mFDB0980AF725DF77D7E72F125C6DF841795D6626,
	LeanSelectionManager_get_ForceSelection_mFC0374C0AADE67AD8016096886B55D7C9E865E72,
	LeanSelectionManager_get_SelectionHistory_mDEC760CDCC58CFBA86314EC7096A0C0772CCBB1A,
	LeanSelectionManager_CanSelect_m32D33DC066EA88D56E7B53A13BAC0B6A846F9B4C,
	LeanSelectionManager_SelectFirstSelectable_mE77857EFD5B3EC1EBA1157E3FDDE53DE8DD4E03C,
	LeanSelectionManager_LateUpdate_m8588AAD646EE92EAA67B77901DA5B457699007E8,
	LeanSelectionManager__ctor_mED2519A2D3BC237A177AE03CDFD4FF3706E6993D,
	LeanSelectionManager__cctor_mB57A87BC30D120E3083327A41C2017AA3F4DACF1,
	LeanSelectionPriority_set_Priority_mD3525BA8F4C649AFC7C68C24CA4D70580844BD01,
	LeanSelectionPriority_get_Priority_mAD0CF5F3BA87210C477FAC4A61BBC85DD5200E8C,
	LeanSelectionPriority__ctor_m81AD5E6C6D573966BC4551B90EDB23DF55EE3266,
	LeanShake_set_Speed_mAC52FAB8D55C72C0C04A417A0EC5A214F0A0B6F6,
	LeanShake_get_Speed_m8E285BB9408659052A55197570A4E6C530FA14CE,
	LeanShake_set_Strength_mEA6A3660983E5B5AB2D9C127472A27B7F7C8D017,
	LeanShake_get_Strength_mC3E0E29DF7254A2A7A102E74D18A88DDFF813310,
	LeanShake_set_Multiplier_m0B0C580E8E470EA45C5D79D15BDBBBF48FF2D655,
	LeanShake_get_Multiplier_m737F8B7AB20DE5AB7070625171720AA6AFF8E2F9,
	LeanShake_set_Damping_m40F7B8C31645911F9C0E404573D4EE9E5C5B612F,
	LeanShake_get_Damping_m44160858D789AF8C04E47DB553A3E2BBCAA6FB99,
	LeanShake_set_Reduction_m1E38D1399DB9BB994389981F9D92F908BB4ADC73,
	LeanShake_get_Reduction_m55D5DC214D560368C602B68DB80AC069481AD776,
	LeanShake_set_ShakePosition_m1507AF7B8A597B7F22A12872A158DE3D699795EE,
	LeanShake_get_ShakePosition_m1F96678D473ABAD25189062B52330E9846C4DC06,
	LeanShake_set_ShakeRotation_mDC29518FF2A751390DA365EF43A0999FA3D249F4,
	LeanShake_get_ShakeRotation_mF08B7607237BAC5ABF2FAE9FB1469563FCE4CFE0,
	LeanShake_Shake_mD771B10F37457F38F38ED9E63C288B6ADDE06C00,
	LeanShake_Start_mC92BFB85D9963688D58F145400F875E7251C39A7,
	LeanShake_Update_m7D50EED9A60A351E0896C2982FE3C9F49CD74AAF,
	LeanShake_Sample_m71C335DED563750C7B1C39E4E732CE08D43DF26E,
	LeanShake__ctor_m60C2F8CB24B98B1DF5449D76C786200CBEE05F08,
	LeanSizer_set_Target_m24428E4E0454E19F3C8AB481B403FF09BD1BD994,
	LeanSizer_get_Target_m6CD6E55A24DB93CDC278CDDEB08746FADDBE7B9F,
	LeanSizer_set_Scale_m4085F8918F5AA9195C3AC0B05E053663DB5E31ED,
	LeanSizer_get_Scale_m7776D8BD56834D164853E5E46014AB895B3EA373,
	LeanSizer_set_Horizontal_mC969AACE37166E014E0CDF62162D94237FC1AE75,
	LeanSizer_get_Horizontal_mDA1B781AC14F0F8E1CCF606CFDF1E122749209D2,
	LeanSizer_set_HorizontalPadding_m535C45EFCDDD361B686E77EBEDA62E2DE2E00275,
	LeanSizer_get_HorizontalPadding_mE1C22DDBC775F953BB319302E9C2864142B34AE0,
	LeanSizer_set_Vertical_m74EF0335F7AEB2DE1538290239F6C7B0049017BB,
	LeanSizer_get_Vertical_m6199C83420B725F0E00B2032B22EA4ED9141EB15,
	LeanSizer_set_VerticalPadding_m0269DAEC58E33328BE89681A13D2A355EC9A9727,
	LeanSizer_get_VerticalPadding_m7C8BBC8F5F5702E112E244BA482E0B66A3BE08DF,
	LeanSizer_UpdateSize_m8548DB7D5C69CA9222BB421B6AD125CB62CB0646,
	LeanSizer_OnRectTransformDimensionsChange_m24DEAAAB595DF1AE4A9F17682AE381987602B145,
	LeanSizer__ctor_m15319E43A6EA8AD5A9F693511C0E933072842690,
	LeanSnap_set_Horizontal_m1612E05431C3D6B01DB08B0381838C23A022051A,
	LeanSnap_get_Horizontal_m3386236ECA1E693E784E4014D338901B8CE0E484,
	LeanSnap_set_HorizontalOffset_m1C559686DDE99D445CFD511699C003CFD5923311,
	LeanSnap_get_HorizontalOffset_m236ADAD7F6F5B0BCD06031D650B19A011DE82985,
	LeanSnap_set_HorizontalIntervalPixel_mB1D54760CB2C7589465DA6E5FC163761A368B221,
	LeanSnap_get_HorizontalIntervalPixel_mB6C0AD01603BBF74B64CC72992FAD1341B3A1F90,
	LeanSnap_set_HorizontalIntervalRect_m93319A8B221350106AF9EBF9CCD33BD1CBE906CC,
	LeanSnap_get_HorizontalIntervalRect_m7B3128A68FBE9C3AECF576C33F5A75544D311DDB,
	LeanSnap_set_HorizontalIntervalParent_m5AC87A8146810D01B0CEB0A5E85951A2B74580D2,
	LeanSnap_get_HorizontalIntervalParent_m12EBFC281462BC6CBDAE76E806D26F080877C9E8,
	LeanSnap_set_HorizontalSpeed_mD3CA12169A79349DC6544DBC14651BC7DF4A5EE4,
	LeanSnap_get_HorizontalSpeed_mA7E030B13DF4B9F1A02BCBB6A3DF1CA04F3C23D5,
	LeanSnap_set_Vertical_mA1E48CC9AADEA2EF89D9DAB4522ACB68229B17A5,
	LeanSnap_get_Vertical_m745909C580D523F7265ABDE7B0356E6B3707D451,
	LeanSnap_set_VerticalOffset_m3D88D676B26A3133F73F8D522316FC8F60D53E26,
	LeanSnap_get_VerticalOffset_mDB1021780B210103F47B6DB82BC262CE8CBEC361,
	LeanSnap_set_VerticalIntervalPixel_m0912D8B615CDCA1A18A8313E6EAF2B581F910A6B,
	LeanSnap_get_VerticalIntervalPixel_m73EBAF2E2169FEC447C16590D106BDC5865A1C66,
	LeanSnap_set_VerticalIntervalRect_mB3C2003164BA4BA889586B8DF7803A40A8A2EDA4,
	LeanSnap_get_VerticalIntervalRect_mEB6DA53625BF1C65E6E9F8D58EDCE74AE1D1FAF3,
	LeanSnap_set_VerticalIntervalParent_m333D3338C4BD15308BED916D8754146369DB3ADE,
	LeanSnap_get_VerticalIntervalParent_m8B0DF6518343E56D149B82E8580F6CE8A4EF2A2E,
	LeanSnap_set_VerticalSpeed_m0C2679C96D5D71D72781A14D70583703EAC5C4EA,
	LeanSnap_get_VerticalSpeed_mFA112173A02DD18542AC4E5132780E2FB9427FA2,
	LeanSnap_set_DisableWith_mF544A4B4A8B44E35488139947913C4E30E169D70,
	LeanSnap_get_DisableWith_m0250D7CA3B2FC28E402A4B3969949030FCCCC44B,
	LeanSnap_get_Position_mEEAAC70FBF2B694AA9FA1531A13D4B7EC6069309,
	LeanSnap_get_OnPositionChanged_m4467EACA276C8C96EC2D2DED3C32B321FFDF5E45,
	LeanSnap_OnEnable_mDDD4639BE0FED1539084E757363B4FFF8E59B67B,
	LeanSnap_LateUpdate_m4A8F1B3A3E2F31DF8D430395BA9A39891BA9D20B,
	LeanSnap_get_ParentSize_m249AE75A44E04D0A879F6675EB5A92B305D293C6,
	LeanSnap__ctor_m4AB8A8392D65251F612171537630B859EA540678,
	LeanSnapEvent_set_Position_m20103BEBA12A6BDF5B9F7FE608FFD38F3447221C,
	LeanSnapEvent_get_Position_m728D59E3FD0CA120425396B57088E3CF35FFEC45,
	LeanSnapEvent_get_OnAction_mBEB7B1FD51962DD2E972C0E58FEA70BDD7C19D21,
	LeanSnapEvent__ctor_mE782D9F238EBAE8C2967E59FF5B5E2254FF17ABA,
	LeanSwipe_set_MinimumDistance_m3E6D0D1F7DE31A4168A4F5D5C024782C68C283C0,
	LeanSwipe_get_MinimumDistance_m4A42497392E411E4A4EF81D0573A5942D943C970,
	LeanSwipe_set_MaximumTime_mC5429CADAB6691EEC1A00A2D064C90AD61D174F4,
	LeanSwipe_get_MaximumTime_m07448AAB68FE127FE9BA68412E81B47B1ACE91ED,
	LeanSwipe_set_CheckAngle_m0140E24E5BE831646402A97B69D324D3331B36E7,
	LeanSwipe_get_CheckAngle_mC9DF5C4A9D3B70DD003B3D4267BF73F2338FD870,
	LeanSwipe_set_DesiredAngle_m8E0571DC50992F81AA99058FBA962C278D031666,
	LeanSwipe_get_DesiredAngle_mF46B743C71C0D0F05605B3F2CB0090D17F021D29,
	LeanSwipe_set_MaximumRange_m5D78653C6BDA538C6027949F0C72AA451B99C372,
	LeanSwipe_get_MaximumRange_m65887249DB16B6F26E4A38B10FC5B8E1133B77DD,
	LeanSwipe_get_SwipeTransitions_m52852A33053CC410FE126D09C6B5CB3CFC03EA54,
	LeanSwipe_get_OnSwipe_m61B73E36D0044B1B5F66FFA042A2E14D4FE0C178,
	LeanSwipe_get_CachedRectTransform_m6CCCB992CEF297167383F424F19807DF3A42AC96,
	LeanSwipe_OnBeginDrag_mFA845AA39EC67879AEFE0DAA133C1FC87828DAC3,
	LeanSwipe_OnDrag_m25B56910AB0A65B10525AADE25C1A1DFBF8CD123,
	LeanSwipe_OnEndDrag_m0C119C24918023E5EACC83263EA537C593C9E636,
	LeanSwipe_Update_m35BF6547E1226E36D79B4375A7BE2243991DA518,
	LeanSwipe__ctor_m7877E1537093B28A3D6CE66FC03643FD3BFA8621,
	LeanSwitch_set_State_mD1A1BD92D5CE9913AAC8F23D38F266ED6E62BC4E,
	LeanSwitch_get_State_m35E7587B2AC53E5637D15808BC00D9DDA65A4756,
	LeanSwitch_get_States_m1A4899F92AF673D602736ACAFF700D05416FB801,
	LeanSwitch_get_ChangedStateTransitions_mE9EE889D6AECCDA3565795E71C5FBEDF5BFCE3E9,
	LeanSwitch_get_OnChangedState_mEDE3567593994165A6E01A443885A0F9B6DD4921,
	LeanSwitch_Switch_m31E363EF53525993AC595F451936EC189E5216B8,
	LeanSwitch_SwitchAll_mD69ED936744605AB38D0D3BC8B26CA08B1438993,
	LeanSwitch_OnEnable_m96DFA14D8006CA53D994716E8C7DD9AC4F5772A7,
	LeanSwitch_OnDisable_mD20C37155D2292EC974C5D1E482FF894E112EC42,
	LeanSwitch__ctor_m6E3F1862418E9CDF75BC23DE2686EF200DC1B81A,
	LeanSwitch__cctor_mAE4E1BF0784C5804BA4FBEAD42910E297747A784,
	LeanToggle_set_On_mFE64A96EF5015D8458357CB9F6B016030748987E,
	LeanToggle_get_On_mEDB8C73D0A187FF10BD250792E8E9799069065AD,
	LeanToggle_set_TurnOffSiblings_mDE31F8EE04CC6106E8D6CA31B83A1720F513F1D3,
	LeanToggle_get_TurnOffSiblings_m7073CEE6576375F37B00700479138FAF2CFC7B18,
	LeanToggle_get_OnTransitions_m63AB565AF90BDB8D2C1C54460764212687CBC3E2,
	LeanToggle_get_OffTransitions_mBB0DB0A9AE04FAA33A56026B1540E2E7A7BF6CF2,
	LeanToggle_get_OnOn_m2D3FD3D3CA8D253E6A7D94BD52C5A4EC7E0F0C1F,
	LeanToggle_get_OnOff_m19295E35EA64AD58F124A99E118731F819354F30,
	LeanToggle_Set_m079B02A4C923E6A1E7E8A9CC7630E5263DC5434F,
	LeanToggle_Toggle_m2624FBAF0DCA08674D336E7DF00D8BDDA53E91D2,
	LeanToggle_TurnOn_m633A8F4B9607EAA607278681830E3D762D57F53E,
	LeanToggle_TurnOff_mBE8CA67BF290EAE616F9947073A1AB05165A9183,
	LeanToggle_TurnOffSiblingsNow_m61F532DFD5C3AC6FF7442DDCB362ABFA2B9C9F4C,
	LeanToggle_TurnOffOthersNow_m1A31C50170E718E8B466226B90E8B6DF66C3076F,
	LeanToggle_AllOn_m60CA3A3AF9F232F47A9AA3DD15147E6B6CE933FC,
	LeanToggle_AllOff_m6F6B5E129F6208860830D675C3F565E6E0196175,
	LeanToggle_SetAll_mB2C6506EF8E5A7D5A7C154ECC30D21EC3B21E239,
	LeanToggle_ToggleAll_m818318E289B5A4FB045E8326D7602A65B35CFE96,
	LeanToggle_TurnOnAll_m33B71D9012B6A7A27D112B4A78B5681334F0FBDE,
	LeanToggle_TurnOffAll_m5D1391C5C285ED90F520DCD27C28F005902954AE,
	LeanToggle_TurnOnNow_m9544DEA88291706FC2A6B2989BED7E1450A35AB3,
	LeanToggle_TurnOffNow_m01B0806241C4A76B1DCB568D965406C31D97698E,
	LeanToggle_OnEnable_mB71E2D18EF0F7C3A1064721F6BEDFBEE1EDC576C,
	LeanToggle_OnDisable_m3F1393116026CD390475807456023AC058432E4E,
	LeanToggle__ctor_mE2497ACC87D76C668F0FFE2015E9ACFC3A86B5C8,
	LeanToggle__cctor_m95F73A98394D12E392CE774460F54DB4530BD072,
	LeanTooltip_set_Activation_mFA21C11F86BE0B019CC23F811A1D3C647F941F18,
	LeanTooltip_get_Activation_mBACA925B51A492AA4F1D29C19840824F2412713D,
	LeanTooltip_set_ShowDelay_m2A854DE4D56EC7CDAAB5216D1DEB28BB9263B0FE,
	LeanTooltip_get_ShowDelay_m9A62E40F0B45E3D8A51D8A50E4D4BE01EA9D54F7,
	LeanTooltip_set_Move_mD8D1453EBC1199D3276E05B2D2D46ABEDE373EBB,
	LeanTooltip_get_Move_mFDB3497A80C13ECF50C7AF378425747EF0CF8528,
	LeanTooltip_set_Boundary_mD0F804E89207D1C6DB4D20BA50BB9A01B516162E,
	LeanTooltip_get_Boundary_m009797289FDFA6B015E2F2E72E2C06BF9066CADC,
	LeanTooltip_get_ShowTransitions_mA376B0AD5DF9181763250DBF482801010EFAECEF,
	LeanTooltip_get_HideTransitions_mB9C28E5D2751AC320D6602848E99A584B8100910,
	LeanTooltip_get_OnShow_mC7CF2F479342006D2DC2EB92696100EACD764910,
	LeanTooltip_get_OnHide_mED0228775499F97EE35EA1A1F3E80C30F1A4A00D,
	LeanTooltip_Update_mD79E9F3B7C56A203B36D0A51F0251C9A034829A2,
	LeanTooltip_Show_m6597DA564F2641D1BD3A4D13D4FAAC431D29A406,
	LeanTooltip_Hide_m96DAE44ABA9A70FBD87AD4F898C16462441BB71E,
	LeanTooltip__ctor_mF6199DDA8BDB89FE64E18C85943E66F99BB087FF,
	LeanTooltip__cctor_m1DBAB3A5D65944AE5D83AE5FBDB51B1C2A182F2D,
	LeanTooltipData_set_Selectable_m128AAEE7910C7D612ACA08D49A312C411C73DC5D,
	LeanTooltipData_get_Selectable_m2E957AFC77CBB11E3D8BFEB0B28580C3A86195F9,
	LeanTooltipData_set_Text_m0FE8D94A6FF527725321F59984282602FA1FE2D4,
	LeanTooltipData_get_Text_m5F1588BF423A2C0FAE956FC850D04F8DDEF877F7,
	LeanTooltipData_Update_mF86765962FD24589E65E27BB2BB1DB7B5ABDF0EA,
	LeanTooltipData_OnPointerEnter_mB2F7D5ADFA1D3473A55E0CB90E0EB1174DFCF5EA,
	LeanTooltipData_OnPointerExit_m7BE6537264DCD78E9AA857B1451032A47B700C16,
	LeanTooltipData_OnPointerDown_m9500FCD6458E97B8DCA5CA20778843EFE0990231,
	LeanTooltipData_OnPointerUp_m90FE7A544F5B93CB142EA27DE049CDD31C7EA51D,
	LeanTooltipData__ctor_mB78F66093C2C9DE09FD7E440EE927534F02C6552,
	LeanWindow_TurnOnNow_mC3C318F9F6F7EDF4345799E2E0A7382B4C1E7355,
	LeanWindow__ctor_m402C38BCBF9C2696BFA82AA3020F08EC5D60850A,
	LeanWindowCloser_set_CloseKey_m08785222B351290AE923A91D7151908D6A2F6CFA,
	LeanWindowCloser_get_CloseKey_m131E1C8734FBE95BF6FD042D7812BCCF4A5A043F,
	LeanWindowCloser_set_EmptyWindow_m3615BEE0DAEDAD02F75F4BF97F90F7646794F1B6,
	LeanWindowCloser_get_EmptyWindow_m2396DD6345A12984BB328CF8624743FF6F1CE2CB,
	LeanWindowCloser_get_WindowOrder_m6A789ECC88A2EAC873506A4947648D220990E50F,
	LeanWindowCloser_Register_m18DF24AC193EA02DCFDB8F13BF580B573819E42D,
	LeanWindowCloser_CloseAll_m660AA5F682F2E35935CE86716DF03AB1EFE9EEB3,
	LeanWindowCloser_CloseTopMost_m13990636891BA9907E6DA03C4D53F88F290EB601,
	LeanWindowCloser_OnEnable_m1F1D62B9E6D38FE37933D4F02A7D8FBD5781D7DE,
	LeanWindowCloser_OnDisable_mADD098805D07A16913FFD5BDFE1CAD8236FF1E7C,
	LeanWindowCloser_Update_m1ABF60BE1D3B91A6935F159FA4BA924FD489EFFF,
	LeanWindowCloser_RegisterNow_mF30AE11FE4D7CD5680FE98970EA1647C6FBA2889,
	LeanWindowCloser__ctor_mB6C5164E32BA14845A42297D76EF067F3DE7D7BA,
	LeanWindowCloser__cctor_m4BCD98259579A2DD6D73566EB48A61D49AAC142C,
	DraggingDelegate__ctor_m3CCA0D325CE92FB84EA7CB04F870A1F7A982FBAF,
	DraggingDelegate_Invoke_m4A3D9E8E2DCF729A985FB9001DA6A1E36DF4EA8D,
	DraggingDelegate_BeginInvoke_m99240362B3A6C70C605CC6DE6E13A8BE4402DCCE,
	DraggingDelegate_EndInvoke_m8CC7C74C4975D39BBCA39650E208C18433675C6F,
	Vector2Event__ctor_m5A44A356A94CC25A973E1AA5188430862644C02F,
	Vector2IntEvent__ctor_m8A2A3486B1A5719E1D957BE6BF008A86FEE5946F,
	IntUnityEvent__ctor_mF83077FDA40BD8447B6CB8624D6C50B5988CE338,
	UnityEventString__ctor_m04212A3A3F82FE3A43D33DE626360A5E082F34A0,
};
static const int32_t s_InvokerIndices[480] = 
{
	31,
	89,
	337,
	731,
	14,
	14,
	14,
	14,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	31,
	89,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	31,
	89,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	14,
	1441,
	1441,
	1641,
	23,
	23,
	31,
	89,
	31,
	89,
	23,
	23,
	23,
	26,
	14,
	31,
	89,
	31,
	89,
	14,
	14,
	14,
	14,
	89,
	14,
	26,
	26,
	26,
	23,
	23,
	23,
	6,
	9,
	23,
	49,
	2248,
	2249,
	3,
	337,
	731,
	14,
	23,
	23,
	23,
	31,
	89,
	31,
	89,
	14,
	14,
	14,
	14,
	26,
	26,
	23,
	32,
	10,
	1442,
	1441,
	337,
	731,
	26,
	14,
	337,
	731,
	31,
	89,
	31,
	89,
	26,
	14,
	1441,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	26,
	26,
	23,
	23,
	32,
	10,
	26,
	14,
	26,
	23,
	3,
	31,
	89,
	1442,
	1441,
	1442,
	1441,
	31,
	89,
	1442,
	1441,
	1442,
	1441,
	31,
	89,
	1442,
	1441,
	1442,
	1441,
	31,
	89,
	1442,
	1441,
	1442,
	1441,
	31,
	89,
	1442,
	1441,
	1442,
	1441,
	31,
	89,
	1442,
	1441,
	1442,
	1441,
	31,
	89,
	1442,
	1441,
	1442,
	1441,
	31,
	89,
	1636,
	1635,
	1636,
	1635,
	31,
	89,
	1434,
	1433,
	1434,
	1433,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	10,
	337,
	731,
	337,
	731,
	32,
	10,
	14,
	14,
	23,
	23,
	154,
	154,
	23,
	23,
	23,
	23,
	23,
	337,
	23,
	3,
	26,
	14,
	31,
	89,
	337,
	731,
	31,
	89,
	337,
	731,
	337,
	731,
	31,
	89,
	337,
	731,
	31,
	89,
	337,
	731,
	337,
	731,
	14,
	14,
	14,
	14,
	14,
	26,
	26,
	26,
	23,
	23,
	23,
	6,
	9,
	23,
	31,
	89,
	1442,
	1441,
	31,
	89,
	1442,
	1441,
	23,
	23,
	23,
	31,
	89,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	26,
	14,
	14,
	14,
	14,
	14,
	23,
	9,
	2250,
	23,
	31,
	89,
	14,
	9,
	23,
	23,
	23,
	3,
	337,
	731,
	23,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	1434,
	1433,
	1434,
	1433,
	337,
	23,
	23,
	2251,
	23,
	26,
	14,
	31,
	89,
	31,
	89,
	337,
	731,
	31,
	89,
	337,
	731,
	23,
	23,
	23,
	31,
	89,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	31,
	89,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	26,
	14,
	1986,
	14,
	23,
	23,
	1441,
	23,
	1987,
	1986,
	14,
	23,
	337,
	731,
	337,
	731,
	31,
	89,
	337,
	731,
	337,
	731,
	14,
	14,
	14,
	26,
	26,
	26,
	23,
	23,
	32,
	10,
	14,
	14,
	14,
	32,
	373,
	23,
	23,
	23,
	3,
	31,
	89,
	31,
	89,
	14,
	14,
	14,
	14,
	31,
	23,
	23,
	23,
	23,
	23,
	114,
	114,
	625,
	154,
	154,
	154,
	23,
	23,
	23,
	23,
	23,
	3,
	32,
	10,
	337,
	731,
	31,
	89,
	32,
	10,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	23,
	3,
	26,
	14,
	26,
	14,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	32,
	10,
	26,
	14,
	14,
	154,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	3,
	124,
	6,
	1180,
	358,
	23,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_LeanGUICodeGenModule;
const Il2CppCodeGenModule g_LeanGUICodeGenModule = 
{
	"LeanGUI.dll",
	480,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
