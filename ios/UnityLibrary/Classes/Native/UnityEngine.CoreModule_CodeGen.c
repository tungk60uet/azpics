﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngineInternal.MathfInternal::.cctor()
extern void MathfInternal__cctor_m885D4921B8E928763E7ABB4466659665780F860F (void);
// 0x00000002 System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern void TypeInferenceRuleAttribute__ctor_m389751AED6740F401AC8DFACD5914C13AB24D8A6 (void);
// 0x00000003 System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern void TypeInferenceRuleAttribute__ctor_m34920F979AA071F4973CEEEF6F91B5B6A53E5765 (void);
// 0x00000004 System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern void TypeInferenceRuleAttribute_ToString_m49343B52ED0F3E75B3E56E37CF523F63E5A746F6 (void);
// 0x00000005 System.Void UnityEngineInternal.GenericStack::.ctor()
extern void GenericStack__ctor_m0659B84DB6B093AF1F01F566686C510DDEEAE848 (void);
// 0x00000006 System.Void Unity.Profiling.ProfilerMarker::.ctor(System.String)
extern void ProfilerMarker__ctor_mF9F9BDCB1E4618F9533D83D47EAD7325A32FDC2A (void);
// 0x00000007 System.IntPtr Unity.Profiling.ProfilerMarker::Internal_Create(System.String,System.UInt16)
extern void ProfilerMarker_Internal_Create_m92F2A7651D4BF3F3D0CB62078DD79B71839FA370 (void);
// 0x00000008 System.Void Unity.Jobs.JobHandle::ScheduleBatchedJobs()
extern void JobHandle_ScheduleBatchedJobs_mE52469B0B3D765B57BC658E82815840C83A6A4D0 (void);
// 0x00000009 System.Void Unity.Collections.NativeLeakDetection::Initialize()
extern void NativeLeakDetection_Initialize_m70E48965BE4B399698C8034015B4F0EBD8D4C6E7 (void);
// 0x0000000A System.Int32 Unity.Collections.NativeArray`1::get_Length()
// 0x0000000B T Unity.Collections.NativeArray`1::get_Item(System.Int32)
// 0x0000000C System.Void Unity.Collections.NativeArray`1::set_Item(System.Int32,T)
// 0x0000000D System.Boolean Unity.Collections.NativeArray`1::get_IsCreated()
// 0x0000000E System.Void Unity.Collections.NativeArray`1::Deallocate()
// 0x0000000F System.Void Unity.Collections.NativeArray`1::Dispose()
// 0x00000010 Unity.Collections.NativeArray`1/Enumerator<T> Unity.Collections.NativeArray`1::GetEnumerator()
// 0x00000011 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.NativeArray`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000012 System.Collections.IEnumerator Unity.Collections.NativeArray`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000013 System.Boolean Unity.Collections.NativeArray`1::Equals(Unity.Collections.NativeArray`1<T>)
// 0x00000014 System.Boolean Unity.Collections.NativeArray`1::Equals(System.Object)
// 0x00000015 System.Int32 Unity.Collections.NativeArray`1::GetHashCode()
// 0x00000016 System.Void Unity.Collections.NativeArray`1/Enumerator::.ctor(Unity.Collections.NativeArray`1<T>&)
// 0x00000017 System.Void Unity.Collections.NativeArray`1/Enumerator::Dispose()
// 0x00000018 System.Boolean Unity.Collections.NativeArray`1/Enumerator::MoveNext()
// 0x00000019 System.Void Unity.Collections.NativeArray`1/Enumerator::Reset()
// 0x0000001A T Unity.Collections.NativeArray`1/Enumerator::get_Current()
// 0x0000001B System.Object Unity.Collections.NativeArray`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000001C System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerAttribute::.ctor()
extern void NativeContainerAttribute__ctor_mD22697FA575BA0404B981921B295C1A4B89C9F42 (void);
// 0x0000001D System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerSupportsMinMaxWriteRestrictionAttribute::.ctor()
extern void NativeContainerSupportsMinMaxWriteRestrictionAttribute__ctor_m3D87D41F66CB34605B2C23D12BD04E9546AF321D (void);
// 0x0000001E System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerSupportsDeallocateOnJobCompletionAttribute::.ctor()
extern void NativeContainerSupportsDeallocateOnJobCompletionAttribute__ctor_m56D5D2E8D7FE0BF8368167A7139204F4740A875B (void);
// 0x0000001F System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerSupportsDeferredConvertListToArray::.ctor()
extern void NativeContainerSupportsDeferredConvertListToArray__ctor_m81D3D40F97FDB1675D128ACD785A9658E5E9DBB2 (void);
// 0x00000020 System.Void Unity.Collections.LowLevel.Unsafe.WriteAccessRequiredAttribute::.ctor()
extern void WriteAccessRequiredAttribute__ctor_mBB72625FD2C0CE5081BCEBF5C6122581723574B5 (void);
// 0x00000021 System.Void Unity.Collections.LowLevel.Unsafe.NativeDisableUnsafePtrRestrictionAttribute::.ctor()
extern void NativeDisableUnsafePtrRestrictionAttribute__ctor_m25F3A64C3715BB3C92C7150DB1F46BC88091B653 (void);
// 0x00000022 Unity.Collections.NativeArray`1<T> Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::ConvertExistingDataToNativeArray(System.Void*,System.Int32,Unity.Collections.Allocator)
// 0x00000023 System.Void* Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::GetUnsafeReadOnlyPtr(Unity.Collections.NativeArray`1<T>)
// 0x00000024 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::Free(System.Void*,Unity.Collections.Allocator)
extern void UnsafeUtility_Free_mAC082BB03B10D20CA9E5AD7FBA33164DF2B52E89 (void);
// 0x00000025 T Unity.Collections.LowLevel.Unsafe.UnsafeUtility::ReadArrayElement(System.Void*,System.Int32)
// 0x00000026 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement(System.Void*,System.Int32,T)
// 0x00000027 System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeUtility::SizeOf()
// 0x00000028 System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor()
extern void MeansImplicitUseAttribute__ctor_m2AA2C4CF491D9E594351112A4606017D4A7EF0E7 (void);
// 0x00000029 System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor(JetBrains.Annotations.ImplicitUseKindFlags,JetBrains.Annotations.ImplicitUseTargetFlags)
extern void MeansImplicitUseAttribute__ctor_m94FE505EEBB6820677CC4146576D44B2D2684E54 (void);
// 0x0000002A System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
extern void SortingLayer_GetLayerValueFromID_m564F9C83200E5EC3E9578A75854CB943CE5546F8 (void);
// 0x0000002B System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
extern void Keyframe__ctor_m0EA9CF8E65F32EE7603302E2CC670C56DC177C13 (void);
// 0x0000002C System.Single UnityEngine.Keyframe::get_time()
extern void Keyframe_get_time_m5A49381A903E63DD63EF8A381BA26C1A2DEF935D (void);
// 0x0000002D System.Void UnityEngine.Keyframe::set_time(System.Single)
extern void Keyframe_set_time_mAD4CA2282CD1B7C1D330C3EE75F79AD636C7FC83 (void);
// 0x0000002E System.Single UnityEngine.Keyframe::get_value()
extern void Keyframe_get_value_m0DD3FAD00F43E7018FECBD011B04310E25C590FD (void);
// 0x0000002F System.Void UnityEngine.Keyframe::set_value(System.Single)
extern void Keyframe_set_value_m578277371F57893D5FF121710CF016A52302652E (void);
// 0x00000030 System.Single UnityEngine.Keyframe::get_inTangent()
extern void Keyframe_get_inTangent_mE19351A6720331D0D01C299ED6480780B20CB8E0 (void);
// 0x00000031 System.Void UnityEngine.Keyframe::set_inTangent(System.Single)
extern void Keyframe_set_inTangent_mAA19EC59A3F6D360310D2F6B765359A069F8058E (void);
// 0x00000032 System.Single UnityEngine.Keyframe::get_outTangent()
extern void Keyframe_get_outTangent_mF1B82FA2B7E060906B77CEAF229516D69789BEAE (void);
// 0x00000033 System.Void UnityEngine.Keyframe::set_outTangent(System.Single)
extern void Keyframe_set_outTangent_mDEEA8549F31FA0AAC090B2D6D7E15C42D3CAE90B (void);
// 0x00000034 UnityEngine.WeightedMode UnityEngine.Keyframe::get_weightedMode()
extern void Keyframe_get_weightedMode_mFF562D4B06BCF6A65135C337D6F10BF1C4F3E14D (void);
// 0x00000035 System.Void UnityEngine.Keyframe::set_weightedMode(UnityEngine.WeightedMode)
extern void Keyframe_set_weightedMode_m95923B248E6E9947136A115AF240F8C3A3CBC399 (void);
// 0x00000036 System.Int32 UnityEngine.Keyframe::get_tangentMode()
extern void Keyframe_get_tangentMode_mBF872460D0D000E593C06723BB8FCDDD48983D77 (void);
// 0x00000037 System.Void UnityEngine.Keyframe::set_tangentMode(System.Int32)
extern void Keyframe_set_tangentMode_mC8D2E68FDEF4D5C9083FAF35E6EACE989318A68C (void);
// 0x00000038 System.Int32 UnityEngine.Keyframe::get_tangentModeInternal()
extern void Keyframe_get_tangentModeInternal_mCA542C1309EAB57FC634AFB50D31721FF82FB631 (void);
// 0x00000039 System.Void UnityEngine.Keyframe::set_tangentModeInternal(System.Int32)
extern void Keyframe_set_tangentModeInternal_mFF714C3EB51D2A08F6A375728C0280D124749AC2 (void);
// 0x0000003A System.Void UnityEngine.AnimationCurve::Internal_Destroy(System.IntPtr)
extern void AnimationCurve_Internal_Destroy_m295BAECEF97D64ACFE55D7EA91B9E9C077DB6A7C (void);
// 0x0000003B System.IntPtr UnityEngine.AnimationCurve::Internal_Create(UnityEngine.Keyframe[])
extern void AnimationCurve_Internal_Create_mA7A2A0191C4AAE7BD5B18F0DCC05AD4290D1691B (void);
// 0x0000003C System.Boolean UnityEngine.AnimationCurve::Internal_Equals(System.IntPtr)
extern void AnimationCurve_Internal_Equals_m7ACF09175F2DC61D95006ABB5BBE1CF7434B2D1D (void);
// 0x0000003D System.Void UnityEngine.AnimationCurve::Finalize()
extern void AnimationCurve_Finalize_mDF0DECA505DA883A56B2E3FCE1EF19CC3959F11D (void);
// 0x0000003E System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern void AnimationCurve_Evaluate_m51CAA6B1C54B7EF44FE4D74B422C1DA1FA6F8776 (void);
// 0x0000003F UnityEngine.Keyframe[] UnityEngine.AnimationCurve::get_keys()
extern void AnimationCurve_get_keys_m88E1848D255C2893F379E855A522DA9B0E0F78FB (void);
// 0x00000040 UnityEngine.Keyframe UnityEngine.AnimationCurve::get_Item(System.Int32)
extern void AnimationCurve_get_Item_m303DEF117A2702D57F8F5D55D422EC395E4388FC (void);
// 0x00000041 System.Int32 UnityEngine.AnimationCurve::get_length()
extern void AnimationCurve_get_length_m36B9D49BCB7D677C38A7963FF00313A2E48E7B26 (void);
// 0x00000042 UnityEngine.Keyframe UnityEngine.AnimationCurve::GetKey(System.Int32)
extern void AnimationCurve_GetKey_m2FD7B7AB3633B9C353FA59FC3927BC7EB904691F (void);
// 0x00000043 UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
extern void AnimationCurve_GetKeys_mC61A3A3E0D17E5849768B3AB3698F2139CF70EBE (void);
// 0x00000044 UnityEngine.WrapMode UnityEngine.AnimationCurve::get_preWrapMode()
extern void AnimationCurve_get_preWrapMode_m3C0DBEEB450ED8FB3FE96C7E1295B8C3562F8905 (void);
// 0x00000045 System.Void UnityEngine.AnimationCurve::set_preWrapMode(UnityEngine.WrapMode)
extern void AnimationCurve_set_preWrapMode_mFE146DFCE1E335655A079DCE6E8F046BD63A6489 (void);
// 0x00000046 UnityEngine.WrapMode UnityEngine.AnimationCurve::get_postWrapMode()
extern void AnimationCurve_get_postWrapMode_m1D9CF9570C4054057C97CDA03773A76C0813CF64 (void);
// 0x00000047 System.Void UnityEngine.AnimationCurve::set_postWrapMode(UnityEngine.WrapMode)
extern void AnimationCurve_set_postWrapMode_mF11FF01671DFC6E54D29111561B8C7104DCEB413 (void);
// 0x00000048 System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern void AnimationCurve__ctor_mE9462D171C06A2A746B9DA1B0A6B0F4FC7DB94CF (void);
// 0x00000049 System.Void UnityEngine.AnimationCurve::.ctor()
extern void AnimationCurve__ctor_mA12B39D1FD275B9A8150227B24805C7B218CDF2C (void);
// 0x0000004A System.Boolean UnityEngine.AnimationCurve::Equals(System.Object)
extern void AnimationCurve_Equals_m5E3528A0595AC6714584CAD54549D756C9B3DDD5 (void);
// 0x0000004B System.Boolean UnityEngine.AnimationCurve::Equals(UnityEngine.AnimationCurve)
extern void AnimationCurve_Equals_m60310C21F9B109BAC9BA4FACE9BEF88931B22DED (void);
// 0x0000004C System.Int32 UnityEngine.AnimationCurve::GetHashCode()
extern void AnimationCurve_GetHashCode_m22EEE795E7C76841C40A1563E3E90CBB089B19A6 (void);
// 0x0000004D System.Void UnityEngine.AnimationCurve::GetKey_Injected(System.Int32,UnityEngine.Keyframe&)
extern void AnimationCurve_GetKey_Injected_m01C24E74FE72837D5E6A59BBB1BACAEC308454EA (void);
// 0x0000004E System.Boolean UnityEngine.Application::get_isPlaying()
extern void Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5 (void);
// 0x0000004F System.Boolean UnityEngine.Application::HasProLicense()
extern void Application_HasProLicense_m2997DCC5DDAECB068C8A0B396AED23A03186CAD7 (void);
// 0x00000050 System.String UnityEngine.Application::get_dataPath()
extern void Application_get_dataPath_m33D721D71C0687F0013C8953FDB0807B7B3F2A01 (void);
// 0x00000051 System.String UnityEngine.Application::get_streamingAssetsPath()
extern void Application_get_streamingAssetsPath_m87163AE531BEB6A6588FABAD3207D829721CF31F (void);
// 0x00000052 System.String UnityEngine.Application::get_persistentDataPath()
extern void Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B (void);
// 0x00000053 System.String UnityEngine.Application::get_temporaryCachePath()
extern void Application_get_temporaryCachePath_m6769909A405A6A5A0477F8E5F312920C8115E50C (void);
// 0x00000054 System.String UnityEngine.Application::get_unityVersion()
extern void Application_get_unityVersion_mC66901DE17E8F4F5BCA46CF3A4DCB34AF245CF99 (void);
// 0x00000055 System.String UnityEngine.Application::get_version()
extern void Application_get_version_m17A1D2CEBF41849D65315834DD4513F16443A13B (void);
// 0x00000056 System.Void UnityEngine.Application::OpenURL(System.String)
extern void Application_OpenURL_m2888DA5BDF68B1BC23E983469157783F390D7BC8 (void);
// 0x00000057 System.Int32 UnityEngine.Application::get_targetFrameRate()
extern void Application_get_targetFrameRate_m74BA4A1BAF9542F7DAA99DA9DFBD22067874730A (void);
// 0x00000058 System.Void UnityEngine.Application::SetLogCallbackDefined(System.Boolean)
extern void Application_SetLogCallbackDefined_mC1FBF1745A2EDE55B290DC4C392A7F991F5039A8 (void);
// 0x00000059 System.Boolean UnityEngine.Application::get_genuine()
extern void Application_get_genuine_mD32DB2B5A6E0DC067136112E2EDA3A5680C97B40 (void);
// 0x0000005A System.Boolean UnityEngine.Application::get_genuineCheckAvailable()
extern void Application_get_genuineCheckAvailable_m0FE8E82E9EAA8382B0DB26C5BCDBD1A05C6A4182 (void);
// 0x0000005B UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern void Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672 (void);
// 0x0000005C System.Boolean UnityEngine.Application::get_isMobilePlatform()
extern void Application_get_isMobilePlatform_m11B260E344378D2A3CE53FCCA64DAC70F0B783E7 (void);
// 0x0000005D UnityEngine.SystemLanguage UnityEngine.Application::get_systemLanguage()
extern void Application_get_systemLanguage_m64281F3CE136BD62EC96EF6AC691C612F0DD08B1 (void);
// 0x0000005E UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
extern void Application_get_internetReachability_m8BB82882CF9E286370E4D64176CF2B9333E83F0A (void);
// 0x0000005F System.Void UnityEngine.Application::CallLowMemory()
extern void Application_CallLowMemory_m4C6693BD717D61DB33C2FB061FDA8CE055966E75 (void);
// 0x00000060 System.Void UnityEngine.Application::add_logMessageReceivedThreaded(UnityEngine.Application/LogCallback)
extern void Application_add_logMessageReceivedThreaded_m6EAA7C1782E341A6A72AC0C9160CF3F781322ABB (void);
// 0x00000061 System.Void UnityEngine.Application::remove_logMessageReceivedThreaded(UnityEngine.Application/LogCallback)
extern void Application_remove_logMessageReceivedThreaded_m2BD60919BDACAF705EC45A89DE2AC815F965B7C2 (void);
// 0x00000062 System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern void Application_CallLogCallback_mCA351E4FBE7397C3D09A7FBD8A9B074A4745ED89 (void);
// 0x00000063 System.Boolean UnityEngine.Application::Internal_ApplicationWantsToQuit()
extern void Application_Internal_ApplicationWantsToQuit_mDF35192EF816ECD73F0BD4AFBCDE1460EF06442A (void);
// 0x00000064 System.Void UnityEngine.Application::Internal_ApplicationQuit()
extern void Application_Internal_ApplicationQuit_mC9ACAA5CB0800C837DBD9925E1E389FB918F3DED (void);
// 0x00000065 System.Void UnityEngine.Application::InvokeOnBeforeRender()
extern void Application_InvokeOnBeforeRender_mF2E1F3E67C1D160AD1209C1DBC1EC91E8FB88C97 (void);
// 0x00000066 System.Void UnityEngine.Application::InvokeFocusChanged(System.Boolean)
extern void Application_InvokeFocusChanged_m61786C9688D01809FAC41250B371CE13C9DBBD6F (void);
// 0x00000067 System.Void UnityEngine.Application::InvokeDeepLinkActivated(System.String)
extern void Application_InvokeDeepLinkActivated_m473D851836BD708C896850AA1DAE2B56A4B01176 (void);
// 0x00000068 System.Boolean UnityEngine.Application::get_isEditor()
extern void Application_get_isEditor_m347E6EE16E5109EF613C83ED98DB1EC6E3EF5E26 (void);
// 0x00000069 System.Void UnityEngine.Application/LowMemoryCallback::.ctor(System.Object,System.IntPtr)
extern void LowMemoryCallback__ctor_m9A428FDE023342AE31B3749FC821B078AEDA2290 (void);
// 0x0000006A System.Void UnityEngine.Application/LowMemoryCallback::Invoke()
extern void LowMemoryCallback_Invoke_m3082D6F2046585D3504696B94A59A4CBC43262F8 (void);
// 0x0000006B System.IAsyncResult UnityEngine.Application/LowMemoryCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void LowMemoryCallback_BeginInvoke_m4686E95B4CF6EDE103DB0448FC54354BAE5F1745 (void);
// 0x0000006C System.Void UnityEngine.Application/LowMemoryCallback::EndInvoke(System.IAsyncResult)
extern void LowMemoryCallback_EndInvoke_mB8843171E51584D380B62D3B79BC4F4930A22D0C (void);
// 0x0000006D System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern void LogCallback__ctor_mF61E7CECD9E360B0B8A992720860F9816E165731 (void);
// 0x0000006E System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern void LogCallback_Invoke_mCB0C38C44CBF8BBE88690BE6C0382011C5D5B61F (void);
// 0x0000006F System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern void LogCallback_BeginInvoke_mECA20C96EB7E35915BC9202F833685D0ED5F66A7 (void);
// 0x00000070 System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern void LogCallback_EndInvoke_m03CD6E28DACBF36E7A756F8CC653E546CBF3FD54 (void);
// 0x00000071 UnityEngine.BootConfigData UnityEngine.BootConfigData::WrapBootConfigData(System.IntPtr)
extern void BootConfigData_WrapBootConfigData_m7C2DCB60E1456C2B7748ECFAAEB492611A5D7690 (void);
// 0x00000072 System.Void UnityEngine.BootConfigData::.ctor(System.IntPtr)
extern void BootConfigData__ctor_m6C109EB48CAE91C89BB6C4BFD10C77EA6723E5AE (void);
// 0x00000073 System.Void UnityEngine.CachedAssetBundle::.ctor(System.String,UnityEngine.Hash128)
extern void CachedAssetBundle__ctor_mBB07A43F19F5095BC21A44543684F6B4269CE315 (void);
// 0x00000074 System.String UnityEngine.CachedAssetBundle::get_name()
extern void CachedAssetBundle_get_name_m31556A8588D4BE9261B0AB02486118A30D3ED971 (void);
// 0x00000075 UnityEngine.Hash128 UnityEngine.CachedAssetBundle::get_hash()
extern void CachedAssetBundle_get_hash_mC7D2AD78B64D7CDEF0604C934BF97D6C388A3EC1 (void);
// 0x00000076 System.Int32 UnityEngine.Cache::get_handle()
extern void Cache_get_handle_m2A81C085DE744E211DCC3F5BD608043EEB93075D (void);
// 0x00000077 System.Int32 UnityEngine.Cache::GetHashCode()
extern void Cache_GetHashCode_m3C305D87FF4578864D1844170133E2758986B244 (void);
// 0x00000078 System.Boolean UnityEngine.Cache::Equals(System.Object)
extern void Cache_Equals_m4E85CF7FABB9052E9673879DF8FB341453D76F06 (void);
// 0x00000079 System.Boolean UnityEngine.Cache::Equals(UnityEngine.Cache)
extern void Cache_Equals_m49F0BDF7BE07D3FA50A70ED8FE85DBFF178C11C5 (void);
// 0x0000007A System.Boolean UnityEngine.Cache::get_valid()
extern void Cache_get_valid_m01BE7338A2AB06309EC3808BD361F0A355E0B29F (void);
// 0x0000007B System.Boolean UnityEngine.Cache::Cache_IsValid(System.Int32)
extern void Cache_Cache_IsValid_mB53CE63DB4F248634246D0FA01304E2CE0634134 (void);
// 0x0000007C System.String UnityEngine.Cache::get_path()
extern void Cache_get_path_mC3C0F1863B3EE5BE472D43BC25797E1EC3278A6B (void);
// 0x0000007D System.String UnityEngine.Cache::Cache_GetPath(System.Int32)
extern void Cache_Cache_GetPath_m0D63A662CE529A63D7B33C557D183019EB3CCFD1 (void);
// 0x0000007E System.Void UnityEngine.Cache::set_maximumAvailableStorageSpace(System.Int64)
extern void Cache_set_maximumAvailableStorageSpace_m2F9DA827094B24940EE439739EC7B78EC5237559 (void);
// 0x0000007F System.Void UnityEngine.Cache::Cache_SetMaximumDiskSpaceAvailable(System.Int32,System.Int64)
extern void Cache_Cache_SetMaximumDiskSpaceAvailable_mBCF682A98B18956111635708BD4BF027763E3E52 (void);
// 0x00000080 System.Void UnityEngine.Cache::set_expirationDelay(System.Int32)
extern void Cache_set_expirationDelay_m2D2ABE9B1D1503A16A94A423852E7D037C7F213A (void);
// 0x00000081 System.Void UnityEngine.Cache::Cache_SetExpirationDelay(System.Int32,System.Int32)
extern void Cache_Cache_SetExpirationDelay_m0ECC3CCFE9B618FFAF32C5B4C431F28B7D36051A (void);
// 0x00000082 System.Void UnityEngine.Caching::set_compressionEnabled(System.Boolean)
extern void Caching_set_compressionEnabled_mDFEB4694C4E4CEA778ED304C4A13C08D4B4BA628 (void);
// 0x00000083 System.Boolean UnityEngine.Caching::get_ready()
extern void Caching_get_ready_m6C8272E098105B2BA5C88055B10EA5B4530F5BBB (void);
// 0x00000084 System.Boolean UnityEngine.Caching::ClearCachedVersion(System.String,UnityEngine.Hash128)
extern void Caching_ClearCachedVersion_m4A896CB52B29B1DE99561B554816835FAFC9EEA7 (void);
// 0x00000085 System.Boolean UnityEngine.Caching::ClearCachedVersionInternal(System.String,UnityEngine.Hash128)
extern void Caching_ClearCachedVersionInternal_m82D1DC4D0D7DB7BE62A575B1A9158740CE45FB4B (void);
// 0x00000086 System.Boolean UnityEngine.Caching::ClearOtherCachedVersions(System.String,UnityEngine.Hash128)
extern void Caching_ClearOtherCachedVersions_m4658A68D22DF56BE2657A10D2DC5CC56AE19DB8C (void);
// 0x00000087 System.Boolean UnityEngine.Caching::ClearAllCachedVersions(System.String)
extern void Caching_ClearAllCachedVersions_m6C3EEF03B4A51993F24FEF394C57BD4F3F66D30A (void);
// 0x00000088 System.Boolean UnityEngine.Caching::ClearCachedVersions(System.String,UnityEngine.Hash128,System.Boolean)
extern void Caching_ClearCachedVersions_mF759564A2D48559C53CB5A18FB2AF0393ED7EEF9 (void);
// 0x00000089 System.Boolean UnityEngine.Caching::IsVersionCached(UnityEngine.CachedAssetBundle)
extern void Caching_IsVersionCached_m4AB434A7DD30FAC3058F9861AEB2F6A967B43081 (void);
// 0x0000008A System.Boolean UnityEngine.Caching::IsVersionCached(System.String,System.String,UnityEngine.Hash128)
extern void Caching_IsVersionCached_m407E9764BC468430F2BCD6B75C1CECBC108613C5 (void);
// 0x0000008B UnityEngine.Cache UnityEngine.Caching::AddCache(System.String)
extern void Caching_AddCache_m573357DBAD8C1DDE863196456B72B8DD191DB689 (void);
// 0x0000008C UnityEngine.Cache UnityEngine.Caching::AddCache(System.String,System.Boolean)
extern void Caching_AddCache_m2D73DCAF1FF375066A5B215AAB7A8209978F18AE (void);
// 0x0000008D UnityEngine.Cache UnityEngine.Caching::GetCacheByPath(System.String)
extern void Caching_GetCacheByPath_m023FB602A5A1041F7AEC5E81AE1273E93B75F7B9 (void);
// 0x0000008E UnityEngine.Cache UnityEngine.Caching::get_defaultCache()
extern void Caching_get_defaultCache_mFB94A67BA445311B4F52E2F44F6263397BF85B29 (void);
// 0x0000008F UnityEngine.Cache UnityEngine.Caching::get_currentCacheForWriting()
extern void Caching_get_currentCacheForWriting_mFB8AE12285B5668804B7CFD7A387D4FA4B7F1748 (void);
// 0x00000090 System.Void UnityEngine.Caching::set_currentCacheForWriting(UnityEngine.Cache)
extern void Caching_set_currentCacheForWriting_mF41EA3ECE25C4A2AFE4037F002A03E3245EEC2E8 (void);
// 0x00000091 System.Boolean UnityEngine.Caching::ClearCachedVersionInternal_Injected(System.String,UnityEngine.Hash128&)
extern void Caching_ClearCachedVersionInternal_Injected_mA7817A8A0AFFB1F20ED896A1A7BBEB4C54F43936 (void);
// 0x00000092 System.Boolean UnityEngine.Caching::ClearCachedVersions_Injected(System.String,UnityEngine.Hash128&,System.Boolean)
extern void Caching_ClearCachedVersions_Injected_m58ABB302FB321A31B01AD3395DD7D45A7C4FBEB7 (void);
// 0x00000093 System.Boolean UnityEngine.Caching::IsVersionCached_Injected(System.String,System.String,UnityEngine.Hash128&)
extern void Caching_IsVersionCached_Injected_mB81B7060C3A07ABC01CADF1B35462F3DA410F73E (void);
// 0x00000094 System.Void UnityEngine.Caching::AddCache_Injected(System.String,System.Boolean,UnityEngine.Cache&)
extern void Caching_AddCache_Injected_m4D9302CA8DE41732BFE94CA7DE4193A25BF2ADC2 (void);
// 0x00000095 System.Void UnityEngine.Caching::GetCacheByPath_Injected(System.String,UnityEngine.Cache&)
extern void Caching_GetCacheByPath_Injected_m4E373A60168E4A550F2B09F21673842DC71CFB51 (void);
// 0x00000096 System.Void UnityEngine.Caching::get_defaultCache_Injected(UnityEngine.Cache&)
extern void Caching_get_defaultCache_Injected_mE50C6C2A72FA0B2862D51E7969F2A6ECF81DAB8D (void);
// 0x00000097 System.Void UnityEngine.Caching::get_currentCacheForWriting_Injected(UnityEngine.Cache&)
extern void Caching_get_currentCacheForWriting_Injected_mCA9AB9FDA1F2AB9A87046B99938327D473AC0C2A (void);
// 0x00000098 System.Void UnityEngine.Caching::set_currentCacheForWriting_Injected(UnityEngine.Cache&)
extern void Caching_set_currentCacheForWriting_Injected_m3C95A1BE071301E134A8B472E8E1C1D022F6316B (void);
// 0x00000099 System.Single UnityEngine.Camera::get_nearClipPlane()
extern void Camera_get_nearClipPlane_mD9D3E3D27186BBAC2CC354CE3609E6118A5BF66C (void);
// 0x0000009A System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
extern void Camera_set_nearClipPlane_m9D81E50F8658C16319BEF3774E78B93DEB208C6B (void);
// 0x0000009B System.Single UnityEngine.Camera::get_farClipPlane()
extern void Camera_get_farClipPlane_mF51F1FF5BE87719CFAC293E272B1138DC1EFFD4B (void);
// 0x0000009C System.Void UnityEngine.Camera::set_farClipPlane(System.Single)
extern void Camera_set_farClipPlane_m52986DC40B7F577255C4D5A4F780FD8A7D862626 (void);
// 0x0000009D System.Single UnityEngine.Camera::get_fieldOfView()
extern void Camera_get_fieldOfView_m065A50B70AC3661337ACA482DDEFA29CCBD249D6 (void);
// 0x0000009E System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern void Camera_set_fieldOfView_m5006BA0D01A27619A053704D3BD6A8938F7DEDA5 (void);
// 0x0000009F System.Single UnityEngine.Camera::get_orthographicSize()
extern void Camera_get_orthographicSize_m700FCD8CF48BC59A0415A624328B4A627B88D958 (void);
// 0x000000A0 System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern void Camera_set_orthographicSize_mF15F37A294A7AA2ADD9519728A495DFA0A836428 (void);
// 0x000000A1 System.Single UnityEngine.Camera::get_depth()
extern void Camera_get_depth_m436C49A1C7669E4AD5665A1F1107BDFBA38742CD (void);
// 0x000000A2 System.Void UnityEngine.Camera::set_depth(System.Single)
extern void Camera_set_depth_m4A83CCCF7370B8AD4BDB2CD5528A6E12A409AE58 (void);
// 0x000000A3 System.Single UnityEngine.Camera::get_aspect()
extern void Camera_get_aspect_m2ADA7982754920C3B58B4DB664801D6F2416E0C6 (void);
// 0x000000A4 System.Void UnityEngine.Camera::set_aspect(System.Single)
extern void Camera_set_aspect_m84BE4641686B30B8F9FFEA47BB1D7D88E27344EE (void);
// 0x000000A5 System.Int32 UnityEngine.Camera::get_cullingMask()
extern void Camera_get_cullingMask_m0992E96D87A4221E38746EBD882780CEFF7C2BCD (void);
// 0x000000A6 System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
extern void Camera_set_cullingMask_m215DB1C878CF1ADEEF6800AF449EEEA7680ECCCD (void);
// 0x000000A7 System.Int32 UnityEngine.Camera::get_eventMask()
extern void Camera_get_eventMask_m1D85900090AF34244340C69B53A42CDE5E9669D3 (void);
// 0x000000A8 UnityEngine.Color UnityEngine.Camera::get_backgroundColor()
extern void Camera_get_backgroundColor_m14496C5DC24582D7227277AF71DBE96F8E9E64FF (void);
// 0x000000A9 System.Void UnityEngine.Camera::set_backgroundColor(UnityEngine.Color)
extern void Camera_set_backgroundColor_mDB9CA1B37FE2D52493823914AC5BC9F8C1935D6F (void);
// 0x000000AA UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern void Camera_get_clearFlags_m1D02BA1ABD7310269F6121C58AF41DCDEF1E0266 (void);
// 0x000000AB System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
extern void Camera_set_clearFlags_m805DFBD136AA3E1E46A2E61441965D174E87FE50 (void);
// 0x000000AC UnityEngine.Rect UnityEngine.Camera::get_rect()
extern void Camera_get_rect_m3570AA056526AB01C7733B4E7BE69F332E128A08 (void);
// 0x000000AD System.Void UnityEngine.Camera::set_rect(UnityEngine.Rect)
extern void Camera_set_rect_m6DB9964EA6E519E2B07561C8CE6AA423980FEC11 (void);
// 0x000000AE UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern void Camera_get_pixelRect_mBA87D6C23FD7A5E1A7F3CE0E8F9B86A9318B5317 (void);
// 0x000000AF System.Void UnityEngine.Camera::set_pixelRect(UnityEngine.Rect)
extern void Camera_set_pixelRect_m9380482EFA5D7912988D585E9538A58988C8E0E9 (void);
// 0x000000B0 UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern void Camera_get_targetTexture_m1E776560FAC888D8210D49CEE310BB39D34A3FDC (void);
// 0x000000B1 System.Int32 UnityEngine.Camera::get_targetDisplay()
extern void Camera_get_targetDisplay_m2C318D2EB9A016FEC76B13F7F7AE382F443FB731 (void);
// 0x000000B2 UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3,UnityEngine.Camera/MonoOrStereoscopicEye)
extern void Camera_WorldToScreenPoint_m315B44D111E92F6C81C39B7B0927622289C1BC52 (void);
// 0x000000B3 UnityEngine.Vector3 UnityEngine.Camera::WorldToViewportPoint(UnityEngine.Vector3,UnityEngine.Camera/MonoOrStereoscopicEye)
extern void Camera_WorldToViewportPoint_m20C50DA05728A82BCDE98CBED34841BA6FB0EA59 (void);
// 0x000000B4 UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern void Camera_WorldToScreenPoint_m880F9611E4848C11F21FDF1A1D307B401C61B1BF (void);
// 0x000000B5 UnityEngine.Vector3 UnityEngine.Camera::WorldToViewportPoint(UnityEngine.Vector3)
extern void Camera_WorldToViewportPoint_mC939A83CE134EA2CE666C2DF5AD8FA4CB48AE3C1 (void);
// 0x000000B6 UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern void Camera_ScreenToViewportPoint_m52ABFA35ADAA0B4FF3A7EE675F92F8F483E821FD (void);
// 0x000000B7 UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector2,UnityEngine.Camera/MonoOrStereoscopicEye)
extern void Camera_ScreenPointToRay_m84C3D8E0A4E8390A353C2361A0900372742065A0 (void);
// 0x000000B8 UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3,UnityEngine.Camera/MonoOrStereoscopicEye)
extern void Camera_ScreenPointToRay_m7069BC09C3D802595AC1FBAEFB3C59C8F1FE5FE2 (void);
// 0x000000B9 UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern void Camera_ScreenPointToRay_m27638E78502DB6D6D7113F81AF7C210773B828F3 (void);
// 0x000000BA UnityEngine.Camera UnityEngine.Camera::get_main()
extern void Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA (void);
// 0x000000BB UnityEngine.Camera UnityEngine.Camera::get_current()
extern void Camera_get_current_m6D8446E8359399CD9108A8E524671E0CC6E20350 (void);
// 0x000000BC System.Int32 UnityEngine.Camera::GetAllCamerasCount()
extern void Camera_GetAllCamerasCount_mBA721F43F94AA5DB555461DE11351CBAF8267662 (void);
// 0x000000BD System.Int32 UnityEngine.Camera::GetAllCamerasImpl(UnityEngine.Camera[])
extern void Camera_GetAllCamerasImpl_mC93829FFC53391EA6C5012E5FA3817BA20DBEA89 (void);
// 0x000000BE System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern void Camera_get_allCamerasCount_mF6CDC46D6F61B1F1A0337A9AD7DFA485E408E6A1 (void);
// 0x000000BF System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern void Camera_GetAllCameras_m500A4F27E7BE1C259E9EAA0AEBB1E1B35893059C (void);
// 0x000000C0 System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern void Camera_FireOnPreCull_m7E8B65875444B1DE75170805AE22908ADE52301E (void);
// 0x000000C1 System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern void Camera_FireOnPreRender_m996699B5D50FC3D0AB05EED9F9CE581CCDC2FF67 (void);
// 0x000000C2 System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern void Camera_FireOnPostRender_m17457A692D59CBDDDBBE0E4C441D393DAD58654B (void);
// 0x000000C3 System.Void UnityEngine.Camera::.ctor()
extern void Camera__ctor_mD07AB17467A910BC7A4EE32BB9DD546748E31254 (void);
// 0x000000C4 System.Void UnityEngine.Camera::get_backgroundColor_Injected(UnityEngine.Color&)
extern void Camera_get_backgroundColor_Injected_m35D7092E021C199D24A3457297EEEAA520CAC999 (void);
// 0x000000C5 System.Void UnityEngine.Camera::set_backgroundColor_Injected(UnityEngine.Color&)
extern void Camera_set_backgroundColor_Injected_m5E1BF10175DAB3F4E2D83810640D1FB5EF49A9F9 (void);
// 0x000000C6 System.Void UnityEngine.Camera::get_rect_Injected(UnityEngine.Rect&)
extern void Camera_get_rect_Injected_m88F10E0BE4F27E638C97010D4616DBB14338EEED (void);
// 0x000000C7 System.Void UnityEngine.Camera::set_rect_Injected(UnityEngine.Rect&)
extern void Camera_set_rect_Injected_m62EE0CFFE15C612C226DEAB0ADB3D3B81ABE0C4E (void);
// 0x000000C8 System.Void UnityEngine.Camera::get_pixelRect_Injected(UnityEngine.Rect&)
extern void Camera_get_pixelRect_Injected_mDE6A7F125BC1DD2BCFEA3CB03DFA948E5635E631 (void);
// 0x000000C9 System.Void UnityEngine.Camera::set_pixelRect_Injected(UnityEngine.Rect&)
extern void Camera_set_pixelRect_Injected_mBE9F9ED0BC921A91C9E1B85075E8E7F8ECD61D45 (void);
// 0x000000CA System.Void UnityEngine.Camera::WorldToScreenPoint_Injected(UnityEngine.Vector3&,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Vector3&)
extern void Camera_WorldToScreenPoint_Injected_m640C6AFA68F6C2AD25AFD9E06C1AEFEAC5B48B01 (void);
// 0x000000CB System.Void UnityEngine.Camera::WorldToViewportPoint_Injected(UnityEngine.Vector3&,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Vector3&)
extern void Camera_WorldToViewportPoint_Injected_m5491C4691EB46452E1A48AE20A18769D41B5216C (void);
// 0x000000CC System.Void UnityEngine.Camera::ScreenToViewportPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Camera_ScreenToViewportPoint_Injected_m407A30EDD4AC317DE3DD0B4361664F438E5A6639 (void);
// 0x000000CD System.Void UnityEngine.Camera::ScreenPointToRay_Injected(UnityEngine.Vector2&,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Ray&)
extern void Camera_ScreenPointToRay_Injected_m1135D2C450C7DED657837BEFE5AD7FAFB9B99387 (void);
// 0x000000CE System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern void CameraCallback__ctor_m7CAE962B355F00AB2868577DC302A1FA80939C50 (void);
// 0x000000CF System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern void CameraCallback_Invoke_m2B4F10A7BF2620A9BBF1C071D5B4EE828FFE821F (void);
// 0x000000D0 System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern void CameraCallback_BeginInvoke_m46CF0E3E7E6A18868CBEBEA62D012713B20A8B14 (void);
// 0x000000D1 System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern void CameraCallback_EndInvoke_m3B1E210D6A4F41F0FF74B187B3D7CB64C302D146 (void);
// 0x000000D2 System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern void CullingGroup_SendEvents_m08EBF10EEFF49CF9894BA940FD969C8F53F807E7 (void);
// 0x000000D3 System.Void UnityEngine.CullingGroup/StateChanged::.ctor(System.Object,System.IntPtr)
extern void StateChanged__ctor_m8DCC0DCE42D5257F92FEA1F2B4DA2EF4558006F9 (void);
// 0x000000D4 System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern void StateChanged_Invoke_m2E371D6B1AD1F23F20038D0DEEEFED15D76BC545 (void);
// 0x000000D5 System.IAsyncResult UnityEngine.CullingGroup/StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern void StateChanged_BeginInvoke_m5BD458B36BF2E71F4FB19444B0FAAA1B87BF8912 (void);
// 0x000000D6 System.Void UnityEngine.CullingGroup/StateChanged::EndInvoke(System.IAsyncResult)
extern void StateChanged_EndInvoke_mBC050D5602C1F3EC3F8137908D81894E646F5212 (void);
// 0x000000D7 System.Void UnityEngine.ReflectionProbe::CallReflectionProbeEvent(UnityEngine.ReflectionProbe,UnityEngine.ReflectionProbe/ReflectionProbeEvent)
extern void ReflectionProbe_CallReflectionProbeEvent_mA6273CE84793FD3CC7AA0506C525EED4F4935B62 (void);
// 0x000000D8 System.Void UnityEngine.ReflectionProbe::CallSetDefaultReflection(UnityEngine.Cubemap)
extern void ReflectionProbe_CallSetDefaultReflection_m97EFBE5F8A57BB7C8CA65C65FF4BD9889060325D (void);
// 0x000000D9 System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,UnityEngine.LogOption,System.String,UnityEngine.Object)
extern void DebugLogHandler_Internal_Log_m2B637FD9089DEAA9D9FDE458DF5415CDF97424C3 (void);
// 0x000000DA System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern void DebugLogHandler_Internal_LogException_m8400B0D97B8D4A155A449BD28A32C68373A1A856 (void);
// 0x000000DB System.Void UnityEngine.DebugLogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern void DebugLogHandler_LogFormat_m3C9B0AD4B5CDFF5AF195F9AA9FCBA908053BA41D (void);
// 0x000000DC System.Void UnityEngine.DebugLogHandler::LogException(System.Exception,UnityEngine.Object)
extern void DebugLogHandler_LogException_m816CF2DDA84DFC1D1715B24F9626BD623FF05416 (void);
// 0x000000DD System.Void UnityEngine.DebugLogHandler::.ctor()
extern void DebugLogHandler__ctor_mE9664BE5E6020FB88C6A301465811C80DEDFA392 (void);
// 0x000000DE UnityEngine.ILogger UnityEngine.Debug::get_unityLogger()
extern void Debug_get_unityLogger_mFA75EC397E067D09FD66D56B4E7692C3FCC3E960 (void);
// 0x000000DF System.Int32 UnityEngine.Debug::ExtractStackTraceNoAlloc(System.Byte*,System.Int32,System.String)
extern void Debug_ExtractStackTraceNoAlloc_m69F8666E91D27D8C341385EB5CF79621C703248C (void);
// 0x000000E0 System.Void UnityEngine.Debug::Log(System.Object)
extern void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (void);
// 0x000000E1 System.Void UnityEngine.Debug::Log(System.Object,UnityEngine.Object)
extern void Debug_Log_mCF5342DCBD53044ABDE6377C236B9E40EC26BF3F (void);
// 0x000000E2 System.Void UnityEngine.Debug::LogFormat(System.String,System.Object[])
extern void Debug_LogFormat_mB23DDD2CD05B2E66F9CF8CA72ECA66C02DCC209E (void);
// 0x000000E3 System.Void UnityEngine.Debug::LogError(System.Object)
extern void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (void);
// 0x000000E4 System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern void Debug_LogError_m97139CB2EE76D5CD8308C1AD0499A5F163FC7F51 (void);
// 0x000000E5 System.Void UnityEngine.Debug::LogErrorFormat(System.String,System.Object[])
extern void Debug_LogErrorFormat_mB54A656B267CF936439D50348FC828921AEDA8A9 (void);
// 0x000000E6 System.Void UnityEngine.Debug::LogErrorFormat(UnityEngine.Object,System.String,System.Object[])
extern void Debug_LogErrorFormat_m994E4759C25BF0E9DD4179C10E3979558137CCF0 (void);
// 0x000000E7 System.Void UnityEngine.Debug::LogException(System.Exception)
extern void Debug_LogException_mBAA6702C240E37B2A834AA74E4FDC15A3A5589A9 (void);
// 0x000000E8 System.Void UnityEngine.Debug::LogException(System.Exception,UnityEngine.Object)
extern void Debug_LogException_m3CC9A37CD398E5B7F2305896F0969939F1BD1E3E (void);
// 0x000000E9 System.Void UnityEngine.Debug::LogWarning(System.Object)
extern void Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568 (void);
// 0x000000EA System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern void Debug_LogWarning_mD417697331190AC1D21C463F412C475103A7256E (void);
// 0x000000EB System.Void UnityEngine.Debug::LogWarningFormat(System.String,System.Object[])
extern void Debug_LogWarningFormat_m29C3DA389E1AA2C1C48C9100F1E83EAE72772FDB (void);
// 0x000000EC System.Void UnityEngine.Debug::LogWarningFormat(UnityEngine.Object,System.String,System.Object[])
extern void Debug_LogWarningFormat_m4A02CCF91F3A9392F4AA93576DCE2222267E5945 (void);
// 0x000000ED System.Void UnityEngine.Debug::LogAssertion(System.Object)
extern void Debug_LogAssertion_m2A8940871EC1BD01A405103429F2FCE2AFB12506 (void);
// 0x000000EE System.Boolean UnityEngine.Debug::get_isDebugBuild()
extern void Debug_get_isDebugBuild_mED5A7963C7B055A9ACC5565862BBBA6F3D86EDE8 (void);
// 0x000000EF System.Boolean UnityEngine.Debug::CallOverridenDebugHandler(System.Exception,UnityEngine.Object)
extern void Debug_CallOverridenDebugHandler_m5F5FC22445A9C957A655734DA5B661A5E256BEBE (void);
// 0x000000F0 System.Boolean UnityEngine.Debug::IsLoggingEnabled()
extern void Debug_IsLoggingEnabled_m749A9476334B62979E6D9949A0A0E489A9C735D8 (void);
// 0x000000F1 System.Void UnityEngine.Debug::.cctor()
extern void Debug__cctor_m9BFDFB65B30AA2962FDACD15F36FC666471D1C5E (void);
// 0x000000F2 System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Bounds__ctor_m294E77A20EC1A3E96985FE1A925CB271D1B5266D (void);
// 0x000000F3 System.Int32 UnityEngine.Bounds::GetHashCode()
extern void Bounds_GetHashCode_m9F5F751E9D52F99FCC3DC07407410078451F06AC (void);
// 0x000000F4 System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern void Bounds_Equals_m1ECFAEFE19BAFB61FFECA5C0B8AE068483A39C61 (void);
// 0x000000F5 System.Boolean UnityEngine.Bounds::Equals(UnityEngine.Bounds)
extern void Bounds_Equals_mC2E2B04AB16455E2C17CD0B3C1497835DEA39859 (void);
// 0x000000F6 UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern void Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B (void);
// 0x000000F7 System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern void Bounds_set_center_mAD29DD80FD631F83AF4E7558BB27A0398E8FD841 (void);
// 0x000000F8 UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern void Bounds_get_size_m0739F2686AE2D3416A33AEF892653091347FD4A6 (void);
// 0x000000F9 System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern void Bounds_set_size_m70855AC67A54062D676174B416FB06019226B39A (void);
// 0x000000FA UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern void Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9 (void);
// 0x000000FB System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern void Bounds_set_extents_mC83719146B06D0575A160CDDE9997202A1192B35 (void);
// 0x000000FC UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern void Bounds_get_min_m2D48F74D29BF904D1AF19C562932E34ACAE2467C (void);
// 0x000000FD UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern void Bounds_get_max_mC3BE43C2A865BAC138D117684BC01E289892549B (void);
// 0x000000FE System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern void Bounds_op_Equality_m8168B65BF71D8E5B2F0181677ED79957DD754FF4 (void);
// 0x000000FF System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern void Bounds_op_Inequality_mA6EBEDD980A41D5E206CBE009731EB1CA0B25502 (void);
// 0x00000100 System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Bounds_SetMinMax_m04969DE5CBC7F9843C12926ADD5F591159C86CA6 (void);
// 0x00000101 System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern void Bounds_Encapsulate_mD1F1DAC416D7147E07BF54D87CA7FF84C1088D8D (void);
// 0x00000102 System.String UnityEngine.Bounds::ToString()
extern void Bounds_ToString_m4637EA7C58B9C75651A040182471E9BAB9295666 (void);
// 0x00000103 UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern void Plane_get_normal_m203D43F51C449990214D04F332E8261295162E84 (void);
// 0x00000104 System.Single UnityEngine.Plane::get_distance()
extern void Plane_get_distance_m5358B80C35E1E295C0133E7DC6449BB09C456DEE (void);
// 0x00000105 System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Plane__ctor_m6535EAD5E675627C2533962F1F7890CBFA2BA44A (void);
// 0x00000106 System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern void Plane_Raycast_m04E61D7C78A5DA70F4F73F9805ABB54177B799A9 (void);
// 0x00000107 System.String UnityEngine.Plane::ToString()
extern void Plane_ToString_mF92ABB5136759C7DFBC26FD3957532B3C26F2099 (void);
// 0x00000108 System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B (void);
// 0x00000109 UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern void Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187 (void);
// 0x0000010A UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern void Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E (void);
// 0x0000010B UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern void Ray_GetPoint_mE8830D3BA68A184AD70514428B75F5664105ED08 (void);
// 0x0000010C System.String UnityEngine.Ray::ToString()
extern void Ray_ToString_m73B5291E29C9C691773B44590C467A0D4FBE0EC1 (void);
// 0x0000010D System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280 (void);
// 0x0000010E System.Void UnityEngine.Rect::.ctor(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Rect__ctor_m027E778E437FED8E6DBCE0C01C854ADF54986ECE (void);
// 0x0000010F UnityEngine.Rect UnityEngine.Rect::get_zero()
extern void Rect_get_zero_m4CF0F9AD904132829A6EFCA85A1BF52794E7E56B (void);
// 0x00000110 System.Single UnityEngine.Rect::get_x()
extern void Rect_get_x_mC51A461F546D14832EB96B11A7198DADDE2597B7 (void);
// 0x00000111 System.Void UnityEngine.Rect::set_x(System.Single)
extern void Rect_set_x_m49EFE25263C03A48D52499C3E9C097298E0EA3A6 (void);
// 0x00000112 System.Single UnityEngine.Rect::get_y()
extern void Rect_get_y_m53E3E4F62D9840FBEA751A66293038F1F5D1D45C (void);
// 0x00000113 System.Void UnityEngine.Rect::set_y(System.Single)
extern void Rect_set_y_mCFDB9BD77334EF9CD896F64BE63C755777D7CCD5 (void);
// 0x00000114 UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern void Rect_get_position_m54A2ACD2F97988561D6C83FCEF7D082BC5226D4C (void);
// 0x00000115 System.Void UnityEngine.Rect::set_position(UnityEngine.Vector2)
extern void Rect_set_position_mD92DFF591D9C96CDD6AF22EA2052BB3D468D68ED (void);
// 0x00000116 UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern void Rect_get_center_mA6E659EAAACC32132022AB199793BF641B3068CB (void);
// 0x00000117 System.Void UnityEngine.Rect::set_center(UnityEngine.Vector2)
extern void Rect_set_center_m6F280BE23E6FAA6214DDA6FB6B7697DB906D879E (void);
// 0x00000118 UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern void Rect_get_min_m17345668569CF57C5F1D2B2DADD05DD4220A5950 (void);
// 0x00000119 System.Void UnityEngine.Rect::set_min(UnityEngine.Vector2)
extern void Rect_set_min_m3A3DE206469065D65650DCF4D23F51C25B6C08A7 (void);
// 0x0000011A UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern void Rect_get_max_m3BFB033D741F205FB04EF163A9D5785E7E020756 (void);
// 0x0000011B System.Void UnityEngine.Rect::set_max(UnityEngine.Vector2)
extern void Rect_set_max_mCC50E64F2DE589A3B7D1BFED72B49AC19D49FAEB (void);
// 0x0000011C System.Single UnityEngine.Rect::get_width()
extern void Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484 (void);
// 0x0000011D System.Void UnityEngine.Rect::set_width(System.Single)
extern void Rect_set_width_mC81EF602AC91E0C615C12FCE060254A461A152B8 (void);
// 0x0000011E System.Single UnityEngine.Rect::get_height()
extern void Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5 (void);
// 0x0000011F System.Void UnityEngine.Rect::set_height(System.Single)
extern void Rect_set_height_mF4CB5A97D4706696F1C9EA31A5D8C466E48050D6 (void);
// 0x00000120 UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern void Rect_get_size_m731642B8F03F6CE372A2C9E2E4A925450630606C (void);
// 0x00000121 System.Void UnityEngine.Rect::set_size(UnityEngine.Vector2)
extern void Rect_set_size_m4618056983660063A74F40CCFF9A683933CB4C93 (void);
// 0x00000122 System.Single UnityEngine.Rect::get_xMin()
extern void Rect_get_xMin_mFDFA74F66595FD2B8CE360183D1A92B575F0A76E (void);
// 0x00000123 System.Void UnityEngine.Rect::set_xMin(System.Single)
extern void Rect_set_xMin_mD8F9BF59F4F33F9C3AB2FEFF32D8C16756B51E34 (void);
// 0x00000124 System.Single UnityEngine.Rect::get_yMin()
extern void Rect_get_yMin_m31EDC3262BE39D2F6464B15397F882237E6158C3 (void);
// 0x00000125 System.Void UnityEngine.Rect::set_yMin(System.Single)
extern void Rect_set_yMin_m58C137C81F3D098CF81498964E1B5987882883A7 (void);
// 0x00000126 System.Single UnityEngine.Rect::get_xMax()
extern void Rect_get_xMax_mA16D7C3C2F30F8608719073ED79028C11CE90983 (void);
// 0x00000127 System.Void UnityEngine.Rect::set_xMax(System.Single)
extern void Rect_set_xMax_m1775041FCD5CA22C77D75CC780D158CD2B31CEAF (void);
// 0x00000128 System.Single UnityEngine.Rect::get_yMax()
extern void Rect_get_yMax_m8AA5E92C322AF3FF571330F00579DA864F33341B (void);
// 0x00000129 System.Void UnityEngine.Rect::set_yMax(System.Single)
extern void Rect_set_yMax_m4F1C5632CD4836853A22E979C810C279FBB20B95 (void);
// 0x0000012A System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern void Rect_Contains_mAD3D41C88795960F177088F847509C9DDA23B682 (void);
// 0x0000012B System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern void Rect_Contains_m5072228CE6251E7C754F227BA330F9ADA95C1495 (void);
// 0x0000012C UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern void Rect_OrderMinMax_m1BE37D433FE6B7FB0FB73652E166A4FB887214CD (void);
// 0x0000012D System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern void Rect_Overlaps_m2FE484659899E54C13772AB7D9E202239A637559 (void);
// 0x0000012E System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern void Rect_Overlaps_m4FFECCEAB3FBF23ED5A51B2E26220F035B942B4B (void);
// 0x0000012F System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern void Rect_op_Inequality_mAF9DC03779A7C3E1B430D7FFA797F2C4CEAD1FC7 (void);
// 0x00000130 System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern void Rect_op_Equality_mFBE3505CEDD6B73F66276E782C1B02E0E5633563 (void);
// 0x00000131 System.Int32 UnityEngine.Rect::GetHashCode()
extern void Rect_GetHashCode_mA23F5D7C299F7E05A0390DF2FA663F5A003799C6 (void);
// 0x00000132 System.Boolean UnityEngine.Rect::Equals(System.Object)
extern void Rect_Equals_m76E3B7E2E5CC43299C4BF4CB2EA9EF6E989E23E3 (void);
// 0x00000133 System.Boolean UnityEngine.Rect::Equals(UnityEngine.Rect)
extern void Rect_Equals_mC8430F80283016D0783FB6C4E7461BEED4B55C82 (void);
// 0x00000134 System.String UnityEngine.Rect::ToString()
extern void Rect_ToString_m045E7857658F27052323E301FBA3867AD13A6FE5 (void);
// 0x00000135 System.Void UnityEngine.RectOffset::.ctor()
extern void RectOffset__ctor_m4A29807F411591FC06BE9367167B8F417EF73828 (void);
// 0x00000136 System.Void UnityEngine.RectOffset::.ctor(System.Object,System.IntPtr)
extern void RectOffset__ctor_m23620FE61AAF476219462230C6839B86736B80BA (void);
// 0x00000137 System.Void UnityEngine.RectOffset::Finalize()
extern void RectOffset_Finalize_m69453D37706F46DD3A2A6F39A018D371A5E7072C (void);
// 0x00000138 System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void RectOffset__ctor_mF2A621DBA17A10660FEBE6237ACF4904DA6F9F29 (void);
// 0x00000139 System.String UnityEngine.RectOffset::ToString()
extern void RectOffset_ToString_m6852E54822C1FACE624F3306DD9DC71628E91F93 (void);
// 0x0000013A System.Void UnityEngine.RectOffset::Destroy()
extern void RectOffset_Destroy_mE1A6BDB23EC0B1A51AD5365CEF0055F7856AA533 (void);
// 0x0000013B System.IntPtr UnityEngine.RectOffset::InternalCreate()
extern void RectOffset_InternalCreate_m6638B085A0DA1CB1DA37B7C91CAC98DCF2CF7A16 (void);
// 0x0000013C System.Void UnityEngine.RectOffset::InternalDestroy(System.IntPtr)
extern void RectOffset_InternalDestroy_m4FAB64D6AECF15082E19BDC4F22913152D5C6FA1 (void);
// 0x0000013D System.Int32 UnityEngine.RectOffset::get_left()
extern void RectOffset_get_left_mA86EC00866C1940134873E3A1565A1F700DE67AD (void);
// 0x0000013E System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern void RectOffset_set_left_mDA6D192FBCB72EF77601FB4C2644A8E9BCBC610F (void);
// 0x0000013F System.Int32 UnityEngine.RectOffset::get_right()
extern void RectOffset_get_right_m9B05958C3C1B31F1FAB8675834A492C7208F6C96 (void);
// 0x00000140 System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern void RectOffset_set_right_m71CC11778FDBD3C7FD0B8CEFA272F53BF7311CD9 (void);
// 0x00000141 System.Int32 UnityEngine.RectOffset::get_top()
extern void RectOffset_get_top_mBA813D4147BFBC079933054018437F411B6B41E1 (void);
// 0x00000142 System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern void RectOffset_set_top_m4B0A86BF50785A9EEF101F370E0005B4D817CB6B (void);
// 0x00000143 System.Int32 UnityEngine.RectOffset::get_bottom()
extern void RectOffset_get_bottom_mE5162CADD266B59539E3EE1967EE9A74705E5632 (void);
// 0x00000144 System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern void RectOffset_set_bottom_m2D1FC60EBFC84557E7AB189A45F3AC2146E558DD (void);
// 0x00000145 System.Int32 UnityEngine.RectOffset::get_horizontal()
extern void RectOffset_get_horizontal_m9274B965D5D388F6F750D127B3E57F70DF0D89C1 (void);
// 0x00000146 System.Int32 UnityEngine.RectOffset::get_vertical()
extern void RectOffset_get_vertical_m89ED337C8D303C8994B2B056C05368E4286CFC5E (void);
// 0x00000147 System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Gizmos_DrawLine_m9515D59D2536571F4906A3C54E613A3986DFD892 (void);
// 0x00000148 System.Void UnityEngine.Gizmos::DrawWireSphere(UnityEngine.Vector3,System.Single)
extern void Gizmos_DrawWireSphere_mF6F2BC5CDF7B3F312FE9AB579CDC1C6B72154BCF (void);
// 0x00000149 System.Void UnityEngine.Gizmos::DrawSphere(UnityEngine.Vector3,System.Single)
extern void Gizmos_DrawSphere_mD6F94B7273D31AED07CCFCEA2E6C158A2039DB54 (void);
// 0x0000014A System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern void Gizmos_set_color_mFA6C199DF05FF557AEF662222CA60EC25DF54F28 (void);
// 0x0000014B System.Void UnityEngine.Gizmos::DrawLine_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Gizmos_DrawLine_Injected_m900F8B1C0DC1781B03C2CDFDB707F888A4A05909 (void);
// 0x0000014C System.Void UnityEngine.Gizmos::DrawWireSphere_Injected(UnityEngine.Vector3&,System.Single)
extern void Gizmos_DrawWireSphere_Injected_mD75DB358F03E22C59D95F1214870E227BB5B1FD2 (void);
// 0x0000014D System.Void UnityEngine.Gizmos::DrawSphere_Injected(UnityEngine.Vector3&,System.Single)
extern void Gizmos_DrawSphere_Injected_m1668DD5BCD5878EB77F3E7C7BCB8692C8A319453 (void);
// 0x0000014E System.Void UnityEngine.Gizmos::set_color_Injected(UnityEngine.Color&)
extern void Gizmos_set_color_Injected_m0BB0F6D3A3ECB3532D5E00FD3B3A3DDFB75590E8 (void);
// 0x0000014F System.Void UnityEngine.BeforeRenderHelper::Invoke()
extern void BeforeRenderHelper_Invoke_m5CADC9F58196CF3F11CB1203AEAF40003C7D4ADD (void);
// 0x00000150 System.Void UnityEngine.BeforeRenderHelper::.cctor()
extern void BeforeRenderHelper__cctor_mAF1DF30E8F7C2CE586303CAA41303230FE2DEB0D (void);
// 0x00000151 System.Void UnityEngine.Display::.ctor()
extern void Display__ctor_m1E66361E430C3698C98D242CEB6820E9B4FC7EB8 (void);
// 0x00000152 System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern void Display__ctor_mE84D2B0874035D8A772F4BAE78E0B8A2C7008E46 (void);
// 0x00000153 System.Int32 UnityEngine.Display::get_renderingWidth()
extern void Display_get_renderingWidth_mA02F65BF724686D7A0CD0C192954CA22592C3B12 (void);
// 0x00000154 System.Int32 UnityEngine.Display::get_renderingHeight()
extern void Display_get_renderingHeight_m1496BF9D66501280B4F75A31A515D8CF416838B0 (void);
// 0x00000155 System.Int32 UnityEngine.Display::get_systemWidth()
extern void Display_get_systemWidth_mA14AF2D3B017CF4BA2C2990DC2398E528AF83413 (void);
// 0x00000156 System.Int32 UnityEngine.Display::get_systemHeight()
extern void Display_get_systemHeight_m0D7950CB39015167C175634EF8A5E0C52FBF5EC7 (void);
// 0x00000157 UnityEngine.Vector3 UnityEngine.Display::RelativeMouseAt(UnityEngine.Vector3)
extern void Display_RelativeMouseAt_mABDA4BAC2C1B328A2C6A205D552AA5488BFFAA93 (void);
// 0x00000158 UnityEngine.Display UnityEngine.Display::get_main()
extern void Display_get_main_mDC0ED8AD60BF5BC3C83384E9C5131403E7033AFA (void);
// 0x00000159 System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern void Display_RecreateDisplayList_mA7E2B69AF4BD88A0C45B9A0BB7E1FFDDA5C60FE8 (void);
// 0x0000015A System.Void UnityEngine.Display::FireDisplaysUpdated()
extern void Display_FireDisplaysUpdated_m1655DF7464EA901E47BCDD6C3BBB9AFF52757D86 (void);
// 0x0000015B System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern void Display_GetSystemExtImpl_m946E5A2D11FC99291208F123B660978106C0B5C6 (void);
// 0x0000015C System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern void Display_GetRenderingExtImpl_m14405A2EC3C99F90D5CD080F3262BB7B4AC2BA49 (void);
// 0x0000015D System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern void Display_RelativeMouseAtImpl_mDC1A8A011C9B7FF7BC9A53A10FEDE8D817D68CDB (void);
// 0x0000015E System.Void UnityEngine.Display::.cctor()
extern void Display__cctor_mC1A1851D26DD51ECF2C09DBB1147A7CF05EEEC9D (void);
// 0x0000015F System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern void DisplaysUpdatedDelegate__ctor_m976C17F642CEF8A7F95FA4C414B17BF0EC025197 (void);
// 0x00000160 System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern void DisplaysUpdatedDelegate_Invoke_mBCC82165E169B27958A8FD4E5A90B83A108DAE89 (void);
// 0x00000161 System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void DisplaysUpdatedDelegate_BeginInvoke_m5DA06B0A901673F809EA597946702A73F9436BFF (void);
// 0x00000162 System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern void DisplaysUpdatedDelegate_EndInvoke_m470B70745AF2631D69A51A3883D774E9B49DD2E2 (void);
// 0x00000163 System.Int32 UnityEngine.Screen::get_width()
extern void Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3 (void);
// 0x00000164 System.Int32 UnityEngine.Screen::get_height()
extern void Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150 (void);
// 0x00000165 System.Single UnityEngine.Screen::get_dpi()
extern void Screen_get_dpi_m92A755DE9E23ABA717B5594F4F52AFB0FBEAC1D3 (void);
// 0x00000166 UnityEngine.ScreenOrientation UnityEngine.Screen::GetScreenOrientation()
extern void Screen_GetScreenOrientation_m8683EC395BB5ABBA6E9B69DE29818E9B1F82B469 (void);
// 0x00000167 UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern void Screen_get_orientation_m7977C7ECC2E3F22BB5DF7661951843FCC7E645B0 (void);
// 0x00000168 System.Boolean UnityEngine.Screen::get_fullScreen()
extern void Screen_get_fullScreen_m7C0CE5B87209523630EE751F54E3180AADD02E71 (void);
// 0x00000169 UnityEngine.FullScreenMode UnityEngine.Screen::get_fullScreenMode()
extern void Screen_get_fullScreenMode_m89BD87B3D0CB7D42B845E3DE1D36EFC5B3577115 (void);
// 0x0000016A UnityEngine.Rect UnityEngine.Screen::get_safeArea()
extern void Screen_get_safeArea_mEC4D352516A1270B474540D3A21B8798A28251F2 (void);
// 0x0000016B System.Void UnityEngine.Screen::get_safeArea_Injected(UnityEngine.Rect&)
extern void Screen_get_safeArea_Injected_m7451CB51BBC812FB534DC16210D407B17304A639 (void);
// 0x0000016C System.Int32 UnityEngine.Graphics::Internal_GetMaxDrawMeshInstanceCount()
extern void Graphics_Internal_GetMaxDrawMeshInstanceCount_mB5F6508A9AB7DBEBC192C6C7BDEF872295FB9801 (void);
// 0x0000016D UnityEngine.Rendering.GraphicsTier UnityEngine.Graphics::get_activeTier()
extern void Graphics_get_activeTier_mC69EEB666BDB6DD90E0DD89D18179DBB54C25141 (void);
// 0x0000016E System.Void UnityEngine.Graphics::set_activeTier(UnityEngine.Rendering.GraphicsTier)
extern void Graphics_set_activeTier_m043BD6073CA03FA27B074DFACB98D8BF715CB228 (void);
// 0x0000016F System.Void UnityEngine.Graphics::.cctor()
extern void Graphics__cctor_m87F7D324CC82B1B70ADFEC237B2BBEDC1767F1FF (void);
// 0x00000170 System.Void UnityEngine.LightProbes::Internal_CallTetrahedralizationCompletedFunction()
extern void LightProbes_Internal_CallTetrahedralizationCompletedFunction_m77B1485999595314268B2089930CEC245B77EEFA (void);
// 0x00000171 System.Void UnityEngine.LightProbes::Internal_CallNeedsRetetrahedralizationFunction()
extern void LightProbes_Internal_CallNeedsRetetrahedralizationFunction_m6AEFB1EE424473756C23793F85A095571BD8368C (void);
// 0x00000172 System.String UnityEngine.Resolution::ToString()
extern void Resolution_ToString_m42289CE0FC4ED41A9DC62B398F46F7954BC52F04 (void);
// 0x00000173 System.Int32 UnityEngine.QualitySettings::GetQualityLevel()
extern void QualitySettings_GetQualityLevel_mAD9FFDEBD51D12D914FDDDE4560E276A4D434A3B (void);
// 0x00000174 System.String[] UnityEngine.QualitySettings::get_names()
extern void QualitySettings_get_names_m3B4CFE04E36768B16C425A6837C9BA09F744CFCD (void);
// 0x00000175 UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern void QualitySettings_get_activeColorSpace_m13DBB3B679AA5D5CEA05C2B4517A1FDE1B2CF9B0 (void);
// 0x00000176 System.Single UnityEngine.TrailRenderer::get_time()
extern void TrailRenderer_get_time_mE4E051F908FAA39CD6D18658B943F5B28F625024 (void);
// 0x00000177 System.Void UnityEngine.TrailRenderer::set_time(System.Single)
extern void TrailRenderer_set_time_mC4B9324EBEF334A58C498806B478E1D296276359 (void);
// 0x00000178 System.Single UnityEngine.TrailRenderer::get_startWidth()
extern void TrailRenderer_get_startWidth_mA5EE0531BEBF2E69495047E0527B13DA1ADB0D9D (void);
// 0x00000179 System.Void UnityEngine.TrailRenderer::set_startWidth(System.Single)
extern void TrailRenderer_set_startWidth_mA60DF0277CDD09E9C98BDEC76A670BA428FF6872 (void);
// 0x0000017A System.Single UnityEngine.TrailRenderer::get_endWidth()
extern void TrailRenderer_get_endWidth_m7AF079A0351C238CBE24A21EEC20241365269C86 (void);
// 0x0000017B System.Void UnityEngine.TrailRenderer::set_endWidth(System.Single)
extern void TrailRenderer_set_endWidth_m50CCB4931A6019B0D24BD18C9C349FF2FAFF42F6 (void);
// 0x0000017C System.Void UnityEngine.LineRenderer::SetColors(UnityEngine.Color,UnityEngine.Color)
extern void LineRenderer_SetColors_mAE8931C4C8A8FE7A1E0CE71B8E69832DA83BC773 (void);
// 0x0000017D System.Void UnityEngine.LineRenderer::set_startColor(UnityEngine.Color)
extern void LineRenderer_set_startColor_m033D0E05F3FC475008FC6CC8008763ABEEFC8D1C (void);
// 0x0000017E System.Void UnityEngine.LineRenderer::set_endColor(UnityEngine.Color)
extern void LineRenderer_set_endColor_mC12E3B60CA0476CC3916C795721B71A4C7234FE3 (void);
// 0x0000017F System.Void UnityEngine.LineRenderer::set_positionCount(System.Int32)
extern void LineRenderer_set_positionCount_mFE47D750AF310FE3073C3F7A46FF116354EDA49F (void);
// 0x00000180 System.Void UnityEngine.LineRenderer::SetPosition(System.Int32,UnityEngine.Vector3)
extern void LineRenderer_SetPosition_m34B4D85846C46404658B07BE930117A7A838F59B (void);
// 0x00000181 System.Void UnityEngine.LineRenderer::set_startColor_Injected(UnityEngine.Color&)
extern void LineRenderer_set_startColor_Injected_m7405704CBED43AD225B992978E7DBB2DE70C98D3 (void);
// 0x00000182 System.Void UnityEngine.LineRenderer::set_endColor_Injected(UnityEngine.Color&)
extern void LineRenderer_set_endColor_Injected_m57D27AC9DF161F86E847A5F76BE4745ED77B6178 (void);
// 0x00000183 System.Void UnityEngine.LineRenderer::SetPosition_Injected(System.Int32,UnityEngine.Vector3&)
extern void LineRenderer_SetPosition_Injected_mFB6BFFC7C0B16DFAFF3CCCDF327677EF9B163719 (void);
// 0x00000184 System.Void UnityEngine.MaterialPropertyBlock::SetColorImpl(System.Int32,UnityEngine.Color)
extern void MaterialPropertyBlock_SetColorImpl_mB67F26F2146332B78205D2CCA31053316C7D728D (void);
// 0x00000185 System.IntPtr UnityEngine.MaterialPropertyBlock::CreateImpl()
extern void MaterialPropertyBlock_CreateImpl_m24B31B00E85647888CE23025A887F670066C167A (void);
// 0x00000186 System.Void UnityEngine.MaterialPropertyBlock::DestroyImpl(System.IntPtr)
extern void MaterialPropertyBlock_DestroyImpl_m8BE5B07A016787E1B841496630E77F31243EF109 (void);
// 0x00000187 System.Void UnityEngine.MaterialPropertyBlock::.ctor()
extern void MaterialPropertyBlock__ctor_m9055A333A5DA8CC70CC3D837BD59B54C313D39F3 (void);
// 0x00000188 System.Void UnityEngine.MaterialPropertyBlock::Finalize()
extern void MaterialPropertyBlock_Finalize_m26F82696BBE81EC8910C0991AD7CFD9B4B26DA3D (void);
// 0x00000189 System.Void UnityEngine.MaterialPropertyBlock::Dispose()
extern void MaterialPropertyBlock_Dispose_m3DED1CD5B678C080B844E4B8CB9F32FDC50041ED (void);
// 0x0000018A System.Void UnityEngine.MaterialPropertyBlock::SetColor(System.String,UnityEngine.Color)
extern void MaterialPropertyBlock_SetColor_m2821A6737D983B1EF0260490CB09BA05F1B6411B (void);
// 0x0000018B System.Void UnityEngine.MaterialPropertyBlock::SetColorImpl_Injected(System.Int32,UnityEngine.Color&)
extern void MaterialPropertyBlock_SetColorImpl_Injected_mA20E5DD8EB1E5BD706F46362B3C0C01FB2F98CEA (void);
// 0x0000018C UnityEngine.Material UnityEngine.Renderer::GetMaterial()
extern void Renderer_GetMaterial_m370ADC0227BC648BEFAAF85AFB09722E8B20024B (void);
// 0x0000018D UnityEngine.Material UnityEngine.Renderer::GetSharedMaterial()
extern void Renderer_GetSharedMaterial_m3819AC6D141381FD4C99793E855D2ACED6FB3071 (void);
// 0x0000018E System.Void UnityEngine.Renderer::SetMaterial(UnityEngine.Material)
extern void Renderer_SetMaterial_m68B4F4BB5C58FB88AE7AF3829B532869C781FD0F (void);
// 0x0000018F System.Void UnityEngine.Renderer::Internal_SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern void Renderer_Internal_SetPropertyBlock_m92C716E9C4DB64FE53D98F1BE4981FA2644AC43B (void);
// 0x00000190 System.Void UnityEngine.Renderer::Internal_GetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern void Renderer_Internal_GetPropertyBlock_mB376DBE5A7423EBFC8E01708D6D095308C3BAF96 (void);
// 0x00000191 System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern void Renderer_SetPropertyBlock_m1B999AB9B425587EF44CF1CB83CDE0A191F76C40 (void);
// 0x00000192 System.Void UnityEngine.Renderer::GetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern void Renderer_GetPropertyBlock_mCD279F8A7CEB56ABB9EF9D150103FB1C4FB3CE8C (void);
// 0x00000193 System.Boolean UnityEngine.Renderer::get_enabled()
extern void Renderer_get_enabled_m40E07BB15DA58D2EF6F6796C6778163107DD7E1B (void);
// 0x00000194 System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern void Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303 (void);
// 0x00000195 System.Void UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)
extern void Renderer_set_shadowCastingMode_mC7E601EE9B32B63097B216C78ED4F854B0AF21EC (void);
// 0x00000196 System.Void UnityEngine.Renderer::set_receiveShadows(System.Boolean)
extern void Renderer_set_receiveShadows_mD2BD2FF58156E328677EAE5E175D2069BC2925A0 (void);
// 0x00000197 System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern void Renderer_get_sortingLayerID_m2E204E68869EDA3176C334AE1C62219F380A5D85 (void);
// 0x00000198 System.Void UnityEngine.Renderer::set_sortingLayerID(System.Int32)
extern void Renderer_set_sortingLayerID_mF2A52A5697C3A9288DEAC949F54C4CCA97716526 (void);
// 0x00000199 System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern void Renderer_get_sortingOrder_m33DD50ED293AA672FDAD862B4A4865666B5FEBAF (void);
// 0x0000019A System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
extern void Renderer_set_sortingOrder_mBCE1207CDB46CB6BA4583B9C3FB4A2D28DC27D81 (void);
// 0x0000019B UnityEngine.Material UnityEngine.Renderer::get_material()
extern void Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617 (void);
// 0x0000019C System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern void Renderer_set_material_mB4988AD6A93C7FDACC4C07A99D1DAC23D10C0344 (void);
// 0x0000019D UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern void Renderer_get_sharedMaterial_m2BE9FF3D269968F2E323AC60EFBBCC0B26E7E6F9 (void);
// 0x0000019E System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
extern void Renderer_set_sharedMaterial_mC94A354D9B0FCA081754A7CB51AEE5A9AD3946A3 (void);
// 0x0000019F UnityEngine.Rendering.ShaderHardwareTier UnityEngine.Shader::get_globalShaderHardwareTier()
extern void Shader_get_globalShaderHardwareTier_mDD5E88C6D806A290A68D4681AA1D5AC530D20DC4 (void);
// 0x000001A0 System.Void UnityEngine.Shader::set_globalShaderHardwareTier(UnityEngine.Rendering.ShaderHardwareTier)
extern void Shader_set_globalShaderHardwareTier_m59BCE1D4304BA2C18CE1950E000BA092066EF7EB (void);
// 0x000001A1 UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern void Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B (void);
// 0x000001A2 UnityEngine.Shader UnityEngine.Shader::FindBuiltin(System.String)
extern void Shader_FindBuiltin_m9BDF3202A668A79B3F46C695384E37A8FB5F8A9C (void);
// 0x000001A3 System.Int32 UnityEngine.Shader::get_maximumLOD()
extern void Shader_get_maximumLOD_m080015C047D4B64E41630BABA1C332D7B96896E3 (void);
// 0x000001A4 System.Void UnityEngine.Shader::set_maximumLOD(System.Int32)
extern void Shader_set_maximumLOD_mAF8E3615BB40E1381CDC4110D20D3BB866AFDC4B (void);
// 0x000001A5 System.Int32 UnityEngine.Shader::get_globalMaximumLOD()
extern void Shader_get_globalMaximumLOD_m74C150DDA16A58ABAE0480CAC1F7E8EA754236A4 (void);
// 0x000001A6 System.Void UnityEngine.Shader::set_globalMaximumLOD(System.Int32)
extern void Shader_set_globalMaximumLOD_m551638F9EA54AE00FB59C055B2BD4E414D2199AE (void);
// 0x000001A7 System.Boolean UnityEngine.Shader::get_isSupported()
extern void Shader_get_isSupported_m3660681289CDFE742D399C3030A6CF5C4D8B030D (void);
// 0x000001A8 System.String UnityEngine.Shader::get_globalRenderPipeline()
extern void Shader_get_globalRenderPipeline_m6DAF7BBDDB57FC9ED30D69047D79B810D29DC973 (void);
// 0x000001A9 System.Void UnityEngine.Shader::set_globalRenderPipeline(System.String)
extern void Shader_set_globalRenderPipeline_mDD57BC9253ECF13404F987D52A55A288817F3B21 (void);
// 0x000001AA System.Void UnityEngine.Shader::EnableKeyword(System.String)
extern void Shader_EnableKeyword_m600614EB1D434CA8ECFC8DAA5BC6E2ED4E55CD9F (void);
// 0x000001AB System.Void UnityEngine.Shader::DisableKeyword(System.String)
extern void Shader_DisableKeyword_m2D15FB4C26535D9AF45445B4149EADD4BF68C701 (void);
// 0x000001AC System.Boolean UnityEngine.Shader::IsKeywordEnabled(System.String)
extern void Shader_IsKeywordEnabled_m08B349CDC3898D74679137BA9AF38FECE72F6119 (void);
// 0x000001AD System.Int32 UnityEngine.Shader::get_renderQueue()
extern void Shader_get_renderQueue_mF5AC966B7BDDF0FEDFD43EAC9EFE48619AF24F27 (void);
// 0x000001AE UnityEngine.DisableBatchingType UnityEngine.Shader::get_disableBatching()
extern void Shader_get_disableBatching_m93E8EC7BA418878FB5C194B74B29EBC50EC5CDC3 (void);
// 0x000001AF System.Void UnityEngine.Shader::WarmupAllShaders()
extern void Shader_WarmupAllShaders_m5C6D779B51EB709A943E8A56588A875CBEBACCF5 (void);
// 0x000001B0 System.Int32 UnityEngine.Shader::TagToID(System.String)
extern void Shader_TagToID_m0597E33DAA0FD822B2E10881E7740E42222A8392 (void);
// 0x000001B1 System.String UnityEngine.Shader::IDToTag(System.Int32)
extern void Shader_IDToTag_mEE9CFFD973795B490BFEDC2B1ACA06FB0644B349 (void);
// 0x000001B2 System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern void Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45 (void);
// 0x000001B3 UnityEngine.Shader UnityEngine.Shader::GetDependency(System.String)
extern void Shader_GetDependency_m306DE9EC247EA9B015E626EE1AD126F61E643017 (void);
// 0x000001B4 System.Int32 UnityEngine.Shader::get_passCount()
extern void Shader_get_passCount_m3A6E01161AB184A36A14ECC0DAB437679B8652E8 (void);
// 0x000001B5 UnityEngine.Rendering.ShaderTagId UnityEngine.Shader::FindPassTagValue(System.Int32,UnityEngine.Rendering.ShaderTagId)
extern void Shader_FindPassTagValue_m284633DA2F70B956526BE4B4044AFEE67AA5DB73 (void);
// 0x000001B6 System.Int32 UnityEngine.Shader::Internal_FindPassTagValue(System.Int32,System.Int32)
extern void Shader_Internal_FindPassTagValue_mF5FA12EAC420E6FA37C5812D264D604C24C8ED22 (void);
// 0x000001B7 System.Void UnityEngine.Shader::SetGlobalFloatImpl(System.Int32,System.Single)
extern void Shader_SetGlobalFloatImpl_m34F78C11132FA32ED2550F0C2641FF0E026CF633 (void);
// 0x000001B8 System.Void UnityEngine.Shader::SetGlobalVectorImpl(System.Int32,UnityEngine.Vector4)
extern void Shader_SetGlobalVectorImpl_m2741D71F920CDE4F2416EFA61A3B44CA258348B7 (void);
// 0x000001B9 System.Void UnityEngine.Shader::SetGlobalMatrixImpl(System.Int32,UnityEngine.Matrix4x4)
extern void Shader_SetGlobalMatrixImpl_mA5166FA119796B15645170AF95169C532C75B967 (void);
// 0x000001BA System.Void UnityEngine.Shader::SetGlobalTextureImpl(System.Int32,UnityEngine.Texture)
extern void Shader_SetGlobalTextureImpl_m8098302027BFD1DC80EB97B82CD1FB97DEC62C20 (void);
// 0x000001BB System.Void UnityEngine.Shader::SetGlobalRenderTextureImpl(System.Int32,UnityEngine.RenderTexture,UnityEngine.Rendering.RenderTextureSubElement)
extern void Shader_SetGlobalRenderTextureImpl_mEEB1AEE0DAFE12880EAB7AC7ED52CC7E8608867B (void);
// 0x000001BC System.Void UnityEngine.Shader::SetGlobalBufferImpl(System.Int32,UnityEngine.ComputeBuffer)
extern void Shader_SetGlobalBufferImpl_m4B040860BDA55685EB66B51A8D01D051EFFB6B76 (void);
// 0x000001BD System.Void UnityEngine.Shader::SetGlobalConstantBufferImpl(System.Int32,UnityEngine.ComputeBuffer,System.Int32,System.Int32)
extern void Shader_SetGlobalConstantBufferImpl_m2D90DBDD146284348761E022A4850475D71FAFEE (void);
// 0x000001BE System.Single UnityEngine.Shader::GetGlobalFloatImpl(System.Int32)
extern void Shader_GetGlobalFloatImpl_mFB65A24295950223ADC2BCD45744D43156C438A7 (void);
// 0x000001BF UnityEngine.Vector4 UnityEngine.Shader::GetGlobalVectorImpl(System.Int32)
extern void Shader_GetGlobalVectorImpl_m76D4FDC07D1B6BD4ADC83B0A217B2057CED58774 (void);
// 0x000001C0 UnityEngine.Matrix4x4 UnityEngine.Shader::GetGlobalMatrixImpl(System.Int32)
extern void Shader_GetGlobalMatrixImpl_m5917481374611DD1A940F9EF5761792CBFFD3872 (void);
// 0x000001C1 UnityEngine.Texture UnityEngine.Shader::GetGlobalTextureImpl(System.Int32)
extern void Shader_GetGlobalTextureImpl_mC7DABCD2E0637DFF8E176B25537EC4D7F23A5763 (void);
// 0x000001C2 System.Void UnityEngine.Shader::SetGlobalFloatArrayImpl(System.Int32,System.Single[],System.Int32)
extern void Shader_SetGlobalFloatArrayImpl_m5A63AB45BA8B47D2C7398243D202662630F89226 (void);
// 0x000001C3 System.Void UnityEngine.Shader::SetGlobalVectorArrayImpl(System.Int32,UnityEngine.Vector4[],System.Int32)
extern void Shader_SetGlobalVectorArrayImpl_m5693BAD05E34CD0899BD29C70331A0525302C331 (void);
// 0x000001C4 System.Void UnityEngine.Shader::SetGlobalMatrixArrayImpl(System.Int32,UnityEngine.Matrix4x4[],System.Int32)
extern void Shader_SetGlobalMatrixArrayImpl_m2BB06786CE8286F687F606998D89420CD1927F85 (void);
// 0x000001C5 System.Single[] UnityEngine.Shader::GetGlobalFloatArrayImpl(System.Int32)
extern void Shader_GetGlobalFloatArrayImpl_mA794C95DD54B4681E610DD8890816282CBC5BEF1 (void);
// 0x000001C6 UnityEngine.Vector4[] UnityEngine.Shader::GetGlobalVectorArrayImpl(System.Int32)
extern void Shader_GetGlobalVectorArrayImpl_mEDA9F74253E7AD8D3239A7633940F7958D581E02 (void);
// 0x000001C7 UnityEngine.Matrix4x4[] UnityEngine.Shader::GetGlobalMatrixArrayImpl(System.Int32)
extern void Shader_GetGlobalMatrixArrayImpl_m521CAE006DD2389DF1E0B8856002E0ADE87188C4 (void);
// 0x000001C8 System.Int32 UnityEngine.Shader::GetGlobalFloatArrayCountImpl(System.Int32)
extern void Shader_GetGlobalFloatArrayCountImpl_m897DE0F5133C83F3D7D91C2EC406B2676127502D (void);
// 0x000001C9 System.Int32 UnityEngine.Shader::GetGlobalVectorArrayCountImpl(System.Int32)
extern void Shader_GetGlobalVectorArrayCountImpl_m44B36055836F94BB510DAAABDF447A9B95567656 (void);
// 0x000001CA System.Int32 UnityEngine.Shader::GetGlobalMatrixArrayCountImpl(System.Int32)
extern void Shader_GetGlobalMatrixArrayCountImpl_mE5467D37F5B11CAEC4808F2FE3C971E69C110ED7 (void);
// 0x000001CB System.Void UnityEngine.Shader::ExtractGlobalFloatArrayImpl(System.Int32,System.Single[])
extern void Shader_ExtractGlobalFloatArrayImpl_m77E1FEE04B921C7CE6F3A3855012E5DDD6E41D9A (void);
// 0x000001CC System.Void UnityEngine.Shader::ExtractGlobalVectorArrayImpl(System.Int32,UnityEngine.Vector4[])
extern void Shader_ExtractGlobalVectorArrayImpl_m9E5A0047DA98EE4833223AB0247FF260434783D3 (void);
// 0x000001CD System.Void UnityEngine.Shader::ExtractGlobalMatrixArrayImpl(System.Int32,UnityEngine.Matrix4x4[])
extern void Shader_ExtractGlobalMatrixArrayImpl_m11B72140F9CCE8D54441219EA94E7B030A18C0B3 (void);
// 0x000001CE System.Void UnityEngine.Shader::SetGlobalFloatArray(System.Int32,System.Single[],System.Int32)
extern void Shader_SetGlobalFloatArray_m17B396ADAA2740C42092C790FAE3159CD96DBAA6 (void);
// 0x000001CF System.Void UnityEngine.Shader::SetGlobalVectorArray(System.Int32,UnityEngine.Vector4[],System.Int32)
extern void Shader_SetGlobalVectorArray_mC7D6E525AB07A7EFF07F60F8B4528B2E7798B4A8 (void);
// 0x000001D0 System.Void UnityEngine.Shader::SetGlobalMatrixArray(System.Int32,UnityEngine.Matrix4x4[],System.Int32)
extern void Shader_SetGlobalMatrixArray_mD98B757921B8F2F40D017BA28D51D894FB0CA5E0 (void);
// 0x000001D1 System.Void UnityEngine.Shader::ExtractGlobalFloatArray(System.Int32,System.Collections.Generic.List`1<System.Single>)
extern void Shader_ExtractGlobalFloatArray_m05567982FD164CD7B14DDC97F2CFF67E0FBA7F8B (void);
// 0x000001D2 System.Void UnityEngine.Shader::ExtractGlobalVectorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Shader_ExtractGlobalVectorArray_m8C602323460F0AAE19135ECA460FDAED67C8B62B (void);
// 0x000001D3 System.Void UnityEngine.Shader::ExtractGlobalMatrixArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Shader_ExtractGlobalMatrixArray_mC77E2FBA3E0E5285E58F187EA30F1E42BCA3C625 (void);
// 0x000001D4 System.Void UnityEngine.Shader::SetGlobalFloat(System.String,System.Single)
extern void Shader_SetGlobalFloat_m9020F5CF5BAC69941EB49C6457AAD9BF63D819AE (void);
// 0x000001D5 System.Void UnityEngine.Shader::SetGlobalFloat(System.Int32,System.Single)
extern void Shader_SetGlobalFloat_mBD7EA0478EDAD67D0AF342A54E97DC8495E5F209 (void);
// 0x000001D6 System.Void UnityEngine.Shader::SetGlobalInt(System.String,System.Int32)
extern void Shader_SetGlobalInt_mF3BD98838D66A75CF6090AB1BD541E491B496E55 (void);
// 0x000001D7 System.Void UnityEngine.Shader::SetGlobalInt(System.Int32,System.Int32)
extern void Shader_SetGlobalInt_mE182A541FAEDAB51EA0214AC9C4FCB7447EA3AA4 (void);
// 0x000001D8 System.Void UnityEngine.Shader::SetGlobalVector(System.String,UnityEngine.Vector4)
extern void Shader_SetGlobalVector_m831AF6E05F7ADEBE49AC4C6EE043E10B86900B76 (void);
// 0x000001D9 System.Void UnityEngine.Shader::SetGlobalVector(System.Int32,UnityEngine.Vector4)
extern void Shader_SetGlobalVector_m95C42CE1CC56A5BD792C48C60EDD1DF220046566 (void);
// 0x000001DA System.Void UnityEngine.Shader::SetGlobalColor(System.String,UnityEngine.Color)
extern void Shader_SetGlobalColor_mF06BC77590D46F1031713BC3FCD614C5E8A0FB18 (void);
// 0x000001DB System.Void UnityEngine.Shader::SetGlobalColor(System.Int32,UnityEngine.Color)
extern void Shader_SetGlobalColor_m5BD0BE3896674DD1DB99770EF300A32CAF25D4A1 (void);
// 0x000001DC System.Void UnityEngine.Shader::SetGlobalMatrix(System.String,UnityEngine.Matrix4x4)
extern void Shader_SetGlobalMatrix_m4581B7990AB6C854301A072E1A15427117F20D1B (void);
// 0x000001DD System.Void UnityEngine.Shader::SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4)
extern void Shader_SetGlobalMatrix_mE9F00E107B245DEFB3210F00228F72F2BF2B8112 (void);
// 0x000001DE System.Void UnityEngine.Shader::SetGlobalTexture(System.String,UnityEngine.Texture)
extern void Shader_SetGlobalTexture_m17D5E35DC746DB65488FE0772F87991ADD63486F (void);
// 0x000001DF System.Void UnityEngine.Shader::SetGlobalTexture(System.Int32,UnityEngine.Texture)
extern void Shader_SetGlobalTexture_m102985DD1111707707723F56A005BFDE53F42B39 (void);
// 0x000001E0 System.Void UnityEngine.Shader::SetGlobalTexture(System.String,UnityEngine.RenderTexture,UnityEngine.Rendering.RenderTextureSubElement)
extern void Shader_SetGlobalTexture_mB0D3AE5A10E57F8F7921EC5234CCCBFB595557D6 (void);
// 0x000001E1 System.Void UnityEngine.Shader::SetGlobalTexture(System.Int32,UnityEngine.RenderTexture,UnityEngine.Rendering.RenderTextureSubElement)
extern void Shader_SetGlobalTexture_mF15125C706FA670931BC9943C6832C37748E2214 (void);
// 0x000001E2 System.Void UnityEngine.Shader::SetGlobalBuffer(System.String,UnityEngine.ComputeBuffer)
extern void Shader_SetGlobalBuffer_m315E6890B93B2FAE0BD3535F7E2ECF302A399683 (void);
// 0x000001E3 System.Void UnityEngine.Shader::SetGlobalBuffer(System.Int32,UnityEngine.ComputeBuffer)
extern void Shader_SetGlobalBuffer_mACEA0E5EB8C2DAF5DDAC28F464EF3BBB320BD858 (void);
// 0x000001E4 System.Void UnityEngine.Shader::SetGlobalConstantBuffer(System.Int32,UnityEngine.ComputeBuffer,System.Int32,System.Int32)
extern void Shader_SetGlobalConstantBuffer_mA7D527084752F14F6449308BF480B8919996FAB2 (void);
// 0x000001E5 System.Void UnityEngine.Shader::SetGlobalFloatArray(System.String,System.Collections.Generic.List`1<System.Single>)
extern void Shader_SetGlobalFloatArray_m3FBC628C38FEE8E1AA834BF37804E30E9412EBDD (void);
// 0x000001E6 System.Void UnityEngine.Shader::SetGlobalFloatArray(System.Int32,System.Collections.Generic.List`1<System.Single>)
extern void Shader_SetGlobalFloatArray_mD90F003C6B47CEF24C739B4069D42C3452FB9502 (void);
// 0x000001E7 System.Void UnityEngine.Shader::SetGlobalFloatArray(System.String,System.Single[])
extern void Shader_SetGlobalFloatArray_m4CA746406152C91960BA38BD4FD49BACC7E7862B (void);
// 0x000001E8 System.Void UnityEngine.Shader::SetGlobalFloatArray(System.Int32,System.Single[])
extern void Shader_SetGlobalFloatArray_m7BFD24A65210FA68D724FBBC004711D85B3ED819 (void);
// 0x000001E9 System.Void UnityEngine.Shader::SetGlobalVectorArray(System.String,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Shader_SetGlobalVectorArray_m8FB32F66DD342DBB54F65875C67E4E89713791E2 (void);
// 0x000001EA System.Void UnityEngine.Shader::SetGlobalVectorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Shader_SetGlobalVectorArray_m83822C9305AFC915419898D4D9D6F0EE720D5E4F (void);
// 0x000001EB System.Void UnityEngine.Shader::SetGlobalVectorArray(System.String,UnityEngine.Vector4[])
extern void Shader_SetGlobalVectorArray_m6269EF99AB8565A62C18E3D6FB90423B26FF0978 (void);
// 0x000001EC System.Void UnityEngine.Shader::SetGlobalVectorArray(System.Int32,UnityEngine.Vector4[])
extern void Shader_SetGlobalVectorArray_m0FD98F7C84F7C1A1D992DE5334ABE24C5B315FD6 (void);
// 0x000001ED System.Void UnityEngine.Shader::SetGlobalMatrixArray(System.String,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Shader_SetGlobalMatrixArray_m6E0E4533E809D9298640CB950A34B74F2EB23846 (void);
// 0x000001EE System.Void UnityEngine.Shader::SetGlobalMatrixArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Shader_SetGlobalMatrixArray_mBD4D97D8042FD63970C395331FC8A429953147ED (void);
// 0x000001EF System.Void UnityEngine.Shader::SetGlobalMatrixArray(System.String,UnityEngine.Matrix4x4[])
extern void Shader_SetGlobalMatrixArray_mA666380B8FA7305C4DD8D83093104F71166984F0 (void);
// 0x000001F0 System.Void UnityEngine.Shader::SetGlobalMatrixArray(System.Int32,UnityEngine.Matrix4x4[])
extern void Shader_SetGlobalMatrixArray_m65CA9E537955C5086BFC454113F8A40395F12411 (void);
// 0x000001F1 System.Single UnityEngine.Shader::GetGlobalFloat(System.String)
extern void Shader_GetGlobalFloat_mE940743B54E43F43067FF9F0F10CB69C7B644B45 (void);
// 0x000001F2 System.Single UnityEngine.Shader::GetGlobalFloat(System.Int32)
extern void Shader_GetGlobalFloat_m944FF3CD8D141168C4DD599F057CED2542C7DFF8 (void);
// 0x000001F3 System.Int32 UnityEngine.Shader::GetGlobalInt(System.String)
extern void Shader_GetGlobalInt_m47D7011151555FC06703452ED43D0E1183C8F8CC (void);
// 0x000001F4 System.Int32 UnityEngine.Shader::GetGlobalInt(System.Int32)
extern void Shader_GetGlobalInt_mD12FAD45C407CE6C847006D4936BFAC3080BF180 (void);
// 0x000001F5 UnityEngine.Vector4 UnityEngine.Shader::GetGlobalVector(System.String)
extern void Shader_GetGlobalVector_mF81172171AECD963E1621761B60F075A686A4E01 (void);
// 0x000001F6 UnityEngine.Vector4 UnityEngine.Shader::GetGlobalVector(System.Int32)
extern void Shader_GetGlobalVector_m730654D4E436A814FB4561204E9AB6214996B703 (void);
// 0x000001F7 UnityEngine.Color UnityEngine.Shader::GetGlobalColor(System.String)
extern void Shader_GetGlobalColor_m4D2B41A0CA067CDB26C3EED29BDE9725079ABE91 (void);
// 0x000001F8 UnityEngine.Color UnityEngine.Shader::GetGlobalColor(System.Int32)
extern void Shader_GetGlobalColor_m01C78E5F65C7B1EF0B1306413EA6B09BE3A5B267 (void);
// 0x000001F9 UnityEngine.Matrix4x4 UnityEngine.Shader::GetGlobalMatrix(System.String)
extern void Shader_GetGlobalMatrix_m2BFF6E202370174A158395805493BB42BB312AFD (void);
// 0x000001FA UnityEngine.Matrix4x4 UnityEngine.Shader::GetGlobalMatrix(System.Int32)
extern void Shader_GetGlobalMatrix_m89C2CFAFB98BEF2064D4F6B441025CEAB1A59A37 (void);
// 0x000001FB UnityEngine.Texture UnityEngine.Shader::GetGlobalTexture(System.String)
extern void Shader_GetGlobalTexture_m36F80A6B474B78FAD2D1F199E1215E7BE6D7C1D8 (void);
// 0x000001FC UnityEngine.Texture UnityEngine.Shader::GetGlobalTexture(System.Int32)
extern void Shader_GetGlobalTexture_m5459D120BE6C734C8736AACF6AF4C5A2C1A9A11D (void);
// 0x000001FD System.Single[] UnityEngine.Shader::GetGlobalFloatArray(System.String)
extern void Shader_GetGlobalFloatArray_mEBC1DB9F2AD755F2D9DB208C803E52CAECFBD9F3 (void);
// 0x000001FE System.Single[] UnityEngine.Shader::GetGlobalFloatArray(System.Int32)
extern void Shader_GetGlobalFloatArray_m2B08CA26087D0253767995EA78A70E2142DB383D (void);
// 0x000001FF UnityEngine.Vector4[] UnityEngine.Shader::GetGlobalVectorArray(System.String)
extern void Shader_GetGlobalVectorArray_mDE097DE9F3AF3C1F4F3F48AA37AF587451D01FCC (void);
// 0x00000200 UnityEngine.Vector4[] UnityEngine.Shader::GetGlobalVectorArray(System.Int32)
extern void Shader_GetGlobalVectorArray_mF7ADDA7E0198A612656CF773C9B9F2DB1F3714A0 (void);
// 0x00000201 UnityEngine.Matrix4x4[] UnityEngine.Shader::GetGlobalMatrixArray(System.String)
extern void Shader_GetGlobalMatrixArray_mD711782B6D5490E6EC7EB9F4FAA3642E37C7590C (void);
// 0x00000202 UnityEngine.Matrix4x4[] UnityEngine.Shader::GetGlobalMatrixArray(System.Int32)
extern void Shader_GetGlobalMatrixArray_m5C9825252F22927C7FE7C3DB715F28DFDF18FFAA (void);
// 0x00000203 System.Void UnityEngine.Shader::GetGlobalFloatArray(System.String,System.Collections.Generic.List`1<System.Single>)
extern void Shader_GetGlobalFloatArray_m6B4DDEC1D47B9EF24A83C9130B3020D9F1FB1ED9 (void);
// 0x00000204 System.Void UnityEngine.Shader::GetGlobalFloatArray(System.Int32,System.Collections.Generic.List`1<System.Single>)
extern void Shader_GetGlobalFloatArray_m069F43C4DA01E6BA6A19B842BD62B137E527C291 (void);
// 0x00000205 System.Void UnityEngine.Shader::GetGlobalVectorArray(System.String,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Shader_GetGlobalVectorArray_mF9D1A26BBF4E4AE2CCA32ABB8888E875A6728F5E (void);
// 0x00000206 System.Void UnityEngine.Shader::GetGlobalVectorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Shader_GetGlobalVectorArray_mE4C492CCB7832557324304F45E7942AD1FA2B1BC (void);
// 0x00000207 System.Void UnityEngine.Shader::GetGlobalMatrixArray(System.String,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Shader_GetGlobalMatrixArray_m8F5094B02BB72D084FF8DF9FB500A0394DE8E657 (void);
// 0x00000208 System.Void UnityEngine.Shader::GetGlobalMatrixArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Shader_GetGlobalMatrixArray_mD196A4D3397F1922D85B9A28995D9B9C8D7DD9EF (void);
// 0x00000209 System.Void UnityEngine.Shader::.ctor()
extern void Shader__ctor_m6D011DE6D578D5F19FBF7EAED326C7209C419E21 (void);
// 0x0000020A System.String UnityEngine.Shader::GetPropertyName(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyName_m09645E39C81AFC9F4CBEA64684106A32CCC24176 (void);
// 0x0000020B System.Int32 UnityEngine.Shader::GetPropertyNameId(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyNameId_mD26AD80F64D495DCF7F14AD52D0B25BDF4F92C0A (void);
// 0x0000020C UnityEngine.Rendering.ShaderPropertyType UnityEngine.Shader::GetPropertyType(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyType_m91AB65D05495C47933FE1469979DE7F0EB8ADF8B (void);
// 0x0000020D System.String UnityEngine.Shader::GetPropertyDescription(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyDescription_m43E12278DC828BBA8098EB97F9F8278BB821DA22 (void);
// 0x0000020E UnityEngine.Rendering.ShaderPropertyFlags UnityEngine.Shader::GetPropertyFlags(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyFlags_m953EAAE229BEEADACA566DD73ADD90B8407E1B83 (void);
// 0x0000020F System.String[] UnityEngine.Shader::GetPropertyAttributes(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyAttributes_m55EF607DECD7E26AEEC28D0D92FF2044519F36E5 (void);
// 0x00000210 UnityEngine.Vector4 UnityEngine.Shader::GetPropertyDefaultValue(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyDefaultValue_m979D2AFE7FFC95913F18D5139F38D2B2EC186FD0 (void);
// 0x00000211 UnityEngine.Rendering.TextureDimension UnityEngine.Shader::GetPropertyTextureDimension(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyTextureDimension_m04A335C5DA03B97B23510A37305212A05F451A7F (void);
// 0x00000212 System.String UnityEngine.Shader::GetPropertyTextureDefaultName(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyTextureDefaultName_mBED1CF1701CA202FD6339DF42A75E339BBD17AE2 (void);
// 0x00000213 System.Void UnityEngine.Shader::CheckPropertyIndex(UnityEngine.Shader,System.Int32)
extern void Shader_CheckPropertyIndex_mECA8A1E1DD1E483E9DD156273FA2AB407CF100FA (void);
// 0x00000214 System.Int32 UnityEngine.Shader::GetPropertyCount()
extern void Shader_GetPropertyCount_m99EF95941D764508220EE397FFB1D94887C04C16 (void);
// 0x00000215 System.Int32 UnityEngine.Shader::FindPropertyIndex(System.String)
extern void Shader_FindPropertyIndex_m8E8AB9B10940D571B8242177CB5C27F561E10C30 (void);
// 0x00000216 System.String UnityEngine.Shader::GetPropertyName(System.Int32)
extern void Shader_GetPropertyName_m791E2E697D4677F72773A5A501DEB192D128AF43 (void);
// 0x00000217 System.Int32 UnityEngine.Shader::GetPropertyNameId(System.Int32)
extern void Shader_GetPropertyNameId_m6A173B313A51D83BC79833361EFDAC5A52EDD71F (void);
// 0x00000218 UnityEngine.Rendering.ShaderPropertyType UnityEngine.Shader::GetPropertyType(System.Int32)
extern void Shader_GetPropertyType_mE6A29BF201BC2326D5F6E5A1BE302BC57D1FAEC6 (void);
// 0x00000219 System.String UnityEngine.Shader::GetPropertyDescription(System.Int32)
extern void Shader_GetPropertyDescription_m89A316AF4485AD548C96A6944C49B0ED8EC0063D (void);
// 0x0000021A UnityEngine.Rendering.ShaderPropertyFlags UnityEngine.Shader::GetPropertyFlags(System.Int32)
extern void Shader_GetPropertyFlags_m9F53F6201CCAE781859332A363C2B2D3C810D8B9 (void);
// 0x0000021B System.String[] UnityEngine.Shader::GetPropertyAttributes(System.Int32)
extern void Shader_GetPropertyAttributes_mF20D64790DB722D0B3B267B7BBC6DC203B430A95 (void);
// 0x0000021C System.Single UnityEngine.Shader::GetPropertyDefaultFloatValue(System.Int32)
extern void Shader_GetPropertyDefaultFloatValue_m8B64279E4FEE502D446CE91C875CA47C80962277 (void);
// 0x0000021D UnityEngine.Vector4 UnityEngine.Shader::GetPropertyDefaultVectorValue(System.Int32)
extern void Shader_GetPropertyDefaultVectorValue_m919E33753E58CA74CBEB1CA3B617AB19312A5B2C (void);
// 0x0000021E UnityEngine.Vector2 UnityEngine.Shader::GetPropertyRangeLimits(System.Int32)
extern void Shader_GetPropertyRangeLimits_mE336116ADEECCDF0FFE183E2CF197EBF0EEE988E (void);
// 0x0000021F UnityEngine.Rendering.TextureDimension UnityEngine.Shader::GetPropertyTextureDimension(System.Int32)
extern void Shader_GetPropertyTextureDimension_mB7AD6DB9EB160248811C6D532243A53D2FF33EE2 (void);
// 0x00000220 System.String UnityEngine.Shader::GetPropertyTextureDefaultName(System.Int32)
extern void Shader_GetPropertyTextureDefaultName_m41B3FAD4323DD40EF37D75A9C59B909108FC2B57 (void);
// 0x00000221 System.Void UnityEngine.Shader::SetGlobalVectorImpl_Injected(System.Int32,UnityEngine.Vector4&)
extern void Shader_SetGlobalVectorImpl_Injected_m163EB9DAE823DD5F7AD16969CA24DA6C2F130D1A (void);
// 0x00000222 System.Void UnityEngine.Shader::SetGlobalMatrixImpl_Injected(System.Int32,UnityEngine.Matrix4x4&)
extern void Shader_SetGlobalMatrixImpl_Injected_m52914EC8C70103DF40621F04ACD8975DCEBE3E67 (void);
// 0x00000223 System.Void UnityEngine.Shader::GetGlobalVectorImpl_Injected(System.Int32,UnityEngine.Vector4&)
extern void Shader_GetGlobalVectorImpl_Injected_m9BF4331E49B29A6A756C29792111F2EDFFBAFC10 (void);
// 0x00000224 System.Void UnityEngine.Shader::GetGlobalMatrixImpl_Injected(System.Int32,UnityEngine.Matrix4x4&)
extern void Shader_GetGlobalMatrixImpl_Injected_m94972B58DEDC01DB4BAFD389B81BAA76C7946D6E (void);
// 0x00000225 System.Void UnityEngine.Shader::GetPropertyDefaultValue_Injected(UnityEngine.Shader,System.Int32,UnityEngine.Vector4&)
extern void Shader_GetPropertyDefaultValue_Injected_mB93EBCD6A25DF4D8A2A96F689A2FE13C4C321352 (void);
// 0x00000226 UnityEngine.Material UnityEngine.Material::Create(System.String)
extern void Material_Create_m3396EC163FABE66B6F03B800DCCED6BF2FFF2A0C (void);
// 0x00000227 System.Void UnityEngine.Material::CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
extern void Material_CreateWithShader_m41F159D25DC785C3BB43E6908057BB2BD1D2CB7F (void);
// 0x00000228 System.Void UnityEngine.Material::CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern void Material_CreateWithMaterial_mD3140BCB57EBB406D063C7A7B0B9D1A4C74DA3F2 (void);
// 0x00000229 System.Void UnityEngine.Material::CreateWithString(UnityEngine.Material)
extern void Material_CreateWithString_mCF4047522DD7D38087CF9AF121766E0D69B9BB55 (void);
// 0x0000022A System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
extern void Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8 (void);
// 0x0000022B System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern void Material__ctor_m0171C6D4D3FD04D58C70808F255DBA67D0ED2BDE (void);
// 0x0000022C System.Void UnityEngine.Material::.ctor(System.String)
extern void Material__ctor_m02F4232D67F46B1EE84441089306E867B1788924 (void);
// 0x0000022D UnityEngine.Material UnityEngine.Material::GetDefaultMaterial()
extern void Material_GetDefaultMaterial_m5FDD53D60389B565ABB231C8B543B625A5534CD3 (void);
// 0x0000022E UnityEngine.Material UnityEngine.Material::GetDefaultParticleMaterial()
extern void Material_GetDefaultParticleMaterial_m359E39F0DEEBEAE3C4A992BD6C4C57F3928931E9 (void);
// 0x0000022F UnityEngine.Material UnityEngine.Material::GetDefaultLineMaterial()
extern void Material_GetDefaultLineMaterial_m11D1657F15317F59623FE9A475D5CDC5D63AA06B (void);
// 0x00000230 UnityEngine.Shader UnityEngine.Material::get_shader()
extern void Material_get_shader_m9CEDCA4D97D42588C6B827400E364E4A8EC55FF0 (void);
// 0x00000231 System.Void UnityEngine.Material::set_shader(UnityEngine.Shader)
extern void Material_set_shader_m689F997F888E3C2A0FF9E6F399AA5D8204B454B1 (void);
// 0x00000232 UnityEngine.Color UnityEngine.Material::get_color()
extern void Material_get_color_m7CE9C1FC0E0B4952D58DFBBA4D569F4B161B27E9 (void);
// 0x00000233 System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern void Material_set_color_m127EAC5D3CC68359E72D12A2B3CE7428EFBB81C3 (void);
// 0x00000234 UnityEngine.Texture UnityEngine.Material::get_mainTexture()
extern void Material_get_mainTexture_mE85CF647728AD145D7E03A172EFD5930773E514E (void);
// 0x00000235 System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
extern void Material_set_mainTexture_m0742CFF768E9701618DA07C71F009239AB31EB41 (void);
// 0x00000236 UnityEngine.Vector2 UnityEngine.Material::get_mainTextureOffset()
extern void Material_get_mainTextureOffset_mCD63885CCB91B4E901FC4847FCCDFDA2E45B3590 (void);
// 0x00000237 System.Void UnityEngine.Material::set_mainTextureOffset(UnityEngine.Vector2)
extern void Material_set_mainTextureOffset_m8602C6F3E7FC358C5F8B9C3709325F676B1ACF5E (void);
// 0x00000238 UnityEngine.Vector2 UnityEngine.Material::get_mainTextureScale()
extern void Material_get_mainTextureScale_m882447FB9D8EF5873279BF15C62AD977EEEFE062 (void);
// 0x00000239 System.Void UnityEngine.Material::set_mainTextureScale(UnityEngine.Vector2)
extern void Material_set_mainTextureScale_m4E1ED22B62AD02B61921AD392A6D2E5B727383BA (void);
// 0x0000023A System.Int32 UnityEngine.Material::GetFirstPropertyNameIdByAttribute(UnityEngine.Rendering.ShaderPropertyFlags)
extern void Material_GetFirstPropertyNameIdByAttribute_m34991F73A65FE72151842050D18EB3EC8969A8C2 (void);
// 0x0000023B System.Boolean UnityEngine.Material::HasProperty(System.Int32)
extern void Material_HasProperty_m901DE6C516A0D2C986B849C7B44F679AE21B8927 (void);
// 0x0000023C System.Boolean UnityEngine.Material::HasProperty(System.String)
extern void Material_HasProperty_m8611FACA6F9D9B2B5C3E92B6D93D2D514B443512 (void);
// 0x0000023D System.Int32 UnityEngine.Material::get_renderQueue()
extern void Material_get_renderQueue_mDEC48BD94C93FF5A04BC7190E4B5C56BB6E44140 (void);
// 0x0000023E System.Void UnityEngine.Material::set_renderQueue(System.Int32)
extern void Material_set_renderQueue_m02A0C73EC4B9C9D2C2ABFFD777EBDA45C1E1BD4D (void);
// 0x0000023F System.Int32 UnityEngine.Material::get_rawRenderQueue()
extern void Material_get_rawRenderQueue_m9AB9F21C8EDB1D90752B432247362985D1A55283 (void);
// 0x00000240 System.Void UnityEngine.Material::EnableKeyword(System.String)
extern void Material_EnableKeyword_m7466758182CBBC40134C9048CDF682DF46F32FA9 (void);
// 0x00000241 System.Void UnityEngine.Material::DisableKeyword(System.String)
extern void Material_DisableKeyword_m2ACBFC5D28ED46FF2CF5532F00D702FF62C02ED3 (void);
// 0x00000242 System.Boolean UnityEngine.Material::IsKeywordEnabled(System.String)
extern void Material_IsKeywordEnabled_mABE55EDC028A2F95C34D9AE382ADBA9C0DC5B97A (void);
// 0x00000243 UnityEngine.MaterialGlobalIlluminationFlags UnityEngine.Material::get_globalIlluminationFlags()
extern void Material_get_globalIlluminationFlags_m8BB45CE933E1CFC49BB0F0F46E428FAF13361504 (void);
// 0x00000244 System.Void UnityEngine.Material::set_globalIlluminationFlags(UnityEngine.MaterialGlobalIlluminationFlags)
extern void Material_set_globalIlluminationFlags_m20C0201EB1CF367A93CE196E186AD784FA4D4F14 (void);
// 0x00000245 System.Boolean UnityEngine.Material::get_doubleSidedGI()
extern void Material_get_doubleSidedGI_m764A82FDF44DC53C6759D71DDB2463212D9F72DD (void);
// 0x00000246 System.Void UnityEngine.Material::set_doubleSidedGI(System.Boolean)
extern void Material_set_doubleSidedGI_m1216E118D46360F69CE42FFACD4AF44CBAD32B15 (void);
// 0x00000247 System.Boolean UnityEngine.Material::get_enableInstancing()
extern void Material_get_enableInstancing_mCB3E53889074239B33F840D28331AE8262B00E91 (void);
// 0x00000248 System.Void UnityEngine.Material::set_enableInstancing(System.Boolean)
extern void Material_set_enableInstancing_m745D94DF21B9749DA7CFE42BAB3CBD05F614B81E (void);
// 0x00000249 System.Int32 UnityEngine.Material::get_passCount()
extern void Material_get_passCount_m790A357C17FB4D829C479652B1B65BC15F73FBB1 (void);
// 0x0000024A System.Void UnityEngine.Material::SetShaderPassEnabled(System.String,System.Boolean)
extern void Material_SetShaderPassEnabled_m301D004116C2DC9B9A63121CB15BEB7D14DB0F68 (void);
// 0x0000024B System.Boolean UnityEngine.Material::GetShaderPassEnabled(System.String)
extern void Material_GetShaderPassEnabled_mBB19A00FE5398F28B5340EB7C8D1BF122E9CDFB3 (void);
// 0x0000024C System.String UnityEngine.Material::GetPassName(System.Int32)
extern void Material_GetPassName_m429460022064237971866E6C22C6BAF7AA7CA845 (void);
// 0x0000024D System.Int32 UnityEngine.Material::FindPass(System.String)
extern void Material_FindPass_m2BE935C6343F0F5111CD5DDA8C3A8691BE044180 (void);
// 0x0000024E System.Void UnityEngine.Material::SetOverrideTag(System.String,System.String)
extern void Material_SetOverrideTag_m773D4D6BE26AA7E1200C76571C6DABC4A53002DB (void);
// 0x0000024F System.String UnityEngine.Material::GetTagImpl(System.String,System.Boolean,System.String)
extern void Material_GetTagImpl_m74481FC086405BAA8614AFCA56F9B13925B85A19 (void);
// 0x00000250 System.String UnityEngine.Material::GetTag(System.String,System.Boolean,System.String)
extern void Material_GetTag_m718957E007771A729C627658C6B5A0F3AE99244C (void);
// 0x00000251 System.String UnityEngine.Material::GetTag(System.String,System.Boolean)
extern void Material_GetTag_m0BFC270775E697347173DD872D655B1F2C1FA131 (void);
// 0x00000252 System.Void UnityEngine.Material::Lerp(UnityEngine.Material,UnityEngine.Material,System.Single)
extern void Material_Lerp_m48C28B02B82EAC62B06165256564B311FE8802FD (void);
// 0x00000253 System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern void Material_SetPass_m4BE0A8FCBF158C83522AA2F69118A2FE33683918 (void);
// 0x00000254 System.Void UnityEngine.Material::CopyPropertiesFromMaterial(UnityEngine.Material)
extern void Material_CopyPropertiesFromMaterial_mC80A6EB2A756B079CEEDEBD5F03A36A6DD18227A (void);
// 0x00000255 System.String[] UnityEngine.Material::GetShaderKeywords()
extern void Material_GetShaderKeywords_m786F6539953C2B890A0827715404D3508BFCF685 (void);
// 0x00000256 System.Void UnityEngine.Material::SetShaderKeywords(System.String[])
extern void Material_SetShaderKeywords_m32714E17C5D17AAE15927F89624416E14B044A82 (void);
// 0x00000257 System.String[] UnityEngine.Material::get_shaderKeywords()
extern void Material_get_shaderKeywords_mF653034CC23EB4A65580BA4388F7258328C9C90C (void);
// 0x00000258 System.Void UnityEngine.Material::set_shaderKeywords(System.String[])
extern void Material_set_shaderKeywords_m336EBA03D542BE657FEBDD62C7546568CD3081C9 (void);
// 0x00000259 System.Int32 UnityEngine.Material::ComputeCRC()
extern void Material_ComputeCRC_mE22B08A446771A98DDC744742C6B24BC86CFEA74 (void);
// 0x0000025A System.String[] UnityEngine.Material::GetTexturePropertyNames()
extern void Material_GetTexturePropertyNames_mCF863B050B213FA4FA16395617ECE4A76B1A9835 (void);
// 0x0000025B System.Int32[] UnityEngine.Material::GetTexturePropertyNameIDs()
extern void Material_GetTexturePropertyNameIDs_m48D0C3D9553534CD03E2E17B4DBB3F57E8D26149 (void);
// 0x0000025C System.Void UnityEngine.Material::GetTexturePropertyNamesInternal(System.Object)
extern void Material_GetTexturePropertyNamesInternal_m4B3C8464A437ADB50C5BD97AEFAC03837F2B5BC8 (void);
// 0x0000025D System.Void UnityEngine.Material::GetTexturePropertyNameIDsInternal(System.Object)
extern void Material_GetTexturePropertyNameIDsInternal_mEE63AC3015A443983DECC97548BA2227BBA7F359 (void);
// 0x0000025E System.Void UnityEngine.Material::GetTexturePropertyNames(System.Collections.Generic.List`1<System.String>)
extern void Material_GetTexturePropertyNames_m2CA15F82FDD8631C10FF2B96A57FD468ECFB3527 (void);
// 0x0000025F System.Void UnityEngine.Material::GetTexturePropertyNameIDs(System.Collections.Generic.List`1<System.Int32>)
extern void Material_GetTexturePropertyNameIDs_m34FF0C128EE26F2F4C75D3329E3445C53BEF85AA (void);
// 0x00000260 System.Void UnityEngine.Material::SetFloatImpl(System.Int32,System.Single)
extern void Material_SetFloatImpl_mFD6022220F625E704EC4F27691F496166E590094 (void);
// 0x00000261 System.Void UnityEngine.Material::SetColorImpl(System.Int32,UnityEngine.Color)
extern void Material_SetColorImpl_m0779CF8FCCFC298AAF42A9E9FF6503C9FDC43CFE (void);
// 0x00000262 System.Void UnityEngine.Material::SetMatrixImpl(System.Int32,UnityEngine.Matrix4x4)
extern void Material_SetMatrixImpl_mAC50A1E655FF577F6AEB3C67F14DAE7D1AA60C4B (void);
// 0x00000263 System.Void UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)
extern void Material_SetTextureImpl_mBCF204C1FAD811B00DBAE98847D6D533EE05B135 (void);
// 0x00000264 System.Void UnityEngine.Material::SetRenderTextureImpl(System.Int32,UnityEngine.RenderTexture,UnityEngine.Rendering.RenderTextureSubElement)
extern void Material_SetRenderTextureImpl_m1716331E4D35E53EDEC8D06CA08F69EBEB49FD8E (void);
// 0x00000265 System.Void UnityEngine.Material::SetBufferImpl(System.Int32,UnityEngine.ComputeBuffer)
extern void Material_SetBufferImpl_m009669A2D3BD2561A902F7DE49C842F438F31DC9 (void);
// 0x00000266 System.Void UnityEngine.Material::SetConstantBufferImpl(System.Int32,UnityEngine.ComputeBuffer,System.Int32,System.Int32)
extern void Material_SetConstantBufferImpl_m9C25A7CF0B38DDE1F6CD66C6DA43DAF8F1C4EE38 (void);
// 0x00000267 System.Single UnityEngine.Material::GetFloatImpl(System.Int32)
extern void Material_GetFloatImpl_m7F4A1A6FE5D9EA1F5C90CD1476AB819C7BD7D4ED (void);
// 0x00000268 UnityEngine.Color UnityEngine.Material::GetColorImpl(System.Int32)
extern void Material_GetColorImpl_m46381ED49C354AEB9FEFCE5BF410A1909E867E76 (void);
// 0x00000269 UnityEngine.Matrix4x4 UnityEngine.Material::GetMatrixImpl(System.Int32)
extern void Material_GetMatrixImpl_mB768AE1ABBCC46A8BB1F58BDD850CA6B553F3A0D (void);
// 0x0000026A UnityEngine.Texture UnityEngine.Material::GetTextureImpl(System.Int32)
extern void Material_GetTextureImpl_m19D8CE6C5701AC4B69A6D5354E08240DF6C036D2 (void);
// 0x0000026B System.Void UnityEngine.Material::SetFloatArrayImpl(System.Int32,System.Single[],System.Int32)
extern void Material_SetFloatArrayImpl_m80EEFAE52B5E5BC228134BE5B070DC6237A3CDE4 (void);
// 0x0000026C System.Void UnityEngine.Material::SetVectorArrayImpl(System.Int32,UnityEngine.Vector4[],System.Int32)
extern void Material_SetVectorArrayImpl_mF499FA2995CA112EF3D8290C2B30E9D06A1ED2F9 (void);
// 0x0000026D System.Void UnityEngine.Material::SetColorArrayImpl(System.Int32,UnityEngine.Color[],System.Int32)
extern void Material_SetColorArrayImpl_mEC4C6D68484BC693CB5115BB0987FBD21FBBA195 (void);
// 0x0000026E System.Void UnityEngine.Material::SetMatrixArrayImpl(System.Int32,UnityEngine.Matrix4x4[],System.Int32)
extern void Material_SetMatrixArrayImpl_m2262E8F2DF3C21B212DE0E7603BF7B0FF86AC2EE (void);
// 0x0000026F System.Single[] UnityEngine.Material::GetFloatArrayImpl(System.Int32)
extern void Material_GetFloatArrayImpl_m8C0878BD1356C08D7A1AA8003B711D222DA03FE7 (void);
// 0x00000270 UnityEngine.Vector4[] UnityEngine.Material::GetVectorArrayImpl(System.Int32)
extern void Material_GetVectorArrayImpl_m1701F9B243640072362E2F9997A0E759A5DE67DB (void);
// 0x00000271 UnityEngine.Color[] UnityEngine.Material::GetColorArrayImpl(System.Int32)
extern void Material_GetColorArrayImpl_mE7740EE7AC1DAC25C74BCD81C97084CFB4388FED (void);
// 0x00000272 UnityEngine.Matrix4x4[] UnityEngine.Material::GetMatrixArrayImpl(System.Int32)
extern void Material_GetMatrixArrayImpl_m48B3DB8C758B68519144CABE503697C684BE6091 (void);
// 0x00000273 System.Int32 UnityEngine.Material::GetFloatArrayCountImpl(System.Int32)
extern void Material_GetFloatArrayCountImpl_m580060E1D5467059B9599796B6301F6A251F8A03 (void);
// 0x00000274 System.Int32 UnityEngine.Material::GetVectorArrayCountImpl(System.Int32)
extern void Material_GetVectorArrayCountImpl_m8477A1307A8B39BD4DF79BF9658C940A3BD7FEBD (void);
// 0x00000275 System.Int32 UnityEngine.Material::GetColorArrayCountImpl(System.Int32)
extern void Material_GetColorArrayCountImpl_m48D8D79A3C0B191EE2167E5C16A6E7AB0FEA3D2D (void);
// 0x00000276 System.Int32 UnityEngine.Material::GetMatrixArrayCountImpl(System.Int32)
extern void Material_GetMatrixArrayCountImpl_m5E9AFA151CA70F349A7349F4B9B526C5DB3D827F (void);
// 0x00000277 System.Void UnityEngine.Material::ExtractFloatArrayImpl(System.Int32,System.Single[])
extern void Material_ExtractFloatArrayImpl_mAE47B7521571D9BCCF9EA757D53CBDC40E636652 (void);
// 0x00000278 System.Void UnityEngine.Material::ExtractVectorArrayImpl(System.Int32,UnityEngine.Vector4[])
extern void Material_ExtractVectorArrayImpl_mBF17D3010B08179139F7E82B158E8B8EC03A30EF (void);
// 0x00000279 System.Void UnityEngine.Material::ExtractColorArrayImpl(System.Int32,UnityEngine.Color[])
extern void Material_ExtractColorArrayImpl_m92D16498F69C0D18FC2EAAD10EEBF2940D0B6108 (void);
// 0x0000027A System.Void UnityEngine.Material::ExtractMatrixArrayImpl(System.Int32,UnityEngine.Matrix4x4[])
extern void Material_ExtractMatrixArrayImpl_m6F51ADC11CF4032A05ACA177180C1087FCAD0834 (void);
// 0x0000027B UnityEngine.Vector4 UnityEngine.Material::GetTextureScaleAndOffsetImpl(System.Int32)
extern void Material_GetTextureScaleAndOffsetImpl_m8946BA8A9E6F974315DBFBAAC9A152BDCA65F3E1 (void);
// 0x0000027C System.Void UnityEngine.Material::SetTextureOffsetImpl(System.Int32,UnityEngine.Vector2)
extern void Material_SetTextureOffsetImpl_mFA6B03949317E27AA65014030ABE8CA79350FE45 (void);
// 0x0000027D System.Void UnityEngine.Material::SetTextureScaleImpl(System.Int32,UnityEngine.Vector2)
extern void Material_SetTextureScaleImpl_mB4BD14FA7A283ACD618A139F0A28D44BE299A89E (void);
// 0x0000027E System.Void UnityEngine.Material::SetFloatArray(System.Int32,System.Single[],System.Int32)
extern void Material_SetFloatArray_m62969A8282FA26EBA3996A5F9B752761A9C4E096 (void);
// 0x0000027F System.Void UnityEngine.Material::SetVectorArray(System.Int32,UnityEngine.Vector4[],System.Int32)
extern void Material_SetVectorArray_m49BD5611E6A2E24B2DB4C69219F7F53739EB413C (void);
// 0x00000280 System.Void UnityEngine.Material::SetColorArray(System.Int32,UnityEngine.Color[],System.Int32)
extern void Material_SetColorArray_m1E9237B662CEA4C70825E4FA36B14B79D627FB51 (void);
// 0x00000281 System.Void UnityEngine.Material::SetMatrixArray(System.Int32,UnityEngine.Matrix4x4[],System.Int32)
extern void Material_SetMatrixArray_mCFAFBCE28D08046FF2168DEE36A6A68FB184A17C (void);
// 0x00000282 System.Void UnityEngine.Material::ExtractFloatArray(System.Int32,System.Collections.Generic.List`1<System.Single>)
extern void Material_ExtractFloatArray_m196AFA3A023BC2B19D2624C12B01DD833D4DE2B6 (void);
// 0x00000283 System.Void UnityEngine.Material::ExtractVectorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Material_ExtractVectorArray_m170AA9CDBB231199910B0C75B69A4E6126E2E64D (void);
// 0x00000284 System.Void UnityEngine.Material::ExtractColorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void Material_ExtractColorArray_m5E3EA1DD4DF054838FCDD3F37DE83D6DEC12C0C7 (void);
// 0x00000285 System.Void UnityEngine.Material::ExtractMatrixArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Material_ExtractMatrixArray_m8FE94720CB4F520050B85B54E78FD0B841429232 (void);
// 0x00000286 System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
extern void Material_SetFloat_m4B7D3FAA00D20BCB3C487E72B7E4B2691D5ECAD2 (void);
// 0x00000287 System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
extern void Material_SetFloat_mC2FDDF0798373DEE6BBA9B9FFFE03EC3CFB9BF47 (void);
// 0x00000288 System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
extern void Material_SetInt_m1FCBDBB985E6A299AE11C3D8AF29BB4D7C7DF278 (void);
// 0x00000289 System.Void UnityEngine.Material::SetInt(System.Int32,System.Int32)
extern void Material_SetInt_m8D3776E4EF0DFA2A278B456F40741E07D83508CD (void);
// 0x0000028A System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern void Material_SetColor_mB91EF8CAC3AB3B39D2535BF9F7FECECF3EC2161C (void);
// 0x0000028B System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern void Material_SetColor_m48FBB701F6177B367EDFAC6BE896D183EF640725 (void);
// 0x0000028C System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
extern void Material_SetVector_m6FC2CC4EBE6C45D48D8B9164148A0CB3124335EC (void);
// 0x0000028D System.Void UnityEngine.Material::SetVector(System.Int32,UnityEngine.Vector4)
extern void Material_SetVector_m95B7CB07B91F004B4DD9DB5DFA5146472737B8EA (void);
// 0x0000028E System.Void UnityEngine.Material::SetMatrix(System.String,UnityEngine.Matrix4x4)
extern void Material_SetMatrix_mE3D3FA75DA02FED1B97E2BECA258F45399715764 (void);
// 0x0000028F System.Void UnityEngine.Material::SetMatrix(System.Int32,UnityEngine.Matrix4x4)
extern void Material_SetMatrix_m6C552C454B30BA42F5972F65403B2EED62D68D81 (void);
// 0x00000290 System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern void Material_SetTexture_mAA0F00FACFE40CFE4BE28A11162E5EEFCC5F5A61 (void);
// 0x00000291 System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern void Material_SetTexture_m4FFF0B403A64253B83534701104F017840142ACA (void);
// 0x00000292 System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.RenderTexture,UnityEngine.Rendering.RenderTextureSubElement)
extern void Material_SetTexture_mBB98BDF71749072D53B4A6426C23335EAF30987D (void);
// 0x00000293 System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.RenderTexture,UnityEngine.Rendering.RenderTextureSubElement)
extern void Material_SetTexture_m5F70C70649242D4DA11A933421638BBC45A721D5 (void);
// 0x00000294 System.Void UnityEngine.Material::SetBuffer(System.String,UnityEngine.ComputeBuffer)
extern void Material_SetBuffer_mD6D6FCAB0A0EB5BEB3A40672198D858670E57A5C (void);
// 0x00000295 System.Void UnityEngine.Material::SetBuffer(System.Int32,UnityEngine.ComputeBuffer)
extern void Material_SetBuffer_m9597D87FEE583129ECB9380D03FF76051A2C99BE (void);
// 0x00000296 System.Void UnityEngine.Material::SetConstantBuffer(System.String,UnityEngine.ComputeBuffer,System.Int32,System.Int32)
extern void Material_SetConstantBuffer_mAA20E3E0FF3C045A8F6337FA6C1D8A2503DF069A (void);
// 0x00000297 System.Void UnityEngine.Material::SetConstantBuffer(System.Int32,UnityEngine.ComputeBuffer,System.Int32,System.Int32)
extern void Material_SetConstantBuffer_m71B4947519DE0A36AC849D062D16FDF1BB47F5BD (void);
// 0x00000298 System.Void UnityEngine.Material::SetFloatArray(System.String,System.Collections.Generic.List`1<System.Single>)
extern void Material_SetFloatArray_m20E65D81B203E7BDACC5AE46A4945330419A5A19 (void);
// 0x00000299 System.Void UnityEngine.Material::SetFloatArray(System.Int32,System.Collections.Generic.List`1<System.Single>)
extern void Material_SetFloatArray_m5D9D21D6ADC910228EC58E272E7EAECEF54F0EF8 (void);
// 0x0000029A System.Void UnityEngine.Material::SetFloatArray(System.String,System.Single[])
extern void Material_SetFloatArray_m826883FAC1B827A065735E70E7B6A706BCE695FA (void);
// 0x0000029B System.Void UnityEngine.Material::SetFloatArray(System.Int32,System.Single[])
extern void Material_SetFloatArray_m03E524B5B1CA096EEC2B0404AE61B7F4F5FACF3C (void);
// 0x0000029C System.Void UnityEngine.Material::SetColorArray(System.String,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void Material_SetColorArray_m76B75E0F814FF7BEBF2436D62F30D27F1B1AB118 (void);
// 0x0000029D System.Void UnityEngine.Material::SetColorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void Material_SetColorArray_mE1CF05ACE8AC659BC701181F1A9EF924D224E373 (void);
// 0x0000029E System.Void UnityEngine.Material::SetColorArray(System.String,UnityEngine.Color[])
extern void Material_SetColorArray_m0C7FB3A0F21A1DCD4EEFE47B504AD10D21D005C0 (void);
// 0x0000029F System.Void UnityEngine.Material::SetColorArray(System.Int32,UnityEngine.Color[])
extern void Material_SetColorArray_mF8B250D1A05705B6781DC7725A977A94D28C2AFA (void);
// 0x000002A0 System.Void UnityEngine.Material::SetVectorArray(System.String,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Material_SetVectorArray_mC73E3C65E0CF82746C9BECC0376522F0722DC178 (void);
// 0x000002A1 System.Void UnityEngine.Material::SetVectorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Material_SetVectorArray_mC1CC338EA86D2D80606CE3B9DC58E2CC415788F2 (void);
// 0x000002A2 System.Void UnityEngine.Material::SetVectorArray(System.String,UnityEngine.Vector4[])
extern void Material_SetVectorArray_mB29351C346C21AB801523B92A277B4A820583D51 (void);
// 0x000002A3 System.Void UnityEngine.Material::SetVectorArray(System.Int32,UnityEngine.Vector4[])
extern void Material_SetVectorArray_m3DEE0383284B478E6A261A485DEEB09F9F63B51C (void);
// 0x000002A4 System.Void UnityEngine.Material::SetMatrixArray(System.String,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Material_SetMatrixArray_mC7F439E54B8AB183073087D09213EEAB40282F6E (void);
// 0x000002A5 System.Void UnityEngine.Material::SetMatrixArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Material_SetMatrixArray_m8B3BD07E05515B2A06C7138B92BCB35EE5B7CA8D (void);
// 0x000002A6 System.Void UnityEngine.Material::SetMatrixArray(System.String,UnityEngine.Matrix4x4[])
extern void Material_SetMatrixArray_m5866D8DDFA81DD15B63F83949FDDEED0E304CCC8 (void);
// 0x000002A7 System.Void UnityEngine.Material::SetMatrixArray(System.Int32,UnityEngine.Matrix4x4[])
extern void Material_SetMatrixArray_mB9988E11FFDBFCC71DCE75433F2E745C77BB34FF (void);
// 0x000002A8 System.Single UnityEngine.Material::GetFloat(System.String)
extern void Material_GetFloat_m8A4243FC6619B4E0E820E87754035700FD4913F0 (void);
// 0x000002A9 System.Single UnityEngine.Material::GetFloat(System.Int32)
extern void Material_GetFloat_mC1764F8B39FD31C6B7629D417BC8375D6E6AC60C (void);
// 0x000002AA System.Int32 UnityEngine.Material::GetInt(System.String)
extern void Material_GetInt_mE2577C1C53AB98AF3FEE2BC95F04EC5694933F41 (void);
// 0x000002AB System.Int32 UnityEngine.Material::GetInt(System.Int32)
extern void Material_GetInt_m7D7C0ECB5A4D5CE5F1F409D04B4F886930271DEC (void);
// 0x000002AC UnityEngine.Color UnityEngine.Material::GetColor(System.String)
extern void Material_GetColor_m95E13C52410F7CE01E8DC32668ABBCBF19BB8808 (void);
// 0x000002AD UnityEngine.Color UnityEngine.Material::GetColor(System.Int32)
extern void Material_GetColor_m1FFF5ECB4967771D751C60205635A0D3F7AA2F0E (void);
// 0x000002AE UnityEngine.Vector4 UnityEngine.Material::GetVector(System.String)
extern void Material_GetVector_m068E8828C9E9FB017161F018E80818C2D1651477 (void);
// 0x000002AF UnityEngine.Vector4 UnityEngine.Material::GetVector(System.Int32)
extern void Material_GetVector_mDE1F87F355E0ECAD883E4F2CF62DB338C2E00C38 (void);
// 0x000002B0 UnityEngine.Matrix4x4 UnityEngine.Material::GetMatrix(System.String)
extern void Material_GetMatrix_mAB53222644DC5DAD4A9E9628C42B954A272C0CC8 (void);
// 0x000002B1 UnityEngine.Matrix4x4 UnityEngine.Material::GetMatrix(System.Int32)
extern void Material_GetMatrix_m685F5AE4EAFE5F49CC1791AFF06707433C70F1BC (void);
// 0x000002B2 UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
extern void Material_GetTexture_mCD6B822EA19773B8D39368FF1A285E7B69043896 (void);
// 0x000002B3 UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
extern void Material_GetTexture_mDB1B89D76D44AD07BD214224C59A6FE0B62F6477 (void);
// 0x000002B4 System.Single[] UnityEngine.Material::GetFloatArray(System.String)
extern void Material_GetFloatArray_mF4B422C881673975D135D0A4256FFF550FB46C4E (void);
// 0x000002B5 System.Single[] UnityEngine.Material::GetFloatArray(System.Int32)
extern void Material_GetFloatArray_m7D42AC14400F26D355FFDDDDED4E8AB2F90DC825 (void);
// 0x000002B6 UnityEngine.Color[] UnityEngine.Material::GetColorArray(System.String)
extern void Material_GetColorArray_mD4332AD6BB02C0913C51E0EAA7A833533EF4B189 (void);
// 0x000002B7 UnityEngine.Color[] UnityEngine.Material::GetColorArray(System.Int32)
extern void Material_GetColorArray_mC0A372A8EC7DBB7C2A891A3DA469826C83A27022 (void);
// 0x000002B8 UnityEngine.Vector4[] UnityEngine.Material::GetVectorArray(System.String)
extern void Material_GetVectorArray_m9BF6FC74660F816FC52A7F1D8A10A75D142A561D (void);
// 0x000002B9 UnityEngine.Vector4[] UnityEngine.Material::GetVectorArray(System.Int32)
extern void Material_GetVectorArray_m78CFEA0BAB12B3E80987E76D88962CBA2E231C20 (void);
// 0x000002BA UnityEngine.Matrix4x4[] UnityEngine.Material::GetMatrixArray(System.String)
extern void Material_GetMatrixArray_m0227057101B57D50DDB8AEE536C88D37824309E4 (void);
// 0x000002BB UnityEngine.Matrix4x4[] UnityEngine.Material::GetMatrixArray(System.Int32)
extern void Material_GetMatrixArray_mC644ED51D04D128D88CF7FB12FBAC5C392EE51D8 (void);
// 0x000002BC System.Void UnityEngine.Material::GetFloatArray(System.String,System.Collections.Generic.List`1<System.Single>)
extern void Material_GetFloatArray_m8C0AFD6B92559EFDCE70027D4FF3621CB004D845 (void);
// 0x000002BD System.Void UnityEngine.Material::GetFloatArray(System.Int32,System.Collections.Generic.List`1<System.Single>)
extern void Material_GetFloatArray_mE671829DD1EDB93EACDC7BB19AF6404CAB4FB30A (void);
// 0x000002BE System.Void UnityEngine.Material::GetColorArray(System.String,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void Material_GetColorArray_mD9B2A59966A9B8D50F0D3D3E5F0AAFE4EFD5EB3F (void);
// 0x000002BF System.Void UnityEngine.Material::GetColorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void Material_GetColorArray_mC9EA848EDD5120DB3689ED9D84475F7E2850B7B5 (void);
// 0x000002C0 System.Void UnityEngine.Material::GetVectorArray(System.String,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Material_GetVectorArray_m62C72E37A866CE06429764E4741B568C10BDAFBF (void);
// 0x000002C1 System.Void UnityEngine.Material::GetVectorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Material_GetVectorArray_m777DE57187F9E93BD9E04995C388926582A4B209 (void);
// 0x000002C2 System.Void UnityEngine.Material::GetMatrixArray(System.String,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Material_GetMatrixArray_mDBF02CC3EBE3958272A1C316B0FEC430CE2E9AA9 (void);
// 0x000002C3 System.Void UnityEngine.Material::GetMatrixArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Material_GetMatrixArray_m3B607490006A830269AFC72245C2AE9431AE89F2 (void);
// 0x000002C4 System.Void UnityEngine.Material::SetTextureOffset(System.String,UnityEngine.Vector2)
extern void Material_SetTextureOffset_mAF1B6A6AD9E952262F7AE80B17B31D8FF720ADE7 (void);
// 0x000002C5 System.Void UnityEngine.Material::SetTextureOffset(System.Int32,UnityEngine.Vector2)
extern void Material_SetTextureOffset_m3E9C91ED11CB6323252AF6B5CB15CCD497EF10BB (void);
// 0x000002C6 System.Void UnityEngine.Material::SetTextureScale(System.String,UnityEngine.Vector2)
extern void Material_SetTextureScale_m9D9C2ADD50088A1712891A6A2AAAAFA734703BBF (void);
// 0x000002C7 System.Void UnityEngine.Material::SetTextureScale(System.Int32,UnityEngine.Vector2)
extern void Material_SetTextureScale_m30A521475E4FF45858D27B8DA562B06165734A9E (void);
// 0x000002C8 UnityEngine.Vector2 UnityEngine.Material::GetTextureOffset(System.String)
extern void Material_GetTextureOffset_m3C0722B8C77DF728586E121E4DD81AED28097789 (void);
// 0x000002C9 UnityEngine.Vector2 UnityEngine.Material::GetTextureOffset(System.Int32)
extern void Material_GetTextureOffset_m5441F649AF03115B34C2EA4C8C84A068823F9E39 (void);
// 0x000002CA UnityEngine.Vector2 UnityEngine.Material::GetTextureScale(System.String)
extern void Material_GetTextureScale_m2B1FFB0B67C22C3544E2E66E3E0D5B8DEE7EA859 (void);
// 0x000002CB UnityEngine.Vector2 UnityEngine.Material::GetTextureScale(System.Int32)
extern void Material_GetTextureScale_m3AD3DBFC4E7F8546BCFA10BE56FD794C64635D9E (void);
// 0x000002CC System.Void UnityEngine.Material::SetColorImpl_Injected(System.Int32,UnityEngine.Color&)
extern void Material_SetColorImpl_Injected_mEE762DBD0B37ACA313061179B3CB767B59E4B0FB (void);
// 0x000002CD System.Void UnityEngine.Material::SetMatrixImpl_Injected(System.Int32,UnityEngine.Matrix4x4&)
extern void Material_SetMatrixImpl_Injected_mD2486E408D7547B08CC36A2DE70520F97A68529C (void);
// 0x000002CE System.Void UnityEngine.Material::GetColorImpl_Injected(System.Int32,UnityEngine.Color&)
extern void Material_GetColorImpl_Injected_m22325A32C4B5AAEA791AED50195F66CDBAE93C82 (void);
// 0x000002CF System.Void UnityEngine.Material::GetMatrixImpl_Injected(System.Int32,UnityEngine.Matrix4x4&)
extern void Material_GetMatrixImpl_Injected_m42181501EB254B8D21F950998CD679A39EBFC1FF (void);
// 0x000002D0 System.Void UnityEngine.Material::GetTextureScaleAndOffsetImpl_Injected(System.Int32,UnityEngine.Vector4&)
extern void Material_GetTextureScaleAndOffsetImpl_Injected_m5DDD8AB08DCF6A52415FB8D76A089CC886425E54 (void);
// 0x000002D1 System.Void UnityEngine.Material::SetTextureOffsetImpl_Injected(System.Int32,UnityEngine.Vector2&)
extern void Material_SetTextureOffsetImpl_Injected_m85850F975A45BBB3AEA145C52309FC7CB82CDF64 (void);
// 0x000002D2 System.Void UnityEngine.Material::SetTextureScaleImpl_Injected(System.Int32,UnityEngine.Vector2&)
extern void Material_SetTextureScaleImpl_Injected_m44A1C509E1EA40BFAFA82C7449EC21E6D39E8BF9 (void);
// 0x000002D3 UnityEngine.LightType UnityEngine.Light::get_type()
extern void Light_get_type_m24F8A5EFB5D0B0B5F4820623132D1EAA327D06E3 (void);
// 0x000002D4 System.Single UnityEngine.Light::get_spotAngle()
extern void Light_get_spotAngle_m1EAB341E449675D41E86A63F047BF3ABE2B99C3B (void);
// 0x000002D5 UnityEngine.Color UnityEngine.Light::get_color()
extern void Light_get_color_m7A83B30FE716A1A931D450A6035A0069A2DD7698 (void);
// 0x000002D6 System.Void UnityEngine.Light::set_color(UnityEngine.Color)
extern void Light_set_color_mCC1582CECB03F511169444C9498B7FFBED8EAF63 (void);
// 0x000002D7 System.Single UnityEngine.Light::get_intensity()
extern void Light_get_intensity_m4E9152844D85D03FEDA5AE4599AFAFC3C66EFF23 (void);
// 0x000002D8 System.Void UnityEngine.Light::set_intensity(System.Single)
extern void Light_set_intensity_mE209975C840F1D887B4207C390DB5A2EF15A763C (void);
// 0x000002D9 System.Single UnityEngine.Light::get_bounceIntensity()
extern void Light_get_bounceIntensity_m9185F30A7DED7FB480B1026B2CC6DBA357FAE9A7 (void);
// 0x000002DA System.Single UnityEngine.Light::get_range()
extern void Light_get_range_m3AA5F21DB9441E888A0E89C465F928445150061A (void);
// 0x000002DB System.Void UnityEngine.Light::set_range(System.Single)
extern void Light_set_range_mA08B30B1776471973285722FCB7633870B23106B (void);
// 0x000002DC UnityEngine.LightShadows UnityEngine.Light::get_shadows()
extern void Light_get_shadows_m8FBBEDB8C442B0426E5D3E457330AA81D764C8F2 (void);
// 0x000002DD System.Single UnityEngine.Light::get_shadowStrength()
extern void Light_get_shadowStrength_mB46E58530AF939421D198670434F15D331BFB672 (void);
// 0x000002DE System.Void UnityEngine.Light::set_shadowStrength(System.Single)
extern void Light_set_shadowStrength_m447AC9B66C28129AA3B2D62AE918FB511E27E2FE (void);
// 0x000002DF System.Void UnityEngine.Light::get_color_Injected(UnityEngine.Color&)
extern void Light_get_color_Injected_m266FA6B59B6A43AE5C2717FE58D91E93626D48DE (void);
// 0x000002E0 System.Void UnityEngine.Light::set_color_Injected(UnityEngine.Color&)
extern void Light_set_color_Injected_mA6F1C470D37DF5D6F1D2AA5919C5EC989A28CD0A (void);
// 0x000002E1 System.Void UnityEngine.MeshFilter::DontStripMeshFilter()
extern void MeshFilter_DontStripMeshFilter_m7FBA33F8214DB646F74E00F7CEFFDF2D0018004C (void);
// 0x000002E2 System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern void MeshFilter_set_sharedMesh_mFE8D12C35148063EB287951C7BFFB0328AAA7C5D (void);
// 0x000002E3 System.Void UnityEngine.MeshRenderer::DontStripMeshRenderer()
extern void MeshRenderer_DontStripMeshRenderer_mC5359CA39BA768EBDB3C90D4FAE999F4EB1B6B24 (void);
// 0x000002E4 System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern void Mesh_Internal_Create_mF1F8E9F726EAC9A087D49C81E2F1609E8266649E (void);
// 0x000002E5 System.Void UnityEngine.Mesh::.ctor()
extern void Mesh__ctor_m3AEBC82AB71D4F9498F6E254174BEBA8372834B4 (void);
// 0x000002E6 System.Int32[] UnityEngine.Mesh::GetIndicesImpl(System.Int32,System.Boolean)
extern void Mesh_GetIndicesImpl_m2118C6CA196093FD19BB05E5258C0DCE06FEAD30 (void);
// 0x000002E7 System.Void UnityEngine.Mesh::SetIndicesImpl(System.Int32,UnityEngine.MeshTopology,UnityEngine.Rendering.IndexFormat,System.Array,System.Int32,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetIndicesImpl_m05B1C648F5E02990B89D1FFF002F5D5672779D8B (void);
// 0x000002E8 System.Void UnityEngine.Mesh::PrintErrorCantAccessChannel(UnityEngine.Rendering.VertexAttribute)
extern void Mesh_PrintErrorCantAccessChannel_m2E8A25959739B006557A94F7E460E8BE0B3ABB19 (void);
// 0x000002E9 System.Boolean UnityEngine.Mesh::HasVertexAttribute(UnityEngine.Rendering.VertexAttribute)
extern void Mesh_HasVertexAttribute_m87C57B859ECB5224EEB77ED9BD3B6FF30F5A9B1C (void);
// 0x000002EA System.Void UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Rendering.VertexAttribute,UnityEngine.Rendering.VertexAttributeFormat,System.Int32,System.Array,System.Int32,System.Int32,System.Int32)
extern void Mesh_SetArrayForChannelImpl_m6FDE8B55C37DC1828C0041DDA3AD1E6D2C028E6C (void);
// 0x000002EB System.Array UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Rendering.VertexAttribute,UnityEngine.Rendering.VertexAttributeFormat,System.Int32)
extern void Mesh_GetAllocArrayFromChannelImpl_mFDE324953466F6DBAC14CD4B48105B62181CC399 (void);
// 0x000002EC System.Boolean UnityEngine.Mesh::get_canAccess()
extern void Mesh_get_canAccess_m1E0020EA7961227FBC0D90D851A49BCF7EB1E194 (void);
// 0x000002ED System.Int32 UnityEngine.Mesh::get_subMeshCount()
extern void Mesh_get_subMeshCount_m6BE7CFB52CE84AEE45B4E5A704E865954397F52F (void);
// 0x000002EE UnityEngine.Bounds UnityEngine.Mesh::get_bounds()
extern void Mesh_get_bounds_m15A146B9397AA62A81986030D289320EDE4B3037 (void);
// 0x000002EF System.Void UnityEngine.Mesh::set_bounds(UnityEngine.Bounds)
extern void Mesh_set_bounds_mB09338F622466456ADBCC449BB1F62F2EF1731B6 (void);
// 0x000002F0 System.Void UnityEngine.Mesh::ClearImpl(System.Boolean)
extern void Mesh_ClearImpl_mFFD3A4748AF067B75DBD646B4A64B3D9A2F3EE14 (void);
// 0x000002F1 System.Void UnityEngine.Mesh::RecalculateBoundsImpl()
extern void Mesh_RecalculateBoundsImpl_m43DE3DEFC2DADC090DC6FD2C27F44D6DBB88CEF6 (void);
// 0x000002F2 System.Void UnityEngine.Mesh::MarkDynamicImpl()
extern void Mesh_MarkDynamicImpl_m439AACE205D2202AD4E5D4A12191F4CCCC031876 (void);
// 0x000002F3 UnityEngine.Rendering.VertexAttribute UnityEngine.Mesh::GetUVChannel(System.Int32)
extern void Mesh_GetUVChannel_m15BB0A6CC8E32867621A78627CD5FF2CAEA163CC (void);
// 0x000002F4 System.Int32 UnityEngine.Mesh::DefaultDimensionForChannel(UnityEngine.Rendering.VertexAttribute)
extern void Mesh_DefaultDimensionForChannel_mF943AF434BB9F54DBB3B3DE65F7B816E617A89C9 (void);
// 0x000002F5 T[] UnityEngine.Mesh::GetAllocArrayFromChannel(UnityEngine.Rendering.VertexAttribute,UnityEngine.Rendering.VertexAttributeFormat,System.Int32)
// 0x000002F6 T[] UnityEngine.Mesh::GetAllocArrayFromChannel(UnityEngine.Rendering.VertexAttribute)
// 0x000002F7 System.Void UnityEngine.Mesh::SetSizedArrayForChannel(UnityEngine.Rendering.VertexAttribute,UnityEngine.Rendering.VertexAttributeFormat,System.Int32,System.Array,System.Int32,System.Int32,System.Int32)
extern void Mesh_SetSizedArrayForChannel_mBBB7F5AE4D5A5A6D5794742745385B02B13E082D (void);
// 0x000002F8 System.Void UnityEngine.Mesh::SetArrayForChannel(UnityEngine.Rendering.VertexAttribute,UnityEngine.Rendering.VertexAttributeFormat,System.Int32,T[])
// 0x000002F9 System.Void UnityEngine.Mesh::SetArrayForChannel(UnityEngine.Rendering.VertexAttribute,T[])
// 0x000002FA System.Void UnityEngine.Mesh::SetListForChannel(UnityEngine.Rendering.VertexAttribute,UnityEngine.Rendering.VertexAttributeFormat,System.Int32,System.Collections.Generic.List`1<T>,System.Int32,System.Int32)
// 0x000002FB System.Void UnityEngine.Mesh::SetListForChannel(UnityEngine.Rendering.VertexAttribute,System.Collections.Generic.List`1<T>,System.Int32,System.Int32)
// 0x000002FC UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern void Mesh_get_vertices_m7D07DC0F071C142B87F675B148FC0F7A243238B9 (void);
// 0x000002FD System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern void Mesh_set_vertices_mC1406AE08BC3495F3B0E29B53BACC9FD7BA685C6 (void);
// 0x000002FE UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern void Mesh_get_normals_m3CE4668899836CBD17C3F85EB24261CBCEB3EABB (void);
// 0x000002FF System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
extern void Mesh_set_normals_m4054D319A67DAAA25A794D67AD37278A84406589 (void);
// 0x00000300 UnityEngine.Vector4[] UnityEngine.Mesh::get_tangents()
extern void Mesh_get_tangents_mFF92BD7D6EBA8C7EB8340E1529B1CB98006F44DD (void);
// 0x00000301 System.Void UnityEngine.Mesh::set_tangents(UnityEngine.Vector4[])
extern void Mesh_set_tangents_mE66D8020B76E43A5CA3C4E60DB61CD962D7D3C57 (void);
// 0x00000302 UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
extern void Mesh_get_uv_m0EBA5CA4644C9D5F1B2125AF3FE3873EFC8A4616 (void);
// 0x00000303 System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern void Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8 (void);
// 0x00000304 UnityEngine.Vector2[] UnityEngine.Mesh::get_uv2()
extern void Mesh_get_uv2_m3E70D5DD7A5C6910A074A78296269EBF2CBAE97F (void);
// 0x00000305 System.Void UnityEngine.Mesh::set_uv2(UnityEngine.Vector2[])
extern void Mesh_set_uv2_m8D37F9622B68D4CEA7FCF96BE4F8E442155DB038 (void);
// 0x00000306 UnityEngine.Vector2[] UnityEngine.Mesh::get_uv3()
extern void Mesh_get_uv3_mC56484D8B69A65DA948C7F23B06ED490BCFBE8B0 (void);
// 0x00000307 UnityEngine.Vector2[] UnityEngine.Mesh::get_uv4()
extern void Mesh_get_uv4_m1C5734938A443D8004339E8D8DDDC33B3E0935F6 (void);
// 0x00000308 UnityEngine.Color32[] UnityEngine.Mesh::get_colors32()
extern void Mesh_get_colors32_m24C6C6BC1A40B7F09FF390F304A96728A4C99246 (void);
// 0x00000309 System.Void UnityEngine.Mesh::set_colors32(UnityEngine.Color32[])
extern void Mesh_set_colors32_m29459AFE2843D67E95D60E5A13B6F6AF7670230C (void);
// 0x0000030A System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void Mesh_SetVertices_m5F487FC255C9CAF4005B75CFE67A88C8C0E7BB06 (void);
// 0x0000030B System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32,System.Int32)
extern void Mesh_SetVertices_mCFFD77B857B3041D407D4EB6BEA3F4FC8F258655 (void);
// 0x0000030C System.Void UnityEngine.Mesh::SetNormals(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void Mesh_SetNormals_m76D71A949B9288FA8ED17DDADC530365307B9797 (void);
// 0x0000030D System.Void UnityEngine.Mesh::SetNormals(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32,System.Int32)
extern void Mesh_SetNormals_m2D6A6379F1C1E974C155DFD0E59812316B17F6B2 (void);
// 0x0000030E System.Void UnityEngine.Mesh::SetTangents(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Mesh_SetTangents_m6EEAB861C9286B1DA4935B87A883045ADD3955E5 (void);
// 0x0000030F System.Void UnityEngine.Mesh::SetTangents(System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Int32,System.Int32)
extern void Mesh_SetTangents_m2C28E9ED06F035D4F726DA8F8782F75B81AF1BF9 (void);
// 0x00000310 System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>)
extern void Mesh_SetColors_m237E41213E82D4BB882ED96FD81A17D9366590CF (void);
// 0x00000311 System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>,System.Int32,System.Int32)
extern void Mesh_SetColors_m7F5D50EA329435A03C41DCF4A6CFAB57504FAAAF (void);
// 0x00000312 System.Void UnityEngine.Mesh::SetUvsImpl(System.Int32,System.Int32,System.Collections.Generic.List`1<T>,System.Int32,System.Int32)
// 0x00000313 System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void Mesh_SetUVs_m0210150B0387289B823488D421BDF9CBF9769116 (void);
// 0x00000314 System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Int32,System.Int32)
extern void Mesh_SetUVs_m5E152C09C814455B8A8FB6672ACD15C0D6804EC5 (void);
// 0x00000315 System.Void UnityEngine.Mesh::PrintErrorCantAccessIndices()
extern void Mesh_PrintErrorCantAccessIndices_mA45D3609288655A328AEB0F2F436403B1ECE5077 (void);
// 0x00000316 System.Boolean UnityEngine.Mesh::CheckCanAccessSubmesh(System.Int32,System.Boolean)
extern void Mesh_CheckCanAccessSubmesh_mF86EBD1C9EC3FE557880FA138510F7761585D227 (void);
// 0x00000317 System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshTriangles(System.Int32)
extern void Mesh_CheckCanAccessSubmeshTriangles_m214B5F30F7461C620D03F10F6CF1CAF53DBEE509 (void);
// 0x00000318 System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshIndices(System.Int32)
extern void Mesh_CheckCanAccessSubmeshIndices_m81DF83B1676084AF0B70A36BC7621ADB08430722 (void);
// 0x00000319 System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern void Mesh_set_triangles_m143A1C262BADCFACE43587EBA2CDC6EBEB5DFAED (void);
// 0x0000031A System.Int32[] UnityEngine.Mesh::GetIndices(System.Int32)
extern void Mesh_GetIndices_m2FD8417547E7595F590CE55D381E0D13A8D72AA5 (void);
// 0x0000031B System.Int32[] UnityEngine.Mesh::GetIndices(System.Int32,System.Boolean)
extern void Mesh_GetIndices_m4260DCF1026449C4E8C4C40229D12AF8CAB26EAF (void);
// 0x0000031C System.Void UnityEngine.Mesh::CheckIndicesArrayRange(System.Int32,System.Int32,System.Int32)
extern void Mesh_CheckIndicesArrayRange_m6DEDA2715437F79DAC8BD7F818B5B2DBAD9BFEBE (void);
// 0x0000031D System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,UnityEngine.Rendering.IndexFormat,System.Array,System.Int32,System.Int32,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetTrianglesImpl_m0AD2B3D12113B5E82AC9E29D2C6308E7410E2B98 (void);
// 0x0000031E System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32)
extern void Mesh_SetTriangles_m6A43D705DE751C622CCF88EC31C4EF1B53578BE5 (void);
// 0x0000031F System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetTriangles_m39FB983B90F36D724CC1C21BA7821C9697F86116 (void);
// 0x00000320 System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Int32,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetTriangles_mBD39F430A044A27A171AD8FD80F2B9DDFDB4A469 (void);
// 0x00000321 System.Void UnityEngine.Mesh::SetIndices(System.Int32[],UnityEngine.MeshTopology,System.Int32)
extern void Mesh_SetIndices_m18C0006CF36C43FF16B1917099E2970C2D4145BD (void);
// 0x00000322 System.Void UnityEngine.Mesh::SetIndices(System.Int32[],UnityEngine.MeshTopology,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetIndices_mFE6F2769EF170F0CD84C739E96976A15052B7024 (void);
// 0x00000323 System.Void UnityEngine.Mesh::SetIndices(System.Int32[],System.Int32,System.Int32,UnityEngine.MeshTopology,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetIndices_m5E72EA4BED8BA7B0732E80227199026B1CD9B6C5 (void);
// 0x00000324 System.Void UnityEngine.Mesh::Clear()
extern void Mesh_Clear_mB750E1DCAB658124AAD81A02B93DED7601047B60 (void);
// 0x00000325 System.Void UnityEngine.Mesh::RecalculateBounds()
extern void Mesh_RecalculateBounds_m1BF701FE2CEA4E8E1183FF878B812808ED1EBA49 (void);
// 0x00000326 System.Void UnityEngine.Mesh::MarkDynamic()
extern void Mesh_MarkDynamic_m554A9D7214E30E4F8F0515B712F3E3FA6D695A5F (void);
// 0x00000327 System.Void UnityEngine.Mesh::get_bounds_Injected(UnityEngine.Bounds&)
extern void Mesh_get_bounds_Injected_mA4338C67E67FC449E1344556D073855ACB4E9DED (void);
// 0x00000328 System.Void UnityEngine.Mesh::set_bounds_Injected(UnityEngine.Bounds&)
extern void Mesh_set_bounds_Injected_mFA253839FEEC7CDF976FE740CA3C2712F491BB8B (void);
// 0x00000329 System.Void UnityEngine.Texture::.ctor()
extern void Texture__ctor_m19850F4654F76731DD82B99217AD5A2EB6974C6C (void);
// 0x0000032A System.Int32 UnityEngine.Texture::GetDataWidth()
extern void Texture_GetDataWidth_m862817D573E6B1BAE31E9412DB1F1C9B3A15B21D (void);
// 0x0000032B System.Int32 UnityEngine.Texture::GetDataHeight()
extern void Texture_GetDataHeight_m3E5739F25B967D6AF703541F236F0B1F3F8F939E (void);
// 0x0000032C System.Int32 UnityEngine.Texture::get_width()
extern void Texture_get_width_mEF9D208720B8FB3E7A29F3A5A5C381B56E657ED2 (void);
// 0x0000032D System.Void UnityEngine.Texture::set_width(System.Int32)
extern void Texture_set_width_m9E42C8B8ED703644B85F54D8DCFB51BF954F56DA (void);
// 0x0000032E System.Int32 UnityEngine.Texture::get_height()
extern void Texture_get_height_m3A004CD1FA238B3D0B32FE7030634B9038EC4AA0 (void);
// 0x0000032F System.Void UnityEngine.Texture::set_height(System.Int32)
extern void Texture_set_height_m601E103C6E803353701370B161F992A5B0C89AB6 (void);
// 0x00000330 System.Boolean UnityEngine.Texture::get_isReadable()
extern void Texture_get_isReadable_mFB3D8E8799AC5EFD9E1AB68386DA16F3607631BD (void);
// 0x00000331 UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
extern void Texture_get_wrapMode_mC21054C7BC6E958937B7459DAF1D17654284B07A (void);
// 0x00000332 UnityEngine.Vector2 UnityEngine.Texture::get_texelSize()
extern void Texture_get_texelSize_m89BA9E4CF5276F4FDBAAD6B497809F3E6DB1E30C (void);
// 0x00000333 System.Boolean UnityEngine.Texture::ValidateFormat(UnityEngine.TextureFormat)
extern void Texture_ValidateFormat_m23ED49E24864EE9D1C4EF775002A91EE049561B1 (void);
// 0x00000334 System.Boolean UnityEngine.Texture::ValidateFormat(UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.FormatUsage)
extern void Texture_ValidateFormat_mA62E75B693BFABECB7CB732C165139B8492DE0ED (void);
// 0x00000335 UnityEngine.UnityException UnityEngine.Texture::CreateNonReadableException(UnityEngine.Texture)
extern void Texture_CreateNonReadableException_m66E69BE853119A5A9FE2C27EA788B62BF7CFE34D (void);
// 0x00000336 System.Void UnityEngine.Texture::.cctor()
extern void Texture__cctor_m63D3A3E79E62355737DCF71786623D516FAA07E1 (void);
// 0x00000337 System.Void UnityEngine.Texture::get_texelSize_Injected(UnityEngine.Vector2&)
extern void Texture_get_texelSize_Injected_m812BEA61C30039FF16BE6A2E174C81DCB40000DE (void);
// 0x00000338 UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
extern void Texture2D_get_format_mF0EE5CEB9F84280D4E722B71546BBBA577101E9F (void);
// 0x00000339 UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern void Texture2D_get_whiteTexture_mF447523DE8957109355641ECE0DD3D3C8D2F6C41 (void);
// 0x0000033A UnityEngine.Texture2D UnityEngine.Texture2D::get_blackTexture()
extern void Texture2D_get_blackTexture_mCF4F978DF9B6066794E7130E0C14618216ED0956 (void);
// 0x0000033B UnityEngine.Texture2D UnityEngine.Texture2D::get_redTexture()
extern void Texture2D_get_redTexture_mFE5BD4C3DD841A5AE75164C35298587865C861EC (void);
// 0x0000033C UnityEngine.Texture2D UnityEngine.Texture2D::get_grayTexture()
extern void Texture2D_get_grayTexture_m5E3FE2355FEBF8C9B6286645B4EA32E85C401A45 (void);
// 0x0000033D UnityEngine.Texture2D UnityEngine.Texture2D::get_linearGrayTexture()
extern void Texture2D_get_linearGrayTexture_m5FE6C178A51E5F0AEF92D7D6C7B6EC8C152D257F (void);
// 0x0000033E UnityEngine.Texture2D UnityEngine.Texture2D::get_normalTexture()
extern void Texture2D_get_normalTexture_mD22C382CEC787AED904CFA73E64609DEA3D2B9B2 (void);
// 0x0000033F System.Void UnityEngine.Texture2D::Compress(System.Boolean)
extern void Texture2D_Compress_m3D0191F151DF6D66F312D35FF76BDDE92A3ED395 (void);
// 0x00000340 System.Boolean UnityEngine.Texture2D::Internal_CreateImpl(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Texture2D_Internal_CreateImpl_mE77AB26318BC128ABD08D138FD3EF3A9954F8A5C (void);
// 0x00000341 System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Texture2D_Internal_Create_mC33D3C13F046ADDAF417534E99A741FCF96538B1 (void);
// 0x00000342 System.Boolean UnityEngine.Texture2D::get_isReadable()
extern void Texture2D_get_isReadable_mCF446A169E1D8BF244235F4E9B35DDC4F5E696AD (void);
// 0x00000343 System.Void UnityEngine.Texture2D::ApplyImpl(System.Boolean,System.Boolean)
extern void Texture2D_ApplyImpl_mEFE072AD63B1B7B317225DAEDDB1FF9012DB398B (void);
// 0x00000344 System.Boolean UnityEngine.Texture2D::ResizeImpl(System.Int32,System.Int32)
extern void Texture2D_ResizeImpl_m92009FC5D076C295BA1DECBA9F4A21A678A425C3 (void);
// 0x00000345 System.Void UnityEngine.Texture2D::SetPixelImpl(System.Int32,System.Int32,System.Int32,UnityEngine.Color)
extern void Texture2D_SetPixelImpl_m62B6A2252516D1D5E09701BA550D72ACB79E328B (void);
// 0x00000346 UnityEngine.Color UnityEngine.Texture2D::GetPixelImpl(System.Int32,System.Int32,System.Int32)
extern void Texture2D_GetPixelImpl_mFC45AE4B9B8B88E595CF6EE0FFF751682A70B76D (void);
// 0x00000347 UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinearImpl(System.Int32,System.Single,System.Single)
extern void Texture2D_GetPixelBilinearImpl_m950AB40E4151F10B6AA6C5759903BA07348114FB (void);
// 0x00000348 System.Boolean UnityEngine.Texture2D::ResizeWithFormatImpl(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture2D_ResizeWithFormatImpl_mA7D86CB3F93C5B34F4F728C3AF4BABC9FC8C42BC (void);
// 0x00000349 System.Void UnityEngine.Texture2D::ReadPixelsImpl(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
extern void Texture2D_ReadPixelsImpl_m8D38B7E5F239AD6A9BE8B7732A9CB5ABF35CD0EA (void);
// 0x0000034A System.Void UnityEngine.Texture2D::SetPixelsImpl(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32,System.Int32)
extern void Texture2D_SetPixelsImpl_m8102FB784092E318608E2BC2BB92A82D1C0C9AD1 (void);
// 0x0000034B System.Boolean UnityEngine.Texture2D::LoadRawTextureDataImpl(System.IntPtr,System.Int32)
extern void Texture2D_LoadRawTextureDataImpl_mFE1F3F88739E577DD302C4BD1E865DFF08516DA8 (void);
// 0x0000034C System.Boolean UnityEngine.Texture2D::LoadRawTextureDataImplArray(System.Byte[])
extern void Texture2D_LoadRawTextureDataImplArray_m57FC49A2675C72881C9AD1FE27E96277F892E70F (void);
// 0x0000034D System.Boolean UnityEngine.Texture2D::SetPixelDataImplArray(System.Array,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Texture2D_SetPixelDataImplArray_mD40E91DCFADDE9FDC31E1CEB6AE92E872FDF8027 (void);
// 0x0000034E System.Boolean UnityEngine.Texture2D::SetPixelDataImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Texture2D_SetPixelDataImpl_m26A489526F9844C7F5BB5571C532157F3F372472 (void);
// 0x0000034F System.IntPtr UnityEngine.Texture2D::GetWritableImageData(System.Int32)
extern void Texture2D_GetWritableImageData_m68888C2D5A3E017101E4C8DE6538D5031034DAFF (void);
// 0x00000350 System.Int64 UnityEngine.Texture2D::GetRawImageDataSize()
extern void Texture2D_GetRawImageDataSize_m1CA6CE6DF0120CD36211E07896448A4CD2DE7F10 (void);
// 0x00000351 System.Void UnityEngine.Texture2D::GenerateAtlasImpl(UnityEngine.Vector2[],System.Int32,System.Int32,UnityEngine.Rect[])
extern void Texture2D_GenerateAtlasImpl_mF9ED8E83B1540A1B142109DC0AFF2570D5B58A1F (void);
// 0x00000352 System.Boolean UnityEngine.Texture2D::get_isPreProcessed()
extern void Texture2D_get_isPreProcessed_mFB94271FD8FE6F56FE6866FE02288928840E0250 (void);
// 0x00000353 System.Boolean UnityEngine.Texture2D::get_streamingMipmaps()
extern void Texture2D_get_streamingMipmaps_m78EB1DE0F6E0FCD92969212C0EB98795ECC991E8 (void);
// 0x00000354 System.Int32 UnityEngine.Texture2D::get_streamingMipmapsPriority()
extern void Texture2D_get_streamingMipmapsPriority_mC2143F53438E36E24E36A9DF6CC41EDEECF13754 (void);
// 0x00000355 System.Int32 UnityEngine.Texture2D::get_requestedMipmapLevel()
extern void Texture2D_get_requestedMipmapLevel_mB3AF7B1155A3D96A9E24EC0405FD6A2C290B632A (void);
// 0x00000356 System.Void UnityEngine.Texture2D::set_requestedMipmapLevel(System.Int32)
extern void Texture2D_set_requestedMipmapLevel_mBA1AA7ACBBEDEA8444570529EC1593C46532D6CF (void);
// 0x00000357 System.Int32 UnityEngine.Texture2D::get_minimumMipmapLevel()
extern void Texture2D_get_minimumMipmapLevel_m1DD6F0FD301819B0F019C4A4CFE8209D91E91122 (void);
// 0x00000358 System.Void UnityEngine.Texture2D::set_minimumMipmapLevel(System.Int32)
extern void Texture2D_set_minimumMipmapLevel_mBF3942CAB2C76B2D266F94CEAE38039C364AF0E2 (void);
// 0x00000359 System.Boolean UnityEngine.Texture2D::get_loadAllMips()
extern void Texture2D_get_loadAllMips_mD01E6A5A83E30C7351FBAB2105343722E24F1DBB (void);
// 0x0000035A System.Void UnityEngine.Texture2D::set_loadAllMips(System.Boolean)
extern void Texture2D_set_loadAllMips_m2C10D1DDA8C72B026A61AB8F4083791AD409498C (void);
// 0x0000035B System.Int32 UnityEngine.Texture2D::get_calculatedMipmapLevel()
extern void Texture2D_get_calculatedMipmapLevel_m7D1062D26A6428FFB63A1D60C7E52BF6F6655959 (void);
// 0x0000035C System.Int32 UnityEngine.Texture2D::get_desiredMipmapLevel()
extern void Texture2D_get_desiredMipmapLevel_mC18FC94ABAD4C2673967DF7D9EDB230474DB27A0 (void);
// 0x0000035D System.Int32 UnityEngine.Texture2D::get_loadingMipmapLevel()
extern void Texture2D_get_loadingMipmapLevel_m91264A61AAF4C34A67241B8EE8170E15C21167C6 (void);
// 0x0000035E System.Int32 UnityEngine.Texture2D::get_loadedMipmapLevel()
extern void Texture2D_get_loadedMipmapLevel_m8EFC1E5785D2CBAF8223927DD0E40FF9FF3A4B88 (void);
// 0x0000035F System.Void UnityEngine.Texture2D::ClearRequestedMipmapLevel()
extern void Texture2D_ClearRequestedMipmapLevel_m3C04AADC29D99C5987E97FCF93C114DCED1335B6 (void);
// 0x00000360 System.Boolean UnityEngine.Texture2D::IsRequestedMipmapLevelLoaded()
extern void Texture2D_IsRequestedMipmapLevelLoaded_m4270351626122999CFB097181E7726DE7E641C22 (void);
// 0x00000361 System.Void UnityEngine.Texture2D::ClearMinimumMipmapLevel()
extern void Texture2D_ClearMinimumMipmapLevel_m03B34D86447F0F423C01B471E466999A27F88BF2 (void);
// 0x00000362 System.Void UnityEngine.Texture2D::UpdateExternalTexture(System.IntPtr)
extern void Texture2D_UpdateExternalTexture_m8A3486B0779454739624888EFD6D9EC42D30874F (void);
// 0x00000363 System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
extern void Texture2D_SetAllPixels32_m7A55BBBC1A890DCFC167F1BB4335F3EA5EEB75D9 (void);
// 0x00000364 System.Void UnityEngine.Texture2D::SetBlockOfPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern void Texture2D_SetBlockOfPixels32_m884C7DFC283BD8B383B7FF611453DBD93215C4C4 (void);
// 0x00000365 System.Byte[] UnityEngine.Texture2D::GetRawTextureData()
extern void Texture2D_GetRawTextureData_m387AAB1686E27DA77F4065A2111DF18934AFB364 (void);
// 0x00000366 UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Texture2D_GetPixels_mBCF24F58376C64F7326040FFEBD6234D7F8FDE6F (void);
// 0x00000367 UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Texture2D_GetPixels_mE12FF280416D83D18B81E07F04BD1080A0F5009D (void);
// 0x00000368 UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32(System.Int32)
extern void Texture2D_GetPixels32_mD84F05C717774887B5EF65ADEE82045A3E2FBD75 (void);
// 0x00000369 UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32()
extern void Texture2D_GetPixels32_m7CC6EC6AD48D4CD84AF28DFDFBE24750900FA2E6 (void);
// 0x0000036A UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32,System.Int32,System.Boolean)
extern void Texture2D_PackTextures_mAC9D9D58AD2F653BA79CCAEC4D92ECE29068435F (void);
// 0x0000036B UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32,System.Int32)
extern void Texture2D_PackTextures_m6ACE150804F05ED53F6A75134EF853E7A571DC65 (void);
// 0x0000036C UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32)
extern void Texture2D_PackTextures_m30EDF78F44C0058966BA40BFD5594D8501E4235A (void);
// 0x0000036D System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32,System.IntPtr)
extern void Texture2D__ctor_m33959787D1B4602D4D0487F25660935182076C64 (void);
// 0x0000036E System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2D__ctor_mA9BD76C2123DF372D9D66C03DD6F65F114FA35BC (void);
// 0x0000036F System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2D__ctor_m11E5D67EEE90F8C673E8E0E3A4C41FFE10FCC7EB (void);
// 0x00000370 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2D__ctor_m25840EA9858514192E0E00B5D9CA27B83487903C (void);
// 0x00000371 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean,System.IntPtr)
extern void Texture2D__ctor_mB33D5D6E83136BF8A42D2628405B10BE0889F439 (void);
// 0x00000372 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean)
extern void Texture2D__ctor_mD7A145588D9FEBCF1B435351E0A986174CFC0266 (void);
// 0x00000373 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
extern void Texture2D__ctor_m01B7AF7873AA43495B8216926C1768FEDDF4CE64 (void);
// 0x00000374 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A (void);
// 0x00000375 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern void Texture2D__ctor_m0C86A87871AA8075791EF98499D34DA95ACB0E35 (void);
// 0x00000376 UnityEngine.Texture2D UnityEngine.Texture2D::CreateExternalTexture(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern void Texture2D_CreateExternalTexture_m292548AA116AA5176E8CA03AFBBC088E3B698DE4 (void);
// 0x00000377 System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color)
extern void Texture2D_SetPixel_m8BE87C152447B812D06CB894B3570269CC2DE7C3 (void);
// 0x00000378 System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color,System.Int32)
extern void Texture2D_SetPixel_m9027565ACB3CFE68ED2475761FDE3EF560600B38 (void);
// 0x00000379 System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern void Texture2D_SetPixels_mC768DC908606FE162BF77A8761AD3ED7BAC1612C (void);
// 0x0000037A System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[])
extern void Texture2D_SetPixels_m09295F6D03DF7A4A0EC0A12325A59065365D6F53 (void);
// 0x0000037B System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
extern void Texture2D_SetPixels_mE261D2D072DB4CF447A259731E51291267EEA3AC (void);
// 0x0000037C System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern void Texture2D_SetPixels_mDE50229135F49F323D265340C415D680CCB2FB92 (void);
// 0x0000037D UnityEngine.Color UnityEngine.Texture2D::GetPixel(System.Int32,System.Int32)
extern void Texture2D_GetPixel_m71EA79FE268858ECD782327F8854EF2C2B16B2CD (void);
// 0x0000037E UnityEngine.Color UnityEngine.Texture2D::GetPixel(System.Int32,System.Int32,System.Int32)
extern void Texture2D_GetPixel_m57F166AE1F42E3FFB882C28D0241B2194B258ABF (void);
// 0x0000037F UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
extern void Texture2D_GetPixelBilinear_m3E0E9A22A0989E99A7295BC6FE6999728F290A78 (void);
// 0x00000380 UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single,System.Int32)
extern void Texture2D_GetPixelBilinear_m66ABE9FF3BA0B835119F04880D80F79596B34DF2 (void);
// 0x00000381 System.Void UnityEngine.Texture2D::LoadRawTextureData(System.IntPtr,System.Int32)
extern void Texture2D_LoadRawTextureData_m7F70DFFDBFA1E307A79838B94BC1A0438E063DFC (void);
// 0x00000382 System.Void UnityEngine.Texture2D::LoadRawTextureData(System.Byte[])
extern void Texture2D_LoadRawTextureData_m0331275D060EC3521BCCF84194FA35BBEEF767CB (void);
// 0x00000383 System.Void UnityEngine.Texture2D::LoadRawTextureData(Unity.Collections.NativeArray`1<T>)
// 0x00000384 System.Void UnityEngine.Texture2D::SetPixelData(T[],System.Int32,System.Int32)
// 0x00000385 System.Void UnityEngine.Texture2D::SetPixelData(Unity.Collections.NativeArray`1<T>,System.Int32,System.Int32)
// 0x00000386 Unity.Collections.NativeArray`1<T> UnityEngine.Texture2D::GetRawTextureData()
// 0x00000387 System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern void Texture2D_Apply_mCC17B1895AEB420CF75B1A50A62AB623C225A6C1 (void);
// 0x00000388 System.Void UnityEngine.Texture2D::Apply(System.Boolean)
extern void Texture2D_Apply_m368893ECE2F9659BDA54ED1E4EB00D01CC2D1B16 (void);
// 0x00000389 System.Void UnityEngine.Texture2D::Apply()
extern void Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA (void);
// 0x0000038A System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32)
extern void Texture2D_Resize_m9B3D67DF42A47D5C573D084C080CEDA5E330A8D2 (void);
// 0x0000038B System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture2D_Resize_m3A64BAF31DF4A82284B56C15614CE4FDA0785F7D (void);
// 0x0000038C System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
extern void Texture2D_ReadPixels_m5664E184458C64BA89450F80F47705A2241E9BFE (void);
// 0x0000038D System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
extern void Texture2D_ReadPixels_m1ED1C11E41D0ACC8CFCABBD25946CF0BD16D4F61 (void);
// 0x0000038E System.Boolean UnityEngine.Texture2D::GenerateAtlas(UnityEngine.Vector2[],System.Int32,System.Int32,System.Collections.Generic.List`1<UnityEngine.Rect>)
extern void Texture2D_GenerateAtlas_mA850247B011DCAD8A29C51B3B3C170F7DB3C49FC (void);
// 0x0000038F System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern void Texture2D_SetPixels32_m22195EEABA78058324C83EBF327A7E5EFBEE809F (void);
// 0x00000390 System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
extern void Texture2D_SetPixels32_m8669AE290D19897A70859BE23D9A438EB7EDA67E (void);
// 0x00000391 System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern void Texture2D_SetPixels32_m0FD3D2A4909D3948A389AC65EC052156C34A62D8 (void);
// 0x00000392 System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[])
extern void Texture2D_SetPixels32_m515E77577D02B697645BD64D6E6AF32646B7975B (void);
// 0x00000393 UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32)
extern void Texture2D_GetPixels_m12211168B2F47A4D4F920E8D52B04509BBA67006 (void);
// 0x00000394 UnityEngine.Color[] UnityEngine.Texture2D::GetPixels()
extern void Texture2D_GetPixels_mE87C4C2438D7DE39C50EC1C91E438BB15026BBE3 (void);
// 0x00000395 System.Void UnityEngine.Texture2D::SetPixelImpl_Injected(System.Int32,System.Int32,System.Int32,UnityEngine.Color&)
extern void Texture2D_SetPixelImpl_Injected_m21934648C4DE2227463F16A67EFA3DC740682ACC (void);
// 0x00000396 System.Void UnityEngine.Texture2D::GetPixelImpl_Injected(System.Int32,System.Int32,System.Int32,UnityEngine.Color&)
extern void Texture2D_GetPixelImpl_Injected_mA54A70585DC14F8F87449E931121507ADABA2CE6 (void);
// 0x00000397 System.Void UnityEngine.Texture2D::GetPixelBilinearImpl_Injected(System.Int32,System.Single,System.Single,UnityEngine.Color&)
extern void Texture2D_GetPixelBilinearImpl_Injected_m120BD9810D176C39E874FFDAF53AD3AC3B6ADF85 (void);
// 0x00000398 System.Void UnityEngine.Texture2D::ReadPixelsImpl_Injected(UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
extern void Texture2D_ReadPixelsImpl_Injected_m8D0C78900FF4C61ED9D6DAF548C3F397FB55D91D (void);
// 0x00000399 System.Boolean UnityEngine.Cubemap::Internal_CreateImpl(UnityEngine.Cubemap,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Cubemap_Internal_CreateImpl_m2E42502B311D4511987453F591F469EFD3D46C7E (void);
// 0x0000039A System.Void UnityEngine.Cubemap::Internal_Create(UnityEngine.Cubemap,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Cubemap_Internal_Create_m7D1672F9247A6CA2578874A69C901311B6196289 (void);
// 0x0000039B System.Boolean UnityEngine.Cubemap::get_isReadable()
extern void Cubemap_get_isReadable_m97956094F4DBC9C67A86AEC8CCE73AB237694121 (void);
// 0x0000039C System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Cubemap__ctor_mA198007748E1B40309793BFD41C6DA8506BFC36E (void);
// 0x0000039D System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Cubemap__ctor_mC713C6EC5AA4BB7091AF19FC75E1A5D3A133550B (void);
// 0x0000039E System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Int32)
extern void Cubemap__ctor_m823CBFD84E8497FEEDE6858F1781ADECB0C6CFBF (void);
// 0x0000039F System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32)
extern void Cubemap__ctor_mDEAB11F63268FC5F1115D928499AC270F21FB249 (void);
// 0x000003A0 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Int32,System.IntPtr)
extern void Cubemap__ctor_mFC82AF58FF4875D6750838AF47A05D5B203523A8 (void);
// 0x000003A1 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Boolean,System.IntPtr)
extern void Cubemap__ctor_m619C9524BF966423D2DE66E878C824113616C371 (void);
// 0x000003A2 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Cubemap__ctor_mB0430DC19209C90736915B41A670C7AC65698D71 (void);
// 0x000003A3 System.Boolean UnityEngine.Texture3D::get_isReadable()
extern void Texture3D_get_isReadable_m2793D34A645AA2A88B903D7C0CBC05BC180A489F (void);
// 0x000003A4 System.Boolean UnityEngine.Texture3D::Internal_CreateImpl(UnityEngine.Texture3D,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture3D_Internal_CreateImpl_m6B7F1E0A4F0A8DF201C84B8F5EBE88F5BC2D0ED5 (void);
// 0x000003A5 System.Void UnityEngine.Texture3D::Internal_Create(UnityEngine.Texture3D,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture3D_Internal_Create_mCE8880719B54D2E8426234438DF2BA893B16CAA5 (void);
// 0x000003A6 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture3D__ctor_m3819CE6527C761C3514E46566BAE8D09CEE6C6C0 (void);
// 0x000003A7 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture3D__ctor_m080D4201C72C73ECB718F44491858309CDCCBF40 (void);
// 0x000003A8 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32)
extern void Texture3D__ctor_m9FB382B0BC5C568B195C9E27E7BCA34C108F5FF7 (void);
// 0x000003A9 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32)
extern void Texture3D__ctor_m13ADB707E9EA6970AE5D6A5DDF8A5DAAD88DD2B0 (void);
// 0x000003AA System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture3D__ctor_m7086160504490544C327FF1C7823830B44441466 (void);
// 0x000003AB System.Boolean UnityEngine.Texture2DArray::get_isReadable()
extern void Texture2DArray_get_isReadable_mB2E454ED94BB334E77B42E6CD9DABC0B1D679C1A (void);
// 0x000003AC System.Boolean UnityEngine.Texture2DArray::Internal_CreateImpl(UnityEngine.Texture2DArray,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray_Internal_CreateImpl_mF6D0DD31CE06DB61D0E3C8D875F20692B33C776E (void);
// 0x000003AD System.Void UnityEngine.Texture2DArray::Internal_Create(UnityEngine.Texture2DArray,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray_Internal_Create_m53A30DE93DCDA75588C999A967F8004AB0EE113F (void);
// 0x000003AE System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray__ctor_m92A39957ECC1DBE79437D3849A1FA7A98615A9F0 (void);
// 0x000003AF System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray__ctor_mD92521FF6DA05FF47471B741DDC7E4D5B3C3F4E2 (void);
// 0x000003B0 System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32)
extern void Texture2DArray__ctor_mF94531ED3A27A6583DCACE742D6D6A56C3B1CB76 (void);
// 0x000003B1 System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean)
extern void Texture2DArray__ctor_m982669D0408998D2038575A421440A8D537D67E6 (void);
// 0x000003B2 System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
extern void Texture2DArray__ctor_mEDE73B65A89EACA4B487FFBA92B155ED5B09970F (void);
// 0x000003B3 System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture2DArray__ctor_mE0F6B7F60470C479258E1CC295456BCA103E66BF (void);
// 0x000003B4 System.Boolean UnityEngine.CubemapArray::get_isReadable()
extern void CubemapArray_get_isReadable_mBE24F088422FA9FE007086C36C7D16A6D6377919 (void);
// 0x000003B5 System.Boolean UnityEngine.CubemapArray::Internal_CreateImpl(UnityEngine.CubemapArray,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray_Internal_CreateImpl_mDA1FF7D490A441C86198448B72B62C2D38B9A046 (void);
// 0x000003B6 System.Void UnityEngine.CubemapArray::Internal_Create(UnityEngine.CubemapArray,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray_Internal_Create_m2503EFCE0A71CBCCCA87C93E15B9F83709274A58 (void);
// 0x000003B7 System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray__ctor_m44E378D2D09F711CF0AEF479DC7D12426C449CF6 (void);
// 0x000003B8 System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray__ctor_m390539598EAAEE1AAE0B89D2241A60EE6BD1B219 (void);
// 0x000003B9 System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32)
extern void CubemapArray__ctor_m88D0AB083EEF112A636EE307337BAFAF036E0A2B (void);
// 0x000003BA System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean)
extern void CubemapArray__ctor_m1FC2738B93636229EC645E15D36C9A3F67FE0E54 (void);
// 0x000003BB System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
extern void CubemapArray__ctor_mE9E5A417064CB9CF4283C8A82F4AE5C463C4014E (void);
// 0x000003BC System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void CubemapArray__ctor_mD52A7D884A01A8DF05B40D820584C1F3869317AC (void);
// 0x000003BD System.Int32 UnityEngine.RenderTexture::get_width()
extern void RenderTexture_get_width_m246F1304B26711BE9D18EE186F130590B9EDADDB (void);
// 0x000003BE System.Void UnityEngine.RenderTexture::set_width(System.Int32)
extern void RenderTexture_set_width_m10EF29F6167493A1C5826869ACB10EA9C7A5BDCE (void);
// 0x000003BF System.Int32 UnityEngine.RenderTexture::get_height()
extern void RenderTexture_get_height_m26FBCCE11E1C6C9A47566CC4421258BAF5F35CE6 (void);
// 0x000003C0 System.Void UnityEngine.RenderTexture::set_height(System.Int32)
extern void RenderTexture_set_height_m4402FEFDF6D35ABE3FBA485FE9A144FB4204B561 (void);
// 0x000003C1 System.Void UnityEngine.RenderTexture::set_graphicsFormat(UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void RenderTexture_set_graphicsFormat_mF60798F07D5994C46C53826F8A8F2E553B8F85CF (void);
// 0x000003C2 System.Void UnityEngine.RenderTexture::SetSRGBReadWrite(System.Boolean)
extern void RenderTexture_SetSRGBReadWrite_mD553522060790CB4984886D2508B79897C3DC2DE (void);
// 0x000003C3 System.Void UnityEngine.RenderTexture::Internal_Create(UnityEngine.RenderTexture)
extern void RenderTexture_Internal_Create_m924B30E7AFD36150F0279057C3A3869D94D7646A (void);
// 0x000003C4 System.Void UnityEngine.RenderTexture::SetRenderTextureDescriptor(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture_SetRenderTextureDescriptor_mF584353E0834F202C955EAC6499CBD0C6A571527 (void);
// 0x000003C5 UnityEngine.RenderTextureDescriptor UnityEngine.RenderTexture::GetDescriptor()
extern void RenderTexture_GetDescriptor_mBDAE7C44038663205A31293B7C4C5AE763CD1128 (void);
// 0x000003C6 System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern void RenderTexture_set_depth_mD4BCB0A9251B7FCF570459A705E03FFFEA4DB3B0 (void);
// 0x000003C7 System.Void UnityEngine.RenderTexture::.ctor()
extern void RenderTexture__ctor_mFB50DBD262C99B38016F518AA0FBF753FE22D13A (void);
// 0x000003C8 System.Void UnityEngine.RenderTexture::.ctor(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture__ctor_mEC30DF610263D5A16EC16E34BD86AD4BC0C87A9B (void);
// 0x000003C9 System.Void UnityEngine.RenderTexture::.ctor(UnityEngine.RenderTexture)
extern void RenderTexture__ctor_m3B3534A6C9696C5CB12ADC78F922237F69CEA33A (void);
// 0x000003CA System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat)
extern void RenderTexture__ctor_mBAE127BF530990C9C8DBF5E155BA05A068777129 (void);
// 0x000003CB System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void RenderTexture__ctor_m7C2F727F747019FC14CF7FB5FF5C29A349F9FCD9 (void);
// 0x000003CC System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32)
extern void RenderTexture__ctor_m2DD279268931EC6E725CAB584DC7D499E4BDCB82 (void);
// 0x000003CD System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern void RenderTexture__ctor_m32060CA5A5C306C485DB6AF9B9050B2FF2AB3A4C (void);
// 0x000003CE System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern void RenderTexture__ctor_m0FF5DDAB599ED301091CF23D4C76691D8EC70CA5 (void);
// 0x000003CF System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern void RenderTexture__ctor_mB54A3ABBD56D38AB762D0AB8B789E2771BC42A7D (void);
// 0x000003D0 System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,System.Int32)
extern void RenderTexture__ctor_mDB3D67FD662C79D27F0C42C6DBA1A7EFF80C2D1E (void);
// 0x000003D1 UnityEngine.RenderTextureDescriptor UnityEngine.RenderTexture::get_descriptor()
extern void RenderTexture_get_descriptor_m67E7BCFA6A50634F6E3863E2F5BA1D4923E4DD00 (void);
// 0x000003D2 System.Void UnityEngine.RenderTexture::set_descriptor(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture_set_descriptor_m88048ECD2E447B3549D59032F5F9B52D4CA3D528 (void);
// 0x000003D3 System.Void UnityEngine.RenderTexture::ValidateRenderTextureDesc(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture_ValidateRenderTextureDesc_mE4BA319BF91FCA90B517EF080E84EE395A4EAD01 (void);
// 0x000003D4 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTexture::GetCompatibleFormat(UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern void RenderTexture_GetCompatibleFormat_m1C84A2875CC17182DEB4CC7EF277E7089B160095 (void);
// 0x000003D5 System.Void UnityEngine.RenderTexture::SetRenderTextureDescriptor_Injected(UnityEngine.RenderTextureDescriptor&)
extern void RenderTexture_SetRenderTextureDescriptor_Injected_m72AC0A28D6BF9041721D95E43BAC302A56C99019 (void);
// 0x000003D6 System.Void UnityEngine.RenderTexture::GetDescriptor_Injected(UnityEngine.RenderTextureDescriptor&)
extern void RenderTexture_GetDescriptor_Injected_m9A137437A3EAD31E2AE4BC123329BF3945B22A64 (void);
// 0x000003D7 System.Int32 UnityEngine.RenderTextureDescriptor::get_width()
extern void RenderTextureDescriptor_get_width_m225FBFD7C33BD02D6879A93F1D57997BC251F3F5 (void);
// 0x000003D8 System.Void UnityEngine.RenderTextureDescriptor::set_width(System.Int32)
extern void RenderTextureDescriptor_set_width_m48ADD4AB04E8DBEEC5CA8CC96F86D2674F4FE55F (void);
// 0x000003D9 System.Int32 UnityEngine.RenderTextureDescriptor::get_height()
extern void RenderTextureDescriptor_get_height_m947A620B3D28090A57A5DC0D6A126CBBF818B97F (void);
// 0x000003DA System.Void UnityEngine.RenderTextureDescriptor::set_height(System.Int32)
extern void RenderTextureDescriptor_set_height_mD19D74EC9679250F63489CF1950351EFA83A1A45 (void);
// 0x000003DB System.Int32 UnityEngine.RenderTextureDescriptor::get_msaaSamples()
extern void RenderTextureDescriptor_get_msaaSamples_mEBE0D743E17068D1898DAE2D281C913E39A33616 (void);
// 0x000003DC System.Void UnityEngine.RenderTextureDescriptor::set_msaaSamples(System.Int32)
extern void RenderTextureDescriptor_set_msaaSamples_m5856FC43DAD667D4462B6BCA938B70E42068D24C (void);
// 0x000003DD System.Int32 UnityEngine.RenderTextureDescriptor::get_volumeDepth()
extern void RenderTextureDescriptor_get_volumeDepth_mBC82F4621E4158E3D2F2457487D1C220AA782AC6 (void);
// 0x000003DE System.Void UnityEngine.RenderTextureDescriptor::set_volumeDepth(System.Int32)
extern void RenderTextureDescriptor_set_volumeDepth_mDC402E6967166BE8CADDA690B32136A1E0AB8583 (void);
// 0x000003DF System.Void UnityEngine.RenderTextureDescriptor::set_mipCount(System.Int32)
extern void RenderTextureDescriptor_set_mipCount_m98C1CE257A8152D8B23EC27D68D0C4C35683DEE5 (void);
// 0x000003E0 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::get_graphicsFormat()
extern void RenderTextureDescriptor_get_graphicsFormat_mD2DD8AC2E1324779F8D497697E725FB93341A14D (void);
// 0x000003E1 System.Void UnityEngine.RenderTextureDescriptor::set_graphicsFormat(UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void RenderTextureDescriptor_set_graphicsFormat_mF24C183BA9C9C42923CEC3ED353661D54AA741CA (void);
// 0x000003E2 System.Int32 UnityEngine.RenderTextureDescriptor::get_depthBufferBits()
extern void RenderTextureDescriptor_get_depthBufferBits_m51E82C47A0CA0BD8B20F90D43169C956C4F24996 (void);
// 0x000003E3 System.Void UnityEngine.RenderTextureDescriptor::set_depthBufferBits(System.Int32)
extern void RenderTextureDescriptor_set_depthBufferBits_mED58A8D9643740713597B0244BF76D0395C1C479 (void);
// 0x000003E4 System.Void UnityEngine.RenderTextureDescriptor::set_dimension(UnityEngine.Rendering.TextureDimension)
extern void RenderTextureDescriptor_set_dimension_mBC3C8793345AC5E627BDD932B46A5F93568ACCE5 (void);
// 0x000003E5 System.Void UnityEngine.RenderTextureDescriptor::set_shadowSamplingMode(UnityEngine.Rendering.ShadowSamplingMode)
extern void RenderTextureDescriptor_set_shadowSamplingMode_m0AC43F8A8BE0755567EED4DDFADBE207739E1ECF (void);
// 0x000003E6 System.Void UnityEngine.RenderTextureDescriptor::set_vrUsage(UnityEngine.VRTextureUsage)
extern void RenderTextureDescriptor_set_vrUsage_mC591604135749B0BD156865141E114F51812E502 (void);
// 0x000003E7 System.Void UnityEngine.RenderTextureDescriptor::set_memoryless(UnityEngine.RenderTextureMemoryless)
extern void RenderTextureDescriptor_set_memoryless_m1FB3F7E8B482C71BDB549AD71D762BCF337D8185 (void);
// 0x000003E8 System.Void UnityEngine.RenderTextureDescriptor::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32,System.Int32)
extern void RenderTextureDescriptor__ctor_m227EBED323830B1059806874C69E1426DDC16B85 (void);
// 0x000003E9 System.Void UnityEngine.RenderTextureDescriptor::SetOrClearRenderTextureCreationFlag(System.Boolean,UnityEngine.RenderTextureCreationFlags)
extern void RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m387E338D17ED601B0587EF4E5CEB10DDFC42CC05 (void);
// 0x000003EA System.Void UnityEngine.RenderTextureDescriptor::.cctor()
extern void RenderTextureDescriptor__cctor_mA5675E6684E1E8C26E848D2390FB2ABE8E3C11FD (void);
// 0x000003EB System.Boolean UnityEngine.Hash128::get_isValid()
extern void Hash128_get_isValid_m45005B82BA8416D4ACA8B20DF9DA5B8513C602B3 (void);
// 0x000003EC System.Int32 UnityEngine.Hash128::CompareTo(UnityEngine.Hash128)
extern void Hash128_CompareTo_m0BC4F8F4228CF2B48C615E39CD3CE260386CC5BC (void);
// 0x000003ED System.String UnityEngine.Hash128::ToString()
extern void Hash128_ToString_mD81890597BCFD67BE47646DA5366347C545EB5E8 (void);
// 0x000003EE UnityEngine.Hash128 UnityEngine.Hash128::Parse(System.String)
extern void Hash128_Parse_mC361CD25A0718BCA7360712283AD2ABD74E26277 (void);
// 0x000003EF System.String UnityEngine.Hash128::Internal_Hash128ToString(UnityEngine.Hash128)
extern void Hash128_Internal_Hash128ToString_m0C8F22C98723DA1488DC4FC86886662AC030E798 (void);
// 0x000003F0 UnityEngine.Hash128 UnityEngine.Hash128::Compute(System.String)
extern void Hash128_Compute_m23D2DFFD9496D37DB57C99056580792850772F29 (void);
// 0x000003F1 System.Boolean UnityEngine.Hash128::Equals(System.Object)
extern void Hash128_Equals_m4701FDD64B5C5F81B1E494D5586C5CB4519BC007 (void);
// 0x000003F2 System.Boolean UnityEngine.Hash128::Equals(UnityEngine.Hash128)
extern void Hash128_Equals_m85BCDD87EB2A629FB8BB72FD63244787BC47E52D (void);
// 0x000003F3 System.Int32 UnityEngine.Hash128::GetHashCode()
extern void Hash128_GetHashCode_m65DA1711C64E83AB514A3D498C568CB10EAA0D69 (void);
// 0x000003F4 System.Int32 UnityEngine.Hash128::CompareTo(System.Object)
extern void Hash128_CompareTo_m3595697B0FC7ACAED77C03FEC7FF80A073A4F6AE (void);
// 0x000003F5 System.Boolean UnityEngine.Hash128::op_Equality(UnityEngine.Hash128,UnityEngine.Hash128)
extern void Hash128_op_Equality_m77950F7840A081CBFE1D01A2B72DA521A4AF5065 (void);
// 0x000003F6 System.Boolean UnityEngine.Hash128::op_LessThan(UnityEngine.Hash128,UnityEngine.Hash128)
extern void Hash128_op_LessThan_m2F98815644822762B642B483F79A72CFB4BADB39 (void);
// 0x000003F7 System.Boolean UnityEngine.Hash128::op_GreaterThan(UnityEngine.Hash128,UnityEngine.Hash128)
extern void Hash128_op_GreaterThan_m1B6A95E0C9A75EE36E33D0E75ADB0D1872B843A1 (void);
// 0x000003F8 System.Void UnityEngine.Hash128::Parse_Injected(System.String,UnityEngine.Hash128&)
extern void Hash128_Parse_Injected_m881C4C2F88C4603BF396695B239CA78F57D20A11 (void);
// 0x000003F9 System.String UnityEngine.Hash128::Internal_Hash128ToString_Injected(UnityEngine.Hash128&)
extern void Hash128_Internal_Hash128ToString_Injected_m6B07AAF4C1F86D9B7E482FD030AFB9CB15527583 (void);
// 0x000003FA System.Void UnityEngine.Hash128::Compute_Injected(System.String,UnityEngine.Hash128&)
extern void Hash128_Compute_Injected_m6922A480AC8B1DADA1D9D708290F683E89BCD23D (void);
// 0x000003FB System.Boolean UnityEngine.Cursor::get_visible()
extern void Cursor_get_visible_m0BB8BC7FEDD558FB661E9023AB8C04FFEE41377C (void);
// 0x000003FC System.Void UnityEngine.Cursor::set_visible(System.Boolean)
extern void Cursor_set_visible_mDB51E60B3D7B14873A6F5FBE5E0A432D4A46C431 (void);
// 0x000003FD UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
extern void Cursor_get_lockState_mE0C93F496E3AA120AD168ED30371C35ED79C9DF1 (void);
// 0x000003FE System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
extern void Cursor_set_lockState_m019E27A0FE021A28A1C672801416ACA5E770933F (void);
// 0x000003FF System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
// 0x00000400 System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object)
// 0x00000401 UnityEngine.ILogHandler UnityEngine.ILogger::get_logHandler()
// 0x00000402 System.Boolean UnityEngine.ILogger::get_logEnabled()
// 0x00000403 System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object)
// 0x00000404 System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object)
// 0x00000405 System.Void UnityEngine.ILogger::LogError(System.String,System.Object)
// 0x00000406 System.Void UnityEngine.ILogger::LogFormat(UnityEngine.LogType,System.String,System.Object[])
// 0x00000407 System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern void Logger__ctor_m447215F90AA8D1508924BFB1CD1E49AFCCE04FFE (void);
// 0x00000408 UnityEngine.ILogHandler UnityEngine.Logger::get_logHandler()
extern void Logger_get_logHandler_m0C450B6FE86FA1B07422A1359CF278D1F5945CCD (void);
// 0x00000409 System.Void UnityEngine.Logger::set_logHandler(UnityEngine.ILogHandler)
extern void Logger_set_logHandler_m891099050CB8C36A01ECA861E4820441B7080C73 (void);
// 0x0000040A System.Boolean UnityEngine.Logger::get_logEnabled()
extern void Logger_get_logEnabled_m5AE50375671516B8E1171838E34C63C3A326E927 (void);
// 0x0000040B System.Void UnityEngine.Logger::set_logEnabled(System.Boolean)
extern void Logger_set_logEnabled_m3CEBD8A4B6DC548168EC38CFC5CB982222DB655F (void);
// 0x0000040C UnityEngine.LogType UnityEngine.Logger::get_filterLogType()
extern void Logger_get_filterLogType_mD850DE3FD8CC67E4B076FCC6B05F8FA603B5B38B (void);
// 0x0000040D System.Void UnityEngine.Logger::set_filterLogType(UnityEngine.LogType)
extern void Logger_set_filterLogType_m1829D8E1B8350B8CFCE881598BABB2AA1826EE72 (void);
// 0x0000040E System.Boolean UnityEngine.Logger::IsLogTypeAllowed(UnityEngine.LogType)
extern void Logger_IsLogTypeAllowed_m7B2A793443A642C8CA0759D1DD0FBDFAED4E1FCB (void);
// 0x0000040F System.String UnityEngine.Logger::GetString(System.Object)
extern void Logger_GetString_m0078CBA4ADAE877152C264A4BEA5408BAB1DC514 (void);
// 0x00000410 System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object)
extern void Logger_Log_m653FDC5B68CB933887D8886762C7BBA75243A9AF (void);
// 0x00000411 System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object)
extern void Logger_Log_mE06FF98F674C73E4BB67302E1EEDEA72F7655FF0 (void);
// 0x00000412 System.Void UnityEngine.Logger::LogError(System.String,System.Object)
extern void Logger_LogError_mE1B376E31566453DEEF94DF2537E1E4ED1F25C58 (void);
// 0x00000413 System.Void UnityEngine.Logger::LogException(System.Exception,UnityEngine.Object)
extern void Logger_LogException_m362D3434D3B275B0B98E434BFBFBF52C76BBC9C3 (void);
// 0x00000414 System.Void UnityEngine.Logger::LogFormat(UnityEngine.LogType,System.String,System.Object[])
extern void Logger_LogFormat_m7F6CBF8AF6A60EF05CF9EE795502036CBAC847A7 (void);
// 0x00000415 System.Void UnityEngine.Logger::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern void Logger_LogFormat_m549605E9E6499650E70B0A168E047727490F31B7 (void);
// 0x00000416 System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)
extern void UnityLogWriter_WriteStringToUnityLog_m0036CA8A9FB1FE3CFF460CA0212B6377B09E6504 (void);
// 0x00000417 System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLogImpl(System.String)
extern void UnityLogWriter_WriteStringToUnityLogImpl_mA39CCE94FF5BD2ABD4A8C8D78A00E366C64B4985 (void);
// 0x00000418 System.Void UnityEngine.UnityLogWriter::Init()
extern void UnityLogWriter_Init_mAD1F3BFE2183E39CFA1E7BEFB948B368547D9E99 (void);
// 0x00000419 System.Text.Encoding UnityEngine.UnityLogWriter::get_Encoding()
extern void UnityLogWriter_get_Encoding_m0FEB104679588C939C4FD906480D1EDD3FD94110 (void);
// 0x0000041A System.Void UnityEngine.UnityLogWriter::Write(System.Char)
extern void UnityLogWriter_Write_mB1200B0B26545C48E178BFE952BEE14BDE53D2A7 (void);
// 0x0000041B System.Void UnityEngine.UnityLogWriter::Write(System.String)
extern void UnityLogWriter_Write_mE3A4616A06A79B87512C3B0C8100EB508BB85C52 (void);
// 0x0000041C System.Void UnityEngine.UnityLogWriter::Write(System.Char[],System.Int32,System.Int32)
extern void UnityLogWriter_Write_mE21873E7757E51C3771C58321E995DEBB2ADF750 (void);
// 0x0000041D System.Void UnityEngine.UnityLogWriter::.ctor()
extern void UnityLogWriter__ctor_mE8DC0EAD466C5F290F6D32CC07F0F70590688833 (void);
// 0x0000041E System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C (void);
// 0x0000041F System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern void Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369 (void);
// 0x00000420 System.String UnityEngine.Color::ToString()
extern void Color_ToString_m17A27E0CFB20D9946D130DAEDB5BDCACA5A4473F (void);
// 0x00000421 System.Int32 UnityEngine.Color::GetHashCode()
extern void Color_GetHashCode_m88317C719D2DAA18E293B3F5CD17B9FB80E26CF1 (void);
// 0x00000422 System.Boolean UnityEngine.Color::Equals(System.Object)
extern void Color_Equals_m63ECBA87A0F27CD7D09EEA36BCB697652E076F4E (void);
// 0x00000423 System.Boolean UnityEngine.Color::Equals(UnityEngine.Color)
extern void Color_Equals_mA81EEDDC4250DE67C2F43BC88A102EA32A138052 (void);
// 0x00000424 UnityEngine.Color UnityEngine.Color::op_Addition(UnityEngine.Color,UnityEngine.Color)
extern void Color_op_Addition_m26E4EEFAF21C0C0DC2FD93CE594E02F61DB1F7DF (void);
// 0x00000425 UnityEngine.Color UnityEngine.Color::op_Subtraction(UnityEngine.Color,UnityEngine.Color)
extern void Color_op_Subtraction_mCEE40D3CDA3D99C65D055D69585BB68B0C1C2C49 (void);
// 0x00000426 UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,UnityEngine.Color)
extern void Color_op_Multiply_mA6BEAF09F1E0468B0557CEDFF1D228248F7D02B5 (void);
// 0x00000427 UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern void Color_op_Multiply_mF36917AD6235221537542FD079817CAB06CB1934 (void);
// 0x00000428 System.Boolean UnityEngine.Color::op_Equality(UnityEngine.Color,UnityEngine.Color)
extern void Color_op_Equality_m71B1A2F64AD6228F10E20149EF6440460D2C748E (void);
// 0x00000429 System.Boolean UnityEngine.Color::op_Inequality(UnityEngine.Color,UnityEngine.Color)
extern void Color_op_Inequality_m9C3EFC058BB205C298A2D3166173342303E660B9 (void);
// 0x0000042A UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void Color_Lerp_mD37EF718F1BAC65A7416655F0BC902CE76559C46 (void);
// 0x0000042B UnityEngine.Color UnityEngine.Color::LerpUnclamped(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void Color_LerpUnclamped_m4DFBD5A647F0ADC12938BF31BBF149EA039AC572 (void);
// 0x0000042C UnityEngine.Color UnityEngine.Color::RGBMultiplied(System.Single)
extern void Color_RGBMultiplied_m41914B23903491843FAA3B0C02027EF8B70F34CF (void);
// 0x0000042D UnityEngine.Color UnityEngine.Color::get_red()
extern void Color_get_red_m5562DD438931CF0D1FBBBB29BF7F8B752AF38957 (void);
// 0x0000042E UnityEngine.Color UnityEngine.Color::get_green()
extern void Color_get_green_mD53D8F980E92A0755759FBB2981E3DDEFCD084C0 (void);
// 0x0000042F UnityEngine.Color UnityEngine.Color::get_blue()
extern void Color_get_blue_m5449DCBB31EEB2324489989754C00123982EBABA (void);
// 0x00000430 UnityEngine.Color UnityEngine.Color::get_white()
extern void Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905 (void);
// 0x00000431 UnityEngine.Color UnityEngine.Color::get_black()
extern void Color_get_black_mEB3C91F45F8AA7E4842238DFCC578BB322723DAF (void);
// 0x00000432 UnityEngine.Color UnityEngine.Color::get_yellow()
extern void Color_get_yellow_mC8BD62CCC364EA5FC4273D4C2E116D0E2DE135AE (void);
// 0x00000433 UnityEngine.Color UnityEngine.Color::get_clear()
extern void Color_get_clear_m419239BDAEB3D3C4B4291BF2C6EF09A7D7D81360 (void);
// 0x00000434 UnityEngine.Color UnityEngine.Color::get_linear()
extern void Color_get_linear_mB10CD29D56ADE2C811AD74A605BA11F6656E9D1A (void);
// 0x00000435 System.Single UnityEngine.Color::get_maxColorComponent()
extern void Color_get_maxColorComponent_mBA8595CB2790747F42145CB696C10E64C9BBD76D (void);
// 0x00000436 UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern void Color_op_Implicit_m653C1CE2391B0A04114B9132C37E41AC92B33AFE (void);
// 0x00000437 UnityEngine.Color UnityEngine.Color::op_Implicit(UnityEngine.Vector4)
extern void Color_op_Implicit_m51CEC50D37ABC484073AECE7EB958B414F2B6E7B (void);
// 0x00000438 System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern void Color32__ctor_m1AEF46FBBBE4B522E6984D081A3D158198E10AA2 (void);
// 0x00000439 UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern void Color32_op_Implicit_m52B034473369A651C8952BD916A2AB193E0E5B30 (void);
// 0x0000043A UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern void Color32_op_Implicit_mA89CAD76E78975F51DF7374A67D18A5F6EF8DA61 (void);
// 0x0000043B System.String UnityEngine.Color32::ToString()
extern void Color32_ToString_m217F2AD5C02E630E37BE5CFB0933630F6D0C3555 (void);
// 0x0000043C System.Boolean UnityEngine.ColorUtility::DoTryParseHtmlColor(System.String,UnityEngine.Color32&)
extern void ColorUtility_DoTryParseHtmlColor_mE61F5CDC712B606366C3A668E16574124626FA79 (void);
// 0x0000043D System.Boolean UnityEngine.ColorUtility::TryParseHtmlString(System.String,UnityEngine.Color&)
extern void ColorUtility_TryParseHtmlString_mAA5964F95A5F122823B656E19B8FDDAA38F3A3E9 (void);
// 0x0000043E System.IntPtr UnityEngine.Gradient::Init()
extern void Gradient_Init_m56D09BE88E111535F8D2278E03BE81C9D70EFAAD (void);
// 0x0000043F System.Void UnityEngine.Gradient::Cleanup()
extern void Gradient_Cleanup_mD38D8100E8FAAC7FBD5047380555802E638DF718 (void);
// 0x00000440 System.Boolean UnityEngine.Gradient::Internal_Equals(System.IntPtr)
extern void Gradient_Internal_Equals_m210D28E9843DBA28E2F60FDBB366FE2B5B739B1A (void);
// 0x00000441 System.Void UnityEngine.Gradient::.ctor()
extern void Gradient__ctor_m297B6B928FDA6BD99A142736017F5C0E2B30BE7F (void);
// 0x00000442 System.Void UnityEngine.Gradient::Finalize()
extern void Gradient_Finalize_mB8CB38D86D7935F98000B5F4A0EBF419BD859210 (void);
// 0x00000443 UnityEngine.GradientColorKey[] UnityEngine.Gradient::get_colorKeys()
extern void Gradient_get_colorKeys_mBAE47BF507C2FDCEC907DFED48855C506C7F444D (void);
// 0x00000444 System.Void UnityEngine.Gradient::set_colorKeys(UnityEngine.GradientColorKey[])
extern void Gradient_set_colorKeys_m50145F3052E08C6249A2B36A07FD4A38CBA17B2C (void);
// 0x00000445 UnityEngine.GradientAlphaKey[] UnityEngine.Gradient::get_alphaKeys()
extern void Gradient_get_alphaKeys_m0B8E542144BBAE420A2CB8D274B33FDDBA035387 (void);
// 0x00000446 System.Void UnityEngine.Gradient::set_alphaKeys(UnityEngine.GradientAlphaKey[])
extern void Gradient_set_alphaKeys_mD8E4BB5FBBA0C4EF7C1553F5CE47297AABF4F1D6 (void);
// 0x00000447 UnityEngine.GradientMode UnityEngine.Gradient::get_mode()
extern void Gradient_get_mode_m9554C64F9058E174B189E4342F89AC9D22F53F49 (void);
// 0x00000448 System.Void UnityEngine.Gradient::set_mode(UnityEngine.GradientMode)
extern void Gradient_set_mode_m6B53343017D7E700EF7D5A7E18193DD53ACEAEE6 (void);
// 0x00000449 System.Boolean UnityEngine.Gradient::Equals(System.Object)
extern void Gradient_Equals_m0A13AD7938F81F21CC380609A506A39B37CF2097 (void);
// 0x0000044A System.Boolean UnityEngine.Gradient::Equals(UnityEngine.Gradient)
extern void Gradient_Equals_m7F23C7692189DDD94FC31758493D4C99C2F3FB1E (void);
// 0x0000044B System.Int32 UnityEngine.Gradient::GetHashCode()
extern void Gradient_GetHashCode_m2596E6672ACDAD829961AB1FBD3EB649A908DE64 (void);
// 0x0000044C UnityEngine.Vector3 UnityEngine.Matrix4x4::GetLossyScale()
extern void Matrix4x4_GetLossyScale_m0FB088456F10D189CDBAD737670B053EE644BB5C (void);
// 0x0000044D UnityEngine.Vector3 UnityEngine.Matrix4x4::get_lossyScale()
extern void Matrix4x4_get_lossyScale_m6E184C6942DA569CE4427C967501B3B4029A655E (void);
// 0x0000044E UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void Matrix4x4_TRS_m5BB2EBA1152301BAC92FDC7F33ECA732BAE57990 (void);
// 0x0000044F System.Void UnityEngine.Matrix4x4::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4)
extern void Matrix4x4__ctor_mC7C5A4F0791B2A3ADAFE1E6C491B7705B6492B12 (void);
// 0x00000450 System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern void Matrix4x4_GetHashCode_m6627C82FBE2092AE4711ABA909D0E2C3C182028F (void);
// 0x00000451 System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern void Matrix4x4_Equals_m7FB9C1A249956C6CDE761838B92097C525596D31 (void);
// 0x00000452 System.Boolean UnityEngine.Matrix4x4::Equals(UnityEngine.Matrix4x4)
extern void Matrix4x4_Equals_mF8358F488D95A9C2E5A9F69F31EC7EA0F4640E51 (void);
// 0x00000453 UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern void Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E (void);
// 0x00000454 UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
extern void Matrix4x4_MultiplyPoint_mD5D082585C5B3564A5EFC90A3C5CAFFE47E45B65 (void);
// 0x00000455 UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern void Matrix4x4_MultiplyPoint3x4_m7C872FDCC9E3378E00A40977F641A45A24994E9A (void);
// 0x00000456 System.String UnityEngine.Matrix4x4::ToString()
extern void Matrix4x4_ToString_m7E29D2447E2FC1EAB3D8565B7DCAFB9037C69E1D (void);
// 0x00000457 System.Void UnityEngine.Matrix4x4::.cctor()
extern void Matrix4x4__cctor_mC5A7950045F0C8DBAD83A45D08812BEDBC6E159E (void);
// 0x00000458 System.Void UnityEngine.Matrix4x4::GetLossyScale_Injected(UnityEngine.Matrix4x4&,UnityEngine.Vector3&)
extern void Matrix4x4_GetLossyScale_Injected_m3FC1B3C317274522F89E7BBD28D60C0B9AA2A8D1 (void);
// 0x00000459 System.Void UnityEngine.Matrix4x4::TRS_Injected(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern void Matrix4x4_TRS_Injected_mADF67489B3C6715B6BA35E22B0BA6713784100CC (void);
// 0x0000045A UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1 (void);
// 0x0000045B UnityEngine.Vector3 UnityEngine.Vector3::LerpUnclamped(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Vector3_LerpUnclamped_m71C7510DEF6E1696FE11FE68C22B21968C04AC3E (void);
// 0x0000045C UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Vector3_MoveTowards_mA288BB5AA73DDA9CA76EDC11F339BAFDA1E4FF45 (void);
// 0x0000045D System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern void Vector3_get_Item_mC3B9D35C070A91D7CA5C5B47280BD0EA3E148AC6 (void);
// 0x0000045E System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
extern void Vector3_set_Item_m89FF112CEC0D9ED43F1C4FE01522C75394B30AE6 (void);
// 0x0000045F System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (void);
// 0x00000460 System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern void Vector3__ctor_m6AD8F21FFCC7723C6F507CCF2E4E2EFFC4871584 (void);
// 0x00000461 System.Void UnityEngine.Vector3::Scale(UnityEngine.Vector3)
extern void Vector3_Scale_mD40CDE2B62BCBABD49426FAE104814F29CF2AA0B (void);
// 0x00000462 UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Cross_m3E9DBC445228FDB850BDBB4B01D6F61AC0111887 (void);
// 0x00000463 System.Int32 UnityEngine.Vector3::GetHashCode()
extern void Vector3_GetHashCode_m6C42B4F413A489535D180E8A99BE0298AD078B0B (void);
// 0x00000464 System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern void Vector3_Equals_m1F74B1FB7EE51589FFFA61D894F616B8F258C056 (void);
// 0x00000465 System.Boolean UnityEngine.Vector3::Equals(UnityEngine.Vector3)
extern void Vector3_Equals_m6B991540378DB8541CEB9472F7ED2BF5FF72B5DB (void);
// 0x00000466 UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern void Vector3_Normalize_mDEA51D0C131125535DA2B49B7281E0086ED583DC (void);
// 0x00000467 System.Void UnityEngine.Vector3::Normalize()
extern void Vector3_Normalize_m174460238EC6322B9095A378AA8624B1DD9000F3 (void);
// 0x00000468 UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern void Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B (void);
// 0x00000469 System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Dot_m0C530E1C51278DE28B77906D56356506232272C1 (void);
// 0x0000046A System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Angle_m8911FFA1DD1C8C46D923B52645B352FA1521CD5F (void);
// 0x0000046B System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7 (void);
// 0x0000046C UnityEngine.Vector3 UnityEngine.Vector3::ClampMagnitude(UnityEngine.Vector3,System.Single)
extern void Vector3_ClampMagnitude_mC9ADEE02D65C578B45F79DF10C53B81AB29D09D5 (void);
// 0x0000046D System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern void Vector3_Magnitude_m3958BE20951093E6B035C5F90493027063B39437 (void);
// 0x0000046E System.Single UnityEngine.Vector3::get_magnitude()
extern void Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274 (void);
// 0x0000046F System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern void Vector3_get_sqrMagnitude_m1C6E190B4A933A183B308736DEC0DD64B0588968 (void);
// 0x00000470 UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Min_m0D0997E6CDFF77E5177C8D4E0A21C592C63F747E (void);
// 0x00000471 UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Max_m78495079CA1E29B0658699B856AFF22E23180F36 (void);
// 0x00000472 UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern void Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2 (void);
// 0x00000473 UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern void Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB (void);
// 0x00000474 UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern void Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D (void);
// 0x00000475 UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern void Vector3_get_back_mE7EF8625637E6F8B9E6B42A6AE140777C51E02F7 (void);
// 0x00000476 UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern void Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7 (void);
// 0x00000477 UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern void Vector3_get_down_m3F76A48E5B7C82B35EE047375538AFD91A305F55 (void);
// 0x00000478 UnityEngine.Vector3 UnityEngine.Vector3::get_left()
extern void Vector3_get_left_m74B52D8CFD8C62138067B2EB6846B6E9E51B7C20 (void);
// 0x00000479 UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern void Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5 (void);
// 0x0000047A UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (void);
// 0x0000047B UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (void);
// 0x0000047C UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern void Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2 (void);
// 0x0000047D UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern void Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E (void);
// 0x0000047E UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern void Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839 (void);
// 0x0000047F UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern void Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624 (void);
// 0x00000480 System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_op_Equality_mA9E2F96E98E71AE7ACCE74766D700D41F0404806 (void);
// 0x00000481 System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_op_Inequality_mFEEAA4C4BF743FB5B8A47FF4967A5E2C73273D6E (void);
// 0x00000482 System.String UnityEngine.Vector3::ToString()
extern void Vector3_ToString_m2682D27AB50CD1CE4677C38D0720A302D582348D (void);
// 0x00000483 System.Void UnityEngine.Vector3::.cctor()
extern void Vector3__cctor_m83F3F89A8A8AFDBB54273660ABCA2E5AE1EAFDBD (void);
// 0x00000484 UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern void Quaternion_Inverse_mC3A78571A826F05CE179637E675BD25F8B203E0C (void);
// 0x00000485 UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern void Quaternion_Slerp_m56DE173C3520C83DF3F1C6EDFA82FF88A2C9E756 (void);
// 0x00000486 UnityEngine.Quaternion UnityEngine.Quaternion::SlerpUnclamped(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern void Quaternion_SlerpUnclamped_m4450A435D9A44F060FBB12A9C328FD338B8F892B (void);
// 0x00000487 UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern void Quaternion_Internal_FromEulerRad_mC6AB58E2F3C37DFE2089A38D578E862B3430E755 (void);
// 0x00000488 UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern void Quaternion_Internal_ToEulerRad_m5F7B68953CC22DCE9EC246396B02F0ADC0B1C470 (void);
// 0x00000489 UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern void Quaternion_AngleAxis_m07DACF59F0403451DABB9BC991C53EE3301E88B0 (void);
// 0x0000048A UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672 (void);
// 0x0000048B System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Quaternion__ctor_m7502F0C38E04C6DE24C965D1CAF278DDD02B9D61 (void);
// 0x0000048C UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern void Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (void);
// 0x0000048D UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790 (void);
// 0x0000048E UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern void Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C (void);
// 0x0000048F System.Boolean UnityEngine.Quaternion::IsEqualUsingDot(System.Single)
extern void Quaternion_IsEqualUsingDot_mA5E0CF75CBB488E3EC55BE9397FC3C92439A0BEF (void);
// 0x00000490 System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void Quaternion_op_Equality_m0DBCE8FE48EEF2D7C79741E498BFFB984DF4956F (void);
// 0x00000491 System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void Quaternion_op_Inequality_mDA6D2E63A498C8A9AB9A11DD7EA3B96567390C70 (void);
// 0x00000492 System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void Quaternion_Dot_m0C931CC8127C5461E5B8A857BDE2CE09297E468B (void);
// 0x00000493 System.Void UnityEngine.Quaternion::SetLookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Quaternion_SetLookRotation_mDB3D5A8083E5AB5881FA9CC1EACFC196F61B8204 (void);
// 0x00000494 UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
extern void Quaternion_Internal_MakePositive_mC458BD7036703798B11C6C46675814B57E236597 (void);
// 0x00000495 UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern void Quaternion_get_eulerAngles_mF8ABA8EB77CD682017E92F0F457374E54BC943F9 (void);
// 0x00000496 UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern void Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05 (void);
// 0x00000497 UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern void Quaternion_Euler_m55C96FCD397CC69109261572710608D12A4CBD2B (void);
// 0x00000498 System.Int32 UnityEngine.Quaternion::GetHashCode()
extern void Quaternion_GetHashCode_m43BDCF3A72E31FA4063C1DEB770890FF47033458 (void);
// 0x00000499 System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern void Quaternion_Equals_m099618C36B86DC63B2E7C89673C8566B18E5996E (void);
// 0x0000049A System.Boolean UnityEngine.Quaternion::Equals(UnityEngine.Quaternion)
extern void Quaternion_Equals_m0A269A9B77E915469801463C8BBEF7A06EF94A09 (void);
// 0x0000049B System.String UnityEngine.Quaternion::ToString()
extern void Quaternion_ToString_m38DF4A1C05A91331D0A208F45CE74AF005AB463D (void);
// 0x0000049C System.Void UnityEngine.Quaternion::.cctor()
extern void Quaternion__cctor_m026361EBBB33BB651A59FC7BC6128195AEDCF935 (void);
// 0x0000049D System.Void UnityEngine.Quaternion::Inverse_Injected(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern void Quaternion_Inverse_Injected_m1CD79ADF97C60D5645C15C5F04219021EE4654DD (void);
// 0x0000049E System.Void UnityEngine.Quaternion::Slerp_Injected(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern void Quaternion_Slerp_Injected_m28511088F8514F6FF5C85A0B042EFB662F2585D3 (void);
// 0x0000049F System.Void UnityEngine.Quaternion::SlerpUnclamped_Injected(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern void Quaternion_SlerpUnclamped_Injected_m4F8FA3284BA2D46D4977498F68E9C0C5539F9193 (void);
// 0x000004A0 System.Void UnityEngine.Quaternion::Internal_FromEulerRad_Injected(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Quaternion_Internal_FromEulerRad_Injected_m2197C7F75B2DB8B99C1947CD7C92714FE8D0099D (void);
// 0x000004A1 System.Void UnityEngine.Quaternion::Internal_ToEulerRad_Injected(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void Quaternion_Internal_ToEulerRad_Injected_mE55FFD02837E4FFFFFF8689E63B4EAF4F3B7396D (void);
// 0x000004A2 System.Void UnityEngine.Quaternion::AngleAxis_Injected(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Quaternion_AngleAxis_Injected_mFEA2ACDAAEEDCA27647FCB47B636F0D041147DD3 (void);
// 0x000004A3 System.Void UnityEngine.Quaternion::LookRotation_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Quaternion_LookRotation_Injected_m59A46014572ACB8F5C8A377B773D12EACCB53D4A (void);
// 0x000004A4 System.Int32 UnityEngine.Mathf::NextPowerOfTwo(System.Int32)
extern void Mathf_NextPowerOfTwo_m071D88C91A1CBECD47F18CA20D219C77879865E7 (void);
// 0x000004A5 System.Single UnityEngine.Mathf::GammaToLinearSpace(System.Single)
extern void Mathf_GammaToLinearSpace_m537DFDE30A58265FDA50F4D245B9599BBA8A4772 (void);
// 0x000004A6 System.Single UnityEngine.Mathf::Sin(System.Single)
extern void Mathf_Sin_m5275643192EFB3BD27A722901C6A4228A0DB8BB6 (void);
// 0x000004A7 System.Single UnityEngine.Mathf::Cos(System.Single)
extern void Mathf_Cos_mC5ECAE74D1FE9AF6F6EFF50AD3CD6BA1941B267A (void);
// 0x000004A8 System.Single UnityEngine.Mathf::Tan(System.Single)
extern void Mathf_Tan_m436F7A034334BCB42BD46ABC94D1200C70E81A49 (void);
// 0x000004A9 System.Single UnityEngine.Mathf::Atan(System.Single)
extern void Mathf_Atan_m1FF47E958C869FCB8BADF804011E04736E23C6F9 (void);
// 0x000004AA System.Single UnityEngine.Mathf::Atan2(System.Single,System.Single)
extern void Mathf_Atan2_mC5BCC778869EF562CD3A5A9D96002F63DA36A823 (void);
// 0x000004AB System.Single UnityEngine.Mathf::Sqrt(System.Single)
extern void Mathf_Sqrt_mF1FBD3142F5A3BCC5C35DFB922A14765BC0A8E2B (void);
// 0x000004AC System.Single UnityEngine.Mathf::Abs(System.Single)
extern void Mathf_Abs_mD852D98E3D4846B45F57D0AD4A8C6E00EF272662 (void);
// 0x000004AD System.Int32 UnityEngine.Mathf::Abs(System.Int32)
extern void Mathf_Abs_mC7DD2FB3789B5409055836D0E7D8227AD2099FDC (void);
// 0x000004AE System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern void Mathf_Min_mCF9BE0E9CAC9F18D207692BB2DAC7F3E1D4E1CB7 (void);
// 0x000004AF System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern void Mathf_Min_m1A2CC204E361AE13C329B6535165179798D3313A (void);
// 0x000004B0 System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern void Mathf_Max_m670AE0EC1B09ED1A56FF9606B0F954670319CB65 (void);
// 0x000004B1 System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern void Mathf_Max_mBDE4C6F1883EE3215CD7AE62550B2AC90592BC3F (void);
// 0x000004B2 System.Single UnityEngine.Mathf::Pow(System.Single,System.Single)
extern void Mathf_Pow_mC1BFA8F6235567CBB31F3D9507A6275635A38B5F (void);
// 0x000004B3 System.Single UnityEngine.Mathf::Exp(System.Single)
extern void Mathf_Exp_m7C92A75E0245B9FAEE93683523C22910C0877693 (void);
// 0x000004B4 System.Single UnityEngine.Mathf::Log(System.Single,System.Single)
extern void Mathf_Log_mD0CFD1242805BD697B5156AA46FBB43E7636A19B (void);
// 0x000004B5 System.Single UnityEngine.Mathf::Log10(System.Single)
extern void Mathf_Log10_m4D3234A75A708BE7BBDC19CF5DEB355E52205AE5 (void);
// 0x000004B6 System.Single UnityEngine.Mathf::Ceil(System.Single)
extern void Mathf_Ceil_m4FC7645E3D0F8FEDC33FE507E3D913108DD94E94 (void);
// 0x000004B7 System.Single UnityEngine.Mathf::Floor(System.Single)
extern void Mathf_Floor_mD447D35DE1D81DE09C2EFE21A75F0444E2AEF9E1 (void);
// 0x000004B8 System.Single UnityEngine.Mathf::Round(System.Single)
extern void Mathf_Round_mC8FAD403F9E68B0339CF65C8F63BFA3107DB3FC9 (void);
// 0x000004B9 System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern void Mathf_CeilToInt_m0230CCC7CC9266F18125D9425C38A25D1CA4275B (void);
// 0x000004BA System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern void Mathf_FloorToInt_m0C42B64571CE92A738AD7BB82388CE12FBE7457C (void);
// 0x000004BB System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern void Mathf_RoundToInt_m0EAD8BD38FCB72FA1D8A04E96337C820EC83F041 (void);
// 0x000004BC System.Single UnityEngine.Mathf::Sign(System.Single)
extern void Mathf_Sign_m6FA1D12786BEE0419D4B9426E5E4955F286BC8D3 (void);
// 0x000004BD System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern void Mathf_Clamp_m033DD894F89E6DCCDAFC580091053059C86A4507 (void);
// 0x000004BE System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern void Mathf_Clamp_mE1EA15D719BF2F632741D42DF96F0BC797A20389 (void);
// 0x000004BF System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern void Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B (void);
// 0x000004C0 System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern void Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364 (void);
// 0x000004C1 System.Single UnityEngine.Mathf::LerpUnclamped(System.Single,System.Single,System.Single)
extern void Mathf_LerpUnclamped_mA2458D5B39272F2996B92978CFBF6FFA43748B19 (void);
// 0x000004C2 System.Single UnityEngine.Mathf::LerpAngle(System.Single,System.Single,System.Single)
extern void Mathf_LerpAngle_m8802FB8DCECA2D48FDD13C20F384B9430E66C99F (void);
// 0x000004C3 System.Single UnityEngine.Mathf::MoveTowards(System.Single,System.Single,System.Single)
extern void Mathf_MoveTowards_m3B267066E774818E369220B0ABD084B271B45A85 (void);
// 0x000004C4 System.Single UnityEngine.Mathf::SmoothStep(System.Single,System.Single,System.Single)
extern void Mathf_SmoothStep_m4A729F510ABD2E9F385A41E4C4DFA972D1D07DF9 (void);
// 0x000004C5 System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern void Mathf_Approximately_m91AF00403E0D2DEA1AAE68601AD218CFAD70DF7E (void);
// 0x000004C6 System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern void Mathf_SmoothDamp_m00F6830F4979901CACDE66A7CEECD8AA467342C8 (void);
// 0x000004C7 System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern void Mathf_Repeat_m8459F4AAFF92DB770CC892BF71EE9438D9D0F779 (void);
// 0x000004C8 System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern void Mathf_InverseLerp_m7054CDF25056E9B27D2467F91C95D628508F1F31 (void);
// 0x000004C9 System.Single UnityEngine.Mathf::DeltaAngle(System.Single,System.Single)
extern void Mathf_DeltaAngle_mF21640BC4B2DDC43DE7AA557EFC8D816CAD6E116 (void);
// 0x000004CA System.Void UnityEngine.Mathf::.cctor()
extern void Mathf__cctor_m4855BF06F66120E2029CFA4F3E82FBDB197A86EC (void);
// 0x000004CB System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern void Vector2_get_Item_m67344A67120E48C32D9419E24BA7AED29F063379 (void);
// 0x000004CC System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern void Vector2_set_Item_m2335DC41E2BB7E64C21CDF0EEDE64FFB56E7ABD1 (void);
// 0x000004CD System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (void);
// 0x000004CE UnityEngine.Vector2 UnityEngine.Vector2::Lerp(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void Vector2_Lerp_m85DD66409D128B4F175627F89FA9D8751B75589F (void);
// 0x000004CF UnityEngine.Vector2 UnityEngine.Vector2::LerpUnclamped(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void Vector2_LerpUnclamped_mA24A011324A636E49C5DB828B208419DA240E405 (void);
// 0x000004D0 UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_Scale_m7AA97B65C683CB3B0BCBC61270A7F1A6350355A2 (void);
// 0x000004D1 System.Void UnityEngine.Vector2::Normalize()
extern void Vector2_Normalize_m99A2CC6E4CB65C1B9231F898D5B7A12B6D72E722 (void);
// 0x000004D2 UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern void Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B (void);
// 0x000004D3 System.String UnityEngine.Vector2::ToString()
extern void Vector2_ToString_m83C7C331834382748956B053E252AE3BD21807C4 (void);
// 0x000004D4 System.Int32 UnityEngine.Vector2::GetHashCode()
extern void Vector2_GetHashCode_m028AB6B14EBC6D668CFA45BF6EDEF17E2C44EA54 (void);
// 0x000004D5 System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern void Vector2_Equals_m4A2A75BC3D09933321220BCEF21219B38AF643AE (void);
// 0x000004D6 System.Boolean UnityEngine.Vector2::Equals(UnityEngine.Vector2)
extern void Vector2_Equals_mD6BF1A738E3CAF57BB46E604B030C072728F4EEB (void);
// 0x000004D7 System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_Dot_m34F6A75BE3FC6F728233811943AC4406C7D905BA (void);
// 0x000004D8 System.Single UnityEngine.Vector2::get_magnitude()
extern void Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF (void);
// 0x000004D9 System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern void Vector2_get_sqrMagnitude_mAEE10A8ECE7D5754E10727BA8C9068A759AD7002 (void);
// 0x000004DA System.Single UnityEngine.Vector2::Angle(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_Angle_mC4A140B49B9E737C9FC6B52763468C5662A8B4AC (void);
// 0x000004DB System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4 (void);
// 0x000004DC UnityEngine.Vector2 UnityEngine.Vector2::Min(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_Min_m4C1D2EB8CD6ECF4A6919A798314095739951445B (void);
// 0x000004DD UnityEngine.Vector2 UnityEngine.Vector2::Max(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_Max_m5E3C15E31483217408A281A5FDB5356BD6F976EE (void);
// 0x000004DE UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682 (void);
// 0x000004DF UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0 (void);
// 0x000004E0 UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_op_Multiply_mEDF9FDDF3BFFAEC997FBCDE5FA34871F2955E7C4 (void);
// 0x000004E1 UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_op_Division_mEF4FA1379564288637A7CF5E73BA30CA2259E591 (void);
// 0x000004E2 UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern void Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56 (void);
// 0x000004E3 UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(System.Single,UnityEngine.Vector2)
extern void Vector2_op_Multiply_m2E30A54E315810911DFC2E25C700757A68AC1F38 (void);
// 0x000004E4 UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern void Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077 (void);
// 0x000004E5 System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_op_Equality_m0E86E1B1038DDB8554A8A0D58729A7788D989588 (void);
// 0x000004E6 System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_op_Inequality_mC16161C640C89D98A00800924F83FF09FD7C100E (void);
// 0x000004E7 UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern void Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28 (void);
// 0x000004E8 UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern void Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F (void);
// 0x000004E9 UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern void Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8 (void);
// 0x000004EA UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern void Vector2_get_one_m6E01BE09CEA40781CB12CCB6AF33BBDA0F60CEED (void);
// 0x000004EB UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern void Vector2_get_up_mC4548731D5E7C71164D18C390A1AC32501DAE441 (void);
// 0x000004EC UnityEngine.Vector2 UnityEngine.Vector2::get_right()
extern void Vector2_get_right_mB4BD67462D579461853F297C0DE85D81E07E911E (void);
// 0x000004ED System.Void UnityEngine.Vector2::.cctor()
extern void Vector2__cctor_m13D18E02B3AC28597F5049D2F54830C9E4BDBE84 (void);
// 0x000004EE System.Int32 UnityEngine.Vector2Int::get_x()
extern void Vector2Int_get_x_m300C7C05CD66D24EE62D91CDC0C557A6C0ABF25E (void);
// 0x000004EF System.Void UnityEngine.Vector2Int::set_x(System.Int32)
extern void Vector2Int_set_x_mC6F9F1E43668A7F8E8C44CEFB427BA97389F1420 (void);
// 0x000004F0 System.Int32 UnityEngine.Vector2Int::get_y()
extern void Vector2Int_get_y_m0D6E131AB5FA4AD532DEC377F981AADA189E680B (void);
// 0x000004F1 System.Void UnityEngine.Vector2Int::set_y(System.Int32)
extern void Vector2Int_set_y_mF79CD7FACF0A9A1CC12678E2E24BD43C5E836D88 (void);
// 0x000004F2 System.Void UnityEngine.Vector2Int::.ctor(System.Int32,System.Int32)
extern void Vector2Int__ctor_m501C34762BA7ECDDCFC25C19A4B9C93BC15004E1 (void);
// 0x000004F3 UnityEngine.Vector2 UnityEngine.Vector2Int::op_Implicit(UnityEngine.Vector2Int)
extern void Vector2Int_op_Implicit_m05B84E680DA5408143C51CB664F97AC4702DC200 (void);
// 0x000004F4 System.Boolean UnityEngine.Vector2Int::op_Equality(UnityEngine.Vector2Int,UnityEngine.Vector2Int)
extern void Vector2Int_op_Equality_m7EDB885618BE7B785798F96DDDDA70E53461BB4B (void);
// 0x000004F5 System.Boolean UnityEngine.Vector2Int::op_Inequality(UnityEngine.Vector2Int,UnityEngine.Vector2Int)
extern void Vector2Int_op_Inequality_mAC8212BEC016167196A2EF04A381D3439070ECAE (void);
// 0x000004F6 System.Boolean UnityEngine.Vector2Int::Equals(System.Object)
extern void Vector2Int_Equals_mF7D7EFBC0286224832BA76701801C28530D40479 (void);
// 0x000004F7 System.Boolean UnityEngine.Vector2Int::Equals(UnityEngine.Vector2Int)
extern void Vector2Int_Equals_m65420C995F326F5C340E4825EA5E16BDE68F5A9C (void);
// 0x000004F8 System.Int32 UnityEngine.Vector2Int::GetHashCode()
extern void Vector2Int_GetHashCode_m73E874F4E94DF3D2603035E2E892873B139A7A9E (void);
// 0x000004F9 System.String UnityEngine.Vector2Int::ToString()
extern void Vector2Int_ToString_mB0D67C1885311767BA89D4C4A6E3A5A515194BF3 (void);
// 0x000004FA System.Void UnityEngine.Vector2Int::.cctor()
extern void Vector2Int__cctor_mCCA5D2394D22AB700421D56C1EB6998F4E3C810A (void);
// 0x000004FB System.Int32 UnityEngine.Vector3Int::get_x()
extern void Vector3Int_get_x_m23CB00F1579FD4CE86291940E2E75FB13405D53A (void);
// 0x000004FC System.Void UnityEngine.Vector3Int::set_x(System.Int32)
extern void Vector3Int_set_x_mED89A481E7FF21D0BF9F61511D5442E2CE9BEB75 (void);
// 0x000004FD System.Int32 UnityEngine.Vector3Int::get_y()
extern void Vector3Int_get_y_m1C2F0AB641A167DF22F9C3C57092EC05AEF8CA26 (void);
// 0x000004FE System.Void UnityEngine.Vector3Int::set_y(System.Int32)
extern void Vector3Int_set_y_m3E8A59AC8851E9AEC6B65AA167047C1F7C497B84 (void);
// 0x000004FF System.Int32 UnityEngine.Vector3Int::get_z()
extern void Vector3Int_get_z_m9A88DC2346FD1838EC611CC8AB2FC29951E94183 (void);
// 0x00000500 System.Void UnityEngine.Vector3Int::set_z(System.Int32)
extern void Vector3Int_set_z_m7BFC415EBBF5D31ACF664EEC37EE801C22B0E8D5 (void);
// 0x00000501 System.Void UnityEngine.Vector3Int::.ctor(System.Int32,System.Int32,System.Int32)
extern void Vector3Int__ctor_m171D642C38B163B353DAE9CCE90ACFE0894C1156 (void);
// 0x00000502 System.Boolean UnityEngine.Vector3Int::op_Equality(UnityEngine.Vector3Int,UnityEngine.Vector3Int)
extern void Vector3Int_op_Equality_mC2E3A3395AC3E18397283F3CBEA7167B2E463DFC (void);
// 0x00000503 System.Boolean UnityEngine.Vector3Int::Equals(System.Object)
extern void Vector3Int_Equals_m704D204F83B9C64C7AF06152F98B542C5C400DC7 (void);
// 0x00000504 System.Boolean UnityEngine.Vector3Int::Equals(UnityEngine.Vector3Int)
extern void Vector3Int_Equals_m9F98F28666ADF5AD0575C4CABAF6881F1317D4C1 (void);
// 0x00000505 System.Int32 UnityEngine.Vector3Int::GetHashCode()
extern void Vector3Int_GetHashCode_m6CDE2FEC995180949111253817BD0E4ECE7EAE3D (void);
// 0x00000506 System.String UnityEngine.Vector3Int::ToString()
extern void Vector3Int_ToString_m08AB1BE6A674B2669839B1C44ACCF6D85EBCFB91 (void);
// 0x00000507 System.Void UnityEngine.Vector3Int::.cctor()
extern void Vector3Int__cctor_m0EE114B6FDC7C783EF7B206D4E25F5CE900003C9 (void);
// 0x00000508 System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern void Vector4_get_Item_m39878FDA732B20347BB37CD1485560E9267EDC98 (void);
// 0x00000509 System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
extern void Vector4_set_Item_m56FB3A149299FEF1C0CF638CFAF71C7F0685EE45 (void);
// 0x0000050A System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Vector4__ctor_m545458525879607A5392A10B175D0C19B2BC715D (void);
// 0x0000050B UnityEngine.Vector4 UnityEngine.Vector4::LerpUnclamped(UnityEngine.Vector4,UnityEngine.Vector4,System.Single)
extern void Vector4_LerpUnclamped_mBFE6BE611E1C4AFF32BB59B052A1A640261CD7D9 (void);
// 0x0000050C System.Int32 UnityEngine.Vector4::GetHashCode()
extern void Vector4_GetHashCode_m7329FEA2E90CDBDBF4F09F51D92C87E08F5DC92E (void);
// 0x0000050D System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern void Vector4_Equals_m552ECA9ECD220D6526D8ECC9902016B6FC6D49B5 (void);
// 0x0000050E System.Boolean UnityEngine.Vector4::Equals(UnityEngine.Vector4)
extern void Vector4_Equals_mB9894C2D4EE56C6E8FDF6CC40DCE0CE16BA4F7BF (void);
// 0x0000050F System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Vector4_Dot_m9FAE8FE89CF99841AD8D2113DFCDB8764F9FBB18 (void);
// 0x00000510 System.Single UnityEngine.Vector4::get_magnitude()
extern void Vector4_get_magnitude_mE33CE76438DDE4DDBBAD94178B07D9364674D356 (void);
// 0x00000511 System.Single UnityEngine.Vector4::get_sqrMagnitude()
extern void Vector4_get_sqrMagnitude_m6B2707CBD31D237605D066A5925E6419D28B5397 (void);
// 0x00000512 UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern void Vector4_get_zero_m42821248DDFA4F9A3E0B2E84CBCB737BE9DCE3E9 (void);
// 0x00000513 UnityEngine.Vector4 UnityEngine.Vector4::op_Addition(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Vector4_op_Addition_m2079975CEB29719BDECFA861B53E499C70AA7015 (void);
// 0x00000514 UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Vector4_op_Subtraction_m2D5AED6DD0324E479548A9346AE29DAB489A8250 (void);
// 0x00000515 UnityEngine.Vector4 UnityEngine.Vector4::op_Multiply(UnityEngine.Vector4,System.Single)
extern void Vector4_op_Multiply_m16A8F11F144C03A8C817AC4FE542689E746043F4 (void);
// 0x00000516 UnityEngine.Vector4 UnityEngine.Vector4::op_Division(UnityEngine.Vector4,System.Single)
extern void Vector4_op_Division_m1D1BD7FFEF0CDBB7CE063CA139C22210A0B76689 (void);
// 0x00000517 System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Vector4_op_Equality_m9AE0D09EC7E02201F94AE469ADE9F416D0E20441 (void);
// 0x00000518 System.Boolean UnityEngine.Vector4::op_Inequality(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Vector4_op_Inequality_m7038D1E27BCA2E4C0EA8E034C30E586635FBF228 (void);
// 0x00000519 UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
extern void Vector4_op_Implicit_mEAB05A77FF8B3EE79C31499F0CF0A0D621A6496C (void);
// 0x0000051A System.String UnityEngine.Vector4::ToString()
extern void Vector4_ToString_m769402E3F7CBD6C92464D916527CC87BBBA53EF9 (void);
// 0x0000051B System.Void UnityEngine.Vector4::.cctor()
extern void Vector4__cctor_m478FA6A83B8E23F8323F150FF90B1FB934B1C251 (void);
// 0x0000051C System.Void UnityEngine.IPlayerEditorConnectionNative::Initialize()
// 0x0000051D System.Void UnityEngine.IPlayerEditorConnectionNative::DisconnectAll()
// 0x0000051E System.Void UnityEngine.IPlayerEditorConnectionNative::SendMessage(System.Guid,System.Byte[],System.Int32)
// 0x0000051F System.Boolean UnityEngine.IPlayerEditorConnectionNative::TrySendMessage(System.Guid,System.Byte[],System.Int32)
// 0x00000520 System.Void UnityEngine.IPlayerEditorConnectionNative::Poll()
// 0x00000521 System.Void UnityEngine.IPlayerEditorConnectionNative::RegisterInternal(System.Guid)
// 0x00000522 System.Void UnityEngine.IPlayerEditorConnectionNative::UnregisterInternal(System.Guid)
// 0x00000523 System.Boolean UnityEngine.IPlayerEditorConnectionNative::IsConnected()
// 0x00000524 System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.SendMessage(System.Guid,System.Byte[],System.Int32)
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_SendMessage_m00395F263B4C7FD7F10C32B1F4C4C1F00503D4BB (void);
// 0x00000525 System.Boolean UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.TrySendMessage(System.Guid,System.Byte[],System.Int32)
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_TrySendMessage_m950332D66588946F8699A64E5B8DBA8956A45303 (void);
// 0x00000526 System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.Poll()
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_Poll_mEAAE7671B5D8E0360BFE50E61E89FFF65DB825E4 (void);
// 0x00000527 System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.RegisterInternal(System.Guid)
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_RegisterInternal_m991A5281F58D94FA0F095A538BD91CA72B864965 (void);
// 0x00000528 System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.UnregisterInternal(System.Guid)
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_UnregisterInternal_mAF6931079473185968AFCD40A23A610F7D6CC3A0 (void);
// 0x00000529 System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.Initialize()
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_Initialize_mBC677B0244B87A53875C8C3A3A716DC09A8D541F (void);
// 0x0000052A System.Boolean UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.IsConnected()
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_IsConnected_m08B0A552BE45CC80F911E22D990B8FE6C82B53DD (void);
// 0x0000052B System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.DisconnectAll()
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_DisconnectAll_m5849A206AC5D274115B352114BD5F4B72900F651 (void);
// 0x0000052C System.Boolean UnityEngine.PlayerConnectionInternal::IsConnected()
extern void PlayerConnectionInternal_IsConnected_m4AD0EABFF2FCE8DE9DE1A6B520C707F300721FB2 (void);
// 0x0000052D System.Void UnityEngine.PlayerConnectionInternal::Initialize()
extern void PlayerConnectionInternal_Initialize_mB0E05590ED32D5DCD074FD6CB60064F8A96BB4FD (void);
// 0x0000052E System.Void UnityEngine.PlayerConnectionInternal::RegisterInternal(System.String)
extern void PlayerConnectionInternal_RegisterInternal_mC3FB67053C4C7DB1AABAB7E78E1F1345720ED84F (void);
// 0x0000052F System.Void UnityEngine.PlayerConnectionInternal::UnregisterInternal(System.String)
extern void PlayerConnectionInternal_UnregisterInternal_m675B2C87E01FCBBF5CB1A205375A2E95A8F150B2 (void);
// 0x00000530 System.Void UnityEngine.PlayerConnectionInternal::SendMessage(System.String,System.Byte[],System.Int32)
extern void PlayerConnectionInternal_SendMessage_m83801DCA5BBCC8116F1C7898FB6A755CCAF6F928 (void);
// 0x00000531 System.Boolean UnityEngine.PlayerConnectionInternal::TrySendMessage(System.String,System.Byte[],System.Int32)
extern void PlayerConnectionInternal_TrySendMessage_mE31F212ED59D69FD01FC2B6B4503A5AF97C1948D (void);
// 0x00000532 System.Void UnityEngine.PlayerConnectionInternal::PollInternal()
extern void PlayerConnectionInternal_PollInternal_m46079D478471FAB04EE8E613CAE8F6E79822472E (void);
// 0x00000533 System.Void UnityEngine.PlayerConnectionInternal::DisconnectAll()
extern void PlayerConnectionInternal_DisconnectAll_m58AFB71131F174149D6AA98C312D9026C15C6090 (void);
// 0x00000534 System.Void UnityEngine.PlayerConnectionInternal::.ctor()
extern void PlayerConnectionInternal__ctor_m882227F7C855BCF5CE1B0D44752124106BE31389 (void);
// 0x00000535 System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern void PlayerPrefsException__ctor_m1780F97210AE9B8FB0EE3DFAD6BB73A01E4A1CAB (void);
// 0x00000536 System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
extern void PlayerPrefs_TrySetSetString_m67AE78AFF61EABF374102466D40944DBA17E37AD (void);
// 0x00000537 System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern void PlayerPrefs_SetString_m7AC4E332A5DCA04E0AD91544AF836744BA8C2583 (void);
// 0x00000538 System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern void PlayerPrefs_GetString_m83CA5F0D9E058C853254ACF9130C72108BE56C9B (void);
// 0x00000539 System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern void PlayerPrefs_GetString_m3031AD2D5DEAB97677A9EF629618541437F079F1 (void);
// 0x0000053A System.Void UnityEngine.PlayerPrefs::Save()
extern void PlayerPrefs_Save_m6CC1FE22D4B10AC819F55802D725BE17EA2AD37B (void);
// 0x0000053B System.Void UnityEngine.PropertyAttribute::.ctor()
extern void PropertyAttribute__ctor_m7F5C473F39D5601486C1127DA0D52F2DC293FC35 (void);
// 0x0000053C System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern void TooltipAttribute__ctor_m13242D6EEE484B1CCA7B3A8FA720C5BCC44AF5DF (void);
// 0x0000053D System.Void UnityEngine.SpaceAttribute::.ctor()
extern void SpaceAttribute__ctor_m645A0DE9B507F2AFD8C67853D788F5F419D547C7 (void);
// 0x0000053E System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern void SpaceAttribute__ctor_mA70DC7F5FDC5474223B45E0F71A9003BBED1EFD0 (void);
// 0x0000053F System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern void RangeAttribute__ctor_mB9CC43849A074C59DD99F154215A755131D47D89 (void);
// 0x00000540 System.Void UnityEngine.MultilineAttribute::.ctor()
extern void MultilineAttribute__ctor_mA72F92D940A051EA25E1F2E052B455FEED13F0A5 (void);
// 0x00000541 System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern void TextAreaAttribute__ctor_m6134ACE5D232B16A9433AA1F47081FAA9BAC1BA1 (void);
// 0x00000542 System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern void Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384 (void);
// 0x00000543 System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern void Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780 (void);
// 0x00000544 System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern void Random_RandomRangeInt_m4D223ACEC78BCC435D90A7D3696775DE973D324E (void);
// 0x00000545 UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern void ResourceRequest_get_asset_m22E63CEED8581B913F8424D2100A47A335032A75 (void);
// 0x00000546 System.Void UnityEngine.ResourceRequest::.ctor()
extern void ResourceRequest__ctor_m9ABE408F147B831EA22F202333FA0A84B8AA575C (void);
// 0x00000547 T[] UnityEngine.Resources::ConvertObjects(UnityEngine.Object[])
// 0x00000548 UnityEngine.Object[] UnityEngine.Resources::FindObjectsOfTypeAll(System.Type)
extern void Resources_FindObjectsOfTypeAll_mFFDC0207FEBD620424377896FC1B67307DB42520 (void);
// 0x00000549 UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern void Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1 (void);
// 0x0000054A T UnityEngine.Resources::Load(System.String)
// 0x0000054B UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern void Resources_Load_mF0FA033BF566CDDA6A0E69BB97283B44C40726E7 (void);
// 0x0000054C UnityEngine.ResourceRequest UnityEngine.Resources::LoadAsync(System.String,System.Type)
extern void Resources_LoadAsync_m7A78B42B839F6ED3A198CE55359006823666D13F (void);
// 0x0000054D UnityEngine.ResourceRequest UnityEngine.Resources::LoadAsyncInternal(System.String,System.Type)
extern void Resources_LoadAsyncInternal_m90563E8C10B5F7F7CEF0AE32C9477CFF897115B9 (void);
// 0x0000054E UnityEngine.Object[] UnityEngine.Resources::LoadAll(System.String,System.Type)
extern void Resources_LoadAll_mCBF6F93023EF2F09957B7193D9966EF3AB374C9E (void);
// 0x0000054F T[] UnityEngine.Resources::LoadAll(System.String)
// 0x00000550 UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern void Resources_GetBuiltinResource_m73DDAC485E1E06C925628AA7285AC63D0797BD0A (void);
// 0x00000551 T UnityEngine.Resources::GetBuiltinResource(System.String)
// 0x00000552 System.Void UnityEngine.Resources::UnloadAsset(UnityEngine.Object)
extern void Resources_UnloadAsset_m066BEEBB51255EF3C0120EA4CF2300CA448FAF64 (void);
// 0x00000553 UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
extern void Resources_UnloadUnusedAssets_m04EC5EA06A8F7F26ED95BD704E1DDF62BBAB7043 (void);
// 0x00000554 System.Void UnityEngine.AsyncOperation::InternalDestroy(System.IntPtr)
extern void AsyncOperation_InternalDestroy_m22D4FE202FC59024E9ED7F6537C87D26F96FC551 (void);
// 0x00000555 System.Boolean UnityEngine.AsyncOperation::get_isDone()
extern void AsyncOperation_get_isDone_m2A08EE3D38FD9FE81F2D619FA66255B6F61DAB54 (void);
// 0x00000556 System.Single UnityEngine.AsyncOperation::get_progress()
extern void AsyncOperation_get_progress_m27F7D885AF1A989B993199181DE70D7B0F1E3984 (void);
// 0x00000557 System.Void UnityEngine.AsyncOperation::set_priority(System.Int32)
extern void AsyncOperation_set_priority_mEDC649F39ABECE1A801F379F9D2C7BA2E34F5B5E (void);
// 0x00000558 System.Boolean UnityEngine.AsyncOperation::get_allowSceneActivation()
extern void AsyncOperation_get_allowSceneActivation_mC5EC961F8BD14C79AFF6AEE2F634FB15F60AFA1F (void);
// 0x00000559 System.Void UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)
extern void AsyncOperation_set_allowSceneActivation_m297E3269310864DE1110ED51C7E2E302B32185C9 (void);
// 0x0000055A System.Void UnityEngine.AsyncOperation::Finalize()
extern void AsyncOperation_Finalize_m36607FEC5F5766510DD0B14440CD9775CF1C23C2 (void);
// 0x0000055B System.Void UnityEngine.AsyncOperation::InvokeCompletionEvent()
extern void AsyncOperation_InvokeCompletionEvent_m5F86FF01A5143016630C9CFADF6AA01DBBBD73A5 (void);
// 0x0000055C System.Void UnityEngine.AsyncOperation::add_completed(System.Action`1<UnityEngine.AsyncOperation>)
extern void AsyncOperation_add_completed_mD9CC08F727DA8EBB849CBF8903333EF6110C7ED6 (void);
// 0x0000055D System.Void UnityEngine.AsyncOperation::remove_completed(System.Action`1<UnityEngine.AsyncOperation>)
extern void AsyncOperation_remove_completed_mE0EA5D4CE4308A12C896F2A8CF193CD0BF5B19DE (void);
// 0x0000055E System.Void UnityEngine.AsyncOperation::.ctor()
extern void AsyncOperation__ctor_mEEE6114B72B8807F4AA6FF48FA79E4EFE480293F (void);
// 0x0000055F System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern void AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m716999F8F469E9398A275432AA5C68E81DD8DB24 (void);
// 0x00000560 System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern void AttributeHelperEngine_GetRequiredComponents_m869E1FF24FE124874E0723E11C12A906E57E3007 (void);
// 0x00000561 System.Int32 UnityEngine.AttributeHelperEngine::GetExecuteMode(System.Type)
extern void AttributeHelperEngine_GetExecuteMode_mDE99262C53FA67B470B8668A13F968B4D5A8E0A1 (void);
// 0x00000562 System.Int32 UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern void AttributeHelperEngine_CheckIsEditorScript_m95CEEF4147D16BC2985EAADD300905AB736F857E (void);
// 0x00000563 System.Int32 UnityEngine.AttributeHelperEngine::GetDefaultExecutionOrderFor(System.Type)
extern void AttributeHelperEngine_GetDefaultExecutionOrderFor_m0972E47FA03C9CEF196B1E7B2E708E30DF4AD063 (void);
// 0x00000564 T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType(System.Type)
// 0x00000565 System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern void AttributeHelperEngine__cctor_mAE0863DCF7EF9C1806BDC1D4DF64573464674964 (void);
// 0x00000566 System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern void DisallowMultipleComponent__ctor_m108E5D8C0DB938F0A747C6D2BA481B4FA9CDECB3 (void);
// 0x00000567 System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern void RequireComponent__ctor_m27819B55F8BD1517378CEFECA00FB183A13D9397 (void);
// 0x00000568 System.Void UnityEngine.RequireComponent::.ctor(System.Type,System.Type)
extern void RequireComponent__ctor_m2833BC8FBE2C72EE6F0CA08C4617BF84CB7D5BE6 (void);
// 0x00000569 System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern void AddComponentMenu__ctor_m33A9DE8FEE9BC9C12F67CF58BFEBECA372C236A3 (void);
// 0x0000056A System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern void AddComponentMenu__ctor_m78D1D62B91D88424AE2175501B17E4609EF645EA (void);
// 0x0000056B System.Void UnityEngine.ContextMenu::.ctor(System.String)
extern void ContextMenu__ctor_m69C18B7CA3C4C3C9687B6BF42697BC375A63CBBC (void);
// 0x0000056C System.Void UnityEngine.ContextMenu::.ctor(System.String,System.Boolean)
extern void ContextMenu__ctor_mC0876137653AF1E3441B1B3E87622140BB697923 (void);
// 0x0000056D System.Void UnityEngine.ContextMenu::.ctor(System.String,System.Boolean,System.Int32)
extern void ContextMenu__ctor_mC35AC9699C3AC41056CCA7A70D4853E8358FAB1F (void);
// 0x0000056E System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern void ExecuteInEditMode__ctor_m9A67409D4A11562F23F928655D9A3EFB7A69BB81 (void);
// 0x0000056F System.Void UnityEngine.ExecuteAlways::.ctor()
extern void ExecuteAlways__ctor_m6199E1FB2E787ABEE85C19153D3C90B815572092 (void);
// 0x00000570 System.Void UnityEngine.HideInInspector::.ctor()
extern void HideInInspector__ctor_mED96F804290F203756C1EDDE57F47C0E5A8FF4B4 (void);
// 0x00000571 System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
extern void HelpURLAttribute__ctor_mE8D73F64B451D3746E3427E74D332B7731D9D3AB (void);
// 0x00000572 System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern void DefaultExecutionOrder_get_order_mFD2CD99AEF550E218FAFC6CB3DDA3CE8D78614A9 (void);
// 0x00000573 System.Void UnityEngine.ExcludeFromPresetAttribute::.ctor()
extern void ExcludeFromPresetAttribute__ctor_mB8BE49E17E4360DDB485784D219AE49847A85FB2 (void);
// 0x00000574 System.Boolean UnityEngine.Behaviour::get_enabled()
extern void Behaviour_get_enabled_mAA0C9ED5A3D1589C1C8AA22636543528DB353CFB (void);
// 0x00000575 System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern void Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B (void);
// 0x00000576 System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern void Behaviour_get_isActiveAndEnabled_mC42DFCC1ECC2C94D52928FFE446CE7E266CA8B61 (void);
// 0x00000577 System.Void UnityEngine.Behaviour::.ctor()
extern void Behaviour__ctor_m409AEC21511ACF9A4CC0654DF4B8253E0D81D22C (void);
// 0x00000578 System.Void UnityEngine.ClassLibraryInitializer::Init()
extern void ClassLibraryInitializer_Init_mB8588C1A9DD9CB6B5CE77DB1F79AE301C46E0CE7 (void);
// 0x00000579 UnityEngine.Transform UnityEngine.Component::get_transform()
extern void Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (void);
// 0x0000057A UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern void Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (void);
// 0x0000057B UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern void Component_GetComponent_m5E75925F29811EEC97BD17CDC7D4BD8460F3090F (void);
// 0x0000057C System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern void Component_GetComponentFastPath_mDEB49C6B56084E436C7FC3D555339FA16949937E (void);
// 0x0000057D T UnityEngine.Component::GetComponent()
// 0x0000057E UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type,System.Boolean)
extern void Component_GetComponentInChildren_mEF7890FAC10EA2F776464285B0DCC58B8C373D34 (void);
// 0x0000057F T UnityEngine.Component::GetComponentInChildren()
// 0x00000580 T[] UnityEngine.Component::GetComponentsInChildren(System.Boolean)
// 0x00000581 System.Void UnityEngine.Component::GetComponentsInChildren(System.Boolean,System.Collections.Generic.List`1<T>)
// 0x00000582 T[] UnityEngine.Component::GetComponentsInChildren()
// 0x00000583 System.Void UnityEngine.Component::GetComponentsInChildren(System.Collections.Generic.List`1<T>)
// 0x00000584 UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern void Component_GetComponentInParent_mFD9A8F6311ABAF986CA0DA556662F89FD9234E7D (void);
// 0x00000585 T UnityEngine.Component::GetComponentInParent()
// 0x00000586 T[] UnityEngine.Component::GetComponentsInParent(System.Boolean)
// 0x00000587 System.Void UnityEngine.Component::GetComponentsInParent(System.Boolean,System.Collections.Generic.List`1<T>)
// 0x00000588 T[] UnityEngine.Component::GetComponentsInParent()
// 0x00000589 System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern void Component_GetComponentsForListInternal_m469B4C3A883942213BEA0EAAA54629219A042480 (void);
// 0x0000058A System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern void Component_GetComponents_m1ACBE6B9A75ECC898BA3B21D59AA7B3339D7735A (void);
// 0x0000058B System.Void UnityEngine.Component::GetComponents(System.Collections.Generic.List`1<T>)
// 0x0000058C T[] UnityEngine.Component::GetComponents()
// 0x0000058D System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern void Component_BroadcastMessage_mB8F26C0376146136DF5311F91A3A9667EAC3EDF8 (void);
// 0x0000058E System.Void UnityEngine.Component::BroadcastMessage(System.String,UnityEngine.SendMessageOptions)
extern void Component_BroadcastMessage_m33BDF64582F1D9EA2FF4A3B819649C8E62027CAC (void);
// 0x0000058F System.Void UnityEngine.Component::.ctor()
extern void Component__ctor_m5E2740C0ACA4B368BC460315FAA2EDBFEAC0B8EF (void);
// 0x00000590 System.Void UnityEngine.Coroutine::.ctor()
extern void Coroutine__ctor_mCA679040DA81B31D1E341400E98F6CF569269201 (void);
// 0x00000591 System.Void UnityEngine.Coroutine::Finalize()
extern void Coroutine_Finalize_mACCDC3AFBA7F1D247231AA875B5099200AF9ECC5 (void);
// 0x00000592 System.Void UnityEngine.Coroutine::ReleaseCoroutine(System.IntPtr)
extern void Coroutine_ReleaseCoroutine_mD33DD220788EEA099B98DD1258D6332A46D3D571 (void);
// 0x00000593 System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern void SetupCoroutine_InvokeMoveNext_m9106BA4E8AE0E794B17F184F1021A53F1D071F31 (void);
// 0x00000594 System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern void SetupCoroutine_InvokeMember_m0F2AD1D817B8E221C0DCAB9A81DA8359B20A8EFB (void);
// 0x00000595 System.Boolean UnityEngine.CustomYieldInstruction::get_keepWaiting()
// 0x00000596 System.Object UnityEngine.CustomYieldInstruction::get_Current()
extern void CustomYieldInstruction_get_Current_m9B2B482ED92A58E85B4D90A5AC7C89DFF87E33DC (void);
// 0x00000597 System.Boolean UnityEngine.CustomYieldInstruction::MoveNext()
extern void CustomYieldInstruction_MoveNext_m7EA6BAAEF6A01DC791D0B013D5AB5C377F9A6990 (void);
// 0x00000598 System.Void UnityEngine.CustomYieldInstruction::Reset()
extern void CustomYieldInstruction_Reset_m9B3349022DFDDA3A059F14D199F2408725727290 (void);
// 0x00000599 System.Void UnityEngine.CustomYieldInstruction::.ctor()
extern void CustomYieldInstruction__ctor_m06E2B5BC73763FE2E734FAA600D567701EA21EC5 (void);
// 0x0000059A System.Void UnityEngine.ExcludeFromObjectFactoryAttribute::.ctor()
extern void ExcludeFromObjectFactoryAttribute__ctor_mE0437C73993AD36DCB7A002D70AC988340742CD0 (void);
// 0x0000059B System.Void UnityEngine.ExtensionOfNativeClassAttribute::.ctor()
extern void ExtensionOfNativeClassAttribute__ctor_m2D759F6D70D6FE632D8872A7D7C3E7ECFF83EE46 (void);
// 0x0000059C UnityEngine.GameObject UnityEngine.GameObject::CreatePrimitive(UnityEngine.PrimitiveType)
extern void GameObject_CreatePrimitive_mA4D35085D817369E4A513FF31D745CEB27B07F6A (void);
// 0x0000059D T UnityEngine.GameObject::GetComponent()
// 0x0000059E UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern void GameObject_GetComponent_mECB756C7EB39F6BB79F8C065AB0013354763B151 (void);
// 0x0000059F System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
extern void GameObject_GetComponentFastPath_m5B276335DD94F6B307E604272A26C15B997C3CD4 (void);
// 0x000005A0 UnityEngine.Component UnityEngine.GameObject::GetComponentByName(System.String)
extern void GameObject_GetComponentByName_mAC730F7E7AACEFACBC17536AADBBB7254B25E3D3 (void);
// 0x000005A1 UnityEngine.Component UnityEngine.GameObject::GetComponent(System.String)
extern void GameObject_GetComponent_m92533B63C1C0C72597C246C2E7A8BBA662BDD1DF (void);
// 0x000005A2 UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
extern void GameObject_GetComponentInChildren_mBC5C12CDA1749A827D136DABBF10498B1096A086 (void);
// 0x000005A3 UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type)
extern void GameObject_GetComponentInChildren_mD4D1DDDF6052346718022CE72474233129BEA768 (void);
// 0x000005A4 T UnityEngine.GameObject::GetComponentInChildren()
// 0x000005A5 T UnityEngine.GameObject::GetComponentInChildren(System.Boolean)
// 0x000005A6 UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern void GameObject_GetComponentInParent_mA5BF9DFCA90C9003EB8F392CD64C45DFCB80F988 (void);
// 0x000005A7 T UnityEngine.GameObject::GetComponentInParent()
// 0x000005A8 System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern void GameObject_GetComponentsInternal_mAB759217A3AD0831ABD9387163126D391459E1B8 (void);
// 0x000005A9 UnityEngine.Component[] UnityEngine.GameObject::GetComponents(System.Type)
extern void GameObject_GetComponents_mD9AF87D297017455ADD8CF0DF05BB57B4382375F (void);
// 0x000005AA T[] UnityEngine.GameObject::GetComponents()
// 0x000005AB System.Void UnityEngine.GameObject::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern void GameObject_GetComponents_mA06AB0C2427DD87C91D14889C542F6E0F66FD915 (void);
// 0x000005AC System.Void UnityEngine.GameObject::GetComponents(System.Collections.Generic.List`1<T>)
// 0x000005AD UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInChildren(System.Type)
extern void GameObject_GetComponentsInChildren_m34C6A0C298EC385E3AAB80A7F3727B3AA0DE5AE1 (void);
// 0x000005AE UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInChildren(System.Type,System.Boolean)
extern void GameObject_GetComponentsInChildren_m39CCEFC6BC28CBD9587311B1E1C842B63DC8BDDB (void);
// 0x000005AF T[] UnityEngine.GameObject::GetComponentsInChildren(System.Boolean)
// 0x000005B0 System.Void UnityEngine.GameObject::GetComponentsInChildren(System.Boolean,System.Collections.Generic.List`1<T>)
// 0x000005B1 T[] UnityEngine.GameObject::GetComponentsInChildren()
// 0x000005B2 System.Void UnityEngine.GameObject::GetComponentsInChildren(System.Collections.Generic.List`1<T>)
// 0x000005B3 UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInParent(System.Type)
extern void GameObject_GetComponentsInParent_mC0D14D4EB6DB8A8D1CB51EA47DA0C25FFEEB2AC3 (void);
// 0x000005B4 UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInParent(System.Type,System.Boolean)
extern void GameObject_GetComponentsInParent_mFDE58BE6E14D1F34DE76EF62623BD0A46A99D24B (void);
// 0x000005B5 System.Void UnityEngine.GameObject::GetComponentsInParent(System.Boolean,System.Collections.Generic.List`1<T>)
// 0x000005B6 T[] UnityEngine.GameObject::GetComponentsInParent(System.Boolean)
// 0x000005B7 T[] UnityEngine.GameObject::GetComponentsInParent()
// 0x000005B8 System.Boolean UnityEngine.GameObject::TryGetComponent(T&)
// 0x000005B9 System.Boolean UnityEngine.GameObject::TryGetComponent(System.Type,UnityEngine.Component&)
extern void GameObject_TryGetComponent_m2AA8E1EF55AE188975F247B84E88EA8830126174 (void);
// 0x000005BA UnityEngine.Component UnityEngine.GameObject::TryGetComponentInternal(System.Type)
extern void GameObject_TryGetComponentInternal_m5003A254549BB4B574A424294C7D93394F7DDDA2 (void);
// 0x000005BB System.Void UnityEngine.GameObject::TryGetComponentFastPath(System.Type,System.IntPtr)
extern void GameObject_TryGetComponentFastPath_mBCE2196B5F774DB34EAE23EA89FDAFB427A04AC2 (void);
// 0x000005BC UnityEngine.GameObject UnityEngine.GameObject::FindWithTag(System.String)
extern void GameObject_FindWithTag_mF188C0A7A5DAC355AB020E2964B155F355237CB5 (void);
// 0x000005BD System.Void UnityEngine.GameObject::SendMessageUpwards(System.String,UnityEngine.SendMessageOptions)
extern void GameObject_SendMessageUpwards_m0765FA6E1EE34F980B5A7EC361EE279DA07E724D (void);
// 0x000005BE System.Void UnityEngine.GameObject::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern void GameObject_SendMessage_m68EE6232C4171FB0767181C1C1DD95C1103A6A75 (void);
// 0x000005BF System.Void UnityEngine.GameObject::BroadcastMessage(System.String,UnityEngine.SendMessageOptions)
extern void GameObject_BroadcastMessage_mABB308EA1B7F641BFC60E091A75E5E053D23A8B5 (void);
// 0x000005C0 UnityEngine.Component UnityEngine.GameObject::AddComponentInternal(System.String)
extern void GameObject_AddComponentInternal_mF527865CB0E00B2DCEC19BF13899241818F26A1A (void);
// 0x000005C1 UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern void GameObject_Internal_AddComponentWithType_m452B72618245B846503E5CA7DEA4662A77FB9896 (void);
// 0x000005C2 UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern void GameObject_AddComponent_m489C9D5426F2050795FA696CD478BB49AAE4BD70 (void);
// 0x000005C3 T UnityEngine.GameObject::AddComponent()
// 0x000005C4 UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern void GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (void);
// 0x000005C5 System.Int32 UnityEngine.GameObject::get_layer()
extern void GameObject_get_layer_m0DE90D8A3D3AA80497A3A80FBEAC2D207C16B9C8 (void);
// 0x000005C6 System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern void GameObject_set_layer_mDAC8037FCFD0CE62DB66004C4342EA20CF604907 (void);
// 0x000005C7 System.Boolean UnityEngine.GameObject::get_active()
extern void GameObject_get_active_m02FE30D766DA2B308BEDEABD70F4A5574D124C1A (void);
// 0x000005C8 System.Void UnityEngine.GameObject::set_active(System.Boolean)
extern void GameObject_set_active_m957ACA535CA30B47ADB282FFE2927A5AC8DDBFF6 (void);
// 0x000005C9 System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (void);
// 0x000005CA System.Boolean UnityEngine.GameObject::get_activeSelf()
extern void GameObject_get_activeSelf_mFE1834886CAE59884AC2BE707A3B821A1DB61F44 (void);
// 0x000005CB System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern void GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF (void);
// 0x000005CC System.Void UnityEngine.GameObject::SetActiveRecursively(System.Boolean)
extern void GameObject_SetActiveRecursively_m37A94311A3FF4C398BD85D0E853FE248DB9C1C22 (void);
// 0x000005CD System.Boolean UnityEngine.GameObject::get_isStatic()
extern void GameObject_get_isStatic_m45405FB1C5B284CF2EB3CB6EA5C57D6D1A384615 (void);
// 0x000005CE System.Void UnityEngine.GameObject::set_isStatic(System.Boolean)
extern void GameObject_set_isStatic_m945C32F2AB4D7AA35AD0E68820475EDEC2A56BBE (void);
// 0x000005CF System.Boolean UnityEngine.GameObject::get_isStaticBatchable()
extern void GameObject_get_isStaticBatchable_mF496707FE77F85744227A7AC72C42C3E3FCE2378 (void);
// 0x000005D0 System.String UnityEngine.GameObject::get_tag()
extern void GameObject_get_tag_mA9DC75D3D591B5E7D1ADAD14EA66CC4186580275 (void);
// 0x000005D1 System.Void UnityEngine.GameObject::set_tag(System.String)
extern void GameObject_set_tag_mEF09E323917FAA2CECA527F821111A92031C1138 (void);
// 0x000005D2 System.Boolean UnityEngine.GameObject::CompareTag(System.String)
extern void GameObject_CompareTag_mF66519C9DAE4CC8873C36A04C3CAF7DDEC3C7EFE (void);
// 0x000005D3 UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern void GameObject_FindGameObjectWithTag_m9F2877F52346B973DE3023209D73852E96ECC10D (void);
// 0x000005D4 UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern void GameObject_FindGameObjectsWithTag_mF49A195F19A598C5FD145FFE175ABE5B4885FAD9 (void);
// 0x000005D5 System.Void UnityEngine.GameObject::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)
extern void GameObject_SendMessageUpwards_mD32EFE21FC143F08243DB4F6333D5993D3935906 (void);
// 0x000005D6 System.Void UnityEngine.GameObject::SendMessageUpwards(System.String,System.Object)
extern void GameObject_SendMessageUpwards_m051F115223C75F7EAE50B95D4F1B04D313CB7215 (void);
// 0x000005D7 System.Void UnityEngine.GameObject::SendMessageUpwards(System.String)
extern void GameObject_SendMessageUpwards_m94C01293657094B2EFC19A803AAA218544E5E7AD (void);
// 0x000005D8 System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern void GameObject_SendMessage_mB9147E503F1F55C4F3BC2816C0BDA8C21EA22E95 (void);
// 0x000005D9 System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object)
extern void GameObject_SendMessage_mAF014F12A3B807BC435571585D4DD34FA89EC28E (void);
// 0x000005DA System.Void UnityEngine.GameObject::SendMessage(System.String)
extern void GameObject_SendMessage_m9A7BE015CC1472A59F1077CADFB22AD67F8722D9 (void);
// 0x000005DB System.Void UnityEngine.GameObject::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern void GameObject_BroadcastMessage_mDF70A40A7E27DF70B4338EBB840500A54060E33D (void);
// 0x000005DC System.Void UnityEngine.GameObject::BroadcastMessage(System.String,System.Object)
extern void GameObject_BroadcastMessage_m19C6E3E01BE544290887B016E2C9FA3EE50FA287 (void);
// 0x000005DD System.Void UnityEngine.GameObject::BroadcastMessage(System.String)
extern void GameObject_BroadcastMessage_m58B3BCA61DA82D13BC28DB128A2717728C053DB8 (void);
// 0x000005DE System.Void UnityEngine.GameObject::.ctor(System.String)
extern void GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686 (void);
// 0x000005DF System.Void UnityEngine.GameObject::.ctor()
extern void GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D (void);
// 0x000005E0 System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern void GameObject__ctor_m20BE06980A232E1D64016957059A9DD834173F68 (void);
// 0x000005E1 System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern void GameObject_Internal_CreateGameObject_m9DC9E92BD086A7ADD9ABCE858646A951FA77F437 (void);
// 0x000005E2 UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern void GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD (void);
// 0x000005E3 UnityEngine.SceneManagement.Scene UnityEngine.GameObject::get_scene()
extern void GameObject_get_scene_m46B0EF291DE58599187604475EC8640416BF9027 (void);
// 0x000005E4 System.UInt64 UnityEngine.GameObject::get_sceneCullingMask()
extern void GameObject_get_sceneCullingMask_m7A2DF40406FF26E0091E4836240A4DA63D24502D (void);
// 0x000005E5 UnityEngine.GameObject UnityEngine.GameObject::get_gameObject()
extern void GameObject_get_gameObject_mB8D6D847ABF95430B098554F3F2D63EC1D30C815 (void);
// 0x000005E6 System.Void UnityEngine.GameObject::get_scene_Injected(UnityEngine.SceneManagement.Scene&)
extern void GameObject_get_scene_Injected_m7B3888363DF23412EF1F431ADE1055C1DFF8EBE2 (void);
// 0x000005E7 System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern void LayerMask_op_Implicit_m2AFFC7F931005437E8F356C953F439829AF4CFA5 (void);
// 0x000005E8 UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern void LayerMask_op_Implicit_m3F256A7D96C66548F5B62C4621B9725301850300 (void);
// 0x000005E9 System.Int32 UnityEngine.LayerMask::get_value()
extern void LayerMask_get_value_m682288E860BBE36F5668DCDBC59245DE6319E537 (void);
// 0x000005EA System.Void UnityEngine.LayerMask::set_value(System.Int32)
extern void LayerMask_set_value_m56653944C7760E9D456E6B9E68F85CAA5B24A042 (void);
// 0x000005EB System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern void LayerMask_NameToLayer_m6491D9EA75F68B1F8AE15A9B4F193FFB9352B901 (void);
// 0x000005EC System.Void UnityEngine.ManagedStreamHelpers::ValidateLoadFromStream(System.IO.Stream)
extern void ManagedStreamHelpers_ValidateLoadFromStream_m550BE5DED66EC83AF331265B81084185B35FD846 (void);
// 0x000005ED System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamRead(System.Byte[],System.Int32,System.Int32,System.IO.Stream,System.IntPtr)
extern void ManagedStreamHelpers_ManagedStreamRead_mC0BDD6B226BBF621F6DAC184E3FC798D6BB0D47B (void);
// 0x000005EE System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamSeek(System.Int64,System.UInt32,System.IO.Stream,System.IntPtr)
extern void ManagedStreamHelpers_ManagedStreamSeek_m450DB930DD5085114F382A6FE05CF15C5CB21168 (void);
// 0x000005EF System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamLength(System.IO.Stream,System.IntPtr)
extern void ManagedStreamHelpers_ManagedStreamLength_mB518EF67FCBA5080CA4C45F34496C05041E07B98 (void);
// 0x000005F0 System.Boolean UnityEngine.MonoBehaviour::IsInvoking()
extern void MonoBehaviour_IsInvoking_mD0C27BE34FB97F408191450A702FA016E19997E5 (void);
// 0x000005F1 System.Void UnityEngine.MonoBehaviour::CancelInvoke()
extern void MonoBehaviour_CancelInvoke_m6ACF5FC83F8FE5A6E744CE1E83A94CB3B0A8B7EF (void);
// 0x000005F2 System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern void MonoBehaviour_Invoke_m979EDEF812D4630882E2E8346776B6CA5A9176BF (void);
// 0x000005F3 System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern void MonoBehaviour_InvokeRepeating_m99F21547D281B3F835745B681E5472F070E7E593 (void);
// 0x000005F4 System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
extern void MonoBehaviour_CancelInvoke_mDD95225EF4DFBB8C00B865468DE8AFEB5D30490F (void);
// 0x000005F5 System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
extern void MonoBehaviour_IsInvoking_mCA9E133D28B55AE0CE0E8EDBB183081DEEE57FBC (void);
// 0x000005F6 UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern void MonoBehaviour_StartCoroutine_m590A0A7F161D579C18E678B4C5ACCE77B1B318DD (void);
// 0x000005F7 UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern void MonoBehaviour_StartCoroutine_mCD250A96284E3C39D579CEC447432681DE8D1E44 (void);
// 0x000005F8 UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern void MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7 (void);
// 0x000005F9 UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern void MonoBehaviour_StartCoroutine_Auto_m5002506E1DE4625F7FEACC4D7F0ED8595E3B3AB5 (void);
// 0x000005FA System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern void MonoBehaviour_StopCoroutine_m3CDD6C046CC660D4CD6583FCE97F88A9735FD5FA (void);
// 0x000005FB System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern void MonoBehaviour_StopCoroutine_mC465FFA3C386BA22384F7AFA5495FF2286510562 (void);
// 0x000005FC System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern void MonoBehaviour_StopCoroutine_mC2C29B39556BFC68657F27343602BCC57AA6604F (void);
// 0x000005FD System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
extern void MonoBehaviour_StopAllCoroutines_mA5469BB7BBB59B8A94BB86590B051E0DFACC12DD (void);
// 0x000005FE System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
extern void MonoBehaviour_get_useGUILayout_m468C9F5A4D7F37643D26EEF41E5BA521CD81C267 (void);
// 0x000005FF System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern void MonoBehaviour_set_useGUILayout_m00327593C0DC39787FB9310328489F802FF63167 (void);
// 0x00000600 System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern void MonoBehaviour_print_m171D860AF3370C46648FE8F3EE3E0E6535E1C774 (void);
// 0x00000601 System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll(UnityEngine.MonoBehaviour)
extern void MonoBehaviour_Internal_CancelInvokeAll_m11071D9A8C6743C4FA754C1A079CFF5171638AB1 (void);
// 0x00000602 System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll(UnityEngine.MonoBehaviour)
extern void MonoBehaviour_Internal_IsInvokingAll_m44707A3C084E2CABC98FBCAF3531E547C6D59644 (void);
// 0x00000603 System.Void UnityEngine.MonoBehaviour::InvokeDelayed(UnityEngine.MonoBehaviour,System.String,System.Single,System.Single)
extern void MonoBehaviour_InvokeDelayed_mB70D589B53A55251F47641C6D3A51DA59F973D63 (void);
// 0x00000604 System.Void UnityEngine.MonoBehaviour::CancelInvoke(UnityEngine.MonoBehaviour,System.String)
extern void MonoBehaviour_CancelInvoke_m38D5CC0BF3FFDD89DD5F5A47E852B1E04BAAC5BB (void);
// 0x00000605 System.Boolean UnityEngine.MonoBehaviour::IsInvoking(UnityEngine.MonoBehaviour,System.String)
extern void MonoBehaviour_IsInvoking_mA209C7787032B92CEC40A5C571CB257D7A87457E (void);
// 0x00000606 System.Boolean UnityEngine.MonoBehaviour::IsObjectMonoBehaviour(UnityEngine.Object)
extern void MonoBehaviour_IsObjectMonoBehaviour_mCB948905029893B860CCD5D852878660A1CEF0D7 (void);
// 0x00000607 UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutineManaged(System.String,System.Object)
extern void MonoBehaviour_StartCoroutineManaged_m53873D1C040DB245BF13B74D2781072015B1EB66 (void);
// 0x00000608 UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutineManaged2(System.Collections.IEnumerator)
extern void MonoBehaviour_StartCoroutineManaged2_m114327BBBA9F208E8BA2F8BBF71FA0E8E996F7B8 (void);
// 0x00000609 System.Void UnityEngine.MonoBehaviour::StopCoroutineManaged(UnityEngine.Coroutine)
extern void MonoBehaviour_StopCoroutineManaged_mB9F1DBC3C5CCF4F64D5277B87518A54DEF7509C2 (void);
// 0x0000060A System.Void UnityEngine.MonoBehaviour::StopCoroutineFromEnumeratorManaged(System.Collections.IEnumerator)
extern void MonoBehaviour_StopCoroutineFromEnumeratorManaged_mA0D7F798094DF352E354304A50E8D97D9AB6900B (void);
// 0x0000060B System.String UnityEngine.MonoBehaviour::GetScriptClassName()
extern void MonoBehaviour_GetScriptClassName_mB38B7D00E1629DF772709F844EDB4C20074A208F (void);
// 0x0000060C System.Void UnityEngine.MonoBehaviour::.ctor()
extern void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (void);
// 0x0000060D System.Void UnityEngine.NoAllocHelpers::ResizeList(System.Collections.Generic.List`1<T>,System.Int32)
// 0x0000060E System.Void UnityEngine.NoAllocHelpers::EnsureListElemCount(System.Collections.Generic.List`1<T>,System.Int32)
// 0x0000060F System.Int32 UnityEngine.NoAllocHelpers::SafeLength(System.Array)
extern void NoAllocHelpers_SafeLength_m85E794F370BFE9D3954E72480AE6ED358AF5102C (void);
// 0x00000610 System.Int32 UnityEngine.NoAllocHelpers::SafeLength(System.Collections.Generic.List`1<T>)
// 0x00000611 T[] UnityEngine.NoAllocHelpers::ExtractArrayFromListT(System.Collections.Generic.List`1<T>)
// 0x00000612 System.Void UnityEngine.NoAllocHelpers::Internal_ResizeList(System.Object,System.Int32)
extern void NoAllocHelpers_Internal_ResizeList_mE270FB6EBB9A362E2C66B1A93C363C98746D28B6 (void);
// 0x00000613 System.Array UnityEngine.NoAllocHelpers::ExtractArrayFromList(System.Object)
extern void NoAllocHelpers_ExtractArrayFromList_mB4B8B76B4F160975C949FB3E15C1D497DBAE8EDC (void);
// 0x00000614 System.Int32 UnityEngine.RangeInt::get_end()
extern void RangeInt_get_end_m7A5182161CC5454E1C200E0173668572BA7FAAFD (void);
// 0x00000615 System.Void UnityEngine.RangeInt::.ctor(System.Int32,System.Int32)
extern void RangeInt__ctor_mACFE54DF73DE3F62053F851423525DB5AC1B100E (void);
// 0x00000616 System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor()
extern void RuntimeInitializeOnLoadMethodAttribute__ctor_mF7E0CAAF0DA4A1F5BAD4CF4C02C4C3A5AB2515D0 (void);
// 0x00000617 System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
extern void RuntimeInitializeOnLoadMethodAttribute__ctor_mB64D0D2B5788BC105D3640A22A70FCD5183CB413 (void);
// 0x00000618 System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::set_loadType(UnityEngine.RuntimeInitializeLoadType)
extern void RuntimeInitializeOnLoadMethodAttribute_set_loadType_m99C91FFBB561C344A90B86F6AF9ED8642CB87532 (void);
// 0x00000619 System.Void UnityEngine.ScriptableObject::.ctor()
extern void ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B (void);
// 0x0000061A UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern void ScriptableObject_CreateInstance_mDC77B7257A5E276CB272D3475B9B473B23A7128D (void);
// 0x0000061B T UnityEngine.ScriptableObject::CreateInstance()
// 0x0000061C System.Void UnityEngine.ScriptableObject::CreateScriptableObject(UnityEngine.ScriptableObject)
extern void ScriptableObject_CreateScriptableObject_m0DEEBEC415354F586C010E7863AEA64D2F628D0B (void);
// 0x0000061D UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateScriptableObjectInstanceFromType(System.Type,System.Boolean)
extern void ScriptableObject_CreateScriptableObjectInstanceFromType_m251F32A1C8B865FBED309AA24B8EAB3A35E2E25C (void);
// 0x0000061E System.Boolean UnityEngine.ScriptingUtility::IsManagedCodeWorking()
extern void ScriptingUtility_IsManagedCodeWorking_m4E53313183C7CB038C14545B933F81E249E707F3 (void);
// 0x0000061F System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern void SelectionBaseAttribute__ctor_mD7E83E67AFD9920E70551A353A33CC94D0584E8E (void);
// 0x00000620 System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern void StackTraceUtility_SetProjectFolder_m05FBBB2FF161F2F9F8551EB67D44B50F7CC98E21 (void);
// 0x00000621 System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern void StackTraceUtility_ExtractStackTrace_mEDFB4ECA329B87BC7DF2AA3EF7F9A31DAC052DC0 (void);
// 0x00000622 System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern void StackTraceUtility_ExtractStringFromExceptionInternal_m1FB3D6414E31C313AC633A24653DA4B1FB59C975 (void);
// 0x00000623 System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern void StackTraceUtility_ExtractFormattedStackTrace_m02A2ACEEF753617FAAA08B4EA840A49263901660 (void);
// 0x00000624 System.Void UnityEngine.StackTraceUtility::.cctor()
extern void StackTraceUtility__cctor_mDDEE2A2B6EBEDB75E0C28C81AFEDB1E9C372A165 (void);
// 0x00000625 System.Void UnityEngine.UnityException::.ctor()
extern void UnityException__ctor_m68C827240B217197615D8DA06FD3A443127D81DE (void);
// 0x00000626 System.Void UnityEngine.UnityException::.ctor(System.String)
extern void UnityException__ctor_mE42363D886E6DD7F075A6AEA689434C8E96722D9 (void);
// 0x00000627 System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void UnityException__ctor_m27B11548FE152B9AB9402E54CB6A50A2EE6FFE31 (void);
// 0x00000628 System.Void UnityEngine.UnassignedReferenceException::.ctor()
extern void UnassignedReferenceException__ctor_mA7A9187C9C01AD3E5D2C6568A907A5FE21D85AB8 (void);
// 0x00000629 System.Void UnityEngine.UnassignedReferenceException::.ctor(System.String)
extern void UnassignedReferenceException__ctor_m9E832DEA7FAAA414155FDA9F033E0128269C0C3B (void);
// 0x0000062A System.Void UnityEngine.UnassignedReferenceException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void UnassignedReferenceException__ctor_m3B13F71C76EBF3FBA6117BB3B0CD44D2AD6D26C5 (void);
// 0x0000062B System.String UnityEngine.TextAsset::get_text()
extern void TextAsset_get_text_mD3FBCD974CF552C7F7C7CD9A07BACAE51A2C5D42 (void);
// 0x0000062C System.String UnityEngine.TextAsset::ToString()
extern void TextAsset_ToString_m8C7ED5DD80E20B3A16A2100F62319811BE5DC830 (void);
// 0x0000062D System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern void UnhandledExceptionHandler_RegisterUECatcher_mE45C6A0301C35F6193F5774B7683683EF78D21DA (void);
// 0x0000062E System.Void UnityEngine.UnhandledExceptionHandler::HandleUnhandledException(System.Object,System.UnhandledExceptionEventArgs)
extern void UnhandledExceptionHandler_HandleUnhandledException_m09FC7ACFE0E555A5815A790856FBF5B0CA50819E (void);
// 0x0000062F System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern void UnhandledExceptionHandler_PrintException_m4CBE36F17C3F2B72205DB96B6D1377E4B3D11C77 (void);
// 0x00000630 System.Void UnityEngine.UnhandledExceptionHandler::iOSNativeUnhandledExceptionHandler(System.String,System.String,System.String)
extern void UnhandledExceptionHandler_iOSNativeUnhandledExceptionHandler_mD7444FEA5E5A468B81682D0C10831FD62ED60DC6 (void);
// 0x00000631 System.Int32 UnityEngine.Object::GetInstanceID()
extern void Object_GetInstanceID_m33A817CEE904B3362C8BAAF02DB45976575CBEF4 (void);
// 0x00000632 System.Int32 UnityEngine.Object::GetHashCode()
extern void Object_GetHashCode_mCF9141C6640C2989CD354118673711D5F3741984 (void);
// 0x00000633 System.Boolean UnityEngine.Object::Equals(System.Object)
extern void Object_Equals_m813F5A9FF65C9BC0D6907570C2A9913507D58F32 (void);
// 0x00000634 System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern void Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534 (void);
// 0x00000635 System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern void Object_CompareBaseObjects_mE918232D595FB366CE5FAD4411C5FBD86809CC04 (void);
// 0x00000636 System.Void UnityEngine.Object::EnsureRunningOnMainThread()
extern void Object_EnsureRunningOnMainThread_m51EC43D45C0101F31785B31F41118AA5D5EE5A7C (void);
// 0x00000637 System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern void Object_IsNativeObjectAlive_m683A8A1607CB2FF5E56EC09C5D150A8DA7D3FF08 (void);
// 0x00000638 System.IntPtr UnityEngine.Object::GetCachedPtr()
extern void Object_GetCachedPtr_m8CCFA6D419ADFBA8F9EF83CB45DFD75C2704C4A0 (void);
// 0x00000639 System.String UnityEngine.Object::get_name()
extern void Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE (void);
// 0x0000063A System.Void UnityEngine.Object::set_name(System.String)
extern void Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826 (void);
// 0x0000063B UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Object_Instantiate_mAF9C2662167396DEE640598515B60BE41B9D5082 (void);
// 0x0000063C UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Object_Instantiate_m1B26356F0AC5A811CA6E02F42A9E2F9C0AEA4BF0 (void);
// 0x0000063D UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern void Object_Instantiate_m17AA3123A55239124BC54A907AEEE509034F0830 (void);
// 0x0000063E UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform)
extern void Object_Instantiate_mFDB3861B1290CA5D04FFBA660789184870BC0560 (void);
// 0x0000063F UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern void Object_Instantiate_m674B3934708548332899CE953CA56BB696C1C887 (void);
// 0x00000640 T UnityEngine.Object::Instantiate(T)
// 0x00000641 T UnityEngine.Object::Instantiate(T,UnityEngine.Vector3,UnityEngine.Quaternion)
// 0x00000642 T UnityEngine.Object::Instantiate(T,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
// 0x00000643 T UnityEngine.Object::Instantiate(T,UnityEngine.Transform)
// 0x00000644 T UnityEngine.Object::Instantiate(T,UnityEngine.Transform,System.Boolean)
// 0x00000645 System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern void Object_Destroy_m09F51D8BDECFD2E8C618498EF7377029B669030D (void);
// 0x00000646 System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (void);
// 0x00000647 System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern void Object_DestroyImmediate_mFCE7947857C832BCBB366FCCE50072ACAD9A4C51 (void);
// 0x00000648 System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern void Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446 (void);
// 0x00000649 UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern void Object_FindObjectsOfType_m3FC26FB3B36525BFBFCCCD1AEEE8A86712A12203 (void);
// 0x0000064A System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern void Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207 (void);
// 0x0000064B UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
extern void Object_get_hideFlags_mCC5D0A1480AC0CDA190A63120B39C2C531428FC8 (void);
// 0x0000064C System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern void Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0 (void);
// 0x0000064D System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern void Object_DestroyObject_mE3A40F1CEB3A1DE929CC00A3A0B397C57FCE5492 (void);
// 0x0000064E System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object)
extern void Object_DestroyObject_m5ECBDE6659EB24A3E48B74578F760292E188BFA6 (void);
// 0x0000064F UnityEngine.Object[] UnityEngine.Object::FindSceneObjectsOfType(System.Type)
extern void Object_FindSceneObjectsOfType_m48C5C906DE6910BF4527D42EA218E7E9A62B9305 (void);
// 0x00000650 UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)
extern void Object_FindObjectsOfTypeIncludingAssets_mE951AE46100FF6715248470291BBF23DEDB750FA (void);
// 0x00000651 T[] UnityEngine.Object::FindObjectsOfType()
// 0x00000652 T UnityEngine.Object::FindObjectOfType()
// 0x00000653 UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeAll(System.Type)
extern void Object_FindObjectsOfTypeAll_m5DEFCAA98B41BD762BA77701A134596C0F854EB1 (void);
// 0x00000654 System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern void Object_CheckNullArgument_m8D42F516655D770DFEEAA13CF86A2612214AAA9B (void);
// 0x00000655 UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern void Object_FindObjectOfType_mCDF38E1667CF4502F60C59709D70B60EF7E408DA (void);
// 0x00000656 System.String UnityEngine.Object::ToString()
extern void Object_ToString_m4EBF621C98D5BFA9C0522C27953BB45AB2430FE1 (void);
// 0x00000657 System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern void Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (void);
// 0x00000658 System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern void Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (void);
// 0x00000659 System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern void Object_GetOffsetOfInstanceIDInCPlusPlusObject_m7C7130E8611F32F6CC9A47400AC5BDC2BA5E6D23 (void);
// 0x0000065A System.Boolean UnityEngine.Object::CurrentThreadIsMainThread()
extern void Object_CurrentThreadIsMainThread_mC0AD3A568DAE48B60FFE4BEFFB1DD265A12B7AA3 (void);
// 0x0000065B UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern void Object_Internal_CloneSingle_m4231A0B9138AC40B76655B772F687CC7E6160C06 (void);
// 0x0000065C UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern void Object_Internal_CloneSingleWithParent_m32F1D681010B51B98CDBC82E3510FC260D2D1E17 (void);
// 0x0000065D UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Object_Internal_InstantiateSingle_mCC3EB0F918934D233D43DFDB457605C4B248738D (void);
// 0x0000065E UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Object_Internal_InstantiateSingleWithParent_m4E81B30B4790121D12DE8E91049F353B81BCC87D (void);
// 0x0000065F System.String UnityEngine.Object::ToString(UnityEngine.Object)
extern void Object_ToString_m7A4BBACD14901DD0181038A25BED62520D273EDC (void);
// 0x00000660 System.String UnityEngine.Object::GetName(UnityEngine.Object)
extern void Object_GetName_m1691C0D50AEBC1C86229AEAC2FBC1EE2DC6B67AF (void);
// 0x00000661 System.Boolean UnityEngine.Object::IsPersistent(UnityEngine.Object)
extern void Object_IsPersistent_m213422DA5E58C3717D0A8EB1D6429F6EE24D3A4E (void);
// 0x00000662 System.Void UnityEngine.Object::SetName(UnityEngine.Object,System.String)
extern void Object_SetName_m2CBABC30BA2B93EFF6A39B3295A7AB85901E60E8 (void);
// 0x00000663 System.Boolean UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)
extern void Object_DoesObjectWithInstanceIDExist_m724FB30225896EC46C735C6BAF06D9370D38F828 (void);
// 0x00000664 UnityEngine.Object UnityEngine.Object::FindObjectFromInstanceID(System.Int32)
extern void Object_FindObjectFromInstanceID_m7594ED98F525AAE38FEC80052729ECAF3E821350 (void);
// 0x00000665 UnityEngine.Object UnityEngine.Object::ForceLoadFromInstanceID(System.Int32)
extern void Object_ForceLoadFromInstanceID_m94B5AF82BBC0041EAF05E71CB95757A469621ADA (void);
// 0x00000666 System.Void UnityEngine.Object::.ctor()
extern void Object__ctor_m091EBAEBC7919B0391ABDAFB7389ADC12206525B (void);
// 0x00000667 System.Void UnityEngine.Object::.cctor()
extern void Object__cctor_m14515D6A9B514D3A8590E2CAE4372A0956E8976C (void);
// 0x00000668 UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle_Injected(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Object_Internal_InstantiateSingle_Injected_m04E25C97D5848B7AA54178A5448744A0DEB1B62E (void);
// 0x00000669 UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent_Injected(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Object_Internal_InstantiateSingleWithParent_Injected_m1AB8ACB112874AADE585A35A270ACB12D35CFACE (void);
// 0x0000066A System.Void UnityEngine.UnitySynchronizationContext::.ctor(System.Int32)
extern void UnitySynchronizationContext__ctor_mCABD0C784640450930DF24FAD73E8AD6D1B52037 (void);
// 0x0000066B System.Void UnityEngine.UnitySynchronizationContext::.ctor(System.Collections.Generic.List`1<UnityEngine.UnitySynchronizationContext/WorkRequest>,System.Int32)
extern void UnitySynchronizationContext__ctor_m9D104656F4EAE96CB3A40DDA6EDCEBA752664612 (void);
// 0x0000066C System.Void UnityEngine.UnitySynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Send_m25CDC5B5ABF8D55B70EB314AA46923E3CF2AD4B9 (void);
// 0x0000066D System.Void UnityEngine.UnitySynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Post_mB4E900B6E9350E8E944011B6BF3D16C0657375FE (void);
// 0x0000066E System.Threading.SynchronizationContext UnityEngine.UnitySynchronizationContext::CreateCopy()
extern void UnitySynchronizationContext_CreateCopy_mC20AC170E7947120E65ED75D71889CDAC957A5CD (void);
// 0x0000066F System.Void UnityEngine.UnitySynchronizationContext::Exec()
extern void UnitySynchronizationContext_Exec_m07342201E337E047B73C8B3259710820EFF75A9C (void);
// 0x00000670 System.Boolean UnityEngine.UnitySynchronizationContext::HasPendingTasks()
extern void UnitySynchronizationContext_HasPendingTasks_mBFCAC1697C6B71584E72079A6EDB83D52EC1700A (void);
// 0x00000671 System.Void UnityEngine.UnitySynchronizationContext::InitializeSynchronizationContext()
extern void UnitySynchronizationContext_InitializeSynchronizationContext_m0F2A055040D6848FAD84A08DBC410E56B2D9E6A3 (void);
// 0x00000672 System.Void UnityEngine.UnitySynchronizationContext::ExecuteTasks()
extern void UnitySynchronizationContext_ExecuteTasks_m027AF329D90D6451B83A2EAF3528C9021800A962 (void);
// 0x00000673 System.Boolean UnityEngine.UnitySynchronizationContext::ExecutePendingTasks(System.Int64)
extern void UnitySynchronizationContext_ExecutePendingTasks_m74DCC56A938FEECD533CFE58CAC4B8B9ED001122 (void);
// 0x00000674 System.Void UnityEngine.UnitySynchronizationContext/WorkRequest::.ctor(System.Threading.SendOrPostCallback,System.Object,System.Threading.ManualResetEvent)
extern void WorkRequest__ctor_mE19AE1779B544378C8CB488F1576BDE618548599 (void);
// 0x00000675 System.Void UnityEngine.UnitySynchronizationContext/WorkRequest::Invoke()
extern void WorkRequest_Invoke_m67D71A48794EEBB6B9793E6F1E015DE90C03C1ED (void);
// 0x00000676 System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern void WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B (void);
// 0x00000677 System.Single UnityEngine.WaitForSecondsRealtime::get_waitTime()
extern void WaitForSecondsRealtime_get_waitTime_m6D1B0EDEAFA3DBBBFE1A0CC2D372BAB8EA82E2FB (void);
// 0x00000678 System.Void UnityEngine.WaitForSecondsRealtime::set_waitTime(System.Single)
extern void WaitForSecondsRealtime_set_waitTime_m867F4482BEE354E33A6FD9191344D74B9CC8C790 (void);
// 0x00000679 System.Boolean UnityEngine.WaitForSecondsRealtime::get_keepWaiting()
extern void WaitForSecondsRealtime_get_keepWaiting_mC257FFC53D5734250B919A8B6BE460A6D8A7CD99 (void);
// 0x0000067A System.Void UnityEngine.WaitForSecondsRealtime::.ctor(System.Single)
extern void WaitForSecondsRealtime__ctor_m775503EC1F4963D8E5BBDD7989B40F6A000E0525 (void);
// 0x0000067B System.Void UnityEngine.YieldInstruction::.ctor()
extern void YieldInstruction__ctor_mA72AD367FB081E0C2493649C6E8F7CFC592AB620 (void);
// 0x0000067C System.Void UnityEngine.SerializeField::.ctor()
extern void SerializeField__ctor_mEE7F6BB7A9643562D8CEF189848925B74F87DA27 (void);
// 0x0000067D System.Void UnityEngine.ISerializationCallbackReceiver::OnBeforeSerialize()
// 0x0000067E System.Void UnityEngine.ISerializationCallbackReceiver::OnAfterDeserialize()
// 0x0000067F System.Int32 UnityEngine.ComputeShader::FindKernel(System.String)
extern void ComputeShader_FindKernel_m4CEBD37F96732810C4C370A6249CF460BE1F93A3 (void);
// 0x00000680 System.Void UnityEngine.LowerResBlitTexture::LowerResBlitTextureDontStripMe()
extern void LowerResBlitTexture_LowerResBlitTextureDontStripMe_mC89EA382E4636DE8BC0E14D1BB024A80C65F5CAF (void);
// 0x00000681 System.Void UnityEngine.PreloadData::PreloadDataDontStripMe()
extern void PreloadData_PreloadDataDontStripMe_m68DF1A35E18F9FC43A63F2F990EA9970D4E4A78B (void);
// 0x00000682 System.String UnityEngine.SystemInfo::get_operatingSystem()
extern void SystemInfo_get_operatingSystem_m74AC18E4195899CD3E61258DAED8FBB588B50082 (void);
// 0x00000683 UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::get_operatingSystemFamily()
extern void SystemInfo_get_operatingSystemFamily_mA35FE1FF2DD6240B2880DC5F642D4A0CC2B58D8D (void);
// 0x00000684 System.String UnityEngine.SystemInfo::get_processorType()
extern void SystemInfo_get_processorType_m7B14D432F490CAC67239862D69F60512E5E2094B (void);
// 0x00000685 System.Int32 UnityEngine.SystemInfo::get_processorCount()
extern void SystemInfo_get_processorCount_m0B474484D797FC99BAEAEDC56D4974EDF4067504 (void);
// 0x00000686 System.Int32 UnityEngine.SystemInfo::get_systemMemorySize()
extern void SystemInfo_get_systemMemorySize_m7599C2A9CA16C15DD72C5563D5C47262CEDE0EB8 (void);
// 0x00000687 System.String UnityEngine.SystemInfo::get_deviceName()
extern void SystemInfo_get_deviceName_mCA23C1149EF385459B51E0176D70561574B28D38 (void);
// 0x00000688 System.String UnityEngine.SystemInfo::get_deviceModel()
extern void SystemInfo_get_deviceModel_m104FF34E03C33AE2AD84FE4CF8ED501C0AE9458A (void);
// 0x00000689 System.Boolean UnityEngine.SystemInfo::get_supportsAccelerometer()
extern void SystemInfo_get_supportsAccelerometer_m7C2C36431E5CF87C2B6BA1E3B1B135152F89C4AB (void);
// 0x0000068A System.Boolean UnityEngine.SystemInfo::get_supportsGyroscope()
extern void SystemInfo_get_supportsGyroscope_mAD26B709942E2F8A6C6CE708568DAE4114792850 (void);
// 0x0000068B System.Boolean UnityEngine.SystemInfo::get_supportsLocationService()
extern void SystemInfo_get_supportsLocationService_m5D232D056D916F19210DC6D6608BF6B7E9415E8F (void);
// 0x0000068C System.Boolean UnityEngine.SystemInfo::get_supportsVibration()
extern void SystemInfo_get_supportsVibration_mDA0CDC28A7813E9614FE5A254C21D903555029D3 (void);
// 0x0000068D UnityEngine.DeviceType UnityEngine.SystemInfo::get_deviceType()
extern void SystemInfo_get_deviceType_mAFCA02B695EE4E37FA3B87B8C05804B6616E1528 (void);
// 0x0000068E System.String UnityEngine.SystemInfo::get_graphicsDeviceName()
extern void SystemInfo_get_graphicsDeviceName_mD9D09CDF695610D3355F28EAD9EE61F7870F52E8 (void);
// 0x0000068F System.String UnityEngine.SystemInfo::get_graphicsDeviceVendor()
extern void SystemInfo_get_graphicsDeviceVendor_m6207F479F5D1D1BC9D770A085B8F3A926ED6141F (void);
// 0x00000690 System.String UnityEngine.SystemInfo::get_graphicsDeviceVersion()
extern void SystemInfo_get_graphicsDeviceVersion_m64E10307E6A8F0351DB4B2CDA96480AD172F119D (void);
// 0x00000691 System.Boolean UnityEngine.SystemInfo::get_supportsShadows()
extern void SystemInfo_get_supportsShadows_m05F10143737B2C6446A663D48447C2006B8701E3 (void);
// 0x00000692 System.Boolean UnityEngine.SystemInfo::get_supportsRenderTextures()
extern void SystemInfo_get_supportsRenderTextures_m52A011F59EDA92779E44D4F49785DC46E2E7F04A (void);
// 0x00000693 System.Boolean UnityEngine.SystemInfo::get_supportsRenderToCubemap()
extern void SystemInfo_get_supportsRenderToCubemap_mE001B8DA31AC4C3727FDB2B1710346F0F8C547A9 (void);
// 0x00000694 System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
extern void SystemInfo_get_supportsImageEffects_m5606438D404910FADC9C50DC29E0649E49B08267 (void);
// 0x00000695 System.Boolean UnityEngine.SystemInfo::get_supports3DTextures()
extern void SystemInfo_get_supports3DTextures_m6416FF7B8551584CE72D063619F2AAA3F3EBB1C7 (void);
// 0x00000696 System.Boolean UnityEngine.SystemInfo::get_supportsComputeShaders()
extern void SystemInfo_get_supportsComputeShaders_m257AEF98322259041D3358B801FD6F34DB13013F (void);
// 0x00000697 System.Boolean UnityEngine.SystemInfo::get_supportsSparseTextures()
extern void SystemInfo_get_supportsSparseTextures_m609B1472F58AA462DE7A80D0303C9FC8F3A189F0 (void);
// 0x00000698 System.Int32 UnityEngine.SystemInfo::get_supportedRenderTargetCount()
extern void SystemInfo_get_supportedRenderTargetCount_mD11B3D770E9DA7EE2F0AC0E9B48299AEDE1B515D (void);
// 0x00000699 System.Int32 UnityEngine.SystemInfo::get_supportsStencil()
extern void SystemInfo_get_supportsStencil_m55DB72FD189FF9C0117AB4E288C2F23BF5E92DC7 (void);
// 0x0000069A System.Boolean UnityEngine.SystemInfo::IsValidEnumValue(System.Enum)
extern void SystemInfo_IsValidEnumValue_m112F964C57B2311EA910CCA5CE0FFABFFF906740 (void);
// 0x0000069B System.Boolean UnityEngine.SystemInfo::SupportsTextureFormat(UnityEngine.TextureFormat)
extern void SystemInfo_SupportsTextureFormat_m1FCBD02367A45D11CAA6503715F3AAE24CA98B79 (void);
// 0x0000069C UnityEngine.NPOTSupport UnityEngine.SystemInfo::get_npotSupport()
extern void SystemInfo_get_npotSupport_m1979DB1E3EA8BDEAE36BEE349B7F2783FED1A90C (void);
// 0x0000069D System.Int32 UnityEngine.SystemInfo::get_maxTextureSize()
extern void SystemInfo_get_maxTextureSize_m10A8228B83EE161D0DCCB9FB01279C245C47D0E3 (void);
// 0x0000069E System.String UnityEngine.SystemInfo::GetOperatingSystem()
extern void SystemInfo_GetOperatingSystem_m01E20879EE653FEAA38F0B042172AB28CD57FA43 (void);
// 0x0000069F UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::GetOperatingSystemFamily()
extern void SystemInfo_GetOperatingSystemFamily_mD20DAFF3A6E6649299A3BCFC845E7EB41BFA1D93 (void);
// 0x000006A0 System.String UnityEngine.SystemInfo::GetProcessorType()
extern void SystemInfo_GetProcessorType_mCEB2AE2578AB2C465092C65C718D01C3BA49F209 (void);
// 0x000006A1 System.Int32 UnityEngine.SystemInfo::GetProcessorCount()
extern void SystemInfo_GetProcessorCount_mF10CAC62D6F1DB8EADD19C5297F4E9897F3AB716 (void);
// 0x000006A2 System.Int32 UnityEngine.SystemInfo::GetPhysicalMemoryMB()
extern void SystemInfo_GetPhysicalMemoryMB_m5AFA40651FD22CC6A5EEEA7FDEA0687D9EF46EF2 (void);
// 0x000006A3 System.String UnityEngine.SystemInfo::GetDeviceName()
extern void SystemInfo_GetDeviceName_mD1E184CD46DBBB9CD945269502634065C3EF516C (void);
// 0x000006A4 System.String UnityEngine.SystemInfo::GetDeviceModel()
extern void SystemInfo_GetDeviceModel_mB039F042DA446DDABACAEA353ABE2EAC1AF635CF (void);
// 0x000006A5 System.Boolean UnityEngine.SystemInfo::SupportsAccelerometer()
extern void SystemInfo_SupportsAccelerometer_m8BEBFA4B3003213D362EB2663C75EB93D7B31FC1 (void);
// 0x000006A6 System.Boolean UnityEngine.SystemInfo::IsGyroAvailable()
extern void SystemInfo_IsGyroAvailable_mD44D668844B7AFF24063BDBB0A2DCC288FBBD382 (void);
// 0x000006A7 System.Boolean UnityEngine.SystemInfo::SupportsLocationService()
extern void SystemInfo_SupportsLocationService_m5508550E1E56669E91A75575308B002EFC9C98B3 (void);
// 0x000006A8 System.Boolean UnityEngine.SystemInfo::SupportsVibration()
extern void SystemInfo_SupportsVibration_m29D4F3B0473CCFF561CC8CC27D3D0D0DC7A4BB32 (void);
// 0x000006A9 UnityEngine.DeviceType UnityEngine.SystemInfo::GetDeviceType()
extern void SystemInfo_GetDeviceType_m749955DFFD2554A64E851BD95C41F6C55D6A777E (void);
// 0x000006AA System.String UnityEngine.SystemInfo::GetGraphicsDeviceName()
extern void SystemInfo_GetGraphicsDeviceName_m621D518DE8F0D7BC5F02D49AF854FC270DC4322B (void);
// 0x000006AB System.String UnityEngine.SystemInfo::GetGraphicsDeviceVendor()
extern void SystemInfo_GetGraphicsDeviceVendor_mD88346AB78AC1CDA78E5887E9FB9290141A6A570 (void);
// 0x000006AC System.String UnityEngine.SystemInfo::GetGraphicsDeviceVersion()
extern void SystemInfo_GetGraphicsDeviceVersion_m7D59AFD88DC43E9EDA7B9A86BE090A13B1408FC0 (void);
// 0x000006AD System.Boolean UnityEngine.SystemInfo::SupportsShadows()
extern void SystemInfo_SupportsShadows_mE90CEC00EBA87DEFAB8E666B02568E07E2E5C771 (void);
// 0x000006AE System.Boolean UnityEngine.SystemInfo::Supports3DTextures()
extern void SystemInfo_Supports3DTextures_mB616CBCD08E13A9BAA541CC5445AD074F9E3D7DF (void);
// 0x000006AF System.Boolean UnityEngine.SystemInfo::SupportsComputeShaders()
extern void SystemInfo_SupportsComputeShaders_mEEA74BF6DA49185642F0E07824298D793904CB67 (void);
// 0x000006B0 System.Boolean UnityEngine.SystemInfo::SupportsSparseTextures()
extern void SystemInfo_SupportsSparseTextures_m8D688048ACAD87F00B36DA519FCCD99D2EA3D711 (void);
// 0x000006B1 System.Int32 UnityEngine.SystemInfo::SupportedRenderTargetCount()
extern void SystemInfo_SupportedRenderTargetCount_mE6CA6DE27146900463A1263DC0A5AB835C528F92 (void);
// 0x000006B2 System.Boolean UnityEngine.SystemInfo::SupportsTextureFormatNative(UnityEngine.TextureFormat)
extern void SystemInfo_SupportsTextureFormatNative_mD028594492646D7AB78A4C2F51CA06F63E665210 (void);
// 0x000006B3 UnityEngine.NPOTSupport UnityEngine.SystemInfo::GetNPOTSupport()
extern void SystemInfo_GetNPOTSupport_mA49CF1030D03A398B582FFECFD11F3C56E358DE3 (void);
// 0x000006B4 System.Int32 UnityEngine.SystemInfo::GetMaxTextureSize()
extern void SystemInfo_GetMaxTextureSize_mC9CBB174AB61833BB9BFD82AD5B23E9096C2D43C (void);
// 0x000006B5 System.Boolean UnityEngine.SystemInfo::IsFormatSupported(UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.FormatUsage)
extern void SystemInfo_IsFormatSupported_m6941B7C4566DEE1EFFD7F6DCB7BFA701ECF9C1D6 (void);
// 0x000006B6 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.SystemInfo::GetCompatibleFormat(UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.FormatUsage)
extern void SystemInfo_GetCompatibleFormat_mC67F0F547EA28CDE08EA9A6B030082516D189EF3 (void);
// 0x000006B7 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.SystemInfo::GetGraphicsFormat(UnityEngine.Experimental.Rendering.DefaultFormat)
extern void SystemInfo_GetGraphicsFormat_m708339B9A94CEBC02A56629FE41F6809DE267F6C (void);
// 0x000006B8 System.Single UnityEngine.Time::get_time()
extern void Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8 (void);
// 0x000006B9 System.Single UnityEngine.Time::get_timeSinceLevelLoad()
extern void Time_get_timeSinceLevelLoad_mDF4964DE4068B0FEC0F950129C7BEF881D0CF9E0 (void);
// 0x000006BA System.Single UnityEngine.Time::get_deltaTime()
extern void Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (void);
// 0x000006BB System.Single UnityEngine.Time::get_unscaledTime()
extern void Time_get_unscaledTime_m57F78B855097C5BA632CF9BE60667A9DEBCAA472 (void);
// 0x000006BC System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern void Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2 (void);
// 0x000006BD System.Single UnityEngine.Time::get_fixedUnscaledDeltaTime()
extern void Time_get_fixedUnscaledDeltaTime_mB0F666A0C4D5B0C73088464FFF1A0B5DDC0A5D9E (void);
// 0x000006BE System.Single UnityEngine.Time::get_fixedDeltaTime()
extern void Time_get_fixedDeltaTime_m76C241EDB6F824713AF57DCECD5765871770FA4C (void);
// 0x000006BF System.Single UnityEngine.Time::get_smoothDeltaTime()
extern void Time_get_smoothDeltaTime_m22F7BB00188785BB7AE979B00CAA96284363C985 (void);
// 0x000006C0 System.Single UnityEngine.Time::get_timeScale()
extern void Time_get_timeScale_m7E198A5814859A08FD0FFE6DD5F7ED5C907719F8 (void);
// 0x000006C1 System.Void UnityEngine.Time::set_timeScale(System.Single)
extern void Time_set_timeScale_mAB89C3BB5DEE81934159C23F103397A77AC3F4AF (void);
// 0x000006C2 System.Int32 UnityEngine.Time::get_frameCount()
extern void Time_get_frameCount_m97573E267B487B8FD4BF37615AFC19BED7B4E436 (void);
// 0x000006C3 System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern void Time_get_realtimeSinceStartup_mCA1086EC9DFCF135F77BC46D3B7127711EA3DE03 (void);
// 0x000006C4 System.Void UnityEngine.TouchScreenKeyboard::Internal_Destroy(System.IntPtr)
extern void TouchScreenKeyboard_Internal_Destroy_m6CD4E2343AB4FE54BC23DCFE62A50180CB3634E0 (void);
// 0x000006C5 System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern void TouchScreenKeyboard_Destroy_m916AE9DA740DBD435A5EDD93C6BC55CCEC8310C3 (void);
// 0x000006C6 System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern void TouchScreenKeyboard_Finalize_m684570CC561058F48B51F7E21A144299FBCE4C76 (void);
// 0x000006C7 System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.Int32)
extern void TouchScreenKeyboard__ctor_mDF71D45DC0F867825BEB1CDF728927FBB0E07F86 (void);
// 0x000006C8 System.IntPtr UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_mD608B3B2A2159D17A8DF7961FA4EB1694A416973 (void);
// 0x000006C9 System.Boolean UnityEngine.TouchScreenKeyboard::get_isSupported()
extern void TouchScreenKeyboard_get_isSupported_m9163BAF0764DCDD9CB87E75A296D820D629D31CA (void);
// 0x000006CA System.Boolean UnityEngine.TouchScreenKeyboard::get_isInPlaceEditingAllowed()
extern void TouchScreenKeyboard_get_isInPlaceEditingAllowed_m503AB6CB0DFBD8E7D396C8E552C643F20E4A5D47 (void);
// 0x000006CB UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.Int32)
extern void TouchScreenKeyboard_Open_mF795EEBFEF7DF5E5418CF2BACA02D1C62291B93A (void);
// 0x000006CC System.String UnityEngine.TouchScreenKeyboard::get_text()
extern void TouchScreenKeyboard_get_text_mC025B2F295D315E1A18E7AA54B013A8072A8FEB0 (void);
// 0x000006CD System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
extern void TouchScreenKeyboard_set_text_mF72A794EEC3FC19A9701B61A70BCF392D53B0D38 (void);
// 0x000006CE System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
extern void TouchScreenKeyboard_set_hideInput_mA9729B01B360BF98153F40B96DDED39534B94840 (void);
// 0x000006CF System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
extern void TouchScreenKeyboard_get_active_m35A2928E54273BF16CB19D11665989D894D5C79D (void);
// 0x000006D0 System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
extern void TouchScreenKeyboard_set_active_m8D5FDCFA997C5EAD7896F7EB456165498727792D (void);
// 0x000006D1 UnityEngine.TouchScreenKeyboard/Status UnityEngine.TouchScreenKeyboard::get_status()
extern void TouchScreenKeyboard_get_status_m17CF606283E9BAF02D76ADC813F63F036FAE33F2 (void);
// 0x000006D2 System.Void UnityEngine.TouchScreenKeyboard::set_characterLimit(System.Int32)
extern void TouchScreenKeyboard_set_characterLimit_m97F392EB376881A77F5210CFA6736F0A49F5D57B (void);
// 0x000006D3 System.Boolean UnityEngine.TouchScreenKeyboard::get_canGetSelection()
extern void TouchScreenKeyboard_get_canGetSelection_m8EEC32EA25638ACB54F698EA72450B51BEC9A362 (void);
// 0x000006D4 System.Boolean UnityEngine.TouchScreenKeyboard::get_canSetSelection()
extern void TouchScreenKeyboard_get_canSetSelection_m3C9574749CA28A6677C77B8FBE6292B80DF88A10 (void);
// 0x000006D5 UnityEngine.RangeInt UnityEngine.TouchScreenKeyboard::get_selection()
extern void TouchScreenKeyboard_get_selection_m9E2AB5FF388968D946B15A3ECDAE1C3C97115AAD (void);
// 0x000006D6 System.Void UnityEngine.TouchScreenKeyboard::set_selection(UnityEngine.RangeInt)
extern void TouchScreenKeyboard_set_selection_m27C5DE6B9DBBBFE2CCA68FA80A600D94337215C7 (void);
// 0x000006D7 System.Void UnityEngine.TouchScreenKeyboard::GetSelection(System.Int32&,System.Int32&)
extern void TouchScreenKeyboard_GetSelection_mED761F9161A43CC8E507EA27DB1BD11127C6C896 (void);
// 0x000006D8 System.Void UnityEngine.TouchScreenKeyboard::SetSelection(System.Int32,System.Int32)
extern void TouchScreenKeyboard_SetSelection_mB728B6A7B07AC33987FB30F7950B3D31E3BA5FBF (void);
// 0x000006D9 System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern void DrivenRectTransformTracker_Add_m51059F302FBD574E93820E8116283D1608D1AB5A (void);
// 0x000006DA System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern void DrivenRectTransformTracker_Clear_m328659F339A4FB519C9A208A685DDED106B6FC89 (void);
// 0x000006DB System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern void RectTransform_add_reapplyDrivenProperties_mDA1F055B02E43F9041D4198D446D89E00381851E (void);
// 0x000006DC System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern void RectTransform_remove_reapplyDrivenProperties_m65A8DB93E1A247A5C8CD880906FF03FEA89E7CEB (void);
// 0x000006DD UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern void RectTransform_get_rect_mE5F283FCB99A66403AC1F0607CA49C156D73A15E (void);
// 0x000006DE UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern void RectTransform_get_anchorMin_mB62D77CAC5A2A086320638AE7DF08135B7028744 (void);
// 0x000006DF System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern void RectTransform_set_anchorMin_mE965F5B0902C2554635010A5752728414A57020A (void);
// 0x000006E0 UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern void RectTransform_get_anchorMax_m1E51C211FBB32326C884375C9F1E8E8221E5C086 (void);
// 0x000006E1 System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern void RectTransform_set_anchorMax_m55EEF00D9E42FE542B5346D7CEDAF9248736F7D3 (void);
// 0x000006E2 UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern void RectTransform_get_anchoredPosition_mCB2171DBADBC572F354CCFE3ACA19F9506F97907 (void);
// 0x000006E3 System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern void RectTransform_set_anchoredPosition_m4DD45DB1A97734A1F3A81E5F259638ECAF35962F (void);
// 0x000006E4 UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern void RectTransform_get_sizeDelta_mDA0A3E73679143B1B52CE2F9A417F90CB9F3DAFF (void);
// 0x000006E5 System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern void RectTransform_set_sizeDelta_m7729BA56325BA667F0F7D60D642124F7909F1302 (void);
// 0x000006E6 UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern void RectTransform_get_pivot_mA5BEEE2069ACA7C0C717530EED3E7D811D46C463 (void);
// 0x000006E7 System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern void RectTransform_set_pivot_mB791A383B3C870B9CBD7BC51B2C95711C88E9DCF (void);
// 0x000006E8 UnityEngine.Vector3 UnityEngine.RectTransform::get_anchoredPosition3D()
extern void RectTransform_get_anchoredPosition3D_mD8D0568957927A07C38FA922F72264D75AE6CC11 (void);
// 0x000006E9 System.Void UnityEngine.RectTransform::set_anchoredPosition3D(UnityEngine.Vector3)
extern void RectTransform_set_anchoredPosition3D_m8C889FB130AF8F4B8213CB1A37ECE109E4821504 (void);
// 0x000006EA UnityEngine.Vector2 UnityEngine.RectTransform::get_offsetMin()
extern void RectTransform_get_offsetMin_mC1C5E8A4F5B830C74C46473E9C6A1B82370CDA4B (void);
// 0x000006EB System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
extern void RectTransform_set_offsetMin_m7455ED64FF16C597E648E022BA768CFDCF4531AC (void);
// 0x000006EC UnityEngine.Vector2 UnityEngine.RectTransform::get_offsetMax()
extern void RectTransform_get_offsetMax_m7E2F8892E1E3352C3FF80747352D29F367B3403E (void);
// 0x000006ED System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
extern void RectTransform_set_offsetMax_mD55D44AD4740C79B5C2C83C60B0C38BF1090501C (void);
// 0x000006EE UnityEngine.Object UnityEngine.RectTransform::get_drivenByObject()
extern void RectTransform_get_drivenByObject_m0B47BB55BEBD4403AEF062FF5C4FBDC3E36C3A0F (void);
// 0x000006EF System.Void UnityEngine.RectTransform::set_drivenByObject(UnityEngine.Object)
extern void RectTransform_set_drivenByObject_m98936E2689227990746D7311DA74225175E62603 (void);
// 0x000006F0 UnityEngine.DrivenTransformProperties UnityEngine.RectTransform::get_drivenProperties()
extern void RectTransform_get_drivenProperties_m8D3938BF88887E011FDD263C769F18D79FB68AF2 (void);
// 0x000006F1 System.Void UnityEngine.RectTransform::set_drivenProperties(UnityEngine.DrivenTransformProperties)
extern void RectTransform_set_drivenProperties_m62A6117C1FC0682E6A7F53851737AB67D3EAEAEF (void);
// 0x000006F2 System.Void UnityEngine.RectTransform::ForceUpdateRectTransforms()
extern void RectTransform_ForceUpdateRectTransforms_m93EE987D36C4DDC3FAEA540486CD7D21F8C7AD6F (void);
// 0x000006F3 System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern void RectTransform_GetLocalCorners_m8761EA5FFE1F36041809D10D8AD7BC40CF06A520 (void);
// 0x000006F4 System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern void RectTransform_GetWorldCorners_m073AA4D13C51C5654A5983EE3FE7E2E60F7761B6 (void);
// 0x000006F5 System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern void RectTransform_SetInsetAndSizeFromParentEdge_m4ED849AA88D194A7AA6B1021DF119B926F322146 (void);
// 0x000006F6 System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern void RectTransform_SetSizeWithCurrentAnchors_m6F93CD5B798E4A53F2085862EA1B4021AEAA6745 (void);
// 0x000006F7 System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern void RectTransform_SendReapplyDrivenProperties_m231712A8CDDCA340752CB72690FE808111286653 (void);
// 0x000006F8 UnityEngine.Rect UnityEngine.RectTransform::GetRectInParentSpace()
extern void RectTransform_GetRectInParentSpace_m645EE1AA5702A6E663C6131EA4B8D0B218F71204 (void);
// 0x000006F9 UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern void RectTransform_GetParentSize_mFD24CC863A4D7DFBFFE23C982E9A11E9B010D25D (void);
// 0x000006FA System.Void UnityEngine.RectTransform::.ctor()
extern void RectTransform__ctor_mAD37F57C9C6BED97B57B15061BC56C8E89B54D23 (void);
// 0x000006FB System.Void UnityEngine.RectTransform::get_rect_Injected(UnityEngine.Rect&)
extern void RectTransform_get_rect_Injected_m94E98A7B55F470FD170EBDA2D47E44CF919CD89F (void);
// 0x000006FC System.Void UnityEngine.RectTransform::get_anchorMin_Injected(UnityEngine.Vector2&)
extern void RectTransform_get_anchorMin_Injected_m11D468B602FFF89A8CC25685C71ACAA36609EF94 (void);
// 0x000006FD System.Void UnityEngine.RectTransform::set_anchorMin_Injected(UnityEngine.Vector2&)
extern void RectTransform_set_anchorMin_Injected_m8BACA1B777D070E5FF42D1D8B1D7A52FFCD59E40 (void);
// 0x000006FE System.Void UnityEngine.RectTransform::get_anchorMax_Injected(UnityEngine.Vector2&)
extern void RectTransform_get_anchorMax_Injected_m61CE870627E5CB0EFAD38D5F207BC36879378DDD (void);
// 0x000006FF System.Void UnityEngine.RectTransform::set_anchorMax_Injected(UnityEngine.Vector2&)
extern void RectTransform_set_anchorMax_Injected_m6BC717A6F528E130AD92994FB9A3614195FBFFB2 (void);
// 0x00000700 System.Void UnityEngine.RectTransform::get_anchoredPosition_Injected(UnityEngine.Vector2&)
extern void RectTransform_get_anchoredPosition_Injected_mCCBAEBA5DD529E03D42194FDE9C2AD1FBA8194E8 (void);
// 0x00000701 System.Void UnityEngine.RectTransform::set_anchoredPosition_Injected(UnityEngine.Vector2&)
extern void RectTransform_set_anchoredPosition_Injected_mCAEE3D22ADA5AF1CB4E37813EB68BF554220DAEF (void);
// 0x00000702 System.Void UnityEngine.RectTransform::get_sizeDelta_Injected(UnityEngine.Vector2&)
extern void RectTransform_get_sizeDelta_Injected_m225C77C72E97B59C07693E339E0132E9547F8982 (void);
// 0x00000703 System.Void UnityEngine.RectTransform::set_sizeDelta_Injected(UnityEngine.Vector2&)
extern void RectTransform_set_sizeDelta_Injected_mD2B6AE8C7BB4DE09F1C62B4A853E592EEF9E8399 (void);
// 0x00000704 System.Void UnityEngine.RectTransform::get_pivot_Injected(UnityEngine.Vector2&)
extern void RectTransform_get_pivot_Injected_m23095C3331BE90EB3EEFFDAE0DAD791E79485F7F (void);
// 0x00000705 System.Void UnityEngine.RectTransform::set_pivot_Injected(UnityEngine.Vector2&)
extern void RectTransform_set_pivot_Injected_m2914C16D5516DE065A932CB43B61C6EEA3C5D3D3 (void);
// 0x00000706 System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern void ReapplyDrivenProperties__ctor_m0633C1B8E1B058DF2C3648C96D3E4DC006A76CA7 (void);
// 0x00000707 System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern void ReapplyDrivenProperties_Invoke_m37F24671E6F3C60B3D0C7E0CE234B8E125D5928A (void);
// 0x00000708 System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern void ReapplyDrivenProperties_BeginInvoke_m5C352648167A36402E86BEDDCAE8FA4784C03604 (void);
// 0x00000709 System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern void ReapplyDrivenProperties_EndInvoke_mC8DF7BD47EC8976C477491961F44912BA4C0CD7C (void);
// 0x0000070A System.Void UnityEngine.Transform::.ctor()
extern void Transform__ctor_mE8E10A06C8922623BAC6053550D19B2E297C2F35 (void);
// 0x0000070B UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern void Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (void);
// 0x0000070C System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (void);
// 0x0000070D UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern void Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8 (void);
// 0x0000070E System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (void);
// 0x0000070F UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern void Transform_get_eulerAngles_mF2D798FA8B18F7A1A0C4A2198329ADBAF07E37CA (void);
// 0x00000710 UnityEngine.Vector3 UnityEngine.Transform::get_localEulerAngles()
extern void Transform_get_localEulerAngles_m445AD7F6706B0BDABA8A875C899EB1E1DF1A2A2B (void);
// 0x00000711 UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern void Transform_get_up_m3E443F6EB278D547946E80D77065A871BEEEE282 (void);
// 0x00000712 UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern void Transform_get_forward_m0BE1E88B86049ADA39391C3ACED2314A624BC67F (void);
// 0x00000713 UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern void Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9 (void);
// 0x00000714 System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern void Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935 (void);
// 0x00000715 UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern void Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE (void);
// 0x00000716 System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern void Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A (void);
// 0x00000717 UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern void Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA (void);
// 0x00000718 System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (void);
// 0x00000719 UnityEngine.Transform UnityEngine.Transform::get_parent()
extern void Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403 (void);
// 0x0000071A System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern void Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E (void);
// 0x0000071B UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern void Transform_get_parentInternal_mEE407FBF144B4EE785164788FD455CAA82DC7C2E (void);
// 0x0000071C System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern void Transform_set_parentInternal_m8534EFFADCF054FFA081769F84256F9921B0258C (void);
// 0x0000071D UnityEngine.Transform UnityEngine.Transform::GetParent()
extern void Transform_GetParent_m1C9AFA68C014287E3D62A496A5F9AE16EF9BD7E6 (void);
// 0x0000071E System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern void Transform_SetParent_mFAF9209CAB6A864552074BA065D740924A4BF979 (void);
// 0x0000071F System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern void Transform_SetParent_m268E3814921D90882EFECE244A797264DE2A5E35 (void);
// 0x00000720 UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern void Transform_get_worldToLocalMatrix_m4791F881839B1087B17DC126FC0CA7F9A596073E (void);
// 0x00000721 UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern void Transform_get_localToWorldMatrix_mBC86B8C7BA6F53DAB8E0120D77729166399A0EED (void);
// 0x00000722 System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern void Transform_Translate_m91072CBFB456E51FC3435D890E3F7E6A04F4BABD (void);
// 0x00000723 System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Transform)
extern void Transform_Translate_mB8C4818A228496C1ADD6C2CA5895D765C94A2EF5 (void);
// 0x00000724 System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern void Transform_Rotate_m3424566A0D19A1487AE3A82B08C47F2A2D2A26CB (void);
// 0x00000725 UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern void Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF (void);
// 0x00000726 UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern void Transform_TransformPoint_mA96DC2A20EE7F4F915F7509863A18D99F5DD76CB (void);
// 0x00000727 UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(System.Single,System.Single,System.Single)
extern void Transform_TransformPoint_m363B3A9E2C3A9A52F4B872CF34F476D87CCC8CEC (void);
// 0x00000728 UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern void Transform_InverseTransformPoint_mB6E3145F20B531B4A781C194BAC43A8255C96C47 (void);
// 0x00000729 System.Int32 UnityEngine.Transform::get_childCount()
extern void Transform_get_childCount_m7665D779DCDB6B175FB52A254276CDF0C384A724 (void);
// 0x0000072A System.Void UnityEngine.Transform::SetAsFirstSibling()
extern void Transform_SetAsFirstSibling_m2CAD80F7C9D89EE145BC9D3D0937D6EBEE909531 (void);
// 0x0000072B System.Void UnityEngine.Transform::SetAsLastSibling()
extern void Transform_SetAsLastSibling_mE3DD5E6421A08BACF1E86FC0EC7EE3AFA262A990 (void);
// 0x0000072C System.Void UnityEngine.Transform::SetSiblingIndex(System.Int32)
extern void Transform_SetSiblingIndex_m108C43B950B2279BF46C914F1A428A9DE0AC0FD2 (void);
// 0x0000072D System.Int32 UnityEngine.Transform::GetSiblingIndex()
extern void Transform_GetSiblingIndex_m6FEF9F4DAB8BEAB964A806F3CEE387C1F462B4C1 (void);
// 0x0000072E UnityEngine.Transform UnityEngine.Transform::FindRelativeTransformWithPath(UnityEngine.Transform,System.String,System.Boolean)
extern void Transform_FindRelativeTransformWithPath_mE13AC72C52AEA193FA2BED0BDE2BF24CEAC13186 (void);
// 0x0000072F UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern void Transform_Find_m673797B6329C2669A543904532ABA1680DA4EAD1 (void);
// 0x00000730 UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern void Transform_get_lossyScale_m9C2597B28BE066FC061B7D7508750E5D5EA9850F (void);
// 0x00000731 System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern void Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80 (void);
// 0x00000732 System.Void UnityEngine.Transform::set_hasChanged(System.Boolean)
extern void Transform_set_hasChanged_mA2BDCABEEF63468A4CF354CB7ACFDCC8E910A3D9 (void);
// 0x00000733 System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern void Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC (void);
// 0x00000734 UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern void Transform_GetChild_mC86B9B61E4EC086A571B09EA7A33FFBF50DF52D3 (void);
// 0x00000735 System.Void UnityEngine.Transform::get_position_Injected(UnityEngine.Vector3&)
extern void Transform_get_position_Injected_mFD1BD0E2D17761BA08289ABBB4F87EDFFF7C1EBB (void);
// 0x00000736 System.Void UnityEngine.Transform::set_position_Injected(UnityEngine.Vector3&)
extern void Transform_set_position_Injected_mB6BEBF6B460A566E933ED59C4470ED58D81B3226 (void);
// 0x00000737 System.Void UnityEngine.Transform::get_localPosition_Injected(UnityEngine.Vector3&)
extern void Transform_get_localPosition_Injected_mC1E8F9DAC652621188ABFB58571782157E4C8FBA (void);
// 0x00000738 System.Void UnityEngine.Transform::set_localPosition_Injected(UnityEngine.Vector3&)
extern void Transform_set_localPosition_Injected_m8B4E45BAADCDD69683EB6424992FC9B9045927DE (void);
// 0x00000739 System.Void UnityEngine.Transform::get_rotation_Injected(UnityEngine.Quaternion&)
extern void Transform_get_rotation_Injected_m41BEC8ACE323E571978CED341997B1174340701B (void);
// 0x0000073A System.Void UnityEngine.Transform::set_rotation_Injected(UnityEngine.Quaternion&)
extern void Transform_set_rotation_Injected_m1B409EA2BBF0C5DEE05153F4CD5134669AA2E5C0 (void);
// 0x0000073B System.Void UnityEngine.Transform::get_localRotation_Injected(UnityEngine.Quaternion&)
extern void Transform_get_localRotation_Injected_m1ADF4910B326BAA828892B3ADC5AD1A332DE963B (void);
// 0x0000073C System.Void UnityEngine.Transform::set_localRotation_Injected(UnityEngine.Quaternion&)
extern void Transform_set_localRotation_Injected_mF84F8CFA00AABFB7520AB782BA8A6E4BBF24FDD5 (void);
// 0x0000073D System.Void UnityEngine.Transform::get_localScale_Injected(UnityEngine.Vector3&)
extern void Transform_get_localScale_Injected_mA8987BAB5DA11154A22E2B36995C7328792371BE (void);
// 0x0000073E System.Void UnityEngine.Transform::set_localScale_Injected(UnityEngine.Vector3&)
extern void Transform_set_localScale_Injected_m9BF22FF0CD55A5008834951B58BB8E70D6982AB2 (void);
// 0x0000073F System.Void UnityEngine.Transform::get_worldToLocalMatrix_Injected(UnityEngine.Matrix4x4&)
extern void Transform_get_worldToLocalMatrix_Injected_mFEC701DE6F97A22DF1718EB82FBE3C3E62447936 (void);
// 0x00000740 System.Void UnityEngine.Transform::get_localToWorldMatrix_Injected(UnityEngine.Matrix4x4&)
extern void Transform_get_localToWorldMatrix_Injected_mF5629FA21895EB6851367E1636283C7FB70333A9 (void);
// 0x00000741 System.Void UnityEngine.Transform::TransformDirection_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_TransformDirection_Injected_m5E7C9D4E879820DF32F1CB1DE1504C59B8E98943 (void);
// 0x00000742 System.Void UnityEngine.Transform::TransformPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_TransformPoint_Injected_mB697D04DF989E68C8AAFAE6BFBBE718B68CB477D (void);
// 0x00000743 System.Void UnityEngine.Transform::InverseTransformPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_InverseTransformPoint_Injected_m320ED08EABA9713FDF7BDAD425630D567D39AB1D (void);
// 0x00000744 System.Void UnityEngine.Transform::get_lossyScale_Injected(UnityEngine.Vector3&)
extern void Transform_get_lossyScale_Injected_m6521BCE12BE0D202E15CDC24EC11304CD837EAE4 (void);
// 0x00000745 System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern void Enumerator__ctor_mBF5A46090D26A1DD98484C00389566FD8CB80770 (void);
// 0x00000746 System.Object UnityEngine.Transform/Enumerator::get_Current()
extern void Enumerator_get_Current_mD91FA41B0959224F24BF83051D46FCF3AF82F773 (void);
// 0x00000747 System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern void Enumerator_MoveNext_mF27E895DC4BB3826D2F00E9484A9ECC635770031 (void);
// 0x00000748 System.Void UnityEngine.Transform/Enumerator::Reset()
extern void Enumerator_Reset_mA4AD59858E0D61FE247C0E158737A4C02FCE244F (void);
// 0x00000749 UnityEngine.Color UnityEngine.SpriteRenderer::get_color()
extern void SpriteRenderer_get_color_m1456AB27D5B09F28A273EC0BBD4F03A0FDA51E99 (void);
// 0x0000074A System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern void SpriteRenderer_set_color_m0729526C86891ADD11611CD13A7A18B851355580 (void);
// 0x0000074B System.Void UnityEngine.SpriteRenderer::get_color_Injected(UnityEngine.Color&)
extern void SpriteRenderer_get_color_Injected_m06403F5B2B080BA7609454575BC793E00B1C0870 (void);
// 0x0000074C System.Void UnityEngine.SpriteRenderer::set_color_Injected(UnityEngine.Color&)
extern void SpriteRenderer_set_color_Injected_mFE58729552E9A143B3C8EA7B89827126DAB3DB90 (void);
// 0x0000074D System.Void UnityEngine.Sprite::.ctor()
extern void Sprite__ctor_m8559FBC54BD7CDA181B190797CC8AC6FB1310F9E (void);
// 0x0000074E System.Int32 UnityEngine.Sprite::GetPackingMode()
extern void Sprite_GetPackingMode_mF0507D88752CDA45A9283445067070092E524317 (void);
// 0x0000074F System.Int32 UnityEngine.Sprite::GetPackingRotation()
extern void Sprite_GetPackingRotation_mEB28589988DEEA60B74E3239182B2F7E4EEE4A6D (void);
// 0x00000750 System.Int32 UnityEngine.Sprite::GetPacked()
extern void Sprite_GetPacked_mBDE07283B07E7FB8892D309C5EDC81584C849BCC (void);
// 0x00000751 UnityEngine.Rect UnityEngine.Sprite::GetTextureRect()
extern void Sprite_GetTextureRect_mE506ABF33181E32E82B75479EE4A0910350B1BF9 (void);
// 0x00000752 UnityEngine.Vector2 UnityEngine.Sprite::GetTextureRectOffset()
extern void Sprite_GetTextureRectOffset_m6CC9A016E5B4D1A665E2E87EC132F9FAC8177FD6 (void);
// 0x00000753 UnityEngine.Vector4 UnityEngine.Sprite::GetInnerUVs()
extern void Sprite_GetInnerUVs_m273E051E7DF38ED3D6077781D75A1C1019CABA25 (void);
// 0x00000754 UnityEngine.Vector4 UnityEngine.Sprite::GetOuterUVs()
extern void Sprite_GetOuterUVs_mD78E47470B4D8AD231F194E256136B0094ECEBC5 (void);
// 0x00000755 UnityEngine.Vector4 UnityEngine.Sprite::GetPadding()
extern void Sprite_GetPadding_m5781452D40FAE3B7D0CE78BF8808637FBFE78105 (void);
// 0x00000756 UnityEngine.Sprite UnityEngine.Sprite::CreateSpriteWithoutTextureScripting(UnityEngine.Rect,UnityEngine.Vector2,System.Single,UnityEngine.Texture2D)
extern void Sprite_CreateSpriteWithoutTextureScripting_mBE41B7AA3AA952B14E7DCC600AA27728AC165C6E (void);
// 0x00000757 UnityEngine.Sprite UnityEngine.Sprite::CreateSprite(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4,System.Boolean)
extern void Sprite_CreateSprite_m1F676037B8C5EB3E216F70EE4630662FF144AFC5 (void);
// 0x00000758 UnityEngine.Bounds UnityEngine.Sprite::get_bounds()
extern void Sprite_get_bounds_mD440465B889CCD2D80D118F9174FD5EAB19AE874 (void);
// 0x00000759 UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern void Sprite_get_rect_mF1D59ED35D077D9B5075E2114605FDEB728D3AFF (void);
// 0x0000075A UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern void Sprite_get_border_m940E803CAD380B3B1B88371D7A4E74DF9A48604F (void);
// 0x0000075B UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern void Sprite_get_texture_mA1FF8462BBB398DC8B3F0F92B2AB41BDA6AF69A5 (void);
// 0x0000075C System.Single UnityEngine.Sprite::get_pixelsPerUnit()
extern void Sprite_get_pixelsPerUnit_m54E3B43BD3D255D18CAE3DC8D963A81846983030 (void);
// 0x0000075D System.Single UnityEngine.Sprite::get_spriteAtlasTextureScale()
extern void Sprite_get_spriteAtlasTextureScale_m8E2E3B0967DA89F39C725A8E0F4C27EE8C1EAF7C (void);
// 0x0000075E UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
extern void Sprite_get_associatedAlphaSplitTexture_m2EF38CF62616FBBBBAE7876ECBCC596DB3F42156 (void);
// 0x0000075F UnityEngine.Vector2 UnityEngine.Sprite::get_pivot()
extern void Sprite_get_pivot_m8E3D24C208E01EC8464B0E63FBF3FB9429E4C1D9 (void);
// 0x00000760 System.Boolean UnityEngine.Sprite::get_packed()
extern void Sprite_get_packed_m501A9E7D2C44867665FB579FD1A8C5D397C872C3 (void);
// 0x00000761 UnityEngine.SpritePackingMode UnityEngine.Sprite::get_packingMode()
extern void Sprite_get_packingMode_m1B5AA0F5476DAEADFF14F65E99944B54940D869F (void);
// 0x00000762 UnityEngine.SpritePackingRotation UnityEngine.Sprite::get_packingRotation()
extern void Sprite_get_packingRotation_m47E139421BE43B85B69CE5A2C41D287E14CF037B (void);
// 0x00000763 UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern void Sprite_get_textureRect_m8CDA38796589CB967909F78076E7138907814DCD (void);
// 0x00000764 UnityEngine.Vector2 UnityEngine.Sprite::get_textureRectOffset()
extern void Sprite_get_textureRectOffset_m7E467306131323AA9240A7A1F4F408637FC33741 (void);
// 0x00000765 UnityEngine.Vector2[] UnityEngine.Sprite::get_vertices()
extern void Sprite_get_vertices_mD858385A07239A56691D1559728B1B5765C32722 (void);
// 0x00000766 System.UInt16[] UnityEngine.Sprite::get_triangles()
extern void Sprite_get_triangles_m3B0A097930B40C800E0591E5B095D118D2A33D2E (void);
// 0x00000767 UnityEngine.Vector2[] UnityEngine.Sprite::get_uv()
extern void Sprite_get_uv_mBD484CDCD2DF54AAE452ADBA927806193CB0FE84 (void);
// 0x00000768 System.Int32 UnityEngine.Sprite::GetPhysicsShapeCount()
extern void Sprite_GetPhysicsShapeCount_m6042C613B97839AFFCCB92CBC74E7B0255FCC86A (void);
// 0x00000769 System.Int32 UnityEngine.Sprite::GetPhysicsShapePointCount(System.Int32)
extern void Sprite_GetPhysicsShapePointCount_m8298AD1731EB569E745DA49639953282A10F3BED (void);
// 0x0000076A System.Int32 UnityEngine.Sprite::Internal_GetPhysicsShapePointCount(System.Int32)
extern void Sprite_Internal_GetPhysicsShapePointCount_m9C7BA414628B5D7B83D466A5F32E5E58BA5E648A (void);
// 0x0000076B System.Int32 UnityEngine.Sprite::GetPhysicsShape(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void Sprite_GetPhysicsShape_mD719BB7A84247FB5734E835AD0128465D6DE9F6B (void);
// 0x0000076C System.Void UnityEngine.Sprite::GetPhysicsShapeImpl(UnityEngine.Sprite,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void Sprite_GetPhysicsShapeImpl_mDFC2D7CD8320C0EA9BAE239F8A1B4F31718FC96D (void);
// 0x0000076D System.Void UnityEngine.Sprite::OverridePhysicsShape(System.Collections.Generic.IList`1<UnityEngine.Vector2[]>)
extern void Sprite_OverridePhysicsShape_m3C19FF12D36CCEFDF8AB9D64F5C8A2B0A24A358A (void);
// 0x0000076E System.Void UnityEngine.Sprite::OverridePhysicsShapeCount(UnityEngine.Sprite,System.Int32)
extern void Sprite_OverridePhysicsShapeCount_m97519C52CFD40529A10AD9E418ED4337A7EEE9E8 (void);
// 0x0000076F System.Void UnityEngine.Sprite::OverridePhysicsShape(UnityEngine.Sprite,UnityEngine.Vector2[],System.Int32)
extern void Sprite_OverridePhysicsShape_m89C6D93927591E283E6769746727AC37B1F2C657 (void);
// 0x00000770 System.Void UnityEngine.Sprite::OverrideGeometry(UnityEngine.Vector2[],System.UInt16[])
extern void Sprite_OverrideGeometry_m181A7790256B034360A8AA2DBDA4E7FAEA1B37EE (void);
// 0x00000771 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Rect,UnityEngine.Vector2,System.Single,UnityEngine.Texture2D)
extern void Sprite_Create_m58E6434E5CFE97E932AA45CBCE45226C75DB1F60 (void);
// 0x00000772 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Rect,UnityEngine.Vector2,System.Single)
extern void Sprite_Create_m4ECCFC2D3CA4DE742ECD9E4622967F7A4782877D (void);
// 0x00000773 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4,System.Boolean)
extern void Sprite_Create_mE9E237E1E936F7A7398361253D1D7C7B7ECD622F (void);
// 0x00000774 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4)
extern void Sprite_Create_m4BF8E8E0F77846FA88502FD896E3AFB6D2AB2E0A (void);
// 0x00000775 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32,UnityEngine.SpriteMeshType)
extern void Sprite_Create_mBF0676DF6C3CB8554451DC01E0713CF73C84DB4A (void);
// 0x00000776 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32)
extern void Sprite_Create_mE9C9B96A1D1EC541FF41148E2001223C2030F803 (void);
// 0x00000777 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single)
extern void Sprite_Create_m84A724DB0F0D73AEBE5296B4324D61EBCA72A843 (void);
// 0x00000778 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2)
extern void Sprite_Create_m9ED36DA8DA0637F93BA2753A16405EB0F7B17A3C (void);
// 0x00000779 System.Void UnityEngine.Sprite::GetTextureRect_Injected(UnityEngine.Rect&)
extern void Sprite_GetTextureRect_Injected_m3D0143FD7E689267FAE3164F7C149DB5FF3384C2 (void);
// 0x0000077A System.Void UnityEngine.Sprite::GetTextureRectOffset_Injected(UnityEngine.Vector2&)
extern void Sprite_GetTextureRectOffset_Injected_mF1E684EA58CEE5FDD1706D13F12C4C2A15314AF0 (void);
// 0x0000077B System.Void UnityEngine.Sprite::GetInnerUVs_Injected(UnityEngine.Vector4&)
extern void Sprite_GetInnerUVs_Injected_m19AF3A32647EE2153374B4B58CB248A5E3715F6B (void);
// 0x0000077C System.Void UnityEngine.Sprite::GetOuterUVs_Injected(UnityEngine.Vector4&)
extern void Sprite_GetOuterUVs_Injected_m57E56A2D7686D47E6948511F102AF8135E98B2B0 (void);
// 0x0000077D System.Void UnityEngine.Sprite::GetPadding_Injected(UnityEngine.Vector4&)
extern void Sprite_GetPadding_Injected_m843873F288F8CBC4BDDF1BBE20211405039ABBDC (void);
// 0x0000077E UnityEngine.Sprite UnityEngine.Sprite::CreateSpriteWithoutTextureScripting_Injected(UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,UnityEngine.Texture2D)
extern void Sprite_CreateSpriteWithoutTextureScripting_Injected_m8E5F54D85D60860C93D2A0C77AF8193CFB6C3472 (void);
// 0x0000077F UnityEngine.Sprite UnityEngine.Sprite::CreateSprite_Injected(UnityEngine.Texture2D,UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4&,System.Boolean)
extern void Sprite_CreateSprite_Injected_m5393FA2042C8AF5D7E9CC32A2255270EE3DA702D (void);
// 0x00000780 System.Void UnityEngine.Sprite::get_bounds_Injected(UnityEngine.Bounds&)
extern void Sprite_get_bounds_Injected_m6422C2DBFD84A7B7F921DCA14BDFF2157738194B (void);
// 0x00000781 System.Void UnityEngine.Sprite::get_rect_Injected(UnityEngine.Rect&)
extern void Sprite_get_rect_Injected_mABF4FCC2AEDD9EE874797E35EDEFF023A52F5830 (void);
// 0x00000782 System.Void UnityEngine.Sprite::get_border_Injected(UnityEngine.Vector4&)
extern void Sprite_get_border_Injected_mA56DD9A38B61783341DF35C808FBFE93A1BD4BB6 (void);
// 0x00000783 System.Void UnityEngine.Sprite::get_pivot_Injected(UnityEngine.Vector2&)
extern void Sprite_get_pivot_Injected_m526201DCD812D7AB10AACE35E4195F7E53B633EC (void);
// 0x00000784 System.Boolean UnityEngine._Scripting.APIUpdating.APIUpdaterRuntimeHelpers::GetMovedFromAttributeDataForType(System.Type,System.String&,System.String&,System.String&)
extern void APIUpdaterRuntimeHelpers_GetMovedFromAttributeDataForType_m2574674719979232087612C3C17A760E439BCA45 (void);
// 0x00000785 System.Boolean UnityEngine._Scripting.APIUpdating.APIUpdaterRuntimeHelpers::GetObsoleteTypeRedirection(System.Type,System.String&,System.String&,System.String&)
extern void APIUpdaterRuntimeHelpers_GetObsoleteTypeRedirection_m43E0605422153F402426F8959BC2E8C65A69F597 (void);
// 0x00000786 UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
extern void DataUtility_GetInnerUV_m19FC4FF27A6733C9595B63F265EFBEC3BC183732 (void);
// 0x00000787 UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
extern void DataUtility_GetOuterUV_mB7DEA861925EECF861EEB643145C54E8BF449213 (void);
// 0x00000788 UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
extern void DataUtility_GetPadding_mE967167C8AB44F752F7C3A7B9D0A2789F5BD7034 (void);
// 0x00000789 UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
extern void DataUtility_GetMinSize_m8031F50000ACDDD00E1CE4C765FA4B0A2E9255AD (void);
// 0x0000078A System.Boolean UnityEngine.U2D.SpriteAtlasManager::RequestAtlas(System.String)
extern void SpriteAtlasManager_RequestAtlas_m792F61C44C634D9E8F1E15401C8CECB7A12F5DDE (void);
// 0x0000078B System.Void UnityEngine.U2D.SpriteAtlasManager::add_atlasRegistered(System.Action`1<UnityEngine.U2D.SpriteAtlas>)
extern void SpriteAtlasManager_add_atlasRegistered_m6742D91F217B69CC2A65D80B5F25CFA372A1E2DA (void);
// 0x0000078C System.Void UnityEngine.U2D.SpriteAtlasManager::remove_atlasRegistered(System.Action`1<UnityEngine.U2D.SpriteAtlas>)
extern void SpriteAtlasManager_remove_atlasRegistered_m55564CC2797E687E4B966DC1797D059ABBB04051 (void);
// 0x0000078D System.Void UnityEngine.U2D.SpriteAtlasManager::PostRegisteredAtlas(UnityEngine.U2D.SpriteAtlas)
extern void SpriteAtlasManager_PostRegisteredAtlas_m2FCA85EDC754279C0A90CC3AF5E12C3E8F6A61CB (void);
// 0x0000078E System.Void UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)
extern void SpriteAtlasManager_Register_m2C324F6E122AF09D44E4EE3F8F024323663670D2 (void);
// 0x0000078F System.Void UnityEngine.U2D.SpriteAtlasManager::.cctor()
extern void SpriteAtlasManager__cctor_m826C9096AB53C9C6CFCF342FA9FDC345A726B6C6 (void);
// 0x00000790 System.Boolean UnityEngine.U2D.SpriteAtlas::CanBindTo(UnityEngine.Sprite)
extern void SpriteAtlas_CanBindTo_m6CE0E011A4C5F489F9A62317380FB35F20DF7016 (void);
// 0x00000791 UnityEngine.Sprite UnityEngine.U2D.SpriteAtlas::GetSprite(System.String)
extern void SpriteAtlas_GetSprite_m1AD3114039CC055C4F4E0EF93ADD53BA2FE44DFE (void);
// 0x00000792 System.Boolean UnityEngine.Profiling.Profiler::get_supported()
extern void Profiler_get_supported_m4678D0BD23545682A1FB6D8581E3B1AD25EEC4F2 (void);
// 0x00000793 System.Boolean UnityEngine.Profiling.Profiler::get_enabled()
extern void Profiler_get_enabled_m0C4E87308897B1FF9485999C112E56D750E5CDFE (void);
// 0x00000794 System.Void UnityEngine.Profiling.Profiler::set_enabled(System.Boolean)
extern void Profiler_set_enabled_m4F366847649387E07AA2C482798915F5B4999B34 (void);
// 0x00000795 System.Void UnityEngine.Profiling.Profiler::BeginSample(System.String)
extern void Profiler_BeginSample_mDA159D5771838F40FC7C2829B2DC4329F8C9596B (void);
// 0x00000796 System.Void UnityEngine.Profiling.Profiler::ValidateArguments(System.String)
extern void Profiler_ValidateArguments_m212D52E66EEBBFC4A04C9FF0CE27388E0C605CB0 (void);
// 0x00000797 System.Void UnityEngine.Profiling.Profiler::BeginSampleImpl(System.String,UnityEngine.Object)
extern void Profiler_BeginSampleImpl_m831FE453CD200A1F5C3C49DB9E0D831C89B70751 (void);
// 0x00000798 System.Void UnityEngine.Profiling.Profiler::EndSample()
extern void Profiler_EndSample_m15350A0463FB3C5789504B4059B0EA68D5B70A21 (void);
// 0x00000799 System.Int64 UnityEngine.Profiling.Profiler::GetMonoHeapSizeLong()
extern void Profiler_GetMonoHeapSizeLong_m448556C394B68156E0C09142F048DC91FA49E1C0 (void);
// 0x0000079A System.Int64 UnityEngine.Profiling.Profiler::GetMonoUsedSizeLong()
extern void Profiler_GetMonoUsedSizeLong_m7D63E9E033B31718C3E5D96404DF5A7609445C77 (void);
// 0x0000079B System.Int64 UnityEngine.Profiling.Profiler::GetTotalAllocatedMemoryLong()
extern void Profiler_GetTotalAllocatedMemoryLong_m8B81D9312DC29887D552EEAFBFA95794A4D4EBD1 (void);
// 0x0000079C System.Int64 UnityEngine.Profiling.Profiler::GetTotalReservedMemoryLong()
extern void Profiler_GetTotalReservedMemoryLong_m3A8A4373D1D4A9539EDD1E9F61E514407422FE79 (void);
// 0x0000079D System.Void UnityEngine.Profiling.Experimental.DebugScreenCapture::set_rawImageDataReference(Unity.Collections.NativeArray`1<System.Byte>)
extern void DebugScreenCapture_set_rawImageDataReference_mE09725CEBC8D7A846033531E33C8E9B8DA60DFDE (void);
// 0x0000079E System.Void UnityEngine.Profiling.Experimental.DebugScreenCapture::set_imageFormat(UnityEngine.TextureFormat)
extern void DebugScreenCapture_set_imageFormat_m779E16070B55AACD76279BFDDAE5CC470667D3FB (void);
// 0x0000079F System.Void UnityEngine.Profiling.Experimental.DebugScreenCapture::set_width(System.Int32)
extern void DebugScreenCapture_set_width_mD8B3C521091CD0049D323410FFD201D47B629127 (void);
// 0x000007A0 System.Void UnityEngine.Profiling.Experimental.DebugScreenCapture::set_height(System.Int32)
extern void DebugScreenCapture_set_height_mD7E3596B40E77F906A3E9517BC6CE795C0609442 (void);
// 0x000007A1 System.Void UnityEngine.Profiling.Memory.Experimental.MetaData::.ctor()
extern void MetaData__ctor_m1852CAF4EDFB43F1ABCE37819D9963CEE959A620 (void);
// 0x000007A2 System.Byte[] UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::PrepareMetadata()
extern void MemoryProfiler_PrepareMetadata_mDFBA7A9960E5B4DF4500092638CD59EB558DD42C (void);
// 0x000007A3 System.Int32 UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::WriteIntToByteArray(System.Byte[],System.Int32,System.Int32)
extern void MemoryProfiler_WriteIntToByteArray_mBC872709F6A09ADFE716F41C459C1FCC1EFF25A0 (void);
// 0x000007A4 System.Int32 UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::WriteStringToByteArray(System.Byte[],System.Int32,System.String)
extern void MemoryProfiler_WriteStringToByteArray_mCAC0D283F16F612E4796C539994FDB487A048332 (void);
// 0x000007A5 System.Void UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::FinalizeSnapshot(System.String,System.Boolean)
extern void MemoryProfiler_FinalizeSnapshot_m48FD62744888BBF0A9B13826622041226C8B9AD7 (void);
// 0x000007A6 System.Void UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::SaveScreenshotToDisk(System.String,System.Boolean,System.IntPtr,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Int32)
extern void MemoryProfiler_SaveScreenshotToDisk_mCA2AE332D689BEDE49DECACD92FDA366EB80047F (void);
// 0x000007A7 UnityEngine.iOS.DeviceGeneration UnityEngine.iOS.Device::get_generation()
extern void Device_get_generation_m1B212A72D6BB18EAED4D65DA89702D69A22A3F99 (void);
// 0x000007A8 System.Boolean UnityEngine.iOS.Device::IsAdvertisingTrackingEnabled()
extern void Device_IsAdvertisingTrackingEnabled_m8C6D05852D4437E00EF5693201863B55971B90D3 (void);
// 0x000007A9 System.Boolean UnityEngine.iOS.Device::get_advertisingTrackingEnabled()
extern void Device_get_advertisingTrackingEnabled_mC133F2C934664A3FDD03A8E060BB522115EB3AFF (void);
// 0x000007AA System.Void UnityEngine.iOS.LocalNotification::.cctor()
extern void LocalNotification__cctor_m9E47ADEC8F786289F7C94ACC65A44C318FF59685 (void);
// 0x000007AB UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern void ArgumentCache_get_unityObjectArgument_m89597514712FB91B443B9FB1A44D099F021DCFA5 (void);
// 0x000007AC System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern void ArgumentCache_get_unityObjectArgumentAssemblyTypeName_mD54EA1424879A2E07B179AE93D374100259FF534 (void);
// 0x000007AD System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern void ArgumentCache_get_intArgument_mD9D072A7856D998847F159350EAD0D9AA552F76B (void);
// 0x000007AE System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern void ArgumentCache_get_floatArgument_mDDAD91A992319CF1FFFDD4F0F87C5D162BFF187A (void);
// 0x000007AF System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern void ArgumentCache_get_stringArgument_mCD1D05766E21EEFDFD03D4DE66C088CF29F84D00 (void);
// 0x000007B0 System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern void ArgumentCache_get_boolArgument_mCCAB5FB41B0F1C8C8A2C31FB3D46DF99A7A71BE2 (void);
// 0x000007B1 System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern void ArgumentCache_TidyAssemblyTypeName_m2D4AFE74BEB5F32B14165BB52F6AB29A75306936 (void);
// 0x000007B2 System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern void ArgumentCache_OnBeforeSerialize_mCBE8301FE0DDE2E516A18051BBE3DC983BC80FBC (void);
// 0x000007B3 System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern void ArgumentCache_OnAfterDeserialize_m59072D771A42C518FECECBE2CE7EE9E15F4BE55F (void);
// 0x000007B4 System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern void ArgumentCache__ctor_m9618D660E40F991782873643F0FDCACA32A63541 (void);
// 0x000007B5 System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern void BaseInvokableCall__ctor_m232CE2068209113988BB35B50A2965FC03FC4A58 (void);
// 0x000007B6 System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern void BaseInvokableCall__ctor_m71AC21A8840CE45C2600FF784E8B0B556D7B2BA5 (void);
// 0x000007B7 System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[])
// 0x000007B8 System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg(System.Object)
// 0x000007B9 System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern void BaseInvokableCall_AllowInvoke_m0B193EBF1EF138FC5354933974DD702D3D9FF091 (void);
// 0x000007BA System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo)
// 0x000007BB System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern void InvokableCall_add_Delegate_mCE91CE04CF7A8B962FF566B018C8C516351AD0F3 (void);
// 0x000007BC System.Void UnityEngine.Events.InvokableCall::remove_Delegate(UnityEngine.Events.UnityAction)
extern void InvokableCall_remove_Delegate_m0CFD9A25842A757309236C500089752BF544E3C7 (void);
// 0x000007BD System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern void InvokableCall__ctor_m2F9F0CD09FCFFEBCBBA87EC75D9BA50058C5B873 (void);
// 0x000007BE System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern void InvokableCall__ctor_m77F593E751D2119119A5F3FD39F8F5D3B653102B (void);
// 0x000007BF System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern void InvokableCall_Invoke_mDB8C26B441658DDA48AC3AF259F4A0EBCF673FD0 (void);
// 0x000007C0 System.Void UnityEngine.Events.InvokableCall::Invoke()
extern void InvokableCall_Invoke_m0B9E7F14A2C67AB51F01745BD2C6C423114C9394 (void);
// 0x000007C1 System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern void InvokableCall_Find_mDC13296B10EFCD0A92E486CD5787E07890C7B8CC (void);
// 0x000007C2 System.Void UnityEngine.Events.InvokableCall`1::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
// 0x000007C3 System.Void UnityEngine.Events.InvokableCall`1::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
// 0x000007C4 System.Void UnityEngine.Events.InvokableCall`1::.ctor(System.Object,System.Reflection.MethodInfo)
// 0x000007C5 System.Void UnityEngine.Events.InvokableCall`1::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// 0x000007C6 System.Void UnityEngine.Events.InvokableCall`1::Invoke(System.Object[])
// 0x000007C7 System.Void UnityEngine.Events.InvokableCall`1::Invoke(T1)
// 0x000007C8 System.Boolean UnityEngine.Events.InvokableCall`1::Find(System.Object,System.Reflection.MethodInfo)
// 0x000007C9 System.Void UnityEngine.Events.InvokableCall`2::.ctor(System.Object,System.Reflection.MethodInfo)
// 0x000007CA System.Void UnityEngine.Events.InvokableCall`2::Invoke(System.Object[])
// 0x000007CB System.Boolean UnityEngine.Events.InvokableCall`2::Find(System.Object,System.Reflection.MethodInfo)
// 0x000007CC System.Void UnityEngine.Events.InvokableCall`3::.ctor(System.Object,System.Reflection.MethodInfo)
// 0x000007CD System.Void UnityEngine.Events.InvokableCall`3::Invoke(System.Object[])
// 0x000007CE System.Void UnityEngine.Events.InvokableCall`3::Invoke(T1,T2,T3)
// 0x000007CF System.Boolean UnityEngine.Events.InvokableCall`3::Find(System.Object,System.Reflection.MethodInfo)
// 0x000007D0 System.Void UnityEngine.Events.InvokableCall`4::.ctor(System.Object,System.Reflection.MethodInfo)
// 0x000007D1 System.Void UnityEngine.Events.InvokableCall`4::Invoke(System.Object[])
// 0x000007D2 System.Boolean UnityEngine.Events.InvokableCall`4::Find(System.Object,System.Reflection.MethodInfo)
// 0x000007D3 System.Void UnityEngine.Events.CachedInvokableCall`1::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// 0x000007D4 System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(System.Object[])
// 0x000007D5 System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(T)
// 0x000007D6 UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern void PersistentCall_get_target_mCAD7D486F28743D49DCF268B791721313A7FC8C5 (void);
// 0x000007D7 System.String UnityEngine.Events.PersistentCall::get_methodName()
extern void PersistentCall_get_methodName_m164BE545C327516CABE9464D88A417B7F00010E1 (void);
// 0x000007D8 UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern void PersistentCall_get_mode_mC88324F8D18B5E4E79045AE1F99DF3EB36CEE644 (void);
// 0x000007D9 UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern void PersistentCall_get_arguments_m53AFE12B586F0C8948D60852866EC71F38B3BAE1 (void);
// 0x000007DA System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern void PersistentCall_IsValid_mF3E3A11AF1547E84B2AC78AFAF6B2907C9B159AE (void);
// 0x000007DB UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern void PersistentCall_GetRuntimeCall_m68F27109F868C451A47DAC3863B27839C515C3A6 (void);
// 0x000007DC UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern void PersistentCall_GetObjectCall_m895EBE1B8E2A4FB297A8C268371CA4CD320F72C9 (void);
// 0x000007DD System.Void UnityEngine.Events.PersistentCall::.ctor()
extern void PersistentCall__ctor_mBF65325BE6B4EBC6B3E8ADAD3C6FA77EF5BBEFA8 (void);
// 0x000007DE System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern void PersistentCallGroup__ctor_m46B7802855B55149B9C1ECD9C9C97B8025BF2D7A (void);
// 0x000007DF System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern void PersistentCallGroup_Initialize_m9F47B3D16F78FD424D50E9AE41EB066BA9C681A3 (void);
// 0x000007E0 System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern void InvokableCallList_AddPersistentInvokableCall_m62BDD6521FB7B68B58673D5C5B1117FCE4826CAD (void);
// 0x000007E1 System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern void InvokableCallList_AddListener_mE4069F40E8762EF21140D688175D7A4E46FD2E83 (void);
// 0x000007E2 System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern void InvokableCallList_RemoveListener_mC886122D45A6682A85066E48637339065085D6DE (void);
// 0x000007E3 System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern void InvokableCallList_ClearPersistent_m4038DB499DCD84B79C0F1A698AAA7B9519553D49 (void);
// 0x000007E4 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::PrepareInvoke()
extern void InvokableCallList_PrepareInvoke_m5BB28A5FBF10C84ECF5B52EBC52F9FCCDC840DC1 (void);
// 0x000007E5 System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern void InvokableCallList__ctor_m8A5D49D58DCCC3D962D84C6DAD703DCABE74EC2D (void);
// 0x000007E6 System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern void UnityEventBase__ctor_m57AF08DAFA9C1B4F4C8DA855116900BAE8DF9595 (void);
// 0x000007E7 System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m147F610873545A23E9005CCB35CA6A05887E7599 (void);
// 0x000007E8 System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3A87E89948C5FF32BD5BA1BDD1A4D791738A141E (void);
// 0x000007E9 System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object)
// 0x000007EA UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x000007EB System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern void UnityEventBase_FindMethod_m4E838DE0D107C86C7CAA5B05D4683066E9EB9C32 (void);
// 0x000007EC System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern void UnityEventBase_FindMethod_m82A135403D677ECE42CEE42A322E15EB2EA53797 (void);
// 0x000007ED System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern void UnityEventBase_DirtyPersistentCalls_m31D9B2D3B265718318F4D7E87373E53F249E65EB (void);
// 0x000007EE System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern void UnityEventBase_RebuildPersistentCallsIfNeeded_mFC89AED628B42E5B1CBCC702222A6DF97605234F (void);
// 0x000007EF System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern void UnityEventBase_AddCall_mD45F68C1A40E2BD9B0754490B7709846B84E8075 (void);
// 0x000007F0 System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern void UnityEventBase_RemoveListener_mE7EBC544115373D2357599AC07F41F13A8C5A49E (void);
// 0x000007F1 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.UnityEventBase::PrepareInvoke()
extern void UnityEventBase_PrepareInvoke_mFA3E2C97DB776A1089DCC85C9F1DA75C295032AE (void);
// 0x000007F2 System.String UnityEngine.Events.UnityEventBase::ToString()
extern void UnityEventBase_ToString_m7672D78CA070AC49FFF04E645523864C300DD66D (void);
// 0x000007F3 System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern void UnityEventBase_GetValidMethodInfo_m4521621AB72C7265A2C0EC6911BE4DC42A99B6A5 (void);
// 0x000007F4 System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern void UnityAction__ctor_mEFC4B92529CE83DF72501F92E07EC5598C54BDAC (void);
// 0x000007F5 System.Void UnityEngine.Events.UnityAction::Invoke()
extern void UnityAction_Invoke_mC9FF5AA1F82FDE635B3B6644CE71C94C31C3E71A (void);
// 0x000007F6 System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void UnityAction_BeginInvoke_m6819B1057D192033B17EB15C9E34305720F0401C (void);
// 0x000007F7 System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern void UnityAction_EndInvoke_m9C465516D5977EF185DCEB6CA81D0B90EDAD7370 (void);
// 0x000007F8 System.Void UnityEngine.Events.UnityEvent::.ctor()
extern void UnityEvent__ctor_m2F8C02F28E289CA65598FF4FA8EAB84D955FF028 (void);
// 0x000007F9 System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern void UnityEvent_AddListener_m31973FDDC5BB0B2828AB6EF519EC4FD6563499C9 (void);
// 0x000007FA System.Void UnityEngine.Events.UnityEvent::RemoveListener(UnityEngine.Events.UnityAction)
extern void UnityEvent_RemoveListener_m26034605306E868B2E332675FCCBDA37CECBBA19 (void);
// 0x000007FB System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern void UnityEvent_FindMethod_Impl_mC96F40A83BB4D1430E254DAE9B091E27E42E8796 (void);
// 0x000007FC UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern void UnityEvent_GetDelegate_m8D277E2D713BB3605B3D46E5A3DB708B6A338EB0 (void);
// 0x000007FD UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern void UnityEvent_GetDelegate_mDFBD636D71E24D75D0851959256A3B97BA842865 (void);
// 0x000007FE System.Void UnityEngine.Events.UnityEvent::Invoke()
extern void UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325 (void);
// 0x000007FF System.Void UnityEngine.Events.UnityAction`1::.ctor(System.Object,System.IntPtr)
// 0x00000800 System.Void UnityEngine.Events.UnityAction`1::Invoke(T0)
// 0x00000801 System.IAsyncResult UnityEngine.Events.UnityAction`1::BeginInvoke(T0,System.AsyncCallback,System.Object)
// 0x00000802 System.Void UnityEngine.Events.UnityAction`1::EndInvoke(System.IAsyncResult)
// 0x00000803 System.Void UnityEngine.Events.UnityEvent`1::.ctor()
// 0x00000804 System.Void UnityEngine.Events.UnityEvent`1::AddListener(UnityEngine.Events.UnityAction`1<T0>)
// 0x00000805 System.Void UnityEngine.Events.UnityEvent`1::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
// 0x00000806 System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1::FindMethod_Impl(System.String,System.Object)
// 0x00000807 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x00000808 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
// 0x00000809 System.Void UnityEngine.Events.UnityEvent`1::Invoke(T0)
// 0x0000080A System.Void UnityEngine.Events.UnityAction`2::.ctor(System.Object,System.IntPtr)
// 0x0000080B System.Void UnityEngine.Events.UnityAction`2::Invoke(T0,T1)
// 0x0000080C System.IAsyncResult UnityEngine.Events.UnityAction`2::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
// 0x0000080D System.Void UnityEngine.Events.UnityAction`2::EndInvoke(System.IAsyncResult)
// 0x0000080E System.Void UnityEngine.Events.UnityEvent`2::.ctor()
// 0x0000080F System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2::FindMethod_Impl(System.String,System.Object)
// 0x00000810 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x00000811 System.Void UnityEngine.Events.UnityAction`3::.ctor(System.Object,System.IntPtr)
// 0x00000812 System.Void UnityEngine.Events.UnityAction`3::Invoke(T0,T1,T2)
// 0x00000813 System.IAsyncResult UnityEngine.Events.UnityAction`3::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
// 0x00000814 System.Void UnityEngine.Events.UnityAction`3::EndInvoke(System.IAsyncResult)
// 0x00000815 System.Void UnityEngine.Events.UnityEvent`3::.ctor()
// 0x00000816 System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3::FindMethod_Impl(System.String,System.Object)
// 0x00000817 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x00000818 System.Void UnityEngine.Events.UnityEvent`3::Invoke(T0,T1,T2)
// 0x00000819 System.Void UnityEngine.Events.UnityAction`4::.ctor(System.Object,System.IntPtr)
// 0x0000081A System.Void UnityEngine.Events.UnityAction`4::Invoke(T0,T1,T2,T3)
// 0x0000081B System.IAsyncResult UnityEngine.Events.UnityAction`4::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
// 0x0000081C System.Void UnityEngine.Events.UnityAction`4::EndInvoke(System.IAsyncResult)
// 0x0000081D System.Void UnityEngine.Events.UnityEvent`4::.ctor()
// 0x0000081E System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4::FindMethod_Impl(System.String,System.Object)
// 0x0000081F UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x00000820 System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern void FormerlySerializedAsAttribute__ctor_m770651B828F499F804DB06A777E8A4103A3ED2BD (void);
// 0x00000821 System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::get_oldName()
extern void FormerlySerializedAsAttribute_get_oldName_m501D396073FA321AF7326D5C5915333BF67AFD16 (void);
// 0x00000822 System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
extern void PreserveAttribute__ctor_mD842EE86496947B39FE0FBC67393CE4401AC53AA (void);
// 0x00000823 System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::Set(System.Boolean,System.String,System.String,System.String)
extern void MovedFromAttributeData_Set_m244D6454BEA753FA4C7C3F2393A5DADCB3166B1C (void);
// 0x00000824 System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String)
extern void MovedFromAttribute__ctor_mF87685F9D527910B31D3EF58F0EAA297E1944B42 (void);
// 0x00000825 System.Boolean UnityEngine.SceneManagement.Scene::IsValidInternal(System.Int32)
extern void Scene_IsValidInternal_mB85F25DED2B4D8B3E15D1CD6F7A7B354CEA43E74 (void);
// 0x00000826 System.String UnityEngine.SceneManagement.Scene::GetPathInternal(System.Int32)
extern void Scene_GetPathInternal_mC727722DC9EA785029CE73DF89E12B9F15DCBC8A (void);
// 0x00000827 System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
extern void Scene_GetNameInternal_mE5393E58A37BC0031702DFBA6AEC577058D2941E (void);
// 0x00000828 System.Boolean UnityEngine.SceneManagement.Scene::GetIsLoadedInternal(System.Int32)
extern void Scene_GetIsLoadedInternal_m6D1D452018259662F506EBB33BFD3B55A613E329 (void);
// 0x00000829 System.Int32 UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)
extern void Scene_GetBuildIndexInternal_m49FEDB2B2DE47388E130C1FE9B9945EED2BC10BA (void);
// 0x0000082A System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern void Scene_get_handle_mFAB5C41D41B90B9CEBB3918A6F3638BD41E980C9 (void);
// 0x0000082B System.Boolean UnityEngine.SceneManagement.Scene::IsValid()
extern void Scene_IsValid_mDA115AE2634680CAE92D2F844347AB4CD2185437 (void);
// 0x0000082C System.String UnityEngine.SceneManagement.Scene::get_path()
extern void Scene_get_path_m6D313F8791A3376EBE73AA6F62C02731F7F48A86 (void);
// 0x0000082D System.String UnityEngine.SceneManagement.Scene::get_name()
extern void Scene_get_name_m0E63ED0F050FCC35A4216220C584BE3D3F77B0E1 (void);
// 0x0000082E System.Boolean UnityEngine.SceneManagement.Scene::get_isLoaded()
extern void Scene_get_isLoaded_m6A733EA533B0DC0041A9579A95530B41F869435C (void);
// 0x0000082F System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern void Scene_get_buildIndex_m764659943B7357F5D6C9165F68EDCFBBDDD6C3C2 (void);
// 0x00000830 System.Boolean UnityEngine.SceneManagement.Scene::op_Equality(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void Scene_op_Equality_m73EF8193688F46ECBBF5DF15436F05CB8A94D501 (void);
// 0x00000831 System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern void Scene_GetHashCode_m65BBB604A5496CF1F2C129860F45D0E437499E34 (void);
// 0x00000832 System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern void Scene_Equals_mD5738AF0B92757DED12A90F136716A5E2DDE3F54 (void);
// 0x00000833 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManagerAPIInternal::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,UnityEngine.SceneManagement.LoadSceneParameters,System.Boolean)
extern void SceneManagerAPIInternal_LoadSceneAsyncNameIndexInternal_m91FE2DD2BD955B3230E7686CB70DA238AEE0C485 (void);
// 0x00000834 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManagerAPIInternal::LoadSceneAsyncNameIndexInternal_Injected(System.String,System.Int32,UnityEngine.SceneManagement.LoadSceneParameters&,System.Boolean)
extern void SceneManagerAPIInternal_LoadSceneAsyncNameIndexInternal_Injected_m0F87909CFE5D0A027E72093D3A8E065CC4ED052C (void);
// 0x00000835 System.Int32 UnityEngine.SceneManagement.SceneManager::get_sceneCount()
extern void SceneManager_get_sceneCount_m21F0C1A9DB4F6105154E7FAEE9461805F3EFAD84 (void);
// 0x00000836 System.Int32 UnityEngine.SceneManagement.SceneManager::get_sceneCountInBuildSettings()
extern void SceneManager_get_sceneCountInBuildSettings_mDEF095420E9AC2FDA96136F51B55254CE11ED529 (void);
// 0x00000837 UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern void SceneManager_GetActiveScene_mD583193D329EBF540D8AB8A062681B1C2AB5EA51 (void);
// 0x00000838 UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetSceneByName(System.String)
extern void SceneManager_GetSceneByName_m99A07E2CCD62F07E67967BDCF16AA6BA97BC20A6 (void);
// 0x00000839 UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetSceneByBuildIndex(System.Int32)
extern void SceneManager_GetSceneByBuildIndex_mEC753530087666DAA2C248E143C1BE948251805B (void);
// 0x0000083A UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetSceneAt(System.Int32)
extern void SceneManager_GetSceneAt_m2D4105040A31A5A42E79A4E617028E84FC357C8A (void);
// 0x0000083B UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::UnloadSceneAsyncInternal(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.UnloadSceneOptions)
extern void SceneManager_UnloadSceneAsyncInternal_m0C5D270E775F3D7301737E5A941C9757695B4EEE (void);
// 0x0000083C UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,UnityEngine.SceneManagement.LoadSceneParameters,System.Boolean)
extern void SceneManager_LoadSceneAsyncNameIndexInternal_m542937B4FCCE8B1AAC326E1E1F9060ECEDCB6159 (void);
// 0x0000083D System.Void UnityEngine.SceneManagement.SceneManager::add_sceneLoaded(UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>)
extern void SceneManager_add_sceneLoaded_mB72463B21F0D89F168C58E994356298D0E38A4F7 (void);
// 0x0000083E System.Void UnityEngine.SceneManagement.SceneManager::remove_sceneLoaded(UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>)
extern void SceneManager_remove_sceneLoaded_m894CC4AE20DC49FF43CF6B2A614877F50D707C92 (void);
// 0x0000083F System.Void UnityEngine.SceneManagement.SceneManager::add_sceneUnloaded(UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>)
extern void SceneManager_add_sceneUnloaded_m14FAB322E1A8BC4981CFC9799D4CD68481377C8A (void);
// 0x00000840 System.Void UnityEngine.SceneManagement.SceneManager::remove_sceneUnloaded(UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>)
extern void SceneManager_remove_sceneUnloaded_mEB3226CE2741E6782E8055436FC51A6A74BBFBAF (void);
// 0x00000841 System.Void UnityEngine.SceneManagement.SceneManager::add_activeSceneChanged(UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>)
extern void SceneManager_add_activeSceneChanged_m0D135E92F11B46E5B46344EEB779402E833FFA86 (void);
// 0x00000842 System.Void UnityEngine.SceneManagement.SceneManager::remove_activeSceneChanged(UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>)
extern void SceneManager_remove_activeSceneChanged_mC680C450DB11B2ACEE12B87D9F41DEBF2C1F8159 (void);
// 0x00000843 System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern void SceneManager_LoadScene_m258051AAA1489D2D8B252815A45C1E9A2C097201 (void);
// 0x00000844 UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32,UnityEngine.SceneManagement.LoadSceneParameters)
extern void SceneManager_LoadScene_m8C95E38F5497F502DAEDC316F6E1C64B36EDC652 (void);
// 0x00000845 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern void SceneManager_LoadSceneAsync_mBB8889B3326DF7DD64983AE44FF5D140670C29B9 (void);
// 0x00000846 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String,UnityEngine.SceneManagement.LoadSceneParameters)
extern void SceneManager_LoadSceneAsync_m8B6EC55B2DE8C281A5D4B2CE88531D8573AF39EE (void);
// 0x00000847 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::UnloadSceneAsync(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.UnloadSceneOptions)
extern void SceneManager_UnloadSceneAsync_mE1691A3988A86BABB0A86F189285A33011722A4A (void);
// 0x00000848 System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void SceneManager_Internal_SceneLoaded_m800F5F7F7B30D5206913EF65548FD7F8DE9EF718 (void);
// 0x00000849 System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneUnloaded(UnityEngine.SceneManagement.Scene)
extern void SceneManager_Internal_SceneUnloaded_m32721E87A02DAC634DD4B9857092CC172EE8CB98 (void);
// 0x0000084A System.Void UnityEngine.SceneManagement.SceneManager::Internal_ActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void SceneManager_Internal_ActiveSceneChanged_mE9AB93D6979594CFCED5B3696F727B7D5E6B25F5 (void);
// 0x0000084B System.Void UnityEngine.SceneManagement.SceneManager::.cctor()
extern void SceneManager__cctor_m36C7C4EECB3F22A383FA829341F6A4EA0B2D3377 (void);
// 0x0000084C System.Void UnityEngine.SceneManagement.SceneManager::GetActiveScene_Injected(UnityEngine.SceneManagement.Scene&)
extern void SceneManager_GetActiveScene_Injected_m1A7248F1FDCB9E3BFC13E4AA4849AA3FB5B911C4 (void);
// 0x0000084D System.Void UnityEngine.SceneManagement.SceneManager::GetSceneByName_Injected(System.String,UnityEngine.SceneManagement.Scene&)
extern void SceneManager_GetSceneByName_Injected_m02D1100582A883A40F82E8E620D8DF04D3E89DA8 (void);
// 0x0000084E System.Void UnityEngine.SceneManagement.SceneManager::GetSceneByBuildIndex_Injected(System.Int32,UnityEngine.SceneManagement.Scene&)
extern void SceneManager_GetSceneByBuildIndex_Injected_m5693DD7A5A8CF9FBFC01CC49C61F98F14622BD9F (void);
// 0x0000084F System.Void UnityEngine.SceneManagement.SceneManager::GetSceneAt_Injected(System.Int32,UnityEngine.SceneManagement.Scene&)
extern void SceneManager_GetSceneAt_Injected_m7DB39BC8E659D73DEE24D5F7F4D382CBB0B31148 (void);
// 0x00000850 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::UnloadSceneAsyncInternal_Injected(UnityEngine.SceneManagement.Scene&,UnityEngine.SceneManagement.UnloadSceneOptions)
extern void SceneManager_UnloadSceneAsyncInternal_Injected_mCB0FB993506CFFD796C3D0F4954023167A188BC0 (void);
// 0x00000851 System.Void UnityEngine.SceneManagement.LoadSceneParameters::set_loadSceneMode(UnityEngine.SceneManagement.LoadSceneMode)
extern void LoadSceneParameters_set_loadSceneMode_mD56FFF214E3909A68BD9C485BBB4D99ADC3A5624 (void);
// 0x00000852 System.Void UnityEngine.SceneManagement.LoadSceneParameters::.ctor(UnityEngine.SceneManagement.LoadSceneMode)
extern void LoadSceneParameters__ctor_m2E00BEC64DCC48351831B7BE9088D9B6AF62102A (void);
// 0x00000853 System.Void UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction::.ctor(System.Object,System.IntPtr)
extern void UpdateFunction__ctor_m203C45C9226ED025C1369B78B759C952ABDA630A (void);
// 0x00000854 System.Void UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction::Invoke()
extern void UpdateFunction_Invoke_m6C0E9E5082FCEEF018602FD40A43E613360D410D (void);
// 0x00000855 System.IAsyncResult UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction::BeginInvoke(System.AsyncCallback,System.Object)
extern void UpdateFunction_BeginInvoke_m7261B0AA3E858CC7A24FF343DE292DB5A34DAC0C (void);
// 0x00000856 System.Void UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction::EndInvoke(System.IAsyncResult)
extern void UpdateFunction_EndInvoke_m49FBADEA0EED0F50341E2E1F5F21349C2059EA55 (void);
// 0x00000857 System.Void UnityEngine.Networking.PlayerConnection.MessageEventArgs::.ctor()
extern void MessageEventArgs__ctor_m436B854CFEEDB949F4D9ACAEA2E512BDAEDC6E1B (void);
// 0x00000858 UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::get_instance()
extern void PlayerConnection_get_instance_mF51FB76C702C7CDD0BAEAD466060E3BDF23D390F (void);
// 0x00000859 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::get_isConnected()
extern void PlayerConnection_get_isConnected_mB902603E2C8CA93299FF0B28E13A9D594CBFE14E (void);
// 0x0000085A UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::CreateInstance()
extern void PlayerConnection_CreateInstance_m6325767D9D05B530116767E164CDCBE613A5787F (void);
// 0x0000085B System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::OnEnable()
extern void PlayerConnection_OnEnable_m9D8136CEB952BC0F44A46A212BF2E91E5A769954 (void);
// 0x0000085C UnityEngine.IPlayerEditorConnectionNative UnityEngine.Networking.PlayerConnection.PlayerConnection::GetConnectionNativeApi()
extern void PlayerConnection_GetConnectionNativeApi_mC88D9972FDF9D4FF15CC8C4BB6CA633FB114D918 (void);
// 0x0000085D System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Register(System.Guid,UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>)
extern void PlayerConnection_Register_m9B203230984995ADF5E19E50C3D7DF7E21036FF2 (void);
// 0x0000085E System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Unregister(System.Guid,UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>)
extern void PlayerConnection_Unregister_m8EB88437DF970AA6627BC301C54A859DAED70534 (void);
// 0x0000085F System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::RegisterConnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void PlayerConnection_RegisterConnection_m7E54302209A4F3FB3E27A0E7FEB8ADE32C100F1B (void);
// 0x00000860 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::RegisterDisconnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void PlayerConnection_RegisterDisconnection_m556075060F55D3FA7F44DEB4B34CE1070ECBF823 (void);
// 0x00000861 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::UnregisterConnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void PlayerConnection_UnregisterConnection_mC6A080D398CD6C267C1638D98F7485E2B837D029 (void);
// 0x00000862 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::UnregisterDisconnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void PlayerConnection_UnregisterDisconnection_m148453805654D809DB02F377D0FBC4524F63EBF6 (void);
// 0x00000863 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Send(System.Guid,System.Byte[])
extern void PlayerConnection_Send_m1CDF41319A60A5940B487D08ECE14D0B61EDE6AC (void);
// 0x00000864 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::TrySend(System.Guid,System.Byte[])
extern void PlayerConnection_TrySend_m3C0D0208A6A8A7F7FF93AF155A71B726ABE8D662 (void);
// 0x00000865 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::BlockUntilRecvMsg(System.Guid,System.Int32)
extern void PlayerConnection_BlockUntilRecvMsg_mFCF2DB02D6F07C0A69C0412D8A3F596AF4AC54A2 (void);
// 0x00000866 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::DisconnectAll()
extern void PlayerConnection_DisconnectAll_m278A4B90D90892338D1B41F5A59CD7C519F1C8D2 (void);
// 0x00000867 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::MessageCallbackInternal(System.IntPtr,System.UInt64,System.UInt64,System.String)
extern void PlayerConnection_MessageCallbackInternal_m3E9A847ED82FDA9ABB680F81595A876450EFB166 (void);
// 0x00000868 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::ConnectedCallbackInternal(System.Int32)
extern void PlayerConnection_ConnectedCallbackInternal_mFEC88D604DE3923849942994ED873B26CEEDDA3D (void);
// 0x00000869 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::DisconnectedCallback(System.Int32)
extern void PlayerConnection_DisconnectedCallback_m2A12A748DDACDD3877D01D7F38ABBC55DEE26A56 (void);
// 0x0000086A System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::.ctor()
extern void PlayerConnection__ctor_m3E1248C28C3082C592C2E5F69778F31F6610D93D (void);
// 0x0000086B System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m2685C903220EF0EFCFABCCCCE85520064EEB9BCE (void);
// 0x0000086C System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection/<>c__DisplayClass12_0::<Register>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass12_0_U3CRegisterU3Eb__0_m1979ADC0BD33A692CDFDD59354B756C347611773 (void);
// 0x0000086D System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m75A34D41161C0967E4A336B2713ACAE2BD5F5F46 (void);
// 0x0000086E System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection/<>c__DisplayClass13_0::<Unregister>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass13_0_U3CUnregisterU3Eb__0_m8C6607EC9FE0F26B57047ED3642837003A532C79 (void);
// 0x0000086F System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m3E72979DC019A7C47E2AB71E1F17B9056A7D068B (void);
// 0x00000870 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection/<>c__DisplayClass20_0::<BlockUntilRecvMsg>b__0(UnityEngine.Networking.PlayerConnection.MessageEventArgs)
extern void U3CU3Ec__DisplayClass20_0_U3CBlockUntilRecvMsgU3Eb__0_m89ABCD175D2DA1D535B2645459C38003ECBC896C (void);
// 0x00000871 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::InvokeMessageIdSubscribers(System.Guid,System.Byte[],System.Int32)
extern void PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_mFA28BDF3B52AEF86161F33B52699253181800926 (void);
// 0x00000872 UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs> UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::AddAndCreate(System.Guid)
extern void PlayerEditorConnectionEvents_AddAndCreate_mB5A51595E4A5DA3B9F353AC72F7B0484C675B7D3 (void);
// 0x00000873 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::UnregisterManagedCallback(System.Guid,UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>)
extern void PlayerEditorConnectionEvents_UnregisterManagedCallback_mFD3A444E636B079C03739DC96BAFFD5FD55C574A (void);
// 0x00000874 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::.ctor()
extern void PlayerEditorConnectionEvents__ctor_m9BE616B901BCACAABEC9063A838BB803AB7EC2A7 (void);
// 0x00000875 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent::.ctor()
extern void MessageEvent__ctor_m700B679037ED52893F092843EE603DBCD6EB8386 (void);
// 0x00000876 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent::.ctor()
extern void ConnectionChangeEvent__ctor_m3F04C39FD710BF0F25416A61F479CBA1B9021F18 (void);
// 0x00000877 System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::get_MessageTypeId()
extern void MessageTypeSubscribers_get_MessageTypeId_mE7DD7E800436C92A325A1080AF60663AE1100D25 (void);
// 0x00000878 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::set_MessageTypeId(System.Guid)
extern void MessageTypeSubscribers_set_MessageTypeId_m294A7B621AAF1984D886D2569CF1206E4F469115 (void);
// 0x00000879 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::.ctor()
extern void MessageTypeSubscribers__ctor_mD26A2485EA3ECACFA2CB35D08A48256CE9DFE825 (void);
// 0x0000087A System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_mC65CF56D3417BA36ED321886F1E7A1AF8D443966 (void);
// 0x0000087B System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<>c__DisplayClass6_0::<InvokeMessageIdSubscribers>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass6_0_U3CInvokeMessageIdSubscribersU3Eb__0_m7208417727D24E769D2C1CF90D6E4CC1AE1F2556 (void);
// 0x0000087C System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m59072FF40A09DA582550D3DED10DA2A93FBEAFEF (void);
// 0x0000087D System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<>c__DisplayClass7_0::<AddAndCreate>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass7_0_U3CAddAndCreateU3Eb__0_m24A60437D2E527CE675AC4AFAC8152BCA69B033B (void);
// 0x0000087E System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mE25781AE393CFFE6170E4D655A751918973393AB (void);
// 0x0000087F System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<>c__DisplayClass8_0::<UnregisterManagedCallback>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass8_0_U3CUnregisterManagedCallbackU3Eb__0_m11D9A0AFB947855B548E99416A058C4AAA2E2B26 (void);
// 0x00000880 System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern void DefaultValueAttribute__ctor_m2E914CFCFD82ACAB447480971570E5763C42DAAD (void);
// 0x00000881 System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern void DefaultValueAttribute_get_Value_m1E1505D5F1838C28CA4C52DED4A20E81F81BFCC3 (void);
// 0x00000882 System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern void DefaultValueAttribute_Equals_mD9073A5C537D4DBDFBD5E3616BC5A05B5D0C51B2 (void);
// 0x00000883 System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern void DefaultValueAttribute_GetHashCode_m4782E2C5A005991FA7E705110A690DD5E0E52D50 (void);
// 0x00000884 System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern void ExcludeFromDocsAttribute__ctor_m01F11706D334D5D31B0C59630DB1674ECFBFAF04 (void);
// 0x00000885 System.Boolean UnityEngine.Rendering.GraphicsSettings::get_lightsUseLinearIntensity()
extern void GraphicsSettings_get_lightsUseLinearIntensity_mED8D75F87016FCF600955146863696AB214BA29A (void);
// 0x00000886 System.Boolean UnityEngine.Rendering.GraphicsSettings::AllowEnlightenSupportForUpgradedProject()
extern void GraphicsSettings_AllowEnlightenSupportForUpgradedProject_m00E568FAA4C9D08BEE0944042CF817BC242F2BEF (void);
// 0x00000887 UnityEngine.Rendering.RenderPipelineAsset UnityEngine.Rendering.GraphicsSettings::get_renderPipelineAsset()
extern void GraphicsSettings_get_renderPipelineAsset_mDDAD0B2B6E87A299B5F7B19F8BDE3C1EF6287B31 (void);
// 0x00000888 UnityEngine.ScriptableObject UnityEngine.Rendering.GraphicsSettings::get_INTERNAL_defaultRenderPipeline()
extern void GraphicsSettings_get_INTERNAL_defaultRenderPipeline_m4150C45BC8C1D23C75F4EFA299345E894C7E5CB6 (void);
// 0x00000889 UnityEngine.Rendering.RenderPipelineAsset UnityEngine.Rendering.GraphicsSettings::get_defaultRenderPipeline()
extern void GraphicsSettings_get_defaultRenderPipeline_m2B485056B44981C55A5B6F0785445F9094078B18 (void);
// 0x0000088A System.Int32 UnityEngine.Rendering.OnDemandRendering::get_renderFrameInterval()
extern void OnDemandRendering_get_renderFrameInterval_m5A12FB459D93296FBACCD6FD59EA27CD092A0132 (void);
// 0x0000088B System.Void UnityEngine.Rendering.OnDemandRendering::GetRenderFrameInterval(System.Int32&)
extern void OnDemandRendering_GetRenderFrameInterval_m2A6D3ADB578BA19E153C6B049FC668975155EB27 (void);
// 0x0000088C System.Void UnityEngine.Rendering.OnDemandRendering::.cctor()
extern void OnDemandRendering__cctor_m00887F122A90C963B1B120117276E3BE3A0D258F (void);
// 0x0000088D System.Boolean UnityEngine.Rendering.LODParameters::Equals(UnityEngine.Rendering.LODParameters)
extern void LODParameters_Equals_mA7C4CFD75190B341999074C56E1BAD9E5136CF61 (void);
// 0x0000088E System.Boolean UnityEngine.Rendering.LODParameters::Equals(System.Object)
extern void LODParameters_Equals_m03754D13F85184E596AFBBCF6DA1EDB06CA58F6B (void);
// 0x0000088F System.Int32 UnityEngine.Rendering.LODParameters::GetHashCode()
extern void LODParameters_GetHashCode_m532D8960CCF3340F7CDF6B757D33BAE13B152716 (void);
// 0x00000890 System.Void UnityEngine.Rendering.RenderPipeline::Render(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera[])
// 0x00000891 System.Void UnityEngine.Rendering.RenderPipeline::InternalRender(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera[])
extern void RenderPipeline_InternalRender_m3601304F718BEEDCC63FAC61AF865392A1B97159 (void);
// 0x00000892 System.Boolean UnityEngine.Rendering.RenderPipeline::get_disposed()
extern void RenderPipeline_get_disposed_mA11FE959A1C7309A86F26893DA04D00DD5D61149 (void);
// 0x00000893 System.Void UnityEngine.Rendering.RenderPipeline::set_disposed(System.Boolean)
extern void RenderPipeline_set_disposed_m6319C7F5991E861B961370FF374CF87E9F0DA691 (void);
// 0x00000894 System.Void UnityEngine.Rendering.RenderPipeline::Dispose()
extern void RenderPipeline_Dispose_mFFDBE5963DA828BA99417035F8F6228535E0EAAB (void);
// 0x00000895 System.Void UnityEngine.Rendering.RenderPipeline::Dispose(System.Boolean)
extern void RenderPipeline_Dispose_m256C636C8C66B12ED0E67F4C6F6470A79CBAA49B (void);
// 0x00000896 UnityEngine.Rendering.RenderPipeline UnityEngine.Rendering.RenderPipelineAsset::InternalCreatePipeline()
extern void RenderPipelineAsset_InternalCreatePipeline_m7FC3209A9640269E6E01FCBCC879E3FC36B23264 (void);
// 0x00000897 System.String[] UnityEngine.Rendering.RenderPipelineAsset::get_renderingLayerMaskNames()
extern void RenderPipelineAsset_get_renderingLayerMaskNames_mD9D46ECB8CB3AA207307DF715AB738EBE9774D4C (void);
// 0x00000898 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultMaterial()
extern void RenderPipelineAsset_get_defaultMaterial_mB049EB56C99330E8392028148336A9329A1F6BEF (void);
// 0x00000899 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_autodeskInteractiveShader()
extern void RenderPipelineAsset_get_autodeskInteractiveShader_mACEA652B47468186F0B74AAE63A5625A70E500A4 (void);
// 0x0000089A UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_autodeskInteractiveTransparentShader()
extern void RenderPipelineAsset_get_autodeskInteractiveTransparentShader_mBC027215A56E8FD6C3B3CC5008B69540BBEBD1FD (void);
// 0x0000089B UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_autodeskInteractiveMaskedShader()
extern void RenderPipelineAsset_get_autodeskInteractiveMaskedShader_m5342E858293BCAB426D1A6E84242CD7D26EE5BC7 (void);
// 0x0000089C UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_terrainDetailLitShader()
extern void RenderPipelineAsset_get_terrainDetailLitShader_mE16DBCD806DD77447A89F3C1109E35CD24FEA7CF (void);
// 0x0000089D UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_terrainDetailGrassShader()
extern void RenderPipelineAsset_get_terrainDetailGrassShader_mAF8F368B3B67F7E6C71A70A93696122E73ED6C47 (void);
// 0x0000089E UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_terrainDetailGrassBillboardShader()
extern void RenderPipelineAsset_get_terrainDetailGrassBillboardShader_m2DAE45A6FF177CB2996EC95EDF1AC05F59A7470B (void);
// 0x0000089F UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultParticleMaterial()
extern void RenderPipelineAsset_get_defaultParticleMaterial_m131DA69D30E5A400B227B98B8877C94A118E9F49 (void);
// 0x000008A0 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultLineMaterial()
extern void RenderPipelineAsset_get_defaultLineMaterial_m9FF591F569273D367C4020C1AC3A30AA161CE6BC (void);
// 0x000008A1 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultTerrainMaterial()
extern void RenderPipelineAsset_get_defaultTerrainMaterial_mA6D10A07A29A577D83AF711CDE2E70B28D51A91A (void);
// 0x000008A2 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultUIMaterial()
extern void RenderPipelineAsset_get_defaultUIMaterial_m39BD27EC73AE39354158451661A0112FC4E4F7F6 (void);
// 0x000008A3 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultUIOverdrawMaterial()
extern void RenderPipelineAsset_get_defaultUIOverdrawMaterial_m0FE02710211BE1CF77728A1086BDC5EDCE106D35 (void);
// 0x000008A4 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultUIETC1SupportedMaterial()
extern void RenderPipelineAsset_get_defaultUIETC1SupportedMaterial_mB8E2388C7F45E6D1B4EC728EC2335B527A741470 (void);
// 0x000008A5 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_default2DMaterial()
extern void RenderPipelineAsset_get_default2DMaterial_mF68C0199E16004C348D11AD0BA4C9482CCFC9762 (void);
// 0x000008A6 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_defaultShader()
extern void RenderPipelineAsset_get_defaultShader_m8F4DD3FBD2EA1A55DE3EB45EF41C3FD502141A40 (void);
// 0x000008A7 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_defaultSpeedTree7Shader()
extern void RenderPipelineAsset_get_defaultSpeedTree7Shader_mE81955ABC171A01E496BD2E6166CC0BCDCC12E39 (void);
// 0x000008A8 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_defaultSpeedTree8Shader()
extern void RenderPipelineAsset_get_defaultSpeedTree8Shader_mF5025B6116D23E28B7A872C3694ADED33A6DE9B0 (void);
// 0x000008A9 UnityEngine.Rendering.RenderPipeline UnityEngine.Rendering.RenderPipelineAsset::CreatePipeline()
// 0x000008AA System.Void UnityEngine.Rendering.RenderPipelineAsset::OnValidate()
extern void RenderPipelineAsset_OnValidate_m232856172D71149CC85645BE94902D01628A1A8E (void);
// 0x000008AB System.Void UnityEngine.Rendering.RenderPipelineAsset::OnDisable()
extern void RenderPipelineAsset_OnDisable_m6DB5D6EE17E7CAEC1254135C61F4EB3E249FDD52 (void);
// 0x000008AC System.Void UnityEngine.Rendering.RenderPipelineAsset::.ctor()
extern void RenderPipelineAsset__ctor_mCB26E546B9DC1F2518E0F0F4A7E6CFF519D852F2 (void);
// 0x000008AD UnityEngine.Rendering.RenderPipeline UnityEngine.Rendering.RenderPipelineManager::get_currentPipeline()
extern void RenderPipelineManager_get_currentPipeline_m07358604B9829E6C1EEDE94064729109D9259852 (void);
// 0x000008AE System.Void UnityEngine.Rendering.RenderPipelineManager::set_currentPipeline(UnityEngine.Rendering.RenderPipeline)
extern void RenderPipelineManager_set_currentPipeline_mDCF377780787BD6CAB3BC9F4A04B01B478293B88 (void);
// 0x000008AF System.Void UnityEngine.Rendering.RenderPipelineManager::add_beginFrameRendering(System.Action`2<UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera[]>)
extern void RenderPipelineManager_add_beginFrameRendering_mF9896552D7B492FBC22B0D1832C536C91E542B56 (void);
// 0x000008B0 System.Void UnityEngine.Rendering.RenderPipelineManager::remove_beginFrameRendering(System.Action`2<UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera[]>)
extern void RenderPipelineManager_remove_beginFrameRendering_mC2D3765FA14C53A4C8DB538D0689C6F77EAE8501 (void);
// 0x000008B1 System.Void UnityEngine.Rendering.RenderPipelineManager::CleanupRenderPipeline()
extern void RenderPipelineManager_CleanupRenderPipeline_m82AC5DAD81AE3B10147198B7C7CFAAD8AE528010 (void);
// 0x000008B2 System.Void UnityEngine.Rendering.RenderPipelineManager::GetCameras(UnityEngine.Rendering.ScriptableRenderContext)
extern void RenderPipelineManager_GetCameras_m53602210E7576F801085A741B2068BEA0995433F (void);
// 0x000008B3 System.Void UnityEngine.Rendering.RenderPipelineManager::DoRenderLoop_Internal(UnityEngine.Rendering.RenderPipelineAsset,System.IntPtr)
extern void RenderPipelineManager_DoRenderLoop_Internal_mF16D72874EE44C297C2D0623933207B448BFCD32 (void);
// 0x000008B4 System.Void UnityEngine.Rendering.RenderPipelineManager::PrepareRenderPipeline(UnityEngine.Rendering.RenderPipelineAsset)
extern void RenderPipelineManager_PrepareRenderPipeline_m4A4D5CD3B9803F6D92DBD7D245A3D925F9028CC7 (void);
// 0x000008B5 System.Void UnityEngine.Rendering.RenderPipelineManager::.cctor()
extern void RenderPipelineManager__cctor_m7B3FA781696A3B82639DA7705E02A4E0BD418421 (void);
// 0x000008B6 System.Int32 UnityEngine.Rendering.ScriptableRenderContext::GetNumberOfCameras_Internal()
extern void ScriptableRenderContext_GetNumberOfCameras_Internal_m58DE22F11E08F3B9C7E4B1D9788711101EBD2395 (void);
// 0x000008B7 UnityEngine.Camera UnityEngine.Rendering.ScriptableRenderContext::GetCamera_Internal(System.Int32)
extern void ScriptableRenderContext_GetCamera_Internal_m1E56C9D9782F0E99A7ED3C64C15F3A06A8A0B917 (void);
// 0x000008B8 System.Void UnityEngine.Rendering.ScriptableRenderContext::.ctor(System.IntPtr)
extern void ScriptableRenderContext__ctor_m554E9C4BB7F69601E65F71557914C6958D3181EE (void);
// 0x000008B9 System.Int32 UnityEngine.Rendering.ScriptableRenderContext::GetNumberOfCameras()
extern void ScriptableRenderContext_GetNumberOfCameras_mF14CD21DA4E8A43DCE5811735E48748313F71CBA (void);
// 0x000008BA UnityEngine.Camera UnityEngine.Rendering.ScriptableRenderContext::GetCamera(System.Int32)
extern void ScriptableRenderContext_GetCamera_mCC4F389E3EB259D9FF01E7F7801D39137BC0E41C (void);
// 0x000008BB System.Boolean UnityEngine.Rendering.ScriptableRenderContext::Equals(UnityEngine.Rendering.ScriptableRenderContext)
extern void ScriptableRenderContext_Equals_m0612225D9DC8BE5574C67738B9B185A05B306803 (void);
// 0x000008BC System.Boolean UnityEngine.Rendering.ScriptableRenderContext::Equals(System.Object)
extern void ScriptableRenderContext_Equals_m239EBA23E760DDF7CC7112D3299D9B4CEA449683 (void);
// 0x000008BD System.Int32 UnityEngine.Rendering.ScriptableRenderContext::GetHashCode()
extern void ScriptableRenderContext_GetHashCode_m2A894A66C98DF28B51758A3579EE04B87D2AC6D6 (void);
// 0x000008BE System.Int32 UnityEngine.Rendering.ScriptableRenderContext::GetNumberOfCameras_Internal_Injected(UnityEngine.Rendering.ScriptableRenderContext&)
extern void ScriptableRenderContext_GetNumberOfCameras_Internal_Injected_m70F9C34BAA4E5548D1B5848EFFBA6ACDC1B9781B (void);
// 0x000008BF UnityEngine.Camera UnityEngine.Rendering.ScriptableRenderContext::GetCamera_Internal_Injected(UnityEngine.Rendering.ScriptableRenderContext&,System.Int32)
extern void ScriptableRenderContext_GetCamera_Internal_Injected_m43E535C1F7FC90BF7254A0D043E624D45B70E459 (void);
// 0x000008C0 System.Int32 UnityEngine.Rendering.ShaderTagId::get_id()
extern void ShaderTagId_get_id_mB1AB67FC09D015D245DAB1F6C491F2D59558C5D5 (void);
// 0x000008C1 System.Void UnityEngine.Rendering.ShaderTagId::set_id(System.Int32)
extern void ShaderTagId_set_id_m47A84B8D0203D1BAE247CF591D1E2B43A1FFEF1E (void);
// 0x000008C2 System.Boolean UnityEngine.Rendering.ShaderTagId::Equals(System.Object)
extern void ShaderTagId_Equals_m7C6B4B00D502970BEDA5032CC9F791795B8C44EE (void);
// 0x000008C3 System.Boolean UnityEngine.Rendering.ShaderTagId::Equals(UnityEngine.Rendering.ShaderTagId)
extern void ShaderTagId_Equals_mC8A45F721611717B4CB12DBD73E23200C67AA7AF (void);
// 0x000008C4 System.Int32 UnityEngine.Rendering.ShaderTagId::GetHashCode()
extern void ShaderTagId_GetHashCode_m51C1D9E4417AD43F0A5E9CCF7FF88C10BCDB2905 (void);
// 0x000008C5 System.Void UnityEngine.Rendering.ShaderTagId::.cctor()
extern void ShaderTagId__cctor_mC73D6F5DE8D099CE53149E383ADF6B64F3EA763D (void);
// 0x000008C6 UnityEngine.Rendering.SupportedRenderingFeatures UnityEngine.Rendering.SupportedRenderingFeatures::get_active()
extern void SupportedRenderingFeatures_get_active_m2C2E65CE6B3B9197E71D6390B4CE3AF024EFC286 (void);
// 0x000008C7 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::set_active(UnityEngine.Rendering.SupportedRenderingFeatures)
extern void SupportedRenderingFeatures_set_active_m2E459FC4898691C6E729A5EC2D2B08A1733CDCC2 (void);
// 0x000008C8 UnityEngine.Rendering.SupportedRenderingFeatures/LightmapMixedBakeModes UnityEngine.Rendering.SupportedRenderingFeatures::get_defaultMixedLightingModes()
extern void SupportedRenderingFeatures_get_defaultMixedLightingModes_m903DC0B0AB86F4C047E8930E0C59C4131DA600C9 (void);
// 0x000008C9 UnityEngine.Rendering.SupportedRenderingFeatures/LightmapMixedBakeModes UnityEngine.Rendering.SupportedRenderingFeatures::get_mixedLightingModes()
extern void SupportedRenderingFeatures_get_mixedLightingModes_m87742B8CBD950598883F391C22FF036BE774E2D9 (void);
// 0x000008CA UnityEngine.LightmapBakeType UnityEngine.Rendering.SupportedRenderingFeatures::get_lightmapBakeTypes()
extern void SupportedRenderingFeatures_get_lightmapBakeTypes_m6B99531AE2EDEB49D54886E103AF12CFB1BC426A (void);
// 0x000008CB UnityEngine.LightmapsMode UnityEngine.Rendering.SupportedRenderingFeatures::get_lightmapsModes()
extern void SupportedRenderingFeatures_get_lightmapsModes_mAAAC00FB06849B233D053DB11B47E8DA8666583B (void);
// 0x000008CC System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::get_enlighten()
extern void SupportedRenderingFeatures_get_enlighten_m493ED393C99DC9105CCC2D8D28D43D6AB0B96C78 (void);
// 0x000008CD System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::get_autoAmbientProbeBaking()
extern void SupportedRenderingFeatures_get_autoAmbientProbeBaking_m8F06C9C90FA3604D222D7CB0C6A5449B9A6D1E38 (void);
// 0x000008CE System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::get_autoDefaultReflectionProbeBaking()
extern void SupportedRenderingFeatures_get_autoDefaultReflectionProbeBaking_m03E0F42FE5C8DAF5D9A06CD74A2C61219F82E0CE (void);
// 0x000008CF System.Void UnityEngine.Rendering.SupportedRenderingFeatures::FallbackMixedLightingModeByRef(System.IntPtr)
extern void SupportedRenderingFeatures_FallbackMixedLightingModeByRef_m91AC959EB7DD16F061466CF2123820AFA257BD76 (void);
// 0x000008D0 System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::IsMixedLightingModeSupported(UnityEngine.MixedLightingMode)
extern void SupportedRenderingFeatures_IsMixedLightingModeSupported_m22C3916FDD1308FCDD201651709ED3861DBC09AB (void);
// 0x000008D1 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsMixedLightingModeSupportedByRef(UnityEngine.MixedLightingMode,System.IntPtr)
extern void SupportedRenderingFeatures_IsMixedLightingModeSupportedByRef_mE77416221A79F75F9EF7FD062AF9674264D1E069 (void);
// 0x000008D2 System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::IsLightmapBakeTypeSupported(UnityEngine.LightmapBakeType)
extern void SupportedRenderingFeatures_IsLightmapBakeTypeSupported_mD21AC518EFAA0892131E4ADE0EBA1DA980937A61 (void);
// 0x000008D3 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsLightmapBakeTypeSupportedByRef(UnityEngine.LightmapBakeType,System.IntPtr)
extern void SupportedRenderingFeatures_IsLightmapBakeTypeSupportedByRef_mE33A63BE6E3D6D4DF4B5584E4F451713DC776DB8 (void);
// 0x000008D4 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsLightmapsModeSupportedByRef(UnityEngine.LightmapsMode,System.IntPtr)
extern void SupportedRenderingFeatures_IsLightmapsModeSupportedByRef_mAFD9EC8C77CF9874911FFC0C084FC161C27E9A13 (void);
// 0x000008D5 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsLightmapperSupportedByRef(System.Int32,System.IntPtr)
extern void SupportedRenderingFeatures_IsLightmapperSupportedByRef_m96E352A96F40887994AE8BA5F07BBB87D8A3E557 (void);
// 0x000008D6 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsAutoAmbientProbeBakingSupported(System.IntPtr)
extern void SupportedRenderingFeatures_IsAutoAmbientProbeBakingSupported_m6F31B8E18EECDC6798BA87F75A72675EFC5C7304 (void);
// 0x000008D7 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsAutoDefaultReflectionProbeBakingSupported(System.IntPtr)
extern void SupportedRenderingFeatures_IsAutoDefaultReflectionProbeBakingSupported_mC6710197013A1E8ED2A89C78C980E056A242E5B6 (void);
// 0x000008D8 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::FallbackLightmapperByRef(System.IntPtr)
extern void SupportedRenderingFeatures_FallbackLightmapperByRef_m04F1D58EF96BB1E1E20C72A562E86F17EEF3B1B9 (void);
// 0x000008D9 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::.ctor()
extern void SupportedRenderingFeatures__ctor_mFA6FBB0F889B8A9ECD5B77C0CE2EEF685C0310F5 (void);
// 0x000008DA System.Void UnityEngine.Rendering.SupportedRenderingFeatures::.cctor()
extern void SupportedRenderingFeatures__cctor_m0AAC399710A8663753C069DDC8CAE61039630C40 (void);
// 0x000008DB System.Void UnityEngine.Rendering.BatchCullingContext::.ctor(Unity.Collections.NativeArray`1<UnityEngine.Plane>,Unity.Collections.NativeArray`1<UnityEngine.Rendering.BatchVisibility>,Unity.Collections.NativeArray`1<System.Int32>,UnityEngine.Rendering.LODParameters)
extern void BatchCullingContext__ctor_m5BB85EDAC0F7C5AFFABF7AD30F18AD272EA23E32 (void);
// 0x000008DC System.Void UnityEngine.Rendering.BatchRendererGroup::InvokeOnPerformCulling(UnityEngine.Rendering.BatchRendererGroup,UnityEngine.Rendering.BatchRendererCullingOutput&,UnityEngine.Rendering.LODParameters&)
extern void BatchRendererGroup_InvokeOnPerformCulling_mE7F3139F1032B8B6160F5601BB55410609D0EF6E (void);
// 0x000008DD System.Void UnityEngine.Rendering.BatchRendererGroup/OnPerformCulling::.ctor(System.Object,System.IntPtr)
extern void OnPerformCulling__ctor_mC901B41F5C6F0A0A144862B9014047CAE3702E2F (void);
// 0x000008DE Unity.Jobs.JobHandle UnityEngine.Rendering.BatchRendererGroup/OnPerformCulling::Invoke(UnityEngine.Rendering.BatchRendererGroup,UnityEngine.Rendering.BatchCullingContext)
extern void OnPerformCulling_Invoke_m91277025DBB74E928F6C98E7A6745B07F5B2FD59 (void);
// 0x000008DF System.IAsyncResult UnityEngine.Rendering.BatchRendererGroup/OnPerformCulling::BeginInvoke(UnityEngine.Rendering.BatchRendererGroup,UnityEngine.Rendering.BatchCullingContext,System.AsyncCallback,System.Object)
extern void OnPerformCulling_BeginInvoke_m8179E1CC4FD6B2858D0333578FCB43594551DAAD (void);
// 0x000008E0 Unity.Jobs.JobHandle UnityEngine.Rendering.BatchRendererGroup/OnPerformCulling::EndInvoke(System.IAsyncResult)
extern void OnPerformCulling_EndInvoke_m3EB7184DC9175B0F722CE41E1CE6507887CA55DB (void);
// 0x000008E1 System.Void UnityEngine.Playables.INotificationReceiver::OnNotify(UnityEngine.Playables.Playable,UnityEngine.Playables.INotification,System.Object)
// 0x000008E2 System.Void UnityEngine.Playables.IPlayableBehaviour::OnGraphStart(UnityEngine.Playables.Playable)
// 0x000008E3 System.Void UnityEngine.Playables.IPlayableBehaviour::OnGraphStop(UnityEngine.Playables.Playable)
// 0x000008E4 System.Void UnityEngine.Playables.IPlayableBehaviour::OnPlayableCreate(UnityEngine.Playables.Playable)
// 0x000008E5 System.Void UnityEngine.Playables.IPlayableBehaviour::OnPlayableDestroy(UnityEngine.Playables.Playable)
// 0x000008E6 System.Void UnityEngine.Playables.IPlayableBehaviour::OnBehaviourPlay(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
// 0x000008E7 System.Void UnityEngine.Playables.IPlayableBehaviour::OnBehaviourPause(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
// 0x000008E8 System.Void UnityEngine.Playables.IPlayableBehaviour::PrepareFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
// 0x000008E9 System.Void UnityEngine.Playables.IPlayableBehaviour::ProcessFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData,System.Object)
// 0x000008EA UnityEngine.Playables.Playable UnityEngine.Playables.Playable::get_Null()
extern void Playable_get_Null_m1641F4B851ACAA6CBCC9BB400EC783EDEAF1A48D (void);
// 0x000008EB System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern void Playable__ctor_m24C6ED455A921F585698BFFEC5CCED397205543E (void);
// 0x000008EC UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern void Playable_GetHandle_m952F17BACFC90BEACD3CB9880E65E69B3271108A (void);
// 0x000008ED System.Boolean UnityEngine.Playables.Playable::Equals(UnityEngine.Playables.Playable)
extern void Playable_Equals_m1EC7E8B579C862BA8C979BD616224EC1B3364DD1 (void);
// 0x000008EE System.Void UnityEngine.Playables.Playable::.cctor()
extern void Playable__cctor_m5655D443F6D04230DB5D37BF7D5EDCA71FD85A32 (void);
// 0x000008EF UnityEngine.Playables.Playable UnityEngine.Playables.PlayableAsset::CreatePlayable(UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject)
// 0x000008F0 System.Double UnityEngine.Playables.PlayableAsset::get_duration()
extern void PlayableAsset_get_duration_m58C1A4AC3A8CF2783815016BE58378D6E17D22D2 (void);
// 0x000008F1 System.Collections.Generic.IEnumerable`1<UnityEngine.Playables.PlayableBinding> UnityEngine.Playables.PlayableAsset::get_outputs()
extern void PlayableAsset_get_outputs_mD839CEB7A22543AC17FAE1C3C4BCD9A7B8DA82B1 (void);
// 0x000008F2 System.Void UnityEngine.Playables.PlayableAsset::Internal_CreatePlayable(UnityEngine.Playables.PlayableAsset,UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject,System.IntPtr)
extern void PlayableAsset_Internal_CreatePlayable_mB36624F1FD210AAD53A848BF8F26912D47DFC09C (void);
// 0x000008F3 System.Void UnityEngine.Playables.PlayableAsset::Internal_GetPlayableAssetDuration(UnityEngine.Playables.PlayableAsset,System.IntPtr)
extern void PlayableAsset_Internal_GetPlayableAssetDuration_m099C6F58730A818ACA8C837D3DDBFC4ACA75693F (void);
// 0x000008F4 System.Void UnityEngine.Playables.PlayableAsset::.ctor()
extern void PlayableAsset__ctor_m669F459CFACFE65873346E428F206C457B488167 (void);
// 0x000008F5 System.Void UnityEngine.Playables.PlayableBehaviour::.ctor()
extern void PlayableBehaviour__ctor_mE96A877D927BEEC8C9368A0673FEAD77A78C35EE (void);
// 0x000008F6 System.Void UnityEngine.Playables.PlayableBehaviour::OnGraphStart(UnityEngine.Playables.Playable)
extern void PlayableBehaviour_OnGraphStart_m41537F7ED140E16D8666B4959D99EF9BC16FF622 (void);
// 0x000008F7 System.Void UnityEngine.Playables.PlayableBehaviour::OnGraphStop(UnityEngine.Playables.Playable)
extern void PlayableBehaviour_OnGraphStop_m5B1C17F7DA22FFBF8BABB18CBA090AB64FD51740 (void);
// 0x000008F8 System.Void UnityEngine.Playables.PlayableBehaviour::OnPlayableCreate(UnityEngine.Playables.Playable)
extern void PlayableBehaviour_OnPlayableCreate_m963F9A0600B2208400FE3F90647FBACE0B5FE0DD (void);
// 0x000008F9 System.Void UnityEngine.Playables.PlayableBehaviour::OnPlayableDestroy(UnityEngine.Playables.Playable)
extern void PlayableBehaviour_OnPlayableDestroy_mC1C991442A5940826928EA321FC5EFBE68482A57 (void);
// 0x000008FA System.Void UnityEngine.Playables.PlayableBehaviour::OnBehaviourPlay(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
extern void PlayableBehaviour_OnBehaviourPlay_m4B44CD41A9CB3EA391BCB142FA3B9A80AFAFF820 (void);
// 0x000008FB System.Void UnityEngine.Playables.PlayableBehaviour::OnBehaviourPause(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
extern void PlayableBehaviour_OnBehaviourPause_mBC9C4536B30B413E248C96294F53CDABA2C1490C (void);
// 0x000008FC System.Void UnityEngine.Playables.PlayableBehaviour::PrepareFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
extern void PlayableBehaviour_PrepareFrame_m0FC4368B39C1DBC6586E417C8505D1A8C49235E2 (void);
// 0x000008FD System.Void UnityEngine.Playables.PlayableBehaviour::ProcessFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData,System.Object)
extern void PlayableBehaviour_ProcessFrame_m32F9B265BB54D1A3A290E2709FDD0B873360B25A (void);
// 0x000008FE System.Object UnityEngine.Playables.PlayableBehaviour::Clone()
extern void PlayableBehaviour_Clone_m2BB70A16D658FD18307D084EFFFE4A7B1BB234FD (void);
// 0x000008FF System.Void UnityEngine.Playables.PlayableBinding::.cctor()
extern void PlayableBinding__cctor_mF1C450FA8C820DA444D8AB9235958EC750AE60C8 (void);
// 0x00000900 System.Void UnityEngine.Playables.PlayableBinding/CreateOutputMethod::.ctor(System.Object,System.IntPtr)
extern void CreateOutputMethod__ctor_m252187F08E76732D791C8458AF5629F29BDDB8F2 (void);
// 0x00000901 UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableBinding/CreateOutputMethod::Invoke(UnityEngine.Playables.PlayableGraph,System.String)
extern void CreateOutputMethod_Invoke_m56F81543465C6F9C65319BEEEFC5AEEF6A0D7764 (void);
// 0x00000902 System.IAsyncResult UnityEngine.Playables.PlayableBinding/CreateOutputMethod::BeginInvoke(UnityEngine.Playables.PlayableGraph,System.String,System.AsyncCallback,System.Object)
extern void CreateOutputMethod_BeginInvoke_m4FC768B14DF77F9DB8847F3FAF1CBFD11048030D (void);
// 0x00000903 UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableBinding/CreateOutputMethod::EndInvoke(System.IAsyncResult)
extern void CreateOutputMethod_EndInvoke_m61A9C47D6ED76402024C0C7139C4A6F1D58D4C47 (void);
// 0x00000904 System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType()
// 0x00000905 UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern void PlayableHandle_get_Null_m09DE585EF795EFA2811950173C80F4FA24CBAAD1 (void);
// 0x00000906 System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern void PlayableHandle_op_Equality_mBA774AE123AF794A1EB55148206CDD52DAFA42DF (void);
// 0x00000907 System.Boolean UnityEngine.Playables.PlayableHandle::Equals(System.Object)
extern void PlayableHandle_Equals_m7D3DC594EE8EE029E514FF9505C5E7A820D2435E (void);
// 0x00000908 System.Boolean UnityEngine.Playables.PlayableHandle::Equals(UnityEngine.Playables.PlayableHandle)
extern void PlayableHandle_Equals_mBCBD135BA1DBB6B5F8884A21EE55FDD5EADB555C (void);
// 0x00000909 System.Int32 UnityEngine.Playables.PlayableHandle::GetHashCode()
extern void PlayableHandle_GetHashCode_mFEF967B1397A1DC2EE05FC8DBB9C5E13161E3C45 (void);
// 0x0000090A System.Boolean UnityEngine.Playables.PlayableHandle::CompareVersion(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern void PlayableHandle_CompareVersion_m24BEA91E99FF5FD3472B1A71CB78296D99F808A9 (void);
// 0x0000090B System.Boolean UnityEngine.Playables.PlayableHandle::IsValid()
extern void PlayableHandle_IsValid_mDA0A998EA6E2442C5F3B6CDFAF07EBA9A6873059 (void);
// 0x0000090C System.Type UnityEngine.Playables.PlayableHandle::GetPlayableType()
extern void PlayableHandle_GetPlayableType_m962BE384C093FF07EAF156DA373806C2D6EF1AD1 (void);
// 0x0000090D System.Void UnityEngine.Playables.PlayableHandle::.cctor()
extern void PlayableHandle__cctor_m6FA486FD9ECB91B10F04E59EFE993EC7663B6EA3 (void);
// 0x0000090E System.Boolean UnityEngine.Playables.PlayableHandle::IsValid_Injected(UnityEngine.Playables.PlayableHandle&)
extern void PlayableHandle_IsValid_Injected_mCCEEB80CD0855FD683299F6DBD4F9BA864C58C31 (void);
// 0x0000090F System.Type UnityEngine.Playables.PlayableHandle::GetPlayableType_Injected(UnityEngine.Playables.PlayableHandle&)
extern void PlayableHandle_GetPlayableType_Injected_m65C3EB2DEC74C0A02707B6A61D25B3BFEC91DB66 (void);
// 0x00000910 System.Void UnityEngine.Playables.PlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern void PlayableOutput__ctor_m881EC9E4BAD358971373EE1D6BF9C37DDB7A4943 (void);
// 0x00000911 UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::GetHandle()
extern void PlayableOutput_GetHandle_m079ADC0139E95AA914CD7502F27EC79BB1A876F3 (void);
// 0x00000912 System.Boolean UnityEngine.Playables.PlayableOutput::Equals(UnityEngine.Playables.PlayableOutput)
extern void PlayableOutput_Equals_m458689DD056559A7460CCE496BF6BEC45EB47C5B (void);
// 0x00000913 System.Void UnityEngine.Playables.PlayableOutput::.cctor()
extern void PlayableOutput__cctor_m833F06DD46347C62096CEF4E22DBC3EB9ECDACD5 (void);
// 0x00000914 UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern void PlayableOutputHandle_get_Null_mA462EF24F3B0CDD5B3C3F0C57073D49CED316FA4 (void);
// 0x00000915 System.Int32 UnityEngine.Playables.PlayableOutputHandle::GetHashCode()
extern void PlayableOutputHandle_GetHashCode_mC35D44FD77BCA850B945C047C6E2913436B1DF1C (void);
// 0x00000916 System.Boolean UnityEngine.Playables.PlayableOutputHandle::op_Equality(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern void PlayableOutputHandle_op_Equality_m9917DCF55902BB4984B830C4E3955F9C4E4CE0C0 (void);
// 0x00000917 System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(System.Object)
extern void PlayableOutputHandle_Equals_m42558FEC083CC424CB4BD715FC1A6561A2E47EB2 (void);
// 0x00000918 System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(UnityEngine.Playables.PlayableOutputHandle)
extern void PlayableOutputHandle_Equals_m1FCA747BC3F69DC784912A932557EB3B24DC3F18 (void);
// 0x00000919 System.Boolean UnityEngine.Playables.PlayableOutputHandle::CompareVersion(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern void PlayableOutputHandle_CompareVersion_m4DD52E80EDD984F824FE235F35B849C5BA9B4964 (void);
// 0x0000091A System.Void UnityEngine.Playables.PlayableOutputHandle::.cctor()
extern void PlayableOutputHandle__cctor_mD1C850FF555697A09A580322C66357B593C9294E (void);
// 0x0000091B UnityEngine.Experimental.GlobalIllumination.LinearColor UnityEngine.Experimental.GlobalIllumination.LinearColor::Convert(UnityEngine.Color,System.Single)
extern void LinearColor_Convert_m34C66A797B11DE3EE19A9ED913B286C5C76F3B4E (void);
// 0x0000091C UnityEngine.Experimental.GlobalIllumination.LinearColor UnityEngine.Experimental.GlobalIllumination.LinearColor::Black()
extern void LinearColor_Black_m0C3B7331D5DEBB72F85BAFC175BC785D964D30D8 (void);
// 0x0000091D System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.DirectionalLight&)
extern void LightDataGI_Init_m93EBCF45B2A5F8A0C9879FD2CF1B853514837F4B (void);
// 0x0000091E System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.PointLight&)
extern void LightDataGI_Init_m236C2DEE096CDCF4B4489B9A5630E231531DF022 (void);
// 0x0000091F System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.SpotLight&)
extern void LightDataGI_Init_mBCCAA12227CF3845C831463F7B8572F00CB17CF3 (void);
// 0x00000920 System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.RectangleLight&)
extern void LightDataGI_Init_m24775B5AF758CAE25BA180BC17D9E032064B52B9 (void);
// 0x00000921 System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.DiscLight&)
extern void LightDataGI_Init_m00F56356E1220B292978ABE384B6500A1F1C9291 (void);
// 0x00000922 System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::InitNoBake(System.Int32)
extern void LightDataGI_InitNoBake_m660C58A4878A0DB9E842F642AA6D2800D30F0C48 (void);
// 0x00000923 UnityEngine.Experimental.GlobalIllumination.LinearColor UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::ExtractIndirect(UnityEngine.Light)
extern void LightmapperUtils_ExtractIndirect_m1BA4D17B92F68DE86B8696BCA09901CB9F70E352 (void);
// 0x00000924 System.Single UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::ExtractInnerCone(UnityEngine.Light)
extern void LightmapperUtils_ExtractInnerCone_mAB6BC006F6841F7881DAC60077A61BA525E09C48 (void);
// 0x00000925 System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.DirectionalLight&)
extern void LightmapperUtils_Extract_mA213675C1DD2F46927D6B0D02619CD0F2247E98D (void);
// 0x00000926 System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.PointLight&)
extern void LightmapperUtils_Extract_mC23121E46C67B16DA74D2B74AA96BC9B734B4A86 (void);
// 0x00000927 System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.SpotLight&)
extern void LightmapperUtils_Extract_mC6A2DEE19D89B532926E1E1EF0F6815E069F27C2 (void);
// 0x00000928 System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.RectangleLight&)
extern void LightmapperUtils_Extract_mFF46D8454835FF7AC4B35A807B1F29222425B594 (void);
// 0x00000929 System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.DiscLight&)
extern void LightmapperUtils_Extract_mF66211092ADB3241E3E1902CB84FF8E1A84A301F (void);
// 0x0000092A System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping::SetDelegate(UnityEngine.Experimental.GlobalIllumination.Lightmapping/RequestLightsDelegate)
extern void Lightmapping_SetDelegate_mEA4A2549370F078869895AAC4E01EB916BB49A01 (void);
// 0x0000092B UnityEngine.Experimental.GlobalIllumination.Lightmapping/RequestLightsDelegate UnityEngine.Experimental.GlobalIllumination.Lightmapping::GetDelegate()
extern void Lightmapping_GetDelegate_mCFE706531D3E5A96E71ED963AF8CB4B93A1C0AEE (void);
// 0x0000092C System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping::ResetDelegate()
extern void Lightmapping_ResetDelegate_m25E3082200DFB9F101AEE07039FC4A33C4183738 (void);
// 0x0000092D System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping::RequestLights(UnityEngine.Light[],System.IntPtr,System.Int32)
extern void Lightmapping_RequestLights_m572FA5D5ADA94FF109696431E2EAE13B6D5C9AB6 (void);
// 0x0000092E System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping::.cctor()
extern void Lightmapping__cctor_m542D9D32613B348F62541FB1F0AF70EBF7EBB93A (void);
// 0x0000092F System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping/RequestLightsDelegate::.ctor(System.Object,System.IntPtr)
extern void RequestLightsDelegate__ctor_m32E80A59669219265627BAF616170C5BA130CA77 (void);
// 0x00000930 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping/RequestLightsDelegate::Invoke(UnityEngine.Light[],Unity.Collections.NativeArray`1<UnityEngine.Experimental.GlobalIllumination.LightDataGI>)
extern void RequestLightsDelegate_Invoke_m2B97E4D1ED4DC45416B5EC472FC12B581373E403 (void);
// 0x00000931 System.IAsyncResult UnityEngine.Experimental.GlobalIllumination.Lightmapping/RequestLightsDelegate::BeginInvoke(UnityEngine.Light[],Unity.Collections.NativeArray`1<UnityEngine.Experimental.GlobalIllumination.LightDataGI>,System.AsyncCallback,System.Object)
extern void RequestLightsDelegate_BeginInvoke_m842A0B872EE1BDC505161CC083CA189F222784A8 (void);
// 0x00000932 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping/RequestLightsDelegate::EndInvoke(System.IAsyncResult)
extern void RequestLightsDelegate_EndInvoke_mB5DE6574659D68281BC3D8179211DA892A481333 (void);
// 0x00000933 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping/<>c::.cctor()
extern void U3CU3Ec__cctor_mB5E5B471CF113C81A1D2CF2C4245C2181590DB0C (void);
// 0x00000934 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping/<>c::.ctor()
extern void U3CU3Ec__ctor_m9919FB3B009FEC46DBE2AAB63233F08B8B8D798D (void);
// 0x00000935 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping/<>c::<.cctor>b__7_0(UnityEngine.Light[],Unity.Collections.NativeArray`1<UnityEngine.Experimental.GlobalIllumination.LightDataGI>)
extern void U3CU3Ec_U3C_cctorU3Eb__7_0_m2D9D2C1DEA68EC9BC1F47CA41F462BB9EF72CF7B (void);
// 0x00000936 UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Playables.CameraPlayable::GetHandle()
extern void CameraPlayable_GetHandle_mC710651CAEE2596697CFFC01014BEB041C67E2B4 (void);
// 0x00000937 System.Boolean UnityEngine.Experimental.Playables.CameraPlayable::Equals(UnityEngine.Experimental.Playables.CameraPlayable)
extern void CameraPlayable_Equals_mBA0325A3187672B8FDE237D2D5229474C19E3A52 (void);
// 0x00000938 UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Playables.MaterialEffectPlayable::GetHandle()
extern void MaterialEffectPlayable_GetHandle_mB0F6F324656489CE4DE4EFA8ACCE37458D292439 (void);
// 0x00000939 System.Boolean UnityEngine.Experimental.Playables.MaterialEffectPlayable::Equals(UnityEngine.Experimental.Playables.MaterialEffectPlayable)
extern void MaterialEffectPlayable_Equals_m9C2DB0EB37CFB9679961D667B61E2360384C71DA (void);
// 0x0000093A UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Playables.TextureMixerPlayable::GetHandle()
extern void TextureMixerPlayable_GetHandle_mBC5D38A23834675B7A8D5314DCF4655C83AE649C (void);
// 0x0000093B System.Boolean UnityEngine.Experimental.Playables.TextureMixerPlayable::Equals(UnityEngine.Experimental.Playables.TextureMixerPlayable)
extern void TextureMixerPlayable_Equals_mEDE18FD43C9501F04871D2357DC66BABE43F397B (void);
// 0x0000093C System.Boolean UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::TickRealtimeProbes()
extern void BuiltinRuntimeReflectionSystem_TickRealtimeProbes_m7100EF316D04C407A21C27A35752826E463603EB (void);
// 0x0000093D System.Void UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::Dispose()
extern void BuiltinRuntimeReflectionSystem_Dispose_m60454D78E8492E739E2537F0B80FC76AC5DA1FCC (void);
// 0x0000093E System.Void UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::Dispose(System.Boolean)
extern void BuiltinRuntimeReflectionSystem_Dispose_m46A331A4A718F67B97CC07E98D419C0BBF38A8B0 (void);
// 0x0000093F System.Boolean UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::BuiltinUpdate()
extern void BuiltinRuntimeReflectionSystem_BuiltinUpdate_mE62DD3456554487F133F6CCF3D07E0CE6B7A07AB (void);
// 0x00000940 UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::Internal_BuiltinRuntimeReflectionSystem_New()
extern void BuiltinRuntimeReflectionSystem_Internal_BuiltinRuntimeReflectionSystem_New_m1B6EE7816B71869B7F874488A51FF0093F1FF8CB (void);
// 0x00000941 System.Void UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::.ctor()
extern void BuiltinRuntimeReflectionSystem__ctor_mB95177E1C8083A75B0980623778F2B6F3A80FE52 (void);
// 0x00000942 System.Boolean UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem::TickRealtimeProbes()
// 0x00000943 System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings::set_Internal_ScriptableRuntimeReflectionSystemSettings_system(UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem)
extern void ScriptableRuntimeReflectionSystemSettings_set_Internal_ScriptableRuntimeReflectionSystemSettings_system_m9D4C5A04E52667F4A9C15144B854A9D84D089590 (void);
// 0x00000944 UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings::get_Internal_ScriptableRuntimeReflectionSystemSettings_instance()
extern void ScriptableRuntimeReflectionSystemSettings_get_Internal_ScriptableRuntimeReflectionSystemSettings_instance_m67BF9AE5DFB70144A8114705C51C96FFB497AA3E (void);
// 0x00000945 System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings::ScriptingDirtyReflectionSystemInstance()
extern void ScriptableRuntimeReflectionSystemSettings_ScriptingDirtyReflectionSystemInstance_m02B2FCD9BBCFA4ECC1D91A0B5B8B83856AC55718 (void);
// 0x00000946 System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings::.cctor()
extern void ScriptableRuntimeReflectionSystemSettings__cctor_m9CC42ECA95CACFFF874575B63D1FA461667D194C (void);
// 0x00000947 UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper::get_implementation()
extern void ScriptableRuntimeReflectionSystemWrapper_get_implementation_mABDBD524BA9D869BCC02159B00C3B5F6DEE21895 (void);
// 0x00000948 System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper::set_implementation(UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem)
extern void ScriptableRuntimeReflectionSystemWrapper_set_implementation_m7866062401FA10180983AFE19BA672AE63CED29B (void);
// 0x00000949 System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper::Internal_ScriptableRuntimeReflectionSystemWrapper_TickRealtimeProbes(System.Boolean&)
extern void ScriptableRuntimeReflectionSystemWrapper_Internal_ScriptableRuntimeReflectionSystemWrapper_TickRealtimeProbes_m65564441C57A8D5D3BA116C171ECE95B91F734A2 (void);
// 0x0000094A System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper::.ctor()
extern void ScriptableRuntimeReflectionSystemWrapper__ctor_mB936D4EA457BCCBEB0413F3F5B216164D88C4A3F (void);
// 0x0000094B UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat(UnityEngine.TextureFormat,System.Boolean)
extern void GraphicsFormatUtility_GetGraphicsFormat_mBA4E395B8A78B67B0969356DE19F6F1E73D284E0 (void);
// 0x0000094C UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat_Native_TextureFormat(UnityEngine.TextureFormat,System.Boolean)
extern void GraphicsFormatUtility_GetGraphicsFormat_Native_TextureFormat_mAB77B8E00F9BE01455C0E011CF426FFBC53FA533 (void);
// 0x0000094D UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat(UnityEngine.RenderTextureFormat,System.Boolean)
extern void GraphicsFormatUtility_GetGraphicsFormat_mA94B78BFCA8AFEBB85150D361A9A20C83FC5277E (void);
// 0x0000094E UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat_Native_RenderTextureFormat(UnityEngine.RenderTextureFormat,System.Boolean)
extern void GraphicsFormatUtility_GetGraphicsFormat_Native_RenderTextureFormat_m36427FF5F0909DFE7AB6849EAC2D01A4E8D5ECF8 (void);
// 0x0000094F UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat(UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern void GraphicsFormatUtility_GetGraphicsFormat_m4527266E37A786CC45C6BDD520A793C3B5A3A09E (void);
// 0x00000950 System.Boolean UnityEngine.Experimental.Rendering.GraphicsFormatUtility::IsSRGBFormat(UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void GraphicsFormatUtility_IsSRGBFormat_mBF49E7451A3960BD67B1F13745BCA3AC5C8AC66E (void);
// 0x00000951 System.Boolean UnityEngine.Experimental.Rendering.GraphicsFormatUtility::IsCompressedTextureFormat(UnityEngine.TextureFormat)
extern void GraphicsFormatUtility_IsCompressedTextureFormat_m456D7B059F25F7378E05E3346CB1670517A46C71 (void);
// 0x00000952 System.Boolean UnityEngine.Experimental.Rendering.GraphicsFormatUtility::IsCrunchFormat(UnityEngine.TextureFormat)
extern void GraphicsFormatUtility_IsCrunchFormat_m97E8A6551AAEE6B1E4E92F92167FC97CC7D73DB1 (void);
// 0x00000953 System.Void UnityEngine.Assertions.Assert::Fail(System.String,System.String)
extern void Assert_Fail_m9A3A2A08A2E1D18D0BB7D0C635055839EAA2EF02 (void);
// 0x00000954 System.Void UnityEngine.Assertions.Assert::AreEqual(T,T,System.String)
// 0x00000955 System.Void UnityEngine.Assertions.Assert::AreEqual(T,T,System.String,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000956 System.Void UnityEngine.Assertions.Assert::AreEqual(UnityEngine.Object,UnityEngine.Object,System.String)
extern void Assert_AreEqual_mFD46F687B9319093BA13D285D19999C801DC0A60 (void);
// 0x00000957 System.Void UnityEngine.Assertions.Assert::AreEqual(System.Int32,System.Int32)
extern void Assert_AreEqual_mF4EDACE982A071478F520E76D1901B840411A91E (void);
// 0x00000958 System.Void UnityEngine.Assertions.Assert::.cctor()
extern void Assert__cctor_mB34E1F44EB37A40F434BDB787B5E55F2B4033AF5 (void);
// 0x00000959 System.Void UnityEngine.Assertions.AssertionException::.ctor(System.String,System.String)
extern void AssertionException__ctor_mDB97D95CF48EE1F6C184D30F6C3E099E1C4DA7FD (void);
// 0x0000095A System.String UnityEngine.Assertions.AssertionException::get_Message()
extern void AssertionException_get_Message_m857FDA8060518415B2FFFCCA4A6BEE7B9BF66ECE (void);
// 0x0000095B System.String UnityEngine.Assertions.AssertionMessageUtil::GetMessage(System.String)
extern void AssertionMessageUtil_GetMessage_mA6DC7467BAF09543EA091FC63587C9011E1CA261 (void);
// 0x0000095C System.String UnityEngine.Assertions.AssertionMessageUtil::GetMessage(System.String,System.String)
extern void AssertionMessageUtil_GetMessage_m97506B9B19E9F75E8FE164BF64085466FC96EA18 (void);
// 0x0000095D System.String UnityEngine.Assertions.AssertionMessageUtil::GetEqualityMessage(System.Object,System.Object,System.Boolean)
extern void AssertionMessageUtil_GetEqualityMessage_mDBD27DDE3A03418E4A2F8DB377AE866F656C812A (void);
static Il2CppMethodPointer s_methodPointers[2397] = 
{
	MathfInternal__cctor_m885D4921B8E928763E7ABB4466659665780F860F,
	TypeInferenceRuleAttribute__ctor_m389751AED6740F401AC8DFACD5914C13AB24D8A6,
	TypeInferenceRuleAttribute__ctor_m34920F979AA071F4973CEEEF6F91B5B6A53E5765,
	TypeInferenceRuleAttribute_ToString_m49343B52ED0F3E75B3E56E37CF523F63E5A746F6,
	GenericStack__ctor_m0659B84DB6B093AF1F01F566686C510DDEEAE848,
	ProfilerMarker__ctor_mF9F9BDCB1E4618F9533D83D47EAD7325A32FDC2A,
	ProfilerMarker_Internal_Create_m92F2A7651D4BF3F3D0CB62078DD79B71839FA370,
	JobHandle_ScheduleBatchedJobs_mE52469B0B3D765B57BC658E82815840C83A6A4D0,
	NativeLeakDetection_Initialize_m70E48965BE4B399698C8034015B4F0EBD8D4C6E7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NativeContainerAttribute__ctor_mD22697FA575BA0404B981921B295C1A4B89C9F42,
	NativeContainerSupportsMinMaxWriteRestrictionAttribute__ctor_m3D87D41F66CB34605B2C23D12BD04E9546AF321D,
	NativeContainerSupportsDeallocateOnJobCompletionAttribute__ctor_m56D5D2E8D7FE0BF8368167A7139204F4740A875B,
	NativeContainerSupportsDeferredConvertListToArray__ctor_m81D3D40F97FDB1675D128ACD785A9658E5E9DBB2,
	WriteAccessRequiredAttribute__ctor_mBB72625FD2C0CE5081BCEBF5C6122581723574B5,
	NativeDisableUnsafePtrRestrictionAttribute__ctor_m25F3A64C3715BB3C92C7150DB1F46BC88091B653,
	NULL,
	NULL,
	UnsafeUtility_Free_mAC082BB03B10D20CA9E5AD7FBA33164DF2B52E89,
	NULL,
	NULL,
	NULL,
	MeansImplicitUseAttribute__ctor_m2AA2C4CF491D9E594351112A4606017D4A7EF0E7,
	MeansImplicitUseAttribute__ctor_m94FE505EEBB6820677CC4146576D44B2D2684E54,
	SortingLayer_GetLayerValueFromID_m564F9C83200E5EC3E9578A75854CB943CE5546F8,
	Keyframe__ctor_m0EA9CF8E65F32EE7603302E2CC670C56DC177C13,
	Keyframe_get_time_m5A49381A903E63DD63EF8A381BA26C1A2DEF935D,
	Keyframe_set_time_mAD4CA2282CD1B7C1D330C3EE75F79AD636C7FC83,
	Keyframe_get_value_m0DD3FAD00F43E7018FECBD011B04310E25C590FD,
	Keyframe_set_value_m578277371F57893D5FF121710CF016A52302652E,
	Keyframe_get_inTangent_mE19351A6720331D0D01C299ED6480780B20CB8E0,
	Keyframe_set_inTangent_mAA19EC59A3F6D360310D2F6B765359A069F8058E,
	Keyframe_get_outTangent_mF1B82FA2B7E060906B77CEAF229516D69789BEAE,
	Keyframe_set_outTangent_mDEEA8549F31FA0AAC090B2D6D7E15C42D3CAE90B,
	Keyframe_get_weightedMode_mFF562D4B06BCF6A65135C337D6F10BF1C4F3E14D,
	Keyframe_set_weightedMode_m95923B248E6E9947136A115AF240F8C3A3CBC399,
	Keyframe_get_tangentMode_mBF872460D0D000E593C06723BB8FCDDD48983D77,
	Keyframe_set_tangentMode_mC8D2E68FDEF4D5C9083FAF35E6EACE989318A68C,
	Keyframe_get_tangentModeInternal_mCA542C1309EAB57FC634AFB50D31721FF82FB631,
	Keyframe_set_tangentModeInternal_mFF714C3EB51D2A08F6A375728C0280D124749AC2,
	AnimationCurve_Internal_Destroy_m295BAECEF97D64ACFE55D7EA91B9E9C077DB6A7C,
	AnimationCurve_Internal_Create_mA7A2A0191C4AAE7BD5B18F0DCC05AD4290D1691B,
	AnimationCurve_Internal_Equals_m7ACF09175F2DC61D95006ABB5BBE1CF7434B2D1D,
	AnimationCurve_Finalize_mDF0DECA505DA883A56B2E3FCE1EF19CC3959F11D,
	AnimationCurve_Evaluate_m51CAA6B1C54B7EF44FE4D74B422C1DA1FA6F8776,
	AnimationCurve_get_keys_m88E1848D255C2893F379E855A522DA9B0E0F78FB,
	AnimationCurve_get_Item_m303DEF117A2702D57F8F5D55D422EC395E4388FC,
	AnimationCurve_get_length_m36B9D49BCB7D677C38A7963FF00313A2E48E7B26,
	AnimationCurve_GetKey_m2FD7B7AB3633B9C353FA59FC3927BC7EB904691F,
	AnimationCurve_GetKeys_mC61A3A3E0D17E5849768B3AB3698F2139CF70EBE,
	AnimationCurve_get_preWrapMode_m3C0DBEEB450ED8FB3FE96C7E1295B8C3562F8905,
	AnimationCurve_set_preWrapMode_mFE146DFCE1E335655A079DCE6E8F046BD63A6489,
	AnimationCurve_get_postWrapMode_m1D9CF9570C4054057C97CDA03773A76C0813CF64,
	AnimationCurve_set_postWrapMode_mF11FF01671DFC6E54D29111561B8C7104DCEB413,
	AnimationCurve__ctor_mE9462D171C06A2A746B9DA1B0A6B0F4FC7DB94CF,
	AnimationCurve__ctor_mA12B39D1FD275B9A8150227B24805C7B218CDF2C,
	AnimationCurve_Equals_m5E3528A0595AC6714584CAD54549D756C9B3DDD5,
	AnimationCurve_Equals_m60310C21F9B109BAC9BA4FACE9BEF88931B22DED,
	AnimationCurve_GetHashCode_m22EEE795E7C76841C40A1563E3E90CBB089B19A6,
	AnimationCurve_GetKey_Injected_m01C24E74FE72837D5E6A59BBB1BACAEC308454EA,
	Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5,
	Application_HasProLicense_m2997DCC5DDAECB068C8A0B396AED23A03186CAD7,
	Application_get_dataPath_m33D721D71C0687F0013C8953FDB0807B7B3F2A01,
	Application_get_streamingAssetsPath_m87163AE531BEB6A6588FABAD3207D829721CF31F,
	Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B,
	Application_get_temporaryCachePath_m6769909A405A6A5A0477F8E5F312920C8115E50C,
	Application_get_unityVersion_mC66901DE17E8F4F5BCA46CF3A4DCB34AF245CF99,
	Application_get_version_m17A1D2CEBF41849D65315834DD4513F16443A13B,
	Application_OpenURL_m2888DA5BDF68B1BC23E983469157783F390D7BC8,
	Application_get_targetFrameRate_m74BA4A1BAF9542F7DAA99DA9DFBD22067874730A,
	Application_SetLogCallbackDefined_mC1FBF1745A2EDE55B290DC4C392A7F991F5039A8,
	Application_get_genuine_mD32DB2B5A6E0DC067136112E2EDA3A5680C97B40,
	Application_get_genuineCheckAvailable_m0FE8E82E9EAA8382B0DB26C5BCDBD1A05C6A4182,
	Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672,
	Application_get_isMobilePlatform_m11B260E344378D2A3CE53FCCA64DAC70F0B783E7,
	Application_get_systemLanguage_m64281F3CE136BD62EC96EF6AC691C612F0DD08B1,
	Application_get_internetReachability_m8BB82882CF9E286370E4D64176CF2B9333E83F0A,
	Application_CallLowMemory_m4C6693BD717D61DB33C2FB061FDA8CE055966E75,
	Application_add_logMessageReceivedThreaded_m6EAA7C1782E341A6A72AC0C9160CF3F781322ABB,
	Application_remove_logMessageReceivedThreaded_m2BD60919BDACAF705EC45A89DE2AC815F965B7C2,
	Application_CallLogCallback_mCA351E4FBE7397C3D09A7FBD8A9B074A4745ED89,
	Application_Internal_ApplicationWantsToQuit_mDF35192EF816ECD73F0BD4AFBCDE1460EF06442A,
	Application_Internal_ApplicationQuit_mC9ACAA5CB0800C837DBD9925E1E389FB918F3DED,
	Application_InvokeOnBeforeRender_mF2E1F3E67C1D160AD1209C1DBC1EC91E8FB88C97,
	Application_InvokeFocusChanged_m61786C9688D01809FAC41250B371CE13C9DBBD6F,
	Application_InvokeDeepLinkActivated_m473D851836BD708C896850AA1DAE2B56A4B01176,
	Application_get_isEditor_m347E6EE16E5109EF613C83ED98DB1EC6E3EF5E26,
	LowMemoryCallback__ctor_m9A428FDE023342AE31B3749FC821B078AEDA2290,
	LowMemoryCallback_Invoke_m3082D6F2046585D3504696B94A59A4CBC43262F8,
	LowMemoryCallback_BeginInvoke_m4686E95B4CF6EDE103DB0448FC54354BAE5F1745,
	LowMemoryCallback_EndInvoke_mB8843171E51584D380B62D3B79BC4F4930A22D0C,
	LogCallback__ctor_mF61E7CECD9E360B0B8A992720860F9816E165731,
	LogCallback_Invoke_mCB0C38C44CBF8BBE88690BE6C0382011C5D5B61F,
	LogCallback_BeginInvoke_mECA20C96EB7E35915BC9202F833685D0ED5F66A7,
	LogCallback_EndInvoke_m03CD6E28DACBF36E7A756F8CC653E546CBF3FD54,
	BootConfigData_WrapBootConfigData_m7C2DCB60E1456C2B7748ECFAAEB492611A5D7690,
	BootConfigData__ctor_m6C109EB48CAE91C89BB6C4BFD10C77EA6723E5AE,
	CachedAssetBundle__ctor_mBB07A43F19F5095BC21A44543684F6B4269CE315,
	CachedAssetBundle_get_name_m31556A8588D4BE9261B0AB02486118A30D3ED971,
	CachedAssetBundle_get_hash_mC7D2AD78B64D7CDEF0604C934BF97D6C388A3EC1,
	Cache_get_handle_m2A81C085DE744E211DCC3F5BD608043EEB93075D,
	Cache_GetHashCode_m3C305D87FF4578864D1844170133E2758986B244,
	Cache_Equals_m4E85CF7FABB9052E9673879DF8FB341453D76F06,
	Cache_Equals_m49F0BDF7BE07D3FA50A70ED8FE85DBFF178C11C5,
	Cache_get_valid_m01BE7338A2AB06309EC3808BD361F0A355E0B29F,
	Cache_Cache_IsValid_mB53CE63DB4F248634246D0FA01304E2CE0634134,
	Cache_get_path_mC3C0F1863B3EE5BE472D43BC25797E1EC3278A6B,
	Cache_Cache_GetPath_m0D63A662CE529A63D7B33C557D183019EB3CCFD1,
	Cache_set_maximumAvailableStorageSpace_m2F9DA827094B24940EE439739EC7B78EC5237559,
	Cache_Cache_SetMaximumDiskSpaceAvailable_mBCF682A98B18956111635708BD4BF027763E3E52,
	Cache_set_expirationDelay_m2D2ABE9B1D1503A16A94A423852E7D037C7F213A,
	Cache_Cache_SetExpirationDelay_m0ECC3CCFE9B618FFAF32C5B4C431F28B7D36051A,
	Caching_set_compressionEnabled_mDFEB4694C4E4CEA778ED304C4A13C08D4B4BA628,
	Caching_get_ready_m6C8272E098105B2BA5C88055B10EA5B4530F5BBB,
	Caching_ClearCachedVersion_m4A896CB52B29B1DE99561B554816835FAFC9EEA7,
	Caching_ClearCachedVersionInternal_m82D1DC4D0D7DB7BE62A575B1A9158740CE45FB4B,
	Caching_ClearOtherCachedVersions_m4658A68D22DF56BE2657A10D2DC5CC56AE19DB8C,
	Caching_ClearAllCachedVersions_m6C3EEF03B4A51993F24FEF394C57BD4F3F66D30A,
	Caching_ClearCachedVersions_mF759564A2D48559C53CB5A18FB2AF0393ED7EEF9,
	Caching_IsVersionCached_m4AB434A7DD30FAC3058F9861AEB2F6A967B43081,
	Caching_IsVersionCached_m407E9764BC468430F2BCD6B75C1CECBC108613C5,
	Caching_AddCache_m573357DBAD8C1DDE863196456B72B8DD191DB689,
	Caching_AddCache_m2D73DCAF1FF375066A5B215AAB7A8209978F18AE,
	Caching_GetCacheByPath_m023FB602A5A1041F7AEC5E81AE1273E93B75F7B9,
	Caching_get_defaultCache_mFB94A67BA445311B4F52E2F44F6263397BF85B29,
	Caching_get_currentCacheForWriting_mFB8AE12285B5668804B7CFD7A387D4FA4B7F1748,
	Caching_set_currentCacheForWriting_mF41EA3ECE25C4A2AFE4037F002A03E3245EEC2E8,
	Caching_ClearCachedVersionInternal_Injected_mA7817A8A0AFFB1F20ED896A1A7BBEB4C54F43936,
	Caching_ClearCachedVersions_Injected_m58ABB302FB321A31B01AD3395DD7D45A7C4FBEB7,
	Caching_IsVersionCached_Injected_mB81B7060C3A07ABC01CADF1B35462F3DA410F73E,
	Caching_AddCache_Injected_m4D9302CA8DE41732BFE94CA7DE4193A25BF2ADC2,
	Caching_GetCacheByPath_Injected_m4E373A60168E4A550F2B09F21673842DC71CFB51,
	Caching_get_defaultCache_Injected_mE50C6C2A72FA0B2862D51E7969F2A6ECF81DAB8D,
	Caching_get_currentCacheForWriting_Injected_mCA9AB9FDA1F2AB9A87046B99938327D473AC0C2A,
	Caching_set_currentCacheForWriting_Injected_m3C95A1BE071301E134A8B472E8E1C1D022F6316B,
	Camera_get_nearClipPlane_mD9D3E3D27186BBAC2CC354CE3609E6118A5BF66C,
	Camera_set_nearClipPlane_m9D81E50F8658C16319BEF3774E78B93DEB208C6B,
	Camera_get_farClipPlane_mF51F1FF5BE87719CFAC293E272B1138DC1EFFD4B,
	Camera_set_farClipPlane_m52986DC40B7F577255C4D5A4F780FD8A7D862626,
	Camera_get_fieldOfView_m065A50B70AC3661337ACA482DDEFA29CCBD249D6,
	Camera_set_fieldOfView_m5006BA0D01A27619A053704D3BD6A8938F7DEDA5,
	Camera_get_orthographicSize_m700FCD8CF48BC59A0415A624328B4A627B88D958,
	Camera_set_orthographicSize_mF15F37A294A7AA2ADD9519728A495DFA0A836428,
	Camera_get_depth_m436C49A1C7669E4AD5665A1F1107BDFBA38742CD,
	Camera_set_depth_m4A83CCCF7370B8AD4BDB2CD5528A6E12A409AE58,
	Camera_get_aspect_m2ADA7982754920C3B58B4DB664801D6F2416E0C6,
	Camera_set_aspect_m84BE4641686B30B8F9FFEA47BB1D7D88E27344EE,
	Camera_get_cullingMask_m0992E96D87A4221E38746EBD882780CEFF7C2BCD,
	Camera_set_cullingMask_m215DB1C878CF1ADEEF6800AF449EEEA7680ECCCD,
	Camera_get_eventMask_m1D85900090AF34244340C69B53A42CDE5E9669D3,
	Camera_get_backgroundColor_m14496C5DC24582D7227277AF71DBE96F8E9E64FF,
	Camera_set_backgroundColor_mDB9CA1B37FE2D52493823914AC5BC9F8C1935D6F,
	Camera_get_clearFlags_m1D02BA1ABD7310269F6121C58AF41DCDEF1E0266,
	Camera_set_clearFlags_m805DFBD136AA3E1E46A2E61441965D174E87FE50,
	Camera_get_rect_m3570AA056526AB01C7733B4E7BE69F332E128A08,
	Camera_set_rect_m6DB9964EA6E519E2B07561C8CE6AA423980FEC11,
	Camera_get_pixelRect_mBA87D6C23FD7A5E1A7F3CE0E8F9B86A9318B5317,
	Camera_set_pixelRect_m9380482EFA5D7912988D585E9538A58988C8E0E9,
	Camera_get_targetTexture_m1E776560FAC888D8210D49CEE310BB39D34A3FDC,
	Camera_get_targetDisplay_m2C318D2EB9A016FEC76B13F7F7AE382F443FB731,
	Camera_WorldToScreenPoint_m315B44D111E92F6C81C39B7B0927622289C1BC52,
	Camera_WorldToViewportPoint_m20C50DA05728A82BCDE98CBED34841BA6FB0EA59,
	Camera_WorldToScreenPoint_m880F9611E4848C11F21FDF1A1D307B401C61B1BF,
	Camera_WorldToViewportPoint_mC939A83CE134EA2CE666C2DF5AD8FA4CB48AE3C1,
	Camera_ScreenToViewportPoint_m52ABFA35ADAA0B4FF3A7EE675F92F8F483E821FD,
	Camera_ScreenPointToRay_m84C3D8E0A4E8390A353C2361A0900372742065A0,
	Camera_ScreenPointToRay_m7069BC09C3D802595AC1FBAEFB3C59C8F1FE5FE2,
	Camera_ScreenPointToRay_m27638E78502DB6D6D7113F81AF7C210773B828F3,
	Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA,
	Camera_get_current_m6D8446E8359399CD9108A8E524671E0CC6E20350,
	Camera_GetAllCamerasCount_mBA721F43F94AA5DB555461DE11351CBAF8267662,
	Camera_GetAllCamerasImpl_mC93829FFC53391EA6C5012E5FA3817BA20DBEA89,
	Camera_get_allCamerasCount_mF6CDC46D6F61B1F1A0337A9AD7DFA485E408E6A1,
	Camera_GetAllCameras_m500A4F27E7BE1C259E9EAA0AEBB1E1B35893059C,
	Camera_FireOnPreCull_m7E8B65875444B1DE75170805AE22908ADE52301E,
	Camera_FireOnPreRender_m996699B5D50FC3D0AB05EED9F9CE581CCDC2FF67,
	Camera_FireOnPostRender_m17457A692D59CBDDDBBE0E4C441D393DAD58654B,
	Camera__ctor_mD07AB17467A910BC7A4EE32BB9DD546748E31254,
	Camera_get_backgroundColor_Injected_m35D7092E021C199D24A3457297EEEAA520CAC999,
	Camera_set_backgroundColor_Injected_m5E1BF10175DAB3F4E2D83810640D1FB5EF49A9F9,
	Camera_get_rect_Injected_m88F10E0BE4F27E638C97010D4616DBB14338EEED,
	Camera_set_rect_Injected_m62EE0CFFE15C612C226DEAB0ADB3D3B81ABE0C4E,
	Camera_get_pixelRect_Injected_mDE6A7F125BC1DD2BCFEA3CB03DFA948E5635E631,
	Camera_set_pixelRect_Injected_mBE9F9ED0BC921A91C9E1B85075E8E7F8ECD61D45,
	Camera_WorldToScreenPoint_Injected_m640C6AFA68F6C2AD25AFD9E06C1AEFEAC5B48B01,
	Camera_WorldToViewportPoint_Injected_m5491C4691EB46452E1A48AE20A18769D41B5216C,
	Camera_ScreenToViewportPoint_Injected_m407A30EDD4AC317DE3DD0B4361664F438E5A6639,
	Camera_ScreenPointToRay_Injected_m1135D2C450C7DED657837BEFE5AD7FAFB9B99387,
	CameraCallback__ctor_m7CAE962B355F00AB2868577DC302A1FA80939C50,
	CameraCallback_Invoke_m2B4F10A7BF2620A9BBF1C071D5B4EE828FFE821F,
	CameraCallback_BeginInvoke_m46CF0E3E7E6A18868CBEBEA62D012713B20A8B14,
	CameraCallback_EndInvoke_m3B1E210D6A4F41F0FF74B187B3D7CB64C302D146,
	CullingGroup_SendEvents_m08EBF10EEFF49CF9894BA940FD969C8F53F807E7,
	StateChanged__ctor_m8DCC0DCE42D5257F92FEA1F2B4DA2EF4558006F9,
	StateChanged_Invoke_m2E371D6B1AD1F23F20038D0DEEEFED15D76BC545,
	StateChanged_BeginInvoke_m5BD458B36BF2E71F4FB19444B0FAAA1B87BF8912,
	StateChanged_EndInvoke_mBC050D5602C1F3EC3F8137908D81894E646F5212,
	ReflectionProbe_CallReflectionProbeEvent_mA6273CE84793FD3CC7AA0506C525EED4F4935B62,
	ReflectionProbe_CallSetDefaultReflection_m97EFBE5F8A57BB7C8CA65C65FF4BD9889060325D,
	DebugLogHandler_Internal_Log_m2B637FD9089DEAA9D9FDE458DF5415CDF97424C3,
	DebugLogHandler_Internal_LogException_m8400B0D97B8D4A155A449BD28A32C68373A1A856,
	DebugLogHandler_LogFormat_m3C9B0AD4B5CDFF5AF195F9AA9FCBA908053BA41D,
	DebugLogHandler_LogException_m816CF2DDA84DFC1D1715B24F9626BD623FF05416,
	DebugLogHandler__ctor_mE9664BE5E6020FB88C6A301465811C80DEDFA392,
	Debug_get_unityLogger_mFA75EC397E067D09FD66D56B4E7692C3FCC3E960,
	Debug_ExtractStackTraceNoAlloc_m69F8666E91D27D8C341385EB5CF79621C703248C,
	Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708,
	Debug_Log_mCF5342DCBD53044ABDE6377C236B9E40EC26BF3F,
	Debug_LogFormat_mB23DDD2CD05B2E66F9CF8CA72ECA66C02DCC209E,
	Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29,
	Debug_LogError_m97139CB2EE76D5CD8308C1AD0499A5F163FC7F51,
	Debug_LogErrorFormat_mB54A656B267CF936439D50348FC828921AEDA8A9,
	Debug_LogErrorFormat_m994E4759C25BF0E9DD4179C10E3979558137CCF0,
	Debug_LogException_mBAA6702C240E37B2A834AA74E4FDC15A3A5589A9,
	Debug_LogException_m3CC9A37CD398E5B7F2305896F0969939F1BD1E3E,
	Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568,
	Debug_LogWarning_mD417697331190AC1D21C463F412C475103A7256E,
	Debug_LogWarningFormat_m29C3DA389E1AA2C1C48C9100F1E83EAE72772FDB,
	Debug_LogWarningFormat_m4A02CCF91F3A9392F4AA93576DCE2222267E5945,
	Debug_LogAssertion_m2A8940871EC1BD01A405103429F2FCE2AFB12506,
	Debug_get_isDebugBuild_mED5A7963C7B055A9ACC5565862BBBA6F3D86EDE8,
	Debug_CallOverridenDebugHandler_m5F5FC22445A9C957A655734DA5B661A5E256BEBE,
	Debug_IsLoggingEnabled_m749A9476334B62979E6D9949A0A0E489A9C735D8,
	Debug__cctor_m9BFDFB65B30AA2962FDACD15F36FC666471D1C5E,
	Bounds__ctor_m294E77A20EC1A3E96985FE1A925CB271D1B5266D,
	Bounds_GetHashCode_m9F5F751E9D52F99FCC3DC07407410078451F06AC,
	Bounds_Equals_m1ECFAEFE19BAFB61FFECA5C0B8AE068483A39C61,
	Bounds_Equals_mC2E2B04AB16455E2C17CD0B3C1497835DEA39859,
	Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B,
	Bounds_set_center_mAD29DD80FD631F83AF4E7558BB27A0398E8FD841,
	Bounds_get_size_m0739F2686AE2D3416A33AEF892653091347FD4A6,
	Bounds_set_size_m70855AC67A54062D676174B416FB06019226B39A,
	Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9,
	Bounds_set_extents_mC83719146B06D0575A160CDDE9997202A1192B35,
	Bounds_get_min_m2D48F74D29BF904D1AF19C562932E34ACAE2467C,
	Bounds_get_max_mC3BE43C2A865BAC138D117684BC01E289892549B,
	Bounds_op_Equality_m8168B65BF71D8E5B2F0181677ED79957DD754FF4,
	Bounds_op_Inequality_mA6EBEDD980A41D5E206CBE009731EB1CA0B25502,
	Bounds_SetMinMax_m04969DE5CBC7F9843C12926ADD5F591159C86CA6,
	Bounds_Encapsulate_mD1F1DAC416D7147E07BF54D87CA7FF84C1088D8D,
	Bounds_ToString_m4637EA7C58B9C75651A040182471E9BAB9295666,
	Plane_get_normal_m203D43F51C449990214D04F332E8261295162E84,
	Plane_get_distance_m5358B80C35E1E295C0133E7DC6449BB09C456DEE,
	Plane__ctor_m6535EAD5E675627C2533962F1F7890CBFA2BA44A,
	Plane_Raycast_m04E61D7C78A5DA70F4F73F9805ABB54177B799A9,
	Plane_ToString_mF92ABB5136759C7DFBC26FD3957532B3C26F2099,
	Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B,
	Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187,
	Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E,
	Ray_GetPoint_mE8830D3BA68A184AD70514428B75F5664105ED08,
	Ray_ToString_m73B5291E29C9C691773B44590C467A0D4FBE0EC1,
	Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280,
	Rect__ctor_m027E778E437FED8E6DBCE0C01C854ADF54986ECE,
	Rect_get_zero_m4CF0F9AD904132829A6EFCA85A1BF52794E7E56B,
	Rect_get_x_mC51A461F546D14832EB96B11A7198DADDE2597B7,
	Rect_set_x_m49EFE25263C03A48D52499C3E9C097298E0EA3A6,
	Rect_get_y_m53E3E4F62D9840FBEA751A66293038F1F5D1D45C,
	Rect_set_y_mCFDB9BD77334EF9CD896F64BE63C755777D7CCD5,
	Rect_get_position_m54A2ACD2F97988561D6C83FCEF7D082BC5226D4C,
	Rect_set_position_mD92DFF591D9C96CDD6AF22EA2052BB3D468D68ED,
	Rect_get_center_mA6E659EAAACC32132022AB199793BF641B3068CB,
	Rect_set_center_m6F280BE23E6FAA6214DDA6FB6B7697DB906D879E,
	Rect_get_min_m17345668569CF57C5F1D2B2DADD05DD4220A5950,
	Rect_set_min_m3A3DE206469065D65650DCF4D23F51C25B6C08A7,
	Rect_get_max_m3BFB033D741F205FB04EF163A9D5785E7E020756,
	Rect_set_max_mCC50E64F2DE589A3B7D1BFED72B49AC19D49FAEB,
	Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484,
	Rect_set_width_mC81EF602AC91E0C615C12FCE060254A461A152B8,
	Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5,
	Rect_set_height_mF4CB5A97D4706696F1C9EA31A5D8C466E48050D6,
	Rect_get_size_m731642B8F03F6CE372A2C9E2E4A925450630606C,
	Rect_set_size_m4618056983660063A74F40CCFF9A683933CB4C93,
	Rect_get_xMin_mFDFA74F66595FD2B8CE360183D1A92B575F0A76E,
	Rect_set_xMin_mD8F9BF59F4F33F9C3AB2FEFF32D8C16756B51E34,
	Rect_get_yMin_m31EDC3262BE39D2F6464B15397F882237E6158C3,
	Rect_set_yMin_m58C137C81F3D098CF81498964E1B5987882883A7,
	Rect_get_xMax_mA16D7C3C2F30F8608719073ED79028C11CE90983,
	Rect_set_xMax_m1775041FCD5CA22C77D75CC780D158CD2B31CEAF,
	Rect_get_yMax_m8AA5E92C322AF3FF571330F00579DA864F33341B,
	Rect_set_yMax_m4F1C5632CD4836853A22E979C810C279FBB20B95,
	Rect_Contains_mAD3D41C88795960F177088F847509C9DDA23B682,
	Rect_Contains_m5072228CE6251E7C754F227BA330F9ADA95C1495,
	Rect_OrderMinMax_m1BE37D433FE6B7FB0FB73652E166A4FB887214CD,
	Rect_Overlaps_m2FE484659899E54C13772AB7D9E202239A637559,
	Rect_Overlaps_m4FFECCEAB3FBF23ED5A51B2E26220F035B942B4B,
	Rect_op_Inequality_mAF9DC03779A7C3E1B430D7FFA797F2C4CEAD1FC7,
	Rect_op_Equality_mFBE3505CEDD6B73F66276E782C1B02E0E5633563,
	Rect_GetHashCode_mA23F5D7C299F7E05A0390DF2FA663F5A003799C6,
	Rect_Equals_m76E3B7E2E5CC43299C4BF4CB2EA9EF6E989E23E3,
	Rect_Equals_mC8430F80283016D0783FB6C4E7461BEED4B55C82,
	Rect_ToString_m045E7857658F27052323E301FBA3867AD13A6FE5,
	RectOffset__ctor_m4A29807F411591FC06BE9367167B8F417EF73828,
	RectOffset__ctor_m23620FE61AAF476219462230C6839B86736B80BA,
	RectOffset_Finalize_m69453D37706F46DD3A2A6F39A018D371A5E7072C,
	RectOffset__ctor_mF2A621DBA17A10660FEBE6237ACF4904DA6F9F29,
	RectOffset_ToString_m6852E54822C1FACE624F3306DD9DC71628E91F93,
	RectOffset_Destroy_mE1A6BDB23EC0B1A51AD5365CEF0055F7856AA533,
	RectOffset_InternalCreate_m6638B085A0DA1CB1DA37B7C91CAC98DCF2CF7A16,
	RectOffset_InternalDestroy_m4FAB64D6AECF15082E19BDC4F22913152D5C6FA1,
	RectOffset_get_left_mA86EC00866C1940134873E3A1565A1F700DE67AD,
	RectOffset_set_left_mDA6D192FBCB72EF77601FB4C2644A8E9BCBC610F,
	RectOffset_get_right_m9B05958C3C1B31F1FAB8675834A492C7208F6C96,
	RectOffset_set_right_m71CC11778FDBD3C7FD0B8CEFA272F53BF7311CD9,
	RectOffset_get_top_mBA813D4147BFBC079933054018437F411B6B41E1,
	RectOffset_set_top_m4B0A86BF50785A9EEF101F370E0005B4D817CB6B,
	RectOffset_get_bottom_mE5162CADD266B59539E3EE1967EE9A74705E5632,
	RectOffset_set_bottom_m2D1FC60EBFC84557E7AB189A45F3AC2146E558DD,
	RectOffset_get_horizontal_m9274B965D5D388F6F750D127B3E57F70DF0D89C1,
	RectOffset_get_vertical_m89ED337C8D303C8994B2B056C05368E4286CFC5E,
	Gizmos_DrawLine_m9515D59D2536571F4906A3C54E613A3986DFD892,
	Gizmos_DrawWireSphere_mF6F2BC5CDF7B3F312FE9AB579CDC1C6B72154BCF,
	Gizmos_DrawSphere_mD6F94B7273D31AED07CCFCEA2E6C158A2039DB54,
	Gizmos_set_color_mFA6C199DF05FF557AEF662222CA60EC25DF54F28,
	Gizmos_DrawLine_Injected_m900F8B1C0DC1781B03C2CDFDB707F888A4A05909,
	Gizmos_DrawWireSphere_Injected_mD75DB358F03E22C59D95F1214870E227BB5B1FD2,
	Gizmos_DrawSphere_Injected_m1668DD5BCD5878EB77F3E7C7BCB8692C8A319453,
	Gizmos_set_color_Injected_m0BB0F6D3A3ECB3532D5E00FD3B3A3DDFB75590E8,
	BeforeRenderHelper_Invoke_m5CADC9F58196CF3F11CB1203AEAF40003C7D4ADD,
	BeforeRenderHelper__cctor_mAF1DF30E8F7C2CE586303CAA41303230FE2DEB0D,
	Display__ctor_m1E66361E430C3698C98D242CEB6820E9B4FC7EB8,
	Display__ctor_mE84D2B0874035D8A772F4BAE78E0B8A2C7008E46,
	Display_get_renderingWidth_mA02F65BF724686D7A0CD0C192954CA22592C3B12,
	Display_get_renderingHeight_m1496BF9D66501280B4F75A31A515D8CF416838B0,
	Display_get_systemWidth_mA14AF2D3B017CF4BA2C2990DC2398E528AF83413,
	Display_get_systemHeight_m0D7950CB39015167C175634EF8A5E0C52FBF5EC7,
	Display_RelativeMouseAt_mABDA4BAC2C1B328A2C6A205D552AA5488BFFAA93,
	Display_get_main_mDC0ED8AD60BF5BC3C83384E9C5131403E7033AFA,
	Display_RecreateDisplayList_mA7E2B69AF4BD88A0C45B9A0BB7E1FFDDA5C60FE8,
	Display_FireDisplaysUpdated_m1655DF7464EA901E47BCDD6C3BBB9AFF52757D86,
	Display_GetSystemExtImpl_m946E5A2D11FC99291208F123B660978106C0B5C6,
	Display_GetRenderingExtImpl_m14405A2EC3C99F90D5CD080F3262BB7B4AC2BA49,
	Display_RelativeMouseAtImpl_mDC1A8A011C9B7FF7BC9A53A10FEDE8D817D68CDB,
	Display__cctor_mC1A1851D26DD51ECF2C09DBB1147A7CF05EEEC9D,
	DisplaysUpdatedDelegate__ctor_m976C17F642CEF8A7F95FA4C414B17BF0EC025197,
	DisplaysUpdatedDelegate_Invoke_mBCC82165E169B27958A8FD4E5A90B83A108DAE89,
	DisplaysUpdatedDelegate_BeginInvoke_m5DA06B0A901673F809EA597946702A73F9436BFF,
	DisplaysUpdatedDelegate_EndInvoke_m470B70745AF2631D69A51A3883D774E9B49DD2E2,
	Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3,
	Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150,
	Screen_get_dpi_m92A755DE9E23ABA717B5594F4F52AFB0FBEAC1D3,
	Screen_GetScreenOrientation_m8683EC395BB5ABBA6E9B69DE29818E9B1F82B469,
	Screen_get_orientation_m7977C7ECC2E3F22BB5DF7661951843FCC7E645B0,
	Screen_get_fullScreen_m7C0CE5B87209523630EE751F54E3180AADD02E71,
	Screen_get_fullScreenMode_m89BD87B3D0CB7D42B845E3DE1D36EFC5B3577115,
	Screen_get_safeArea_mEC4D352516A1270B474540D3A21B8798A28251F2,
	Screen_get_safeArea_Injected_m7451CB51BBC812FB534DC16210D407B17304A639,
	Graphics_Internal_GetMaxDrawMeshInstanceCount_mB5F6508A9AB7DBEBC192C6C7BDEF872295FB9801,
	Graphics_get_activeTier_mC69EEB666BDB6DD90E0DD89D18179DBB54C25141,
	Graphics_set_activeTier_m043BD6073CA03FA27B074DFACB98D8BF715CB228,
	Graphics__cctor_m87F7D324CC82B1B70ADFEC237B2BBEDC1767F1FF,
	LightProbes_Internal_CallTetrahedralizationCompletedFunction_m77B1485999595314268B2089930CEC245B77EEFA,
	LightProbes_Internal_CallNeedsRetetrahedralizationFunction_m6AEFB1EE424473756C23793F85A095571BD8368C,
	Resolution_ToString_m42289CE0FC4ED41A9DC62B398F46F7954BC52F04,
	QualitySettings_GetQualityLevel_mAD9FFDEBD51D12D914FDDDE4560E276A4D434A3B,
	QualitySettings_get_names_m3B4CFE04E36768B16C425A6837C9BA09F744CFCD,
	QualitySettings_get_activeColorSpace_m13DBB3B679AA5D5CEA05C2B4517A1FDE1B2CF9B0,
	TrailRenderer_get_time_mE4E051F908FAA39CD6D18658B943F5B28F625024,
	TrailRenderer_set_time_mC4B9324EBEF334A58C498806B478E1D296276359,
	TrailRenderer_get_startWidth_mA5EE0531BEBF2E69495047E0527B13DA1ADB0D9D,
	TrailRenderer_set_startWidth_mA60DF0277CDD09E9C98BDEC76A670BA428FF6872,
	TrailRenderer_get_endWidth_m7AF079A0351C238CBE24A21EEC20241365269C86,
	TrailRenderer_set_endWidth_m50CCB4931A6019B0D24BD18C9C349FF2FAFF42F6,
	LineRenderer_SetColors_mAE8931C4C8A8FE7A1E0CE71B8E69832DA83BC773,
	LineRenderer_set_startColor_m033D0E05F3FC475008FC6CC8008763ABEEFC8D1C,
	LineRenderer_set_endColor_mC12E3B60CA0476CC3916C795721B71A4C7234FE3,
	LineRenderer_set_positionCount_mFE47D750AF310FE3073C3F7A46FF116354EDA49F,
	LineRenderer_SetPosition_m34B4D85846C46404658B07BE930117A7A838F59B,
	LineRenderer_set_startColor_Injected_m7405704CBED43AD225B992978E7DBB2DE70C98D3,
	LineRenderer_set_endColor_Injected_m57D27AC9DF161F86E847A5F76BE4745ED77B6178,
	LineRenderer_SetPosition_Injected_mFB6BFFC7C0B16DFAFF3CCCDF327677EF9B163719,
	MaterialPropertyBlock_SetColorImpl_mB67F26F2146332B78205D2CCA31053316C7D728D,
	MaterialPropertyBlock_CreateImpl_m24B31B00E85647888CE23025A887F670066C167A,
	MaterialPropertyBlock_DestroyImpl_m8BE5B07A016787E1B841496630E77F31243EF109,
	MaterialPropertyBlock__ctor_m9055A333A5DA8CC70CC3D837BD59B54C313D39F3,
	MaterialPropertyBlock_Finalize_m26F82696BBE81EC8910C0991AD7CFD9B4B26DA3D,
	MaterialPropertyBlock_Dispose_m3DED1CD5B678C080B844E4B8CB9F32FDC50041ED,
	MaterialPropertyBlock_SetColor_m2821A6737D983B1EF0260490CB09BA05F1B6411B,
	MaterialPropertyBlock_SetColorImpl_Injected_mA20E5DD8EB1E5BD706F46362B3C0C01FB2F98CEA,
	Renderer_GetMaterial_m370ADC0227BC648BEFAAF85AFB09722E8B20024B,
	Renderer_GetSharedMaterial_m3819AC6D141381FD4C99793E855D2ACED6FB3071,
	Renderer_SetMaterial_m68B4F4BB5C58FB88AE7AF3829B532869C781FD0F,
	Renderer_Internal_SetPropertyBlock_m92C716E9C4DB64FE53D98F1BE4981FA2644AC43B,
	Renderer_Internal_GetPropertyBlock_mB376DBE5A7423EBFC8E01708D6D095308C3BAF96,
	Renderer_SetPropertyBlock_m1B999AB9B425587EF44CF1CB83CDE0A191F76C40,
	Renderer_GetPropertyBlock_mCD279F8A7CEB56ABB9EF9D150103FB1C4FB3CE8C,
	Renderer_get_enabled_m40E07BB15DA58D2EF6F6796C6778163107DD7E1B,
	Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303,
	Renderer_set_shadowCastingMode_mC7E601EE9B32B63097B216C78ED4F854B0AF21EC,
	Renderer_set_receiveShadows_mD2BD2FF58156E328677EAE5E175D2069BC2925A0,
	Renderer_get_sortingLayerID_m2E204E68869EDA3176C334AE1C62219F380A5D85,
	Renderer_set_sortingLayerID_mF2A52A5697C3A9288DEAC949F54C4CCA97716526,
	Renderer_get_sortingOrder_m33DD50ED293AA672FDAD862B4A4865666B5FEBAF,
	Renderer_set_sortingOrder_mBCE1207CDB46CB6BA4583B9C3FB4A2D28DC27D81,
	Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617,
	Renderer_set_material_mB4988AD6A93C7FDACC4C07A99D1DAC23D10C0344,
	Renderer_get_sharedMaterial_m2BE9FF3D269968F2E323AC60EFBBCC0B26E7E6F9,
	Renderer_set_sharedMaterial_mC94A354D9B0FCA081754A7CB51AEE5A9AD3946A3,
	Shader_get_globalShaderHardwareTier_mDD5E88C6D806A290A68D4681AA1D5AC530D20DC4,
	Shader_set_globalShaderHardwareTier_m59BCE1D4304BA2C18CE1950E000BA092066EF7EB,
	Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B,
	Shader_FindBuiltin_m9BDF3202A668A79B3F46C695384E37A8FB5F8A9C,
	Shader_get_maximumLOD_m080015C047D4B64E41630BABA1C332D7B96896E3,
	Shader_set_maximumLOD_mAF8E3615BB40E1381CDC4110D20D3BB866AFDC4B,
	Shader_get_globalMaximumLOD_m74C150DDA16A58ABAE0480CAC1F7E8EA754236A4,
	Shader_set_globalMaximumLOD_m551638F9EA54AE00FB59C055B2BD4E414D2199AE,
	Shader_get_isSupported_m3660681289CDFE742D399C3030A6CF5C4D8B030D,
	Shader_get_globalRenderPipeline_m6DAF7BBDDB57FC9ED30D69047D79B810D29DC973,
	Shader_set_globalRenderPipeline_mDD57BC9253ECF13404F987D52A55A288817F3B21,
	Shader_EnableKeyword_m600614EB1D434CA8ECFC8DAA5BC6E2ED4E55CD9F,
	Shader_DisableKeyword_m2D15FB4C26535D9AF45445B4149EADD4BF68C701,
	Shader_IsKeywordEnabled_m08B349CDC3898D74679137BA9AF38FECE72F6119,
	Shader_get_renderQueue_mF5AC966B7BDDF0FEDFD43EAC9EFE48619AF24F27,
	Shader_get_disableBatching_m93E8EC7BA418878FB5C194B74B29EBC50EC5CDC3,
	Shader_WarmupAllShaders_m5C6D779B51EB709A943E8A56588A875CBEBACCF5,
	Shader_TagToID_m0597E33DAA0FD822B2E10881E7740E42222A8392,
	Shader_IDToTag_mEE9CFFD973795B490BFEDC2B1ACA06FB0644B349,
	Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45,
	Shader_GetDependency_m306DE9EC247EA9B015E626EE1AD126F61E643017,
	Shader_get_passCount_m3A6E01161AB184A36A14ECC0DAB437679B8652E8,
	Shader_FindPassTagValue_m284633DA2F70B956526BE4B4044AFEE67AA5DB73,
	Shader_Internal_FindPassTagValue_mF5FA12EAC420E6FA37C5812D264D604C24C8ED22,
	Shader_SetGlobalFloatImpl_m34F78C11132FA32ED2550F0C2641FF0E026CF633,
	Shader_SetGlobalVectorImpl_m2741D71F920CDE4F2416EFA61A3B44CA258348B7,
	Shader_SetGlobalMatrixImpl_mA5166FA119796B15645170AF95169C532C75B967,
	Shader_SetGlobalTextureImpl_m8098302027BFD1DC80EB97B82CD1FB97DEC62C20,
	Shader_SetGlobalRenderTextureImpl_mEEB1AEE0DAFE12880EAB7AC7ED52CC7E8608867B,
	Shader_SetGlobalBufferImpl_m4B040860BDA55685EB66B51A8D01D051EFFB6B76,
	Shader_SetGlobalConstantBufferImpl_m2D90DBDD146284348761E022A4850475D71FAFEE,
	Shader_GetGlobalFloatImpl_mFB65A24295950223ADC2BCD45744D43156C438A7,
	Shader_GetGlobalVectorImpl_m76D4FDC07D1B6BD4ADC83B0A217B2057CED58774,
	Shader_GetGlobalMatrixImpl_m5917481374611DD1A940F9EF5761792CBFFD3872,
	Shader_GetGlobalTextureImpl_mC7DABCD2E0637DFF8E176B25537EC4D7F23A5763,
	Shader_SetGlobalFloatArrayImpl_m5A63AB45BA8B47D2C7398243D202662630F89226,
	Shader_SetGlobalVectorArrayImpl_m5693BAD05E34CD0899BD29C70331A0525302C331,
	Shader_SetGlobalMatrixArrayImpl_m2BB06786CE8286F687F606998D89420CD1927F85,
	Shader_GetGlobalFloatArrayImpl_mA794C95DD54B4681E610DD8890816282CBC5BEF1,
	Shader_GetGlobalVectorArrayImpl_mEDA9F74253E7AD8D3239A7633940F7958D581E02,
	Shader_GetGlobalMatrixArrayImpl_m521CAE006DD2389DF1E0B8856002E0ADE87188C4,
	Shader_GetGlobalFloatArrayCountImpl_m897DE0F5133C83F3D7D91C2EC406B2676127502D,
	Shader_GetGlobalVectorArrayCountImpl_m44B36055836F94BB510DAAABDF447A9B95567656,
	Shader_GetGlobalMatrixArrayCountImpl_mE5467D37F5B11CAEC4808F2FE3C971E69C110ED7,
	Shader_ExtractGlobalFloatArrayImpl_m77E1FEE04B921C7CE6F3A3855012E5DDD6E41D9A,
	Shader_ExtractGlobalVectorArrayImpl_m9E5A0047DA98EE4833223AB0247FF260434783D3,
	Shader_ExtractGlobalMatrixArrayImpl_m11B72140F9CCE8D54441219EA94E7B030A18C0B3,
	Shader_SetGlobalFloatArray_m17B396ADAA2740C42092C790FAE3159CD96DBAA6,
	Shader_SetGlobalVectorArray_mC7D6E525AB07A7EFF07F60F8B4528B2E7798B4A8,
	Shader_SetGlobalMatrixArray_mD98B757921B8F2F40D017BA28D51D894FB0CA5E0,
	Shader_ExtractGlobalFloatArray_m05567982FD164CD7B14DDC97F2CFF67E0FBA7F8B,
	Shader_ExtractGlobalVectorArray_m8C602323460F0AAE19135ECA460FDAED67C8B62B,
	Shader_ExtractGlobalMatrixArray_mC77E2FBA3E0E5285E58F187EA30F1E42BCA3C625,
	Shader_SetGlobalFloat_m9020F5CF5BAC69941EB49C6457AAD9BF63D819AE,
	Shader_SetGlobalFloat_mBD7EA0478EDAD67D0AF342A54E97DC8495E5F209,
	Shader_SetGlobalInt_mF3BD98838D66A75CF6090AB1BD541E491B496E55,
	Shader_SetGlobalInt_mE182A541FAEDAB51EA0214AC9C4FCB7447EA3AA4,
	Shader_SetGlobalVector_m831AF6E05F7ADEBE49AC4C6EE043E10B86900B76,
	Shader_SetGlobalVector_m95C42CE1CC56A5BD792C48C60EDD1DF220046566,
	Shader_SetGlobalColor_mF06BC77590D46F1031713BC3FCD614C5E8A0FB18,
	Shader_SetGlobalColor_m5BD0BE3896674DD1DB99770EF300A32CAF25D4A1,
	Shader_SetGlobalMatrix_m4581B7990AB6C854301A072E1A15427117F20D1B,
	Shader_SetGlobalMatrix_mE9F00E107B245DEFB3210F00228F72F2BF2B8112,
	Shader_SetGlobalTexture_m17D5E35DC746DB65488FE0772F87991ADD63486F,
	Shader_SetGlobalTexture_m102985DD1111707707723F56A005BFDE53F42B39,
	Shader_SetGlobalTexture_mB0D3AE5A10E57F8F7921EC5234CCCBFB595557D6,
	Shader_SetGlobalTexture_mF15125C706FA670931BC9943C6832C37748E2214,
	Shader_SetGlobalBuffer_m315E6890B93B2FAE0BD3535F7E2ECF302A399683,
	Shader_SetGlobalBuffer_mACEA0E5EB8C2DAF5DDAC28F464EF3BBB320BD858,
	Shader_SetGlobalConstantBuffer_mA7D527084752F14F6449308BF480B8919996FAB2,
	Shader_SetGlobalFloatArray_m3FBC628C38FEE8E1AA834BF37804E30E9412EBDD,
	Shader_SetGlobalFloatArray_mD90F003C6B47CEF24C739B4069D42C3452FB9502,
	Shader_SetGlobalFloatArray_m4CA746406152C91960BA38BD4FD49BACC7E7862B,
	Shader_SetGlobalFloatArray_m7BFD24A65210FA68D724FBBC004711D85B3ED819,
	Shader_SetGlobalVectorArray_m8FB32F66DD342DBB54F65875C67E4E89713791E2,
	Shader_SetGlobalVectorArray_m83822C9305AFC915419898D4D9D6F0EE720D5E4F,
	Shader_SetGlobalVectorArray_m6269EF99AB8565A62C18E3D6FB90423B26FF0978,
	Shader_SetGlobalVectorArray_m0FD98F7C84F7C1A1D992DE5334ABE24C5B315FD6,
	Shader_SetGlobalMatrixArray_m6E0E4533E809D9298640CB950A34B74F2EB23846,
	Shader_SetGlobalMatrixArray_mBD4D97D8042FD63970C395331FC8A429953147ED,
	Shader_SetGlobalMatrixArray_mA666380B8FA7305C4DD8D83093104F71166984F0,
	Shader_SetGlobalMatrixArray_m65CA9E537955C5086BFC454113F8A40395F12411,
	Shader_GetGlobalFloat_mE940743B54E43F43067FF9F0F10CB69C7B644B45,
	Shader_GetGlobalFloat_m944FF3CD8D141168C4DD599F057CED2542C7DFF8,
	Shader_GetGlobalInt_m47D7011151555FC06703452ED43D0E1183C8F8CC,
	Shader_GetGlobalInt_mD12FAD45C407CE6C847006D4936BFAC3080BF180,
	Shader_GetGlobalVector_mF81172171AECD963E1621761B60F075A686A4E01,
	Shader_GetGlobalVector_m730654D4E436A814FB4561204E9AB6214996B703,
	Shader_GetGlobalColor_m4D2B41A0CA067CDB26C3EED29BDE9725079ABE91,
	Shader_GetGlobalColor_m01C78E5F65C7B1EF0B1306413EA6B09BE3A5B267,
	Shader_GetGlobalMatrix_m2BFF6E202370174A158395805493BB42BB312AFD,
	Shader_GetGlobalMatrix_m89C2CFAFB98BEF2064D4F6B441025CEAB1A59A37,
	Shader_GetGlobalTexture_m36F80A6B474B78FAD2D1F199E1215E7BE6D7C1D8,
	Shader_GetGlobalTexture_m5459D120BE6C734C8736AACF6AF4C5A2C1A9A11D,
	Shader_GetGlobalFloatArray_mEBC1DB9F2AD755F2D9DB208C803E52CAECFBD9F3,
	Shader_GetGlobalFloatArray_m2B08CA26087D0253767995EA78A70E2142DB383D,
	Shader_GetGlobalVectorArray_mDE097DE9F3AF3C1F4F3F48AA37AF587451D01FCC,
	Shader_GetGlobalVectorArray_mF7ADDA7E0198A612656CF773C9B9F2DB1F3714A0,
	Shader_GetGlobalMatrixArray_mD711782B6D5490E6EC7EB9F4FAA3642E37C7590C,
	Shader_GetGlobalMatrixArray_m5C9825252F22927C7FE7C3DB715F28DFDF18FFAA,
	Shader_GetGlobalFloatArray_m6B4DDEC1D47B9EF24A83C9130B3020D9F1FB1ED9,
	Shader_GetGlobalFloatArray_m069F43C4DA01E6BA6A19B842BD62B137E527C291,
	Shader_GetGlobalVectorArray_mF9D1A26BBF4E4AE2CCA32ABB8888E875A6728F5E,
	Shader_GetGlobalVectorArray_mE4C492CCB7832557324304F45E7942AD1FA2B1BC,
	Shader_GetGlobalMatrixArray_m8F5094B02BB72D084FF8DF9FB500A0394DE8E657,
	Shader_GetGlobalMatrixArray_mD196A4D3397F1922D85B9A28995D9B9C8D7DD9EF,
	Shader__ctor_m6D011DE6D578D5F19FBF7EAED326C7209C419E21,
	Shader_GetPropertyName_m09645E39C81AFC9F4CBEA64684106A32CCC24176,
	Shader_GetPropertyNameId_mD26AD80F64D495DCF7F14AD52D0B25BDF4F92C0A,
	Shader_GetPropertyType_m91AB65D05495C47933FE1469979DE7F0EB8ADF8B,
	Shader_GetPropertyDescription_m43E12278DC828BBA8098EB97F9F8278BB821DA22,
	Shader_GetPropertyFlags_m953EAAE229BEEADACA566DD73ADD90B8407E1B83,
	Shader_GetPropertyAttributes_m55EF607DECD7E26AEEC28D0D92FF2044519F36E5,
	Shader_GetPropertyDefaultValue_m979D2AFE7FFC95913F18D5139F38D2B2EC186FD0,
	Shader_GetPropertyTextureDimension_m04A335C5DA03B97B23510A37305212A05F451A7F,
	Shader_GetPropertyTextureDefaultName_mBED1CF1701CA202FD6339DF42A75E339BBD17AE2,
	Shader_CheckPropertyIndex_mECA8A1E1DD1E483E9DD156273FA2AB407CF100FA,
	Shader_GetPropertyCount_m99EF95941D764508220EE397FFB1D94887C04C16,
	Shader_FindPropertyIndex_m8E8AB9B10940D571B8242177CB5C27F561E10C30,
	Shader_GetPropertyName_m791E2E697D4677F72773A5A501DEB192D128AF43,
	Shader_GetPropertyNameId_m6A173B313A51D83BC79833361EFDAC5A52EDD71F,
	Shader_GetPropertyType_mE6A29BF201BC2326D5F6E5A1BE302BC57D1FAEC6,
	Shader_GetPropertyDescription_m89A316AF4485AD548C96A6944C49B0ED8EC0063D,
	Shader_GetPropertyFlags_m9F53F6201CCAE781859332A363C2B2D3C810D8B9,
	Shader_GetPropertyAttributes_mF20D64790DB722D0B3B267B7BBC6DC203B430A95,
	Shader_GetPropertyDefaultFloatValue_m8B64279E4FEE502D446CE91C875CA47C80962277,
	Shader_GetPropertyDefaultVectorValue_m919E33753E58CA74CBEB1CA3B617AB19312A5B2C,
	Shader_GetPropertyRangeLimits_mE336116ADEECCDF0FFE183E2CF197EBF0EEE988E,
	Shader_GetPropertyTextureDimension_mB7AD6DB9EB160248811C6D532243A53D2FF33EE2,
	Shader_GetPropertyTextureDefaultName_m41B3FAD4323DD40EF37D75A9C59B909108FC2B57,
	Shader_SetGlobalVectorImpl_Injected_m163EB9DAE823DD5F7AD16969CA24DA6C2F130D1A,
	Shader_SetGlobalMatrixImpl_Injected_m52914EC8C70103DF40621F04ACD8975DCEBE3E67,
	Shader_GetGlobalVectorImpl_Injected_m9BF4331E49B29A6A756C29792111F2EDFFBAFC10,
	Shader_GetGlobalMatrixImpl_Injected_m94972B58DEDC01DB4BAFD389B81BAA76C7946D6E,
	Shader_GetPropertyDefaultValue_Injected_mB93EBCD6A25DF4D8A2A96F689A2FE13C4C321352,
	Material_Create_m3396EC163FABE66B6F03B800DCCED6BF2FFF2A0C,
	Material_CreateWithShader_m41F159D25DC785C3BB43E6908057BB2BD1D2CB7F,
	Material_CreateWithMaterial_mD3140BCB57EBB406D063C7A7B0B9D1A4C74DA3F2,
	Material_CreateWithString_mCF4047522DD7D38087CF9AF121766E0D69B9BB55,
	Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8,
	Material__ctor_m0171C6D4D3FD04D58C70808F255DBA67D0ED2BDE,
	Material__ctor_m02F4232D67F46B1EE84441089306E867B1788924,
	Material_GetDefaultMaterial_m5FDD53D60389B565ABB231C8B543B625A5534CD3,
	Material_GetDefaultParticleMaterial_m359E39F0DEEBEAE3C4A992BD6C4C57F3928931E9,
	Material_GetDefaultLineMaterial_m11D1657F15317F59623FE9A475D5CDC5D63AA06B,
	Material_get_shader_m9CEDCA4D97D42588C6B827400E364E4A8EC55FF0,
	Material_set_shader_m689F997F888E3C2A0FF9E6F399AA5D8204B454B1,
	Material_get_color_m7CE9C1FC0E0B4952D58DFBBA4D569F4B161B27E9,
	Material_set_color_m127EAC5D3CC68359E72D12A2B3CE7428EFBB81C3,
	Material_get_mainTexture_mE85CF647728AD145D7E03A172EFD5930773E514E,
	Material_set_mainTexture_m0742CFF768E9701618DA07C71F009239AB31EB41,
	Material_get_mainTextureOffset_mCD63885CCB91B4E901FC4847FCCDFDA2E45B3590,
	Material_set_mainTextureOffset_m8602C6F3E7FC358C5F8B9C3709325F676B1ACF5E,
	Material_get_mainTextureScale_m882447FB9D8EF5873279BF15C62AD977EEEFE062,
	Material_set_mainTextureScale_m4E1ED22B62AD02B61921AD392A6D2E5B727383BA,
	Material_GetFirstPropertyNameIdByAttribute_m34991F73A65FE72151842050D18EB3EC8969A8C2,
	Material_HasProperty_m901DE6C516A0D2C986B849C7B44F679AE21B8927,
	Material_HasProperty_m8611FACA6F9D9B2B5C3E92B6D93D2D514B443512,
	Material_get_renderQueue_mDEC48BD94C93FF5A04BC7190E4B5C56BB6E44140,
	Material_set_renderQueue_m02A0C73EC4B9C9D2C2ABFFD777EBDA45C1E1BD4D,
	Material_get_rawRenderQueue_m9AB9F21C8EDB1D90752B432247362985D1A55283,
	Material_EnableKeyword_m7466758182CBBC40134C9048CDF682DF46F32FA9,
	Material_DisableKeyword_m2ACBFC5D28ED46FF2CF5532F00D702FF62C02ED3,
	Material_IsKeywordEnabled_mABE55EDC028A2F95C34D9AE382ADBA9C0DC5B97A,
	Material_get_globalIlluminationFlags_m8BB45CE933E1CFC49BB0F0F46E428FAF13361504,
	Material_set_globalIlluminationFlags_m20C0201EB1CF367A93CE196E186AD784FA4D4F14,
	Material_get_doubleSidedGI_m764A82FDF44DC53C6759D71DDB2463212D9F72DD,
	Material_set_doubleSidedGI_m1216E118D46360F69CE42FFACD4AF44CBAD32B15,
	Material_get_enableInstancing_mCB3E53889074239B33F840D28331AE8262B00E91,
	Material_set_enableInstancing_m745D94DF21B9749DA7CFE42BAB3CBD05F614B81E,
	Material_get_passCount_m790A357C17FB4D829C479652B1B65BC15F73FBB1,
	Material_SetShaderPassEnabled_m301D004116C2DC9B9A63121CB15BEB7D14DB0F68,
	Material_GetShaderPassEnabled_mBB19A00FE5398F28B5340EB7C8D1BF122E9CDFB3,
	Material_GetPassName_m429460022064237971866E6C22C6BAF7AA7CA845,
	Material_FindPass_m2BE935C6343F0F5111CD5DDA8C3A8691BE044180,
	Material_SetOverrideTag_m773D4D6BE26AA7E1200C76571C6DABC4A53002DB,
	Material_GetTagImpl_m74481FC086405BAA8614AFCA56F9B13925B85A19,
	Material_GetTag_m718957E007771A729C627658C6B5A0F3AE99244C,
	Material_GetTag_m0BFC270775E697347173DD872D655B1F2C1FA131,
	Material_Lerp_m48C28B02B82EAC62B06165256564B311FE8802FD,
	Material_SetPass_m4BE0A8FCBF158C83522AA2F69118A2FE33683918,
	Material_CopyPropertiesFromMaterial_mC80A6EB2A756B079CEEDEBD5F03A36A6DD18227A,
	Material_GetShaderKeywords_m786F6539953C2B890A0827715404D3508BFCF685,
	Material_SetShaderKeywords_m32714E17C5D17AAE15927F89624416E14B044A82,
	Material_get_shaderKeywords_mF653034CC23EB4A65580BA4388F7258328C9C90C,
	Material_set_shaderKeywords_m336EBA03D542BE657FEBDD62C7546568CD3081C9,
	Material_ComputeCRC_mE22B08A446771A98DDC744742C6B24BC86CFEA74,
	Material_GetTexturePropertyNames_mCF863B050B213FA4FA16395617ECE4A76B1A9835,
	Material_GetTexturePropertyNameIDs_m48D0C3D9553534CD03E2E17B4DBB3F57E8D26149,
	Material_GetTexturePropertyNamesInternal_m4B3C8464A437ADB50C5BD97AEFAC03837F2B5BC8,
	Material_GetTexturePropertyNameIDsInternal_mEE63AC3015A443983DECC97548BA2227BBA7F359,
	Material_GetTexturePropertyNames_m2CA15F82FDD8631C10FF2B96A57FD468ECFB3527,
	Material_GetTexturePropertyNameIDs_m34FF0C128EE26F2F4C75D3329E3445C53BEF85AA,
	Material_SetFloatImpl_mFD6022220F625E704EC4F27691F496166E590094,
	Material_SetColorImpl_m0779CF8FCCFC298AAF42A9E9FF6503C9FDC43CFE,
	Material_SetMatrixImpl_mAC50A1E655FF577F6AEB3C67F14DAE7D1AA60C4B,
	Material_SetTextureImpl_mBCF204C1FAD811B00DBAE98847D6D533EE05B135,
	Material_SetRenderTextureImpl_m1716331E4D35E53EDEC8D06CA08F69EBEB49FD8E,
	Material_SetBufferImpl_m009669A2D3BD2561A902F7DE49C842F438F31DC9,
	Material_SetConstantBufferImpl_m9C25A7CF0B38DDE1F6CD66C6DA43DAF8F1C4EE38,
	Material_GetFloatImpl_m7F4A1A6FE5D9EA1F5C90CD1476AB819C7BD7D4ED,
	Material_GetColorImpl_m46381ED49C354AEB9FEFCE5BF410A1909E867E76,
	Material_GetMatrixImpl_mB768AE1ABBCC46A8BB1F58BDD850CA6B553F3A0D,
	Material_GetTextureImpl_m19D8CE6C5701AC4B69A6D5354E08240DF6C036D2,
	Material_SetFloatArrayImpl_m80EEFAE52B5E5BC228134BE5B070DC6237A3CDE4,
	Material_SetVectorArrayImpl_mF499FA2995CA112EF3D8290C2B30E9D06A1ED2F9,
	Material_SetColorArrayImpl_mEC4C6D68484BC693CB5115BB0987FBD21FBBA195,
	Material_SetMatrixArrayImpl_m2262E8F2DF3C21B212DE0E7603BF7B0FF86AC2EE,
	Material_GetFloatArrayImpl_m8C0878BD1356C08D7A1AA8003B711D222DA03FE7,
	Material_GetVectorArrayImpl_m1701F9B243640072362E2F9997A0E759A5DE67DB,
	Material_GetColorArrayImpl_mE7740EE7AC1DAC25C74BCD81C97084CFB4388FED,
	Material_GetMatrixArrayImpl_m48B3DB8C758B68519144CABE503697C684BE6091,
	Material_GetFloatArrayCountImpl_m580060E1D5467059B9599796B6301F6A251F8A03,
	Material_GetVectorArrayCountImpl_m8477A1307A8B39BD4DF79BF9658C940A3BD7FEBD,
	Material_GetColorArrayCountImpl_m48D8D79A3C0B191EE2167E5C16A6E7AB0FEA3D2D,
	Material_GetMatrixArrayCountImpl_m5E9AFA151CA70F349A7349F4B9B526C5DB3D827F,
	Material_ExtractFloatArrayImpl_mAE47B7521571D9BCCF9EA757D53CBDC40E636652,
	Material_ExtractVectorArrayImpl_mBF17D3010B08179139F7E82B158E8B8EC03A30EF,
	Material_ExtractColorArrayImpl_m92D16498F69C0D18FC2EAAD10EEBF2940D0B6108,
	Material_ExtractMatrixArrayImpl_m6F51ADC11CF4032A05ACA177180C1087FCAD0834,
	Material_GetTextureScaleAndOffsetImpl_m8946BA8A9E6F974315DBFBAAC9A152BDCA65F3E1,
	Material_SetTextureOffsetImpl_mFA6B03949317E27AA65014030ABE8CA79350FE45,
	Material_SetTextureScaleImpl_mB4BD14FA7A283ACD618A139F0A28D44BE299A89E,
	Material_SetFloatArray_m62969A8282FA26EBA3996A5F9B752761A9C4E096,
	Material_SetVectorArray_m49BD5611E6A2E24B2DB4C69219F7F53739EB413C,
	Material_SetColorArray_m1E9237B662CEA4C70825E4FA36B14B79D627FB51,
	Material_SetMatrixArray_mCFAFBCE28D08046FF2168DEE36A6A68FB184A17C,
	Material_ExtractFloatArray_m196AFA3A023BC2B19D2624C12B01DD833D4DE2B6,
	Material_ExtractVectorArray_m170AA9CDBB231199910B0C75B69A4E6126E2E64D,
	Material_ExtractColorArray_m5E3EA1DD4DF054838FCDD3F37DE83D6DEC12C0C7,
	Material_ExtractMatrixArray_m8FE94720CB4F520050B85B54E78FD0B841429232,
	Material_SetFloat_m4B7D3FAA00D20BCB3C487E72B7E4B2691D5ECAD2,
	Material_SetFloat_mC2FDDF0798373DEE6BBA9B9FFFE03EC3CFB9BF47,
	Material_SetInt_m1FCBDBB985E6A299AE11C3D8AF29BB4D7C7DF278,
	Material_SetInt_m8D3776E4EF0DFA2A278B456F40741E07D83508CD,
	Material_SetColor_mB91EF8CAC3AB3B39D2535BF9F7FECECF3EC2161C,
	Material_SetColor_m48FBB701F6177B367EDFAC6BE896D183EF640725,
	Material_SetVector_m6FC2CC4EBE6C45D48D8B9164148A0CB3124335EC,
	Material_SetVector_m95B7CB07B91F004B4DD9DB5DFA5146472737B8EA,
	Material_SetMatrix_mE3D3FA75DA02FED1B97E2BECA258F45399715764,
	Material_SetMatrix_m6C552C454B30BA42F5972F65403B2EED62D68D81,
	Material_SetTexture_mAA0F00FACFE40CFE4BE28A11162E5EEFCC5F5A61,
	Material_SetTexture_m4FFF0B403A64253B83534701104F017840142ACA,
	Material_SetTexture_mBB98BDF71749072D53B4A6426C23335EAF30987D,
	Material_SetTexture_m5F70C70649242D4DA11A933421638BBC45A721D5,
	Material_SetBuffer_mD6D6FCAB0A0EB5BEB3A40672198D858670E57A5C,
	Material_SetBuffer_m9597D87FEE583129ECB9380D03FF76051A2C99BE,
	Material_SetConstantBuffer_mAA20E3E0FF3C045A8F6337FA6C1D8A2503DF069A,
	Material_SetConstantBuffer_m71B4947519DE0A36AC849D062D16FDF1BB47F5BD,
	Material_SetFloatArray_m20E65D81B203E7BDACC5AE46A4945330419A5A19,
	Material_SetFloatArray_m5D9D21D6ADC910228EC58E272E7EAECEF54F0EF8,
	Material_SetFloatArray_m826883FAC1B827A065735E70E7B6A706BCE695FA,
	Material_SetFloatArray_m03E524B5B1CA096EEC2B0404AE61B7F4F5FACF3C,
	Material_SetColorArray_m76B75E0F814FF7BEBF2436D62F30D27F1B1AB118,
	Material_SetColorArray_mE1CF05ACE8AC659BC701181F1A9EF924D224E373,
	Material_SetColorArray_m0C7FB3A0F21A1DCD4EEFE47B504AD10D21D005C0,
	Material_SetColorArray_mF8B250D1A05705B6781DC7725A977A94D28C2AFA,
	Material_SetVectorArray_mC73E3C65E0CF82746C9BECC0376522F0722DC178,
	Material_SetVectorArray_mC1CC338EA86D2D80606CE3B9DC58E2CC415788F2,
	Material_SetVectorArray_mB29351C346C21AB801523B92A277B4A820583D51,
	Material_SetVectorArray_m3DEE0383284B478E6A261A485DEEB09F9F63B51C,
	Material_SetMatrixArray_mC7F439E54B8AB183073087D09213EEAB40282F6E,
	Material_SetMatrixArray_m8B3BD07E05515B2A06C7138B92BCB35EE5B7CA8D,
	Material_SetMatrixArray_m5866D8DDFA81DD15B63F83949FDDEED0E304CCC8,
	Material_SetMatrixArray_mB9988E11FFDBFCC71DCE75433F2E745C77BB34FF,
	Material_GetFloat_m8A4243FC6619B4E0E820E87754035700FD4913F0,
	Material_GetFloat_mC1764F8B39FD31C6B7629D417BC8375D6E6AC60C,
	Material_GetInt_mE2577C1C53AB98AF3FEE2BC95F04EC5694933F41,
	Material_GetInt_m7D7C0ECB5A4D5CE5F1F409D04B4F886930271DEC,
	Material_GetColor_m95E13C52410F7CE01E8DC32668ABBCBF19BB8808,
	Material_GetColor_m1FFF5ECB4967771D751C60205635A0D3F7AA2F0E,
	Material_GetVector_m068E8828C9E9FB017161F018E80818C2D1651477,
	Material_GetVector_mDE1F87F355E0ECAD883E4F2CF62DB338C2E00C38,
	Material_GetMatrix_mAB53222644DC5DAD4A9E9628C42B954A272C0CC8,
	Material_GetMatrix_m685F5AE4EAFE5F49CC1791AFF06707433C70F1BC,
	Material_GetTexture_mCD6B822EA19773B8D39368FF1A285E7B69043896,
	Material_GetTexture_mDB1B89D76D44AD07BD214224C59A6FE0B62F6477,
	Material_GetFloatArray_mF4B422C881673975D135D0A4256FFF550FB46C4E,
	Material_GetFloatArray_m7D42AC14400F26D355FFDDDDED4E8AB2F90DC825,
	Material_GetColorArray_mD4332AD6BB02C0913C51E0EAA7A833533EF4B189,
	Material_GetColorArray_mC0A372A8EC7DBB7C2A891A3DA469826C83A27022,
	Material_GetVectorArray_m9BF6FC74660F816FC52A7F1D8A10A75D142A561D,
	Material_GetVectorArray_m78CFEA0BAB12B3E80987E76D88962CBA2E231C20,
	Material_GetMatrixArray_m0227057101B57D50DDB8AEE536C88D37824309E4,
	Material_GetMatrixArray_mC644ED51D04D128D88CF7FB12FBAC5C392EE51D8,
	Material_GetFloatArray_m8C0AFD6B92559EFDCE70027D4FF3621CB004D845,
	Material_GetFloatArray_mE671829DD1EDB93EACDC7BB19AF6404CAB4FB30A,
	Material_GetColorArray_mD9B2A59966A9B8D50F0D3D3E5F0AAFE4EFD5EB3F,
	Material_GetColorArray_mC9EA848EDD5120DB3689ED9D84475F7E2850B7B5,
	Material_GetVectorArray_m62C72E37A866CE06429764E4741B568C10BDAFBF,
	Material_GetVectorArray_m777DE57187F9E93BD9E04995C388926582A4B209,
	Material_GetMatrixArray_mDBF02CC3EBE3958272A1C316B0FEC430CE2E9AA9,
	Material_GetMatrixArray_m3B607490006A830269AFC72245C2AE9431AE89F2,
	Material_SetTextureOffset_mAF1B6A6AD9E952262F7AE80B17B31D8FF720ADE7,
	Material_SetTextureOffset_m3E9C91ED11CB6323252AF6B5CB15CCD497EF10BB,
	Material_SetTextureScale_m9D9C2ADD50088A1712891A6A2AAAAFA734703BBF,
	Material_SetTextureScale_m30A521475E4FF45858D27B8DA562B06165734A9E,
	Material_GetTextureOffset_m3C0722B8C77DF728586E121E4DD81AED28097789,
	Material_GetTextureOffset_m5441F649AF03115B34C2EA4C8C84A068823F9E39,
	Material_GetTextureScale_m2B1FFB0B67C22C3544E2E66E3E0D5B8DEE7EA859,
	Material_GetTextureScale_m3AD3DBFC4E7F8546BCFA10BE56FD794C64635D9E,
	Material_SetColorImpl_Injected_mEE762DBD0B37ACA313061179B3CB767B59E4B0FB,
	Material_SetMatrixImpl_Injected_mD2486E408D7547B08CC36A2DE70520F97A68529C,
	Material_GetColorImpl_Injected_m22325A32C4B5AAEA791AED50195F66CDBAE93C82,
	Material_GetMatrixImpl_Injected_m42181501EB254B8D21F950998CD679A39EBFC1FF,
	Material_GetTextureScaleAndOffsetImpl_Injected_m5DDD8AB08DCF6A52415FB8D76A089CC886425E54,
	Material_SetTextureOffsetImpl_Injected_m85850F975A45BBB3AEA145C52309FC7CB82CDF64,
	Material_SetTextureScaleImpl_Injected_m44A1C509E1EA40BFAFA82C7449EC21E6D39E8BF9,
	Light_get_type_m24F8A5EFB5D0B0B5F4820623132D1EAA327D06E3,
	Light_get_spotAngle_m1EAB341E449675D41E86A63F047BF3ABE2B99C3B,
	Light_get_color_m7A83B30FE716A1A931D450A6035A0069A2DD7698,
	Light_set_color_mCC1582CECB03F511169444C9498B7FFBED8EAF63,
	Light_get_intensity_m4E9152844D85D03FEDA5AE4599AFAFC3C66EFF23,
	Light_set_intensity_mE209975C840F1D887B4207C390DB5A2EF15A763C,
	Light_get_bounceIntensity_m9185F30A7DED7FB480B1026B2CC6DBA357FAE9A7,
	Light_get_range_m3AA5F21DB9441E888A0E89C465F928445150061A,
	Light_set_range_mA08B30B1776471973285722FCB7633870B23106B,
	Light_get_shadows_m8FBBEDB8C442B0426E5D3E457330AA81D764C8F2,
	Light_get_shadowStrength_mB46E58530AF939421D198670434F15D331BFB672,
	Light_set_shadowStrength_m447AC9B66C28129AA3B2D62AE918FB511E27E2FE,
	Light_get_color_Injected_m266FA6B59B6A43AE5C2717FE58D91E93626D48DE,
	Light_set_color_Injected_mA6F1C470D37DF5D6F1D2AA5919C5EC989A28CD0A,
	MeshFilter_DontStripMeshFilter_m7FBA33F8214DB646F74E00F7CEFFDF2D0018004C,
	MeshFilter_set_sharedMesh_mFE8D12C35148063EB287951C7BFFB0328AAA7C5D,
	MeshRenderer_DontStripMeshRenderer_mC5359CA39BA768EBDB3C90D4FAE999F4EB1B6B24,
	Mesh_Internal_Create_mF1F8E9F726EAC9A087D49C81E2F1609E8266649E,
	Mesh__ctor_m3AEBC82AB71D4F9498F6E254174BEBA8372834B4,
	Mesh_GetIndicesImpl_m2118C6CA196093FD19BB05E5258C0DCE06FEAD30,
	Mesh_SetIndicesImpl_m05B1C648F5E02990B89D1FFF002F5D5672779D8B,
	Mesh_PrintErrorCantAccessChannel_m2E8A25959739B006557A94F7E460E8BE0B3ABB19,
	Mesh_HasVertexAttribute_m87C57B859ECB5224EEB77ED9BD3B6FF30F5A9B1C,
	Mesh_SetArrayForChannelImpl_m6FDE8B55C37DC1828C0041DDA3AD1E6D2C028E6C,
	Mesh_GetAllocArrayFromChannelImpl_mFDE324953466F6DBAC14CD4B48105B62181CC399,
	Mesh_get_canAccess_m1E0020EA7961227FBC0D90D851A49BCF7EB1E194,
	Mesh_get_subMeshCount_m6BE7CFB52CE84AEE45B4E5A704E865954397F52F,
	Mesh_get_bounds_m15A146B9397AA62A81986030D289320EDE4B3037,
	Mesh_set_bounds_mB09338F622466456ADBCC449BB1F62F2EF1731B6,
	Mesh_ClearImpl_mFFD3A4748AF067B75DBD646B4A64B3D9A2F3EE14,
	Mesh_RecalculateBoundsImpl_m43DE3DEFC2DADC090DC6FD2C27F44D6DBB88CEF6,
	Mesh_MarkDynamicImpl_m439AACE205D2202AD4E5D4A12191F4CCCC031876,
	Mesh_GetUVChannel_m15BB0A6CC8E32867621A78627CD5FF2CAEA163CC,
	Mesh_DefaultDimensionForChannel_mF943AF434BB9F54DBB3B3DE65F7B816E617A89C9,
	NULL,
	NULL,
	Mesh_SetSizedArrayForChannel_mBBB7F5AE4D5A5A6D5794742745385B02B13E082D,
	NULL,
	NULL,
	NULL,
	NULL,
	Mesh_get_vertices_m7D07DC0F071C142B87F675B148FC0F7A243238B9,
	Mesh_set_vertices_mC1406AE08BC3495F3B0E29B53BACC9FD7BA685C6,
	Mesh_get_normals_m3CE4668899836CBD17C3F85EB24261CBCEB3EABB,
	Mesh_set_normals_m4054D319A67DAAA25A794D67AD37278A84406589,
	Mesh_get_tangents_mFF92BD7D6EBA8C7EB8340E1529B1CB98006F44DD,
	Mesh_set_tangents_mE66D8020B76E43A5CA3C4E60DB61CD962D7D3C57,
	Mesh_get_uv_m0EBA5CA4644C9D5F1B2125AF3FE3873EFC8A4616,
	Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8,
	Mesh_get_uv2_m3E70D5DD7A5C6910A074A78296269EBF2CBAE97F,
	Mesh_set_uv2_m8D37F9622B68D4CEA7FCF96BE4F8E442155DB038,
	Mesh_get_uv3_mC56484D8B69A65DA948C7F23B06ED490BCFBE8B0,
	Mesh_get_uv4_m1C5734938A443D8004339E8D8DDDC33B3E0935F6,
	Mesh_get_colors32_m24C6C6BC1A40B7F09FF390F304A96728A4C99246,
	Mesh_set_colors32_m29459AFE2843D67E95D60E5A13B6F6AF7670230C,
	Mesh_SetVertices_m5F487FC255C9CAF4005B75CFE67A88C8C0E7BB06,
	Mesh_SetVertices_mCFFD77B857B3041D407D4EB6BEA3F4FC8F258655,
	Mesh_SetNormals_m76D71A949B9288FA8ED17DDADC530365307B9797,
	Mesh_SetNormals_m2D6A6379F1C1E974C155DFD0E59812316B17F6B2,
	Mesh_SetTangents_m6EEAB861C9286B1DA4935B87A883045ADD3955E5,
	Mesh_SetTangents_m2C28E9ED06F035D4F726DA8F8782F75B81AF1BF9,
	Mesh_SetColors_m237E41213E82D4BB882ED96FD81A17D9366590CF,
	Mesh_SetColors_m7F5D50EA329435A03C41DCF4A6CFAB57504FAAAF,
	NULL,
	Mesh_SetUVs_m0210150B0387289B823488D421BDF9CBF9769116,
	Mesh_SetUVs_m5E152C09C814455B8A8FB6672ACD15C0D6804EC5,
	Mesh_PrintErrorCantAccessIndices_mA45D3609288655A328AEB0F2F436403B1ECE5077,
	Mesh_CheckCanAccessSubmesh_mF86EBD1C9EC3FE557880FA138510F7761585D227,
	Mesh_CheckCanAccessSubmeshTriangles_m214B5F30F7461C620D03F10F6CF1CAF53DBEE509,
	Mesh_CheckCanAccessSubmeshIndices_m81DF83B1676084AF0B70A36BC7621ADB08430722,
	Mesh_set_triangles_m143A1C262BADCFACE43587EBA2CDC6EBEB5DFAED,
	Mesh_GetIndices_m2FD8417547E7595F590CE55D381E0D13A8D72AA5,
	Mesh_GetIndices_m4260DCF1026449C4E8C4C40229D12AF8CAB26EAF,
	Mesh_CheckIndicesArrayRange_m6DEDA2715437F79DAC8BD7F818B5B2DBAD9BFEBE,
	Mesh_SetTrianglesImpl_m0AD2B3D12113B5E82AC9E29D2C6308E7410E2B98,
	Mesh_SetTriangles_m6A43D705DE751C622CCF88EC31C4EF1B53578BE5,
	Mesh_SetTriangles_m39FB983B90F36D724CC1C21BA7821C9697F86116,
	Mesh_SetTriangles_mBD39F430A044A27A171AD8FD80F2B9DDFDB4A469,
	Mesh_SetIndices_m18C0006CF36C43FF16B1917099E2970C2D4145BD,
	Mesh_SetIndices_mFE6F2769EF170F0CD84C739E96976A15052B7024,
	Mesh_SetIndices_m5E72EA4BED8BA7B0732E80227199026B1CD9B6C5,
	Mesh_Clear_mB750E1DCAB658124AAD81A02B93DED7601047B60,
	Mesh_RecalculateBounds_m1BF701FE2CEA4E8E1183FF878B812808ED1EBA49,
	Mesh_MarkDynamic_m554A9D7214E30E4F8F0515B712F3E3FA6D695A5F,
	Mesh_get_bounds_Injected_mA4338C67E67FC449E1344556D073855ACB4E9DED,
	Mesh_set_bounds_Injected_mFA253839FEEC7CDF976FE740CA3C2712F491BB8B,
	Texture__ctor_m19850F4654F76731DD82B99217AD5A2EB6974C6C,
	Texture_GetDataWidth_m862817D573E6B1BAE31E9412DB1F1C9B3A15B21D,
	Texture_GetDataHeight_m3E5739F25B967D6AF703541F236F0B1F3F8F939E,
	Texture_get_width_mEF9D208720B8FB3E7A29F3A5A5C381B56E657ED2,
	Texture_set_width_m9E42C8B8ED703644B85F54D8DCFB51BF954F56DA,
	Texture_get_height_m3A004CD1FA238B3D0B32FE7030634B9038EC4AA0,
	Texture_set_height_m601E103C6E803353701370B161F992A5B0C89AB6,
	Texture_get_isReadable_mFB3D8E8799AC5EFD9E1AB68386DA16F3607631BD,
	Texture_get_wrapMode_mC21054C7BC6E958937B7459DAF1D17654284B07A,
	Texture_get_texelSize_m89BA9E4CF5276F4FDBAAD6B497809F3E6DB1E30C,
	Texture_ValidateFormat_m23ED49E24864EE9D1C4EF775002A91EE049561B1,
	Texture_ValidateFormat_mA62E75B693BFABECB7CB732C165139B8492DE0ED,
	Texture_CreateNonReadableException_m66E69BE853119A5A9FE2C27EA788B62BF7CFE34D,
	Texture__cctor_m63D3A3E79E62355737DCF71786623D516FAA07E1,
	Texture_get_texelSize_Injected_m812BEA61C30039FF16BE6A2E174C81DCB40000DE,
	Texture2D_get_format_mF0EE5CEB9F84280D4E722B71546BBBA577101E9F,
	Texture2D_get_whiteTexture_mF447523DE8957109355641ECE0DD3D3C8D2F6C41,
	Texture2D_get_blackTexture_mCF4F978DF9B6066794E7130E0C14618216ED0956,
	Texture2D_get_redTexture_mFE5BD4C3DD841A5AE75164C35298587865C861EC,
	Texture2D_get_grayTexture_m5E3FE2355FEBF8C9B6286645B4EA32E85C401A45,
	Texture2D_get_linearGrayTexture_m5FE6C178A51E5F0AEF92D7D6C7B6EC8C152D257F,
	Texture2D_get_normalTexture_mD22C382CEC787AED904CFA73E64609DEA3D2B9B2,
	Texture2D_Compress_m3D0191F151DF6D66F312D35FF76BDDE92A3ED395,
	Texture2D_Internal_CreateImpl_mE77AB26318BC128ABD08D138FD3EF3A9954F8A5C,
	Texture2D_Internal_Create_mC33D3C13F046ADDAF417534E99A741FCF96538B1,
	Texture2D_get_isReadable_mCF446A169E1D8BF244235F4E9B35DDC4F5E696AD,
	Texture2D_ApplyImpl_mEFE072AD63B1B7B317225DAEDDB1FF9012DB398B,
	Texture2D_ResizeImpl_m92009FC5D076C295BA1DECBA9F4A21A678A425C3,
	Texture2D_SetPixelImpl_m62B6A2252516D1D5E09701BA550D72ACB79E328B,
	Texture2D_GetPixelImpl_mFC45AE4B9B8B88E595CF6EE0FFF751682A70B76D,
	Texture2D_GetPixelBilinearImpl_m950AB40E4151F10B6AA6C5759903BA07348114FB,
	Texture2D_ResizeWithFormatImpl_mA7D86CB3F93C5B34F4F728C3AF4BABC9FC8C42BC,
	Texture2D_ReadPixelsImpl_m8D38B7E5F239AD6A9BE8B7732A9CB5ABF35CD0EA,
	Texture2D_SetPixelsImpl_m8102FB784092E318608E2BC2BB92A82D1C0C9AD1,
	Texture2D_LoadRawTextureDataImpl_mFE1F3F88739E577DD302C4BD1E865DFF08516DA8,
	Texture2D_LoadRawTextureDataImplArray_m57FC49A2675C72881C9AD1FE27E96277F892E70F,
	Texture2D_SetPixelDataImplArray_mD40E91DCFADDE9FDC31E1CEB6AE92E872FDF8027,
	Texture2D_SetPixelDataImpl_m26A489526F9844C7F5BB5571C532157F3F372472,
	Texture2D_GetWritableImageData_m68888C2D5A3E017101E4C8DE6538D5031034DAFF,
	Texture2D_GetRawImageDataSize_m1CA6CE6DF0120CD36211E07896448A4CD2DE7F10,
	Texture2D_GenerateAtlasImpl_mF9ED8E83B1540A1B142109DC0AFF2570D5B58A1F,
	Texture2D_get_isPreProcessed_mFB94271FD8FE6F56FE6866FE02288928840E0250,
	Texture2D_get_streamingMipmaps_m78EB1DE0F6E0FCD92969212C0EB98795ECC991E8,
	Texture2D_get_streamingMipmapsPriority_mC2143F53438E36E24E36A9DF6CC41EDEECF13754,
	Texture2D_get_requestedMipmapLevel_mB3AF7B1155A3D96A9E24EC0405FD6A2C290B632A,
	Texture2D_set_requestedMipmapLevel_mBA1AA7ACBBEDEA8444570529EC1593C46532D6CF,
	Texture2D_get_minimumMipmapLevel_m1DD6F0FD301819B0F019C4A4CFE8209D91E91122,
	Texture2D_set_minimumMipmapLevel_mBF3942CAB2C76B2D266F94CEAE38039C364AF0E2,
	Texture2D_get_loadAllMips_mD01E6A5A83E30C7351FBAB2105343722E24F1DBB,
	Texture2D_set_loadAllMips_m2C10D1DDA8C72B026A61AB8F4083791AD409498C,
	Texture2D_get_calculatedMipmapLevel_m7D1062D26A6428FFB63A1D60C7E52BF6F6655959,
	Texture2D_get_desiredMipmapLevel_mC18FC94ABAD4C2673967DF7D9EDB230474DB27A0,
	Texture2D_get_loadingMipmapLevel_m91264A61AAF4C34A67241B8EE8170E15C21167C6,
	Texture2D_get_loadedMipmapLevel_m8EFC1E5785D2CBAF8223927DD0E40FF9FF3A4B88,
	Texture2D_ClearRequestedMipmapLevel_m3C04AADC29D99C5987E97FCF93C114DCED1335B6,
	Texture2D_IsRequestedMipmapLevelLoaded_m4270351626122999CFB097181E7726DE7E641C22,
	Texture2D_ClearMinimumMipmapLevel_m03B34D86447F0F423C01B471E466999A27F88BF2,
	Texture2D_UpdateExternalTexture_m8A3486B0779454739624888EFD6D9EC42D30874F,
	Texture2D_SetAllPixels32_m7A55BBBC1A890DCFC167F1BB4335F3EA5EEB75D9,
	Texture2D_SetBlockOfPixels32_m884C7DFC283BD8B383B7FF611453DBD93215C4C4,
	Texture2D_GetRawTextureData_m387AAB1686E27DA77F4065A2111DF18934AFB364,
	Texture2D_GetPixels_mBCF24F58376C64F7326040FFEBD6234D7F8FDE6F,
	Texture2D_GetPixels_mE12FF280416D83D18B81E07F04BD1080A0F5009D,
	Texture2D_GetPixels32_mD84F05C717774887B5EF65ADEE82045A3E2FBD75,
	Texture2D_GetPixels32_m7CC6EC6AD48D4CD84AF28DFDFBE24750900FA2E6,
	Texture2D_PackTextures_mAC9D9D58AD2F653BA79CCAEC4D92ECE29068435F,
	Texture2D_PackTextures_m6ACE150804F05ED53F6A75134EF853E7A571DC65,
	Texture2D_PackTextures_m30EDF78F44C0058966BA40BFD5594D8501E4235A,
	Texture2D__ctor_m33959787D1B4602D4D0487F25660935182076C64,
	Texture2D__ctor_mA9BD76C2123DF372D9D66C03DD6F65F114FA35BC,
	Texture2D__ctor_m11E5D67EEE90F8C673E8E0E3A4C41FFE10FCC7EB,
	Texture2D__ctor_m25840EA9858514192E0E00B5D9CA27B83487903C,
	Texture2D__ctor_mB33D5D6E83136BF8A42D2628405B10BE0889F439,
	Texture2D__ctor_mD7A145588D9FEBCF1B435351E0A986174CFC0266,
	Texture2D__ctor_m01B7AF7873AA43495B8216926C1768FEDDF4CE64,
	Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A,
	Texture2D__ctor_m0C86A87871AA8075791EF98499D34DA95ACB0E35,
	Texture2D_CreateExternalTexture_m292548AA116AA5176E8CA03AFBBC088E3B698DE4,
	Texture2D_SetPixel_m8BE87C152447B812D06CB894B3570269CC2DE7C3,
	Texture2D_SetPixel_m9027565ACB3CFE68ED2475761FDE3EF560600B38,
	Texture2D_SetPixels_mC768DC908606FE162BF77A8761AD3ED7BAC1612C,
	Texture2D_SetPixels_m09295F6D03DF7A4A0EC0A12325A59065365D6F53,
	Texture2D_SetPixels_mE261D2D072DB4CF447A259731E51291267EEA3AC,
	Texture2D_SetPixels_mDE50229135F49F323D265340C415D680CCB2FB92,
	Texture2D_GetPixel_m71EA79FE268858ECD782327F8854EF2C2B16B2CD,
	Texture2D_GetPixel_m57F166AE1F42E3FFB882C28D0241B2194B258ABF,
	Texture2D_GetPixelBilinear_m3E0E9A22A0989E99A7295BC6FE6999728F290A78,
	Texture2D_GetPixelBilinear_m66ABE9FF3BA0B835119F04880D80F79596B34DF2,
	Texture2D_LoadRawTextureData_m7F70DFFDBFA1E307A79838B94BC1A0438E063DFC,
	Texture2D_LoadRawTextureData_m0331275D060EC3521BCCF84194FA35BBEEF767CB,
	NULL,
	NULL,
	NULL,
	NULL,
	Texture2D_Apply_mCC17B1895AEB420CF75B1A50A62AB623C225A6C1,
	Texture2D_Apply_m368893ECE2F9659BDA54ED1E4EB00D01CC2D1B16,
	Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA,
	Texture2D_Resize_m9B3D67DF42A47D5C573D084C080CEDA5E330A8D2,
	Texture2D_Resize_m3A64BAF31DF4A82284B56C15614CE4FDA0785F7D,
	Texture2D_ReadPixels_m5664E184458C64BA89450F80F47705A2241E9BFE,
	Texture2D_ReadPixels_m1ED1C11E41D0ACC8CFCABBD25946CF0BD16D4F61,
	Texture2D_GenerateAtlas_mA850247B011DCAD8A29C51B3B3C170F7DB3C49FC,
	Texture2D_SetPixels32_m22195EEABA78058324C83EBF327A7E5EFBEE809F,
	Texture2D_SetPixels32_m8669AE290D19897A70859BE23D9A438EB7EDA67E,
	Texture2D_SetPixels32_m0FD3D2A4909D3948A389AC65EC052156C34A62D8,
	Texture2D_SetPixels32_m515E77577D02B697645BD64D6E6AF32646B7975B,
	Texture2D_GetPixels_m12211168B2F47A4D4F920E8D52B04509BBA67006,
	Texture2D_GetPixels_mE87C4C2438D7DE39C50EC1C91E438BB15026BBE3,
	Texture2D_SetPixelImpl_Injected_m21934648C4DE2227463F16A67EFA3DC740682ACC,
	Texture2D_GetPixelImpl_Injected_mA54A70585DC14F8F87449E931121507ADABA2CE6,
	Texture2D_GetPixelBilinearImpl_Injected_m120BD9810D176C39E874FFDAF53AD3AC3B6ADF85,
	Texture2D_ReadPixelsImpl_Injected_m8D0C78900FF4C61ED9D6DAF548C3F397FB55D91D,
	Cubemap_Internal_CreateImpl_m2E42502B311D4511987453F591F469EFD3D46C7E,
	Cubemap_Internal_Create_m7D1672F9247A6CA2578874A69C901311B6196289,
	Cubemap_get_isReadable_m97956094F4DBC9C67A86AEC8CCE73AB237694121,
	Cubemap__ctor_mA198007748E1B40309793BFD41C6DA8506BFC36E,
	Cubemap__ctor_mC713C6EC5AA4BB7091AF19FC75E1A5D3A133550B,
	Cubemap__ctor_m823CBFD84E8497FEEDE6858F1781ADECB0C6CFBF,
	Cubemap__ctor_mDEAB11F63268FC5F1115D928499AC270F21FB249,
	Cubemap__ctor_mFC82AF58FF4875D6750838AF47A05D5B203523A8,
	Cubemap__ctor_m619C9524BF966423D2DE66E878C824113616C371,
	Cubemap__ctor_mB0430DC19209C90736915B41A670C7AC65698D71,
	Texture3D_get_isReadable_m2793D34A645AA2A88B903D7C0CBC05BC180A489F,
	Texture3D_Internal_CreateImpl_m6B7F1E0A4F0A8DF201C84B8F5EBE88F5BC2D0ED5,
	Texture3D_Internal_Create_mCE8880719B54D2E8426234438DF2BA893B16CAA5,
	Texture3D__ctor_m3819CE6527C761C3514E46566BAE8D09CEE6C6C0,
	Texture3D__ctor_m080D4201C72C73ECB718F44491858309CDCCBF40,
	Texture3D__ctor_m9FB382B0BC5C568B195C9E27E7BCA34C108F5FF7,
	Texture3D__ctor_m13ADB707E9EA6970AE5D6A5DDF8A5DAAD88DD2B0,
	Texture3D__ctor_m7086160504490544C327FF1C7823830B44441466,
	Texture2DArray_get_isReadable_mB2E454ED94BB334E77B42E6CD9DABC0B1D679C1A,
	Texture2DArray_Internal_CreateImpl_mF6D0DD31CE06DB61D0E3C8D875F20692B33C776E,
	Texture2DArray_Internal_Create_m53A30DE93DCDA75588C999A967F8004AB0EE113F,
	Texture2DArray__ctor_m92A39957ECC1DBE79437D3849A1FA7A98615A9F0,
	Texture2DArray__ctor_mD92521FF6DA05FF47471B741DDC7E4D5B3C3F4E2,
	Texture2DArray__ctor_mF94531ED3A27A6583DCACE742D6D6A56C3B1CB76,
	Texture2DArray__ctor_m982669D0408998D2038575A421440A8D537D67E6,
	Texture2DArray__ctor_mEDE73B65A89EACA4B487FFBA92B155ED5B09970F,
	Texture2DArray__ctor_mE0F6B7F60470C479258E1CC295456BCA103E66BF,
	CubemapArray_get_isReadable_mBE24F088422FA9FE007086C36C7D16A6D6377919,
	CubemapArray_Internal_CreateImpl_mDA1FF7D490A441C86198448B72B62C2D38B9A046,
	CubemapArray_Internal_Create_m2503EFCE0A71CBCCCA87C93E15B9F83709274A58,
	CubemapArray__ctor_m44E378D2D09F711CF0AEF479DC7D12426C449CF6,
	CubemapArray__ctor_m390539598EAAEE1AAE0B89D2241A60EE6BD1B219,
	CubemapArray__ctor_m88D0AB083EEF112A636EE307337BAFAF036E0A2B,
	CubemapArray__ctor_m1FC2738B93636229EC645E15D36C9A3F67FE0E54,
	CubemapArray__ctor_mE9E5A417064CB9CF4283C8A82F4AE5C463C4014E,
	CubemapArray__ctor_mD52A7D884A01A8DF05B40D820584C1F3869317AC,
	RenderTexture_get_width_m246F1304B26711BE9D18EE186F130590B9EDADDB,
	RenderTexture_set_width_m10EF29F6167493A1C5826869ACB10EA9C7A5BDCE,
	RenderTexture_get_height_m26FBCCE11E1C6C9A47566CC4421258BAF5F35CE6,
	RenderTexture_set_height_m4402FEFDF6D35ABE3FBA485FE9A144FB4204B561,
	RenderTexture_set_graphicsFormat_mF60798F07D5994C46C53826F8A8F2E553B8F85CF,
	RenderTexture_SetSRGBReadWrite_mD553522060790CB4984886D2508B79897C3DC2DE,
	RenderTexture_Internal_Create_m924B30E7AFD36150F0279057C3A3869D94D7646A,
	RenderTexture_SetRenderTextureDescriptor_mF584353E0834F202C955EAC6499CBD0C6A571527,
	RenderTexture_GetDescriptor_mBDAE7C44038663205A31293B7C4C5AE763CD1128,
	RenderTexture_set_depth_mD4BCB0A9251B7FCF570459A705E03FFFEA4DB3B0,
	RenderTexture__ctor_mFB50DBD262C99B38016F518AA0FBF753FE22D13A,
	RenderTexture__ctor_mEC30DF610263D5A16EC16E34BD86AD4BC0C87A9B,
	RenderTexture__ctor_m3B3534A6C9696C5CB12ADC78F922237F69CEA33A,
	RenderTexture__ctor_mBAE127BF530990C9C8DBF5E155BA05A068777129,
	RenderTexture__ctor_m7C2F727F747019FC14CF7FB5FF5C29A349F9FCD9,
	RenderTexture__ctor_m2DD279268931EC6E725CAB584DC7D499E4BDCB82,
	RenderTexture__ctor_m32060CA5A5C306C485DB6AF9B9050B2FF2AB3A4C,
	RenderTexture__ctor_m0FF5DDAB599ED301091CF23D4C76691D8EC70CA5,
	RenderTexture__ctor_mB54A3ABBD56D38AB762D0AB8B789E2771BC42A7D,
	RenderTexture__ctor_mDB3D67FD662C79D27F0C42C6DBA1A7EFF80C2D1E,
	RenderTexture_get_descriptor_m67E7BCFA6A50634F6E3863E2F5BA1D4923E4DD00,
	RenderTexture_set_descriptor_m88048ECD2E447B3549D59032F5F9B52D4CA3D528,
	RenderTexture_ValidateRenderTextureDesc_mE4BA319BF91FCA90B517EF080E84EE395A4EAD01,
	RenderTexture_GetCompatibleFormat_m1C84A2875CC17182DEB4CC7EF277E7089B160095,
	RenderTexture_SetRenderTextureDescriptor_Injected_m72AC0A28D6BF9041721D95E43BAC302A56C99019,
	RenderTexture_GetDescriptor_Injected_m9A137437A3EAD31E2AE4BC123329BF3945B22A64,
	RenderTextureDescriptor_get_width_m225FBFD7C33BD02D6879A93F1D57997BC251F3F5,
	RenderTextureDescriptor_set_width_m48ADD4AB04E8DBEEC5CA8CC96F86D2674F4FE55F,
	RenderTextureDescriptor_get_height_m947A620B3D28090A57A5DC0D6A126CBBF818B97F,
	RenderTextureDescriptor_set_height_mD19D74EC9679250F63489CF1950351EFA83A1A45,
	RenderTextureDescriptor_get_msaaSamples_mEBE0D743E17068D1898DAE2D281C913E39A33616,
	RenderTextureDescriptor_set_msaaSamples_m5856FC43DAD667D4462B6BCA938B70E42068D24C,
	RenderTextureDescriptor_get_volumeDepth_mBC82F4621E4158E3D2F2457487D1C220AA782AC6,
	RenderTextureDescriptor_set_volumeDepth_mDC402E6967166BE8CADDA690B32136A1E0AB8583,
	RenderTextureDescriptor_set_mipCount_m98C1CE257A8152D8B23EC27D68D0C4C35683DEE5,
	RenderTextureDescriptor_get_graphicsFormat_mD2DD8AC2E1324779F8D497697E725FB93341A14D,
	RenderTextureDescriptor_set_graphicsFormat_mF24C183BA9C9C42923CEC3ED353661D54AA741CA,
	RenderTextureDescriptor_get_depthBufferBits_m51E82C47A0CA0BD8B20F90D43169C956C4F24996,
	RenderTextureDescriptor_set_depthBufferBits_mED58A8D9643740713597B0244BF76D0395C1C479,
	RenderTextureDescriptor_set_dimension_mBC3C8793345AC5E627BDD932B46A5F93568ACCE5,
	RenderTextureDescriptor_set_shadowSamplingMode_m0AC43F8A8BE0755567EED4DDFADBE207739E1ECF,
	RenderTextureDescriptor_set_vrUsage_mC591604135749B0BD156865141E114F51812E502,
	RenderTextureDescriptor_set_memoryless_m1FB3F7E8B482C71BDB549AD71D762BCF337D8185,
	RenderTextureDescriptor__ctor_m227EBED323830B1059806874C69E1426DDC16B85,
	RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m387E338D17ED601B0587EF4E5CEB10DDFC42CC05,
	RenderTextureDescriptor__cctor_mA5675E6684E1E8C26E848D2390FB2ABE8E3C11FD,
	Hash128_get_isValid_m45005B82BA8416D4ACA8B20DF9DA5B8513C602B3,
	Hash128_CompareTo_m0BC4F8F4228CF2B48C615E39CD3CE260386CC5BC,
	Hash128_ToString_mD81890597BCFD67BE47646DA5366347C545EB5E8,
	Hash128_Parse_mC361CD25A0718BCA7360712283AD2ABD74E26277,
	Hash128_Internal_Hash128ToString_m0C8F22C98723DA1488DC4FC86886662AC030E798,
	Hash128_Compute_m23D2DFFD9496D37DB57C99056580792850772F29,
	Hash128_Equals_m4701FDD64B5C5F81B1E494D5586C5CB4519BC007,
	Hash128_Equals_m85BCDD87EB2A629FB8BB72FD63244787BC47E52D,
	Hash128_GetHashCode_m65DA1711C64E83AB514A3D498C568CB10EAA0D69,
	Hash128_CompareTo_m3595697B0FC7ACAED77C03FEC7FF80A073A4F6AE,
	Hash128_op_Equality_m77950F7840A081CBFE1D01A2B72DA521A4AF5065,
	Hash128_op_LessThan_m2F98815644822762B642B483F79A72CFB4BADB39,
	Hash128_op_GreaterThan_m1B6A95E0C9A75EE36E33D0E75ADB0D1872B843A1,
	Hash128_Parse_Injected_m881C4C2F88C4603BF396695B239CA78F57D20A11,
	Hash128_Internal_Hash128ToString_Injected_m6B07AAF4C1F86D9B7E482FD030AFB9CB15527583,
	Hash128_Compute_Injected_m6922A480AC8B1DADA1D9D708290F683E89BCD23D,
	Cursor_get_visible_m0BB8BC7FEDD558FB661E9023AB8C04FFEE41377C,
	Cursor_set_visible_mDB51E60B3D7B14873A6F5FBE5E0A432D4A46C431,
	Cursor_get_lockState_mE0C93F496E3AA120AD168ED30371C35ED79C9DF1,
	Cursor_set_lockState_m019E27A0FE021A28A1C672801416ACA5E770933F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Logger__ctor_m447215F90AA8D1508924BFB1CD1E49AFCCE04FFE,
	Logger_get_logHandler_m0C450B6FE86FA1B07422A1359CF278D1F5945CCD,
	Logger_set_logHandler_m891099050CB8C36A01ECA861E4820441B7080C73,
	Logger_get_logEnabled_m5AE50375671516B8E1171838E34C63C3A326E927,
	Logger_set_logEnabled_m3CEBD8A4B6DC548168EC38CFC5CB982222DB655F,
	Logger_get_filterLogType_mD850DE3FD8CC67E4B076FCC6B05F8FA603B5B38B,
	Logger_set_filterLogType_m1829D8E1B8350B8CFCE881598BABB2AA1826EE72,
	Logger_IsLogTypeAllowed_m7B2A793443A642C8CA0759D1DD0FBDFAED4E1FCB,
	Logger_GetString_m0078CBA4ADAE877152C264A4BEA5408BAB1DC514,
	Logger_Log_m653FDC5B68CB933887D8886762C7BBA75243A9AF,
	Logger_Log_mE06FF98F674C73E4BB67302E1EEDEA72F7655FF0,
	Logger_LogError_mE1B376E31566453DEEF94DF2537E1E4ED1F25C58,
	Logger_LogException_m362D3434D3B275B0B98E434BFBFBF52C76BBC9C3,
	Logger_LogFormat_m7F6CBF8AF6A60EF05CF9EE795502036CBAC847A7,
	Logger_LogFormat_m549605E9E6499650E70B0A168E047727490F31B7,
	UnityLogWriter_WriteStringToUnityLog_m0036CA8A9FB1FE3CFF460CA0212B6377B09E6504,
	UnityLogWriter_WriteStringToUnityLogImpl_mA39CCE94FF5BD2ABD4A8C8D78A00E366C64B4985,
	UnityLogWriter_Init_mAD1F3BFE2183E39CFA1E7BEFB948B368547D9E99,
	UnityLogWriter_get_Encoding_m0FEB104679588C939C4FD906480D1EDD3FD94110,
	UnityLogWriter_Write_mB1200B0B26545C48E178BFE952BEE14BDE53D2A7,
	UnityLogWriter_Write_mE3A4616A06A79B87512C3B0C8100EB508BB85C52,
	UnityLogWriter_Write_mE21873E7757E51C3771C58321E995DEBB2ADF750,
	UnityLogWriter__ctor_mE8DC0EAD466C5F290F6D32CC07F0F70590688833,
	Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C,
	Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369,
	Color_ToString_m17A27E0CFB20D9946D130DAEDB5BDCACA5A4473F,
	Color_GetHashCode_m88317C719D2DAA18E293B3F5CD17B9FB80E26CF1,
	Color_Equals_m63ECBA87A0F27CD7D09EEA36BCB697652E076F4E,
	Color_Equals_mA81EEDDC4250DE67C2F43BC88A102EA32A138052,
	Color_op_Addition_m26E4EEFAF21C0C0DC2FD93CE594E02F61DB1F7DF,
	Color_op_Subtraction_mCEE40D3CDA3D99C65D055D69585BB68B0C1C2C49,
	Color_op_Multiply_mA6BEAF09F1E0468B0557CEDFF1D228248F7D02B5,
	Color_op_Multiply_mF36917AD6235221537542FD079817CAB06CB1934,
	Color_op_Equality_m71B1A2F64AD6228F10E20149EF6440460D2C748E,
	Color_op_Inequality_m9C3EFC058BB205C298A2D3166173342303E660B9,
	Color_Lerp_mD37EF718F1BAC65A7416655F0BC902CE76559C46,
	Color_LerpUnclamped_m4DFBD5A647F0ADC12938BF31BBF149EA039AC572,
	Color_RGBMultiplied_m41914B23903491843FAA3B0C02027EF8B70F34CF,
	Color_get_red_m5562DD438931CF0D1FBBBB29BF7F8B752AF38957,
	Color_get_green_mD53D8F980E92A0755759FBB2981E3DDEFCD084C0,
	Color_get_blue_m5449DCBB31EEB2324489989754C00123982EBABA,
	Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905,
	Color_get_black_mEB3C91F45F8AA7E4842238DFCC578BB322723DAF,
	Color_get_yellow_mC8BD62CCC364EA5FC4273D4C2E116D0E2DE135AE,
	Color_get_clear_m419239BDAEB3D3C4B4291BF2C6EF09A7D7D81360,
	Color_get_linear_mB10CD29D56ADE2C811AD74A605BA11F6656E9D1A,
	Color_get_maxColorComponent_mBA8595CB2790747F42145CB696C10E64C9BBD76D,
	Color_op_Implicit_m653C1CE2391B0A04114B9132C37E41AC92B33AFE,
	Color_op_Implicit_m51CEC50D37ABC484073AECE7EB958B414F2B6E7B,
	Color32__ctor_m1AEF46FBBBE4B522E6984D081A3D158198E10AA2,
	Color32_op_Implicit_m52B034473369A651C8952BD916A2AB193E0E5B30,
	Color32_op_Implicit_mA89CAD76E78975F51DF7374A67D18A5F6EF8DA61,
	Color32_ToString_m217F2AD5C02E630E37BE5CFB0933630F6D0C3555,
	ColorUtility_DoTryParseHtmlColor_mE61F5CDC712B606366C3A668E16574124626FA79,
	ColorUtility_TryParseHtmlString_mAA5964F95A5F122823B656E19B8FDDAA38F3A3E9,
	Gradient_Init_m56D09BE88E111535F8D2278E03BE81C9D70EFAAD,
	Gradient_Cleanup_mD38D8100E8FAAC7FBD5047380555802E638DF718,
	Gradient_Internal_Equals_m210D28E9843DBA28E2F60FDBB366FE2B5B739B1A,
	Gradient__ctor_m297B6B928FDA6BD99A142736017F5C0E2B30BE7F,
	Gradient_Finalize_mB8CB38D86D7935F98000B5F4A0EBF419BD859210,
	Gradient_get_colorKeys_mBAE47BF507C2FDCEC907DFED48855C506C7F444D,
	Gradient_set_colorKeys_m50145F3052E08C6249A2B36A07FD4A38CBA17B2C,
	Gradient_get_alphaKeys_m0B8E542144BBAE420A2CB8D274B33FDDBA035387,
	Gradient_set_alphaKeys_mD8E4BB5FBBA0C4EF7C1553F5CE47297AABF4F1D6,
	Gradient_get_mode_m9554C64F9058E174B189E4342F89AC9D22F53F49,
	Gradient_set_mode_m6B53343017D7E700EF7D5A7E18193DD53ACEAEE6,
	Gradient_Equals_m0A13AD7938F81F21CC380609A506A39B37CF2097,
	Gradient_Equals_m7F23C7692189DDD94FC31758493D4C99C2F3FB1E,
	Gradient_GetHashCode_m2596E6672ACDAD829961AB1FBD3EB649A908DE64,
	Matrix4x4_GetLossyScale_m0FB088456F10D189CDBAD737670B053EE644BB5C,
	Matrix4x4_get_lossyScale_m6E184C6942DA569CE4427C967501B3B4029A655E,
	Matrix4x4_TRS_m5BB2EBA1152301BAC92FDC7F33ECA732BAE57990,
	Matrix4x4__ctor_mC7C5A4F0791B2A3ADAFE1E6C491B7705B6492B12,
	Matrix4x4_GetHashCode_m6627C82FBE2092AE4711ABA909D0E2C3C182028F,
	Matrix4x4_Equals_m7FB9C1A249956C6CDE761838B92097C525596D31,
	Matrix4x4_Equals_mF8358F488D95A9C2E5A9F69F31EC7EA0F4640E51,
	Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E,
	Matrix4x4_MultiplyPoint_mD5D082585C5B3564A5EFC90A3C5CAFFE47E45B65,
	Matrix4x4_MultiplyPoint3x4_m7C872FDCC9E3378E00A40977F641A45A24994E9A,
	Matrix4x4_ToString_m7E29D2447E2FC1EAB3D8565B7DCAFB9037C69E1D,
	Matrix4x4__cctor_mC5A7950045F0C8DBAD83A45D08812BEDBC6E159E,
	Matrix4x4_GetLossyScale_Injected_m3FC1B3C317274522F89E7BBD28D60C0B9AA2A8D1,
	Matrix4x4_TRS_Injected_mADF67489B3C6715B6BA35E22B0BA6713784100CC,
	Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1,
	Vector3_LerpUnclamped_m71C7510DEF6E1696FE11FE68C22B21968C04AC3E,
	Vector3_MoveTowards_mA288BB5AA73DDA9CA76EDC11F339BAFDA1E4FF45,
	Vector3_get_Item_mC3B9D35C070A91D7CA5C5B47280BD0EA3E148AC6,
	Vector3_set_Item_m89FF112CEC0D9ED43F1C4FE01522C75394B30AE6,
	Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1,
	Vector3__ctor_m6AD8F21FFCC7723C6F507CCF2E4E2EFFC4871584,
	Vector3_Scale_mD40CDE2B62BCBABD49426FAE104814F29CF2AA0B,
	Vector3_Cross_m3E9DBC445228FDB850BDBB4B01D6F61AC0111887,
	Vector3_GetHashCode_m6C42B4F413A489535D180E8A99BE0298AD078B0B,
	Vector3_Equals_m1F74B1FB7EE51589FFFA61D894F616B8F258C056,
	Vector3_Equals_m6B991540378DB8541CEB9472F7ED2BF5FF72B5DB,
	Vector3_Normalize_mDEA51D0C131125535DA2B49B7281E0086ED583DC,
	Vector3_Normalize_m174460238EC6322B9095A378AA8624B1DD9000F3,
	Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B,
	Vector3_Dot_m0C530E1C51278DE28B77906D56356506232272C1,
	Vector3_Angle_m8911FFA1DD1C8C46D923B52645B352FA1521CD5F,
	Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7,
	Vector3_ClampMagnitude_mC9ADEE02D65C578B45F79DF10C53B81AB29D09D5,
	Vector3_Magnitude_m3958BE20951093E6B035C5F90493027063B39437,
	Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274,
	Vector3_get_sqrMagnitude_m1C6E190B4A933A183B308736DEC0DD64B0588968,
	Vector3_Min_m0D0997E6CDFF77E5177C8D4E0A21C592C63F747E,
	Vector3_Max_m78495079CA1E29B0658699B856AFF22E23180F36,
	Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2,
	Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB,
	Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D,
	Vector3_get_back_mE7EF8625637E6F8B9E6B42A6AE140777C51E02F7,
	Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7,
	Vector3_get_down_m3F76A48E5B7C82B35EE047375538AFD91A305F55,
	Vector3_get_left_m74B52D8CFD8C62138067B2EB6846B6E9E51B7C20,
	Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5,
	Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E,
	Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3,
	Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2,
	Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E,
	Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839,
	Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624,
	Vector3_op_Equality_mA9E2F96E98E71AE7ACCE74766D700D41F0404806,
	Vector3_op_Inequality_mFEEAA4C4BF743FB5B8A47FF4967A5E2C73273D6E,
	Vector3_ToString_m2682D27AB50CD1CE4677C38D0720A302D582348D,
	Vector3__cctor_m83F3F89A8A8AFDBB54273660ABCA2E5AE1EAFDBD,
	Quaternion_Inverse_mC3A78571A826F05CE179637E675BD25F8B203E0C,
	Quaternion_Slerp_m56DE173C3520C83DF3F1C6EDFA82FF88A2C9E756,
	Quaternion_SlerpUnclamped_m4450A435D9A44F060FBB12A9C328FD338B8F892B,
	Quaternion_Internal_FromEulerRad_mC6AB58E2F3C37DFE2089A38D578E862B3430E755,
	Quaternion_Internal_ToEulerRad_m5F7B68953CC22DCE9EC246396B02F0ADC0B1C470,
	Quaternion_AngleAxis_m07DACF59F0403451DABB9BC991C53EE3301E88B0,
	Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672,
	Quaternion__ctor_m7502F0C38E04C6DE24C965D1CAF278DDD02B9D61,
	Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64,
	Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790,
	Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C,
	Quaternion_IsEqualUsingDot_mA5E0CF75CBB488E3EC55BE9397FC3C92439A0BEF,
	Quaternion_op_Equality_m0DBCE8FE48EEF2D7C79741E498BFFB984DF4956F,
	Quaternion_op_Inequality_mDA6D2E63A498C8A9AB9A11DD7EA3B96567390C70,
	Quaternion_Dot_m0C931CC8127C5461E5B8A857BDE2CE09297E468B,
	Quaternion_SetLookRotation_mDB3D5A8083E5AB5881FA9CC1EACFC196F61B8204,
	Quaternion_Internal_MakePositive_mC458BD7036703798B11C6C46675814B57E236597,
	Quaternion_get_eulerAngles_mF8ABA8EB77CD682017E92F0F457374E54BC943F9,
	Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05,
	Quaternion_Euler_m55C96FCD397CC69109261572710608D12A4CBD2B,
	Quaternion_GetHashCode_m43BDCF3A72E31FA4063C1DEB770890FF47033458,
	Quaternion_Equals_m099618C36B86DC63B2E7C89673C8566B18E5996E,
	Quaternion_Equals_m0A269A9B77E915469801463C8BBEF7A06EF94A09,
	Quaternion_ToString_m38DF4A1C05A91331D0A208F45CE74AF005AB463D,
	Quaternion__cctor_m026361EBBB33BB651A59FC7BC6128195AEDCF935,
	Quaternion_Inverse_Injected_m1CD79ADF97C60D5645C15C5F04219021EE4654DD,
	Quaternion_Slerp_Injected_m28511088F8514F6FF5C85A0B042EFB662F2585D3,
	Quaternion_SlerpUnclamped_Injected_m4F8FA3284BA2D46D4977498F68E9C0C5539F9193,
	Quaternion_Internal_FromEulerRad_Injected_m2197C7F75B2DB8B99C1947CD7C92714FE8D0099D,
	Quaternion_Internal_ToEulerRad_Injected_mE55FFD02837E4FFFFFF8689E63B4EAF4F3B7396D,
	Quaternion_AngleAxis_Injected_mFEA2ACDAAEEDCA27647FCB47B636F0D041147DD3,
	Quaternion_LookRotation_Injected_m59A46014572ACB8F5C8A377B773D12EACCB53D4A,
	Mathf_NextPowerOfTwo_m071D88C91A1CBECD47F18CA20D219C77879865E7,
	Mathf_GammaToLinearSpace_m537DFDE30A58265FDA50F4D245B9599BBA8A4772,
	Mathf_Sin_m5275643192EFB3BD27A722901C6A4228A0DB8BB6,
	Mathf_Cos_mC5ECAE74D1FE9AF6F6EFF50AD3CD6BA1941B267A,
	Mathf_Tan_m436F7A034334BCB42BD46ABC94D1200C70E81A49,
	Mathf_Atan_m1FF47E958C869FCB8BADF804011E04736E23C6F9,
	Mathf_Atan2_mC5BCC778869EF562CD3A5A9D96002F63DA36A823,
	Mathf_Sqrt_mF1FBD3142F5A3BCC5C35DFB922A14765BC0A8E2B,
	Mathf_Abs_mD852D98E3D4846B45F57D0AD4A8C6E00EF272662,
	Mathf_Abs_mC7DD2FB3789B5409055836D0E7D8227AD2099FDC,
	Mathf_Min_mCF9BE0E9CAC9F18D207692BB2DAC7F3E1D4E1CB7,
	Mathf_Min_m1A2CC204E361AE13C329B6535165179798D3313A,
	Mathf_Max_m670AE0EC1B09ED1A56FF9606B0F954670319CB65,
	Mathf_Max_mBDE4C6F1883EE3215CD7AE62550B2AC90592BC3F,
	Mathf_Pow_mC1BFA8F6235567CBB31F3D9507A6275635A38B5F,
	Mathf_Exp_m7C92A75E0245B9FAEE93683523C22910C0877693,
	Mathf_Log_mD0CFD1242805BD697B5156AA46FBB43E7636A19B,
	Mathf_Log10_m4D3234A75A708BE7BBDC19CF5DEB355E52205AE5,
	Mathf_Ceil_m4FC7645E3D0F8FEDC33FE507E3D913108DD94E94,
	Mathf_Floor_mD447D35DE1D81DE09C2EFE21A75F0444E2AEF9E1,
	Mathf_Round_mC8FAD403F9E68B0339CF65C8F63BFA3107DB3FC9,
	Mathf_CeilToInt_m0230CCC7CC9266F18125D9425C38A25D1CA4275B,
	Mathf_FloorToInt_m0C42B64571CE92A738AD7BB82388CE12FBE7457C,
	Mathf_RoundToInt_m0EAD8BD38FCB72FA1D8A04E96337C820EC83F041,
	Mathf_Sign_m6FA1D12786BEE0419D4B9426E5E4955F286BC8D3,
	Mathf_Clamp_m033DD894F89E6DCCDAFC580091053059C86A4507,
	Mathf_Clamp_mE1EA15D719BF2F632741D42DF96F0BC797A20389,
	Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B,
	Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364,
	Mathf_LerpUnclamped_mA2458D5B39272F2996B92978CFBF6FFA43748B19,
	Mathf_LerpAngle_m8802FB8DCECA2D48FDD13C20F384B9430E66C99F,
	Mathf_MoveTowards_m3B267066E774818E369220B0ABD084B271B45A85,
	Mathf_SmoothStep_m4A729F510ABD2E9F385A41E4C4DFA972D1D07DF9,
	Mathf_Approximately_m91AF00403E0D2DEA1AAE68601AD218CFAD70DF7E,
	Mathf_SmoothDamp_m00F6830F4979901CACDE66A7CEECD8AA467342C8,
	Mathf_Repeat_m8459F4AAFF92DB770CC892BF71EE9438D9D0F779,
	Mathf_InverseLerp_m7054CDF25056E9B27D2467F91C95D628508F1F31,
	Mathf_DeltaAngle_mF21640BC4B2DDC43DE7AA557EFC8D816CAD6E116,
	Mathf__cctor_m4855BF06F66120E2029CFA4F3E82FBDB197A86EC,
	Vector2_get_Item_m67344A67120E48C32D9419E24BA7AED29F063379,
	Vector2_set_Item_m2335DC41E2BB7E64C21CDF0EEDE64FFB56E7ABD1,
	Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0,
	Vector2_Lerp_m85DD66409D128B4F175627F89FA9D8751B75589F,
	Vector2_LerpUnclamped_mA24A011324A636E49C5DB828B208419DA240E405,
	Vector2_Scale_m7AA97B65C683CB3B0BCBC61270A7F1A6350355A2,
	Vector2_Normalize_m99A2CC6E4CB65C1B9231F898D5B7A12B6D72E722,
	Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B,
	Vector2_ToString_m83C7C331834382748956B053E252AE3BD21807C4,
	Vector2_GetHashCode_m028AB6B14EBC6D668CFA45BF6EDEF17E2C44EA54,
	Vector2_Equals_m4A2A75BC3D09933321220BCEF21219B38AF643AE,
	Vector2_Equals_mD6BF1A738E3CAF57BB46E604B030C072728F4EEB,
	Vector2_Dot_m34F6A75BE3FC6F728233811943AC4406C7D905BA,
	Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF,
	Vector2_get_sqrMagnitude_mAEE10A8ECE7D5754E10727BA8C9068A759AD7002,
	Vector2_Angle_mC4A140B49B9E737C9FC6B52763468C5662A8B4AC,
	Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4,
	Vector2_Min_m4C1D2EB8CD6ECF4A6919A798314095739951445B,
	Vector2_Max_m5E3C15E31483217408A281A5FDB5356BD6F976EE,
	Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682,
	Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0,
	Vector2_op_Multiply_mEDF9FDDF3BFFAEC997FBCDE5FA34871F2955E7C4,
	Vector2_op_Division_mEF4FA1379564288637A7CF5E73BA30CA2259E591,
	Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56,
	Vector2_op_Multiply_m2E30A54E315810911DFC2E25C700757A68AC1F38,
	Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077,
	Vector2_op_Equality_m0E86E1B1038DDB8554A8A0D58729A7788D989588,
	Vector2_op_Inequality_mC16161C640C89D98A00800924F83FF09FD7C100E,
	Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28,
	Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F,
	Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8,
	Vector2_get_one_m6E01BE09CEA40781CB12CCB6AF33BBDA0F60CEED,
	Vector2_get_up_mC4548731D5E7C71164D18C390A1AC32501DAE441,
	Vector2_get_right_mB4BD67462D579461853F297C0DE85D81E07E911E,
	Vector2__cctor_m13D18E02B3AC28597F5049D2F54830C9E4BDBE84,
	Vector2Int_get_x_m300C7C05CD66D24EE62D91CDC0C557A6C0ABF25E,
	Vector2Int_set_x_mC6F9F1E43668A7F8E8C44CEFB427BA97389F1420,
	Vector2Int_get_y_m0D6E131AB5FA4AD532DEC377F981AADA189E680B,
	Vector2Int_set_y_mF79CD7FACF0A9A1CC12678E2E24BD43C5E836D88,
	Vector2Int__ctor_m501C34762BA7ECDDCFC25C19A4B9C93BC15004E1,
	Vector2Int_op_Implicit_m05B84E680DA5408143C51CB664F97AC4702DC200,
	Vector2Int_op_Equality_m7EDB885618BE7B785798F96DDDDA70E53461BB4B,
	Vector2Int_op_Inequality_mAC8212BEC016167196A2EF04A381D3439070ECAE,
	Vector2Int_Equals_mF7D7EFBC0286224832BA76701801C28530D40479,
	Vector2Int_Equals_m65420C995F326F5C340E4825EA5E16BDE68F5A9C,
	Vector2Int_GetHashCode_m73E874F4E94DF3D2603035E2E892873B139A7A9E,
	Vector2Int_ToString_mB0D67C1885311767BA89D4C4A6E3A5A515194BF3,
	Vector2Int__cctor_mCCA5D2394D22AB700421D56C1EB6998F4E3C810A,
	Vector3Int_get_x_m23CB00F1579FD4CE86291940E2E75FB13405D53A,
	Vector3Int_set_x_mED89A481E7FF21D0BF9F61511D5442E2CE9BEB75,
	Vector3Int_get_y_m1C2F0AB641A167DF22F9C3C57092EC05AEF8CA26,
	Vector3Int_set_y_m3E8A59AC8851E9AEC6B65AA167047C1F7C497B84,
	Vector3Int_get_z_m9A88DC2346FD1838EC611CC8AB2FC29951E94183,
	Vector3Int_set_z_m7BFC415EBBF5D31ACF664EEC37EE801C22B0E8D5,
	Vector3Int__ctor_m171D642C38B163B353DAE9CCE90ACFE0894C1156,
	Vector3Int_op_Equality_mC2E3A3395AC3E18397283F3CBEA7167B2E463DFC,
	Vector3Int_Equals_m704D204F83B9C64C7AF06152F98B542C5C400DC7,
	Vector3Int_Equals_m9F98F28666ADF5AD0575C4CABAF6881F1317D4C1,
	Vector3Int_GetHashCode_m6CDE2FEC995180949111253817BD0E4ECE7EAE3D,
	Vector3Int_ToString_m08AB1BE6A674B2669839B1C44ACCF6D85EBCFB91,
	Vector3Int__cctor_m0EE114B6FDC7C783EF7B206D4E25F5CE900003C9,
	Vector4_get_Item_m39878FDA732B20347BB37CD1485560E9267EDC98,
	Vector4_set_Item_m56FB3A149299FEF1C0CF638CFAF71C7F0685EE45,
	Vector4__ctor_m545458525879607A5392A10B175D0C19B2BC715D,
	Vector4_LerpUnclamped_mBFE6BE611E1C4AFF32BB59B052A1A640261CD7D9,
	Vector4_GetHashCode_m7329FEA2E90CDBDBF4F09F51D92C87E08F5DC92E,
	Vector4_Equals_m552ECA9ECD220D6526D8ECC9902016B6FC6D49B5,
	Vector4_Equals_mB9894C2D4EE56C6E8FDF6CC40DCE0CE16BA4F7BF,
	Vector4_Dot_m9FAE8FE89CF99841AD8D2113DFCDB8764F9FBB18,
	Vector4_get_magnitude_mE33CE76438DDE4DDBBAD94178B07D9364674D356,
	Vector4_get_sqrMagnitude_m6B2707CBD31D237605D066A5925E6419D28B5397,
	Vector4_get_zero_m42821248DDFA4F9A3E0B2E84CBCB737BE9DCE3E9,
	Vector4_op_Addition_m2079975CEB29719BDECFA861B53E499C70AA7015,
	Vector4_op_Subtraction_m2D5AED6DD0324E479548A9346AE29DAB489A8250,
	Vector4_op_Multiply_m16A8F11F144C03A8C817AC4FE542689E746043F4,
	Vector4_op_Division_m1D1BD7FFEF0CDBB7CE063CA139C22210A0B76689,
	Vector4_op_Equality_m9AE0D09EC7E02201F94AE469ADE9F416D0E20441,
	Vector4_op_Inequality_m7038D1E27BCA2E4C0EA8E034C30E586635FBF228,
	Vector4_op_Implicit_mEAB05A77FF8B3EE79C31499F0CF0A0D621A6496C,
	Vector4_ToString_m769402E3F7CBD6C92464D916527CC87BBBA53EF9,
	Vector4__cctor_m478FA6A83B8E23F8323F150FF90B1FB934B1C251,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_SendMessage_m00395F263B4C7FD7F10C32B1F4C4C1F00503D4BB,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_TrySendMessage_m950332D66588946F8699A64E5B8DBA8956A45303,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_Poll_mEAAE7671B5D8E0360BFE50E61E89FFF65DB825E4,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_RegisterInternal_m991A5281F58D94FA0F095A538BD91CA72B864965,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_UnregisterInternal_mAF6931079473185968AFCD40A23A610F7D6CC3A0,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_Initialize_mBC677B0244B87A53875C8C3A3A716DC09A8D541F,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_IsConnected_m08B0A552BE45CC80F911E22D990B8FE6C82B53DD,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_DisconnectAll_m5849A206AC5D274115B352114BD5F4B72900F651,
	PlayerConnectionInternal_IsConnected_m4AD0EABFF2FCE8DE9DE1A6B520C707F300721FB2,
	PlayerConnectionInternal_Initialize_mB0E05590ED32D5DCD074FD6CB60064F8A96BB4FD,
	PlayerConnectionInternal_RegisterInternal_mC3FB67053C4C7DB1AABAB7E78E1F1345720ED84F,
	PlayerConnectionInternal_UnregisterInternal_m675B2C87E01FCBBF5CB1A205375A2E95A8F150B2,
	PlayerConnectionInternal_SendMessage_m83801DCA5BBCC8116F1C7898FB6A755CCAF6F928,
	PlayerConnectionInternal_TrySendMessage_mE31F212ED59D69FD01FC2B6B4503A5AF97C1948D,
	PlayerConnectionInternal_PollInternal_m46079D478471FAB04EE8E613CAE8F6E79822472E,
	PlayerConnectionInternal_DisconnectAll_m58AFB71131F174149D6AA98C312D9026C15C6090,
	PlayerConnectionInternal__ctor_m882227F7C855BCF5CE1B0D44752124106BE31389,
	PlayerPrefsException__ctor_m1780F97210AE9B8FB0EE3DFAD6BB73A01E4A1CAB,
	PlayerPrefs_TrySetSetString_m67AE78AFF61EABF374102466D40944DBA17E37AD,
	PlayerPrefs_SetString_m7AC4E332A5DCA04E0AD91544AF836744BA8C2583,
	PlayerPrefs_GetString_m83CA5F0D9E058C853254ACF9130C72108BE56C9B,
	PlayerPrefs_GetString_m3031AD2D5DEAB97677A9EF629618541437F079F1,
	PlayerPrefs_Save_m6CC1FE22D4B10AC819F55802D725BE17EA2AD37B,
	PropertyAttribute__ctor_m7F5C473F39D5601486C1127DA0D52F2DC293FC35,
	TooltipAttribute__ctor_m13242D6EEE484B1CCA7B3A8FA720C5BCC44AF5DF,
	SpaceAttribute__ctor_m645A0DE9B507F2AFD8C67853D788F5F419D547C7,
	SpaceAttribute__ctor_mA70DC7F5FDC5474223B45E0F71A9003BBED1EFD0,
	RangeAttribute__ctor_mB9CC43849A074C59DD99F154215A755131D47D89,
	MultilineAttribute__ctor_mA72F92D940A051EA25E1F2E052B455FEED13F0A5,
	TextAreaAttribute__ctor_m6134ACE5D232B16A9433AA1F47081FAA9BAC1BA1,
	Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384,
	Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780,
	Random_RandomRangeInt_m4D223ACEC78BCC435D90A7D3696775DE973D324E,
	ResourceRequest_get_asset_m22E63CEED8581B913F8424D2100A47A335032A75,
	ResourceRequest__ctor_m9ABE408F147B831EA22F202333FA0A84B8AA575C,
	NULL,
	Resources_FindObjectsOfTypeAll_mFFDC0207FEBD620424377896FC1B67307DB42520,
	Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1,
	NULL,
	Resources_Load_mF0FA033BF566CDDA6A0E69BB97283B44C40726E7,
	Resources_LoadAsync_m7A78B42B839F6ED3A198CE55359006823666D13F,
	Resources_LoadAsyncInternal_m90563E8C10B5F7F7CEF0AE32C9477CFF897115B9,
	Resources_LoadAll_mCBF6F93023EF2F09957B7193D9966EF3AB374C9E,
	NULL,
	Resources_GetBuiltinResource_m73DDAC485E1E06C925628AA7285AC63D0797BD0A,
	NULL,
	Resources_UnloadAsset_m066BEEBB51255EF3C0120EA4CF2300CA448FAF64,
	Resources_UnloadUnusedAssets_m04EC5EA06A8F7F26ED95BD704E1DDF62BBAB7043,
	AsyncOperation_InternalDestroy_m22D4FE202FC59024E9ED7F6537C87D26F96FC551,
	AsyncOperation_get_isDone_m2A08EE3D38FD9FE81F2D619FA66255B6F61DAB54,
	AsyncOperation_get_progress_m27F7D885AF1A989B993199181DE70D7B0F1E3984,
	AsyncOperation_set_priority_mEDC649F39ABECE1A801F379F9D2C7BA2E34F5B5E,
	AsyncOperation_get_allowSceneActivation_mC5EC961F8BD14C79AFF6AEE2F634FB15F60AFA1F,
	AsyncOperation_set_allowSceneActivation_m297E3269310864DE1110ED51C7E2E302B32185C9,
	AsyncOperation_Finalize_m36607FEC5F5766510DD0B14440CD9775CF1C23C2,
	AsyncOperation_InvokeCompletionEvent_m5F86FF01A5143016630C9CFADF6AA01DBBBD73A5,
	AsyncOperation_add_completed_mD9CC08F727DA8EBB849CBF8903333EF6110C7ED6,
	AsyncOperation_remove_completed_mE0EA5D4CE4308A12C896F2A8CF193CD0BF5B19DE,
	AsyncOperation__ctor_mEEE6114B72B8807F4AA6FF48FA79E4EFE480293F,
	AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m716999F8F469E9398A275432AA5C68E81DD8DB24,
	AttributeHelperEngine_GetRequiredComponents_m869E1FF24FE124874E0723E11C12A906E57E3007,
	AttributeHelperEngine_GetExecuteMode_mDE99262C53FA67B470B8668A13F968B4D5A8E0A1,
	AttributeHelperEngine_CheckIsEditorScript_m95CEEF4147D16BC2985EAADD300905AB736F857E,
	AttributeHelperEngine_GetDefaultExecutionOrderFor_m0972E47FA03C9CEF196B1E7B2E708E30DF4AD063,
	NULL,
	AttributeHelperEngine__cctor_mAE0863DCF7EF9C1806BDC1D4DF64573464674964,
	DisallowMultipleComponent__ctor_m108E5D8C0DB938F0A747C6D2BA481B4FA9CDECB3,
	RequireComponent__ctor_m27819B55F8BD1517378CEFECA00FB183A13D9397,
	RequireComponent__ctor_m2833BC8FBE2C72EE6F0CA08C4617BF84CB7D5BE6,
	AddComponentMenu__ctor_m33A9DE8FEE9BC9C12F67CF58BFEBECA372C236A3,
	AddComponentMenu__ctor_m78D1D62B91D88424AE2175501B17E4609EF645EA,
	ContextMenu__ctor_m69C18B7CA3C4C3C9687B6BF42697BC375A63CBBC,
	ContextMenu__ctor_mC0876137653AF1E3441B1B3E87622140BB697923,
	ContextMenu__ctor_mC35AC9699C3AC41056CCA7A70D4853E8358FAB1F,
	ExecuteInEditMode__ctor_m9A67409D4A11562F23F928655D9A3EFB7A69BB81,
	ExecuteAlways__ctor_m6199E1FB2E787ABEE85C19153D3C90B815572092,
	HideInInspector__ctor_mED96F804290F203756C1EDDE57F47C0E5A8FF4B4,
	HelpURLAttribute__ctor_mE8D73F64B451D3746E3427E74D332B7731D9D3AB,
	DefaultExecutionOrder_get_order_mFD2CD99AEF550E218FAFC6CB3DDA3CE8D78614A9,
	ExcludeFromPresetAttribute__ctor_mB8BE49E17E4360DDB485784D219AE49847A85FB2,
	Behaviour_get_enabled_mAA0C9ED5A3D1589C1C8AA22636543528DB353CFB,
	Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B,
	Behaviour_get_isActiveAndEnabled_mC42DFCC1ECC2C94D52928FFE446CE7E266CA8B61,
	Behaviour__ctor_m409AEC21511ACF9A4CC0654DF4B8253E0D81D22C,
	ClassLibraryInitializer_Init_mB8588C1A9DD9CB6B5CE77DB1F79AE301C46E0CE7,
	Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9,
	Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C,
	Component_GetComponent_m5E75925F29811EEC97BD17CDC7D4BD8460F3090F,
	Component_GetComponentFastPath_mDEB49C6B56084E436C7FC3D555339FA16949937E,
	NULL,
	Component_GetComponentInChildren_mEF7890FAC10EA2F776464285B0DCC58B8C373D34,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Component_GetComponentInParent_mFD9A8F6311ABAF986CA0DA556662F89FD9234E7D,
	NULL,
	NULL,
	NULL,
	NULL,
	Component_GetComponentsForListInternal_m469B4C3A883942213BEA0EAAA54629219A042480,
	Component_GetComponents_m1ACBE6B9A75ECC898BA3B21D59AA7B3339D7735A,
	NULL,
	NULL,
	Component_BroadcastMessage_mB8F26C0376146136DF5311F91A3A9667EAC3EDF8,
	Component_BroadcastMessage_m33BDF64582F1D9EA2FF4A3B819649C8E62027CAC,
	Component__ctor_m5E2740C0ACA4B368BC460315FAA2EDBFEAC0B8EF,
	Coroutine__ctor_mCA679040DA81B31D1E341400E98F6CF569269201,
	Coroutine_Finalize_mACCDC3AFBA7F1D247231AA875B5099200AF9ECC5,
	Coroutine_ReleaseCoroutine_mD33DD220788EEA099B98DD1258D6332A46D3D571,
	SetupCoroutine_InvokeMoveNext_m9106BA4E8AE0E794B17F184F1021A53F1D071F31,
	SetupCoroutine_InvokeMember_m0F2AD1D817B8E221C0DCAB9A81DA8359B20A8EFB,
	NULL,
	CustomYieldInstruction_get_Current_m9B2B482ED92A58E85B4D90A5AC7C89DFF87E33DC,
	CustomYieldInstruction_MoveNext_m7EA6BAAEF6A01DC791D0B013D5AB5C377F9A6990,
	CustomYieldInstruction_Reset_m9B3349022DFDDA3A059F14D199F2408725727290,
	CustomYieldInstruction__ctor_m06E2B5BC73763FE2E734FAA600D567701EA21EC5,
	ExcludeFromObjectFactoryAttribute__ctor_mE0437C73993AD36DCB7A002D70AC988340742CD0,
	ExtensionOfNativeClassAttribute__ctor_m2D759F6D70D6FE632D8872A7D7C3E7ECFF83EE46,
	GameObject_CreatePrimitive_mA4D35085D817369E4A513FF31D745CEB27B07F6A,
	NULL,
	GameObject_GetComponent_mECB756C7EB39F6BB79F8C065AB0013354763B151,
	GameObject_GetComponentFastPath_m5B276335DD94F6B307E604272A26C15B997C3CD4,
	GameObject_GetComponentByName_mAC730F7E7AACEFACBC17536AADBBB7254B25E3D3,
	GameObject_GetComponent_m92533B63C1C0C72597C246C2E7A8BBA662BDD1DF,
	GameObject_GetComponentInChildren_mBC5C12CDA1749A827D136DABBF10498B1096A086,
	GameObject_GetComponentInChildren_mD4D1DDDF6052346718022CE72474233129BEA768,
	NULL,
	NULL,
	GameObject_GetComponentInParent_mA5BF9DFCA90C9003EB8F392CD64C45DFCB80F988,
	NULL,
	GameObject_GetComponentsInternal_mAB759217A3AD0831ABD9387163126D391459E1B8,
	GameObject_GetComponents_mD9AF87D297017455ADD8CF0DF05BB57B4382375F,
	NULL,
	GameObject_GetComponents_mA06AB0C2427DD87C91D14889C542F6E0F66FD915,
	NULL,
	GameObject_GetComponentsInChildren_m34C6A0C298EC385E3AAB80A7F3727B3AA0DE5AE1,
	GameObject_GetComponentsInChildren_m39CCEFC6BC28CBD9587311B1E1C842B63DC8BDDB,
	NULL,
	NULL,
	NULL,
	NULL,
	GameObject_GetComponentsInParent_mC0D14D4EB6DB8A8D1CB51EA47DA0C25FFEEB2AC3,
	GameObject_GetComponentsInParent_mFDE58BE6E14D1F34DE76EF62623BD0A46A99D24B,
	NULL,
	NULL,
	NULL,
	NULL,
	GameObject_TryGetComponent_m2AA8E1EF55AE188975F247B84E88EA8830126174,
	GameObject_TryGetComponentInternal_m5003A254549BB4B574A424294C7D93394F7DDDA2,
	GameObject_TryGetComponentFastPath_mBCE2196B5F774DB34EAE23EA89FDAFB427A04AC2,
	GameObject_FindWithTag_mF188C0A7A5DAC355AB020E2964B155F355237CB5,
	GameObject_SendMessageUpwards_m0765FA6E1EE34F980B5A7EC361EE279DA07E724D,
	GameObject_SendMessage_m68EE6232C4171FB0767181C1C1DD95C1103A6A75,
	GameObject_BroadcastMessage_mABB308EA1B7F641BFC60E091A75E5E053D23A8B5,
	GameObject_AddComponentInternal_mF527865CB0E00B2DCEC19BF13899241818F26A1A,
	GameObject_Internal_AddComponentWithType_m452B72618245B846503E5CA7DEA4662A77FB9896,
	GameObject_AddComponent_m489C9D5426F2050795FA696CD478BB49AAE4BD70,
	NULL,
	GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C,
	GameObject_get_layer_m0DE90D8A3D3AA80497A3A80FBEAC2D207C16B9C8,
	GameObject_set_layer_mDAC8037FCFD0CE62DB66004C4342EA20CF604907,
	GameObject_get_active_m02FE30D766DA2B308BEDEABD70F4A5574D124C1A,
	GameObject_set_active_m957ACA535CA30B47ADB282FFE2927A5AC8DDBFF6,
	GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04,
	GameObject_get_activeSelf_mFE1834886CAE59884AC2BE707A3B821A1DB61F44,
	GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF,
	GameObject_SetActiveRecursively_m37A94311A3FF4C398BD85D0E853FE248DB9C1C22,
	GameObject_get_isStatic_m45405FB1C5B284CF2EB3CB6EA5C57D6D1A384615,
	GameObject_set_isStatic_m945C32F2AB4D7AA35AD0E68820475EDEC2A56BBE,
	GameObject_get_isStaticBatchable_mF496707FE77F85744227A7AC72C42C3E3FCE2378,
	GameObject_get_tag_mA9DC75D3D591B5E7D1ADAD14EA66CC4186580275,
	GameObject_set_tag_mEF09E323917FAA2CECA527F821111A92031C1138,
	GameObject_CompareTag_mF66519C9DAE4CC8873C36A04C3CAF7DDEC3C7EFE,
	GameObject_FindGameObjectWithTag_m9F2877F52346B973DE3023209D73852E96ECC10D,
	GameObject_FindGameObjectsWithTag_mF49A195F19A598C5FD145FFE175ABE5B4885FAD9,
	GameObject_SendMessageUpwards_mD32EFE21FC143F08243DB4F6333D5993D3935906,
	GameObject_SendMessageUpwards_m051F115223C75F7EAE50B95D4F1B04D313CB7215,
	GameObject_SendMessageUpwards_m94C01293657094B2EFC19A803AAA218544E5E7AD,
	GameObject_SendMessage_mB9147E503F1F55C4F3BC2816C0BDA8C21EA22E95,
	GameObject_SendMessage_mAF014F12A3B807BC435571585D4DD34FA89EC28E,
	GameObject_SendMessage_m9A7BE015CC1472A59F1077CADFB22AD67F8722D9,
	GameObject_BroadcastMessage_mDF70A40A7E27DF70B4338EBB840500A54060E33D,
	GameObject_BroadcastMessage_m19C6E3E01BE544290887B016E2C9FA3EE50FA287,
	GameObject_BroadcastMessage_m58B3BCA61DA82D13BC28DB128A2717728C053DB8,
	GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686,
	GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D,
	GameObject__ctor_m20BE06980A232E1D64016957059A9DD834173F68,
	GameObject_Internal_CreateGameObject_m9DC9E92BD086A7ADD9ABCE858646A951FA77F437,
	GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD,
	GameObject_get_scene_m46B0EF291DE58599187604475EC8640416BF9027,
	GameObject_get_sceneCullingMask_m7A2DF40406FF26E0091E4836240A4DA63D24502D,
	GameObject_get_gameObject_mB8D6D847ABF95430B098554F3F2D63EC1D30C815,
	GameObject_get_scene_Injected_m7B3888363DF23412EF1F431ADE1055C1DFF8EBE2,
	LayerMask_op_Implicit_m2AFFC7F931005437E8F356C953F439829AF4CFA5,
	LayerMask_op_Implicit_m3F256A7D96C66548F5B62C4621B9725301850300,
	LayerMask_get_value_m682288E860BBE36F5668DCDBC59245DE6319E537,
	LayerMask_set_value_m56653944C7760E9D456E6B9E68F85CAA5B24A042,
	LayerMask_NameToLayer_m6491D9EA75F68B1F8AE15A9B4F193FFB9352B901,
	ManagedStreamHelpers_ValidateLoadFromStream_m550BE5DED66EC83AF331265B81084185B35FD846,
	ManagedStreamHelpers_ManagedStreamRead_mC0BDD6B226BBF621F6DAC184E3FC798D6BB0D47B,
	ManagedStreamHelpers_ManagedStreamSeek_m450DB930DD5085114F382A6FE05CF15C5CB21168,
	ManagedStreamHelpers_ManagedStreamLength_mB518EF67FCBA5080CA4C45F34496C05041E07B98,
	MonoBehaviour_IsInvoking_mD0C27BE34FB97F408191450A702FA016E19997E5,
	MonoBehaviour_CancelInvoke_m6ACF5FC83F8FE5A6E744CE1E83A94CB3B0A8B7EF,
	MonoBehaviour_Invoke_m979EDEF812D4630882E2E8346776B6CA5A9176BF,
	MonoBehaviour_InvokeRepeating_m99F21547D281B3F835745B681E5472F070E7E593,
	MonoBehaviour_CancelInvoke_mDD95225EF4DFBB8C00B865468DE8AFEB5D30490F,
	MonoBehaviour_IsInvoking_mCA9E133D28B55AE0CE0E8EDBB183081DEEE57FBC,
	MonoBehaviour_StartCoroutine_m590A0A7F161D579C18E678B4C5ACCE77B1B318DD,
	MonoBehaviour_StartCoroutine_mCD250A96284E3C39D579CEC447432681DE8D1E44,
	MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7,
	MonoBehaviour_StartCoroutine_Auto_m5002506E1DE4625F7FEACC4D7F0ED8595E3B3AB5,
	MonoBehaviour_StopCoroutine_m3CDD6C046CC660D4CD6583FCE97F88A9735FD5FA,
	MonoBehaviour_StopCoroutine_mC465FFA3C386BA22384F7AFA5495FF2286510562,
	MonoBehaviour_StopCoroutine_mC2C29B39556BFC68657F27343602BCC57AA6604F,
	MonoBehaviour_StopAllCoroutines_mA5469BB7BBB59B8A94BB86590B051E0DFACC12DD,
	MonoBehaviour_get_useGUILayout_m468C9F5A4D7F37643D26EEF41E5BA521CD81C267,
	MonoBehaviour_set_useGUILayout_m00327593C0DC39787FB9310328489F802FF63167,
	MonoBehaviour_print_m171D860AF3370C46648FE8F3EE3E0E6535E1C774,
	MonoBehaviour_Internal_CancelInvokeAll_m11071D9A8C6743C4FA754C1A079CFF5171638AB1,
	MonoBehaviour_Internal_IsInvokingAll_m44707A3C084E2CABC98FBCAF3531E547C6D59644,
	MonoBehaviour_InvokeDelayed_mB70D589B53A55251F47641C6D3A51DA59F973D63,
	MonoBehaviour_CancelInvoke_m38D5CC0BF3FFDD89DD5F5A47E852B1E04BAAC5BB,
	MonoBehaviour_IsInvoking_mA209C7787032B92CEC40A5C571CB257D7A87457E,
	MonoBehaviour_IsObjectMonoBehaviour_mCB948905029893B860CCD5D852878660A1CEF0D7,
	MonoBehaviour_StartCoroutineManaged_m53873D1C040DB245BF13B74D2781072015B1EB66,
	MonoBehaviour_StartCoroutineManaged2_m114327BBBA9F208E8BA2F8BBF71FA0E8E996F7B8,
	MonoBehaviour_StopCoroutineManaged_mB9F1DBC3C5CCF4F64D5277B87518A54DEF7509C2,
	MonoBehaviour_StopCoroutineFromEnumeratorManaged_mA0D7F798094DF352E354304A50E8D97D9AB6900B,
	MonoBehaviour_GetScriptClassName_mB38B7D00E1629DF772709F844EDB4C20074A208F,
	MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97,
	NULL,
	NULL,
	NoAllocHelpers_SafeLength_m85E794F370BFE9D3954E72480AE6ED358AF5102C,
	NULL,
	NULL,
	NoAllocHelpers_Internal_ResizeList_mE270FB6EBB9A362E2C66B1A93C363C98746D28B6,
	NoAllocHelpers_ExtractArrayFromList_mB4B8B76B4F160975C949FB3E15C1D497DBAE8EDC,
	RangeInt_get_end_m7A5182161CC5454E1C200E0173668572BA7FAAFD,
	RangeInt__ctor_mACFE54DF73DE3F62053F851423525DB5AC1B100E,
	RuntimeInitializeOnLoadMethodAttribute__ctor_mF7E0CAAF0DA4A1F5BAD4CF4C02C4C3A5AB2515D0,
	RuntimeInitializeOnLoadMethodAttribute__ctor_mB64D0D2B5788BC105D3640A22A70FCD5183CB413,
	RuntimeInitializeOnLoadMethodAttribute_set_loadType_m99C91FFBB561C344A90B86F6AF9ED8642CB87532,
	ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B,
	ScriptableObject_CreateInstance_mDC77B7257A5E276CB272D3475B9B473B23A7128D,
	NULL,
	ScriptableObject_CreateScriptableObject_m0DEEBEC415354F586C010E7863AEA64D2F628D0B,
	ScriptableObject_CreateScriptableObjectInstanceFromType_m251F32A1C8B865FBED309AA24B8EAB3A35E2E25C,
	ScriptingUtility_IsManagedCodeWorking_m4E53313183C7CB038C14545B933F81E249E707F3,
	SelectionBaseAttribute__ctor_mD7E83E67AFD9920E70551A353A33CC94D0584E8E,
	StackTraceUtility_SetProjectFolder_m05FBBB2FF161F2F9F8551EB67D44B50F7CC98E21,
	StackTraceUtility_ExtractStackTrace_mEDFB4ECA329B87BC7DF2AA3EF7F9A31DAC052DC0,
	StackTraceUtility_ExtractStringFromExceptionInternal_m1FB3D6414E31C313AC633A24653DA4B1FB59C975,
	StackTraceUtility_ExtractFormattedStackTrace_m02A2ACEEF753617FAAA08B4EA840A49263901660,
	StackTraceUtility__cctor_mDDEE2A2B6EBEDB75E0C28C81AFEDB1E9C372A165,
	UnityException__ctor_m68C827240B217197615D8DA06FD3A443127D81DE,
	UnityException__ctor_mE42363D886E6DD7F075A6AEA689434C8E96722D9,
	UnityException__ctor_m27B11548FE152B9AB9402E54CB6A50A2EE6FFE31,
	UnassignedReferenceException__ctor_mA7A9187C9C01AD3E5D2C6568A907A5FE21D85AB8,
	UnassignedReferenceException__ctor_m9E832DEA7FAAA414155FDA9F033E0128269C0C3B,
	UnassignedReferenceException__ctor_m3B13F71C76EBF3FBA6117BB3B0CD44D2AD6D26C5,
	TextAsset_get_text_mD3FBCD974CF552C7F7C7CD9A07BACAE51A2C5D42,
	TextAsset_ToString_m8C7ED5DD80E20B3A16A2100F62319811BE5DC830,
	UnhandledExceptionHandler_RegisterUECatcher_mE45C6A0301C35F6193F5774B7683683EF78D21DA,
	UnhandledExceptionHandler_HandleUnhandledException_m09FC7ACFE0E555A5815A790856FBF5B0CA50819E,
	UnhandledExceptionHandler_PrintException_m4CBE36F17C3F2B72205DB96B6D1377E4B3D11C77,
	UnhandledExceptionHandler_iOSNativeUnhandledExceptionHandler_mD7444FEA5E5A468B81682D0C10831FD62ED60DC6,
	Object_GetInstanceID_m33A817CEE904B3362C8BAAF02DB45976575CBEF4,
	Object_GetHashCode_mCF9141C6640C2989CD354118673711D5F3741984,
	Object_Equals_m813F5A9FF65C9BC0D6907570C2A9913507D58F32,
	Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534,
	Object_CompareBaseObjects_mE918232D595FB366CE5FAD4411C5FBD86809CC04,
	Object_EnsureRunningOnMainThread_m51EC43D45C0101F31785B31F41118AA5D5EE5A7C,
	Object_IsNativeObjectAlive_m683A8A1607CB2FF5E56EC09C5D150A8DA7D3FF08,
	Object_GetCachedPtr_m8CCFA6D419ADFBA8F9EF83CB45DFD75C2704C4A0,
	Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE,
	Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826,
	Object_Instantiate_mAF9C2662167396DEE640598515B60BE41B9D5082,
	Object_Instantiate_m1B26356F0AC5A811CA6E02F42A9E2F9C0AEA4BF0,
	Object_Instantiate_m17AA3123A55239124BC54A907AEEE509034F0830,
	Object_Instantiate_mFDB3861B1290CA5D04FFBA660789184870BC0560,
	Object_Instantiate_m674B3934708548332899CE953CA56BB696C1C887,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Object_Destroy_m09F51D8BDECFD2E8C618498EF7377029B669030D,
	Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A,
	Object_DestroyImmediate_mFCE7947857C832BCBB366FCCE50072ACAD9A4C51,
	Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446,
	Object_FindObjectsOfType_m3FC26FB3B36525BFBFCCCD1AEEE8A86712A12203,
	Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207,
	Object_get_hideFlags_mCC5D0A1480AC0CDA190A63120B39C2C531428FC8,
	Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0,
	Object_DestroyObject_mE3A40F1CEB3A1DE929CC00A3A0B397C57FCE5492,
	Object_DestroyObject_m5ECBDE6659EB24A3E48B74578F760292E188BFA6,
	Object_FindSceneObjectsOfType_m48C5C906DE6910BF4527D42EA218E7E9A62B9305,
	Object_FindObjectsOfTypeIncludingAssets_mE951AE46100FF6715248470291BBF23DEDB750FA,
	NULL,
	NULL,
	Object_FindObjectsOfTypeAll_m5DEFCAA98B41BD762BA77701A134596C0F854EB1,
	Object_CheckNullArgument_m8D42F516655D770DFEEAA13CF86A2612214AAA9B,
	Object_FindObjectOfType_mCDF38E1667CF4502F60C59709D70B60EF7E408DA,
	Object_ToString_m4EBF621C98D5BFA9C0522C27953BB45AB2430FE1,
	Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C,
	Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1,
	Object_GetOffsetOfInstanceIDInCPlusPlusObject_m7C7130E8611F32F6CC9A47400AC5BDC2BA5E6D23,
	Object_CurrentThreadIsMainThread_mC0AD3A568DAE48B60FFE4BEFFB1DD265A12B7AA3,
	Object_Internal_CloneSingle_m4231A0B9138AC40B76655B772F687CC7E6160C06,
	Object_Internal_CloneSingleWithParent_m32F1D681010B51B98CDBC82E3510FC260D2D1E17,
	Object_Internal_InstantiateSingle_mCC3EB0F918934D233D43DFDB457605C4B248738D,
	Object_Internal_InstantiateSingleWithParent_m4E81B30B4790121D12DE8E91049F353B81BCC87D,
	Object_ToString_m7A4BBACD14901DD0181038A25BED62520D273EDC,
	Object_GetName_m1691C0D50AEBC1C86229AEAC2FBC1EE2DC6B67AF,
	Object_IsPersistent_m213422DA5E58C3717D0A8EB1D6429F6EE24D3A4E,
	Object_SetName_m2CBABC30BA2B93EFF6A39B3295A7AB85901E60E8,
	Object_DoesObjectWithInstanceIDExist_m724FB30225896EC46C735C6BAF06D9370D38F828,
	Object_FindObjectFromInstanceID_m7594ED98F525AAE38FEC80052729ECAF3E821350,
	Object_ForceLoadFromInstanceID_m94B5AF82BBC0041EAF05E71CB95757A469621ADA,
	Object__ctor_m091EBAEBC7919B0391ABDAFB7389ADC12206525B,
	Object__cctor_m14515D6A9B514D3A8590E2CAE4372A0956E8976C,
	Object_Internal_InstantiateSingle_Injected_m04E25C97D5848B7AA54178A5448744A0DEB1B62E,
	Object_Internal_InstantiateSingleWithParent_Injected_m1AB8ACB112874AADE585A35A270ACB12D35CFACE,
	UnitySynchronizationContext__ctor_mCABD0C784640450930DF24FAD73E8AD6D1B52037,
	UnitySynchronizationContext__ctor_m9D104656F4EAE96CB3A40DDA6EDCEBA752664612,
	UnitySynchronizationContext_Send_m25CDC5B5ABF8D55B70EB314AA46923E3CF2AD4B9,
	UnitySynchronizationContext_Post_mB4E900B6E9350E8E944011B6BF3D16C0657375FE,
	UnitySynchronizationContext_CreateCopy_mC20AC170E7947120E65ED75D71889CDAC957A5CD,
	UnitySynchronizationContext_Exec_m07342201E337E047B73C8B3259710820EFF75A9C,
	UnitySynchronizationContext_HasPendingTasks_mBFCAC1697C6B71584E72079A6EDB83D52EC1700A,
	UnitySynchronizationContext_InitializeSynchronizationContext_m0F2A055040D6848FAD84A08DBC410E56B2D9E6A3,
	UnitySynchronizationContext_ExecuteTasks_m027AF329D90D6451B83A2EAF3528C9021800A962,
	UnitySynchronizationContext_ExecutePendingTasks_m74DCC56A938FEECD533CFE58CAC4B8B9ED001122,
	WorkRequest__ctor_mE19AE1779B544378C8CB488F1576BDE618548599,
	WorkRequest_Invoke_m67D71A48794EEBB6B9793E6F1E015DE90C03C1ED,
	WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B,
	WaitForSecondsRealtime_get_waitTime_m6D1B0EDEAFA3DBBBFE1A0CC2D372BAB8EA82E2FB,
	WaitForSecondsRealtime_set_waitTime_m867F4482BEE354E33A6FD9191344D74B9CC8C790,
	WaitForSecondsRealtime_get_keepWaiting_mC257FFC53D5734250B919A8B6BE460A6D8A7CD99,
	WaitForSecondsRealtime__ctor_m775503EC1F4963D8E5BBDD7989B40F6A000E0525,
	YieldInstruction__ctor_mA72AD367FB081E0C2493649C6E8F7CFC592AB620,
	SerializeField__ctor_mEE7F6BB7A9643562D8CEF189848925B74F87DA27,
	NULL,
	NULL,
	ComputeShader_FindKernel_m4CEBD37F96732810C4C370A6249CF460BE1F93A3,
	LowerResBlitTexture_LowerResBlitTextureDontStripMe_mC89EA382E4636DE8BC0E14D1BB024A80C65F5CAF,
	PreloadData_PreloadDataDontStripMe_m68DF1A35E18F9FC43A63F2F990EA9970D4E4A78B,
	SystemInfo_get_operatingSystem_m74AC18E4195899CD3E61258DAED8FBB588B50082,
	SystemInfo_get_operatingSystemFamily_mA35FE1FF2DD6240B2880DC5F642D4A0CC2B58D8D,
	SystemInfo_get_processorType_m7B14D432F490CAC67239862D69F60512E5E2094B,
	SystemInfo_get_processorCount_m0B474484D797FC99BAEAEDC56D4974EDF4067504,
	SystemInfo_get_systemMemorySize_m7599C2A9CA16C15DD72C5563D5C47262CEDE0EB8,
	SystemInfo_get_deviceName_mCA23C1149EF385459B51E0176D70561574B28D38,
	SystemInfo_get_deviceModel_m104FF34E03C33AE2AD84FE4CF8ED501C0AE9458A,
	SystemInfo_get_supportsAccelerometer_m7C2C36431E5CF87C2B6BA1E3B1B135152F89C4AB,
	SystemInfo_get_supportsGyroscope_mAD26B709942E2F8A6C6CE708568DAE4114792850,
	SystemInfo_get_supportsLocationService_m5D232D056D916F19210DC6D6608BF6B7E9415E8F,
	SystemInfo_get_supportsVibration_mDA0CDC28A7813E9614FE5A254C21D903555029D3,
	SystemInfo_get_deviceType_mAFCA02B695EE4E37FA3B87B8C05804B6616E1528,
	SystemInfo_get_graphicsDeviceName_mD9D09CDF695610D3355F28EAD9EE61F7870F52E8,
	SystemInfo_get_graphicsDeviceVendor_m6207F479F5D1D1BC9D770A085B8F3A926ED6141F,
	SystemInfo_get_graphicsDeviceVersion_m64E10307E6A8F0351DB4B2CDA96480AD172F119D,
	SystemInfo_get_supportsShadows_m05F10143737B2C6446A663D48447C2006B8701E3,
	SystemInfo_get_supportsRenderTextures_m52A011F59EDA92779E44D4F49785DC46E2E7F04A,
	SystemInfo_get_supportsRenderToCubemap_mE001B8DA31AC4C3727FDB2B1710346F0F8C547A9,
	SystemInfo_get_supportsImageEffects_m5606438D404910FADC9C50DC29E0649E49B08267,
	SystemInfo_get_supports3DTextures_m6416FF7B8551584CE72D063619F2AAA3F3EBB1C7,
	SystemInfo_get_supportsComputeShaders_m257AEF98322259041D3358B801FD6F34DB13013F,
	SystemInfo_get_supportsSparseTextures_m609B1472F58AA462DE7A80D0303C9FC8F3A189F0,
	SystemInfo_get_supportedRenderTargetCount_mD11B3D770E9DA7EE2F0AC0E9B48299AEDE1B515D,
	SystemInfo_get_supportsStencil_m55DB72FD189FF9C0117AB4E288C2F23BF5E92DC7,
	SystemInfo_IsValidEnumValue_m112F964C57B2311EA910CCA5CE0FFABFFF906740,
	SystemInfo_SupportsTextureFormat_m1FCBD02367A45D11CAA6503715F3AAE24CA98B79,
	SystemInfo_get_npotSupport_m1979DB1E3EA8BDEAE36BEE349B7F2783FED1A90C,
	SystemInfo_get_maxTextureSize_m10A8228B83EE161D0DCCB9FB01279C245C47D0E3,
	SystemInfo_GetOperatingSystem_m01E20879EE653FEAA38F0B042172AB28CD57FA43,
	SystemInfo_GetOperatingSystemFamily_mD20DAFF3A6E6649299A3BCFC845E7EB41BFA1D93,
	SystemInfo_GetProcessorType_mCEB2AE2578AB2C465092C65C718D01C3BA49F209,
	SystemInfo_GetProcessorCount_mF10CAC62D6F1DB8EADD19C5297F4E9897F3AB716,
	SystemInfo_GetPhysicalMemoryMB_m5AFA40651FD22CC6A5EEEA7FDEA0687D9EF46EF2,
	SystemInfo_GetDeviceName_mD1E184CD46DBBB9CD945269502634065C3EF516C,
	SystemInfo_GetDeviceModel_mB039F042DA446DDABACAEA353ABE2EAC1AF635CF,
	SystemInfo_SupportsAccelerometer_m8BEBFA4B3003213D362EB2663C75EB93D7B31FC1,
	SystemInfo_IsGyroAvailable_mD44D668844B7AFF24063BDBB0A2DCC288FBBD382,
	SystemInfo_SupportsLocationService_m5508550E1E56669E91A75575308B002EFC9C98B3,
	SystemInfo_SupportsVibration_m29D4F3B0473CCFF561CC8CC27D3D0D0DC7A4BB32,
	SystemInfo_GetDeviceType_m749955DFFD2554A64E851BD95C41F6C55D6A777E,
	SystemInfo_GetGraphicsDeviceName_m621D518DE8F0D7BC5F02D49AF854FC270DC4322B,
	SystemInfo_GetGraphicsDeviceVendor_mD88346AB78AC1CDA78E5887E9FB9290141A6A570,
	SystemInfo_GetGraphicsDeviceVersion_m7D59AFD88DC43E9EDA7B9A86BE090A13B1408FC0,
	SystemInfo_SupportsShadows_mE90CEC00EBA87DEFAB8E666B02568E07E2E5C771,
	SystemInfo_Supports3DTextures_mB616CBCD08E13A9BAA541CC5445AD074F9E3D7DF,
	SystemInfo_SupportsComputeShaders_mEEA74BF6DA49185642F0E07824298D793904CB67,
	SystemInfo_SupportsSparseTextures_m8D688048ACAD87F00B36DA519FCCD99D2EA3D711,
	SystemInfo_SupportedRenderTargetCount_mE6CA6DE27146900463A1263DC0A5AB835C528F92,
	SystemInfo_SupportsTextureFormatNative_mD028594492646D7AB78A4C2F51CA06F63E665210,
	SystemInfo_GetNPOTSupport_mA49CF1030D03A398B582FFECFD11F3C56E358DE3,
	SystemInfo_GetMaxTextureSize_mC9CBB174AB61833BB9BFD82AD5B23E9096C2D43C,
	SystemInfo_IsFormatSupported_m6941B7C4566DEE1EFFD7F6DCB7BFA701ECF9C1D6,
	SystemInfo_GetCompatibleFormat_mC67F0F547EA28CDE08EA9A6B030082516D189EF3,
	SystemInfo_GetGraphicsFormat_m708339B9A94CEBC02A56629FE41F6809DE267F6C,
	Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8,
	Time_get_timeSinceLevelLoad_mDF4964DE4068B0FEC0F950129C7BEF881D0CF9E0,
	Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E,
	Time_get_unscaledTime_m57F78B855097C5BA632CF9BE60667A9DEBCAA472,
	Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2,
	Time_get_fixedUnscaledDeltaTime_mB0F666A0C4D5B0C73088464FFF1A0B5DDC0A5D9E,
	Time_get_fixedDeltaTime_m76C241EDB6F824713AF57DCECD5765871770FA4C,
	Time_get_smoothDeltaTime_m22F7BB00188785BB7AE979B00CAA96284363C985,
	Time_get_timeScale_m7E198A5814859A08FD0FFE6DD5F7ED5C907719F8,
	Time_set_timeScale_mAB89C3BB5DEE81934159C23F103397A77AC3F4AF,
	Time_get_frameCount_m97573E267B487B8FD4BF37615AFC19BED7B4E436,
	Time_get_realtimeSinceStartup_mCA1086EC9DFCF135F77BC46D3B7127711EA3DE03,
	TouchScreenKeyboard_Internal_Destroy_m6CD4E2343AB4FE54BC23DCFE62A50180CB3634E0,
	TouchScreenKeyboard_Destroy_m916AE9DA740DBD435A5EDD93C6BC55CCEC8310C3,
	TouchScreenKeyboard_Finalize_m684570CC561058F48B51F7E21A144299FBCE4C76,
	TouchScreenKeyboard__ctor_mDF71D45DC0F867825BEB1CDF728927FBB0E07F86,
	TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_mD608B3B2A2159D17A8DF7961FA4EB1694A416973,
	TouchScreenKeyboard_get_isSupported_m9163BAF0764DCDD9CB87E75A296D820D629D31CA,
	TouchScreenKeyboard_get_isInPlaceEditingAllowed_m503AB6CB0DFBD8E7D396C8E552C643F20E4A5D47,
	TouchScreenKeyboard_Open_mF795EEBFEF7DF5E5418CF2BACA02D1C62291B93A,
	TouchScreenKeyboard_get_text_mC025B2F295D315E1A18E7AA54B013A8072A8FEB0,
	TouchScreenKeyboard_set_text_mF72A794EEC3FC19A9701B61A70BCF392D53B0D38,
	TouchScreenKeyboard_set_hideInput_mA9729B01B360BF98153F40B96DDED39534B94840,
	TouchScreenKeyboard_get_active_m35A2928E54273BF16CB19D11665989D894D5C79D,
	TouchScreenKeyboard_set_active_m8D5FDCFA997C5EAD7896F7EB456165498727792D,
	TouchScreenKeyboard_get_status_m17CF606283E9BAF02D76ADC813F63F036FAE33F2,
	TouchScreenKeyboard_set_characterLimit_m97F392EB376881A77F5210CFA6736F0A49F5D57B,
	TouchScreenKeyboard_get_canGetSelection_m8EEC32EA25638ACB54F698EA72450B51BEC9A362,
	TouchScreenKeyboard_get_canSetSelection_m3C9574749CA28A6677C77B8FBE6292B80DF88A10,
	TouchScreenKeyboard_get_selection_m9E2AB5FF388968D946B15A3ECDAE1C3C97115AAD,
	TouchScreenKeyboard_set_selection_m27C5DE6B9DBBBFE2CCA68FA80A600D94337215C7,
	TouchScreenKeyboard_GetSelection_mED761F9161A43CC8E507EA27DB1BD11127C6C896,
	TouchScreenKeyboard_SetSelection_mB728B6A7B07AC33987FB30F7950B3D31E3BA5FBF,
	DrivenRectTransformTracker_Add_m51059F302FBD574E93820E8116283D1608D1AB5A,
	DrivenRectTransformTracker_Clear_m328659F339A4FB519C9A208A685DDED106B6FC89,
	RectTransform_add_reapplyDrivenProperties_mDA1F055B02E43F9041D4198D446D89E00381851E,
	RectTransform_remove_reapplyDrivenProperties_m65A8DB93E1A247A5C8CD880906FF03FEA89E7CEB,
	RectTransform_get_rect_mE5F283FCB99A66403AC1F0607CA49C156D73A15E,
	RectTransform_get_anchorMin_mB62D77CAC5A2A086320638AE7DF08135B7028744,
	RectTransform_set_anchorMin_mE965F5B0902C2554635010A5752728414A57020A,
	RectTransform_get_anchorMax_m1E51C211FBB32326C884375C9F1E8E8221E5C086,
	RectTransform_set_anchorMax_m55EEF00D9E42FE542B5346D7CEDAF9248736F7D3,
	RectTransform_get_anchoredPosition_mCB2171DBADBC572F354CCFE3ACA19F9506F97907,
	RectTransform_set_anchoredPosition_m4DD45DB1A97734A1F3A81E5F259638ECAF35962F,
	RectTransform_get_sizeDelta_mDA0A3E73679143B1B52CE2F9A417F90CB9F3DAFF,
	RectTransform_set_sizeDelta_m7729BA56325BA667F0F7D60D642124F7909F1302,
	RectTransform_get_pivot_mA5BEEE2069ACA7C0C717530EED3E7D811D46C463,
	RectTransform_set_pivot_mB791A383B3C870B9CBD7BC51B2C95711C88E9DCF,
	RectTransform_get_anchoredPosition3D_mD8D0568957927A07C38FA922F72264D75AE6CC11,
	RectTransform_set_anchoredPosition3D_m8C889FB130AF8F4B8213CB1A37ECE109E4821504,
	RectTransform_get_offsetMin_mC1C5E8A4F5B830C74C46473E9C6A1B82370CDA4B,
	RectTransform_set_offsetMin_m7455ED64FF16C597E648E022BA768CFDCF4531AC,
	RectTransform_get_offsetMax_m7E2F8892E1E3352C3FF80747352D29F367B3403E,
	RectTransform_set_offsetMax_mD55D44AD4740C79B5C2C83C60B0C38BF1090501C,
	RectTransform_get_drivenByObject_m0B47BB55BEBD4403AEF062FF5C4FBDC3E36C3A0F,
	RectTransform_set_drivenByObject_m98936E2689227990746D7311DA74225175E62603,
	RectTransform_get_drivenProperties_m8D3938BF88887E011FDD263C769F18D79FB68AF2,
	RectTransform_set_drivenProperties_m62A6117C1FC0682E6A7F53851737AB67D3EAEAEF,
	RectTransform_ForceUpdateRectTransforms_m93EE987D36C4DDC3FAEA540486CD7D21F8C7AD6F,
	RectTransform_GetLocalCorners_m8761EA5FFE1F36041809D10D8AD7BC40CF06A520,
	RectTransform_GetWorldCorners_m073AA4D13C51C5654A5983EE3FE7E2E60F7761B6,
	RectTransform_SetInsetAndSizeFromParentEdge_m4ED849AA88D194A7AA6B1021DF119B926F322146,
	RectTransform_SetSizeWithCurrentAnchors_m6F93CD5B798E4A53F2085862EA1B4021AEAA6745,
	RectTransform_SendReapplyDrivenProperties_m231712A8CDDCA340752CB72690FE808111286653,
	RectTransform_GetRectInParentSpace_m645EE1AA5702A6E663C6131EA4B8D0B218F71204,
	RectTransform_GetParentSize_mFD24CC863A4D7DFBFFE23C982E9A11E9B010D25D,
	RectTransform__ctor_mAD37F57C9C6BED97B57B15061BC56C8E89B54D23,
	RectTransform_get_rect_Injected_m94E98A7B55F470FD170EBDA2D47E44CF919CD89F,
	RectTransform_get_anchorMin_Injected_m11D468B602FFF89A8CC25685C71ACAA36609EF94,
	RectTransform_set_anchorMin_Injected_m8BACA1B777D070E5FF42D1D8B1D7A52FFCD59E40,
	RectTransform_get_anchorMax_Injected_m61CE870627E5CB0EFAD38D5F207BC36879378DDD,
	RectTransform_set_anchorMax_Injected_m6BC717A6F528E130AD92994FB9A3614195FBFFB2,
	RectTransform_get_anchoredPosition_Injected_mCCBAEBA5DD529E03D42194FDE9C2AD1FBA8194E8,
	RectTransform_set_anchoredPosition_Injected_mCAEE3D22ADA5AF1CB4E37813EB68BF554220DAEF,
	RectTransform_get_sizeDelta_Injected_m225C77C72E97B59C07693E339E0132E9547F8982,
	RectTransform_set_sizeDelta_Injected_mD2B6AE8C7BB4DE09F1C62B4A853E592EEF9E8399,
	RectTransform_get_pivot_Injected_m23095C3331BE90EB3EEFFDAE0DAD791E79485F7F,
	RectTransform_set_pivot_Injected_m2914C16D5516DE065A932CB43B61C6EEA3C5D3D3,
	ReapplyDrivenProperties__ctor_m0633C1B8E1B058DF2C3648C96D3E4DC006A76CA7,
	ReapplyDrivenProperties_Invoke_m37F24671E6F3C60B3D0C7E0CE234B8E125D5928A,
	ReapplyDrivenProperties_BeginInvoke_m5C352648167A36402E86BEDDCAE8FA4784C03604,
	ReapplyDrivenProperties_EndInvoke_mC8DF7BD47EC8976C477491961F44912BA4C0CD7C,
	Transform__ctor_mE8E10A06C8922623BAC6053550D19B2E297C2F35,
	Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294,
	Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC,
	Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8,
	Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728,
	Transform_get_eulerAngles_mF2D798FA8B18F7A1A0C4A2198329ADBAF07E37CA,
	Transform_get_localEulerAngles_m445AD7F6706B0BDABA8A875C899EB1E1DF1A2A2B,
	Transform_get_up_m3E443F6EB278D547946E80D77065A871BEEEE282,
	Transform_get_forward_m0BE1E88B86049ADA39391C3ACED2314A624BC67F,
	Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9,
	Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935,
	Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE,
	Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A,
	Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA,
	Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556,
	Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403,
	Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E,
	Transform_get_parentInternal_mEE407FBF144B4EE785164788FD455CAA82DC7C2E,
	Transform_set_parentInternal_m8534EFFADCF054FFA081769F84256F9921B0258C,
	Transform_GetParent_m1C9AFA68C014287E3D62A496A5F9AE16EF9BD7E6,
	Transform_SetParent_mFAF9209CAB6A864552074BA065D740924A4BF979,
	Transform_SetParent_m268E3814921D90882EFECE244A797264DE2A5E35,
	Transform_get_worldToLocalMatrix_m4791F881839B1087B17DC126FC0CA7F9A596073E,
	Transform_get_localToWorldMatrix_mBC86B8C7BA6F53DAB8E0120D77729166399A0EED,
	Transform_Translate_m91072CBFB456E51FC3435D890E3F7E6A04F4BABD,
	Transform_Translate_mB8C4818A228496C1ADD6C2CA5895D765C94A2EF5,
	Transform_Rotate_m3424566A0D19A1487AE3A82B08C47F2A2D2A26CB,
	Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF,
	Transform_TransformPoint_mA96DC2A20EE7F4F915F7509863A18D99F5DD76CB,
	Transform_TransformPoint_m363B3A9E2C3A9A52F4B872CF34F476D87CCC8CEC,
	Transform_InverseTransformPoint_mB6E3145F20B531B4A781C194BAC43A8255C96C47,
	Transform_get_childCount_m7665D779DCDB6B175FB52A254276CDF0C384A724,
	Transform_SetAsFirstSibling_m2CAD80F7C9D89EE145BC9D3D0937D6EBEE909531,
	Transform_SetAsLastSibling_mE3DD5E6421A08BACF1E86FC0EC7EE3AFA262A990,
	Transform_SetSiblingIndex_m108C43B950B2279BF46C914F1A428A9DE0AC0FD2,
	Transform_GetSiblingIndex_m6FEF9F4DAB8BEAB964A806F3CEE387C1F462B4C1,
	Transform_FindRelativeTransformWithPath_mE13AC72C52AEA193FA2BED0BDE2BF24CEAC13186,
	Transform_Find_m673797B6329C2669A543904532ABA1680DA4EAD1,
	Transform_get_lossyScale_m9C2597B28BE066FC061B7D7508750E5D5EA9850F,
	Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80,
	Transform_set_hasChanged_mA2BDCABEEF63468A4CF354CB7ACFDCC8E910A3D9,
	Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC,
	Transform_GetChild_mC86B9B61E4EC086A571B09EA7A33FFBF50DF52D3,
	Transform_get_position_Injected_mFD1BD0E2D17761BA08289ABBB4F87EDFFF7C1EBB,
	Transform_set_position_Injected_mB6BEBF6B460A566E933ED59C4470ED58D81B3226,
	Transform_get_localPosition_Injected_mC1E8F9DAC652621188ABFB58571782157E4C8FBA,
	Transform_set_localPosition_Injected_m8B4E45BAADCDD69683EB6424992FC9B9045927DE,
	Transform_get_rotation_Injected_m41BEC8ACE323E571978CED341997B1174340701B,
	Transform_set_rotation_Injected_m1B409EA2BBF0C5DEE05153F4CD5134669AA2E5C0,
	Transform_get_localRotation_Injected_m1ADF4910B326BAA828892B3ADC5AD1A332DE963B,
	Transform_set_localRotation_Injected_mF84F8CFA00AABFB7520AB782BA8A6E4BBF24FDD5,
	Transform_get_localScale_Injected_mA8987BAB5DA11154A22E2B36995C7328792371BE,
	Transform_set_localScale_Injected_m9BF22FF0CD55A5008834951B58BB8E70D6982AB2,
	Transform_get_worldToLocalMatrix_Injected_mFEC701DE6F97A22DF1718EB82FBE3C3E62447936,
	Transform_get_localToWorldMatrix_Injected_mF5629FA21895EB6851367E1636283C7FB70333A9,
	Transform_TransformDirection_Injected_m5E7C9D4E879820DF32F1CB1DE1504C59B8E98943,
	Transform_TransformPoint_Injected_mB697D04DF989E68C8AAFAE6BFBBE718B68CB477D,
	Transform_InverseTransformPoint_Injected_m320ED08EABA9713FDF7BDAD425630D567D39AB1D,
	Transform_get_lossyScale_Injected_m6521BCE12BE0D202E15CDC24EC11304CD837EAE4,
	Enumerator__ctor_mBF5A46090D26A1DD98484C00389566FD8CB80770,
	Enumerator_get_Current_mD91FA41B0959224F24BF83051D46FCF3AF82F773,
	Enumerator_MoveNext_mF27E895DC4BB3826D2F00E9484A9ECC635770031,
	Enumerator_Reset_mA4AD59858E0D61FE247C0E158737A4C02FCE244F,
	SpriteRenderer_get_color_m1456AB27D5B09F28A273EC0BBD4F03A0FDA51E99,
	SpriteRenderer_set_color_m0729526C86891ADD11611CD13A7A18B851355580,
	SpriteRenderer_get_color_Injected_m06403F5B2B080BA7609454575BC793E00B1C0870,
	SpriteRenderer_set_color_Injected_mFE58729552E9A143B3C8EA7B89827126DAB3DB90,
	Sprite__ctor_m8559FBC54BD7CDA181B190797CC8AC6FB1310F9E,
	Sprite_GetPackingMode_mF0507D88752CDA45A9283445067070092E524317,
	Sprite_GetPackingRotation_mEB28589988DEEA60B74E3239182B2F7E4EEE4A6D,
	Sprite_GetPacked_mBDE07283B07E7FB8892D309C5EDC81584C849BCC,
	Sprite_GetTextureRect_mE506ABF33181E32E82B75479EE4A0910350B1BF9,
	Sprite_GetTextureRectOffset_m6CC9A016E5B4D1A665E2E87EC132F9FAC8177FD6,
	Sprite_GetInnerUVs_m273E051E7DF38ED3D6077781D75A1C1019CABA25,
	Sprite_GetOuterUVs_mD78E47470B4D8AD231F194E256136B0094ECEBC5,
	Sprite_GetPadding_m5781452D40FAE3B7D0CE78BF8808637FBFE78105,
	Sprite_CreateSpriteWithoutTextureScripting_mBE41B7AA3AA952B14E7DCC600AA27728AC165C6E,
	Sprite_CreateSprite_m1F676037B8C5EB3E216F70EE4630662FF144AFC5,
	Sprite_get_bounds_mD440465B889CCD2D80D118F9174FD5EAB19AE874,
	Sprite_get_rect_mF1D59ED35D077D9B5075E2114605FDEB728D3AFF,
	Sprite_get_border_m940E803CAD380B3B1B88371D7A4E74DF9A48604F,
	Sprite_get_texture_mA1FF8462BBB398DC8B3F0F92B2AB41BDA6AF69A5,
	Sprite_get_pixelsPerUnit_m54E3B43BD3D255D18CAE3DC8D963A81846983030,
	Sprite_get_spriteAtlasTextureScale_m8E2E3B0967DA89F39C725A8E0F4C27EE8C1EAF7C,
	Sprite_get_associatedAlphaSplitTexture_m2EF38CF62616FBBBBAE7876ECBCC596DB3F42156,
	Sprite_get_pivot_m8E3D24C208E01EC8464B0E63FBF3FB9429E4C1D9,
	Sprite_get_packed_m501A9E7D2C44867665FB579FD1A8C5D397C872C3,
	Sprite_get_packingMode_m1B5AA0F5476DAEADFF14F65E99944B54940D869F,
	Sprite_get_packingRotation_m47E139421BE43B85B69CE5A2C41D287E14CF037B,
	Sprite_get_textureRect_m8CDA38796589CB967909F78076E7138907814DCD,
	Sprite_get_textureRectOffset_m7E467306131323AA9240A7A1F4F408637FC33741,
	Sprite_get_vertices_mD858385A07239A56691D1559728B1B5765C32722,
	Sprite_get_triangles_m3B0A097930B40C800E0591E5B095D118D2A33D2E,
	Sprite_get_uv_mBD484CDCD2DF54AAE452ADBA927806193CB0FE84,
	Sprite_GetPhysicsShapeCount_m6042C613B97839AFFCCB92CBC74E7B0255FCC86A,
	Sprite_GetPhysicsShapePointCount_m8298AD1731EB569E745DA49639953282A10F3BED,
	Sprite_Internal_GetPhysicsShapePointCount_m9C7BA414628B5D7B83D466A5F32E5E58BA5E648A,
	Sprite_GetPhysicsShape_mD719BB7A84247FB5734E835AD0128465D6DE9F6B,
	Sprite_GetPhysicsShapeImpl_mDFC2D7CD8320C0EA9BAE239F8A1B4F31718FC96D,
	Sprite_OverridePhysicsShape_m3C19FF12D36CCEFDF8AB9D64F5C8A2B0A24A358A,
	Sprite_OverridePhysicsShapeCount_m97519C52CFD40529A10AD9E418ED4337A7EEE9E8,
	Sprite_OverridePhysicsShape_m89C6D93927591E283E6769746727AC37B1F2C657,
	Sprite_OverrideGeometry_m181A7790256B034360A8AA2DBDA4E7FAEA1B37EE,
	Sprite_Create_m58E6434E5CFE97E932AA45CBCE45226C75DB1F60,
	Sprite_Create_m4ECCFC2D3CA4DE742ECD9E4622967F7A4782877D,
	Sprite_Create_mE9E237E1E936F7A7398361253D1D7C7B7ECD622F,
	Sprite_Create_m4BF8E8E0F77846FA88502FD896E3AFB6D2AB2E0A,
	Sprite_Create_mBF0676DF6C3CB8554451DC01E0713CF73C84DB4A,
	Sprite_Create_mE9C9B96A1D1EC541FF41148E2001223C2030F803,
	Sprite_Create_m84A724DB0F0D73AEBE5296B4324D61EBCA72A843,
	Sprite_Create_m9ED36DA8DA0637F93BA2753A16405EB0F7B17A3C,
	Sprite_GetTextureRect_Injected_m3D0143FD7E689267FAE3164F7C149DB5FF3384C2,
	Sprite_GetTextureRectOffset_Injected_mF1E684EA58CEE5FDD1706D13F12C4C2A15314AF0,
	Sprite_GetInnerUVs_Injected_m19AF3A32647EE2153374B4B58CB248A5E3715F6B,
	Sprite_GetOuterUVs_Injected_m57E56A2D7686D47E6948511F102AF8135E98B2B0,
	Sprite_GetPadding_Injected_m843873F288F8CBC4BDDF1BBE20211405039ABBDC,
	Sprite_CreateSpriteWithoutTextureScripting_Injected_m8E5F54D85D60860C93D2A0C77AF8193CFB6C3472,
	Sprite_CreateSprite_Injected_m5393FA2042C8AF5D7E9CC32A2255270EE3DA702D,
	Sprite_get_bounds_Injected_m6422C2DBFD84A7B7F921DCA14BDFF2157738194B,
	Sprite_get_rect_Injected_mABF4FCC2AEDD9EE874797E35EDEFF023A52F5830,
	Sprite_get_border_Injected_mA56DD9A38B61783341DF35C808FBFE93A1BD4BB6,
	Sprite_get_pivot_Injected_m526201DCD812D7AB10AACE35E4195F7E53B633EC,
	APIUpdaterRuntimeHelpers_GetMovedFromAttributeDataForType_m2574674719979232087612C3C17A760E439BCA45,
	APIUpdaterRuntimeHelpers_GetObsoleteTypeRedirection_m43E0605422153F402426F8959BC2E8C65A69F597,
	DataUtility_GetInnerUV_m19FC4FF27A6733C9595B63F265EFBEC3BC183732,
	DataUtility_GetOuterUV_mB7DEA861925EECF861EEB643145C54E8BF449213,
	DataUtility_GetPadding_mE967167C8AB44F752F7C3A7B9D0A2789F5BD7034,
	DataUtility_GetMinSize_m8031F50000ACDDD00E1CE4C765FA4B0A2E9255AD,
	SpriteAtlasManager_RequestAtlas_m792F61C44C634D9E8F1E15401C8CECB7A12F5DDE,
	SpriteAtlasManager_add_atlasRegistered_m6742D91F217B69CC2A65D80B5F25CFA372A1E2DA,
	SpriteAtlasManager_remove_atlasRegistered_m55564CC2797E687E4B966DC1797D059ABBB04051,
	SpriteAtlasManager_PostRegisteredAtlas_m2FCA85EDC754279C0A90CC3AF5E12C3E8F6A61CB,
	SpriteAtlasManager_Register_m2C324F6E122AF09D44E4EE3F8F024323663670D2,
	SpriteAtlasManager__cctor_m826C9096AB53C9C6CFCF342FA9FDC345A726B6C6,
	SpriteAtlas_CanBindTo_m6CE0E011A4C5F489F9A62317380FB35F20DF7016,
	SpriteAtlas_GetSprite_m1AD3114039CC055C4F4E0EF93ADD53BA2FE44DFE,
	Profiler_get_supported_m4678D0BD23545682A1FB6D8581E3B1AD25EEC4F2,
	Profiler_get_enabled_m0C4E87308897B1FF9485999C112E56D750E5CDFE,
	Profiler_set_enabled_m4F366847649387E07AA2C482798915F5B4999B34,
	Profiler_BeginSample_mDA159D5771838F40FC7C2829B2DC4329F8C9596B,
	Profiler_ValidateArguments_m212D52E66EEBBFC4A04C9FF0CE27388E0C605CB0,
	Profiler_BeginSampleImpl_m831FE453CD200A1F5C3C49DB9E0D831C89B70751,
	Profiler_EndSample_m15350A0463FB3C5789504B4059B0EA68D5B70A21,
	Profiler_GetMonoHeapSizeLong_m448556C394B68156E0C09142F048DC91FA49E1C0,
	Profiler_GetMonoUsedSizeLong_m7D63E9E033B31718C3E5D96404DF5A7609445C77,
	Profiler_GetTotalAllocatedMemoryLong_m8B81D9312DC29887D552EEAFBFA95794A4D4EBD1,
	Profiler_GetTotalReservedMemoryLong_m3A8A4373D1D4A9539EDD1E9F61E514407422FE79,
	DebugScreenCapture_set_rawImageDataReference_mE09725CEBC8D7A846033531E33C8E9B8DA60DFDE,
	DebugScreenCapture_set_imageFormat_m779E16070B55AACD76279BFDDAE5CC470667D3FB,
	DebugScreenCapture_set_width_mD8B3C521091CD0049D323410FFD201D47B629127,
	DebugScreenCapture_set_height_mD7E3596B40E77F906A3E9517BC6CE795C0609442,
	MetaData__ctor_m1852CAF4EDFB43F1ABCE37819D9963CEE959A620,
	MemoryProfiler_PrepareMetadata_mDFBA7A9960E5B4DF4500092638CD59EB558DD42C,
	MemoryProfiler_WriteIntToByteArray_mBC872709F6A09ADFE716F41C459C1FCC1EFF25A0,
	MemoryProfiler_WriteStringToByteArray_mCAC0D283F16F612E4796C539994FDB487A048332,
	MemoryProfiler_FinalizeSnapshot_m48FD62744888BBF0A9B13826622041226C8B9AD7,
	MemoryProfiler_SaveScreenshotToDisk_mCA2AE332D689BEDE49DECACD92FDA366EB80047F,
	Device_get_generation_m1B212A72D6BB18EAED4D65DA89702D69A22A3F99,
	Device_IsAdvertisingTrackingEnabled_m8C6D05852D4437E00EF5693201863B55971B90D3,
	Device_get_advertisingTrackingEnabled_mC133F2C934664A3FDD03A8E060BB522115EB3AFF,
	LocalNotification__cctor_m9E47ADEC8F786289F7C94ACC65A44C318FF59685,
	ArgumentCache_get_unityObjectArgument_m89597514712FB91B443B9FB1A44D099F021DCFA5,
	ArgumentCache_get_unityObjectArgumentAssemblyTypeName_mD54EA1424879A2E07B179AE93D374100259FF534,
	ArgumentCache_get_intArgument_mD9D072A7856D998847F159350EAD0D9AA552F76B,
	ArgumentCache_get_floatArgument_mDDAD91A992319CF1FFFDD4F0F87C5D162BFF187A,
	ArgumentCache_get_stringArgument_mCD1D05766E21EEFDFD03D4DE66C088CF29F84D00,
	ArgumentCache_get_boolArgument_mCCAB5FB41B0F1C8C8A2C31FB3D46DF99A7A71BE2,
	ArgumentCache_TidyAssemblyTypeName_m2D4AFE74BEB5F32B14165BB52F6AB29A75306936,
	ArgumentCache_OnBeforeSerialize_mCBE8301FE0DDE2E516A18051BBE3DC983BC80FBC,
	ArgumentCache_OnAfterDeserialize_m59072D771A42C518FECECBE2CE7EE9E15F4BE55F,
	ArgumentCache__ctor_m9618D660E40F991782873643F0FDCACA32A63541,
	BaseInvokableCall__ctor_m232CE2068209113988BB35B50A2965FC03FC4A58,
	BaseInvokableCall__ctor_m71AC21A8840CE45C2600FF784E8B0B556D7B2BA5,
	NULL,
	NULL,
	BaseInvokableCall_AllowInvoke_m0B193EBF1EF138FC5354933974DD702D3D9FF091,
	NULL,
	InvokableCall_add_Delegate_mCE91CE04CF7A8B962FF566B018C8C516351AD0F3,
	InvokableCall_remove_Delegate_m0CFD9A25842A757309236C500089752BF544E3C7,
	InvokableCall__ctor_m2F9F0CD09FCFFEBCBBA87EC75D9BA50058C5B873,
	InvokableCall__ctor_m77F593E751D2119119A5F3FD39F8F5D3B653102B,
	InvokableCall_Invoke_mDB8C26B441658DDA48AC3AF259F4A0EBCF673FD0,
	InvokableCall_Invoke_m0B9E7F14A2C67AB51F01745BD2C6C423114C9394,
	InvokableCall_Find_mDC13296B10EFCD0A92E486CD5787E07890C7B8CC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PersistentCall_get_target_mCAD7D486F28743D49DCF268B791721313A7FC8C5,
	PersistentCall_get_methodName_m164BE545C327516CABE9464D88A417B7F00010E1,
	PersistentCall_get_mode_mC88324F8D18B5E4E79045AE1F99DF3EB36CEE644,
	PersistentCall_get_arguments_m53AFE12B586F0C8948D60852866EC71F38B3BAE1,
	PersistentCall_IsValid_mF3E3A11AF1547E84B2AC78AFAF6B2907C9B159AE,
	PersistentCall_GetRuntimeCall_m68F27109F868C451A47DAC3863B27839C515C3A6,
	PersistentCall_GetObjectCall_m895EBE1B8E2A4FB297A8C268371CA4CD320F72C9,
	PersistentCall__ctor_mBF65325BE6B4EBC6B3E8ADAD3C6FA77EF5BBEFA8,
	PersistentCallGroup__ctor_m46B7802855B55149B9C1ECD9C9C97B8025BF2D7A,
	PersistentCallGroup_Initialize_m9F47B3D16F78FD424D50E9AE41EB066BA9C681A3,
	InvokableCallList_AddPersistentInvokableCall_m62BDD6521FB7B68B58673D5C5B1117FCE4826CAD,
	InvokableCallList_AddListener_mE4069F40E8762EF21140D688175D7A4E46FD2E83,
	InvokableCallList_RemoveListener_mC886122D45A6682A85066E48637339065085D6DE,
	InvokableCallList_ClearPersistent_m4038DB499DCD84B79C0F1A698AAA7B9519553D49,
	InvokableCallList_PrepareInvoke_m5BB28A5FBF10C84ECF5B52EBC52F9FCCDC840DC1,
	InvokableCallList__ctor_m8A5D49D58DCCC3D962D84C6DAD703DCABE74EC2D,
	UnityEventBase__ctor_m57AF08DAFA9C1B4F4C8DA855116900BAE8DF9595,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m147F610873545A23E9005CCB35CA6A05887E7599,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3A87E89948C5FF32BD5BA1BDD1A4D791738A141E,
	NULL,
	NULL,
	UnityEventBase_FindMethod_m4E838DE0D107C86C7CAA5B05D4683066E9EB9C32,
	UnityEventBase_FindMethod_m82A135403D677ECE42CEE42A322E15EB2EA53797,
	UnityEventBase_DirtyPersistentCalls_m31D9B2D3B265718318F4D7E87373E53F249E65EB,
	UnityEventBase_RebuildPersistentCallsIfNeeded_mFC89AED628B42E5B1CBCC702222A6DF97605234F,
	UnityEventBase_AddCall_mD45F68C1A40E2BD9B0754490B7709846B84E8075,
	UnityEventBase_RemoveListener_mE7EBC544115373D2357599AC07F41F13A8C5A49E,
	UnityEventBase_PrepareInvoke_mFA3E2C97DB776A1089DCC85C9F1DA75C295032AE,
	UnityEventBase_ToString_m7672D78CA070AC49FFF04E645523864C300DD66D,
	UnityEventBase_GetValidMethodInfo_m4521621AB72C7265A2C0EC6911BE4DC42A99B6A5,
	UnityAction__ctor_mEFC4B92529CE83DF72501F92E07EC5598C54BDAC,
	UnityAction_Invoke_mC9FF5AA1F82FDE635B3B6644CE71C94C31C3E71A,
	UnityAction_BeginInvoke_m6819B1057D192033B17EB15C9E34305720F0401C,
	UnityAction_EndInvoke_m9C465516D5977EF185DCEB6CA81D0B90EDAD7370,
	UnityEvent__ctor_m2F8C02F28E289CA65598FF4FA8EAB84D955FF028,
	UnityEvent_AddListener_m31973FDDC5BB0B2828AB6EF519EC4FD6563499C9,
	UnityEvent_RemoveListener_m26034605306E868B2E332675FCCBDA37CECBBA19,
	UnityEvent_FindMethod_Impl_mC96F40A83BB4D1430E254DAE9B091E27E42E8796,
	UnityEvent_GetDelegate_m8D277E2D713BB3605B3D46E5A3DB708B6A338EB0,
	UnityEvent_GetDelegate_mDFBD636D71E24D75D0851959256A3B97BA842865,
	UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FormerlySerializedAsAttribute__ctor_m770651B828F499F804DB06A777E8A4103A3ED2BD,
	FormerlySerializedAsAttribute_get_oldName_m501D396073FA321AF7326D5C5915333BF67AFD16,
	PreserveAttribute__ctor_mD842EE86496947B39FE0FBC67393CE4401AC53AA,
	MovedFromAttributeData_Set_m244D6454BEA753FA4C7C3F2393A5DADCB3166B1C,
	MovedFromAttribute__ctor_mF87685F9D527910B31D3EF58F0EAA297E1944B42,
	Scene_IsValidInternal_mB85F25DED2B4D8B3E15D1CD6F7A7B354CEA43E74,
	Scene_GetPathInternal_mC727722DC9EA785029CE73DF89E12B9F15DCBC8A,
	Scene_GetNameInternal_mE5393E58A37BC0031702DFBA6AEC577058D2941E,
	Scene_GetIsLoadedInternal_m6D1D452018259662F506EBB33BFD3B55A613E329,
	Scene_GetBuildIndexInternal_m49FEDB2B2DE47388E130C1FE9B9945EED2BC10BA,
	Scene_get_handle_mFAB5C41D41B90B9CEBB3918A6F3638BD41E980C9,
	Scene_IsValid_mDA115AE2634680CAE92D2F844347AB4CD2185437,
	Scene_get_path_m6D313F8791A3376EBE73AA6F62C02731F7F48A86,
	Scene_get_name_m0E63ED0F050FCC35A4216220C584BE3D3F77B0E1,
	Scene_get_isLoaded_m6A733EA533B0DC0041A9579A95530B41F869435C,
	Scene_get_buildIndex_m764659943B7357F5D6C9165F68EDCFBBDDD6C3C2,
	Scene_op_Equality_m73EF8193688F46ECBBF5DF15436F05CB8A94D501,
	Scene_GetHashCode_m65BBB604A5496CF1F2C129860F45D0E437499E34,
	Scene_Equals_mD5738AF0B92757DED12A90F136716A5E2DDE3F54,
	SceneManagerAPIInternal_LoadSceneAsyncNameIndexInternal_m91FE2DD2BD955B3230E7686CB70DA238AEE0C485,
	SceneManagerAPIInternal_LoadSceneAsyncNameIndexInternal_Injected_m0F87909CFE5D0A027E72093D3A8E065CC4ED052C,
	SceneManager_get_sceneCount_m21F0C1A9DB4F6105154E7FAEE9461805F3EFAD84,
	SceneManager_get_sceneCountInBuildSettings_mDEF095420E9AC2FDA96136F51B55254CE11ED529,
	SceneManager_GetActiveScene_mD583193D329EBF540D8AB8A062681B1C2AB5EA51,
	SceneManager_GetSceneByName_m99A07E2CCD62F07E67967BDCF16AA6BA97BC20A6,
	SceneManager_GetSceneByBuildIndex_mEC753530087666DAA2C248E143C1BE948251805B,
	SceneManager_GetSceneAt_m2D4105040A31A5A42E79A4E617028E84FC357C8A,
	SceneManager_UnloadSceneAsyncInternal_m0C5D270E775F3D7301737E5A941C9757695B4EEE,
	SceneManager_LoadSceneAsyncNameIndexInternal_m542937B4FCCE8B1AAC326E1E1F9060ECEDCB6159,
	SceneManager_add_sceneLoaded_mB72463B21F0D89F168C58E994356298D0E38A4F7,
	SceneManager_remove_sceneLoaded_m894CC4AE20DC49FF43CF6B2A614877F50D707C92,
	SceneManager_add_sceneUnloaded_m14FAB322E1A8BC4981CFC9799D4CD68481377C8A,
	SceneManager_remove_sceneUnloaded_mEB3226CE2741E6782E8055436FC51A6A74BBFBAF,
	SceneManager_add_activeSceneChanged_m0D135E92F11B46E5B46344EEB779402E833FFA86,
	SceneManager_remove_activeSceneChanged_mC680C450DB11B2ACEE12B87D9F41DEBF2C1F8159,
	SceneManager_LoadScene_m258051AAA1489D2D8B252815A45C1E9A2C097201,
	SceneManager_LoadScene_m8C95E38F5497F502DAEDC316F6E1C64B36EDC652,
	SceneManager_LoadSceneAsync_mBB8889B3326DF7DD64983AE44FF5D140670C29B9,
	SceneManager_LoadSceneAsync_m8B6EC55B2DE8C281A5D4B2CE88531D8573AF39EE,
	SceneManager_UnloadSceneAsync_mE1691A3988A86BABB0A86F189285A33011722A4A,
	SceneManager_Internal_SceneLoaded_m800F5F7F7B30D5206913EF65548FD7F8DE9EF718,
	SceneManager_Internal_SceneUnloaded_m32721E87A02DAC634DD4B9857092CC172EE8CB98,
	SceneManager_Internal_ActiveSceneChanged_mE9AB93D6979594CFCED5B3696F727B7D5E6B25F5,
	SceneManager__cctor_m36C7C4EECB3F22A383FA829341F6A4EA0B2D3377,
	SceneManager_GetActiveScene_Injected_m1A7248F1FDCB9E3BFC13E4AA4849AA3FB5B911C4,
	SceneManager_GetSceneByName_Injected_m02D1100582A883A40F82E8E620D8DF04D3E89DA8,
	SceneManager_GetSceneByBuildIndex_Injected_m5693DD7A5A8CF9FBFC01CC49C61F98F14622BD9F,
	SceneManager_GetSceneAt_Injected_m7DB39BC8E659D73DEE24D5F7F4D382CBB0B31148,
	SceneManager_UnloadSceneAsyncInternal_Injected_mCB0FB993506CFFD796C3D0F4954023167A188BC0,
	LoadSceneParameters_set_loadSceneMode_mD56FFF214E3909A68BD9C485BBB4D99ADC3A5624,
	LoadSceneParameters__ctor_m2E00BEC64DCC48351831B7BE9088D9B6AF62102A,
	UpdateFunction__ctor_m203C45C9226ED025C1369B78B759C952ABDA630A,
	UpdateFunction_Invoke_m6C0E9E5082FCEEF018602FD40A43E613360D410D,
	UpdateFunction_BeginInvoke_m7261B0AA3E858CC7A24FF343DE292DB5A34DAC0C,
	UpdateFunction_EndInvoke_m49FBADEA0EED0F50341E2E1F5F21349C2059EA55,
	MessageEventArgs__ctor_m436B854CFEEDB949F4D9ACAEA2E512BDAEDC6E1B,
	PlayerConnection_get_instance_mF51FB76C702C7CDD0BAEAD466060E3BDF23D390F,
	PlayerConnection_get_isConnected_mB902603E2C8CA93299FF0B28E13A9D594CBFE14E,
	PlayerConnection_CreateInstance_m6325767D9D05B530116767E164CDCBE613A5787F,
	PlayerConnection_OnEnable_m9D8136CEB952BC0F44A46A212BF2E91E5A769954,
	PlayerConnection_GetConnectionNativeApi_mC88D9972FDF9D4FF15CC8C4BB6CA633FB114D918,
	PlayerConnection_Register_m9B203230984995ADF5E19E50C3D7DF7E21036FF2,
	PlayerConnection_Unregister_m8EB88437DF970AA6627BC301C54A859DAED70534,
	PlayerConnection_RegisterConnection_m7E54302209A4F3FB3E27A0E7FEB8ADE32C100F1B,
	PlayerConnection_RegisterDisconnection_m556075060F55D3FA7F44DEB4B34CE1070ECBF823,
	PlayerConnection_UnregisterConnection_mC6A080D398CD6C267C1638D98F7485E2B837D029,
	PlayerConnection_UnregisterDisconnection_m148453805654D809DB02F377D0FBC4524F63EBF6,
	PlayerConnection_Send_m1CDF41319A60A5940B487D08ECE14D0B61EDE6AC,
	PlayerConnection_TrySend_m3C0D0208A6A8A7F7FF93AF155A71B726ABE8D662,
	PlayerConnection_BlockUntilRecvMsg_mFCF2DB02D6F07C0A69C0412D8A3F596AF4AC54A2,
	PlayerConnection_DisconnectAll_m278A4B90D90892338D1B41F5A59CD7C519F1C8D2,
	PlayerConnection_MessageCallbackInternal_m3E9A847ED82FDA9ABB680F81595A876450EFB166,
	PlayerConnection_ConnectedCallbackInternal_mFEC88D604DE3923849942994ED873B26CEEDDA3D,
	PlayerConnection_DisconnectedCallback_m2A12A748DDACDD3877D01D7F38ABBC55DEE26A56,
	PlayerConnection__ctor_m3E1248C28C3082C592C2E5F69778F31F6610D93D,
	U3CU3Ec__DisplayClass12_0__ctor_m2685C903220EF0EFCFABCCCCE85520064EEB9BCE,
	U3CU3Ec__DisplayClass12_0_U3CRegisterU3Eb__0_m1979ADC0BD33A692CDFDD59354B756C347611773,
	U3CU3Ec__DisplayClass13_0__ctor_m75A34D41161C0967E4A336B2713ACAE2BD5F5F46,
	U3CU3Ec__DisplayClass13_0_U3CUnregisterU3Eb__0_m8C6607EC9FE0F26B57047ED3642837003A532C79,
	U3CU3Ec__DisplayClass20_0__ctor_m3E72979DC019A7C47E2AB71E1F17B9056A7D068B,
	U3CU3Ec__DisplayClass20_0_U3CBlockUntilRecvMsgU3Eb__0_m89ABCD175D2DA1D535B2645459C38003ECBC896C,
	PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_mFA28BDF3B52AEF86161F33B52699253181800926,
	PlayerEditorConnectionEvents_AddAndCreate_mB5A51595E4A5DA3B9F353AC72F7B0484C675B7D3,
	PlayerEditorConnectionEvents_UnregisterManagedCallback_mFD3A444E636B079C03739DC96BAFFD5FD55C574A,
	PlayerEditorConnectionEvents__ctor_m9BE616B901BCACAABEC9063A838BB803AB7EC2A7,
	MessageEvent__ctor_m700B679037ED52893F092843EE603DBCD6EB8386,
	ConnectionChangeEvent__ctor_m3F04C39FD710BF0F25416A61F479CBA1B9021F18,
	MessageTypeSubscribers_get_MessageTypeId_mE7DD7E800436C92A325A1080AF60663AE1100D25,
	MessageTypeSubscribers_set_MessageTypeId_m294A7B621AAF1984D886D2569CF1206E4F469115,
	MessageTypeSubscribers__ctor_mD26A2485EA3ECACFA2CB35D08A48256CE9DFE825,
	U3CU3Ec__DisplayClass6_0__ctor_mC65CF56D3417BA36ED321886F1E7A1AF8D443966,
	U3CU3Ec__DisplayClass6_0_U3CInvokeMessageIdSubscribersU3Eb__0_m7208417727D24E769D2C1CF90D6E4CC1AE1F2556,
	U3CU3Ec__DisplayClass7_0__ctor_m59072FF40A09DA582550D3DED10DA2A93FBEAFEF,
	U3CU3Ec__DisplayClass7_0_U3CAddAndCreateU3Eb__0_m24A60437D2E527CE675AC4AFAC8152BCA69B033B,
	U3CU3Ec__DisplayClass8_0__ctor_mE25781AE393CFFE6170E4D655A751918973393AB,
	U3CU3Ec__DisplayClass8_0_U3CUnregisterManagedCallbackU3Eb__0_m11D9A0AFB947855B548E99416A058C4AAA2E2B26,
	DefaultValueAttribute__ctor_m2E914CFCFD82ACAB447480971570E5763C42DAAD,
	DefaultValueAttribute_get_Value_m1E1505D5F1838C28CA4C52DED4A20E81F81BFCC3,
	DefaultValueAttribute_Equals_mD9073A5C537D4DBDFBD5E3616BC5A05B5D0C51B2,
	DefaultValueAttribute_GetHashCode_m4782E2C5A005991FA7E705110A690DD5E0E52D50,
	ExcludeFromDocsAttribute__ctor_m01F11706D334D5D31B0C59630DB1674ECFBFAF04,
	GraphicsSettings_get_lightsUseLinearIntensity_mED8D75F87016FCF600955146863696AB214BA29A,
	GraphicsSettings_AllowEnlightenSupportForUpgradedProject_m00E568FAA4C9D08BEE0944042CF817BC242F2BEF,
	GraphicsSettings_get_renderPipelineAsset_mDDAD0B2B6E87A299B5F7B19F8BDE3C1EF6287B31,
	GraphicsSettings_get_INTERNAL_defaultRenderPipeline_m4150C45BC8C1D23C75F4EFA299345E894C7E5CB6,
	GraphicsSettings_get_defaultRenderPipeline_m2B485056B44981C55A5B6F0785445F9094078B18,
	OnDemandRendering_get_renderFrameInterval_m5A12FB459D93296FBACCD6FD59EA27CD092A0132,
	OnDemandRendering_GetRenderFrameInterval_m2A6D3ADB578BA19E153C6B049FC668975155EB27,
	OnDemandRendering__cctor_m00887F122A90C963B1B120117276E3BE3A0D258F,
	LODParameters_Equals_mA7C4CFD75190B341999074C56E1BAD9E5136CF61,
	LODParameters_Equals_m03754D13F85184E596AFBBCF6DA1EDB06CA58F6B,
	LODParameters_GetHashCode_m532D8960CCF3340F7CDF6B757D33BAE13B152716,
	NULL,
	RenderPipeline_InternalRender_m3601304F718BEEDCC63FAC61AF865392A1B97159,
	RenderPipeline_get_disposed_mA11FE959A1C7309A86F26893DA04D00DD5D61149,
	RenderPipeline_set_disposed_m6319C7F5991E861B961370FF374CF87E9F0DA691,
	RenderPipeline_Dispose_mFFDBE5963DA828BA99417035F8F6228535E0EAAB,
	RenderPipeline_Dispose_m256C636C8C66B12ED0E67F4C6F6470A79CBAA49B,
	RenderPipelineAsset_InternalCreatePipeline_m7FC3209A9640269E6E01FCBCC879E3FC36B23264,
	RenderPipelineAsset_get_renderingLayerMaskNames_mD9D46ECB8CB3AA207307DF715AB738EBE9774D4C,
	RenderPipelineAsset_get_defaultMaterial_mB049EB56C99330E8392028148336A9329A1F6BEF,
	RenderPipelineAsset_get_autodeskInteractiveShader_mACEA652B47468186F0B74AAE63A5625A70E500A4,
	RenderPipelineAsset_get_autodeskInteractiveTransparentShader_mBC027215A56E8FD6C3B3CC5008B69540BBEBD1FD,
	RenderPipelineAsset_get_autodeskInteractiveMaskedShader_m5342E858293BCAB426D1A6E84242CD7D26EE5BC7,
	RenderPipelineAsset_get_terrainDetailLitShader_mE16DBCD806DD77447A89F3C1109E35CD24FEA7CF,
	RenderPipelineAsset_get_terrainDetailGrassShader_mAF8F368B3B67F7E6C71A70A93696122E73ED6C47,
	RenderPipelineAsset_get_terrainDetailGrassBillboardShader_m2DAE45A6FF177CB2996EC95EDF1AC05F59A7470B,
	RenderPipelineAsset_get_defaultParticleMaterial_m131DA69D30E5A400B227B98B8877C94A118E9F49,
	RenderPipelineAsset_get_defaultLineMaterial_m9FF591F569273D367C4020C1AC3A30AA161CE6BC,
	RenderPipelineAsset_get_defaultTerrainMaterial_mA6D10A07A29A577D83AF711CDE2E70B28D51A91A,
	RenderPipelineAsset_get_defaultUIMaterial_m39BD27EC73AE39354158451661A0112FC4E4F7F6,
	RenderPipelineAsset_get_defaultUIOverdrawMaterial_m0FE02710211BE1CF77728A1086BDC5EDCE106D35,
	RenderPipelineAsset_get_defaultUIETC1SupportedMaterial_mB8E2388C7F45E6D1B4EC728EC2335B527A741470,
	RenderPipelineAsset_get_default2DMaterial_mF68C0199E16004C348D11AD0BA4C9482CCFC9762,
	RenderPipelineAsset_get_defaultShader_m8F4DD3FBD2EA1A55DE3EB45EF41C3FD502141A40,
	RenderPipelineAsset_get_defaultSpeedTree7Shader_mE81955ABC171A01E496BD2E6166CC0BCDCC12E39,
	RenderPipelineAsset_get_defaultSpeedTree8Shader_mF5025B6116D23E28B7A872C3694ADED33A6DE9B0,
	NULL,
	RenderPipelineAsset_OnValidate_m232856172D71149CC85645BE94902D01628A1A8E,
	RenderPipelineAsset_OnDisable_m6DB5D6EE17E7CAEC1254135C61F4EB3E249FDD52,
	RenderPipelineAsset__ctor_mCB26E546B9DC1F2518E0F0F4A7E6CFF519D852F2,
	RenderPipelineManager_get_currentPipeline_m07358604B9829E6C1EEDE94064729109D9259852,
	RenderPipelineManager_set_currentPipeline_mDCF377780787BD6CAB3BC9F4A04B01B478293B88,
	RenderPipelineManager_add_beginFrameRendering_mF9896552D7B492FBC22B0D1832C536C91E542B56,
	RenderPipelineManager_remove_beginFrameRendering_mC2D3765FA14C53A4C8DB538D0689C6F77EAE8501,
	RenderPipelineManager_CleanupRenderPipeline_m82AC5DAD81AE3B10147198B7C7CFAAD8AE528010,
	RenderPipelineManager_GetCameras_m53602210E7576F801085A741B2068BEA0995433F,
	RenderPipelineManager_DoRenderLoop_Internal_mF16D72874EE44C297C2D0623933207B448BFCD32,
	RenderPipelineManager_PrepareRenderPipeline_m4A4D5CD3B9803F6D92DBD7D245A3D925F9028CC7,
	RenderPipelineManager__cctor_m7B3FA781696A3B82639DA7705E02A4E0BD418421,
	ScriptableRenderContext_GetNumberOfCameras_Internal_m58DE22F11E08F3B9C7E4B1D9788711101EBD2395,
	ScriptableRenderContext_GetCamera_Internal_m1E56C9D9782F0E99A7ED3C64C15F3A06A8A0B917,
	ScriptableRenderContext__ctor_m554E9C4BB7F69601E65F71557914C6958D3181EE,
	ScriptableRenderContext_GetNumberOfCameras_mF14CD21DA4E8A43DCE5811735E48748313F71CBA,
	ScriptableRenderContext_GetCamera_mCC4F389E3EB259D9FF01E7F7801D39137BC0E41C,
	ScriptableRenderContext_Equals_m0612225D9DC8BE5574C67738B9B185A05B306803,
	ScriptableRenderContext_Equals_m239EBA23E760DDF7CC7112D3299D9B4CEA449683,
	ScriptableRenderContext_GetHashCode_m2A894A66C98DF28B51758A3579EE04B87D2AC6D6,
	ScriptableRenderContext_GetNumberOfCameras_Internal_Injected_m70F9C34BAA4E5548D1B5848EFFBA6ACDC1B9781B,
	ScriptableRenderContext_GetCamera_Internal_Injected_m43E535C1F7FC90BF7254A0D043E624D45B70E459,
	ShaderTagId_get_id_mB1AB67FC09D015D245DAB1F6C491F2D59558C5D5,
	ShaderTagId_set_id_m47A84B8D0203D1BAE247CF591D1E2B43A1FFEF1E,
	ShaderTagId_Equals_m7C6B4B00D502970BEDA5032CC9F791795B8C44EE,
	ShaderTagId_Equals_mC8A45F721611717B4CB12DBD73E23200C67AA7AF,
	ShaderTagId_GetHashCode_m51C1D9E4417AD43F0A5E9CCF7FF88C10BCDB2905,
	ShaderTagId__cctor_mC73D6F5DE8D099CE53149E383ADF6B64F3EA763D,
	SupportedRenderingFeatures_get_active_m2C2E65CE6B3B9197E71D6390B4CE3AF024EFC286,
	SupportedRenderingFeatures_set_active_m2E459FC4898691C6E729A5EC2D2B08A1733CDCC2,
	SupportedRenderingFeatures_get_defaultMixedLightingModes_m903DC0B0AB86F4C047E8930E0C59C4131DA600C9,
	SupportedRenderingFeatures_get_mixedLightingModes_m87742B8CBD950598883F391C22FF036BE774E2D9,
	SupportedRenderingFeatures_get_lightmapBakeTypes_m6B99531AE2EDEB49D54886E103AF12CFB1BC426A,
	SupportedRenderingFeatures_get_lightmapsModes_mAAAC00FB06849B233D053DB11B47E8DA8666583B,
	SupportedRenderingFeatures_get_enlighten_m493ED393C99DC9105CCC2D8D28D43D6AB0B96C78,
	SupportedRenderingFeatures_get_autoAmbientProbeBaking_m8F06C9C90FA3604D222D7CB0C6A5449B9A6D1E38,
	SupportedRenderingFeatures_get_autoDefaultReflectionProbeBaking_m03E0F42FE5C8DAF5D9A06CD74A2C61219F82E0CE,
	SupportedRenderingFeatures_FallbackMixedLightingModeByRef_m91AC959EB7DD16F061466CF2123820AFA257BD76,
	SupportedRenderingFeatures_IsMixedLightingModeSupported_m22C3916FDD1308FCDD201651709ED3861DBC09AB,
	SupportedRenderingFeatures_IsMixedLightingModeSupportedByRef_mE77416221A79F75F9EF7FD062AF9674264D1E069,
	SupportedRenderingFeatures_IsLightmapBakeTypeSupported_mD21AC518EFAA0892131E4ADE0EBA1DA980937A61,
	SupportedRenderingFeatures_IsLightmapBakeTypeSupportedByRef_mE33A63BE6E3D6D4DF4B5584E4F451713DC776DB8,
	SupportedRenderingFeatures_IsLightmapsModeSupportedByRef_mAFD9EC8C77CF9874911FFC0C084FC161C27E9A13,
	SupportedRenderingFeatures_IsLightmapperSupportedByRef_m96E352A96F40887994AE8BA5F07BBB87D8A3E557,
	SupportedRenderingFeatures_IsAutoAmbientProbeBakingSupported_m6F31B8E18EECDC6798BA87F75A72675EFC5C7304,
	SupportedRenderingFeatures_IsAutoDefaultReflectionProbeBakingSupported_mC6710197013A1E8ED2A89C78C980E056A242E5B6,
	SupportedRenderingFeatures_FallbackLightmapperByRef_m04F1D58EF96BB1E1E20C72A562E86F17EEF3B1B9,
	SupportedRenderingFeatures__ctor_mFA6FBB0F889B8A9ECD5B77C0CE2EEF685C0310F5,
	SupportedRenderingFeatures__cctor_m0AAC399710A8663753C069DDC8CAE61039630C40,
	BatchCullingContext__ctor_m5BB85EDAC0F7C5AFFABF7AD30F18AD272EA23E32,
	BatchRendererGroup_InvokeOnPerformCulling_mE7F3139F1032B8B6160F5601BB55410609D0EF6E,
	OnPerformCulling__ctor_mC901B41F5C6F0A0A144862B9014047CAE3702E2F,
	OnPerformCulling_Invoke_m91277025DBB74E928F6C98E7A6745B07F5B2FD59,
	OnPerformCulling_BeginInvoke_m8179E1CC4FD6B2858D0333578FCB43594551DAAD,
	OnPerformCulling_EndInvoke_m3EB7184DC9175B0F722CE41E1CE6507887CA55DB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Playable_get_Null_m1641F4B851ACAA6CBCC9BB400EC783EDEAF1A48D,
	Playable__ctor_m24C6ED455A921F585698BFFEC5CCED397205543E,
	Playable_GetHandle_m952F17BACFC90BEACD3CB9880E65E69B3271108A,
	Playable_Equals_m1EC7E8B579C862BA8C979BD616224EC1B3364DD1,
	Playable__cctor_m5655D443F6D04230DB5D37BF7D5EDCA71FD85A32,
	NULL,
	PlayableAsset_get_duration_m58C1A4AC3A8CF2783815016BE58378D6E17D22D2,
	PlayableAsset_get_outputs_mD839CEB7A22543AC17FAE1C3C4BCD9A7B8DA82B1,
	PlayableAsset_Internal_CreatePlayable_mB36624F1FD210AAD53A848BF8F26912D47DFC09C,
	PlayableAsset_Internal_GetPlayableAssetDuration_m099C6F58730A818ACA8C837D3DDBFC4ACA75693F,
	PlayableAsset__ctor_m669F459CFACFE65873346E428F206C457B488167,
	PlayableBehaviour__ctor_mE96A877D927BEEC8C9368A0673FEAD77A78C35EE,
	PlayableBehaviour_OnGraphStart_m41537F7ED140E16D8666B4959D99EF9BC16FF622,
	PlayableBehaviour_OnGraphStop_m5B1C17F7DA22FFBF8BABB18CBA090AB64FD51740,
	PlayableBehaviour_OnPlayableCreate_m963F9A0600B2208400FE3F90647FBACE0B5FE0DD,
	PlayableBehaviour_OnPlayableDestroy_mC1C991442A5940826928EA321FC5EFBE68482A57,
	PlayableBehaviour_OnBehaviourPlay_m4B44CD41A9CB3EA391BCB142FA3B9A80AFAFF820,
	PlayableBehaviour_OnBehaviourPause_mBC9C4536B30B413E248C96294F53CDABA2C1490C,
	PlayableBehaviour_PrepareFrame_m0FC4368B39C1DBC6586E417C8505D1A8C49235E2,
	PlayableBehaviour_ProcessFrame_m32F9B265BB54D1A3A290E2709FDD0B873360B25A,
	PlayableBehaviour_Clone_m2BB70A16D658FD18307D084EFFFE4A7B1BB234FD,
	PlayableBinding__cctor_mF1C450FA8C820DA444D8AB9235958EC750AE60C8,
	CreateOutputMethod__ctor_m252187F08E76732D791C8458AF5629F29BDDB8F2,
	CreateOutputMethod_Invoke_m56F81543465C6F9C65319BEEEFC5AEEF6A0D7764,
	CreateOutputMethod_BeginInvoke_m4FC768B14DF77F9DB8847F3FAF1CBFD11048030D,
	CreateOutputMethod_EndInvoke_m61A9C47D6ED76402024C0C7139C4A6F1D58D4C47,
	NULL,
	PlayableHandle_get_Null_m09DE585EF795EFA2811950173C80F4FA24CBAAD1,
	PlayableHandle_op_Equality_mBA774AE123AF794A1EB55148206CDD52DAFA42DF,
	PlayableHandle_Equals_m7D3DC594EE8EE029E514FF9505C5E7A820D2435E,
	PlayableHandle_Equals_mBCBD135BA1DBB6B5F8884A21EE55FDD5EADB555C,
	PlayableHandle_GetHashCode_mFEF967B1397A1DC2EE05FC8DBB9C5E13161E3C45,
	PlayableHandle_CompareVersion_m24BEA91E99FF5FD3472B1A71CB78296D99F808A9,
	PlayableHandle_IsValid_mDA0A998EA6E2442C5F3B6CDFAF07EBA9A6873059,
	PlayableHandle_GetPlayableType_m962BE384C093FF07EAF156DA373806C2D6EF1AD1,
	PlayableHandle__cctor_m6FA486FD9ECB91B10F04E59EFE993EC7663B6EA3,
	PlayableHandle_IsValid_Injected_mCCEEB80CD0855FD683299F6DBD4F9BA864C58C31,
	PlayableHandle_GetPlayableType_Injected_m65C3EB2DEC74C0A02707B6A61D25B3BFEC91DB66,
	PlayableOutput__ctor_m881EC9E4BAD358971373EE1D6BF9C37DDB7A4943,
	PlayableOutput_GetHandle_m079ADC0139E95AA914CD7502F27EC79BB1A876F3,
	PlayableOutput_Equals_m458689DD056559A7460CCE496BF6BEC45EB47C5B,
	PlayableOutput__cctor_m833F06DD46347C62096CEF4E22DBC3EB9ECDACD5,
	PlayableOutputHandle_get_Null_mA462EF24F3B0CDD5B3C3F0C57073D49CED316FA4,
	PlayableOutputHandle_GetHashCode_mC35D44FD77BCA850B945C047C6E2913436B1DF1C,
	PlayableOutputHandle_op_Equality_m9917DCF55902BB4984B830C4E3955F9C4E4CE0C0,
	PlayableOutputHandle_Equals_m42558FEC083CC424CB4BD715FC1A6561A2E47EB2,
	PlayableOutputHandle_Equals_m1FCA747BC3F69DC784912A932557EB3B24DC3F18,
	PlayableOutputHandle_CompareVersion_m4DD52E80EDD984F824FE235F35B849C5BA9B4964,
	PlayableOutputHandle__cctor_mD1C850FF555697A09A580322C66357B593C9294E,
	LinearColor_Convert_m34C66A797B11DE3EE19A9ED913B286C5C76F3B4E,
	LinearColor_Black_m0C3B7331D5DEBB72F85BAFC175BC785D964D30D8,
	LightDataGI_Init_m93EBCF45B2A5F8A0C9879FD2CF1B853514837F4B,
	LightDataGI_Init_m236C2DEE096CDCF4B4489B9A5630E231531DF022,
	LightDataGI_Init_mBCCAA12227CF3845C831463F7B8572F00CB17CF3,
	LightDataGI_Init_m24775B5AF758CAE25BA180BC17D9E032064B52B9,
	LightDataGI_Init_m00F56356E1220B292978ABE384B6500A1F1C9291,
	LightDataGI_InitNoBake_m660C58A4878A0DB9E842F642AA6D2800D30F0C48,
	LightmapperUtils_ExtractIndirect_m1BA4D17B92F68DE86B8696BCA09901CB9F70E352,
	LightmapperUtils_ExtractInnerCone_mAB6BC006F6841F7881DAC60077A61BA525E09C48,
	LightmapperUtils_Extract_mA213675C1DD2F46927D6B0D02619CD0F2247E98D,
	LightmapperUtils_Extract_mC23121E46C67B16DA74D2B74AA96BC9B734B4A86,
	LightmapperUtils_Extract_mC6A2DEE19D89B532926E1E1EF0F6815E069F27C2,
	LightmapperUtils_Extract_mFF46D8454835FF7AC4B35A807B1F29222425B594,
	LightmapperUtils_Extract_mF66211092ADB3241E3E1902CB84FF8E1A84A301F,
	Lightmapping_SetDelegate_mEA4A2549370F078869895AAC4E01EB916BB49A01,
	Lightmapping_GetDelegate_mCFE706531D3E5A96E71ED963AF8CB4B93A1C0AEE,
	Lightmapping_ResetDelegate_m25E3082200DFB9F101AEE07039FC4A33C4183738,
	Lightmapping_RequestLights_m572FA5D5ADA94FF109696431E2EAE13B6D5C9AB6,
	Lightmapping__cctor_m542D9D32613B348F62541FB1F0AF70EBF7EBB93A,
	RequestLightsDelegate__ctor_m32E80A59669219265627BAF616170C5BA130CA77,
	RequestLightsDelegate_Invoke_m2B97E4D1ED4DC45416B5EC472FC12B581373E403,
	RequestLightsDelegate_BeginInvoke_m842A0B872EE1BDC505161CC083CA189F222784A8,
	RequestLightsDelegate_EndInvoke_mB5DE6574659D68281BC3D8179211DA892A481333,
	U3CU3Ec__cctor_mB5E5B471CF113C81A1D2CF2C4245C2181590DB0C,
	U3CU3Ec__ctor_m9919FB3B009FEC46DBE2AAB63233F08B8B8D798D,
	U3CU3Ec_U3C_cctorU3Eb__7_0_m2D9D2C1DEA68EC9BC1F47CA41F462BB9EF72CF7B,
	CameraPlayable_GetHandle_mC710651CAEE2596697CFFC01014BEB041C67E2B4,
	CameraPlayable_Equals_mBA0325A3187672B8FDE237D2D5229474C19E3A52,
	MaterialEffectPlayable_GetHandle_mB0F6F324656489CE4DE4EFA8ACCE37458D292439,
	MaterialEffectPlayable_Equals_m9C2DB0EB37CFB9679961D667B61E2360384C71DA,
	TextureMixerPlayable_GetHandle_mBC5D38A23834675B7A8D5314DCF4655C83AE649C,
	TextureMixerPlayable_Equals_mEDE18FD43C9501F04871D2357DC66BABE43F397B,
	BuiltinRuntimeReflectionSystem_TickRealtimeProbes_m7100EF316D04C407A21C27A35752826E463603EB,
	BuiltinRuntimeReflectionSystem_Dispose_m60454D78E8492E739E2537F0B80FC76AC5DA1FCC,
	BuiltinRuntimeReflectionSystem_Dispose_m46A331A4A718F67B97CC07E98D419C0BBF38A8B0,
	BuiltinRuntimeReflectionSystem_BuiltinUpdate_mE62DD3456554487F133F6CCF3D07E0CE6B7A07AB,
	BuiltinRuntimeReflectionSystem_Internal_BuiltinRuntimeReflectionSystem_New_m1B6EE7816B71869B7F874488A51FF0093F1FF8CB,
	BuiltinRuntimeReflectionSystem__ctor_mB95177E1C8083A75B0980623778F2B6F3A80FE52,
	NULL,
	ScriptableRuntimeReflectionSystemSettings_set_Internal_ScriptableRuntimeReflectionSystemSettings_system_m9D4C5A04E52667F4A9C15144B854A9D84D089590,
	ScriptableRuntimeReflectionSystemSettings_get_Internal_ScriptableRuntimeReflectionSystemSettings_instance_m67BF9AE5DFB70144A8114705C51C96FFB497AA3E,
	ScriptableRuntimeReflectionSystemSettings_ScriptingDirtyReflectionSystemInstance_m02B2FCD9BBCFA4ECC1D91A0B5B8B83856AC55718,
	ScriptableRuntimeReflectionSystemSettings__cctor_m9CC42ECA95CACFFF874575B63D1FA461667D194C,
	ScriptableRuntimeReflectionSystemWrapper_get_implementation_mABDBD524BA9D869BCC02159B00C3B5F6DEE21895,
	ScriptableRuntimeReflectionSystemWrapper_set_implementation_m7866062401FA10180983AFE19BA672AE63CED29B,
	ScriptableRuntimeReflectionSystemWrapper_Internal_ScriptableRuntimeReflectionSystemWrapper_TickRealtimeProbes_m65564441C57A8D5D3BA116C171ECE95B91F734A2,
	ScriptableRuntimeReflectionSystemWrapper__ctor_mB936D4EA457BCCBEB0413F3F5B216164D88C4A3F,
	GraphicsFormatUtility_GetGraphicsFormat_mBA4E395B8A78B67B0969356DE19F6F1E73D284E0,
	GraphicsFormatUtility_GetGraphicsFormat_Native_TextureFormat_mAB77B8E00F9BE01455C0E011CF426FFBC53FA533,
	GraphicsFormatUtility_GetGraphicsFormat_mA94B78BFCA8AFEBB85150D361A9A20C83FC5277E,
	GraphicsFormatUtility_GetGraphicsFormat_Native_RenderTextureFormat_m36427FF5F0909DFE7AB6849EAC2D01A4E8D5ECF8,
	GraphicsFormatUtility_GetGraphicsFormat_m4527266E37A786CC45C6BDD520A793C3B5A3A09E,
	GraphicsFormatUtility_IsSRGBFormat_mBF49E7451A3960BD67B1F13745BCA3AC5C8AC66E,
	GraphicsFormatUtility_IsCompressedTextureFormat_m456D7B059F25F7378E05E3346CB1670517A46C71,
	GraphicsFormatUtility_IsCrunchFormat_m97E8A6551AAEE6B1E4E92F92167FC97CC7D73DB1,
	Assert_Fail_m9A3A2A08A2E1D18D0BB7D0C635055839EAA2EF02,
	NULL,
	NULL,
	Assert_AreEqual_mFD46F687B9319093BA13D285D19999C801DC0A60,
	Assert_AreEqual_mF4EDACE982A071478F520E76D1901B840411A91E,
	Assert__cctor_mB34E1F44EB37A40F434BDB787B5E55F2B4033AF5,
	AssertionException__ctor_mDB97D95CF48EE1F6C184D30F6C3E099E1C4DA7FD,
	AssertionException_get_Message_m857FDA8060518415B2FFFCCA4A6BEE7B9BF66ECE,
	AssertionMessageUtil_GetMessage_mA6DC7467BAF09543EA091FC63587C9011E1CA261,
	AssertionMessageUtil_GetMessage_m97506B9B19E9F75E8FE164BF64085466FC96EA18,
	AssertionMessageUtil_GetEqualityMessage_mDBD27DDE3A03418E4A2F8DB377AE866F656C812A,
};
extern void ProfilerMarker__ctor_mF9F9BDCB1E4618F9533D83D47EAD7325A32FDC2A_AdjustorThunk (void);
extern void Keyframe__ctor_m0EA9CF8E65F32EE7603302E2CC670C56DC177C13_AdjustorThunk (void);
extern void Keyframe_get_time_m5A49381A903E63DD63EF8A381BA26C1A2DEF935D_AdjustorThunk (void);
extern void Keyframe_set_time_mAD4CA2282CD1B7C1D330C3EE75F79AD636C7FC83_AdjustorThunk (void);
extern void Keyframe_get_value_m0DD3FAD00F43E7018FECBD011B04310E25C590FD_AdjustorThunk (void);
extern void Keyframe_set_value_m578277371F57893D5FF121710CF016A52302652E_AdjustorThunk (void);
extern void Keyframe_get_inTangent_mE19351A6720331D0D01C299ED6480780B20CB8E0_AdjustorThunk (void);
extern void Keyframe_set_inTangent_mAA19EC59A3F6D360310D2F6B765359A069F8058E_AdjustorThunk (void);
extern void Keyframe_get_outTangent_mF1B82FA2B7E060906B77CEAF229516D69789BEAE_AdjustorThunk (void);
extern void Keyframe_set_outTangent_mDEEA8549F31FA0AAC090B2D6D7E15C42D3CAE90B_AdjustorThunk (void);
extern void Keyframe_get_weightedMode_mFF562D4B06BCF6A65135C337D6F10BF1C4F3E14D_AdjustorThunk (void);
extern void Keyframe_set_weightedMode_m95923B248E6E9947136A115AF240F8C3A3CBC399_AdjustorThunk (void);
extern void Keyframe_get_tangentMode_mBF872460D0D000E593C06723BB8FCDDD48983D77_AdjustorThunk (void);
extern void Keyframe_set_tangentMode_mC8D2E68FDEF4D5C9083FAF35E6EACE989318A68C_AdjustorThunk (void);
extern void Keyframe_get_tangentModeInternal_mCA542C1309EAB57FC634AFB50D31721FF82FB631_AdjustorThunk (void);
extern void Keyframe_set_tangentModeInternal_mFF714C3EB51D2A08F6A375728C0280D124749AC2_AdjustorThunk (void);
extern void CachedAssetBundle__ctor_mBB07A43F19F5095BC21A44543684F6B4269CE315_AdjustorThunk (void);
extern void CachedAssetBundle_get_name_m31556A8588D4BE9261B0AB02486118A30D3ED971_AdjustorThunk (void);
extern void CachedAssetBundle_get_hash_mC7D2AD78B64D7CDEF0604C934BF97D6C388A3EC1_AdjustorThunk (void);
extern void Cache_get_handle_m2A81C085DE744E211DCC3F5BD608043EEB93075D_AdjustorThunk (void);
extern void Cache_GetHashCode_m3C305D87FF4578864D1844170133E2758986B244_AdjustorThunk (void);
extern void Cache_Equals_m4E85CF7FABB9052E9673879DF8FB341453D76F06_AdjustorThunk (void);
extern void Cache_Equals_m49F0BDF7BE07D3FA50A70ED8FE85DBFF178C11C5_AdjustorThunk (void);
extern void Cache_get_valid_m01BE7338A2AB06309EC3808BD361F0A355E0B29F_AdjustorThunk (void);
extern void Cache_get_path_mC3C0F1863B3EE5BE472D43BC25797E1EC3278A6B_AdjustorThunk (void);
extern void Cache_set_maximumAvailableStorageSpace_m2F9DA827094B24940EE439739EC7B78EC5237559_AdjustorThunk (void);
extern void Cache_set_expirationDelay_m2D2ABE9B1D1503A16A94A423852E7D037C7F213A_AdjustorThunk (void);
extern void Bounds__ctor_m294E77A20EC1A3E96985FE1A925CB271D1B5266D_AdjustorThunk (void);
extern void Bounds_GetHashCode_m9F5F751E9D52F99FCC3DC07407410078451F06AC_AdjustorThunk (void);
extern void Bounds_Equals_m1ECFAEFE19BAFB61FFECA5C0B8AE068483A39C61_AdjustorThunk (void);
extern void Bounds_Equals_mC2E2B04AB16455E2C17CD0B3C1497835DEA39859_AdjustorThunk (void);
extern void Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B_AdjustorThunk (void);
extern void Bounds_set_center_mAD29DD80FD631F83AF4E7558BB27A0398E8FD841_AdjustorThunk (void);
extern void Bounds_get_size_m0739F2686AE2D3416A33AEF892653091347FD4A6_AdjustorThunk (void);
extern void Bounds_set_size_m70855AC67A54062D676174B416FB06019226B39A_AdjustorThunk (void);
extern void Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9_AdjustorThunk (void);
extern void Bounds_set_extents_mC83719146B06D0575A160CDDE9997202A1192B35_AdjustorThunk (void);
extern void Bounds_get_min_m2D48F74D29BF904D1AF19C562932E34ACAE2467C_AdjustorThunk (void);
extern void Bounds_get_max_mC3BE43C2A865BAC138D117684BC01E289892549B_AdjustorThunk (void);
extern void Bounds_SetMinMax_m04969DE5CBC7F9843C12926ADD5F591159C86CA6_AdjustorThunk (void);
extern void Bounds_Encapsulate_mD1F1DAC416D7147E07BF54D87CA7FF84C1088D8D_AdjustorThunk (void);
extern void Bounds_ToString_m4637EA7C58B9C75651A040182471E9BAB9295666_AdjustorThunk (void);
extern void Plane_get_normal_m203D43F51C449990214D04F332E8261295162E84_AdjustorThunk (void);
extern void Plane_get_distance_m5358B80C35E1E295C0133E7DC6449BB09C456DEE_AdjustorThunk (void);
extern void Plane__ctor_m6535EAD5E675627C2533962F1F7890CBFA2BA44A_AdjustorThunk (void);
extern void Plane_Raycast_m04E61D7C78A5DA70F4F73F9805ABB54177B799A9_AdjustorThunk (void);
extern void Plane_ToString_mF92ABB5136759C7DFBC26FD3957532B3C26F2099_AdjustorThunk (void);
extern void Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B_AdjustorThunk (void);
extern void Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187_AdjustorThunk (void);
extern void Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E_AdjustorThunk (void);
extern void Ray_GetPoint_mE8830D3BA68A184AD70514428B75F5664105ED08_AdjustorThunk (void);
extern void Ray_ToString_m73B5291E29C9C691773B44590C467A0D4FBE0EC1_AdjustorThunk (void);
extern void Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280_AdjustorThunk (void);
extern void Rect__ctor_m027E778E437FED8E6DBCE0C01C854ADF54986ECE_AdjustorThunk (void);
extern void Rect_get_x_mC51A461F546D14832EB96B11A7198DADDE2597B7_AdjustorThunk (void);
extern void Rect_set_x_m49EFE25263C03A48D52499C3E9C097298E0EA3A6_AdjustorThunk (void);
extern void Rect_get_y_m53E3E4F62D9840FBEA751A66293038F1F5D1D45C_AdjustorThunk (void);
extern void Rect_set_y_mCFDB9BD77334EF9CD896F64BE63C755777D7CCD5_AdjustorThunk (void);
extern void Rect_get_position_m54A2ACD2F97988561D6C83FCEF7D082BC5226D4C_AdjustorThunk (void);
extern void Rect_set_position_mD92DFF591D9C96CDD6AF22EA2052BB3D468D68ED_AdjustorThunk (void);
extern void Rect_get_center_mA6E659EAAACC32132022AB199793BF641B3068CB_AdjustorThunk (void);
extern void Rect_set_center_m6F280BE23E6FAA6214DDA6FB6B7697DB906D879E_AdjustorThunk (void);
extern void Rect_get_min_m17345668569CF57C5F1D2B2DADD05DD4220A5950_AdjustorThunk (void);
extern void Rect_set_min_m3A3DE206469065D65650DCF4D23F51C25B6C08A7_AdjustorThunk (void);
extern void Rect_get_max_m3BFB033D741F205FB04EF163A9D5785E7E020756_AdjustorThunk (void);
extern void Rect_set_max_mCC50E64F2DE589A3B7D1BFED72B49AC19D49FAEB_AdjustorThunk (void);
extern void Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484_AdjustorThunk (void);
extern void Rect_set_width_mC81EF602AC91E0C615C12FCE060254A461A152B8_AdjustorThunk (void);
extern void Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5_AdjustorThunk (void);
extern void Rect_set_height_mF4CB5A97D4706696F1C9EA31A5D8C466E48050D6_AdjustorThunk (void);
extern void Rect_get_size_m731642B8F03F6CE372A2C9E2E4A925450630606C_AdjustorThunk (void);
extern void Rect_set_size_m4618056983660063A74F40CCFF9A683933CB4C93_AdjustorThunk (void);
extern void Rect_get_xMin_mFDFA74F66595FD2B8CE360183D1A92B575F0A76E_AdjustorThunk (void);
extern void Rect_set_xMin_mD8F9BF59F4F33F9C3AB2FEFF32D8C16756B51E34_AdjustorThunk (void);
extern void Rect_get_yMin_m31EDC3262BE39D2F6464B15397F882237E6158C3_AdjustorThunk (void);
extern void Rect_set_yMin_m58C137C81F3D098CF81498964E1B5987882883A7_AdjustorThunk (void);
extern void Rect_get_xMax_mA16D7C3C2F30F8608719073ED79028C11CE90983_AdjustorThunk (void);
extern void Rect_set_xMax_m1775041FCD5CA22C77D75CC780D158CD2B31CEAF_AdjustorThunk (void);
extern void Rect_get_yMax_m8AA5E92C322AF3FF571330F00579DA864F33341B_AdjustorThunk (void);
extern void Rect_set_yMax_m4F1C5632CD4836853A22E979C810C279FBB20B95_AdjustorThunk (void);
extern void Rect_Contains_mAD3D41C88795960F177088F847509C9DDA23B682_AdjustorThunk (void);
extern void Rect_Contains_m5072228CE6251E7C754F227BA330F9ADA95C1495_AdjustorThunk (void);
extern void Rect_Overlaps_m2FE484659899E54C13772AB7D9E202239A637559_AdjustorThunk (void);
extern void Rect_Overlaps_m4FFECCEAB3FBF23ED5A51B2E26220F035B942B4B_AdjustorThunk (void);
extern void Rect_GetHashCode_mA23F5D7C299F7E05A0390DF2FA663F5A003799C6_AdjustorThunk (void);
extern void Rect_Equals_m76E3B7E2E5CC43299C4BF4CB2EA9EF6E989E23E3_AdjustorThunk (void);
extern void Rect_Equals_mC8430F80283016D0783FB6C4E7461BEED4B55C82_AdjustorThunk (void);
extern void Rect_ToString_m045E7857658F27052323E301FBA3867AD13A6FE5_AdjustorThunk (void);
extern void Resolution_ToString_m42289CE0FC4ED41A9DC62B398F46F7954BC52F04_AdjustorThunk (void);
extern void RenderTextureDescriptor_get_width_m225FBFD7C33BD02D6879A93F1D57997BC251F3F5_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_width_m48ADD4AB04E8DBEEC5CA8CC96F86D2674F4FE55F_AdjustorThunk (void);
extern void RenderTextureDescriptor_get_height_m947A620B3D28090A57A5DC0D6A126CBBF818B97F_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_height_mD19D74EC9679250F63489CF1950351EFA83A1A45_AdjustorThunk (void);
extern void RenderTextureDescriptor_get_msaaSamples_mEBE0D743E17068D1898DAE2D281C913E39A33616_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_msaaSamples_m5856FC43DAD667D4462B6BCA938B70E42068D24C_AdjustorThunk (void);
extern void RenderTextureDescriptor_get_volumeDepth_mBC82F4621E4158E3D2F2457487D1C220AA782AC6_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_volumeDepth_mDC402E6967166BE8CADDA690B32136A1E0AB8583_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_mipCount_m98C1CE257A8152D8B23EC27D68D0C4C35683DEE5_AdjustorThunk (void);
extern void RenderTextureDescriptor_get_graphicsFormat_mD2DD8AC2E1324779F8D497697E725FB93341A14D_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_graphicsFormat_mF24C183BA9C9C42923CEC3ED353661D54AA741CA_AdjustorThunk (void);
extern void RenderTextureDescriptor_get_depthBufferBits_m51E82C47A0CA0BD8B20F90D43169C956C4F24996_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_depthBufferBits_mED58A8D9643740713597B0244BF76D0395C1C479_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_dimension_mBC3C8793345AC5E627BDD932B46A5F93568ACCE5_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_shadowSamplingMode_m0AC43F8A8BE0755567EED4DDFADBE207739E1ECF_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_vrUsage_mC591604135749B0BD156865141E114F51812E502_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_memoryless_m1FB3F7E8B482C71BDB549AD71D762BCF337D8185_AdjustorThunk (void);
extern void RenderTextureDescriptor__ctor_m227EBED323830B1059806874C69E1426DDC16B85_AdjustorThunk (void);
extern void RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m387E338D17ED601B0587EF4E5CEB10DDFC42CC05_AdjustorThunk (void);
extern void Hash128_get_isValid_m45005B82BA8416D4ACA8B20DF9DA5B8513C602B3_AdjustorThunk (void);
extern void Hash128_CompareTo_m0BC4F8F4228CF2B48C615E39CD3CE260386CC5BC_AdjustorThunk (void);
extern void Hash128_ToString_mD81890597BCFD67BE47646DA5366347C545EB5E8_AdjustorThunk (void);
extern void Hash128_Equals_m4701FDD64B5C5F81B1E494D5586C5CB4519BC007_AdjustorThunk (void);
extern void Hash128_Equals_m85BCDD87EB2A629FB8BB72FD63244787BC47E52D_AdjustorThunk (void);
extern void Hash128_GetHashCode_m65DA1711C64E83AB514A3D498C568CB10EAA0D69_AdjustorThunk (void);
extern void Hash128_CompareTo_m3595697B0FC7ACAED77C03FEC7FF80A073A4F6AE_AdjustorThunk (void);
extern void Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C_AdjustorThunk (void);
extern void Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369_AdjustorThunk (void);
extern void Color_ToString_m17A27E0CFB20D9946D130DAEDB5BDCACA5A4473F_AdjustorThunk (void);
extern void Color_GetHashCode_m88317C719D2DAA18E293B3F5CD17B9FB80E26CF1_AdjustorThunk (void);
extern void Color_Equals_m63ECBA87A0F27CD7D09EEA36BCB697652E076F4E_AdjustorThunk (void);
extern void Color_Equals_mA81EEDDC4250DE67C2F43BC88A102EA32A138052_AdjustorThunk (void);
extern void Color_RGBMultiplied_m41914B23903491843FAA3B0C02027EF8B70F34CF_AdjustorThunk (void);
extern void Color_get_linear_mB10CD29D56ADE2C811AD74A605BA11F6656E9D1A_AdjustorThunk (void);
extern void Color_get_maxColorComponent_mBA8595CB2790747F42145CB696C10E64C9BBD76D_AdjustorThunk (void);
extern void Color32__ctor_m1AEF46FBBBE4B522E6984D081A3D158198E10AA2_AdjustorThunk (void);
extern void Color32_ToString_m217F2AD5C02E630E37BE5CFB0933630F6D0C3555_AdjustorThunk (void);
extern void Matrix4x4_GetLossyScale_m0FB088456F10D189CDBAD737670B053EE644BB5C_AdjustorThunk (void);
extern void Matrix4x4_get_lossyScale_m6E184C6942DA569CE4427C967501B3B4029A655E_AdjustorThunk (void);
extern void Matrix4x4__ctor_mC7C5A4F0791B2A3ADAFE1E6C491B7705B6492B12_AdjustorThunk (void);
extern void Matrix4x4_GetHashCode_m6627C82FBE2092AE4711ABA909D0E2C3C182028F_AdjustorThunk (void);
extern void Matrix4x4_Equals_m7FB9C1A249956C6CDE761838B92097C525596D31_AdjustorThunk (void);
extern void Matrix4x4_Equals_mF8358F488D95A9C2E5A9F69F31EC7EA0F4640E51_AdjustorThunk (void);
extern void Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E_AdjustorThunk (void);
extern void Matrix4x4_MultiplyPoint_mD5D082585C5B3564A5EFC90A3C5CAFFE47E45B65_AdjustorThunk (void);
extern void Matrix4x4_MultiplyPoint3x4_m7C872FDCC9E3378E00A40977F641A45A24994E9A_AdjustorThunk (void);
extern void Matrix4x4_ToString_m7E29D2447E2FC1EAB3D8565B7DCAFB9037C69E1D_AdjustorThunk (void);
extern void Vector3_get_Item_mC3B9D35C070A91D7CA5C5B47280BD0EA3E148AC6_AdjustorThunk (void);
extern void Vector3_set_Item_m89FF112CEC0D9ED43F1C4FE01522C75394B30AE6_AdjustorThunk (void);
extern void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1_AdjustorThunk (void);
extern void Vector3__ctor_m6AD8F21FFCC7723C6F507CCF2E4E2EFFC4871584_AdjustorThunk (void);
extern void Vector3_Scale_mD40CDE2B62BCBABD49426FAE104814F29CF2AA0B_AdjustorThunk (void);
extern void Vector3_GetHashCode_m6C42B4F413A489535D180E8A99BE0298AD078B0B_AdjustorThunk (void);
extern void Vector3_Equals_m1F74B1FB7EE51589FFFA61D894F616B8F258C056_AdjustorThunk (void);
extern void Vector3_Equals_m6B991540378DB8541CEB9472F7ED2BF5FF72B5DB_AdjustorThunk (void);
extern void Vector3_Normalize_m174460238EC6322B9095A378AA8624B1DD9000F3_AdjustorThunk (void);
extern void Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B_AdjustorThunk (void);
extern void Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274_AdjustorThunk (void);
extern void Vector3_get_sqrMagnitude_m1C6E190B4A933A183B308736DEC0DD64B0588968_AdjustorThunk (void);
extern void Vector3_ToString_m2682D27AB50CD1CE4677C38D0720A302D582348D_AdjustorThunk (void);
extern void Quaternion__ctor_m7502F0C38E04C6DE24C965D1CAF278DDD02B9D61_AdjustorThunk (void);
extern void Quaternion_SetLookRotation_mDB3D5A8083E5AB5881FA9CC1EACFC196F61B8204_AdjustorThunk (void);
extern void Quaternion_get_eulerAngles_mF8ABA8EB77CD682017E92F0F457374E54BC943F9_AdjustorThunk (void);
extern void Quaternion_GetHashCode_m43BDCF3A72E31FA4063C1DEB770890FF47033458_AdjustorThunk (void);
extern void Quaternion_Equals_m099618C36B86DC63B2E7C89673C8566B18E5996E_AdjustorThunk (void);
extern void Quaternion_Equals_m0A269A9B77E915469801463C8BBEF7A06EF94A09_AdjustorThunk (void);
extern void Quaternion_ToString_m38DF4A1C05A91331D0A208F45CE74AF005AB463D_AdjustorThunk (void);
extern void Vector2_get_Item_m67344A67120E48C32D9419E24BA7AED29F063379_AdjustorThunk (void);
extern void Vector2_set_Item_m2335DC41E2BB7E64C21CDF0EEDE64FFB56E7ABD1_AdjustorThunk (void);
extern void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0_AdjustorThunk (void);
extern void Vector2_Normalize_m99A2CC6E4CB65C1B9231F898D5B7A12B6D72E722_AdjustorThunk (void);
extern void Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B_AdjustorThunk (void);
extern void Vector2_ToString_m83C7C331834382748956B053E252AE3BD21807C4_AdjustorThunk (void);
extern void Vector2_GetHashCode_m028AB6B14EBC6D668CFA45BF6EDEF17E2C44EA54_AdjustorThunk (void);
extern void Vector2_Equals_m4A2A75BC3D09933321220BCEF21219B38AF643AE_AdjustorThunk (void);
extern void Vector2_Equals_mD6BF1A738E3CAF57BB46E604B030C072728F4EEB_AdjustorThunk (void);
extern void Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF_AdjustorThunk (void);
extern void Vector2_get_sqrMagnitude_mAEE10A8ECE7D5754E10727BA8C9068A759AD7002_AdjustorThunk (void);
extern void Vector2Int_get_x_m300C7C05CD66D24EE62D91CDC0C557A6C0ABF25E_AdjustorThunk (void);
extern void Vector2Int_set_x_mC6F9F1E43668A7F8E8C44CEFB427BA97389F1420_AdjustorThunk (void);
extern void Vector2Int_get_y_m0D6E131AB5FA4AD532DEC377F981AADA189E680B_AdjustorThunk (void);
extern void Vector2Int_set_y_mF79CD7FACF0A9A1CC12678E2E24BD43C5E836D88_AdjustorThunk (void);
extern void Vector2Int__ctor_m501C34762BA7ECDDCFC25C19A4B9C93BC15004E1_AdjustorThunk (void);
extern void Vector2Int_Equals_mF7D7EFBC0286224832BA76701801C28530D40479_AdjustorThunk (void);
extern void Vector2Int_Equals_m65420C995F326F5C340E4825EA5E16BDE68F5A9C_AdjustorThunk (void);
extern void Vector2Int_GetHashCode_m73E874F4E94DF3D2603035E2E892873B139A7A9E_AdjustorThunk (void);
extern void Vector2Int_ToString_mB0D67C1885311767BA89D4C4A6E3A5A515194BF3_AdjustorThunk (void);
extern void Vector3Int_get_x_m23CB00F1579FD4CE86291940E2E75FB13405D53A_AdjustorThunk (void);
extern void Vector3Int_set_x_mED89A481E7FF21D0BF9F61511D5442E2CE9BEB75_AdjustorThunk (void);
extern void Vector3Int_get_y_m1C2F0AB641A167DF22F9C3C57092EC05AEF8CA26_AdjustorThunk (void);
extern void Vector3Int_set_y_m3E8A59AC8851E9AEC6B65AA167047C1F7C497B84_AdjustorThunk (void);
extern void Vector3Int_get_z_m9A88DC2346FD1838EC611CC8AB2FC29951E94183_AdjustorThunk (void);
extern void Vector3Int_set_z_m7BFC415EBBF5D31ACF664EEC37EE801C22B0E8D5_AdjustorThunk (void);
extern void Vector3Int__ctor_m171D642C38B163B353DAE9CCE90ACFE0894C1156_AdjustorThunk (void);
extern void Vector3Int_Equals_m704D204F83B9C64C7AF06152F98B542C5C400DC7_AdjustorThunk (void);
extern void Vector3Int_Equals_m9F98F28666ADF5AD0575C4CABAF6881F1317D4C1_AdjustorThunk (void);
extern void Vector3Int_GetHashCode_m6CDE2FEC995180949111253817BD0E4ECE7EAE3D_AdjustorThunk (void);
extern void Vector3Int_ToString_m08AB1BE6A674B2669839B1C44ACCF6D85EBCFB91_AdjustorThunk (void);
extern void Vector4_get_Item_m39878FDA732B20347BB37CD1485560E9267EDC98_AdjustorThunk (void);
extern void Vector4_set_Item_m56FB3A149299FEF1C0CF638CFAF71C7F0685EE45_AdjustorThunk (void);
extern void Vector4__ctor_m545458525879607A5392A10B175D0C19B2BC715D_AdjustorThunk (void);
extern void Vector4_GetHashCode_m7329FEA2E90CDBDBF4F09F51D92C87E08F5DC92E_AdjustorThunk (void);
extern void Vector4_Equals_m552ECA9ECD220D6526D8ECC9902016B6FC6D49B5_AdjustorThunk (void);
extern void Vector4_Equals_mB9894C2D4EE56C6E8FDF6CC40DCE0CE16BA4F7BF_AdjustorThunk (void);
extern void Vector4_get_magnitude_mE33CE76438DDE4DDBBAD94178B07D9364674D356_AdjustorThunk (void);
extern void Vector4_get_sqrMagnitude_m6B2707CBD31D237605D066A5925E6419D28B5397_AdjustorThunk (void);
extern void Vector4_ToString_m769402E3F7CBD6C92464D916527CC87BBBA53EF9_AdjustorThunk (void);
extern void LayerMask_get_value_m682288E860BBE36F5668DCDBC59245DE6319E537_AdjustorThunk (void);
extern void LayerMask_set_value_m56653944C7760E9D456E6B9E68F85CAA5B24A042_AdjustorThunk (void);
extern void RangeInt_get_end_m7A5182161CC5454E1C200E0173668572BA7FAAFD_AdjustorThunk (void);
extern void RangeInt__ctor_mACFE54DF73DE3F62053F851423525DB5AC1B100E_AdjustorThunk (void);
extern void WorkRequest__ctor_mE19AE1779B544378C8CB488F1576BDE618548599_AdjustorThunk (void);
extern void WorkRequest_Invoke_m67D71A48794EEBB6B9793E6F1E015DE90C03C1ED_AdjustorThunk (void);
extern void DrivenRectTransformTracker_Add_m51059F302FBD574E93820E8116283D1608D1AB5A_AdjustorThunk (void);
extern void DrivenRectTransformTracker_Clear_m328659F339A4FB519C9A208A685DDED106B6FC89_AdjustorThunk (void);
extern void DebugScreenCapture_set_rawImageDataReference_mE09725CEBC8D7A846033531E33C8E9B8DA60DFDE_AdjustorThunk (void);
extern void DebugScreenCapture_set_imageFormat_m779E16070B55AACD76279BFDDAE5CC470667D3FB_AdjustorThunk (void);
extern void DebugScreenCapture_set_width_mD8B3C521091CD0049D323410FFD201D47B629127_AdjustorThunk (void);
extern void DebugScreenCapture_set_height_mD7E3596B40E77F906A3E9517BC6CE795C0609442_AdjustorThunk (void);
extern void MovedFromAttributeData_Set_m244D6454BEA753FA4C7C3F2393A5DADCB3166B1C_AdjustorThunk (void);
extern void Scene_get_handle_mFAB5C41D41B90B9CEBB3918A6F3638BD41E980C9_AdjustorThunk (void);
extern void Scene_IsValid_mDA115AE2634680CAE92D2F844347AB4CD2185437_AdjustorThunk (void);
extern void Scene_get_path_m6D313F8791A3376EBE73AA6F62C02731F7F48A86_AdjustorThunk (void);
extern void Scene_get_name_m0E63ED0F050FCC35A4216220C584BE3D3F77B0E1_AdjustorThunk (void);
extern void Scene_get_isLoaded_m6A733EA533B0DC0041A9579A95530B41F869435C_AdjustorThunk (void);
extern void Scene_get_buildIndex_m764659943B7357F5D6C9165F68EDCFBBDDD6C3C2_AdjustorThunk (void);
extern void Scene_GetHashCode_m65BBB604A5496CF1F2C129860F45D0E437499E34_AdjustorThunk (void);
extern void Scene_Equals_mD5738AF0B92757DED12A90F136716A5E2DDE3F54_AdjustorThunk (void);
extern void LoadSceneParameters_set_loadSceneMode_mD56FFF214E3909A68BD9C485BBB4D99ADC3A5624_AdjustorThunk (void);
extern void LoadSceneParameters__ctor_m2E00BEC64DCC48351831B7BE9088D9B6AF62102A_AdjustorThunk (void);
extern void LODParameters_Equals_mA7C4CFD75190B341999074C56E1BAD9E5136CF61_AdjustorThunk (void);
extern void LODParameters_Equals_m03754D13F85184E596AFBBCF6DA1EDB06CA58F6B_AdjustorThunk (void);
extern void LODParameters_GetHashCode_m532D8960CCF3340F7CDF6B757D33BAE13B152716_AdjustorThunk (void);
extern void ScriptableRenderContext_GetNumberOfCameras_Internal_m58DE22F11E08F3B9C7E4B1D9788711101EBD2395_AdjustorThunk (void);
extern void ScriptableRenderContext_GetCamera_Internal_m1E56C9D9782F0E99A7ED3C64C15F3A06A8A0B917_AdjustorThunk (void);
extern void ScriptableRenderContext__ctor_m554E9C4BB7F69601E65F71557914C6958D3181EE_AdjustorThunk (void);
extern void ScriptableRenderContext_GetNumberOfCameras_mF14CD21DA4E8A43DCE5811735E48748313F71CBA_AdjustorThunk (void);
extern void ScriptableRenderContext_GetCamera_mCC4F389E3EB259D9FF01E7F7801D39137BC0E41C_AdjustorThunk (void);
extern void ScriptableRenderContext_Equals_m0612225D9DC8BE5574C67738B9B185A05B306803_AdjustorThunk (void);
extern void ScriptableRenderContext_Equals_m239EBA23E760DDF7CC7112D3299D9B4CEA449683_AdjustorThunk (void);
extern void ScriptableRenderContext_GetHashCode_m2A894A66C98DF28B51758A3579EE04B87D2AC6D6_AdjustorThunk (void);
extern void ShaderTagId_get_id_mB1AB67FC09D015D245DAB1F6C491F2D59558C5D5_AdjustorThunk (void);
extern void ShaderTagId_set_id_m47A84B8D0203D1BAE247CF591D1E2B43A1FFEF1E_AdjustorThunk (void);
extern void ShaderTagId_Equals_m7C6B4B00D502970BEDA5032CC9F791795B8C44EE_AdjustorThunk (void);
extern void ShaderTagId_Equals_mC8A45F721611717B4CB12DBD73E23200C67AA7AF_AdjustorThunk (void);
extern void ShaderTagId_GetHashCode_m51C1D9E4417AD43F0A5E9CCF7FF88C10BCDB2905_AdjustorThunk (void);
extern void BatchCullingContext__ctor_m5BB85EDAC0F7C5AFFABF7AD30F18AD272EA23E32_AdjustorThunk (void);
extern void Playable__ctor_m24C6ED455A921F585698BFFEC5CCED397205543E_AdjustorThunk (void);
extern void Playable_GetHandle_m952F17BACFC90BEACD3CB9880E65E69B3271108A_AdjustorThunk (void);
extern void Playable_Equals_m1EC7E8B579C862BA8C979BD616224EC1B3364DD1_AdjustorThunk (void);
extern void PlayableHandle_Equals_m7D3DC594EE8EE029E514FF9505C5E7A820D2435E_AdjustorThunk (void);
extern void PlayableHandle_Equals_mBCBD135BA1DBB6B5F8884A21EE55FDD5EADB555C_AdjustorThunk (void);
extern void PlayableHandle_GetHashCode_mFEF967B1397A1DC2EE05FC8DBB9C5E13161E3C45_AdjustorThunk (void);
extern void PlayableHandle_IsValid_mDA0A998EA6E2442C5F3B6CDFAF07EBA9A6873059_AdjustorThunk (void);
extern void PlayableHandle_GetPlayableType_m962BE384C093FF07EAF156DA373806C2D6EF1AD1_AdjustorThunk (void);
extern void PlayableOutput__ctor_m881EC9E4BAD358971373EE1D6BF9C37DDB7A4943_AdjustorThunk (void);
extern void PlayableOutput_GetHandle_m079ADC0139E95AA914CD7502F27EC79BB1A876F3_AdjustorThunk (void);
extern void PlayableOutput_Equals_m458689DD056559A7460CCE496BF6BEC45EB47C5B_AdjustorThunk (void);
extern void PlayableOutputHandle_GetHashCode_mC35D44FD77BCA850B945C047C6E2913436B1DF1C_AdjustorThunk (void);
extern void PlayableOutputHandle_Equals_m42558FEC083CC424CB4BD715FC1A6561A2E47EB2_AdjustorThunk (void);
extern void PlayableOutputHandle_Equals_m1FCA747BC3F69DC784912A932557EB3B24DC3F18_AdjustorThunk (void);
extern void LightDataGI_Init_m93EBCF45B2A5F8A0C9879FD2CF1B853514837F4B_AdjustorThunk (void);
extern void LightDataGI_Init_m236C2DEE096CDCF4B4489B9A5630E231531DF022_AdjustorThunk (void);
extern void LightDataGI_Init_mBCCAA12227CF3845C831463F7B8572F00CB17CF3_AdjustorThunk (void);
extern void LightDataGI_Init_m24775B5AF758CAE25BA180BC17D9E032064B52B9_AdjustorThunk (void);
extern void LightDataGI_Init_m00F56356E1220B292978ABE384B6500A1F1C9291_AdjustorThunk (void);
extern void LightDataGI_InitNoBake_m660C58A4878A0DB9E842F642AA6D2800D30F0C48_AdjustorThunk (void);
extern void CameraPlayable_GetHandle_mC710651CAEE2596697CFFC01014BEB041C67E2B4_AdjustorThunk (void);
extern void CameraPlayable_Equals_mBA0325A3187672B8FDE237D2D5229474C19E3A52_AdjustorThunk (void);
extern void MaterialEffectPlayable_GetHandle_mB0F6F324656489CE4DE4EFA8ACCE37458D292439_AdjustorThunk (void);
extern void MaterialEffectPlayable_Equals_m9C2DB0EB37CFB9679961D667B61E2360384C71DA_AdjustorThunk (void);
extern void TextureMixerPlayable_GetHandle_mBC5D38A23834675B7A8D5314DCF4655C83AE649C_AdjustorThunk (void);
extern void TextureMixerPlayable_Equals_mEDE18FD43C9501F04871D2357DC66BABE43F397B_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[262] = 
{
	{ 0x06000006, ProfilerMarker__ctor_mF9F9BDCB1E4618F9533D83D47EAD7325A32FDC2A_AdjustorThunk },
	{ 0x0600002B, Keyframe__ctor_m0EA9CF8E65F32EE7603302E2CC670C56DC177C13_AdjustorThunk },
	{ 0x0600002C, Keyframe_get_time_m5A49381A903E63DD63EF8A381BA26C1A2DEF935D_AdjustorThunk },
	{ 0x0600002D, Keyframe_set_time_mAD4CA2282CD1B7C1D330C3EE75F79AD636C7FC83_AdjustorThunk },
	{ 0x0600002E, Keyframe_get_value_m0DD3FAD00F43E7018FECBD011B04310E25C590FD_AdjustorThunk },
	{ 0x0600002F, Keyframe_set_value_m578277371F57893D5FF121710CF016A52302652E_AdjustorThunk },
	{ 0x06000030, Keyframe_get_inTangent_mE19351A6720331D0D01C299ED6480780B20CB8E0_AdjustorThunk },
	{ 0x06000031, Keyframe_set_inTangent_mAA19EC59A3F6D360310D2F6B765359A069F8058E_AdjustorThunk },
	{ 0x06000032, Keyframe_get_outTangent_mF1B82FA2B7E060906B77CEAF229516D69789BEAE_AdjustorThunk },
	{ 0x06000033, Keyframe_set_outTangent_mDEEA8549F31FA0AAC090B2D6D7E15C42D3CAE90B_AdjustorThunk },
	{ 0x06000034, Keyframe_get_weightedMode_mFF562D4B06BCF6A65135C337D6F10BF1C4F3E14D_AdjustorThunk },
	{ 0x06000035, Keyframe_set_weightedMode_m95923B248E6E9947136A115AF240F8C3A3CBC399_AdjustorThunk },
	{ 0x06000036, Keyframe_get_tangentMode_mBF872460D0D000E593C06723BB8FCDDD48983D77_AdjustorThunk },
	{ 0x06000037, Keyframe_set_tangentMode_mC8D2E68FDEF4D5C9083FAF35E6EACE989318A68C_AdjustorThunk },
	{ 0x06000038, Keyframe_get_tangentModeInternal_mCA542C1309EAB57FC634AFB50D31721FF82FB631_AdjustorThunk },
	{ 0x06000039, Keyframe_set_tangentModeInternal_mFF714C3EB51D2A08F6A375728C0280D124749AC2_AdjustorThunk },
	{ 0x06000073, CachedAssetBundle__ctor_mBB07A43F19F5095BC21A44543684F6B4269CE315_AdjustorThunk },
	{ 0x06000074, CachedAssetBundle_get_name_m31556A8588D4BE9261B0AB02486118A30D3ED971_AdjustorThunk },
	{ 0x06000075, CachedAssetBundle_get_hash_mC7D2AD78B64D7CDEF0604C934BF97D6C388A3EC1_AdjustorThunk },
	{ 0x06000076, Cache_get_handle_m2A81C085DE744E211DCC3F5BD608043EEB93075D_AdjustorThunk },
	{ 0x06000077, Cache_GetHashCode_m3C305D87FF4578864D1844170133E2758986B244_AdjustorThunk },
	{ 0x06000078, Cache_Equals_m4E85CF7FABB9052E9673879DF8FB341453D76F06_AdjustorThunk },
	{ 0x06000079, Cache_Equals_m49F0BDF7BE07D3FA50A70ED8FE85DBFF178C11C5_AdjustorThunk },
	{ 0x0600007A, Cache_get_valid_m01BE7338A2AB06309EC3808BD361F0A355E0B29F_AdjustorThunk },
	{ 0x0600007C, Cache_get_path_mC3C0F1863B3EE5BE472D43BC25797E1EC3278A6B_AdjustorThunk },
	{ 0x0600007E, Cache_set_maximumAvailableStorageSpace_m2F9DA827094B24940EE439739EC7B78EC5237559_AdjustorThunk },
	{ 0x06000080, Cache_set_expirationDelay_m2D2ABE9B1D1503A16A94A423852E7D037C7F213A_AdjustorThunk },
	{ 0x060000F2, Bounds__ctor_m294E77A20EC1A3E96985FE1A925CB271D1B5266D_AdjustorThunk },
	{ 0x060000F3, Bounds_GetHashCode_m9F5F751E9D52F99FCC3DC07407410078451F06AC_AdjustorThunk },
	{ 0x060000F4, Bounds_Equals_m1ECFAEFE19BAFB61FFECA5C0B8AE068483A39C61_AdjustorThunk },
	{ 0x060000F5, Bounds_Equals_mC2E2B04AB16455E2C17CD0B3C1497835DEA39859_AdjustorThunk },
	{ 0x060000F6, Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B_AdjustorThunk },
	{ 0x060000F7, Bounds_set_center_mAD29DD80FD631F83AF4E7558BB27A0398E8FD841_AdjustorThunk },
	{ 0x060000F8, Bounds_get_size_m0739F2686AE2D3416A33AEF892653091347FD4A6_AdjustorThunk },
	{ 0x060000F9, Bounds_set_size_m70855AC67A54062D676174B416FB06019226B39A_AdjustorThunk },
	{ 0x060000FA, Bounds_get_extents_mBA4B2196036DD5A858BDAD53BC71A778B41841C9_AdjustorThunk },
	{ 0x060000FB, Bounds_set_extents_mC83719146B06D0575A160CDDE9997202A1192B35_AdjustorThunk },
	{ 0x060000FC, Bounds_get_min_m2D48F74D29BF904D1AF19C562932E34ACAE2467C_AdjustorThunk },
	{ 0x060000FD, Bounds_get_max_mC3BE43C2A865BAC138D117684BC01E289892549B_AdjustorThunk },
	{ 0x06000100, Bounds_SetMinMax_m04969DE5CBC7F9843C12926ADD5F591159C86CA6_AdjustorThunk },
	{ 0x06000101, Bounds_Encapsulate_mD1F1DAC416D7147E07BF54D87CA7FF84C1088D8D_AdjustorThunk },
	{ 0x06000102, Bounds_ToString_m4637EA7C58B9C75651A040182471E9BAB9295666_AdjustorThunk },
	{ 0x06000103, Plane_get_normal_m203D43F51C449990214D04F332E8261295162E84_AdjustorThunk },
	{ 0x06000104, Plane_get_distance_m5358B80C35E1E295C0133E7DC6449BB09C456DEE_AdjustorThunk },
	{ 0x06000105, Plane__ctor_m6535EAD5E675627C2533962F1F7890CBFA2BA44A_AdjustorThunk },
	{ 0x06000106, Plane_Raycast_m04E61D7C78A5DA70F4F73F9805ABB54177B799A9_AdjustorThunk },
	{ 0x06000107, Plane_ToString_mF92ABB5136759C7DFBC26FD3957532B3C26F2099_AdjustorThunk },
	{ 0x06000108, Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B_AdjustorThunk },
	{ 0x06000109, Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187_AdjustorThunk },
	{ 0x0600010A, Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E_AdjustorThunk },
	{ 0x0600010B, Ray_GetPoint_mE8830D3BA68A184AD70514428B75F5664105ED08_AdjustorThunk },
	{ 0x0600010C, Ray_ToString_m73B5291E29C9C691773B44590C467A0D4FBE0EC1_AdjustorThunk },
	{ 0x0600010D, Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280_AdjustorThunk },
	{ 0x0600010E, Rect__ctor_m027E778E437FED8E6DBCE0C01C854ADF54986ECE_AdjustorThunk },
	{ 0x06000110, Rect_get_x_mC51A461F546D14832EB96B11A7198DADDE2597B7_AdjustorThunk },
	{ 0x06000111, Rect_set_x_m49EFE25263C03A48D52499C3E9C097298E0EA3A6_AdjustorThunk },
	{ 0x06000112, Rect_get_y_m53E3E4F62D9840FBEA751A66293038F1F5D1D45C_AdjustorThunk },
	{ 0x06000113, Rect_set_y_mCFDB9BD77334EF9CD896F64BE63C755777D7CCD5_AdjustorThunk },
	{ 0x06000114, Rect_get_position_m54A2ACD2F97988561D6C83FCEF7D082BC5226D4C_AdjustorThunk },
	{ 0x06000115, Rect_set_position_mD92DFF591D9C96CDD6AF22EA2052BB3D468D68ED_AdjustorThunk },
	{ 0x06000116, Rect_get_center_mA6E659EAAACC32132022AB199793BF641B3068CB_AdjustorThunk },
	{ 0x06000117, Rect_set_center_m6F280BE23E6FAA6214DDA6FB6B7697DB906D879E_AdjustorThunk },
	{ 0x06000118, Rect_get_min_m17345668569CF57C5F1D2B2DADD05DD4220A5950_AdjustorThunk },
	{ 0x06000119, Rect_set_min_m3A3DE206469065D65650DCF4D23F51C25B6C08A7_AdjustorThunk },
	{ 0x0600011A, Rect_get_max_m3BFB033D741F205FB04EF163A9D5785E7E020756_AdjustorThunk },
	{ 0x0600011B, Rect_set_max_mCC50E64F2DE589A3B7D1BFED72B49AC19D49FAEB_AdjustorThunk },
	{ 0x0600011C, Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484_AdjustorThunk },
	{ 0x0600011D, Rect_set_width_mC81EF602AC91E0C615C12FCE060254A461A152B8_AdjustorThunk },
	{ 0x0600011E, Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5_AdjustorThunk },
	{ 0x0600011F, Rect_set_height_mF4CB5A97D4706696F1C9EA31A5D8C466E48050D6_AdjustorThunk },
	{ 0x06000120, Rect_get_size_m731642B8F03F6CE372A2C9E2E4A925450630606C_AdjustorThunk },
	{ 0x06000121, Rect_set_size_m4618056983660063A74F40CCFF9A683933CB4C93_AdjustorThunk },
	{ 0x06000122, Rect_get_xMin_mFDFA74F66595FD2B8CE360183D1A92B575F0A76E_AdjustorThunk },
	{ 0x06000123, Rect_set_xMin_mD8F9BF59F4F33F9C3AB2FEFF32D8C16756B51E34_AdjustorThunk },
	{ 0x06000124, Rect_get_yMin_m31EDC3262BE39D2F6464B15397F882237E6158C3_AdjustorThunk },
	{ 0x06000125, Rect_set_yMin_m58C137C81F3D098CF81498964E1B5987882883A7_AdjustorThunk },
	{ 0x06000126, Rect_get_xMax_mA16D7C3C2F30F8608719073ED79028C11CE90983_AdjustorThunk },
	{ 0x06000127, Rect_set_xMax_m1775041FCD5CA22C77D75CC780D158CD2B31CEAF_AdjustorThunk },
	{ 0x06000128, Rect_get_yMax_m8AA5E92C322AF3FF571330F00579DA864F33341B_AdjustorThunk },
	{ 0x06000129, Rect_set_yMax_m4F1C5632CD4836853A22E979C810C279FBB20B95_AdjustorThunk },
	{ 0x0600012A, Rect_Contains_mAD3D41C88795960F177088F847509C9DDA23B682_AdjustorThunk },
	{ 0x0600012B, Rect_Contains_m5072228CE6251E7C754F227BA330F9ADA95C1495_AdjustorThunk },
	{ 0x0600012D, Rect_Overlaps_m2FE484659899E54C13772AB7D9E202239A637559_AdjustorThunk },
	{ 0x0600012E, Rect_Overlaps_m4FFECCEAB3FBF23ED5A51B2E26220F035B942B4B_AdjustorThunk },
	{ 0x06000131, Rect_GetHashCode_mA23F5D7C299F7E05A0390DF2FA663F5A003799C6_AdjustorThunk },
	{ 0x06000132, Rect_Equals_m76E3B7E2E5CC43299C4BF4CB2EA9EF6E989E23E3_AdjustorThunk },
	{ 0x06000133, Rect_Equals_mC8430F80283016D0783FB6C4E7461BEED4B55C82_AdjustorThunk },
	{ 0x06000134, Rect_ToString_m045E7857658F27052323E301FBA3867AD13A6FE5_AdjustorThunk },
	{ 0x06000172, Resolution_ToString_m42289CE0FC4ED41A9DC62B398F46F7954BC52F04_AdjustorThunk },
	{ 0x060003D7, RenderTextureDescriptor_get_width_m225FBFD7C33BD02D6879A93F1D57997BC251F3F5_AdjustorThunk },
	{ 0x060003D8, RenderTextureDescriptor_set_width_m48ADD4AB04E8DBEEC5CA8CC96F86D2674F4FE55F_AdjustorThunk },
	{ 0x060003D9, RenderTextureDescriptor_get_height_m947A620B3D28090A57A5DC0D6A126CBBF818B97F_AdjustorThunk },
	{ 0x060003DA, RenderTextureDescriptor_set_height_mD19D74EC9679250F63489CF1950351EFA83A1A45_AdjustorThunk },
	{ 0x060003DB, RenderTextureDescriptor_get_msaaSamples_mEBE0D743E17068D1898DAE2D281C913E39A33616_AdjustorThunk },
	{ 0x060003DC, RenderTextureDescriptor_set_msaaSamples_m5856FC43DAD667D4462B6BCA938B70E42068D24C_AdjustorThunk },
	{ 0x060003DD, RenderTextureDescriptor_get_volumeDepth_mBC82F4621E4158E3D2F2457487D1C220AA782AC6_AdjustorThunk },
	{ 0x060003DE, RenderTextureDescriptor_set_volumeDepth_mDC402E6967166BE8CADDA690B32136A1E0AB8583_AdjustorThunk },
	{ 0x060003DF, RenderTextureDescriptor_set_mipCount_m98C1CE257A8152D8B23EC27D68D0C4C35683DEE5_AdjustorThunk },
	{ 0x060003E0, RenderTextureDescriptor_get_graphicsFormat_mD2DD8AC2E1324779F8D497697E725FB93341A14D_AdjustorThunk },
	{ 0x060003E1, RenderTextureDescriptor_set_graphicsFormat_mF24C183BA9C9C42923CEC3ED353661D54AA741CA_AdjustorThunk },
	{ 0x060003E2, RenderTextureDescriptor_get_depthBufferBits_m51E82C47A0CA0BD8B20F90D43169C956C4F24996_AdjustorThunk },
	{ 0x060003E3, RenderTextureDescriptor_set_depthBufferBits_mED58A8D9643740713597B0244BF76D0395C1C479_AdjustorThunk },
	{ 0x060003E4, RenderTextureDescriptor_set_dimension_mBC3C8793345AC5E627BDD932B46A5F93568ACCE5_AdjustorThunk },
	{ 0x060003E5, RenderTextureDescriptor_set_shadowSamplingMode_m0AC43F8A8BE0755567EED4DDFADBE207739E1ECF_AdjustorThunk },
	{ 0x060003E6, RenderTextureDescriptor_set_vrUsage_mC591604135749B0BD156865141E114F51812E502_AdjustorThunk },
	{ 0x060003E7, RenderTextureDescriptor_set_memoryless_m1FB3F7E8B482C71BDB549AD71D762BCF337D8185_AdjustorThunk },
	{ 0x060003E8, RenderTextureDescriptor__ctor_m227EBED323830B1059806874C69E1426DDC16B85_AdjustorThunk },
	{ 0x060003E9, RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m387E338D17ED601B0587EF4E5CEB10DDFC42CC05_AdjustorThunk },
	{ 0x060003EB, Hash128_get_isValid_m45005B82BA8416D4ACA8B20DF9DA5B8513C602B3_AdjustorThunk },
	{ 0x060003EC, Hash128_CompareTo_m0BC4F8F4228CF2B48C615E39CD3CE260386CC5BC_AdjustorThunk },
	{ 0x060003ED, Hash128_ToString_mD81890597BCFD67BE47646DA5366347C545EB5E8_AdjustorThunk },
	{ 0x060003F1, Hash128_Equals_m4701FDD64B5C5F81B1E494D5586C5CB4519BC007_AdjustorThunk },
	{ 0x060003F2, Hash128_Equals_m85BCDD87EB2A629FB8BB72FD63244787BC47E52D_AdjustorThunk },
	{ 0x060003F3, Hash128_GetHashCode_m65DA1711C64E83AB514A3D498C568CB10EAA0D69_AdjustorThunk },
	{ 0x060003F4, Hash128_CompareTo_m3595697B0FC7ACAED77C03FEC7FF80A073A4F6AE_AdjustorThunk },
	{ 0x0600041E, Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C_AdjustorThunk },
	{ 0x0600041F, Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369_AdjustorThunk },
	{ 0x06000420, Color_ToString_m17A27E0CFB20D9946D130DAEDB5BDCACA5A4473F_AdjustorThunk },
	{ 0x06000421, Color_GetHashCode_m88317C719D2DAA18E293B3F5CD17B9FB80E26CF1_AdjustorThunk },
	{ 0x06000422, Color_Equals_m63ECBA87A0F27CD7D09EEA36BCB697652E076F4E_AdjustorThunk },
	{ 0x06000423, Color_Equals_mA81EEDDC4250DE67C2F43BC88A102EA32A138052_AdjustorThunk },
	{ 0x0600042C, Color_RGBMultiplied_m41914B23903491843FAA3B0C02027EF8B70F34CF_AdjustorThunk },
	{ 0x06000434, Color_get_linear_mB10CD29D56ADE2C811AD74A605BA11F6656E9D1A_AdjustorThunk },
	{ 0x06000435, Color_get_maxColorComponent_mBA8595CB2790747F42145CB696C10E64C9BBD76D_AdjustorThunk },
	{ 0x06000438, Color32__ctor_m1AEF46FBBBE4B522E6984D081A3D158198E10AA2_AdjustorThunk },
	{ 0x0600043B, Color32_ToString_m217F2AD5C02E630E37BE5CFB0933630F6D0C3555_AdjustorThunk },
	{ 0x0600044C, Matrix4x4_GetLossyScale_m0FB088456F10D189CDBAD737670B053EE644BB5C_AdjustorThunk },
	{ 0x0600044D, Matrix4x4_get_lossyScale_m6E184C6942DA569CE4427C967501B3B4029A655E_AdjustorThunk },
	{ 0x0600044F, Matrix4x4__ctor_mC7C5A4F0791B2A3ADAFE1E6C491B7705B6492B12_AdjustorThunk },
	{ 0x06000450, Matrix4x4_GetHashCode_m6627C82FBE2092AE4711ABA909D0E2C3C182028F_AdjustorThunk },
	{ 0x06000451, Matrix4x4_Equals_m7FB9C1A249956C6CDE761838B92097C525596D31_AdjustorThunk },
	{ 0x06000452, Matrix4x4_Equals_mF8358F488D95A9C2E5A9F69F31EC7EA0F4640E51_AdjustorThunk },
	{ 0x06000453, Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E_AdjustorThunk },
	{ 0x06000454, Matrix4x4_MultiplyPoint_mD5D082585C5B3564A5EFC90A3C5CAFFE47E45B65_AdjustorThunk },
	{ 0x06000455, Matrix4x4_MultiplyPoint3x4_m7C872FDCC9E3378E00A40977F641A45A24994E9A_AdjustorThunk },
	{ 0x06000456, Matrix4x4_ToString_m7E29D2447E2FC1EAB3D8565B7DCAFB9037C69E1D_AdjustorThunk },
	{ 0x0600045D, Vector3_get_Item_mC3B9D35C070A91D7CA5C5B47280BD0EA3E148AC6_AdjustorThunk },
	{ 0x0600045E, Vector3_set_Item_m89FF112CEC0D9ED43F1C4FE01522C75394B30AE6_AdjustorThunk },
	{ 0x0600045F, Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1_AdjustorThunk },
	{ 0x06000460, Vector3__ctor_m6AD8F21FFCC7723C6F507CCF2E4E2EFFC4871584_AdjustorThunk },
	{ 0x06000461, Vector3_Scale_mD40CDE2B62BCBABD49426FAE104814F29CF2AA0B_AdjustorThunk },
	{ 0x06000463, Vector3_GetHashCode_m6C42B4F413A489535D180E8A99BE0298AD078B0B_AdjustorThunk },
	{ 0x06000464, Vector3_Equals_m1F74B1FB7EE51589FFFA61D894F616B8F258C056_AdjustorThunk },
	{ 0x06000465, Vector3_Equals_m6B991540378DB8541CEB9472F7ED2BF5FF72B5DB_AdjustorThunk },
	{ 0x06000467, Vector3_Normalize_m174460238EC6322B9095A378AA8624B1DD9000F3_AdjustorThunk },
	{ 0x06000468, Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B_AdjustorThunk },
	{ 0x0600046E, Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274_AdjustorThunk },
	{ 0x0600046F, Vector3_get_sqrMagnitude_m1C6E190B4A933A183B308736DEC0DD64B0588968_AdjustorThunk },
	{ 0x06000482, Vector3_ToString_m2682D27AB50CD1CE4677C38D0720A302D582348D_AdjustorThunk },
	{ 0x0600048B, Quaternion__ctor_m7502F0C38E04C6DE24C965D1CAF278DDD02B9D61_AdjustorThunk },
	{ 0x06000493, Quaternion_SetLookRotation_mDB3D5A8083E5AB5881FA9CC1EACFC196F61B8204_AdjustorThunk },
	{ 0x06000495, Quaternion_get_eulerAngles_mF8ABA8EB77CD682017E92F0F457374E54BC943F9_AdjustorThunk },
	{ 0x06000498, Quaternion_GetHashCode_m43BDCF3A72E31FA4063C1DEB770890FF47033458_AdjustorThunk },
	{ 0x06000499, Quaternion_Equals_m099618C36B86DC63B2E7C89673C8566B18E5996E_AdjustorThunk },
	{ 0x0600049A, Quaternion_Equals_m0A269A9B77E915469801463C8BBEF7A06EF94A09_AdjustorThunk },
	{ 0x0600049B, Quaternion_ToString_m38DF4A1C05A91331D0A208F45CE74AF005AB463D_AdjustorThunk },
	{ 0x060004CB, Vector2_get_Item_m67344A67120E48C32D9419E24BA7AED29F063379_AdjustorThunk },
	{ 0x060004CC, Vector2_set_Item_m2335DC41E2BB7E64C21CDF0EEDE64FFB56E7ABD1_AdjustorThunk },
	{ 0x060004CD, Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0_AdjustorThunk },
	{ 0x060004D1, Vector2_Normalize_m99A2CC6E4CB65C1B9231F898D5B7A12B6D72E722_AdjustorThunk },
	{ 0x060004D2, Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B_AdjustorThunk },
	{ 0x060004D3, Vector2_ToString_m83C7C331834382748956B053E252AE3BD21807C4_AdjustorThunk },
	{ 0x060004D4, Vector2_GetHashCode_m028AB6B14EBC6D668CFA45BF6EDEF17E2C44EA54_AdjustorThunk },
	{ 0x060004D5, Vector2_Equals_m4A2A75BC3D09933321220BCEF21219B38AF643AE_AdjustorThunk },
	{ 0x060004D6, Vector2_Equals_mD6BF1A738E3CAF57BB46E604B030C072728F4EEB_AdjustorThunk },
	{ 0x060004D8, Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF_AdjustorThunk },
	{ 0x060004D9, Vector2_get_sqrMagnitude_mAEE10A8ECE7D5754E10727BA8C9068A759AD7002_AdjustorThunk },
	{ 0x060004EE, Vector2Int_get_x_m300C7C05CD66D24EE62D91CDC0C557A6C0ABF25E_AdjustorThunk },
	{ 0x060004EF, Vector2Int_set_x_mC6F9F1E43668A7F8E8C44CEFB427BA97389F1420_AdjustorThunk },
	{ 0x060004F0, Vector2Int_get_y_m0D6E131AB5FA4AD532DEC377F981AADA189E680B_AdjustorThunk },
	{ 0x060004F1, Vector2Int_set_y_mF79CD7FACF0A9A1CC12678E2E24BD43C5E836D88_AdjustorThunk },
	{ 0x060004F2, Vector2Int__ctor_m501C34762BA7ECDDCFC25C19A4B9C93BC15004E1_AdjustorThunk },
	{ 0x060004F6, Vector2Int_Equals_mF7D7EFBC0286224832BA76701801C28530D40479_AdjustorThunk },
	{ 0x060004F7, Vector2Int_Equals_m65420C995F326F5C340E4825EA5E16BDE68F5A9C_AdjustorThunk },
	{ 0x060004F8, Vector2Int_GetHashCode_m73E874F4E94DF3D2603035E2E892873B139A7A9E_AdjustorThunk },
	{ 0x060004F9, Vector2Int_ToString_mB0D67C1885311767BA89D4C4A6E3A5A515194BF3_AdjustorThunk },
	{ 0x060004FB, Vector3Int_get_x_m23CB00F1579FD4CE86291940E2E75FB13405D53A_AdjustorThunk },
	{ 0x060004FC, Vector3Int_set_x_mED89A481E7FF21D0BF9F61511D5442E2CE9BEB75_AdjustorThunk },
	{ 0x060004FD, Vector3Int_get_y_m1C2F0AB641A167DF22F9C3C57092EC05AEF8CA26_AdjustorThunk },
	{ 0x060004FE, Vector3Int_set_y_m3E8A59AC8851E9AEC6B65AA167047C1F7C497B84_AdjustorThunk },
	{ 0x060004FF, Vector3Int_get_z_m9A88DC2346FD1838EC611CC8AB2FC29951E94183_AdjustorThunk },
	{ 0x06000500, Vector3Int_set_z_m7BFC415EBBF5D31ACF664EEC37EE801C22B0E8D5_AdjustorThunk },
	{ 0x06000501, Vector3Int__ctor_m171D642C38B163B353DAE9CCE90ACFE0894C1156_AdjustorThunk },
	{ 0x06000503, Vector3Int_Equals_m704D204F83B9C64C7AF06152F98B542C5C400DC7_AdjustorThunk },
	{ 0x06000504, Vector3Int_Equals_m9F98F28666ADF5AD0575C4CABAF6881F1317D4C1_AdjustorThunk },
	{ 0x06000505, Vector3Int_GetHashCode_m6CDE2FEC995180949111253817BD0E4ECE7EAE3D_AdjustorThunk },
	{ 0x06000506, Vector3Int_ToString_m08AB1BE6A674B2669839B1C44ACCF6D85EBCFB91_AdjustorThunk },
	{ 0x06000508, Vector4_get_Item_m39878FDA732B20347BB37CD1485560E9267EDC98_AdjustorThunk },
	{ 0x06000509, Vector4_set_Item_m56FB3A149299FEF1C0CF638CFAF71C7F0685EE45_AdjustorThunk },
	{ 0x0600050A, Vector4__ctor_m545458525879607A5392A10B175D0C19B2BC715D_AdjustorThunk },
	{ 0x0600050C, Vector4_GetHashCode_m7329FEA2E90CDBDBF4F09F51D92C87E08F5DC92E_AdjustorThunk },
	{ 0x0600050D, Vector4_Equals_m552ECA9ECD220D6526D8ECC9902016B6FC6D49B5_AdjustorThunk },
	{ 0x0600050E, Vector4_Equals_mB9894C2D4EE56C6E8FDF6CC40DCE0CE16BA4F7BF_AdjustorThunk },
	{ 0x06000510, Vector4_get_magnitude_mE33CE76438DDE4DDBBAD94178B07D9364674D356_AdjustorThunk },
	{ 0x06000511, Vector4_get_sqrMagnitude_m6B2707CBD31D237605D066A5925E6419D28B5397_AdjustorThunk },
	{ 0x0600051A, Vector4_ToString_m769402E3F7CBD6C92464D916527CC87BBBA53EF9_AdjustorThunk },
	{ 0x060005E9, LayerMask_get_value_m682288E860BBE36F5668DCDBC59245DE6319E537_AdjustorThunk },
	{ 0x060005EA, LayerMask_set_value_m56653944C7760E9D456E6B9E68F85CAA5B24A042_AdjustorThunk },
	{ 0x06000614, RangeInt_get_end_m7A5182161CC5454E1C200E0173668572BA7FAAFD_AdjustorThunk },
	{ 0x06000615, RangeInt__ctor_mACFE54DF73DE3F62053F851423525DB5AC1B100E_AdjustorThunk },
	{ 0x06000674, WorkRequest__ctor_mE19AE1779B544378C8CB488F1576BDE618548599_AdjustorThunk },
	{ 0x06000675, WorkRequest_Invoke_m67D71A48794EEBB6B9793E6F1E015DE90C03C1ED_AdjustorThunk },
	{ 0x060006D9, DrivenRectTransformTracker_Add_m51059F302FBD574E93820E8116283D1608D1AB5A_AdjustorThunk },
	{ 0x060006DA, DrivenRectTransformTracker_Clear_m328659F339A4FB519C9A208A685DDED106B6FC89_AdjustorThunk },
	{ 0x0600079D, DebugScreenCapture_set_rawImageDataReference_mE09725CEBC8D7A846033531E33C8E9B8DA60DFDE_AdjustorThunk },
	{ 0x0600079E, DebugScreenCapture_set_imageFormat_m779E16070B55AACD76279BFDDAE5CC470667D3FB_AdjustorThunk },
	{ 0x0600079F, DebugScreenCapture_set_width_mD8B3C521091CD0049D323410FFD201D47B629127_AdjustorThunk },
	{ 0x060007A0, DebugScreenCapture_set_height_mD7E3596B40E77F906A3E9517BC6CE795C0609442_AdjustorThunk },
	{ 0x06000823, MovedFromAttributeData_Set_m244D6454BEA753FA4C7C3F2393A5DADCB3166B1C_AdjustorThunk },
	{ 0x0600082A, Scene_get_handle_mFAB5C41D41B90B9CEBB3918A6F3638BD41E980C9_AdjustorThunk },
	{ 0x0600082B, Scene_IsValid_mDA115AE2634680CAE92D2F844347AB4CD2185437_AdjustorThunk },
	{ 0x0600082C, Scene_get_path_m6D313F8791A3376EBE73AA6F62C02731F7F48A86_AdjustorThunk },
	{ 0x0600082D, Scene_get_name_m0E63ED0F050FCC35A4216220C584BE3D3F77B0E1_AdjustorThunk },
	{ 0x0600082E, Scene_get_isLoaded_m6A733EA533B0DC0041A9579A95530B41F869435C_AdjustorThunk },
	{ 0x0600082F, Scene_get_buildIndex_m764659943B7357F5D6C9165F68EDCFBBDDD6C3C2_AdjustorThunk },
	{ 0x06000831, Scene_GetHashCode_m65BBB604A5496CF1F2C129860F45D0E437499E34_AdjustorThunk },
	{ 0x06000832, Scene_Equals_mD5738AF0B92757DED12A90F136716A5E2DDE3F54_AdjustorThunk },
	{ 0x06000851, LoadSceneParameters_set_loadSceneMode_mD56FFF214E3909A68BD9C485BBB4D99ADC3A5624_AdjustorThunk },
	{ 0x06000852, LoadSceneParameters__ctor_m2E00BEC64DCC48351831B7BE9088D9B6AF62102A_AdjustorThunk },
	{ 0x0600088D, LODParameters_Equals_mA7C4CFD75190B341999074C56E1BAD9E5136CF61_AdjustorThunk },
	{ 0x0600088E, LODParameters_Equals_m03754D13F85184E596AFBBCF6DA1EDB06CA58F6B_AdjustorThunk },
	{ 0x0600088F, LODParameters_GetHashCode_m532D8960CCF3340F7CDF6B757D33BAE13B152716_AdjustorThunk },
	{ 0x060008B6, ScriptableRenderContext_GetNumberOfCameras_Internal_m58DE22F11E08F3B9C7E4B1D9788711101EBD2395_AdjustorThunk },
	{ 0x060008B7, ScriptableRenderContext_GetCamera_Internal_m1E56C9D9782F0E99A7ED3C64C15F3A06A8A0B917_AdjustorThunk },
	{ 0x060008B8, ScriptableRenderContext__ctor_m554E9C4BB7F69601E65F71557914C6958D3181EE_AdjustorThunk },
	{ 0x060008B9, ScriptableRenderContext_GetNumberOfCameras_mF14CD21DA4E8A43DCE5811735E48748313F71CBA_AdjustorThunk },
	{ 0x060008BA, ScriptableRenderContext_GetCamera_mCC4F389E3EB259D9FF01E7F7801D39137BC0E41C_AdjustorThunk },
	{ 0x060008BB, ScriptableRenderContext_Equals_m0612225D9DC8BE5574C67738B9B185A05B306803_AdjustorThunk },
	{ 0x060008BC, ScriptableRenderContext_Equals_m239EBA23E760DDF7CC7112D3299D9B4CEA449683_AdjustorThunk },
	{ 0x060008BD, ScriptableRenderContext_GetHashCode_m2A894A66C98DF28B51758A3579EE04B87D2AC6D6_AdjustorThunk },
	{ 0x060008C0, ShaderTagId_get_id_mB1AB67FC09D015D245DAB1F6C491F2D59558C5D5_AdjustorThunk },
	{ 0x060008C1, ShaderTagId_set_id_m47A84B8D0203D1BAE247CF591D1E2B43A1FFEF1E_AdjustorThunk },
	{ 0x060008C2, ShaderTagId_Equals_m7C6B4B00D502970BEDA5032CC9F791795B8C44EE_AdjustorThunk },
	{ 0x060008C3, ShaderTagId_Equals_mC8A45F721611717B4CB12DBD73E23200C67AA7AF_AdjustorThunk },
	{ 0x060008C4, ShaderTagId_GetHashCode_m51C1D9E4417AD43F0A5E9CCF7FF88C10BCDB2905_AdjustorThunk },
	{ 0x060008DB, BatchCullingContext__ctor_m5BB85EDAC0F7C5AFFABF7AD30F18AD272EA23E32_AdjustorThunk },
	{ 0x060008EB, Playable__ctor_m24C6ED455A921F585698BFFEC5CCED397205543E_AdjustorThunk },
	{ 0x060008EC, Playable_GetHandle_m952F17BACFC90BEACD3CB9880E65E69B3271108A_AdjustorThunk },
	{ 0x060008ED, Playable_Equals_m1EC7E8B579C862BA8C979BD616224EC1B3364DD1_AdjustorThunk },
	{ 0x06000907, PlayableHandle_Equals_m7D3DC594EE8EE029E514FF9505C5E7A820D2435E_AdjustorThunk },
	{ 0x06000908, PlayableHandle_Equals_mBCBD135BA1DBB6B5F8884A21EE55FDD5EADB555C_AdjustorThunk },
	{ 0x06000909, PlayableHandle_GetHashCode_mFEF967B1397A1DC2EE05FC8DBB9C5E13161E3C45_AdjustorThunk },
	{ 0x0600090B, PlayableHandle_IsValid_mDA0A998EA6E2442C5F3B6CDFAF07EBA9A6873059_AdjustorThunk },
	{ 0x0600090C, PlayableHandle_GetPlayableType_m962BE384C093FF07EAF156DA373806C2D6EF1AD1_AdjustorThunk },
	{ 0x06000910, PlayableOutput__ctor_m881EC9E4BAD358971373EE1D6BF9C37DDB7A4943_AdjustorThunk },
	{ 0x06000911, PlayableOutput_GetHandle_m079ADC0139E95AA914CD7502F27EC79BB1A876F3_AdjustorThunk },
	{ 0x06000912, PlayableOutput_Equals_m458689DD056559A7460CCE496BF6BEC45EB47C5B_AdjustorThunk },
	{ 0x06000915, PlayableOutputHandle_GetHashCode_mC35D44FD77BCA850B945C047C6E2913436B1DF1C_AdjustorThunk },
	{ 0x06000917, PlayableOutputHandle_Equals_m42558FEC083CC424CB4BD715FC1A6561A2E47EB2_AdjustorThunk },
	{ 0x06000918, PlayableOutputHandle_Equals_m1FCA747BC3F69DC784912A932557EB3B24DC3F18_AdjustorThunk },
	{ 0x0600091D, LightDataGI_Init_m93EBCF45B2A5F8A0C9879FD2CF1B853514837F4B_AdjustorThunk },
	{ 0x0600091E, LightDataGI_Init_m236C2DEE096CDCF4B4489B9A5630E231531DF022_AdjustorThunk },
	{ 0x0600091F, LightDataGI_Init_mBCCAA12227CF3845C831463F7B8572F00CB17CF3_AdjustorThunk },
	{ 0x06000920, LightDataGI_Init_m24775B5AF758CAE25BA180BC17D9E032064B52B9_AdjustorThunk },
	{ 0x06000921, LightDataGI_Init_m00F56356E1220B292978ABE384B6500A1F1C9291_AdjustorThunk },
	{ 0x06000922, LightDataGI_InitNoBake_m660C58A4878A0DB9E842F642AA6D2800D30F0C48_AdjustorThunk },
	{ 0x06000936, CameraPlayable_GetHandle_mC710651CAEE2596697CFFC01014BEB041C67E2B4_AdjustorThunk },
	{ 0x06000937, CameraPlayable_Equals_mBA0325A3187672B8FDE237D2D5229474C19E3A52_AdjustorThunk },
	{ 0x06000938, MaterialEffectPlayable_GetHandle_mB0F6F324656489CE4DE4EFA8ACCE37458D292439_AdjustorThunk },
	{ 0x06000939, MaterialEffectPlayable_Equals_m9C2DB0EB37CFB9679961D667B61E2360384C71DA_AdjustorThunk },
	{ 0x0600093A, TextureMixerPlayable_GetHandle_mBC5D38A23834675B7A8D5314DCF4655C83AE649C_AdjustorThunk },
	{ 0x0600093B, TextureMixerPlayable_Equals_mEDE18FD43C9501F04871D2357DC66BABE43F397B_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2397] = 
{
	3,
	32,
	26,
	14,
	23,
	26,
	1399,
	3,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	23,
	23,
	-1,
	-1,
	352,
	-1,
	-1,
	-1,
	23,
	129,
	21,
	1400,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	10,
	32,
	10,
	32,
	10,
	32,
	25,
	24,
	1401,
	23,
	1402,
	14,
	1403,
	10,
	1403,
	14,
	10,
	32,
	10,
	32,
	26,
	23,
	9,
	9,
	10,
	835,
	49,
	49,
	4,
	4,
	4,
	4,
	4,
	4,
	154,
	106,
	859,
	49,
	49,
	106,
	49,
	106,
	106,
	3,
	154,
	154,
	729,
	49,
	3,
	3,
	859,
	154,
	49,
	124,
	23,
	105,
	26,
	124,
	118,
	1353,
	26,
	18,
	7,
	1404,
	14,
	1405,
	10,
	10,
	9,
	1406,
	89,
	46,
	14,
	43,
	200,
	1407,
	32,
	165,
	859,
	49,
	1408,
	1408,
	1408,
	114,
	1409,
	1410,
	1411,
	1412,
	1413,
	1412,
	1414,
	1414,
	1415,
	216,
	1416,
	391,
	839,
	396,
	17,
	17,
	17,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	10,
	32,
	10,
	1417,
	1418,
	10,
	32,
	1419,
	1420,
	1419,
	1420,
	14,
	10,
	1421,
	1421,
	1422,
	1422,
	1422,
	1423,
	1424,
	1425,
	4,
	4,
	106,
	94,
	106,
	94,
	154,
	154,
	154,
	23,
	6,
	6,
	6,
	6,
	6,
	6,
	1426,
	1426,
	582,
	1426,
	124,
	26,
	205,
	26,
	1427,
	124,
	1428,
	1429,
	26,
	373,
	154,
	1430,
	137,
	40,
	27,
	23,
	4,
	694,
	154,
	137,
	137,
	154,
	137,
	137,
	186,
	154,
	137,
	154,
	137,
	137,
	186,
	154,
	49,
	135,
	49,
	3,
	1431,
	10,
	9,
	1432,
	1433,
	1434,
	1433,
	1434,
	1433,
	1434,
	1433,
	1433,
	1435,
	1435,
	1431,
	1434,
	14,
	1433,
	731,
	1431,
	1436,
	14,
	1431,
	1433,
	1433,
	1437,
	14,
	1438,
	1439,
	1440,
	731,
	337,
	731,
	337,
	1441,
	1442,
	1441,
	1442,
	1441,
	1442,
	1441,
	1442,
	731,
	337,
	731,
	337,
	1441,
	1442,
	731,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	1443,
	1444,
	1445,
	1446,
	1447,
	1448,
	1448,
	10,
	9,
	1446,
	14,
	23,
	124,
	23,
	341,
	14,
	23,
	769,
	25,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	10,
	1449,
	1450,
	1450,
	1451,
	349,
	1452,
	1452,
	17,
	3,
	3,
	23,
	7,
	10,
	10,
	10,
	10,
	1453,
	4,
	154,
	3,
	1454,
	1454,
	1455,
	3,
	124,
	23,
	105,
	26,
	106,
	106,
	1456,
	106,
	106,
	49,
	106,
	1440,
	17,
	106,
	106,
	164,
	3,
	3,
	3,
	14,
	106,
	4,
	106,
	731,
	337,
	731,
	337,
	731,
	337,
	1457,
	1418,
	1418,
	32,
	1458,
	6,
	6,
	835,
	1459,
	769,
	25,
	23,
	23,
	23,
	1460,
	835,
	14,
	14,
	26,
	26,
	26,
	26,
	26,
	89,
	31,
	32,
	31,
	10,
	32,
	10,
	32,
	14,
	26,
	14,
	26,
	106,
	164,
	0,
	0,
	10,
	32,
	106,
	164,
	89,
	4,
	154,
	154,
	154,
	114,
	10,
	10,
	3,
	94,
	43,
	94,
	28,
	10,
	1461,
	56,
	1462,
	1463,
	1464,
	589,
	1465,
	589,
	1466,
	267,
	1467,
	1468,
	43,
	1465,
	1465,
	1465,
	43,
	43,
	43,
	21,
	21,
	21,
	589,
	589,
	589,
	1465,
	1465,
	1465,
	589,
	589,
	589,
	1469,
	1462,
	373,
	165,
	1470,
	1463,
	1471,
	1472,
	1473,
	1464,
	137,
	589,
	195,
	1465,
	137,
	589,
	1466,
	137,
	589,
	137,
	589,
	137,
	589,
	137,
	589,
	137,
	589,
	137,
	589,
	493,
	267,
	94,
	21,
	1474,
	1467,
	1475,
	1476,
	1477,
	1468,
	0,
	43,
	0,
	43,
	0,
	43,
	0,
	43,
	137,
	589,
	137,
	589,
	137,
	589,
	23,
	119,
	131,
	131,
	119,
	131,
	119,
	1478,
	131,
	119,
	373,
	10,
	112,
	34,
	37,
	37,
	34,
	37,
	34,
	1479,
	1480,
	1481,
	37,
	34,
	1482,
	1482,
	1482,
	1482,
	844,
	0,
	137,
	137,
	154,
	26,
	26,
	26,
	4,
	4,
	4,
	14,
	26,
	1417,
	1418,
	14,
	26,
	1441,
	1442,
	1441,
	1442,
	37,
	30,
	9,
	10,
	32,
	10,
	26,
	26,
	9,
	10,
	32,
	89,
	31,
	89,
	31,
	10,
	459,
	9,
	34,
	112,
	27,
	1030,
	1030,
	148,
	1483,
	30,
	26,
	14,
	26,
	14,
	26,
	10,
	14,
	14,
	26,
	26,
	26,
	26,
	1020,
	1459,
	1484,
	62,
	805,
	62,
	496,
	1479,
	1485,
	1486,
	34,
	805,
	805,
	805,
	805,
	34,
	34,
	34,
	34,
	37,
	37,
	37,
	37,
	62,
	62,
	62,
	62,
	1480,
	1487,
	1487,
	805,
	805,
	805,
	805,
	62,
	62,
	62,
	62,
	943,
	1020,
	130,
	129,
	1460,
	1459,
	1488,
	1489,
	1490,
	1484,
	27,
	62,
	118,
	805,
	27,
	62,
	36,
	496,
	27,
	62,
	27,
	62,
	27,
	62,
	27,
	62,
	27,
	62,
	27,
	62,
	27,
	62,
	27,
	62,
	219,
	1479,
	112,
	37,
	1491,
	1485,
	1492,
	1480,
	1493,
	1486,
	28,
	34,
	28,
	34,
	28,
	34,
	28,
	34,
	28,
	34,
	27,
	62,
	27,
	62,
	27,
	62,
	27,
	62,
	1494,
	1487,
	1494,
	1487,
	1495,
	1481,
	1495,
	1481,
	835,
	835,
	835,
	835,
	835,
	835,
	835,
	10,
	731,
	1417,
	1418,
	731,
	337,
	731,
	731,
	337,
	10,
	731,
	337,
	6,
	6,
	23,
	26,
	23,
	154,
	23,
	93,
	1496,
	32,
	30,
	1497,
	191,
	89,
	10,
	1498,
	1499,
	31,
	23,
	23,
	37,
	21,
	-1,
	-1,
	1497,
	-1,
	-1,
	-1,
	-1,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	14,
	14,
	26,
	26,
	35,
	26,
	35,
	26,
	35,
	26,
	35,
	-1,
	62,
	496,
	23,
	860,
	30,
	30,
	26,
	34,
	93,
	38,
	1500,
	130,
	1334,
	1501,
	35,
	1502,
	755,
	23,
	23,
	23,
	6,
	6,
	23,
	10,
	10,
	10,
	32,
	10,
	32,
	89,
	10,
	1441,
	30,
	52,
	28,
	3,
	6,
	10,
	4,
	4,
	4,
	4,
	4,
	4,
	31,
	1503,
	1504,
	89,
	42,
	52,
	1505,
	1506,
	1507,
	1508,
	1509,
	1510,
	1511,
	9,
	1512,
	1513,
	16,
	172,
	185,
	89,
	89,
	10,
	10,
	32,
	10,
	32,
	89,
	31,
	10,
	10,
	10,
	10,
	23,
	89,
	23,
	7,
	130,
	1514,
	14,
	1515,
	1516,
	34,
	14,
	815,
	54,
	58,
	1517,
	341,
	341,
	524,
	1518,
	1519,
	339,
	39,
	129,
	1520,
	1521,
	1522,
	1514,
	1523,
	130,
	26,
	1524,
	1506,
	1525,
	1526,
	1337,
	26,
	-1,
	-1,
	-1,
	-1,
	42,
	31,
	23,
	52,
	1508,
	1509,
	1527,
	469,
	130,
	26,
	1514,
	1523,
	34,
	14,
	1528,
	1528,
	1529,
	1530,
	1531,
	1532,
	89,
	38,
	38,
	38,
	341,
	1533,
	1534,
	1053,
	89,
	1535,
	1536,
	524,
	524,
	297,
	524,
	1519,
	89,
	1535,
	1536,
	524,
	524,
	297,
	1537,
	1538,
	1519,
	89,
	1539,
	1540,
	341,
	341,
	524,
	1519,
	339,
	39,
	10,
	32,
	10,
	32,
	32,
	31,
	154,
	1541,
	1542,
	32,
	23,
	1541,
	26,
	341,
	341,
	524,
	524,
	341,
	38,
	524,
	1542,
	1541,
	1543,
	168,
	6,
	6,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	32,
	10,
	32,
	10,
	32,
	32,
	32,
	32,
	32,
	524,
	830,
	3,
	89,
	1544,
	14,
	1545,
	1546,
	1545,
	9,
	1547,
	10,
	112,
	1548,
	1548,
	1548,
	396,
	96,
	396,
	49,
	859,
	106,
	164,
	40,
	27,
	14,
	89,
	62,
	370,
	27,
	370,
	26,
	14,
	26,
	89,
	31,
	10,
	32,
	30,
	0,
	62,
	370,
	27,
	27,
	370,
	40,
	154,
	154,
	3,
	14,
	617,
	26,
	35,
	23,
	1438,
	1549,
	14,
	10,
	9,
	1550,
	1551,
	1551,
	1551,
	1552,
	1553,
	1553,
	1554,
	1554,
	1555,
	1556,
	1556,
	1556,
	1556,
	1556,
	1556,
	1556,
	1417,
	731,
	1557,
	1558,
	85,
	1559,
	1560,
	14,
	216,
	216,
	769,
	23,
	1401,
	23,
	23,
	14,
	26,
	14,
	26,
	10,
	32,
	9,
	9,
	10,
	1433,
	1433,
	1561,
	1562,
	10,
	9,
	1563,
	1480,
	1422,
	1422,
	14,
	3,
	349,
	1564,
	1565,
	1565,
	1565,
	1479,
	1020,
	1549,
	1400,
	1434,
	1566,
	10,
	9,
	1444,
	1453,
	23,
	1433,
	1567,
	1567,
	1567,
	1568,
	1569,
	731,
	731,
	1566,
	1566,
	1570,
	1570,
	1570,
	1570,
	1570,
	1570,
	1570,
	1570,
	1566,
	1566,
	1453,
	1568,
	1571,
	1568,
	1572,
	1572,
	14,
	3,
	1573,
	1574,
	1574,
	1575,
	1576,
	1577,
	1578,
	1438,
	1579,
	1580,
	1581,
	243,
	1582,
	1582,
	1583,
	1431,
	1453,
	1433,
	1584,
	1575,
	10,
	9,
	1585,
	14,
	3,
	349,
	1586,
	1586,
	349,
	349,
	1587,
	920,
	21,
	444,
	444,
	444,
	444,
	444,
	445,
	444,
	444,
	21,
	445,
	168,
	445,
	168,
	445,
	444,
	445,
	444,
	444,
	444,
	444,
	254,
	254,
	254,
	444,
	1588,
	198,
	444,
	1588,
	1588,
	1588,
	1588,
	1588,
	1589,
	1590,
	445,
	1588,
	445,
	3,
	1479,
	1020,
	1400,
	1591,
	1591,
	1592,
	23,
	1441,
	14,
	10,
	9,
	1443,
	1593,
	731,
	731,
	1593,
	1593,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1594,
	1595,
	1594,
	1596,
	1596,
	1597,
	1598,
	1599,
	1599,
	1599,
	1599,
	3,
	10,
	32,
	10,
	32,
	129,
	1600,
	1601,
	1601,
	9,
	1602,
	10,
	14,
	3,
	10,
	32,
	10,
	32,
	10,
	32,
	38,
	1603,
	9,
	1604,
	10,
	14,
	3,
	1479,
	1020,
	1438,
	1605,
	10,
	9,
	1606,
	1607,
	731,
	731,
	1608,
	1609,
	1609,
	1610,
	1610,
	1611,
	1611,
	1612,
	14,
	3,
	23,
	23,
	1613,
	1614,
	23,
	1615,
	1615,
	89,
	1613,
	1614,
	23,
	1615,
	1615,
	23,
	89,
	23,
	49,
	3,
	154,
	154,
	195,
	495,
	3,
	3,
	23,
	26,
	135,
	137,
	1,
	0,
	3,
	23,
	26,
	23,
	337,
	1400,
	23,
	129,
	445,
	168,
	168,
	14,
	23,
	-1,
	0,
	0,
	-1,
	1,
	1,
	1,
	1,
	-1,
	1,
	-1,
	154,
	4,
	25,
	89,
	731,
	32,
	89,
	31,
	23,
	23,
	26,
	26,
	23,
	0,
	0,
	94,
	94,
	94,
	-1,
	3,
	23,
	26,
	27,
	26,
	130,
	26,
	459,
	1269,
	23,
	23,
	23,
	26,
	10,
	23,
	89,
	31,
	89,
	23,
	3,
	14,
	14,
	28,
	124,
	-1,
	148,
	-1,
	-1,
	-1,
	-1,
	-1,
	28,
	-1,
	-1,
	-1,
	-1,
	27,
	27,
	-1,
	-1,
	118,
	130,
	23,
	23,
	23,
	25,
	1003,
	2,
	89,
	14,
	89,
	23,
	23,
	23,
	23,
	43,
	-1,
	28,
	124,
	28,
	28,
	148,
	28,
	-1,
	-1,
	28,
	-1,
	1616,
	28,
	-1,
	27,
	-1,
	28,
	148,
	-1,
	-1,
	-1,
	-1,
	28,
	148,
	-1,
	-1,
	-1,
	-1,
	803,
	28,
	124,
	0,
	130,
	130,
	130,
	28,
	28,
	28,
	-1,
	14,
	10,
	32,
	89,
	31,
	31,
	89,
	89,
	31,
	89,
	31,
	89,
	14,
	26,
	9,
	0,
	0,
	118,
	27,
	26,
	118,
	27,
	26,
	118,
	27,
	26,
	26,
	23,
	27,
	137,
	0,
	1617,
	172,
	14,
	6,
	1618,
	1619,
	10,
	32,
	94,
	154,
	1620,
	1621,
	1003,
	89,
	23,
	943,
	1622,
	26,
	9,
	28,
	105,
	28,
	28,
	26,
	26,
	26,
	23,
	89,
	31,
	154,
	154,
	114,
	1623,
	137,
	135,
	114,
	105,
	28,
	26,
	26,
	14,
	23,
	-1,
	-1,
	94,
	-1,
	-1,
	373,
	0,
	10,
	129,
	23,
	32,
	32,
	23,
	0,
	-1,
	154,
	153,
	49,
	23,
	154,
	4,
	464,
	0,
	3,
	23,
	26,
	111,
	23,
	26,
	111,
	14,
	14,
	3,
	137,
	137,
	186,
	10,
	10,
	9,
	114,
	135,
	23,
	114,
	15,
	14,
	26,
	1624,
	1625,
	0,
	1,
	206,
	-1,
	-1,
	-1,
	-1,
	-1,
	1469,
	154,
	625,
	154,
	0,
	154,
	10,
	32,
	1469,
	154,
	0,
	0,
	-1,
	-1,
	0,
	137,
	0,
	14,
	135,
	135,
	106,
	49,
	0,
	206,
	1624,
	1626,
	0,
	0,
	114,
	137,
	46,
	43,
	43,
	23,
	3,
	1627,
	993,
	32,
	130,
	27,
	27,
	14,
	23,
	89,
	3,
	3,
	242,
	197,
	23,
	23,
	731,
	337,
	89,
	337,
	23,
	23,
	23,
	23,
	112,
	23,
	23,
	4,
	106,
	4,
	106,
	106,
	4,
	4,
	49,
	49,
	49,
	49,
	106,
	4,
	4,
	4,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	106,
	106,
	114,
	46,
	106,
	106,
	4,
	106,
	4,
	106,
	106,
	4,
	4,
	49,
	49,
	49,
	49,
	106,
	4,
	4,
	4,
	49,
	49,
	49,
	49,
	106,
	46,
	106,
	106,
	53,
	168,
	21,
	1456,
	1456,
	1456,
	1456,
	1456,
	1456,
	1456,
	1456,
	1456,
	1628,
	106,
	1456,
	25,
	23,
	23,
	1629,
	1630,
	49,
	49,
	1631,
	14,
	26,
	859,
	89,
	31,
	10,
	32,
	89,
	89,
	1632,
	1633,
	349,
	165,
	118,
	23,
	154,
	154,
	1419,
	1441,
	1442,
	1441,
	1442,
	1441,
	1442,
	1441,
	1442,
	1441,
	1442,
	1433,
	1434,
	1441,
	1442,
	1441,
	1442,
	14,
	26,
	10,
	32,
	23,
	26,
	26,
	1634,
	1020,
	154,
	1419,
	1441,
	23,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	124,
	26,
	205,
	26,
	23,
	1433,
	1434,
	1433,
	1434,
	1433,
	1433,
	1433,
	1433,
	1635,
	1636,
	1635,
	1636,
	1433,
	1434,
	14,
	26,
	14,
	26,
	14,
	26,
	459,
	1637,
	1637,
	1638,
	1639,
	1638,
	1422,
	1422,
	1640,
	1422,
	10,
	23,
	23,
	32,
	10,
	206,
	28,
	1433,
	9,
	31,
	14,
	34,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	582,
	582,
	582,
	6,
	26,
	14,
	89,
	23,
	1417,
	1418,
	6,
	6,
	23,
	10,
	10,
	10,
	1419,
	1441,
	1641,
	1641,
	1641,
	1642,
	1643,
	1498,
	1419,
	1641,
	14,
	731,
	731,
	14,
	1441,
	89,
	10,
	10,
	1419,
	1441,
	14,
	14,
	14,
	10,
	37,
	37,
	1104,
	501,
	26,
	373,
	195,
	27,
	1642,
	1644,
	1643,
	1645,
	1646,
	1647,
	1648,
	1649,
	6,
	6,
	6,
	6,
	6,
	1650,
	1651,
	6,
	6,
	6,
	6,
	1652,
	1652,
	1474,
	1474,
	1474,
	1653,
	114,
	154,
	154,
	154,
	154,
	3,
	9,
	28,
	49,
	49,
	859,
	154,
	154,
	137,
	3,
	150,
	150,
	150,
	150,
	1654,
	32,
	32,
	32,
	23,
	4,
	594,
	377,
	625,
	1655,
	106,
	49,
	49,
	3,
	14,
	14,
	10,
	731,
	14,
	89,
	23,
	23,
	23,
	23,
	23,
	27,
	26,
	-1,
	114,
	90,
	26,
	26,
	27,
	26,
	26,
	23,
	90,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	14,
	10,
	14,
	89,
	28,
	2,
	23,
	23,
	27,
	26,
	26,
	27,
	23,
	14,
	23,
	23,
	23,
	23,
	105,
	105,
	28,
	103,
	23,
	23,
	26,
	27,
	14,
	14,
	2,
	124,
	23,
	105,
	26,
	23,
	26,
	26,
	105,
	105,
	0,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	14,
	23,
	1302,
	26,
	46,
	43,
	43,
	46,
	21,
	10,
	89,
	14,
	14,
	89,
	10,
	1656,
	10,
	9,
	1657,
	1658,
	106,
	106,
	1659,
	1660,
	1661,
	1661,
	1662,
	1657,
	154,
	154,
	154,
	154,
	154,
	154,
	164,
	1663,
	119,
	1664,
	1662,
	1665,
	1666,
	1667,
	3,
	17,
	396,
	1482,
	1482,
	292,
	32,
	32,
	124,
	23,
	105,
	26,
	23,
	4,
	89,
	4,
	23,
	14,
	1668,
	1668,
	26,
	26,
	26,
	26,
	1668,
	1669,
	1670,
	23,
	1671,
	164,
	164,
	23,
	23,
	9,
	23,
	9,
	23,
	26,
	1613,
	1672,
	1668,
	23,
	23,
	23,
	487,
	1615,
	23,
	23,
	9,
	23,
	9,
	23,
	9,
	26,
	14,
	9,
	10,
	23,
	49,
	49,
	4,
	4,
	4,
	106,
	17,
	3,
	1673,
	9,
	10,
	1674,
	1674,
	89,
	31,
	23,
	31,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	4,
	154,
	154,
	154,
	3,
	1675,
	1003,
	154,
	3,
	10,
	34,
	7,
	10,
	34,
	1676,
	9,
	10,
	502,
	292,
	10,
	32,
	9,
	1677,
	10,
	3,
	4,
	154,
	10,
	10,
	10,
	10,
	89,
	89,
	89,
	25,
	46,
	1678,
	46,
	1678,
	1678,
	1678,
	25,
	25,
	25,
	23,
	3,
	1679,
	464,
	124,
	1680,
	1681,
	1682,
	1683,
	1684,
	1684,
	1684,
	1684,
	1685,
	1685,
	1685,
	1686,
	1687,
	1688,
	1689,
	1690,
	3,
	1691,
	463,
	14,
	1692,
	1003,
	23,
	23,
	1684,
	1684,
	1684,
	1684,
	1685,
	1685,
	1685,
	1686,
	14,
	3,
	124,
	1693,
	1694,
	1695,
	-1,
	1696,
	1697,
	9,
	1698,
	10,
	1697,
	89,
	14,
	3,
	388,
	96,
	1699,
	1700,
	1701,
	3,
	1702,
	10,
	1703,
	9,
	1704,
	1703,
	3,
	1705,
	1706,
	6,
	6,
	6,
	6,
	6,
	32,
	1707,
	493,
	396,
	396,
	396,
	396,
	396,
	154,
	4,
	3,
	1427,
	3,
	124,
	1708,
	1709,
	26,
	3,
	23,
	1708,
	1689,
	1710,
	1689,
	1711,
	1689,
	1712,
	89,
	23,
	31,
	49,
	4,
	23,
	89,
	154,
	4,
	3,
	3,
	14,
	26,
	6,
	23,
	291,
	291,
	291,
	291,
	168,
	46,
	46,
	46,
	137,
	-1,
	-1,
	186,
	165,
	3,
	27,
	14,
	0,
	1,
	206,
};
static const Il2CppTokenRangePair s_rgctxIndices[71] = 
{
	{ 0x02000010, { 0, 8 } },
	{ 0x02000011, { 8, 4 } },
	{ 0x020000FE, { 99, 7 } },
	{ 0x020000FF, { 106, 7 } },
	{ 0x02000100, { 113, 9 } },
	{ 0x02000101, { 122, 11 } },
	{ 0x02000102, { 133, 3 } },
	{ 0x0200010B, { 136, 8 } },
	{ 0x0200010D, { 144, 4 } },
	{ 0x0200010F, { 148, 9 } },
	{ 0x02000111, { 157, 6 } },
	{ 0x06000025, { 12, 1 } },
	{ 0x06000026, { 13, 1 } },
	{ 0x06000027, { 14, 1 } },
	{ 0x060002F5, { 15, 2 } },
	{ 0x060002F6, { 17, 1 } },
	{ 0x060002FA, { 18, 1 } },
	{ 0x060002FB, { 19, 1 } },
	{ 0x06000312, { 20, 1 } },
	{ 0x06000383, { 21, 4 } },
	{ 0x06000384, { 25, 1 } },
	{ 0x06000385, { 26, 4 } },
	{ 0x06000386, { 30, 2 } },
	{ 0x06000547, { 32, 2 } },
	{ 0x0600054A, { 34, 2 } },
	{ 0x0600054F, { 36, 2 } },
	{ 0x06000551, { 38, 2 } },
	{ 0x06000564, { 40, 2 } },
	{ 0x0600057D, { 42, 1 } },
	{ 0x0600057F, { 43, 2 } },
	{ 0x06000580, { 45, 1 } },
	{ 0x06000581, { 46, 1 } },
	{ 0x06000582, { 47, 1 } },
	{ 0x06000583, { 48, 1 } },
	{ 0x06000585, { 49, 2 } },
	{ 0x06000586, { 51, 1 } },
	{ 0x06000587, { 52, 1 } },
	{ 0x06000588, { 53, 1 } },
	{ 0x0600058B, { 54, 1 } },
	{ 0x0600058C, { 55, 1 } },
	{ 0x0600059D, { 56, 1 } },
	{ 0x060005A4, { 57, 1 } },
	{ 0x060005A5, { 58, 2 } },
	{ 0x060005A7, { 60, 2 } },
	{ 0x060005AA, { 62, 2 } },
	{ 0x060005AC, { 64, 1 } },
	{ 0x060005AF, { 65, 2 } },
	{ 0x060005B0, { 67, 1 } },
	{ 0x060005B1, { 68, 1 } },
	{ 0x060005B2, { 69, 1 } },
	{ 0x060005B5, { 70, 1 } },
	{ 0x060005B6, { 71, 2 } },
	{ 0x060005B7, { 73, 1 } },
	{ 0x060005B8, { 74, 2 } },
	{ 0x060005C3, { 76, 2 } },
	{ 0x0600060D, { 78, 2 } },
	{ 0x0600060E, { 80, 4 } },
	{ 0x06000610, { 84, 1 } },
	{ 0x06000611, { 85, 1 } },
	{ 0x0600061B, { 86, 2 } },
	{ 0x06000640, { 88, 1 } },
	{ 0x06000641, { 89, 1 } },
	{ 0x06000642, { 90, 1 } },
	{ 0x06000643, { 91, 1 } },
	{ 0x06000644, { 92, 1 } },
	{ 0x06000651, { 93, 2 } },
	{ 0x06000652, { 95, 2 } },
	{ 0x060007B8, { 97, 2 } },
	{ 0x06000904, { 163, 1 } },
	{ 0x06000954, { 164, 3 } },
	{ 0x06000955, { 167, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[170] = 
{
	{ (Il2CppRGCTXDataType)3, 25234 },
	{ (Il2CppRGCTXDataType)3, 25235 },
	{ (Il2CppRGCTXDataType)3, 25236 },
	{ (Il2CppRGCTXDataType)2, 24703 },
	{ (Il2CppRGCTXDataType)3, 25237 },
	{ (Il2CppRGCTXDataType)3, 25238 },
	{ (Il2CppRGCTXDataType)2, 24705 },
	{ (Il2CppRGCTXDataType)3, 25239 },
	{ (Il2CppRGCTXDataType)3, 25240 },
	{ (Il2CppRGCTXDataType)3, 25241 },
	{ (Il2CppRGCTXDataType)3, 25242 },
	{ (Il2CppRGCTXDataType)2, 24712 },
	{ (Il2CppRGCTXDataType)2, 24751 },
	{ (Il2CppRGCTXDataType)2, 24752 },
	{ (Il2CppRGCTXDataType)2, 34719 },
	{ (Il2CppRGCTXDataType)2, 24984 },
	{ (Il2CppRGCTXDataType)2, 24984 },
	{ (Il2CppRGCTXDataType)3, 25243 },
	{ (Il2CppRGCTXDataType)3, 25244 },
	{ (Il2CppRGCTXDataType)3, 25245 },
	{ (Il2CppRGCTXDataType)3, 25246 },
	{ (Il2CppRGCTXDataType)3, 25247 },
	{ (Il2CppRGCTXDataType)3, 25248 },
	{ (Il2CppRGCTXDataType)3, 25249 },
	{ (Il2CppRGCTXDataType)3, 25250 },
	{ (Il2CppRGCTXDataType)2, 25014 },
	{ (Il2CppRGCTXDataType)3, 25251 },
	{ (Il2CppRGCTXDataType)3, 25252 },
	{ (Il2CppRGCTXDataType)3, 25253 },
	{ (Il2CppRGCTXDataType)3, 25254 },
	{ (Il2CppRGCTXDataType)3, 25255 },
	{ (Il2CppRGCTXDataType)3, 25256 },
	{ (Il2CppRGCTXDataType)2, 25107 },
	{ (Il2CppRGCTXDataType)2, 25108 },
	{ (Il2CppRGCTXDataType)1, 25109 },
	{ (Il2CppRGCTXDataType)2, 25109 },
	{ (Il2CppRGCTXDataType)1, 25111 },
	{ (Il2CppRGCTXDataType)3, 25257 },
	{ (Il2CppRGCTXDataType)1, 25112 },
	{ (Il2CppRGCTXDataType)2, 25112 },
	{ (Il2CppRGCTXDataType)1, 25117 },
	{ (Il2CppRGCTXDataType)2, 25117 },
	{ (Il2CppRGCTXDataType)1, 25145 },
	{ (Il2CppRGCTXDataType)1, 25146 },
	{ (Il2CppRGCTXDataType)2, 25146 },
	{ (Il2CppRGCTXDataType)3, 25258 },
	{ (Il2CppRGCTXDataType)3, 25259 },
	{ (Il2CppRGCTXDataType)3, 25260 },
	{ (Il2CppRGCTXDataType)3, 25261 },
	{ (Il2CppRGCTXDataType)1, 25155 },
	{ (Il2CppRGCTXDataType)2, 25155 },
	{ (Il2CppRGCTXDataType)3, 25262 },
	{ (Il2CppRGCTXDataType)3, 25263 },
	{ (Il2CppRGCTXDataType)3, 25264 },
	{ (Il2CppRGCTXDataType)1, 25163 },
	{ (Il2CppRGCTXDataType)3, 25265 },
	{ (Il2CppRGCTXDataType)1, 25178 },
	{ (Il2CppRGCTXDataType)3, 25266 },
	{ (Il2CppRGCTXDataType)1, 25180 },
	{ (Il2CppRGCTXDataType)2, 25180 },
	{ (Il2CppRGCTXDataType)1, 25181 },
	{ (Il2CppRGCTXDataType)2, 25181 },
	{ (Il2CppRGCTXDataType)1, 25184 },
	{ (Il2CppRGCTXDataType)2, 25183 },
	{ (Il2CppRGCTXDataType)1, 25186 },
	{ (Il2CppRGCTXDataType)1, 25188 },
	{ (Il2CppRGCTXDataType)2, 25187 },
	{ (Il2CppRGCTXDataType)1, 25190 },
	{ (Il2CppRGCTXDataType)3, 25267 },
	{ (Il2CppRGCTXDataType)3, 25268 },
	{ (Il2CppRGCTXDataType)1, 25196 },
	{ (Il2CppRGCTXDataType)1, 25198 },
	{ (Il2CppRGCTXDataType)2, 25197 },
	{ (Il2CppRGCTXDataType)3, 25269 },
	{ (Il2CppRGCTXDataType)1, 25202 },
	{ (Il2CppRGCTXDataType)2, 25202 },
	{ (Il2CppRGCTXDataType)1, 25204 },
	{ (Il2CppRGCTXDataType)2, 25204 },
	{ (Il2CppRGCTXDataType)3, 25270 },
	{ (Il2CppRGCTXDataType)3, 25271 },
	{ (Il2CppRGCTXDataType)3, 25272 },
	{ (Il2CppRGCTXDataType)3, 25273 },
	{ (Il2CppRGCTXDataType)3, 25274 },
	{ (Il2CppRGCTXDataType)3, 25275 },
	{ (Il2CppRGCTXDataType)3, 25276 },
	{ (Il2CppRGCTXDataType)2, 25221 },
	{ (Il2CppRGCTXDataType)1, 25232 },
	{ (Il2CppRGCTXDataType)2, 25232 },
	{ (Il2CppRGCTXDataType)2, 25253 },
	{ (Il2CppRGCTXDataType)2, 25254 },
	{ (Il2CppRGCTXDataType)2, 25255 },
	{ (Il2CppRGCTXDataType)3, 25277 },
	{ (Il2CppRGCTXDataType)2, 25257 },
	{ (Il2CppRGCTXDataType)1, 25259 },
	{ (Il2CppRGCTXDataType)3, 25278 },
	{ (Il2CppRGCTXDataType)1, 25260 },
	{ (Il2CppRGCTXDataType)2, 25260 },
	{ (Il2CppRGCTXDataType)2, 34720 },
	{ (Il2CppRGCTXDataType)1, 34720 },
	{ (Il2CppRGCTXDataType)2, 25388 },
	{ (Il2CppRGCTXDataType)3, 25279 },
	{ (Il2CppRGCTXDataType)1, 25388 },
	{ (Il2CppRGCTXDataType)3, 25280 },
	{ (Il2CppRGCTXDataType)3, 25281 },
	{ (Il2CppRGCTXDataType)2, 25389 },
	{ (Il2CppRGCTXDataType)3, 25282 },
	{ (Il2CppRGCTXDataType)1, 34721 },
	{ (Il2CppRGCTXDataType)2, 34721 },
	{ (Il2CppRGCTXDataType)3, 25283 },
	{ (Il2CppRGCTXDataType)3, 25284 },
	{ (Il2CppRGCTXDataType)2, 25394 },
	{ (Il2CppRGCTXDataType)2, 25395 },
	{ (Il2CppRGCTXDataType)3, 25285 },
	{ (Il2CppRGCTXDataType)1, 34722 },
	{ (Il2CppRGCTXDataType)2, 34722 },
	{ (Il2CppRGCTXDataType)3, 25286 },
	{ (Il2CppRGCTXDataType)3, 25287 },
	{ (Il2CppRGCTXDataType)3, 25288 },
	{ (Il2CppRGCTXDataType)2, 25398 },
	{ (Il2CppRGCTXDataType)2, 25399 },
	{ (Il2CppRGCTXDataType)2, 25400 },
	{ (Il2CppRGCTXDataType)3, 25289 },
	{ (Il2CppRGCTXDataType)1, 34723 },
	{ (Il2CppRGCTXDataType)2, 34723 },
	{ (Il2CppRGCTXDataType)3, 25290 },
	{ (Il2CppRGCTXDataType)3, 25291 },
	{ (Il2CppRGCTXDataType)3, 25292 },
	{ (Il2CppRGCTXDataType)3, 25293 },
	{ (Il2CppRGCTXDataType)2, 25405 },
	{ (Il2CppRGCTXDataType)2, 25406 },
	{ (Il2CppRGCTXDataType)2, 25407 },
	{ (Il2CppRGCTXDataType)2, 25408 },
	{ (Il2CppRGCTXDataType)3, 25294 },
	{ (Il2CppRGCTXDataType)3, 25295 },
	{ (Il2CppRGCTXDataType)2, 25410 },
	{ (Il2CppRGCTXDataType)3, 25296 },
	{ (Il2CppRGCTXDataType)3, 25297 },
	{ (Il2CppRGCTXDataType)2, 34724 },
	{ (Il2CppRGCTXDataType)1, 25437 },
	{ (Il2CppRGCTXDataType)2, 34725 },
	{ (Il2CppRGCTXDataType)3, 25298 },
	{ (Il2CppRGCTXDataType)3, 25299 },
	{ (Il2CppRGCTXDataType)3, 25300 },
	{ (Il2CppRGCTXDataType)2, 25437 },
	{ (Il2CppRGCTXDataType)1, 34726 },
	{ (Il2CppRGCTXDataType)1, 34727 },
	{ (Il2CppRGCTXDataType)2, 34728 },
	{ (Il2CppRGCTXDataType)3, 25301 },
	{ (Il2CppRGCTXDataType)1, 25451 },
	{ (Il2CppRGCTXDataType)1, 25452 },
	{ (Il2CppRGCTXDataType)1, 25453 },
	{ (Il2CppRGCTXDataType)2, 34729 },
	{ (Il2CppRGCTXDataType)3, 25302 },
	{ (Il2CppRGCTXDataType)3, 25303 },
	{ (Il2CppRGCTXDataType)2, 25451 },
	{ (Il2CppRGCTXDataType)2, 25452 },
	{ (Il2CppRGCTXDataType)2, 25453 },
	{ (Il2CppRGCTXDataType)1, 34730 },
	{ (Il2CppRGCTXDataType)1, 34731 },
	{ (Il2CppRGCTXDataType)1, 34732 },
	{ (Il2CppRGCTXDataType)1, 34733 },
	{ (Il2CppRGCTXDataType)2, 34734 },
	{ (Il2CppRGCTXDataType)3, 25304 },
	{ (Il2CppRGCTXDataType)1, 34735 },
	{ (Il2CppRGCTXDataType)3, 25305 },
	{ (Il2CppRGCTXDataType)2, 34736 },
	{ (Il2CppRGCTXDataType)3, 25306 },
	{ (Il2CppRGCTXDataType)1, 25979 },
	{ (Il2CppRGCTXDataType)2, 25979 },
	{ (Il2CppRGCTXDataType)2, 25980 },
};
extern const Il2CppCodeGenModule g_UnityEngine_CoreModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_CoreModuleCodeGenModule = 
{
	"UnityEngine.CoreModule.dll",
	2397,
	s_methodPointers,
	262,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	71,
	s_rgctxIndices,
	170,
	s_rgctxValues,
	NULL,
};
