﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean SRDebugUtil::get_IsFixedUpdate()
extern void SRDebugUtil_get_IsFixedUpdate_m42755BCA911DBE2714F87ED49334754D200862AC (void);
// 0x00000002 System.Void SRDebugUtil::set_IsFixedUpdate(System.Boolean)
extern void SRDebugUtil_set_IsFixedUpdate_mC453280DAE4EB7BF173A36534C506F94BD0DE2FE (void);
// 0x00000003 System.Void SRDebugUtil::AssertNotNull(System.Object,System.String,UnityEngine.MonoBehaviour)
extern void SRDebugUtil_AssertNotNull_m329BBCC2FEFF6101969ACA0F3B16B702F724BA63 (void);
// 0x00000004 System.Void SRDebugUtil::Assert(System.Boolean,System.String,UnityEngine.MonoBehaviour)
extern void SRDebugUtil_Assert_m42E8BD8E5148D83EB85B80C1C9B70CFC1AB0778C (void);
// 0x00000005 System.Void SRDebugUtil::EditorAssertNotNull(System.Object,System.String,UnityEngine.MonoBehaviour)
extern void SRDebugUtil_EditorAssertNotNull_m161957627DBD9C8E76147550E5EEB7E388B96EDA (void);
// 0x00000006 System.Void SRDebugUtil::EditorAssert(System.Boolean,System.String,UnityEngine.MonoBehaviour)
extern void SRDebugUtil_EditorAssert_m0A5D0A512A5206E445F2737C3AD858CB7A9BEC05 (void);
// 0x00000007 System.Void SRFileUtil::DeleteDirectory(System.String)
extern void SRFileUtil_DeleteDirectory_mBF8E03E276A631EAD13EC9B945AD367F0B0B4730 (void);
// 0x00000008 System.String SRFileUtil::GetBytesReadable(System.Int64)
extern void SRFileUtil_GetBytesReadable_mB508665E2CE66374A33AC37FB3B8075485CA0B72 (void);
// 0x00000009 T SRInstantiate::Instantiate(T)
// 0x0000000A UnityEngine.GameObject SRInstantiate::Instantiate(UnityEngine.GameObject)
extern void SRInstantiate_Instantiate_m3BBB52B37E5073463CEA7B55AF8E64DB3FA36538 (void);
// 0x0000000B T SRInstantiate::Instantiate(T,UnityEngine.Vector3,UnityEngine.Quaternion)
// 0x0000000C System.Single SRMath::Ease(System.Single,System.Single,System.Single,SRMath/EaseType)
extern void SRMath_Ease_m1338555BB09E55446DD824A3BF7D0FB2EC18D9C8 (void);
// 0x0000000D System.Single SRMath::SpringLerp(System.Single,System.Single)
extern void SRMath_SpringLerp_mCB275F97AB0583B9C50D3C17DC353D63D63DF04E (void);
// 0x0000000E System.Single SRMath::SpringLerp(System.Single,System.Single,System.Single,System.Single)
extern void SRMath_SpringLerp_mCDBE2D83E7F49386C23F119CB44A9D3B3AA3B412 (void);
// 0x0000000F UnityEngine.Vector3 SRMath::SpringLerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void SRMath_SpringLerp_mE2D21DDC8F8AFB9D2C4DD90B455DC22D5F704E29 (void);
// 0x00000010 UnityEngine.Quaternion SRMath::SpringLerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single,System.Single)
extern void SRMath_SpringLerp_mCEBC68267270F4166AEE0268418E7E2C691CBBDD (void);
// 0x00000011 System.Single SRMath::SmoothClamp(System.Single,System.Single,System.Single,System.Single,SRMath/EaseType)
extern void SRMath_SmoothClamp_m4114F3CE0309771D08BE6250C83C9D858E963E10 (void);
// 0x00000012 System.Single SRMath::LerpUnclamped(System.Single,System.Single,System.Single)
extern void SRMath_LerpUnclamped_m67C8E2CFF7E84FEFBF9C2727E9F9BA92D52D9F2D (void);
// 0x00000013 UnityEngine.Vector3 SRMath::LerpUnclamped(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void SRMath_LerpUnclamped_m025EF3006DFBCC7560C40BBEA34AF61934382158 (void);
// 0x00000014 System.Single SRMath::FacingNormalized(UnityEngine.Vector3,UnityEngine.Vector3)
extern void SRMath_FacingNormalized_m7B5A447FDA16766CCD2B30E05B12905776299F4E (void);
// 0x00000015 System.Single SRMath::WrapAngle(System.Single)
extern void SRMath_WrapAngle_m2ED56AC77B0F3B0FD55EAB55F9C2D691D5679334 (void);
// 0x00000016 System.Single SRMath::NearestAngle(System.Single,System.Single,System.Single)
extern void SRMath_NearestAngle_mAB5091AB2357C0EEDC33FCD1910E4D2374067666 (void);
// 0x00000017 System.Int32 SRMath::Wrap(System.Int32,System.Int32)
extern void SRMath_Wrap_mB11B3F203EE75E04436580C25726041B857A5B04 (void);
// 0x00000018 System.Single SRMath::Wrap(System.Single,System.Single)
extern void SRMath_Wrap_m561BE4663F1D15F3F9894020D8C988003950106E (void);
// 0x00000019 System.Single SRMath::Average(System.Single,System.Single)
extern void SRMath_Average_mF97C450E40989F2EC495028BFE19230E5C7B7B69 (void);
// 0x0000001A System.Single SRMath::Angle(UnityEngine.Vector2)
extern void SRMath_Angle_m95F90BE88BDD475144E3B5A5011599DDE0BBB535 (void);
// 0x0000001B System.Object SRF.Json::Deserialize(System.String)
extern void Json_Deserialize_mF9B9ED81B45857F3A167AD769774BC7B9207622A (void);
// 0x0000001C System.String SRF.Json::Serialize(System.Object)
extern void Json_Serialize_mF26FF87D4BDDA37196A13F540BC0E4830ABA3F69 (void);
// 0x0000001D System.Void SRF.SRList`1::.ctor()
// 0x0000001E System.Void SRF.SRList`1::.ctor(System.Int32)
// 0x0000001F System.Void SRF.SRList`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000020 T[] SRF.SRList`1::get_Buffer()
// 0x00000021 System.Void SRF.SRList`1::set_Buffer(T[])
// 0x00000022 System.Collections.Generic.EqualityComparer`1<T> SRF.SRList`1::get_EqualityComparer()
// 0x00000023 System.Int32 SRF.SRList`1::get_Count()
// 0x00000024 System.Void SRF.SRList`1::set_Count(System.Int32)
// 0x00000025 System.Collections.Generic.IEnumerator`1<T> SRF.SRList`1::GetEnumerator()
// 0x00000026 System.Collections.IEnumerator SRF.SRList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000027 System.Void SRF.SRList`1::Add(T)
// 0x00000028 System.Void SRF.SRList`1::Clear()
// 0x00000029 System.Boolean SRF.SRList`1::Contains(T)
// 0x0000002A System.Void SRF.SRList`1::CopyTo(T[],System.Int32)
// 0x0000002B System.Boolean SRF.SRList`1::Remove(T)
// 0x0000002C System.Boolean SRF.SRList`1::get_IsReadOnly()
// 0x0000002D System.Int32 SRF.SRList`1::IndexOf(T)
// 0x0000002E System.Void SRF.SRList`1::Insert(System.Int32,T)
// 0x0000002F System.Void SRF.SRList`1::RemoveAt(System.Int32)
// 0x00000030 T SRF.SRList`1::get_Item(System.Int32)
// 0x00000031 System.Void SRF.SRList`1::set_Item(System.Int32,T)
// 0x00000032 System.Void SRF.SRList`1::OnBeforeSerialize()
// 0x00000033 System.Void SRF.SRList`1::OnAfterDeserialize()
// 0x00000034 System.Void SRF.SRList`1::AddRange(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000035 System.Void SRF.SRList`1::Clear(System.Boolean)
// 0x00000036 System.Void SRF.SRList`1::Clean()
// 0x00000037 System.Collections.ObjectModel.ReadOnlyCollection`1<T> SRF.SRList`1::AsReadOnly()
// 0x00000038 System.Void SRF.SRList`1::Expand()
// 0x00000039 System.Void SRF.SRList`1::Trim()
// 0x0000003A System.Void SRF.SRList`1::Sort(System.Comparison`1<T>)
// 0x0000003B UnityEngine.Transform SRF.SRMonoBehaviour::get_CachedTransform()
extern void SRMonoBehaviour_get_CachedTransform_mDCBB38954DFF098EF4845B2133259F4F33456D8C (void);
// 0x0000003C UnityEngine.Collider SRF.SRMonoBehaviour::get_CachedCollider()
extern void SRMonoBehaviour_get_CachedCollider_mDDBA14C83F31408644522D43258C3A33F03A68E2 (void);
// 0x0000003D UnityEngine.Collider2D SRF.SRMonoBehaviour::get_CachedCollider2D()
extern void SRMonoBehaviour_get_CachedCollider2D_mE8C5615C3CABBF1E949C1D38458821D85A94460A (void);
// 0x0000003E UnityEngine.Rigidbody SRF.SRMonoBehaviour::get_CachedRigidBody()
extern void SRMonoBehaviour_get_CachedRigidBody_m5BF4A0AD9F6B9BE03F3E473C990635CE0D9D6F72 (void);
// 0x0000003F UnityEngine.Rigidbody2D SRF.SRMonoBehaviour::get_CachedRigidBody2D()
extern void SRMonoBehaviour_get_CachedRigidBody2D_m6BF89FB8D5AE8D60E5D7FB450680EC9DC3E54522 (void);
// 0x00000040 UnityEngine.GameObject SRF.SRMonoBehaviour::get_CachedGameObject()
extern void SRMonoBehaviour_get_CachedGameObject_m0594155BD3FC6EA20375B035FCF0EB09A64BD4F7 (void);
// 0x00000041 UnityEngine.Transform SRF.SRMonoBehaviour::get_transform()
extern void SRMonoBehaviour_get_transform_mF9E38D15E9788B9EF9339975F1CE4B5DED54506D (void);
// 0x00000042 System.Void SRF.SRMonoBehaviour::AssertNotNull(System.Object,System.String)
extern void SRMonoBehaviour_AssertNotNull_m3F8044FB947B855B44F10D3704E60CAACA567AA6 (void);
// 0x00000043 System.Void SRF.SRMonoBehaviour::Assert(System.Boolean,System.String)
extern void SRMonoBehaviour_Assert_m92BC18BC18894BBF4F09AD263830810560CDDD1F (void);
// 0x00000044 System.Void SRF.SRMonoBehaviour::EditorAssertNotNull(System.Object,System.String)
extern void SRMonoBehaviour_EditorAssertNotNull_m224A5047E5B85229D59D8A865DF31847A140E385 (void);
// 0x00000045 System.Void SRF.SRMonoBehaviour::EditorAssert(System.Boolean,System.String)
extern void SRMonoBehaviour_EditorAssert_mFD778E352407BC82B36643710BEC200290B55350 (void);
// 0x00000046 System.Void SRF.SRMonoBehaviour::.ctor()
extern void SRMonoBehaviour__ctor_mEAFA5432C2D1CBF3993C07918C880E09A9465597 (void);
// 0x00000047 System.Void SRF.RequiredFieldAttribute::.ctor(System.Boolean)
extern void RequiredFieldAttribute__ctor_mEFEF94681F30D2DFA378AAFACBC7B60D35DD23DB (void);
// 0x00000048 System.Void SRF.RequiredFieldAttribute::.ctor()
extern void RequiredFieldAttribute__ctor_mDD252DADEB178C431BDA1B6FFC20186828613C1B (void);
// 0x00000049 System.Boolean SRF.RequiredFieldAttribute::get_AutoSearch()
extern void RequiredFieldAttribute_get_AutoSearch_mD5E30F2D0C09005A3ED9CB48018F8421916C7870 (void);
// 0x0000004A System.Void SRF.RequiredFieldAttribute::set_AutoSearch(System.Boolean)
extern void RequiredFieldAttribute_set_AutoSearch_m9892C491C77369070B5A0E6FD6D25E89E361C477 (void);
// 0x0000004B System.Boolean SRF.RequiredFieldAttribute::get_AutoCreate()
extern void RequiredFieldAttribute_get_AutoCreate_m2734590C6F34AEE1E7EB03F1672D77389CB84993 (void);
// 0x0000004C System.Void SRF.RequiredFieldAttribute::set_AutoCreate(System.Boolean)
extern void RequiredFieldAttribute_set_AutoCreate_m376DA1AF3A1F9A6F0A7B4ECDF372E750564B0272 (void);
// 0x0000004D System.Boolean SRF.RequiredFieldAttribute::get_EditorOnly()
extern void RequiredFieldAttribute_get_EditorOnly_mD7EA4B968624C44D131BD0435F461B93E9DF6EAC (void);
// 0x0000004E System.Void SRF.RequiredFieldAttribute::set_EditorOnly(System.Boolean)
extern void RequiredFieldAttribute_set_EditorOnly_mC5030F3277A69067278B6A26DD0E1CF8AFAE9A96 (void);
// 0x0000004F System.Void SRF.ImportAttribute::.ctor()
extern void ImportAttribute__ctor_m43886C5690126F6D565AE7E510118415E085883A (void);
// 0x00000050 System.Void SRF.ImportAttribute::.ctor(System.Type)
extern void ImportAttribute__ctor_m7353ECACE1CA37CCD13A8164F7DF884D62B30FF8 (void);
// 0x00000051 System.Void SRF.SRMonoBehaviourEx::CheckFields(SRF.SRMonoBehaviourEx,System.Boolean)
extern void SRMonoBehaviourEx_CheckFields_m3F0788A3019FF8E7186B744C02BB2E9DAAA6A7B5 (void);
// 0x00000052 System.Void SRF.SRMonoBehaviourEx::PopulateObject(System.Collections.Generic.IList`1<SRF.SRMonoBehaviourEx/FieldInfo>,SRF.SRMonoBehaviourEx,System.Boolean)
extern void SRMonoBehaviourEx_PopulateObject_m78704A22CC28A09B1B071F06E226AA8CDD4A691F (void);
// 0x00000053 System.Collections.Generic.List`1<SRF.SRMonoBehaviourEx/FieldInfo> SRF.SRMonoBehaviourEx::ScanType(System.Type)
extern void SRMonoBehaviourEx_ScanType_mD0B633932E7626D671FFD46DD04B9B2C4FB4F921 (void);
// 0x00000054 System.Void SRF.SRMonoBehaviourEx::Awake()
extern void SRMonoBehaviourEx_Awake_m5273E60992532123296D9A6F07D17D0D206553D3 (void);
// 0x00000055 System.Void SRF.SRMonoBehaviourEx::Start()
extern void SRMonoBehaviourEx_Start_m901BB03C7EACA4FD65889373303C692BA61E47F4 (void);
// 0x00000056 System.Void SRF.SRMonoBehaviourEx::Update()
extern void SRMonoBehaviourEx_Update_m69BC1E57223CB33DD8B02B4BC42354D5B278F993 (void);
// 0x00000057 System.Void SRF.SRMonoBehaviourEx::FixedUpdate()
extern void SRMonoBehaviourEx_FixedUpdate_m38925ED79F9822EE0077542D88C1E80E76C04889 (void);
// 0x00000058 System.Void SRF.SRMonoBehaviourEx::OnEnable()
extern void SRMonoBehaviourEx_OnEnable_mD2FD21E4D4F234CC4B73E78E04F90C6156FD7828 (void);
// 0x00000059 System.Void SRF.SRMonoBehaviourEx::OnDisable()
extern void SRMonoBehaviourEx_OnDisable_m5320A449D0217E10AAF292CD17C7E74BC500A49C (void);
// 0x0000005A System.Void SRF.SRMonoBehaviourEx::OnDestroy()
extern void SRMonoBehaviourEx_OnDestroy_m06E89F391729656B8E62606E076A6961A8080497 (void);
// 0x0000005B System.Void SRF.SRMonoBehaviourEx::.ctor()
extern void SRMonoBehaviourEx__ctor_m058A362C4878D202ECD26D160D172151285404CB (void);
// 0x0000005C System.Collections.IEnumerator SRF.Coroutines::WaitForSecondsRealTime(System.Single)
extern void Coroutines_WaitForSecondsRealTime_m82707BBCD7907C57D1C9BF0BDE465A8A04492535 (void);
// 0x0000005D System.Single SRF.SRFFloatExtensions::Sqr(System.Single)
extern void SRFFloatExtensions_Sqr_m16519937EDF450DDC1FB9A0B17F7CB78312523A0 (void);
// 0x0000005E System.Single SRF.SRFFloatExtensions::SqrRt(System.Single)
extern void SRFFloatExtensions_SqrRt_m00109E83F8C9BD8CF57A26C88224FA8208B1D91E (void);
// 0x0000005F System.Boolean SRF.SRFFloatExtensions::ApproxZero(System.Single)
extern void SRFFloatExtensions_ApproxZero_m81DE68FD1BB9A32D9748B1FE45F6EA328A6C1F11 (void);
// 0x00000060 System.Boolean SRF.SRFFloatExtensions::Approx(System.Single,System.Single)
extern void SRFFloatExtensions_Approx_m2195AAAC7B9D1A8E3396FA4B5BB5398FFF1AF75E (void);
// 0x00000061 T SRF.SRFGameObjectExtensions::GetIComponent(UnityEngine.GameObject)
// 0x00000062 T SRF.SRFGameObjectExtensions::GetComponentOrAdd(UnityEngine.GameObject)
// 0x00000063 System.Void SRF.SRFGameObjectExtensions::RemoveComponentIfExists(UnityEngine.GameObject)
// 0x00000064 System.Void SRF.SRFGameObjectExtensions::RemoveComponentsIfExists(UnityEngine.GameObject)
// 0x00000065 System.Boolean SRF.SRFGameObjectExtensions::EnableComponentIfExists(UnityEngine.GameObject,System.Boolean)
// 0x00000066 System.Void SRF.SRFGameObjectExtensions::SetLayerRecursive(UnityEngine.GameObject,System.Int32)
extern void SRFGameObjectExtensions_SetLayerRecursive_mA57EDF7E4CDA58291783299992F7AA035A0F7CD7 (void);
// 0x00000067 System.Void SRF.SRFGameObjectExtensions::SetLayerInternal(UnityEngine.Transform,System.Int32)
extern void SRFGameObjectExtensions_SetLayerInternal_m405ECBE4CA3B14728D8734C7D3DDE71CD0B9654C (void);
// 0x00000068 T SRF.SRFIListExtensions::Random(System.Collections.Generic.IList`1<T>)
// 0x00000069 T SRF.SRFIListExtensions::RandomOrDefault(System.Collections.Generic.IList`1<T>)
// 0x0000006A T SRF.SRFIListExtensions::PopLast(System.Collections.Generic.IList`1<T>)
// 0x0000006B System.String SRF.SRFStringExtensions::Fmt(System.String,System.Object[])
extern void SRFStringExtensions_Fmt_mBD139EDDF35D50ABE63736D6159C6A77241D38E8 (void);
// 0x0000006C System.Collections.Generic.IEnumerable`1<UnityEngine.Transform> SRF.SRFTransformExtensions::GetChildren(UnityEngine.Transform)
extern void SRFTransformExtensions_GetChildren_mC079689055D65C548C31EB270D7704E2A2124423 (void);
// 0x0000006D System.Void SRF.SRFTransformExtensions::ResetLocal(UnityEngine.Transform)
extern void SRFTransformExtensions_ResetLocal_m375B98E0E1AB93D04A0582DD5ED543355CBB0058 (void);
// 0x0000006E UnityEngine.GameObject SRF.SRFTransformExtensions::CreateChild(UnityEngine.Transform,System.String)
extern void SRFTransformExtensions_CreateChild_m52BE36CBFABB57A740DF967CECFC6CA3AFBC1555 (void);
// 0x0000006F System.Void SRF.SRFTransformExtensions::SetParentMaintainLocals(UnityEngine.Transform,UnityEngine.Transform)
extern void SRFTransformExtensions_SetParentMaintainLocals_m59AB2ECCBFA14E4718A0653750449662A0725B68 (void);
// 0x00000070 System.Void SRF.SRFTransformExtensions::SetLocals(UnityEngine.Transform,UnityEngine.Transform)
extern void SRFTransformExtensions_SetLocals_mB21D9E0F2C4AE782E22DDEA885AD62AEDF327AC1 (void);
// 0x00000071 System.Void SRF.SRFTransformExtensions::Match(UnityEngine.Transform,UnityEngine.Transform)
extern void SRFTransformExtensions_Match_mBC932E5A16A9FDB1A09784B14F76511562BB5184 (void);
// 0x00000072 System.Void SRF.SRFTransformExtensions::DestroyChildren(UnityEngine.Transform)
extern void SRFTransformExtensions_DestroyChildren_mFCF395C485FCD121D35ECD09BA18FD94991CB675 (void);
// 0x00000073 UnityEngine.Transform SRF.Hierarchy::get_Item(System.String)
extern void Hierarchy_get_Item_mF4C1AF7D5EAD8F9CD38D04C52447A8CF0D7FF4F3 (void);
// 0x00000074 UnityEngine.Transform SRF.Hierarchy::Get(System.String)
extern void Hierarchy_Get_mBD19B83FF8DD50D089C338674FFFA1DE749D75FE (void);
// 0x00000075 System.Void SRF.Hierarchy::.ctor()
extern void Hierarchy__ctor_m04CC3C2D423F5B296939F215554813736AF5A521 (void);
// 0x00000076 System.Void SRF.Hierarchy::.cctor()
extern void Hierarchy__cctor_m86F55340D235E37A5E89BB0BF048F4E86DDBB23D (void);
// 0x00000077 System.Single SRF.UI.ContentFitText::get_minWidth()
extern void ContentFitText_get_minWidth_m71936C6E94E43785E624307AF193526FBF6E5D2D (void);
// 0x00000078 System.Single SRF.UI.ContentFitText::get_preferredWidth()
extern void ContentFitText_get_preferredWidth_mA62AE791CA1F696E328B6EFAA90B66D7AA63F05C (void);
// 0x00000079 System.Single SRF.UI.ContentFitText::get_flexibleWidth()
extern void ContentFitText_get_flexibleWidth_mC5249A9090756BEE03C8EBE213D9C01C1FF03844 (void);
// 0x0000007A System.Single SRF.UI.ContentFitText::get_minHeight()
extern void ContentFitText_get_minHeight_m018B01E2F631577811A965EF9482A98D1795C0A8 (void);
// 0x0000007B System.Single SRF.UI.ContentFitText::get_preferredHeight()
extern void ContentFitText_get_preferredHeight_mD90A43C1A0120AD4DC3C3ED738CDE98805AB14A7 (void);
// 0x0000007C System.Single SRF.UI.ContentFitText::get_flexibleHeight()
extern void ContentFitText_get_flexibleHeight_mB8C98E184739788141420BB45D001E8EF79D97D4 (void);
// 0x0000007D System.Int32 SRF.UI.ContentFitText::get_layoutPriority()
extern void ContentFitText_get_layoutPriority_m9518CF7D496423EA636B1720C0FAB00F1F770D52 (void);
// 0x0000007E System.Void SRF.UI.ContentFitText::CalculateLayoutInputHorizontal()
extern void ContentFitText_CalculateLayoutInputHorizontal_m6100386FC0346F6DA9BBD32C595DF053C6FF2AB4 (void);
// 0x0000007F System.Void SRF.UI.ContentFitText::CalculateLayoutInputVertical()
extern void ContentFitText_CalculateLayoutInputVertical_mF90D578F0DD375392E7F3E67AB32E069E699987C (void);
// 0x00000080 System.Void SRF.UI.ContentFitText::OnEnable()
extern void ContentFitText_OnEnable_m1BF429F7BF48D084F7E2AE0DE69B3EEEBBA2E1AF (void);
// 0x00000081 System.Void SRF.UI.ContentFitText::CopySourceOnLayoutDirty(SRF.UI.SRText)
extern void ContentFitText_CopySourceOnLayoutDirty_mEA56D2DA374147146E5A6A3E3F1F03D223EEDDBB (void);
// 0x00000082 System.Void SRF.UI.ContentFitText::OnTransformParentChanged()
extern void ContentFitText_OnTransformParentChanged_m0089193360102AF2F32D973A836571D6D966E94E (void);
// 0x00000083 System.Void SRF.UI.ContentFitText::OnDisable()
extern void ContentFitText_OnDisable_mD86C492ECFC40E04655422F3C02F76DE6C6C8133 (void);
// 0x00000084 System.Void SRF.UI.ContentFitText::OnDidApplyAnimationProperties()
extern void ContentFitText_OnDidApplyAnimationProperties_m4AF02CF3CC02F9FE43790668929A73833186EEC7 (void);
// 0x00000085 System.Void SRF.UI.ContentFitText::OnBeforeTransformParentChanged()
extern void ContentFitText_OnBeforeTransformParentChanged_m0A61678250DDDA442AB95F832C948E5B4618406C (void);
// 0x00000086 System.Void SRF.UI.ContentFitText::SetDirty()
extern void ContentFitText_SetDirty_m7270E687658270637FBD5851C069A2C0774230A1 (void);
// 0x00000087 System.Void SRF.UI.ContentFitText::.ctor()
extern void ContentFitText__ctor_mD57F8E1A44C23592A57D31B956B640054A2D7BA0 (void);
// 0x00000088 System.Single SRF.UI.CopyLayoutElement::get_preferredWidth()
extern void CopyLayoutElement_get_preferredWidth_m263205876685F3B6C062B1E33ED3E6B6439AA132 (void);
// 0x00000089 System.Single SRF.UI.CopyLayoutElement::get_preferredHeight()
extern void CopyLayoutElement_get_preferredHeight_mA419E1F15A789CCCA768610D179D9E365650989E (void);
// 0x0000008A System.Single SRF.UI.CopyLayoutElement::get_minWidth()
extern void CopyLayoutElement_get_minWidth_m1F1C747DDEE6F9CCFB1D8ED883066F72C3708165 (void);
// 0x0000008B System.Single SRF.UI.CopyLayoutElement::get_minHeight()
extern void CopyLayoutElement_get_minHeight_m4C8E1B3139E7478945A0E62C1B5A16C72D35D162 (void);
// 0x0000008C System.Int32 SRF.UI.CopyLayoutElement::get_layoutPriority()
extern void CopyLayoutElement_get_layoutPriority_m489455381639B61CE186724D7817749E4C4677D7 (void);
// 0x0000008D System.Single SRF.UI.CopyLayoutElement::get_flexibleHeight()
extern void CopyLayoutElement_get_flexibleHeight_mB8D7FF7355F5B91DC26C3075F9FE1F2101C41E9D (void);
// 0x0000008E System.Single SRF.UI.CopyLayoutElement::get_flexibleWidth()
extern void CopyLayoutElement_get_flexibleWidth_mBBE1E15477BDDF65551C4DF40EDCE78DB371713A (void);
// 0x0000008F System.Void SRF.UI.CopyLayoutElement::CalculateLayoutInputHorizontal()
extern void CopyLayoutElement_CalculateLayoutInputHorizontal_mF3D8F9164260AFC61DE2293E29E6A7239FEE73B9 (void);
// 0x00000090 System.Void SRF.UI.CopyLayoutElement::CalculateLayoutInputVertical()
extern void CopyLayoutElement_CalculateLayoutInputVertical_mA9D0658A17BEBB19CBFC620D5CA26958F88FE4AB (void);
// 0x00000091 System.Void SRF.UI.CopyLayoutElement::.ctor()
extern void CopyLayoutElement__ctor_m9A5C7826307E561CB78050F3B0236577FEF58416 (void);
// 0x00000092 System.Single SRF.UI.CopyPreferredSize::get_preferredWidth()
extern void CopyPreferredSize_get_preferredWidth_m91D78D4E44F7A480A2C6683877961DD6EAE5A53D (void);
// 0x00000093 System.Single SRF.UI.CopyPreferredSize::get_preferredHeight()
extern void CopyPreferredSize_get_preferredHeight_m8FE0BBD1BF3BF738CAC6AC03D149CF5E976B2A60 (void);
// 0x00000094 System.Int32 SRF.UI.CopyPreferredSize::get_layoutPriority()
extern void CopyPreferredSize_get_layoutPriority_m669E0692CC80E4D060770C47A4EE73CE906D56D9 (void);
// 0x00000095 System.Void SRF.UI.CopyPreferredSize::.ctor()
extern void CopyPreferredSize__ctor_mF559A65533EA4E2FA4C3A7B98AEA616135DAA976 (void);
// 0x00000096 System.Single SRF.UI.CopySizeIntoLayoutElement::get_preferredWidth()
extern void CopySizeIntoLayoutElement_get_preferredWidth_m12C59DCB2BB74A9C6B699A8B4FCE4CB819CA088E (void);
// 0x00000097 System.Single SRF.UI.CopySizeIntoLayoutElement::get_preferredHeight()
extern void CopySizeIntoLayoutElement_get_preferredHeight_mD2A988E320B7AE7C8818D9034193BB05216AEACE (void);
// 0x00000098 System.Single SRF.UI.CopySizeIntoLayoutElement::get_minWidth()
extern void CopySizeIntoLayoutElement_get_minWidth_mB79D55AA0DFFFDB98017CA4A77B9F9C5F5D72ACF (void);
// 0x00000099 System.Single SRF.UI.CopySizeIntoLayoutElement::get_minHeight()
extern void CopySizeIntoLayoutElement_get_minHeight_mD5EF56C401E4B9B911C32D1ACC2C4A992454E3D5 (void);
// 0x0000009A System.Int32 SRF.UI.CopySizeIntoLayoutElement::get_layoutPriority()
extern void CopySizeIntoLayoutElement_get_layoutPriority_mEE5D925BD5F8E1D5CE93524D87D86C2C22E4683E (void);
// 0x0000009B System.Void SRF.UI.CopySizeIntoLayoutElement::.ctor()
extern void CopySizeIntoLayoutElement__ctor_m787FBFBEEA6B98AB8D65E98339276609929A0F30 (void);
// 0x0000009C System.Single SRF.UI.DragHandle::get_Mult()
extern void DragHandle_get_Mult_mAC931EF10093BB78B102EBD8875958AC99D93D8B (void);
// 0x0000009D System.Void SRF.UI.DragHandle::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragHandle_OnBeginDrag_mB39A8B0EABF7CF54773EF645B859E26A810948A9 (void);
// 0x0000009E System.Void SRF.UI.DragHandle::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragHandle_OnDrag_m98E4DC073EF7398194835CA1A559D67BCA149262 (void);
// 0x0000009F System.Void SRF.UI.DragHandle::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragHandle_OnEndDrag_mE048565DFEDF563C2B327CC99170EB37B1FE98F2 (void);
// 0x000000A0 System.Void SRF.UI.DragHandle::Start()
extern void DragHandle_Start_m739D759F88E7DD8CD5D05004BA2D30B774CE8191 (void);
// 0x000000A1 System.Boolean SRF.UI.DragHandle::Verify()
extern void DragHandle_Verify_m6CB87B159D5EA6ED888DC40063D1775CD0BA3BAA (void);
// 0x000000A2 System.Single SRF.UI.DragHandle::GetCurrentValue()
extern void DragHandle_GetCurrentValue_m44AF9CA190F68AEF111841FF4E60D39501FB83E4 (void);
// 0x000000A3 System.Void SRF.UI.DragHandle::SetCurrentValue(System.Single)
extern void DragHandle_SetCurrentValue_mED2AEE53B10086E7355504F7D62BAC53A16030F1 (void);
// 0x000000A4 System.Void SRF.UI.DragHandle::CommitCurrentValue()
extern void DragHandle_CommitCurrentValue_m45AD34AF26F4A8011E3774CD803CDB1F95D8660F (void);
// 0x000000A5 System.Single SRF.UI.DragHandle::GetMinSize()
extern void DragHandle_GetMinSize_mBB4B669389F6BC04F0E68E6901EE7F4626C9F394 (void);
// 0x000000A6 System.Single SRF.UI.DragHandle::GetMaxSize()
extern void DragHandle_GetMaxSize_m99AF6C5FECB02D9E2EF69F02746D622CDAAC3C47 (void);
// 0x000000A7 System.Void SRF.UI.DragHandle::.ctor()
extern void DragHandle__ctor_mCEEBE6985FEB660692AEBDCBBF52728E2491720C (void);
// 0x000000A8 System.Void SRF.UI.FlashGraphic::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void FlashGraphic_OnPointerDown_m7EAB6B0AA90EB06647EB7EF0FE3744B117D52ABF (void);
// 0x000000A9 System.Void SRF.UI.FlashGraphic::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void FlashGraphic_OnPointerUp_mF62779598E8EDB551288B6553E5078D89B39C793 (void);
// 0x000000AA System.Void SRF.UI.FlashGraphic::OnEnable()
extern void FlashGraphic_OnEnable_mBFAB09DB08849CFE029C8F7AFDC9BCF45FD58959 (void);
// 0x000000AB System.Void SRF.UI.FlashGraphic::Update()
extern void FlashGraphic_Update_mA7A1A3C31A322F314CA9E4BE3389592B1A33ECE6 (void);
// 0x000000AC System.Void SRF.UI.FlashGraphic::Flash()
extern void FlashGraphic_Flash_m83F756F7A0651A729640922FBC3111500AB8CEED (void);
// 0x000000AD System.Void SRF.UI.FlashGraphic::.ctor()
extern void FlashGraphic__ctor_mEDCA56D21DA6AD7CAC68CDCE1D85D3B08529B28B (void);
// 0x000000AE UnityEngine.UI.Graphic SRF.UI.InheritColour::get_Graphic()
extern void InheritColour_get_Graphic_m8CD7DA3BE11AEC7CF728AF527E91A6DD98119B45 (void);
// 0x000000AF System.Void SRF.UI.InheritColour::Refresh()
extern void InheritColour_Refresh_m9DE63A98E00CD4990847CAF1356674BF995EB489 (void);
// 0x000000B0 System.Void SRF.UI.InheritColour::Update()
extern void InheritColour_Update_m6BF1F4A2DE395C74D6519E310BF0AD26C022B814 (void);
// 0x000000B1 System.Void SRF.UI.InheritColour::Start()
extern void InheritColour_Start_m55610901CD145A94DCCD28C0C4FE426B243C2BF0 (void);
// 0x000000B2 System.Void SRF.UI.InheritColour::.ctor()
extern void InheritColour__ctor_m0A17724289BFC595F542880A363A61A0D44C295F (void);
// 0x000000B3 UnityEngine.UI.Button/ButtonClickedEvent SRF.UI.LongPressButton::get_onLongPress()
extern void LongPressButton_get_onLongPress_mA47E1F93A07B0839A444B27B108CA65A48D53DDE (void);
// 0x000000B4 System.Void SRF.UI.LongPressButton::set_onLongPress(UnityEngine.UI.Button/ButtonClickedEvent)
extern void LongPressButton_set_onLongPress_m343B99D9E23A23771EC56E9B0925A8C3A4AF6B76 (void);
// 0x000000B5 System.Void SRF.UI.LongPressButton::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void LongPressButton_OnPointerExit_m27571BB6C4350A73B4D0341B732E8A926AFBFB51 (void);
// 0x000000B6 System.Void SRF.UI.LongPressButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void LongPressButton_OnPointerDown_mCB471784E4DC7F573E5128787FBD92495A247B0E (void);
// 0x000000B7 System.Void SRF.UI.LongPressButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void LongPressButton_OnPointerUp_m4E9AA2E4886CB6E95C79DFB99687FFE0938A4BE7 (void);
// 0x000000B8 System.Void SRF.UI.LongPressButton::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void LongPressButton_OnPointerClick_mF01EFDD5CDC8B41CCF49D73CE94B792F3670886A (void);
// 0x000000B9 System.Void SRF.UI.LongPressButton::Update()
extern void LongPressButton_Update_m060AFC4AD2CD0CB967F90984C08865BBD6BA3174 (void);
// 0x000000BA System.Void SRF.UI.LongPressButton::.ctor()
extern void LongPressButton__ctor_mB351535AC7C0321E5BDEF7132AFFC4E4B48D837D (void);
// 0x000000BB UnityEngine.RectTransform SRF.UI.ResponsiveBase::get_RectTransform()
extern void ResponsiveBase_get_RectTransform_mBDF09658DACB1675D5C7BAC7505CB50D3A8EEDB8 (void);
// 0x000000BC System.Void SRF.UI.ResponsiveBase::OnEnable()
extern void ResponsiveBase_OnEnable_m892FE98A40F7E7C025069CB4AEDD51384929EC57 (void);
// 0x000000BD System.Void SRF.UI.ResponsiveBase::OnRectTransformDimensionsChange()
extern void ResponsiveBase_OnRectTransformDimensionsChange_mFF1EC9563DFE5547730B9D2BA6347EFDDF1F35EB (void);
// 0x000000BE System.Void SRF.UI.ResponsiveBase::Update()
extern void ResponsiveBase_Update_mC11285514E2B67E04C0E4EAFD292289FC4341660 (void);
// 0x000000BF System.Void SRF.UI.ResponsiveBase::Refresh()
// 0x000000C0 System.Void SRF.UI.ResponsiveBase::DoRefresh()
extern void ResponsiveBase_DoRefresh_m9A86017130D4365CFA545A86633FBD3E6F9DD62A (void);
// 0x000000C1 System.Void SRF.UI.ResponsiveBase::.ctor()
extern void ResponsiveBase__ctor_m61DD4219B28F47D093E0B958D494F0BE9F6B0B9B (void);
// 0x000000C2 System.Void SRF.UI.ResponsiveEnable::Refresh()
extern void ResponsiveEnable_Refresh_m651080C839A622B28E7526218CCE092B78474AAF (void);
// 0x000000C3 System.Void SRF.UI.ResponsiveEnable::.ctor()
extern void ResponsiveEnable__ctor_m4CAD9C4C6F974A31544A1623CB6E3E7442D2D13C (void);
// 0x000000C4 System.Void SRF.UI.ResponsiveResize::Refresh()
extern void ResponsiveResize_Refresh_m1FF75986CADFF8817BD732292BFEB5EE6157572C (void);
// 0x000000C5 System.Void SRF.UI.ResponsiveResize::.ctor()
extern void ResponsiveResize__ctor_m10B8F24C96CC8F884A895A8607970FE9B5A6C2CF (void);
// 0x000000C6 System.Void SRF.UI.SRNumberButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void SRNumberButton_OnPointerDown_mAB78297888ABA87C47247FAB95C7CD0178530140 (void);
// 0x000000C7 System.Void SRF.UI.SRNumberButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void SRNumberButton_OnPointerUp_m577DA8ED71A63AE7AA464A14E30A7673A4E6F112 (void);
// 0x000000C8 System.Void SRF.UI.SRNumberButton::Update()
extern void SRNumberButton_Update_m583613A75316C77168F99C6C910F2902428979A1 (void);
// 0x000000C9 System.Void SRF.UI.SRNumberButton::Apply()
extern void SRNumberButton_Apply_mD6F7BE598BF9BF8902A2FB091FBE53E658D04F18 (void);
// 0x000000CA System.Void SRF.UI.SRNumberButton::.ctor()
extern void SRNumberButton__ctor_m3B50EBC2C9A881A3D6595622D5426DBA37EC490B (void);
// 0x000000CB System.Void SRF.UI.SRNumberSpinner::Awake()
extern void SRNumberSpinner_Awake_m54C3F778DEE61A0308B48C1DA9820471C84478F5 (void);
// 0x000000CC System.Void SRF.UI.SRNumberSpinner::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void SRNumberSpinner_OnPointerClick_mAD0D03BFC231C27720BDBDF910908C65C6328F70 (void);
// 0x000000CD System.Void SRF.UI.SRNumberSpinner::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void SRNumberSpinner_OnPointerDown_m53C36926C264D162A8E5826E80FE33860C05C110 (void);
// 0x000000CE System.Void SRF.UI.SRNumberSpinner::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void SRNumberSpinner_OnPointerUp_mEAB601AB50B3395E23E3937E08ACEEBBDE889D78 (void);
// 0x000000CF System.Void SRF.UI.SRNumberSpinner::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void SRNumberSpinner_OnBeginDrag_m046E71FAF362510E305801D3BEA6EE8A83DD9601 (void);
// 0x000000D0 System.Void SRF.UI.SRNumberSpinner::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void SRNumberSpinner_OnDrag_mDD9B658D8194885D12BA0C8C78BF5B2F697BFF04 (void);
// 0x000000D1 System.Void SRF.UI.SRNumberSpinner::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void SRNumberSpinner_OnEndDrag_m96FD8A786295C8BEC59B3885696326B52922E8F7 (void);
// 0x000000D2 System.Void SRF.UI.SRNumberSpinner::.ctor()
extern void SRNumberSpinner__ctor_m8962720FB0C3A9B54CE2FBC45B1595C9ECCE8FF9 (void);
// 0x000000D3 System.Int32 SRF.UI.SRRetinaScaler::get_ThresholdDpi()
extern void SRRetinaScaler_get_ThresholdDpi_mE3B4BBBCBDE51F864BF8EA81D6A41170645B56A9 (void);
// 0x000000D4 System.Single SRF.UI.SRRetinaScaler::get_RetinaScale()
extern void SRRetinaScaler_get_RetinaScale_m3E9496E06AF39202AE0537A2D1E3E3DCBEDD8D3F (void);
// 0x000000D5 System.Void SRF.UI.SRRetinaScaler::Start()
extern void SRRetinaScaler_Start_m2883FE885A87E647356DC90621EB487D2E098D49 (void);
// 0x000000D6 System.Void SRF.UI.SRRetinaScaler::.ctor()
extern void SRRetinaScaler__ctor_mFD0F50B26F6974AEFBD669BB354663EA72AC59B2 (void);
// 0x000000D7 SRF.UI.SRSpinner/SpinEvent SRF.UI.SRSpinner::get_OnSpinIncrement()
extern void SRSpinner_get_OnSpinIncrement_m083B1303A4D11EE15C8C7058FE709D6AF3C5DB3B (void);
// 0x000000D8 System.Void SRF.UI.SRSpinner::set_OnSpinIncrement(SRF.UI.SRSpinner/SpinEvent)
extern void SRSpinner_set_OnSpinIncrement_m8089E76E1151A553BC8241C21518394B64070B7C (void);
// 0x000000D9 SRF.UI.SRSpinner/SpinEvent SRF.UI.SRSpinner::get_OnSpinDecrement()
extern void SRSpinner_get_OnSpinDecrement_m3F078912B0FEF248BB9B810EDBA914DC4D86703F (void);
// 0x000000DA System.Void SRF.UI.SRSpinner::set_OnSpinDecrement(SRF.UI.SRSpinner/SpinEvent)
extern void SRSpinner_set_OnSpinDecrement_m77EB41316324D2F59132E1DCCB458D3775D44F2D (void);
// 0x000000DB System.Void SRF.UI.SRSpinner::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void SRSpinner_OnBeginDrag_m269E08FA5BAAC658BE1AF735F5A61225C2298F7B (void);
// 0x000000DC System.Void SRF.UI.SRSpinner::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void SRSpinner_OnDrag_m6E5357E3FEBD6D44174C4DE27EE74D43D5BAC56E (void);
// 0x000000DD System.Void SRF.UI.SRSpinner::OnIncrement(System.Int32)
extern void SRSpinner_OnIncrement_m87455DA472A63C68120A7F6C025C922718B7BED6 (void);
// 0x000000DE System.Void SRF.UI.SRSpinner::OnDecrement(System.Int32)
extern void SRSpinner_OnDecrement_m2F2A4922C5587B93B96539098739119870C3DCEF (void);
// 0x000000DF System.Void SRF.UI.SRSpinner::.ctor()
extern void SRSpinner__ctor_mBC391963E897B7B969622941DFA78D4FCDFB6BEF (void);
// 0x000000E0 System.Void SRF.UI.SRText::add_LayoutDirty(System.Action`1<SRF.UI.SRText>)
extern void SRText_add_LayoutDirty_m37FE42668AFC0D0C91EB8E6DF008DF25233F2097 (void);
// 0x000000E1 System.Void SRF.UI.SRText::remove_LayoutDirty(System.Action`1<SRF.UI.SRText>)
extern void SRText_remove_LayoutDirty_m0A9628EF15DB07FCB206CF52AAE61327E2EF4F45 (void);
// 0x000000E2 System.Void SRF.UI.SRText::SetLayoutDirty()
extern void SRText_SetLayoutDirty_m792CD1E229058BB52587C9F1616F487DFABC6360 (void);
// 0x000000E3 System.Void SRF.UI.SRText::.ctor()
extern void SRText__ctor_m75B252322CCD4A9A2162539ABD255A314A4D2374 (void);
// 0x000000E4 System.Void SRF.UI.ScrollToBottomBehaviour::Start()
extern void ScrollToBottomBehaviour_Start_mEDE535860139CA53B703291D6F99B6B19C57D635 (void);
// 0x000000E5 System.Void SRF.UI.ScrollToBottomBehaviour::OnEnable()
extern void ScrollToBottomBehaviour_OnEnable_m9D3E2292D0A2B713AC1401AE745A122ABC129CCD (void);
// 0x000000E6 System.Void SRF.UI.ScrollToBottomBehaviour::Trigger()
extern void ScrollToBottomBehaviour_Trigger_m315273782FFC1F80745D7D1D779DABAC265CEC42 (void);
// 0x000000E7 System.Void SRF.UI.ScrollToBottomBehaviour::OnScrollRectValueChanged(UnityEngine.Vector2)
extern void ScrollToBottomBehaviour_OnScrollRectValueChanged_mD28927F134D1923C61169A8EB7C093B0EB433207 (void);
// 0x000000E8 System.Void SRF.UI.ScrollToBottomBehaviour::Refresh()
extern void ScrollToBottomBehaviour_Refresh_mE39E15BB38D78FCBB51CB6586CA5ABDB681093C8 (void);
// 0x000000E9 System.Void SRF.UI.ScrollToBottomBehaviour::SetVisible(System.Boolean)
extern void ScrollToBottomBehaviour_SetVisible_m0F8F79A5FA82F3065AC0A0B7A75DA4C89CBA51E4 (void);
// 0x000000EA System.Void SRF.UI.ScrollToBottomBehaviour::.ctor()
extern void ScrollToBottomBehaviour__ctor_mD8D4ACD04D58672B7D48438F06026D3078D86766 (void);
// 0x000000EB System.String SRF.UI.StyleComponent::get_StyleKey()
extern void StyleComponent_get_StyleKey_mCA4532490E29E7BA10D394A8082F29EA3FCBFC25 (void);
// 0x000000EC System.Void SRF.UI.StyleComponent::set_StyleKey(System.String)
extern void StyleComponent_set_StyleKey_m11DF5F90251449B06D44A8754FF045EF734AA884 (void);
// 0x000000ED System.Void SRF.UI.StyleComponent::Start()
extern void StyleComponent_Start_mB18CA205D9E2251403BA27F1BB9EF8DF5D154A31 (void);
// 0x000000EE System.Void SRF.UI.StyleComponent::OnEnable()
extern void StyleComponent_OnEnable_m14BC36B942B7560849468A67B83B4E22418EBB98 (void);
// 0x000000EF System.Void SRF.UI.StyleComponent::Refresh(System.Boolean)
extern void StyleComponent_Refresh_m4FFBB3F87D15F65BEAB421811C83C86998E2D457 (void);
// 0x000000F0 SRF.UI.StyleRoot SRF.UI.StyleComponent::GetStyleRoot()
extern void StyleComponent_GetStyleRoot_m02A00E2794D6AE281455CE45607979B8BBFD7552 (void);
// 0x000000F1 System.Void SRF.UI.StyleComponent::ApplyStyle()
extern void StyleComponent_ApplyStyle_mE4C0B1B01A061342AB33F78CDC19699253CD8B93 (void);
// 0x000000F2 System.Void SRF.UI.StyleComponent::SRStyleDirty()
extern void StyleComponent_SRStyleDirty_m04BC090762B131BD72A63BA248EA34F1EE995B72 (void);
// 0x000000F3 System.Void SRF.UI.StyleComponent::.ctor()
extern void StyleComponent__ctor_mFFD116954C4BC710F03193025BBA66FECF36555D (void);
// 0x000000F4 SRF.UI.Style SRF.UI.StyleRoot::GetStyle(System.String)
extern void StyleRoot_GetStyle_mC2F8C7CACF6E7DAFFE8F1EFDDD9D89E6D7843FFE (void);
// 0x000000F5 System.Void SRF.UI.StyleRoot::OnEnable()
extern void StyleRoot_OnEnable_mFA160E5C644CBC978EAB1089C920D77CA8C39D47 (void);
// 0x000000F6 System.Void SRF.UI.StyleRoot::OnDisable()
extern void StyleRoot_OnDisable_mFBE8F207325057F5BE16A20C10717A42F46A8720 (void);
// 0x000000F7 System.Void SRF.UI.StyleRoot::Update()
extern void StyleRoot_Update_mDE4FDC671BF7F736DB9828A809AEF9095848F78E (void);
// 0x000000F8 System.Void SRF.UI.StyleRoot::OnStyleSheetChanged()
extern void StyleRoot_OnStyleSheetChanged_m5B0A32103A73C6EEC7AF044B502475E6ECC861AC (void);
// 0x000000F9 System.Void SRF.UI.StyleRoot::SetDirty()
extern void StyleRoot_SetDirty_mDBC0B7C8CB68DFEABA18A808465C278319E03829 (void);
// 0x000000FA System.Void SRF.UI.StyleRoot::.ctor()
extern void StyleRoot__ctor_m01B1166B6319A015C7A181BA2870769E50D68F1C (void);
// 0x000000FB SRF.UI.Style SRF.UI.Style::Copy()
extern void Style_Copy_m1902B50C78314E7F0EA71CA95158C0FCE9AE01CA (void);
// 0x000000FC System.Void SRF.UI.Style::CopyFrom(SRF.UI.Style)
extern void Style_CopyFrom_mFAE8A6A7F7E48FB594FFC4CB382E7351304B65F1 (void);
// 0x000000FD System.Void SRF.UI.Style::.ctor()
extern void Style__ctor_mDDC0440ED6111979BBBFDF4F97FB92BC45F52079 (void);
// 0x000000FE SRF.UI.Style SRF.UI.StyleSheet::GetStyle(System.String,System.Boolean)
extern void StyleSheet_GetStyle_m4362ACC33900C5A6F395B27A528354BD9F90D5C0 (void);
// 0x000000FF System.Void SRF.UI.StyleSheet::.ctor()
extern void StyleSheet__ctor_m26B0A40188B46E49A8B5DF6C7BE0B0E2F3272722 (void);
// 0x00000100 System.Void SRF.UI.Unselectable::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern void Unselectable_OnSelect_mBA2E6E355E4E8F0BA5ED9AD2B64FBC353DC2FF85 (void);
// 0x00000101 System.Void SRF.UI.Unselectable::Update()
extern void Unselectable_Update_mED2C5C55A8FA8BFA43C16515E8625EA79247B2C6 (void);
// 0x00000102 System.Void SRF.UI.Unselectable::.ctor()
extern void Unselectable__ctor_mD34960B9CCC272EA85663AF3C31B4788380EEFD4 (void);
// 0x00000103 System.Boolean SRF.UI.Layout.FlowLayoutGroup::get_IsCenterAlign()
extern void FlowLayoutGroup_get_IsCenterAlign_mA0C36A0155C2CC3FFC827FF3C11A39A510B148A2 (void);
// 0x00000104 System.Boolean SRF.UI.Layout.FlowLayoutGroup::get_IsRightAlign()
extern void FlowLayoutGroup_get_IsRightAlign_m05E1E3FE1910AB26F8F66DE8C9DE574D625F642B (void);
// 0x00000105 System.Boolean SRF.UI.Layout.FlowLayoutGroup::get_IsMiddleAlign()
extern void FlowLayoutGroup_get_IsMiddleAlign_mA900FCB97310AD76C5CDE1AF8E964136A7C81566 (void);
// 0x00000106 System.Boolean SRF.UI.Layout.FlowLayoutGroup::get_IsLowerAlign()
extern void FlowLayoutGroup_get_IsLowerAlign_mF204BEE4602B2218B859B1F17D1199E9D333EDEA (void);
// 0x00000107 System.Void SRF.UI.Layout.FlowLayoutGroup::CalculateLayoutInputHorizontal()
extern void FlowLayoutGroup_CalculateLayoutInputHorizontal_m8C4495CB4F2FBD3E87DBAEDCC6AB623CAB3B00A8 (void);
// 0x00000108 System.Void SRF.UI.Layout.FlowLayoutGroup::SetLayoutHorizontal()
extern void FlowLayoutGroup_SetLayoutHorizontal_m58ED1D3E46EB60C850775674714B15520E386846 (void);
// 0x00000109 System.Void SRF.UI.Layout.FlowLayoutGroup::SetLayoutVertical()
extern void FlowLayoutGroup_SetLayoutVertical_m51B24D701CA14F1941ACF44F042065E80ED6BCE3 (void);
// 0x0000010A System.Void SRF.UI.Layout.FlowLayoutGroup::CalculateLayoutInputVertical()
extern void FlowLayoutGroup_CalculateLayoutInputVertical_m8CD425FD992C3136131460B28010C95251F5AD1D (void);
// 0x0000010B System.Single SRF.UI.Layout.FlowLayoutGroup::SetLayout(System.Single,System.Int32,System.Boolean)
extern void FlowLayoutGroup_SetLayout_m15E282026AE7835DC6B71CDBED29838C562709AD (void);
// 0x0000010C System.Single SRF.UI.Layout.FlowLayoutGroup::CalculateRowVerticalOffset(System.Single,System.Single,System.Single)
extern void FlowLayoutGroup_CalculateRowVerticalOffset_m92085127DA08A48CC02D1ECB87873E6A29D12D04 (void);
// 0x0000010D System.Void SRF.UI.Layout.FlowLayoutGroup::LayoutRow(System.Collections.Generic.IList`1<UnityEngine.RectTransform>,System.Single,System.Single,System.Single,System.Single,System.Single,System.Int32)
extern void FlowLayoutGroup_LayoutRow_m2C3DB27EACA66C9BE802744F2D2459D55F5BD7AB (void);
// 0x0000010E System.Single SRF.UI.Layout.FlowLayoutGroup::GetGreatestMinimumChildWidth()
extern void FlowLayoutGroup_GetGreatestMinimumChildWidth_m88B6EAC36B153AC8ECDC1CA8DCF94D451A354C10 (void);
// 0x0000010F System.Void SRF.UI.Layout.FlowLayoutGroup::.ctor()
extern void FlowLayoutGroup__ctor_m7A1B3B7F62E98D0C7FF665BF5A94377AAF8B9A21 (void);
// 0x00000110 System.Void SRF.UI.Layout.IVirtualView::SetDataContext(System.Object)
// 0x00000111 SRF.UI.Layout.VirtualVerticalLayoutGroup/SelectedItemChangedEvent SRF.UI.Layout.VirtualVerticalLayoutGroup::get_SelectedItemChanged()
extern void VirtualVerticalLayoutGroup_get_SelectedItemChanged_m2E45E50CE3D9398BD66EE485BD79EAA52C4D6AC6 (void);
// 0x00000112 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::set_SelectedItemChanged(SRF.UI.Layout.VirtualVerticalLayoutGroup/SelectedItemChangedEvent)
extern void VirtualVerticalLayoutGroup_set_SelectedItemChanged_m0A4EEE82BFAF9FE2E19E4FAB00EB179A37240FF1 (void);
// 0x00000113 System.Object SRF.UI.Layout.VirtualVerticalLayoutGroup::get_SelectedItem()
extern void VirtualVerticalLayoutGroup_get_SelectedItem_m794B1E5B69AE5BB026DF3C8770A22F8F99691621 (void);
// 0x00000114 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::set_SelectedItem(System.Object)
extern void VirtualVerticalLayoutGroup_set_SelectedItem_m432B1A19861F25B1E55A6657BDD3BD41BB780711 (void);
// 0x00000115 System.Single SRF.UI.Layout.VirtualVerticalLayoutGroup::get_minHeight()
extern void VirtualVerticalLayoutGroup_get_minHeight_m5B88EB0A2DCF0A190641B034EF297F578E872677 (void);
// 0x00000116 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void VirtualVerticalLayoutGroup_OnPointerClick_m8F74DBB00FA21A2D0E75659E4DC932AFD1FCD85A (void);
// 0x00000117 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::Awake()
extern void VirtualVerticalLayoutGroup_Awake_m65BD3A21B6C2974F288334D6469E8A2200A83305 (void);
// 0x00000118 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::OnScrollRectValueChanged(UnityEngine.Vector2)
extern void VirtualVerticalLayoutGroup_OnScrollRectValueChanged_mBF269E601E2E0EF67ABEB45B5400AFB48242B407 (void);
// 0x00000119 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::Start()
extern void VirtualVerticalLayoutGroup_Start_mD392FAFF504CD9A8E3D3DE2EBF345E56B7FEEF1C (void);
// 0x0000011A System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::OnEnable()
extern void VirtualVerticalLayoutGroup_OnEnable_mCF30426326C6BB173DD32056BF04B9B40894AA4D (void);
// 0x0000011B System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::Update()
extern void VirtualVerticalLayoutGroup_Update_m49956BC1F090430B525956FC2215C293ABB51729 (void);
// 0x0000011C System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::InvalidateItem(System.Int32)
extern void VirtualVerticalLayoutGroup_InvalidateItem_mDB8F4C0DC5D33D60882F6404F0780E9F8AEB7579 (void);
// 0x0000011D System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::RefreshIndexCache()
extern void VirtualVerticalLayoutGroup_RefreshIndexCache_m6242B2C44842B88083D790A251E30E777712CD42 (void);
// 0x0000011E System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::ScrollUpdate()
extern void VirtualVerticalLayoutGroup_ScrollUpdate_m870017F811BC8F4077F480F14A446AED0E112CF1 (void);
// 0x0000011F System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::CalculateLayoutInputVertical()
extern void VirtualVerticalLayoutGroup_CalculateLayoutInputVertical_m0F52F0F3EE2D53FE78C0F2F86CB2817EAF29DF53 (void);
// 0x00000120 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::SetLayoutHorizontal()
extern void VirtualVerticalLayoutGroup_SetLayoutHorizontal_mEE56655F57746B3B90D3FC66B5065C6B4269185F (void);
// 0x00000121 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::SetLayoutVertical()
extern void VirtualVerticalLayoutGroup_SetLayoutVertical_m431FF4084C2E52931713FEF67A637506CFF38A8F (void);
// 0x00000122 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::SetDirty()
extern void VirtualVerticalLayoutGroup_SetDirty_mF96F92F4553B8EC8BB20DE72102E4E522F13224F (void);
// 0x00000123 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::AddItem(System.Object)
extern void VirtualVerticalLayoutGroup_AddItem_m9738F06F9D0C2C6408E58096C3D9756A46434FD4 (void);
// 0x00000124 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::RemoveItem(System.Object)
extern void VirtualVerticalLayoutGroup_RemoveItem_m8EF6F223F65F06CD2F82374FE5A1DDE1008C9C30 (void);
// 0x00000125 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::ClearItems()
extern void VirtualVerticalLayoutGroup_ClearItems_m00DAE6B8FE6D528668027CFB7040B33DACCB563B (void);
// 0x00000126 UnityEngine.UI.ScrollRect SRF.UI.Layout.VirtualVerticalLayoutGroup::get_ScrollRect()
extern void VirtualVerticalLayoutGroup_get_ScrollRect_m86FDB5AEB6F811303C55DB47595ADD42DE965D82 (void);
// 0x00000127 System.Boolean SRF.UI.Layout.VirtualVerticalLayoutGroup::get_AlignBottom()
extern void VirtualVerticalLayoutGroup_get_AlignBottom_m8D0BAC5001A936F9D08BE9806A5C1420C30BA565 (void);
// 0x00000128 System.Boolean SRF.UI.Layout.VirtualVerticalLayoutGroup::get_AlignTop()
extern void VirtualVerticalLayoutGroup_get_AlignTop_m519F36A0155BE1725ED4381754E8ADDAB70CD32B (void);
// 0x00000129 System.Single SRF.UI.Layout.VirtualVerticalLayoutGroup::get_ItemHeight()
extern void VirtualVerticalLayoutGroup_get_ItemHeight_mE7F4A8C578FA0E3A57A44C8DD443F4D9883F1ED6 (void);
// 0x0000012A SRF.UI.Layout.VirtualVerticalLayoutGroup/Row SRF.UI.Layout.VirtualVerticalLayoutGroup::GetRow(System.Int32)
extern void VirtualVerticalLayoutGroup_GetRow_mB710CBF601D939F261A73DCD6910B0ABAC501585 (void);
// 0x0000012B System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::RecycleRow(SRF.UI.Layout.VirtualVerticalLayoutGroup/Row)
extern void VirtualVerticalLayoutGroup_RecycleRow_mFBE0E4D1CFD7ECFEB6195D8DE284F4303EFEC491 (void);
// 0x0000012C System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::PopulateRow(System.Int32,SRF.UI.Layout.VirtualVerticalLayoutGroup/Row)
extern void VirtualVerticalLayoutGroup_PopulateRow_m452E1DBC0AAA252ADC28512BCF2A3EC07D2D4FED (void);
// 0x0000012D SRF.UI.Layout.VirtualVerticalLayoutGroup/Row SRF.UI.Layout.VirtualVerticalLayoutGroup::CreateRow()
extern void VirtualVerticalLayoutGroup_CreateRow_m3C90AC320A2EE85B41D538117CA0ED6902BE1387 (void);
// 0x0000012E System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup::.ctor()
extern void VirtualVerticalLayoutGroup__ctor_m800E17E18858B99EF206F601B0683EE083DED632 (void);
// 0x0000012F System.Void SRF.Service.ServiceAttribute::.ctor(System.Type)
extern void ServiceAttribute__ctor_mA14F2564D8999D7C252A02861F5607C5892B7A04 (void);
// 0x00000130 System.Type SRF.Service.ServiceAttribute::get_ServiceType()
extern void ServiceAttribute_get_ServiceType_m35E689F52314B2E75DC928C93F57542B87B6BE73 (void);
// 0x00000131 System.Void SRF.Service.ServiceAttribute::set_ServiceType(System.Type)
extern void ServiceAttribute_set_ServiceType_mB6833B40B7BCD18F99496B7554C2789D6D150403 (void);
// 0x00000132 System.Void SRF.Service.ServiceSelectorAttribute::.ctor(System.Type)
extern void ServiceSelectorAttribute__ctor_m24FB871F409FDF7874828B1AED9F8780FF23216A (void);
// 0x00000133 System.Type SRF.Service.ServiceSelectorAttribute::get_ServiceType()
extern void ServiceSelectorAttribute_get_ServiceType_m7AB870E8521DB39327F2D63D4AA0D82913222A99 (void);
// 0x00000134 System.Void SRF.Service.ServiceSelectorAttribute::set_ServiceType(System.Type)
extern void ServiceSelectorAttribute_set_ServiceType_m052699D719B806EC4E7DF7A2A4CDB749A7125E1C (void);
// 0x00000135 System.Void SRF.Service.ServiceConstructorAttribute::.ctor(System.Type)
extern void ServiceConstructorAttribute__ctor_m4B56EAEB9BC785547AFE4E0D4B22DAF223CA14A5 (void);
// 0x00000136 System.Type SRF.Service.ServiceConstructorAttribute::get_ServiceType()
extern void ServiceConstructorAttribute_get_ServiceType_m617E53723192215114FC5662AAAC2AD52D8BD7DF (void);
// 0x00000137 System.Void SRF.Service.ServiceConstructorAttribute::set_ServiceType(System.Type)
extern void ServiceConstructorAttribute_set_ServiceType_m106C3978300FFCA8ABD0FD2DF3BEE69A365CE72F (void);
// 0x00000138 System.Boolean SRF.Service.IAsyncService::get_IsLoaded()
// 0x00000139 System.Type[] SRF.Service.SRDependencyServiceBase`1::get_Dependencies()
// 0x0000013A System.Boolean SRF.Service.SRDependencyServiceBase`1::get_IsLoaded()
// 0x0000013B System.Void SRF.Service.SRDependencyServiceBase`1::Log(System.String,UnityEngine.Object)
// 0x0000013C System.Void SRF.Service.SRDependencyServiceBase`1::Start()
// 0x0000013D System.Void SRF.Service.SRDependencyServiceBase`1::OnLoaded()
// 0x0000013E System.Collections.IEnumerator SRF.Service.SRDependencyServiceBase`1::LoadDependencies()
// 0x0000013F System.Void SRF.Service.SRDependencyServiceBase`1::.ctor()
// 0x00000140 System.String SRF.Service.SRSceneServiceBase`2::get_SceneName()
// 0x00000141 TImpl SRF.Service.SRSceneServiceBase`2::get_RootObject()
// 0x00000142 System.Boolean SRF.Service.SRSceneServiceBase`2::get_IsLoaded()
// 0x00000143 System.Void SRF.Service.SRSceneServiceBase`2::Log(System.String,UnityEngine.Object)
// 0x00000144 System.Void SRF.Service.SRSceneServiceBase`2::Start()
// 0x00000145 System.Void SRF.Service.SRSceneServiceBase`2::OnDestroy()
// 0x00000146 System.Void SRF.Service.SRSceneServiceBase`2::OnLoaded()
// 0x00000147 System.Collections.IEnumerator SRF.Service.SRSceneServiceBase`2::LoadCoroutine()
// 0x00000148 System.Void SRF.Service.SRSceneServiceBase`2::.ctor()
// 0x00000149 System.Void SRF.Service.SRServiceBase`1::Awake()
// 0x0000014A System.Void SRF.Service.SRServiceBase`1::OnDestroy()
// 0x0000014B System.Void SRF.Service.SRServiceBase`1::.ctor()
// 0x0000014C System.Void SRF.Service.SRServiceManager::RuntimeInitialize()
extern void SRServiceManager_RuntimeInitialize_mA2DD11FCB7674E49DC852B6DAFB5DF7DCE402F43 (void);
// 0x0000014D System.Void SRF.Service.SRServiceManager::RegisterAssembly()
// 0x0000014E System.Boolean SRF.Service.SRServiceManager::get_IsLoading()
extern void SRServiceManager_get_IsLoading_mAB9771A9877F976529CAA69CC55C5E7A3234F511 (void);
// 0x0000014F T SRF.Service.SRServiceManager::GetService()
// 0x00000150 System.Object SRF.Service.SRServiceManager::GetService(System.Type)
extern void SRServiceManager_GetService_mC5BDFEAF341A6BFFD1A7DFB93DA47A5C23199611 (void);
// 0x00000151 System.Object SRF.Service.SRServiceManager::GetServiceInternal(System.Type)
extern void SRServiceManager_GetServiceInternal_mAEA0144ED6FFB9ED0EDF9372D362C338672F6593 (void);
// 0x00000152 System.Boolean SRF.Service.SRServiceManager::HasService()
// 0x00000153 System.Boolean SRF.Service.SRServiceManager::HasService(System.Type)
extern void SRServiceManager_HasService_mBD0514E7D95A2BB3360E897F82BED2C28B559FB0 (void);
// 0x00000154 System.Void SRF.Service.SRServiceManager::RegisterService(System.Object)
// 0x00000155 System.Void SRF.Service.SRServiceManager::RegisterService(System.Type,System.Object)
extern void SRServiceManager_RegisterService_m4AC9DFCFC7AF35437857FE72F1C68582752F0463 (void);
// 0x00000156 System.Void SRF.Service.SRServiceManager::UnRegisterService()
// 0x00000157 System.Void SRF.Service.SRServiceManager::UnRegisterService(System.Type)
extern void SRServiceManager_UnRegisterService_m61DF9F0EF65DD755593A256D3D395A6DE1C76A48 (void);
// 0x00000158 System.Void SRF.Service.SRServiceManager::Awake()
extern void SRServiceManager_Awake_m2B3E6C70AE19DF4945892FEADB91ACC1212F00B8 (void);
// 0x00000159 System.Void SRF.Service.SRServiceManager::UpdateStubs()
extern void SRServiceManager_UpdateStubs_mF6B39CCB5DEA52646B6A5C54E1E152588FCD7079 (void);
// 0x0000015A System.Object SRF.Service.SRServiceManager::AutoCreateService(System.Type)
extern void SRServiceManager_AutoCreateService_mE157A3B15BD3F00223A300413FFFC449B14311FC (void);
// 0x0000015B System.Void SRF.Service.SRServiceManager::OnApplicationQuit()
extern void SRServiceManager_OnApplicationQuit_mACE22B1A3565698F88870C5A2E2539E49EE9D2DF (void);
// 0x0000015C System.Object SRF.Service.SRServiceManager::DefaultServiceConstructor(System.Type,System.Type)
extern void SRServiceManager_DefaultServiceConstructor_mA71AFD97F247592A7B3DE22093580FE2C4198E35 (void);
// 0x0000015D System.Void SRF.Service.SRServiceManager::ScanType(System.Type)
extern void SRServiceManager_ScanType_mDC6855A2B8391D6044D938AB3AAA37D0CC488E36 (void);
// 0x0000015E System.Void SRF.Service.SRServiceManager::ScanTypeForSelectors(System.Type,System.Collections.Generic.List`1<SRF.Service.SRServiceManager/ServiceStub>)
extern void SRServiceManager_ScanTypeForSelectors_m6700A4064BBD2375E9BDD95E74E40BA39178D1DE (void);
// 0x0000015F System.Void SRF.Service.SRServiceManager::ScanTypeForConstructors(System.Type,System.Collections.Generic.List`1<SRF.Service.SRServiceManager/ServiceStub>)
extern void SRServiceManager_ScanTypeForConstructors_m283D2889D7C3221951C6A316298FFBB4BB1344C4 (void);
// 0x00000160 System.Reflection.MethodInfo[] SRF.Service.SRServiceManager::GetStaticMethods(System.Type)
extern void SRServiceManager_GetStaticMethods_m039692329095F3C20596DC0B35BED8074C4EE583 (void);
// 0x00000161 System.Void SRF.Service.SRServiceManager::.ctor()
extern void SRServiceManager__ctor_m11604B82B36AB410E385D298B08B4112AD31ED66 (void);
// 0x00000162 System.Void SRF.Service.SRServiceManager::.cctor()
extern void SRServiceManager__cctor_mAC957B834187D255B433370CB61DB2737F134385 (void);
// 0x00000163 System.Void SRF.Helpers.MethodReference::.ctor(System.Object,System.Reflection.MethodInfo)
extern void MethodReference__ctor_m07B0F98AEC963534E7D335BC4B4CE3C52D3016FE (void);
// 0x00000164 System.String SRF.Helpers.MethodReference::get_MethodName()
extern void MethodReference_get_MethodName_m98F99BFCB03D3CD6BAC73DBFFD48D53C9CF8F65A (void);
// 0x00000165 System.Object SRF.Helpers.MethodReference::Invoke(System.Object[])
extern void MethodReference_Invoke_m3691AD1FA58F3A22A06DB2083E4B7725A761A2A2 (void);
// 0x00000166 System.Void SRF.Helpers.PropertyReference::.ctor(System.Object,System.Reflection.PropertyInfo)
extern void PropertyReference__ctor_m692424332F8F16E3B1189BB87E9ADFAA1901EB73 (void);
// 0x00000167 System.String SRF.Helpers.PropertyReference::get_PropertyName()
extern void PropertyReference_get_PropertyName_mB1A49B3B71F620C7ECF07E7FC4EA19D1083128A0 (void);
// 0x00000168 System.Type SRF.Helpers.PropertyReference::get_PropertyType()
extern void PropertyReference_get_PropertyType_mFE7B0445B3CD9B46DA2B984235EB59FFAAC27670 (void);
// 0x00000169 System.Boolean SRF.Helpers.PropertyReference::get_CanRead()
extern void PropertyReference_get_CanRead_m8068EF49C04842D54AF5DB1508759310CD12858F (void);
// 0x0000016A System.Boolean SRF.Helpers.PropertyReference::get_CanWrite()
extern void PropertyReference_get_CanWrite_m8D86547A9E3B445A39F34E106B05D143F7823FF7 (void);
// 0x0000016B System.Object SRF.Helpers.PropertyReference::GetValue()
extern void PropertyReference_GetValue_m8FA146D2E17DDF8D929471BA0F67593281374513 (void);
// 0x0000016C System.Void SRF.Helpers.PropertyReference::SetValue(System.Object)
extern void PropertyReference_SetValue_m30EAD407C8FFC5B7B691908E6E906A0A8B846F91 (void);
// 0x0000016D T SRF.Helpers.PropertyReference::GetAttribute()
// 0x0000016E System.Void SRF.Helpers.SRReflection::SetPropertyValue(System.Object,System.Reflection.PropertyInfo,System.Object)
extern void SRReflection_SetPropertyValue_mF03C292387F6E7E84AABA2EE6D2E3E003B55A563 (void);
// 0x0000016F System.Object SRF.Helpers.SRReflection::GetPropertyValue(System.Object,System.Reflection.PropertyInfo)
extern void SRReflection_GetPropertyValue_mC60B671028B099C294CF2F409999FE6530C434A0 (void);
// 0x00000170 T SRF.Helpers.SRReflection::GetAttribute(System.Reflection.MemberInfo)
// 0x00000171 T SRF.Components.SRAutoSingleton`1::get_Instance()
// 0x00000172 System.Boolean SRF.Components.SRAutoSingleton`1::get_HasInstance()
// 0x00000173 System.Void SRF.Components.SRAutoSingleton`1::Awake()
// 0x00000174 System.Void SRF.Components.SRAutoSingleton`1::OnApplicationQuit()
// 0x00000175 System.Void SRF.Components.SRAutoSingleton`1::.ctor()
// 0x00000176 T SRF.Components.SRSingleton`1::get_Instance()
// 0x00000177 System.Boolean SRF.Components.SRSingleton`1::get_HasInstance()
// 0x00000178 System.Void SRF.Components.SRSingleton`1::Register()
// 0x00000179 System.Void SRF.Components.SRSingleton`1::Awake()
// 0x0000017A System.Void SRF.Components.SRSingleton`1::OnEnable()
// 0x0000017B System.Void SRF.Components.SRSingleton`1::OnApplicationQuit()
// 0x0000017C System.Void SRF.Components.SRSingleton`1::.ctor()
// 0x0000017D System.Single SRMath/TweenFunctions::Linear(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_Linear_mC810E7062E10BE516B1759EB9ACA08EF45650A71 (void);
// 0x0000017E System.Single SRMath/TweenFunctions::ExpoEaseOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_ExpoEaseOut_m9A2570C13FC7521BE0A140E54F431F4C7FF4FC11 (void);
// 0x0000017F System.Single SRMath/TweenFunctions::ExpoEaseIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_ExpoEaseIn_m592E27F97D1141803CC3510361587AA17C039732 (void);
// 0x00000180 System.Single SRMath/TweenFunctions::ExpoEaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_ExpoEaseInOut_m0F856FD09E5B39C5AEAACB862BAE9AEA7224616D (void);
// 0x00000181 System.Single SRMath/TweenFunctions::ExpoEaseOutIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_ExpoEaseOutIn_mFBFDA01BD55B2852FC725D7A46C4C480DE6F4E13 (void);
// 0x00000182 System.Single SRMath/TweenFunctions::CircEaseOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_CircEaseOut_m251A7E11A04A2043A2059115DDEE69605134CE1E (void);
// 0x00000183 System.Single SRMath/TweenFunctions::CircEaseIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_CircEaseIn_m4AFB94A8233DB303636526E0B598C45B99B053C4 (void);
// 0x00000184 System.Single SRMath/TweenFunctions::CircEaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_CircEaseInOut_mE7362B67CE9481667D9C533F422D02E918BAE45E (void);
// 0x00000185 System.Single SRMath/TweenFunctions::CircEaseOutIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_CircEaseOutIn_mA34C044F7ED42F2D6B97DEE5DC46D50FC305C168 (void);
// 0x00000186 System.Single SRMath/TweenFunctions::QuadEaseOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_QuadEaseOut_m02FEDEE19194F4AB29054A64AB92A0082F503A10 (void);
// 0x00000187 System.Single SRMath/TweenFunctions::QuadEaseIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_QuadEaseIn_m87A817ABA9B8C52B490ACDC341756788074165A7 (void);
// 0x00000188 System.Single SRMath/TweenFunctions::QuadEaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_QuadEaseInOut_m1A34F2E1E505D5717B6638200680B7B44D048FAC (void);
// 0x00000189 System.Single SRMath/TweenFunctions::QuadEaseOutIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_QuadEaseOutIn_m61D906A7805547DD42E624A8F2BEFDB18B9E956C (void);
// 0x0000018A System.Single SRMath/TweenFunctions::SineEaseOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_SineEaseOut_m4E6F03001C67D2A9B5886E2CA6908A68555BA14D (void);
// 0x0000018B System.Single SRMath/TweenFunctions::SineEaseIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_SineEaseIn_mC76AC4FAA88BA2312C894445B6A25B7CD483CF13 (void);
// 0x0000018C System.Single SRMath/TweenFunctions::SineEaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_SineEaseInOut_mF7F42EF23B21B06EEF298D3BAECBBED9925C9EB6 (void);
// 0x0000018D System.Single SRMath/TweenFunctions::SineEaseOutIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_SineEaseOutIn_m1EF0E54480B58582035391F53AD4E1945B6D9AC0 (void);
// 0x0000018E System.Single SRMath/TweenFunctions::CubicEaseOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_CubicEaseOut_m2073B536FC3CA4AAA2B41B7547172088471C0E52 (void);
// 0x0000018F System.Single SRMath/TweenFunctions::CubicEaseIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_CubicEaseIn_m559186F2937A35048C1A9F01BFFA4098D39013EC (void);
// 0x00000190 System.Single SRMath/TweenFunctions::CubicEaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_CubicEaseInOut_m639E91B4861BCBABA7BCFD2B7B9A535354EF1E97 (void);
// 0x00000191 System.Single SRMath/TweenFunctions::CubicEaseOutIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_CubicEaseOutIn_mCBA25CA198484D911BED6A3B0152162413D52CC0 (void);
// 0x00000192 System.Single SRMath/TweenFunctions::QuartEaseOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_QuartEaseOut_m6C2217B96022BEDE88ABC7AB3B7701F07DD95D02 (void);
// 0x00000193 System.Single SRMath/TweenFunctions::QuartEaseIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_QuartEaseIn_m34D82E3F39522B81F89A61CD13A0825CD7FBBD84 (void);
// 0x00000194 System.Single SRMath/TweenFunctions::QuartEaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_QuartEaseInOut_m32FCE18DFC0FA73663EF55F2CC75E6DAB7C19CCD (void);
// 0x00000195 System.Single SRMath/TweenFunctions::QuartEaseOutIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_QuartEaseOutIn_m29AD1668F17BE1A0D08A3DAC78B39639EB44E643 (void);
// 0x00000196 System.Single SRMath/TweenFunctions::QuintEaseOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_QuintEaseOut_m5E8DD4F3DD13ED1F3178DB7A82951BE981A4AD39 (void);
// 0x00000197 System.Single SRMath/TweenFunctions::QuintEaseIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_QuintEaseIn_m1D8A08E7FEA04F7AF91912EFBE6E9E337C06F63F (void);
// 0x00000198 System.Single SRMath/TweenFunctions::QuintEaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_QuintEaseInOut_m43CBA5F8C99B920EFA9CE4BEB7A878C4C37A8E76 (void);
// 0x00000199 System.Single SRMath/TweenFunctions::QuintEaseOutIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_QuintEaseOutIn_m199FB2838381EC5A37EC7F4BF482C01E950BB0BE (void);
// 0x0000019A System.Single SRMath/TweenFunctions::ElasticEaseOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_ElasticEaseOut_m4CE4446E48780349641BC7151499D05EC9904603 (void);
// 0x0000019B System.Single SRMath/TweenFunctions::ElasticEaseIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_ElasticEaseIn_m4329F14B508A15651C6CD8E9791A86D58C332B3F (void);
// 0x0000019C System.Single SRMath/TweenFunctions::ElasticEaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_ElasticEaseInOut_mDBE127AE286670CCF7122D1B3A4EE8386B43B85D (void);
// 0x0000019D System.Single SRMath/TweenFunctions::ElasticEaseOutIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_ElasticEaseOutIn_mB5C3D85C5FE9CDB1A01693C8A13F7BCDA49B9F53 (void);
// 0x0000019E System.Single SRMath/TweenFunctions::BounceEaseOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_BounceEaseOut_m638BEE2D29F0EDE7456C1274F77F324E51EDEBD6 (void);
// 0x0000019F System.Single SRMath/TweenFunctions::BounceEaseIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_BounceEaseIn_m2AD31FAB8C88D5B83DFF86CF0066D2C7755D419E (void);
// 0x000001A0 System.Single SRMath/TweenFunctions::BounceEaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_BounceEaseInOut_m307094CCBCDC9306668AF617B0166625668DDAEF (void);
// 0x000001A1 System.Single SRMath/TweenFunctions::BounceEaseOutIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_BounceEaseOutIn_mA3026ED12B2C9D3642D48DD57571863D98FA5355 (void);
// 0x000001A2 System.Single SRMath/TweenFunctions::BackEaseOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_BackEaseOut_m3CD24966A426454130BF06FD62B01BA2AA429E53 (void);
// 0x000001A3 System.Single SRMath/TweenFunctions::BackEaseIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_BackEaseIn_m660FC09B47CB328DF23C9F1A4153A8A92ABE71E1 (void);
// 0x000001A4 System.Single SRMath/TweenFunctions::BackEaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_BackEaseInOut_mA1AA2A432716FEAEC9A39D0205AC7C0B8F5E06CB (void);
// 0x000001A5 System.Single SRMath/TweenFunctions::BackEaseOutIn(System.Single,System.Single,System.Single,System.Single)
extern void TweenFunctions_BackEaseOutIn_mC2F16AED2728CD5EAE6B164CC5FB8DB56329BE8C (void);
// 0x000001A6 System.Void SRF.Json/Parser::.ctor(System.String)
extern void Parser__ctor_mDC4C8F62390F503AFDF6DE21EF54355FD66350CC (void);
// 0x000001A7 System.Char SRF.Json/Parser::get_PeekChar()
extern void Parser_get_PeekChar_m24EC2879535CFB39D91AC36F8E084E5D2DD065AA (void);
// 0x000001A8 System.Char SRF.Json/Parser::get_NextChar()
extern void Parser_get_NextChar_m23E7FA2C13886E96D6F1D8A736E9342B4197E2D5 (void);
// 0x000001A9 System.String SRF.Json/Parser::get_NextWord()
extern void Parser_get_NextWord_mA26C4E933B089119C8045C50AD65091E539AF1DC (void);
// 0x000001AA SRF.Json/Parser/TOKEN SRF.Json/Parser::get_NextToken()
extern void Parser_get_NextToken_m33BB77CBDA555C19C72B9BAEB69ABE8AF1C20125 (void);
// 0x000001AB System.Void SRF.Json/Parser::Dispose()
extern void Parser_Dispose_m02B94AD40A7D96A395B0B140C0E553B0E38C03F9 (void);
// 0x000001AC System.Boolean SRF.Json/Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_mC57DCDD911BAE1780D4F2F11D88ED9A178E4B08C (void);
// 0x000001AD System.Object SRF.Json/Parser::Parse(System.String)
extern void Parser_Parse_m34902A9AD5F31EE8A59559D3819FDD2AA90E2E5A (void);
// 0x000001AE System.Collections.Generic.Dictionary`2<System.String,System.Object> SRF.Json/Parser::ParseObject()
extern void Parser_ParseObject_mC607B1D8BB8296556209E62476CE5180A5C17523 (void);
// 0x000001AF System.Collections.Generic.List`1<System.Object> SRF.Json/Parser::ParseArray()
extern void Parser_ParseArray_mA7C900B7AE88A2104F286FAAC588985BB6B2C27A (void);
// 0x000001B0 System.Object SRF.Json/Parser::ParseValue()
extern void Parser_ParseValue_mDF67032D6AAD9D95F365E8304D661DC511C39E99 (void);
// 0x000001B1 System.Object SRF.Json/Parser::ParseByToken(SRF.Json/Parser/TOKEN)
extern void Parser_ParseByToken_m28BB6D085467B250187A0BDDE692451759F600D1 (void);
// 0x000001B2 System.String SRF.Json/Parser::ParseString()
extern void Parser_ParseString_mB8939476A75A6DBC61022C058A89EDF6A46A0722 (void);
// 0x000001B3 System.Object SRF.Json/Parser::ParseNumber()
extern void Parser_ParseNumber_m618C3CB577ECD0F6458CF7F5A7B10172638BDBFF (void);
// 0x000001B4 System.Void SRF.Json/Parser::EatWhitespace()
extern void Parser_EatWhitespace_m03A9308FD9D8DA8977BAA9952B8FB5BBB3F81BBF (void);
// 0x000001B5 System.Void SRF.Json/Serializer::.ctor()
extern void Serializer__ctor_m9B5A9593E0604A2CBDBA4D8517FD41EB9F2AC76B (void);
// 0x000001B6 System.String SRF.Json/Serializer::Serialize(System.Object)
extern void Serializer_Serialize_mE63B1CE00239A4A1A0028436848C5AB868827FFA (void);
// 0x000001B7 System.Void SRF.Json/Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m18EA20A57F6EA7FCA8B98DAE1F855BE40DFB27D5 (void);
// 0x000001B8 System.Void SRF.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_m08AD9B0227F3E5F499F86082DFC0BC4429CE409B (void);
// 0x000001B9 System.Void SRF.Json/Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_m51C08AD634123CB6174F656329543578E87D97EF (void);
// 0x000001BA System.Void SRF.Json/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_mF10F5612F3D99187806CB52EB252F39CFA3AE7AA (void);
// 0x000001BB System.Void SRF.Json/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_m3004D42CFAABDC519BECDB626CB977B5473631E7 (void);
// 0x000001BC System.Void SRF.SRList`1/<GetEnumerator>d__15::.ctor(System.Int32)
// 0x000001BD System.Void SRF.SRList`1/<GetEnumerator>d__15::System.IDisposable.Dispose()
// 0x000001BE System.Boolean SRF.SRList`1/<GetEnumerator>d__15::MoveNext()
// 0x000001BF T SRF.SRList`1/<GetEnumerator>d__15::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000001C0 System.Void SRF.SRList`1/<GetEnumerator>d__15::System.Collections.IEnumerator.Reset()
// 0x000001C1 System.Object SRF.SRList`1/<GetEnumerator>d__15::System.Collections.IEnumerator.get_Current()
// 0x000001C2 System.Void SRF.Coroutines/<WaitForSecondsRealTime>d__0::.ctor(System.Int32)
extern void U3CWaitForSecondsRealTimeU3Ed__0__ctor_m73E2FBE1704C4105F34AA572B0574C03CDC89439 (void);
// 0x000001C3 System.Void SRF.Coroutines/<WaitForSecondsRealTime>d__0::System.IDisposable.Dispose()
extern void U3CWaitForSecondsRealTimeU3Ed__0_System_IDisposable_Dispose_m5CAB778FD3F6AE903EDA2353BCF28D858501341F (void);
// 0x000001C4 System.Boolean SRF.Coroutines/<WaitForSecondsRealTime>d__0::MoveNext()
extern void U3CWaitForSecondsRealTimeU3Ed__0_MoveNext_m948A4CFAA30E74608CC99A2D1C9EDD7169908BED (void);
// 0x000001C5 System.Object SRF.Coroutines/<WaitForSecondsRealTime>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSecondsRealTimeU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5778F1553314F5F92FE60C975B25DCED7C344EDA (void);
// 0x000001C6 System.Void SRF.Coroutines/<WaitForSecondsRealTime>d__0::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSecondsRealTimeU3Ed__0_System_Collections_IEnumerator_Reset_m33F1F2F19567C3B1EF2881C22D547F36048D43B0 (void);
// 0x000001C7 System.Object SRF.Coroutines/<WaitForSecondsRealTime>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSecondsRealTimeU3Ed__0_System_Collections_IEnumerator_get_Current_m2FF811A3E815EE35906E29C8282C066798105C4B (void);
// 0x000001C8 System.Void SRF.SRFTransformExtensions/<GetChildren>d__0::.ctor(System.Int32)
extern void U3CGetChildrenU3Ed__0__ctor_mDEF50FEB9E66802DD490B5422F15CD2DC4E3B3F8 (void);
// 0x000001C9 System.Void SRF.SRFTransformExtensions/<GetChildren>d__0::System.IDisposable.Dispose()
extern void U3CGetChildrenU3Ed__0_System_IDisposable_Dispose_m0B1A7F813FF9E2317C3BA3925B748E9D244116AB (void);
// 0x000001CA System.Boolean SRF.SRFTransformExtensions/<GetChildren>d__0::MoveNext()
extern void U3CGetChildrenU3Ed__0_MoveNext_mE575E00B8FB0CC6A9B426560165F9C3C10D040BB (void);
// 0x000001CB UnityEngine.Transform SRF.SRFTransformExtensions/<GetChildren>d__0::System.Collections.Generic.IEnumerator<UnityEngine.Transform>.get_Current()
extern void U3CGetChildrenU3Ed__0_System_Collections_Generic_IEnumeratorU3CUnityEngine_TransformU3E_get_Current_m430FDEADF6193824182E19CC52BDE70A6CCE4A55 (void);
// 0x000001CC System.Void SRF.SRFTransformExtensions/<GetChildren>d__0::System.Collections.IEnumerator.Reset()
extern void U3CGetChildrenU3Ed__0_System_Collections_IEnumerator_Reset_m4174D49F98727E3BCECE0D1C8F37F2CCEAF3DADA (void);
// 0x000001CD System.Object SRF.SRFTransformExtensions/<GetChildren>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CGetChildrenU3Ed__0_System_Collections_IEnumerator_get_Current_m24DFD4C121BF4705F14B97641F7CD8F6F5FBF1DB (void);
// 0x000001CE System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> SRF.SRFTransformExtensions/<GetChildren>d__0::System.Collections.Generic.IEnumerable<UnityEngine.Transform>.GetEnumerator()
extern void U3CGetChildrenU3Ed__0_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_mF26E99781AC869019E97030C859A7D73761815A2 (void);
// 0x000001CF System.Collections.IEnumerator SRF.SRFTransformExtensions/<GetChildren>d__0::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetChildrenU3Ed__0_System_Collections_IEnumerable_GetEnumerator_m3D2E7C0F1581576E88C23189F1822350FB387C42 (void);
// 0x000001D0 System.Void SRF.UI.SRSpinner/SpinEvent::.ctor()
extern void SpinEvent__ctor_m0923DEA7C7A33F5E304538DE7DB500B789FED39F (void);
// 0x000001D1 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup/SelectedItemChangedEvent::.ctor()
extern void SelectedItemChangedEvent__ctor_m28C580B3711D492C54806427434000F705A63EC4 (void);
// 0x000001D2 System.Void SRF.UI.Layout.VirtualVerticalLayoutGroup/Row::.ctor()
extern void Row__ctor_m6835D1EF80456B05B33F7FB15D30C3D0F80B720B (void);
// 0x000001D3 System.Void SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>d__8::.ctor(System.Int32)
// 0x000001D4 System.Void SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>d__8::System.IDisposable.Dispose()
// 0x000001D5 System.Boolean SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>d__8::MoveNext()
// 0x000001D6 System.Object SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x000001D7 System.Void SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>d__8::System.Collections.IEnumerator.Reset()
// 0x000001D8 System.Object SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>d__8::System.Collections.IEnumerator.get_Current()
// 0x000001D9 System.Void SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>d__11::.ctor(System.Int32)
// 0x000001DA System.Void SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>d__11::System.IDisposable.Dispose()
// 0x000001DB System.Boolean SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>d__11::MoveNext()
// 0x000001DC System.Object SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x000001DD System.Void SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>d__11::System.Collections.IEnumerator.Reset()
// 0x000001DE System.Object SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>d__11::System.Collections.IEnumerator.get_Current()
// 0x000001DF System.Void SRF.Service.SRServiceManager/Service::.ctor()
extern void Service__ctor_mDAC8BE1C6A4FF294363B2C1F25AE870FB557E540 (void);
// 0x000001E0 System.String SRF.Service.SRServiceManager/ServiceStub::ToString()
extern void ServiceStub_ToString_m8528A45DDB4B334B92156E73C8AB6525A954EE8E (void);
// 0x000001E1 System.Void SRF.Service.SRServiceManager/ServiceStub::.ctor()
extern void ServiceStub__ctor_mE380B254D8717A970EFB17CEF64F509F0D1C04DD (void);
// 0x000001E2 System.Void SRF.Service.SRServiceManager/<>c::.cctor()
extern void U3CU3Ec__cctor_mEFC8CF876834882935159BEFD7E1BACDADD5248C (void);
// 0x000001E3 System.Void SRF.Service.SRServiceManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mCE1B9A2F219EC13CC62FC04E9CC0D5A96C849A56 (void);
// 0x000001E4 System.String SRF.Service.SRServiceManager/<>c::<UpdateStubs>b__22_0(SRF.Service.SRServiceManager/ServiceStub)
extern void U3CU3Ec_U3CUpdateStubsU3Eb__22_0_m6827063FECB265D40BD6E5E5D0ABF1C0A0EC6A19 (void);
// 0x000001E5 System.Void SRF.Service.SRServiceManager/<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_mDCDAA5E0EEF3463AA882716FFFEDA9EB822F072A (void);
// 0x000001E6 System.Boolean SRF.Service.SRServiceManager/<>c__DisplayClass27_0::<ScanTypeForSelectors>b__0(SRF.Service.SRServiceManager/ServiceStub)
extern void U3CU3Ec__DisplayClass27_0_U3CScanTypeForSelectorsU3Eb__0_m4A4701A42E4000F1506410CE67F8FA779C0919F1 (void);
// 0x000001E7 System.Void SRF.Service.SRServiceManager/<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m219AC9C6AB2E791A0865015BB3D0B4AECB771552 (void);
// 0x000001E8 System.Boolean SRF.Service.SRServiceManager/<>c__DisplayClass28_0::<ScanTypeForConstructors>b__0(SRF.Service.SRServiceManager/ServiceStub)
extern void U3CU3Ec__DisplayClass28_0_U3CScanTypeForConstructorsU3Eb__0_m4DC746BD18B5506A901A6857079C19AB3363D6C6 (void);
// 0x000001E9 System.Object SRF.Service.SRServiceManager/<>c__DisplayClass28_0::<ScanTypeForConstructors>b__1()
extern void U3CU3Ec__DisplayClass28_0_U3CScanTypeForConstructorsU3Eb__1_mABFC079C7443EEDBA3BA7D4FFD13A9C2749F0AAB (void);
static Il2CppMethodPointer s_methodPointers[489] = 
{
	SRDebugUtil_get_IsFixedUpdate_m42755BCA911DBE2714F87ED49334754D200862AC,
	SRDebugUtil_set_IsFixedUpdate_mC453280DAE4EB7BF173A36534C506F94BD0DE2FE,
	SRDebugUtil_AssertNotNull_m329BBCC2FEFF6101969ACA0F3B16B702F724BA63,
	SRDebugUtil_Assert_m42E8BD8E5148D83EB85B80C1C9B70CFC1AB0778C,
	SRDebugUtil_EditorAssertNotNull_m161957627DBD9C8E76147550E5EEB7E388B96EDA,
	SRDebugUtil_EditorAssert_m0A5D0A512A5206E445F2737C3AD858CB7A9BEC05,
	SRFileUtil_DeleteDirectory_mBF8E03E276A631EAD13EC9B945AD367F0B0B4730,
	SRFileUtil_GetBytesReadable_mB508665E2CE66374A33AC37FB3B8075485CA0B72,
	NULL,
	SRInstantiate_Instantiate_m3BBB52B37E5073463CEA7B55AF8E64DB3FA36538,
	NULL,
	SRMath_Ease_m1338555BB09E55446DD824A3BF7D0FB2EC18D9C8,
	SRMath_SpringLerp_mCB275F97AB0583B9C50D3C17DC353D63D63DF04E,
	SRMath_SpringLerp_mCDBE2D83E7F49386C23F119CB44A9D3B3AA3B412,
	SRMath_SpringLerp_mE2D21DDC8F8AFB9D2C4DD90B455DC22D5F704E29,
	SRMath_SpringLerp_mCEBC68267270F4166AEE0268418E7E2C691CBBDD,
	SRMath_SmoothClamp_m4114F3CE0309771D08BE6250C83C9D858E963E10,
	SRMath_LerpUnclamped_m67C8E2CFF7E84FEFBF9C2727E9F9BA92D52D9F2D,
	SRMath_LerpUnclamped_m025EF3006DFBCC7560C40BBEA34AF61934382158,
	SRMath_FacingNormalized_m7B5A447FDA16766CCD2B30E05B12905776299F4E,
	SRMath_WrapAngle_m2ED56AC77B0F3B0FD55EAB55F9C2D691D5679334,
	SRMath_NearestAngle_mAB5091AB2357C0EEDC33FCD1910E4D2374067666,
	SRMath_Wrap_mB11B3F203EE75E04436580C25726041B857A5B04,
	SRMath_Wrap_m561BE4663F1D15F3F9894020D8C988003950106E,
	SRMath_Average_mF97C450E40989F2EC495028BFE19230E5C7B7B69,
	SRMath_Angle_m95F90BE88BDD475144E3B5A5011599DDE0BBB535,
	Json_Deserialize_mF9B9ED81B45857F3A167AD769774BC7B9207622A,
	Json_Serialize_mF26FF87D4BDDA37196A13F540BC0E4830ABA3F69,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SRMonoBehaviour_get_CachedTransform_mDCBB38954DFF098EF4845B2133259F4F33456D8C,
	SRMonoBehaviour_get_CachedCollider_mDDBA14C83F31408644522D43258C3A33F03A68E2,
	SRMonoBehaviour_get_CachedCollider2D_mE8C5615C3CABBF1E949C1D38458821D85A94460A,
	SRMonoBehaviour_get_CachedRigidBody_m5BF4A0AD9F6B9BE03F3E473C990635CE0D9D6F72,
	SRMonoBehaviour_get_CachedRigidBody2D_m6BF89FB8D5AE8D60E5D7FB450680EC9DC3E54522,
	SRMonoBehaviour_get_CachedGameObject_m0594155BD3FC6EA20375B035FCF0EB09A64BD4F7,
	SRMonoBehaviour_get_transform_mF9E38D15E9788B9EF9339975F1CE4B5DED54506D,
	SRMonoBehaviour_AssertNotNull_m3F8044FB947B855B44F10D3704E60CAACA567AA6,
	SRMonoBehaviour_Assert_m92BC18BC18894BBF4F09AD263830810560CDDD1F,
	SRMonoBehaviour_EditorAssertNotNull_m224A5047E5B85229D59D8A865DF31847A140E385,
	SRMonoBehaviour_EditorAssert_mFD778E352407BC82B36643710BEC200290B55350,
	SRMonoBehaviour__ctor_mEAFA5432C2D1CBF3993C07918C880E09A9465597,
	RequiredFieldAttribute__ctor_mEFEF94681F30D2DFA378AAFACBC7B60D35DD23DB,
	RequiredFieldAttribute__ctor_mDD252DADEB178C431BDA1B6FFC20186828613C1B,
	RequiredFieldAttribute_get_AutoSearch_mD5E30F2D0C09005A3ED9CB48018F8421916C7870,
	RequiredFieldAttribute_set_AutoSearch_m9892C491C77369070B5A0E6FD6D25E89E361C477,
	RequiredFieldAttribute_get_AutoCreate_m2734590C6F34AEE1E7EB03F1672D77389CB84993,
	RequiredFieldAttribute_set_AutoCreate_m376DA1AF3A1F9A6F0A7B4ECDF372E750564B0272,
	RequiredFieldAttribute_get_EditorOnly_mD7EA4B968624C44D131BD0435F461B93E9DF6EAC,
	RequiredFieldAttribute_set_EditorOnly_mC5030F3277A69067278B6A26DD0E1CF8AFAE9A96,
	ImportAttribute__ctor_m43886C5690126F6D565AE7E510118415E085883A,
	ImportAttribute__ctor_m7353ECACE1CA37CCD13A8164F7DF884D62B30FF8,
	SRMonoBehaviourEx_CheckFields_m3F0788A3019FF8E7186B744C02BB2E9DAAA6A7B5,
	SRMonoBehaviourEx_PopulateObject_m78704A22CC28A09B1B071F06E226AA8CDD4A691F,
	SRMonoBehaviourEx_ScanType_mD0B633932E7626D671FFD46DD04B9B2C4FB4F921,
	SRMonoBehaviourEx_Awake_m5273E60992532123296D9A6F07D17D0D206553D3,
	SRMonoBehaviourEx_Start_m901BB03C7EACA4FD65889373303C692BA61E47F4,
	SRMonoBehaviourEx_Update_m69BC1E57223CB33DD8B02B4BC42354D5B278F993,
	SRMonoBehaviourEx_FixedUpdate_m38925ED79F9822EE0077542D88C1E80E76C04889,
	SRMonoBehaviourEx_OnEnable_mD2FD21E4D4F234CC4B73E78E04F90C6156FD7828,
	SRMonoBehaviourEx_OnDisable_m5320A449D0217E10AAF292CD17C7E74BC500A49C,
	SRMonoBehaviourEx_OnDestroy_m06E89F391729656B8E62606E076A6961A8080497,
	SRMonoBehaviourEx__ctor_m058A362C4878D202ECD26D160D172151285404CB,
	Coroutines_WaitForSecondsRealTime_m82707BBCD7907C57D1C9BF0BDE465A8A04492535,
	SRFFloatExtensions_Sqr_m16519937EDF450DDC1FB9A0B17F7CB78312523A0,
	SRFFloatExtensions_SqrRt_m00109E83F8C9BD8CF57A26C88224FA8208B1D91E,
	SRFFloatExtensions_ApproxZero_m81DE68FD1BB9A32D9748B1FE45F6EA328A6C1F11,
	SRFFloatExtensions_Approx_m2195AAAC7B9D1A8E3396FA4B5BB5398FFF1AF75E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SRFGameObjectExtensions_SetLayerRecursive_mA57EDF7E4CDA58291783299992F7AA035A0F7CD7,
	SRFGameObjectExtensions_SetLayerInternal_m405ECBE4CA3B14728D8734C7D3DDE71CD0B9654C,
	NULL,
	NULL,
	NULL,
	SRFStringExtensions_Fmt_mBD139EDDF35D50ABE63736D6159C6A77241D38E8,
	SRFTransformExtensions_GetChildren_mC079689055D65C548C31EB270D7704E2A2124423,
	SRFTransformExtensions_ResetLocal_m375B98E0E1AB93D04A0582DD5ED543355CBB0058,
	SRFTransformExtensions_CreateChild_m52BE36CBFABB57A740DF967CECFC6CA3AFBC1555,
	SRFTransformExtensions_SetParentMaintainLocals_m59AB2ECCBFA14E4718A0653750449662A0725B68,
	SRFTransformExtensions_SetLocals_mB21D9E0F2C4AE782E22DDEA885AD62AEDF327AC1,
	SRFTransformExtensions_Match_mBC932E5A16A9FDB1A09784B14F76511562BB5184,
	SRFTransformExtensions_DestroyChildren_mFCF395C485FCD121D35ECD09BA18FD94991CB675,
	Hierarchy_get_Item_mF4C1AF7D5EAD8F9CD38D04C52447A8CF0D7FF4F3,
	Hierarchy_Get_mBD19B83FF8DD50D089C338674FFFA1DE749D75FE,
	Hierarchy__ctor_m04CC3C2D423F5B296939F215554813736AF5A521,
	Hierarchy__cctor_m86F55340D235E37A5E89BB0BF048F4E86DDBB23D,
	ContentFitText_get_minWidth_m71936C6E94E43785E624307AF193526FBF6E5D2D,
	ContentFitText_get_preferredWidth_mA62AE791CA1F696E328B6EFAA90B66D7AA63F05C,
	ContentFitText_get_flexibleWidth_mC5249A9090756BEE03C8EBE213D9C01C1FF03844,
	ContentFitText_get_minHeight_m018B01E2F631577811A965EF9482A98D1795C0A8,
	ContentFitText_get_preferredHeight_mD90A43C1A0120AD4DC3C3ED738CDE98805AB14A7,
	ContentFitText_get_flexibleHeight_mB8C98E184739788141420BB45D001E8EF79D97D4,
	ContentFitText_get_layoutPriority_m9518CF7D496423EA636B1720C0FAB00F1F770D52,
	ContentFitText_CalculateLayoutInputHorizontal_m6100386FC0346F6DA9BBD32C595DF053C6FF2AB4,
	ContentFitText_CalculateLayoutInputVertical_mF90D578F0DD375392E7F3E67AB32E069E699987C,
	ContentFitText_OnEnable_m1BF429F7BF48D084F7E2AE0DE69B3EEEBBA2E1AF,
	ContentFitText_CopySourceOnLayoutDirty_mEA56D2DA374147146E5A6A3E3F1F03D223EEDDBB,
	ContentFitText_OnTransformParentChanged_m0089193360102AF2F32D973A836571D6D966E94E,
	ContentFitText_OnDisable_mD86C492ECFC40E04655422F3C02F76DE6C6C8133,
	ContentFitText_OnDidApplyAnimationProperties_m4AF02CF3CC02F9FE43790668929A73833186EEC7,
	ContentFitText_OnBeforeTransformParentChanged_m0A61678250DDDA442AB95F832C948E5B4618406C,
	ContentFitText_SetDirty_m7270E687658270637FBD5851C069A2C0774230A1,
	ContentFitText__ctor_mD57F8E1A44C23592A57D31B956B640054A2D7BA0,
	CopyLayoutElement_get_preferredWidth_m263205876685F3B6C062B1E33ED3E6B6439AA132,
	CopyLayoutElement_get_preferredHeight_mA419E1F15A789CCCA768610D179D9E365650989E,
	CopyLayoutElement_get_minWidth_m1F1C747DDEE6F9CCFB1D8ED883066F72C3708165,
	CopyLayoutElement_get_minHeight_m4C8E1B3139E7478945A0E62C1B5A16C72D35D162,
	CopyLayoutElement_get_layoutPriority_m489455381639B61CE186724D7817749E4C4677D7,
	CopyLayoutElement_get_flexibleHeight_mB8D7FF7355F5B91DC26C3075F9FE1F2101C41E9D,
	CopyLayoutElement_get_flexibleWidth_mBBE1E15477BDDF65551C4DF40EDCE78DB371713A,
	CopyLayoutElement_CalculateLayoutInputHorizontal_mF3D8F9164260AFC61DE2293E29E6A7239FEE73B9,
	CopyLayoutElement_CalculateLayoutInputVertical_mA9D0658A17BEBB19CBFC620D5CA26958F88FE4AB,
	CopyLayoutElement__ctor_m9A5C7826307E561CB78050F3B0236577FEF58416,
	CopyPreferredSize_get_preferredWidth_m91D78D4E44F7A480A2C6683877961DD6EAE5A53D,
	CopyPreferredSize_get_preferredHeight_m8FE0BBD1BF3BF738CAC6AC03D149CF5E976B2A60,
	CopyPreferredSize_get_layoutPriority_m669E0692CC80E4D060770C47A4EE73CE906D56D9,
	CopyPreferredSize__ctor_mF559A65533EA4E2FA4C3A7B98AEA616135DAA976,
	CopySizeIntoLayoutElement_get_preferredWidth_m12C59DCB2BB74A9C6B699A8B4FCE4CB819CA088E,
	CopySizeIntoLayoutElement_get_preferredHeight_mD2A988E320B7AE7C8818D9034193BB05216AEACE,
	CopySizeIntoLayoutElement_get_minWidth_mB79D55AA0DFFFDB98017CA4A77B9F9C5F5D72ACF,
	CopySizeIntoLayoutElement_get_minHeight_mD5EF56C401E4B9B911C32D1ACC2C4A992454E3D5,
	CopySizeIntoLayoutElement_get_layoutPriority_mEE5D925BD5F8E1D5CE93524D87D86C2C22E4683E,
	CopySizeIntoLayoutElement__ctor_m787FBFBEEA6B98AB8D65E98339276609929A0F30,
	DragHandle_get_Mult_mAC931EF10093BB78B102EBD8875958AC99D93D8B,
	DragHandle_OnBeginDrag_mB39A8B0EABF7CF54773EF645B859E26A810948A9,
	DragHandle_OnDrag_m98E4DC073EF7398194835CA1A559D67BCA149262,
	DragHandle_OnEndDrag_mE048565DFEDF563C2B327CC99170EB37B1FE98F2,
	DragHandle_Start_m739D759F88E7DD8CD5D05004BA2D30B774CE8191,
	DragHandle_Verify_m6CB87B159D5EA6ED888DC40063D1775CD0BA3BAA,
	DragHandle_GetCurrentValue_m44AF9CA190F68AEF111841FF4E60D39501FB83E4,
	DragHandle_SetCurrentValue_mED2AEE53B10086E7355504F7D62BAC53A16030F1,
	DragHandle_CommitCurrentValue_m45AD34AF26F4A8011E3774CD803CDB1F95D8660F,
	DragHandle_GetMinSize_mBB4B669389F6BC04F0E68E6901EE7F4626C9F394,
	DragHandle_GetMaxSize_m99AF6C5FECB02D9E2EF69F02746D622CDAAC3C47,
	DragHandle__ctor_mCEEBE6985FEB660692AEBDCBBF52728E2491720C,
	FlashGraphic_OnPointerDown_m7EAB6B0AA90EB06647EB7EF0FE3744B117D52ABF,
	FlashGraphic_OnPointerUp_mF62779598E8EDB551288B6553E5078D89B39C793,
	FlashGraphic_OnEnable_mBFAB09DB08849CFE029C8F7AFDC9BCF45FD58959,
	FlashGraphic_Update_mA7A1A3C31A322F314CA9E4BE3389592B1A33ECE6,
	FlashGraphic_Flash_m83F756F7A0651A729640922FBC3111500AB8CEED,
	FlashGraphic__ctor_mEDCA56D21DA6AD7CAC68CDCE1D85D3B08529B28B,
	InheritColour_get_Graphic_m8CD7DA3BE11AEC7CF728AF527E91A6DD98119B45,
	InheritColour_Refresh_m9DE63A98E00CD4990847CAF1356674BF995EB489,
	InheritColour_Update_m6BF1F4A2DE395C74D6519E310BF0AD26C022B814,
	InheritColour_Start_m55610901CD145A94DCCD28C0C4FE426B243C2BF0,
	InheritColour__ctor_m0A17724289BFC595F542880A363A61A0D44C295F,
	LongPressButton_get_onLongPress_mA47E1F93A07B0839A444B27B108CA65A48D53DDE,
	LongPressButton_set_onLongPress_m343B99D9E23A23771EC56E9B0925A8C3A4AF6B76,
	LongPressButton_OnPointerExit_m27571BB6C4350A73B4D0341B732E8A926AFBFB51,
	LongPressButton_OnPointerDown_mCB471784E4DC7F573E5128787FBD92495A247B0E,
	LongPressButton_OnPointerUp_m4E9AA2E4886CB6E95C79DFB99687FFE0938A4BE7,
	LongPressButton_OnPointerClick_mF01EFDD5CDC8B41CCF49D73CE94B792F3670886A,
	LongPressButton_Update_m060AFC4AD2CD0CB967F90984C08865BBD6BA3174,
	LongPressButton__ctor_mB351535AC7C0321E5BDEF7132AFFC4E4B48D837D,
	ResponsiveBase_get_RectTransform_mBDF09658DACB1675D5C7BAC7505CB50D3A8EEDB8,
	ResponsiveBase_OnEnable_m892FE98A40F7E7C025069CB4AEDD51384929EC57,
	ResponsiveBase_OnRectTransformDimensionsChange_mFF1EC9563DFE5547730B9D2BA6347EFDDF1F35EB,
	ResponsiveBase_Update_mC11285514E2B67E04C0E4EAFD292289FC4341660,
	NULL,
	ResponsiveBase_DoRefresh_m9A86017130D4365CFA545A86633FBD3E6F9DD62A,
	ResponsiveBase__ctor_m61DD4219B28F47D093E0B958D494F0BE9F6B0B9B,
	ResponsiveEnable_Refresh_m651080C839A622B28E7526218CCE092B78474AAF,
	ResponsiveEnable__ctor_m4CAD9C4C6F974A31544A1623CB6E3E7442D2D13C,
	ResponsiveResize_Refresh_m1FF75986CADFF8817BD732292BFEB5EE6157572C,
	ResponsiveResize__ctor_m10B8F24C96CC8F884A895A8607970FE9B5A6C2CF,
	SRNumberButton_OnPointerDown_mAB78297888ABA87C47247FAB95C7CD0178530140,
	SRNumberButton_OnPointerUp_m577DA8ED71A63AE7AA464A14E30A7673A4E6F112,
	SRNumberButton_Update_m583613A75316C77168F99C6C910F2902428979A1,
	SRNumberButton_Apply_mD6F7BE598BF9BF8902A2FB091FBE53E658D04F18,
	SRNumberButton__ctor_m3B50EBC2C9A881A3D6595622D5426DBA37EC490B,
	SRNumberSpinner_Awake_m54C3F778DEE61A0308B48C1DA9820471C84478F5,
	SRNumberSpinner_OnPointerClick_mAD0D03BFC231C27720BDBDF910908C65C6328F70,
	SRNumberSpinner_OnPointerDown_m53C36926C264D162A8E5826E80FE33860C05C110,
	SRNumberSpinner_OnPointerUp_mEAB601AB50B3395E23E3937E08ACEEBBDE889D78,
	SRNumberSpinner_OnBeginDrag_m046E71FAF362510E305801D3BEA6EE8A83DD9601,
	SRNumberSpinner_OnDrag_mDD9B658D8194885D12BA0C8C78BF5B2F697BFF04,
	SRNumberSpinner_OnEndDrag_m96FD8A786295C8BEC59B3885696326B52922E8F7,
	SRNumberSpinner__ctor_m8962720FB0C3A9B54CE2FBC45B1595C9ECCE8FF9,
	SRRetinaScaler_get_ThresholdDpi_mE3B4BBBCBDE51F864BF8EA81D6A41170645B56A9,
	SRRetinaScaler_get_RetinaScale_m3E9496E06AF39202AE0537A2D1E3E3DCBEDD8D3F,
	SRRetinaScaler_Start_m2883FE885A87E647356DC90621EB487D2E098D49,
	SRRetinaScaler__ctor_mFD0F50B26F6974AEFBD669BB354663EA72AC59B2,
	SRSpinner_get_OnSpinIncrement_m083B1303A4D11EE15C8C7058FE709D6AF3C5DB3B,
	SRSpinner_set_OnSpinIncrement_m8089E76E1151A553BC8241C21518394B64070B7C,
	SRSpinner_get_OnSpinDecrement_m3F078912B0FEF248BB9B810EDBA914DC4D86703F,
	SRSpinner_set_OnSpinDecrement_m77EB41316324D2F59132E1DCCB458D3775D44F2D,
	SRSpinner_OnBeginDrag_m269E08FA5BAAC658BE1AF735F5A61225C2298F7B,
	SRSpinner_OnDrag_m6E5357E3FEBD6D44174C4DE27EE74D43D5BAC56E,
	SRSpinner_OnIncrement_m87455DA472A63C68120A7F6C025C922718B7BED6,
	SRSpinner_OnDecrement_m2F2A4922C5587B93B96539098739119870C3DCEF,
	SRSpinner__ctor_mBC391963E897B7B969622941DFA78D4FCDFB6BEF,
	SRText_add_LayoutDirty_m37FE42668AFC0D0C91EB8E6DF008DF25233F2097,
	SRText_remove_LayoutDirty_m0A9628EF15DB07FCB206CF52AAE61327E2EF4F45,
	SRText_SetLayoutDirty_m792CD1E229058BB52587C9F1616F487DFABC6360,
	SRText__ctor_m75B252322CCD4A9A2162539ABD255A314A4D2374,
	ScrollToBottomBehaviour_Start_mEDE535860139CA53B703291D6F99B6B19C57D635,
	ScrollToBottomBehaviour_OnEnable_m9D3E2292D0A2B713AC1401AE745A122ABC129CCD,
	ScrollToBottomBehaviour_Trigger_m315273782FFC1F80745D7D1D779DABAC265CEC42,
	ScrollToBottomBehaviour_OnScrollRectValueChanged_mD28927F134D1923C61169A8EB7C093B0EB433207,
	ScrollToBottomBehaviour_Refresh_mE39E15BB38D78FCBB51CB6586CA5ABDB681093C8,
	ScrollToBottomBehaviour_SetVisible_m0F8F79A5FA82F3065AC0A0B7A75DA4C89CBA51E4,
	ScrollToBottomBehaviour__ctor_mD8D4ACD04D58672B7D48438F06026D3078D86766,
	StyleComponent_get_StyleKey_mCA4532490E29E7BA10D394A8082F29EA3FCBFC25,
	StyleComponent_set_StyleKey_m11DF5F90251449B06D44A8754FF045EF734AA884,
	StyleComponent_Start_mB18CA205D9E2251403BA27F1BB9EF8DF5D154A31,
	StyleComponent_OnEnable_m14BC36B942B7560849468A67B83B4E22418EBB98,
	StyleComponent_Refresh_m4FFBB3F87D15F65BEAB421811C83C86998E2D457,
	StyleComponent_GetStyleRoot_m02A00E2794D6AE281455CE45607979B8BBFD7552,
	StyleComponent_ApplyStyle_mE4C0B1B01A061342AB33F78CDC19699253CD8B93,
	StyleComponent_SRStyleDirty_m04BC090762B131BD72A63BA248EA34F1EE995B72,
	StyleComponent__ctor_mFFD116954C4BC710F03193025BBA66FECF36555D,
	StyleRoot_GetStyle_mC2F8C7CACF6E7DAFFE8F1EFDDD9D89E6D7843FFE,
	StyleRoot_OnEnable_mFA160E5C644CBC978EAB1089C920D77CA8C39D47,
	StyleRoot_OnDisable_mFBE8F207325057F5BE16A20C10717A42F46A8720,
	StyleRoot_Update_mDE4FDC671BF7F736DB9828A809AEF9095848F78E,
	StyleRoot_OnStyleSheetChanged_m5B0A32103A73C6EEC7AF044B502475E6ECC861AC,
	StyleRoot_SetDirty_mDBC0B7C8CB68DFEABA18A808465C278319E03829,
	StyleRoot__ctor_m01B1166B6319A015C7A181BA2870769E50D68F1C,
	Style_Copy_m1902B50C78314E7F0EA71CA95158C0FCE9AE01CA,
	Style_CopyFrom_mFAE8A6A7F7E48FB594FFC4CB382E7351304B65F1,
	Style__ctor_mDDC0440ED6111979BBBFDF4F97FB92BC45F52079,
	StyleSheet_GetStyle_m4362ACC33900C5A6F395B27A528354BD9F90D5C0,
	StyleSheet__ctor_m26B0A40188B46E49A8B5DF6C7BE0B0E2F3272722,
	Unselectable_OnSelect_mBA2E6E355E4E8F0BA5ED9AD2B64FBC353DC2FF85,
	Unselectable_Update_mED2C5C55A8FA8BFA43C16515E8625EA79247B2C6,
	Unselectable__ctor_mD34960B9CCC272EA85663AF3C31B4788380EEFD4,
	FlowLayoutGroup_get_IsCenterAlign_mA0C36A0155C2CC3FFC827FF3C11A39A510B148A2,
	FlowLayoutGroup_get_IsRightAlign_m05E1E3FE1910AB26F8F66DE8C9DE574D625F642B,
	FlowLayoutGroup_get_IsMiddleAlign_mA900FCB97310AD76C5CDE1AF8E964136A7C81566,
	FlowLayoutGroup_get_IsLowerAlign_mF204BEE4602B2218B859B1F17D1199E9D333EDEA,
	FlowLayoutGroup_CalculateLayoutInputHorizontal_m8C4495CB4F2FBD3E87DBAEDCC6AB623CAB3B00A8,
	FlowLayoutGroup_SetLayoutHorizontal_m58ED1D3E46EB60C850775674714B15520E386846,
	FlowLayoutGroup_SetLayoutVertical_m51B24D701CA14F1941ACF44F042065E80ED6BCE3,
	FlowLayoutGroup_CalculateLayoutInputVertical_m8CD425FD992C3136131460B28010C95251F5AD1D,
	FlowLayoutGroup_SetLayout_m15E282026AE7835DC6B71CDBED29838C562709AD,
	FlowLayoutGroup_CalculateRowVerticalOffset_m92085127DA08A48CC02D1ECB87873E6A29D12D04,
	FlowLayoutGroup_LayoutRow_m2C3DB27EACA66C9BE802744F2D2459D55F5BD7AB,
	FlowLayoutGroup_GetGreatestMinimumChildWidth_m88B6EAC36B153AC8ECDC1CA8DCF94D451A354C10,
	FlowLayoutGroup__ctor_m7A1B3B7F62E98D0C7FF665BF5A94377AAF8B9A21,
	NULL,
	VirtualVerticalLayoutGroup_get_SelectedItemChanged_m2E45E50CE3D9398BD66EE485BD79EAA52C4D6AC6,
	VirtualVerticalLayoutGroup_set_SelectedItemChanged_m0A4EEE82BFAF9FE2E19E4FAB00EB179A37240FF1,
	VirtualVerticalLayoutGroup_get_SelectedItem_m794B1E5B69AE5BB026DF3C8770A22F8F99691621,
	VirtualVerticalLayoutGroup_set_SelectedItem_m432B1A19861F25B1E55A6657BDD3BD41BB780711,
	VirtualVerticalLayoutGroup_get_minHeight_m5B88EB0A2DCF0A190641B034EF297F578E872677,
	VirtualVerticalLayoutGroup_OnPointerClick_m8F74DBB00FA21A2D0E75659E4DC932AFD1FCD85A,
	VirtualVerticalLayoutGroup_Awake_m65BD3A21B6C2974F288334D6469E8A2200A83305,
	VirtualVerticalLayoutGroup_OnScrollRectValueChanged_mBF269E601E2E0EF67ABEB45B5400AFB48242B407,
	VirtualVerticalLayoutGroup_Start_mD392FAFF504CD9A8E3D3DE2EBF345E56B7FEEF1C,
	VirtualVerticalLayoutGroup_OnEnable_mCF30426326C6BB173DD32056BF04B9B40894AA4D,
	VirtualVerticalLayoutGroup_Update_m49956BC1F090430B525956FC2215C293ABB51729,
	VirtualVerticalLayoutGroup_InvalidateItem_mDB8F4C0DC5D33D60882F6404F0780E9F8AEB7579,
	VirtualVerticalLayoutGroup_RefreshIndexCache_m6242B2C44842B88083D790A251E30E777712CD42,
	VirtualVerticalLayoutGroup_ScrollUpdate_m870017F811BC8F4077F480F14A446AED0E112CF1,
	VirtualVerticalLayoutGroup_CalculateLayoutInputVertical_m0F52F0F3EE2D53FE78C0F2F86CB2817EAF29DF53,
	VirtualVerticalLayoutGroup_SetLayoutHorizontal_mEE56655F57746B3B90D3FC66B5065C6B4269185F,
	VirtualVerticalLayoutGroup_SetLayoutVertical_m431FF4084C2E52931713FEF67A637506CFF38A8F,
	VirtualVerticalLayoutGroup_SetDirty_mF96F92F4553B8EC8BB20DE72102E4E522F13224F,
	VirtualVerticalLayoutGroup_AddItem_m9738F06F9D0C2C6408E58096C3D9756A46434FD4,
	VirtualVerticalLayoutGroup_RemoveItem_m8EF6F223F65F06CD2F82374FE5A1DDE1008C9C30,
	VirtualVerticalLayoutGroup_ClearItems_m00DAE6B8FE6D528668027CFB7040B33DACCB563B,
	VirtualVerticalLayoutGroup_get_ScrollRect_m86FDB5AEB6F811303C55DB47595ADD42DE965D82,
	VirtualVerticalLayoutGroup_get_AlignBottom_m8D0BAC5001A936F9D08BE9806A5C1420C30BA565,
	VirtualVerticalLayoutGroup_get_AlignTop_m519F36A0155BE1725ED4381754E8ADDAB70CD32B,
	VirtualVerticalLayoutGroup_get_ItemHeight_mE7F4A8C578FA0E3A57A44C8DD443F4D9883F1ED6,
	VirtualVerticalLayoutGroup_GetRow_mB710CBF601D939F261A73DCD6910B0ABAC501585,
	VirtualVerticalLayoutGroup_RecycleRow_mFBE0E4D1CFD7ECFEB6195D8DE284F4303EFEC491,
	VirtualVerticalLayoutGroup_PopulateRow_m452E1DBC0AAA252ADC28512BCF2A3EC07D2D4FED,
	VirtualVerticalLayoutGroup_CreateRow_m3C90AC320A2EE85B41D538117CA0ED6902BE1387,
	VirtualVerticalLayoutGroup__ctor_m800E17E18858B99EF206F601B0683EE083DED632,
	ServiceAttribute__ctor_mA14F2564D8999D7C252A02861F5607C5892B7A04,
	ServiceAttribute_get_ServiceType_m35E689F52314B2E75DC928C93F57542B87B6BE73,
	ServiceAttribute_set_ServiceType_mB6833B40B7BCD18F99496B7554C2789D6D150403,
	ServiceSelectorAttribute__ctor_m24FB871F409FDF7874828B1AED9F8780FF23216A,
	ServiceSelectorAttribute_get_ServiceType_m7AB870E8521DB39327F2D63D4AA0D82913222A99,
	ServiceSelectorAttribute_set_ServiceType_m052699D719B806EC4E7DF7A2A4CDB749A7125E1C,
	ServiceConstructorAttribute__ctor_m4B56EAEB9BC785547AFE4E0D4B22DAF223CA14A5,
	ServiceConstructorAttribute_get_ServiceType_m617E53723192215114FC5662AAAC2AD52D8BD7DF,
	ServiceConstructorAttribute_set_ServiceType_m106C3978300FFCA8ABD0FD2DF3BEE69A365CE72F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SRServiceManager_RuntimeInitialize_mA2DD11FCB7674E49DC852B6DAFB5DF7DCE402F43,
	NULL,
	SRServiceManager_get_IsLoading_mAB9771A9877F976529CAA69CC55C5E7A3234F511,
	NULL,
	SRServiceManager_GetService_mC5BDFEAF341A6BFFD1A7DFB93DA47A5C23199611,
	SRServiceManager_GetServiceInternal_mAEA0144ED6FFB9ED0EDF9372D362C338672F6593,
	NULL,
	SRServiceManager_HasService_mBD0514E7D95A2BB3360E897F82BED2C28B559FB0,
	NULL,
	SRServiceManager_RegisterService_m4AC9DFCFC7AF35437857FE72F1C68582752F0463,
	NULL,
	SRServiceManager_UnRegisterService_m61DF9F0EF65DD755593A256D3D395A6DE1C76A48,
	SRServiceManager_Awake_m2B3E6C70AE19DF4945892FEADB91ACC1212F00B8,
	SRServiceManager_UpdateStubs_mF6B39CCB5DEA52646B6A5C54E1E152588FCD7079,
	SRServiceManager_AutoCreateService_mE157A3B15BD3F00223A300413FFFC449B14311FC,
	SRServiceManager_OnApplicationQuit_mACE22B1A3565698F88870C5A2E2539E49EE9D2DF,
	SRServiceManager_DefaultServiceConstructor_mA71AFD97F247592A7B3DE22093580FE2C4198E35,
	SRServiceManager_ScanType_mDC6855A2B8391D6044D938AB3AAA37D0CC488E36,
	SRServiceManager_ScanTypeForSelectors_m6700A4064BBD2375E9BDD95E74E40BA39178D1DE,
	SRServiceManager_ScanTypeForConstructors_m283D2889D7C3221951C6A316298FFBB4BB1344C4,
	SRServiceManager_GetStaticMethods_m039692329095F3C20596DC0B35BED8074C4EE583,
	SRServiceManager__ctor_m11604B82B36AB410E385D298B08B4112AD31ED66,
	SRServiceManager__cctor_mAC957B834187D255B433370CB61DB2737F134385,
	MethodReference__ctor_m07B0F98AEC963534E7D335BC4B4CE3C52D3016FE,
	MethodReference_get_MethodName_m98F99BFCB03D3CD6BAC73DBFFD48D53C9CF8F65A,
	MethodReference_Invoke_m3691AD1FA58F3A22A06DB2083E4B7725A761A2A2,
	PropertyReference__ctor_m692424332F8F16E3B1189BB87E9ADFAA1901EB73,
	PropertyReference_get_PropertyName_mB1A49B3B71F620C7ECF07E7FC4EA19D1083128A0,
	PropertyReference_get_PropertyType_mFE7B0445B3CD9B46DA2B984235EB59FFAAC27670,
	PropertyReference_get_CanRead_m8068EF49C04842D54AF5DB1508759310CD12858F,
	PropertyReference_get_CanWrite_m8D86547A9E3B445A39F34E106B05D143F7823FF7,
	PropertyReference_GetValue_m8FA146D2E17DDF8D929471BA0F67593281374513,
	PropertyReference_SetValue_m30EAD407C8FFC5B7B691908E6E906A0A8B846F91,
	NULL,
	SRReflection_SetPropertyValue_mF03C292387F6E7E84AABA2EE6D2E3E003B55A563,
	SRReflection_GetPropertyValue_mC60B671028B099C294CF2F409999FE6530C434A0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TweenFunctions_Linear_mC810E7062E10BE516B1759EB9ACA08EF45650A71,
	TweenFunctions_ExpoEaseOut_m9A2570C13FC7521BE0A140E54F431F4C7FF4FC11,
	TweenFunctions_ExpoEaseIn_m592E27F97D1141803CC3510361587AA17C039732,
	TweenFunctions_ExpoEaseInOut_m0F856FD09E5B39C5AEAACB862BAE9AEA7224616D,
	TweenFunctions_ExpoEaseOutIn_mFBFDA01BD55B2852FC725D7A46C4C480DE6F4E13,
	TweenFunctions_CircEaseOut_m251A7E11A04A2043A2059115DDEE69605134CE1E,
	TweenFunctions_CircEaseIn_m4AFB94A8233DB303636526E0B598C45B99B053C4,
	TweenFunctions_CircEaseInOut_mE7362B67CE9481667D9C533F422D02E918BAE45E,
	TweenFunctions_CircEaseOutIn_mA34C044F7ED42F2D6B97DEE5DC46D50FC305C168,
	TweenFunctions_QuadEaseOut_m02FEDEE19194F4AB29054A64AB92A0082F503A10,
	TweenFunctions_QuadEaseIn_m87A817ABA9B8C52B490ACDC341756788074165A7,
	TweenFunctions_QuadEaseInOut_m1A34F2E1E505D5717B6638200680B7B44D048FAC,
	TweenFunctions_QuadEaseOutIn_m61D906A7805547DD42E624A8F2BEFDB18B9E956C,
	TweenFunctions_SineEaseOut_m4E6F03001C67D2A9B5886E2CA6908A68555BA14D,
	TweenFunctions_SineEaseIn_mC76AC4FAA88BA2312C894445B6A25B7CD483CF13,
	TweenFunctions_SineEaseInOut_mF7F42EF23B21B06EEF298D3BAECBBED9925C9EB6,
	TweenFunctions_SineEaseOutIn_m1EF0E54480B58582035391F53AD4E1945B6D9AC0,
	TweenFunctions_CubicEaseOut_m2073B536FC3CA4AAA2B41B7547172088471C0E52,
	TweenFunctions_CubicEaseIn_m559186F2937A35048C1A9F01BFFA4098D39013EC,
	TweenFunctions_CubicEaseInOut_m639E91B4861BCBABA7BCFD2B7B9A535354EF1E97,
	TweenFunctions_CubicEaseOutIn_mCBA25CA198484D911BED6A3B0152162413D52CC0,
	TweenFunctions_QuartEaseOut_m6C2217B96022BEDE88ABC7AB3B7701F07DD95D02,
	TweenFunctions_QuartEaseIn_m34D82E3F39522B81F89A61CD13A0825CD7FBBD84,
	TweenFunctions_QuartEaseInOut_m32FCE18DFC0FA73663EF55F2CC75E6DAB7C19CCD,
	TweenFunctions_QuartEaseOutIn_m29AD1668F17BE1A0D08A3DAC78B39639EB44E643,
	TweenFunctions_QuintEaseOut_m5E8DD4F3DD13ED1F3178DB7A82951BE981A4AD39,
	TweenFunctions_QuintEaseIn_m1D8A08E7FEA04F7AF91912EFBE6E9E337C06F63F,
	TweenFunctions_QuintEaseInOut_m43CBA5F8C99B920EFA9CE4BEB7A878C4C37A8E76,
	TweenFunctions_QuintEaseOutIn_m199FB2838381EC5A37EC7F4BF482C01E950BB0BE,
	TweenFunctions_ElasticEaseOut_m4CE4446E48780349641BC7151499D05EC9904603,
	TweenFunctions_ElasticEaseIn_m4329F14B508A15651C6CD8E9791A86D58C332B3F,
	TweenFunctions_ElasticEaseInOut_mDBE127AE286670CCF7122D1B3A4EE8386B43B85D,
	TweenFunctions_ElasticEaseOutIn_mB5C3D85C5FE9CDB1A01693C8A13F7BCDA49B9F53,
	TweenFunctions_BounceEaseOut_m638BEE2D29F0EDE7456C1274F77F324E51EDEBD6,
	TweenFunctions_BounceEaseIn_m2AD31FAB8C88D5B83DFF86CF0066D2C7755D419E,
	TweenFunctions_BounceEaseInOut_m307094CCBCDC9306668AF617B0166625668DDAEF,
	TweenFunctions_BounceEaseOutIn_mA3026ED12B2C9D3642D48DD57571863D98FA5355,
	TweenFunctions_BackEaseOut_m3CD24966A426454130BF06FD62B01BA2AA429E53,
	TweenFunctions_BackEaseIn_m660FC09B47CB328DF23C9F1A4153A8A92ABE71E1,
	TweenFunctions_BackEaseInOut_mA1AA2A432716FEAEC9A39D0205AC7C0B8F5E06CB,
	TweenFunctions_BackEaseOutIn_mC2F16AED2728CD5EAE6B164CC5FB8DB56329BE8C,
	Parser__ctor_mDC4C8F62390F503AFDF6DE21EF54355FD66350CC,
	Parser_get_PeekChar_m24EC2879535CFB39D91AC36F8E084E5D2DD065AA,
	Parser_get_NextChar_m23E7FA2C13886E96D6F1D8A736E9342B4197E2D5,
	Parser_get_NextWord_mA26C4E933B089119C8045C50AD65091E539AF1DC,
	Parser_get_NextToken_m33BB77CBDA555C19C72B9BAEB69ABE8AF1C20125,
	Parser_Dispose_m02B94AD40A7D96A395B0B140C0E553B0E38C03F9,
	Parser_IsWordBreak_mC57DCDD911BAE1780D4F2F11D88ED9A178E4B08C,
	Parser_Parse_m34902A9AD5F31EE8A59559D3819FDD2AA90E2E5A,
	Parser_ParseObject_mC607B1D8BB8296556209E62476CE5180A5C17523,
	Parser_ParseArray_mA7C900B7AE88A2104F286FAAC588985BB6B2C27A,
	Parser_ParseValue_mDF67032D6AAD9D95F365E8304D661DC511C39E99,
	Parser_ParseByToken_m28BB6D085467B250187A0BDDE692451759F600D1,
	Parser_ParseString_mB8939476A75A6DBC61022C058A89EDF6A46A0722,
	Parser_ParseNumber_m618C3CB577ECD0F6458CF7F5A7B10172638BDBFF,
	Parser_EatWhitespace_m03A9308FD9D8DA8977BAA9952B8FB5BBB3F81BBF,
	Serializer__ctor_m9B5A9593E0604A2CBDBA4D8517FD41EB9F2AC76B,
	Serializer_Serialize_mE63B1CE00239A4A1A0028436848C5AB868827FFA,
	Serializer_SerializeValue_m18EA20A57F6EA7FCA8B98DAE1F855BE40DFB27D5,
	Serializer_SerializeObject_m08AD9B0227F3E5F499F86082DFC0BC4429CE409B,
	Serializer_SerializeArray_m51C08AD634123CB6174F656329543578E87D97EF,
	Serializer_SerializeString_mF10F5612F3D99187806CB52EB252F39CFA3AE7AA,
	Serializer_SerializeOther_m3004D42CFAABDC519BECDB626CB977B5473631E7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CWaitForSecondsRealTimeU3Ed__0__ctor_m73E2FBE1704C4105F34AA572B0574C03CDC89439,
	U3CWaitForSecondsRealTimeU3Ed__0_System_IDisposable_Dispose_m5CAB778FD3F6AE903EDA2353BCF28D858501341F,
	U3CWaitForSecondsRealTimeU3Ed__0_MoveNext_m948A4CFAA30E74608CC99A2D1C9EDD7169908BED,
	U3CWaitForSecondsRealTimeU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5778F1553314F5F92FE60C975B25DCED7C344EDA,
	U3CWaitForSecondsRealTimeU3Ed__0_System_Collections_IEnumerator_Reset_m33F1F2F19567C3B1EF2881C22D547F36048D43B0,
	U3CWaitForSecondsRealTimeU3Ed__0_System_Collections_IEnumerator_get_Current_m2FF811A3E815EE35906E29C8282C066798105C4B,
	U3CGetChildrenU3Ed__0__ctor_mDEF50FEB9E66802DD490B5422F15CD2DC4E3B3F8,
	U3CGetChildrenU3Ed__0_System_IDisposable_Dispose_m0B1A7F813FF9E2317C3BA3925B748E9D244116AB,
	U3CGetChildrenU3Ed__0_MoveNext_mE575E00B8FB0CC6A9B426560165F9C3C10D040BB,
	U3CGetChildrenU3Ed__0_System_Collections_Generic_IEnumeratorU3CUnityEngine_TransformU3E_get_Current_m430FDEADF6193824182E19CC52BDE70A6CCE4A55,
	U3CGetChildrenU3Ed__0_System_Collections_IEnumerator_Reset_m4174D49F98727E3BCECE0D1C8F37F2CCEAF3DADA,
	U3CGetChildrenU3Ed__0_System_Collections_IEnumerator_get_Current_m24DFD4C121BF4705F14B97641F7CD8F6F5FBF1DB,
	U3CGetChildrenU3Ed__0_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_mF26E99781AC869019E97030C859A7D73761815A2,
	U3CGetChildrenU3Ed__0_System_Collections_IEnumerable_GetEnumerator_m3D2E7C0F1581576E88C23189F1822350FB387C42,
	SpinEvent__ctor_m0923DEA7C7A33F5E304538DE7DB500B789FED39F,
	SelectedItemChangedEvent__ctor_m28C580B3711D492C54806427434000F705A63EC4,
	Row__ctor_m6835D1EF80456B05B33F7FB15D30C3D0F80B720B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Service__ctor_mDAC8BE1C6A4FF294363B2C1F25AE870FB557E540,
	ServiceStub_ToString_m8528A45DDB4B334B92156E73C8AB6525A954EE8E,
	ServiceStub__ctor_mE380B254D8717A970EFB17CEF64F509F0D1C04DD,
	U3CU3Ec__cctor_mEFC8CF876834882935159BEFD7E1BACDADD5248C,
	U3CU3Ec__ctor_mCE1B9A2F219EC13CC62FC04E9CC0D5A96C849A56,
	U3CU3Ec_U3CUpdateStubsU3Eb__22_0_m6827063FECB265D40BD6E5E5D0ABF1C0A0EC6A19,
	U3CU3Ec__DisplayClass27_0__ctor_mDCDAA5E0EEF3463AA882716FFFEDA9EB822F072A,
	U3CU3Ec__DisplayClass27_0_U3CScanTypeForSelectorsU3Eb__0_m4A4701A42E4000F1506410CE67F8FA779C0919F1,
	U3CU3Ec__DisplayClass28_0__ctor_m219AC9C6AB2E791A0865015BB3D0B4AECB771552,
	U3CU3Ec__DisplayClass28_0_U3CScanTypeForConstructorsU3Eb__0_m4DC746BD18B5506A901A6857079C19AB3363D6C6,
	U3CU3Ec__DisplayClass28_0_U3CScanTypeForConstructorsU3Eb__1_mABFC079C7443EEDBA3BA7D4FFD13A9C2749F0AAB,
};
static const int32_t s_InvokerIndices[489] = 
{
	49,
	859,
	186,
	2238,
	186,
	2238,
	154,
	925,
	-1,
	0,
	-1,
	1938,
	445,
	2201,
	2239,
	2240,
	2241,
	1588,
	1565,
	1567,
	444,
	1588,
	168,
	445,
	445,
	2242,
	0,
	0,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	27,
	88,
	27,
	88,
	23,
	31,
	23,
	89,
	31,
	89,
	31,
	89,
	31,
	23,
	26,
	625,
	109,
	0,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	97,
	444,
	444,
	243,
	1589,
	-1,
	-1,
	-1,
	-1,
	-1,
	373,
	373,
	-1,
	-1,
	-1,
	1,
	0,
	154,
	1,
	137,
	137,
	137,
	154,
	28,
	0,
	23,
	3,
	731,
	731,
	731,
	731,
	731,
	731,
	10,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	731,
	731,
	731,
	731,
	10,
	731,
	731,
	23,
	23,
	23,
	731,
	731,
	10,
	23,
	731,
	731,
	731,
	731,
	10,
	23,
	731,
	26,
	26,
	26,
	23,
	89,
	731,
	337,
	23,
	731,
	731,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	14,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	10,
	731,
	23,
	23,
	14,
	26,
	14,
	26,
	26,
	26,
	32,
	32,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	1442,
	23,
	31,
	23,
	14,
	26,
	23,
	23,
	31,
	14,
	23,
	23,
	23,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	148,
	23,
	26,
	23,
	23,
	89,
	89,
	89,
	89,
	23,
	23,
	23,
	23,
	2243,
	2244,
	2245,
	731,
	23,
	26,
	14,
	26,
	14,
	26,
	731,
	26,
	23,
	1442,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	14,
	89,
	89,
	731,
	34,
	26,
	62,
	14,
	23,
	26,
	14,
	26,
	26,
	14,
	26,
	26,
	14,
	26,
	89,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3,
	-1,
	49,
	-1,
	0,
	0,
	-1,
	114,
	-1,
	137,
	-1,
	154,
	23,
	23,
	28,
	23,
	1,
	26,
	137,
	137,
	0,
	23,
	3,
	27,
	14,
	28,
	27,
	14,
	14,
	89,
	89,
	14,
	26,
	-1,
	186,
	1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	2201,
	26,
	238,
	238,
	14,
	10,
	23,
	48,
	0,
	14,
	14,
	14,
	34,
	14,
	14,
	23,
	23,
	0,
	26,
	26,
	26,
	26,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	14,
	23,
	3,
	23,
	28,
	23,
	9,
	23,
	9,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[26] = 
{
	{ 0x02000007, { 2, 25 } },
	{ 0x02000030, { 47, 5 } },
	{ 0x02000031, { 54, 8 } },
	{ 0x02000032, { 66, 2 } },
	{ 0x02000038, { 78, 4 } },
	{ 0x02000039, { 82, 4 } },
	{ 0x0200003F, { 27, 3 } },
	{ 0x02000049, { 52, 2 } },
	{ 0x0200004A, { 62, 4 } },
	{ 0x06000009, { 0, 1 } },
	{ 0x0600000B, { 1, 1 } },
	{ 0x06000061, { 30, 2 } },
	{ 0x06000062, { 32, 3 } },
	{ 0x06000063, { 35, 2 } },
	{ 0x06000064, { 37, 2 } },
	{ 0x06000065, { 39, 2 } },
	{ 0x06000068, { 41, 2 } },
	{ 0x06000069, { 43, 2 } },
	{ 0x0600006A, { 45, 2 } },
	{ 0x0600014D, { 68, 1 } },
	{ 0x0600014F, { 69, 2 } },
	{ 0x06000152, { 71, 1 } },
	{ 0x06000154, { 72, 1 } },
	{ 0x06000156, { 73, 1 } },
	{ 0x0600016D, { 74, 2 } },
	{ 0x06000170, { 76, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[86] = 
{
	{ (Il2CppRGCTXDataType)3, 25748 },
	{ (Il2CppRGCTXDataType)3, 25749 },
	{ (Il2CppRGCTXDataType)2, 29582 },
	{ (Il2CppRGCTXDataType)3, 25750 },
	{ (Il2CppRGCTXDataType)3, 25751 },
	{ (Il2CppRGCTXDataType)3, 25752 },
	{ (Il2CppRGCTXDataType)2, 29583 },
	{ (Il2CppRGCTXDataType)2, 34871 },
	{ (Il2CppRGCTXDataType)3, 25753 },
	{ (Il2CppRGCTXDataType)3, 25754 },
	{ (Il2CppRGCTXDataType)3, 25755 },
	{ (Il2CppRGCTXDataType)3, 25756 },
	{ (Il2CppRGCTXDataType)3, 25757 },
	{ (Il2CppRGCTXDataType)3, 25758 },
	{ (Il2CppRGCTXDataType)3, 25759 },
	{ (Il2CppRGCTXDataType)3, 25760 },
	{ (Il2CppRGCTXDataType)3, 25761 },
	{ (Il2CppRGCTXDataType)3, 25762 },
	{ (Il2CppRGCTXDataType)3, 25763 },
	{ (Il2CppRGCTXDataType)3, 25764 },
	{ (Il2CppRGCTXDataType)3, 25765 },
	{ (Il2CppRGCTXDataType)2, 29580 },
	{ (Il2CppRGCTXDataType)2, 29584 },
	{ (Il2CppRGCTXDataType)3, 25766 },
	{ (Il2CppRGCTXDataType)2, 29585 },
	{ (Il2CppRGCTXDataType)3, 25767 },
	{ (Il2CppRGCTXDataType)3, 25768 },
	{ (Il2CppRGCTXDataType)3, 25769 },
	{ (Il2CppRGCTXDataType)3, 25770 },
	{ (Il2CppRGCTXDataType)2, 29592 },
	{ (Il2CppRGCTXDataType)1, 29618 },
	{ (Il2CppRGCTXDataType)2, 29618 },
	{ (Il2CppRGCTXDataType)3, 25771 },
	{ (Il2CppRGCTXDataType)2, 29619 },
	{ (Il2CppRGCTXDataType)3, 25772 },
	{ (Il2CppRGCTXDataType)3, 25773 },
	{ (Il2CppRGCTXDataType)2, 34872 },
	{ (Il2CppRGCTXDataType)3, 25774 },
	{ (Il2CppRGCTXDataType)2, 34873 },
	{ (Il2CppRGCTXDataType)3, 25775 },
	{ (Il2CppRGCTXDataType)2, 34874 },
	{ (Il2CppRGCTXDataType)2, 34875 },
	{ (Il2CppRGCTXDataType)2, 29622 },
	{ (Il2CppRGCTXDataType)2, 34876 },
	{ (Il2CppRGCTXDataType)3, 25776 },
	{ (Il2CppRGCTXDataType)2, 34877 },
	{ (Il2CppRGCTXDataType)2, 29626 },
	{ (Il2CppRGCTXDataType)3, 25777 },
	{ (Il2CppRGCTXDataType)2, 34878 },
	{ (Il2CppRGCTXDataType)3, 25778 },
	{ (Il2CppRGCTXDataType)3, 25779 },
	{ (Il2CppRGCTXDataType)2, 29729 },
	{ (Il2CppRGCTXDataType)3, 25780 },
	{ (Il2CppRGCTXDataType)3, 25781 },
	{ (Il2CppRGCTXDataType)2, 29740 },
	{ (Il2CppRGCTXDataType)3, 25782 },
	{ (Il2CppRGCTXDataType)3, 25783 },
	{ (Il2CppRGCTXDataType)3, 25784 },
	{ (Il2CppRGCTXDataType)2, 34879 },
	{ (Il2CppRGCTXDataType)3, 25785 },
	{ (Il2CppRGCTXDataType)3, 25786 },
	{ (Il2CppRGCTXDataType)2, 29738 },
	{ (Il2CppRGCTXDataType)2, 29746 },
	{ (Il2CppRGCTXDataType)3, 25787 },
	{ (Il2CppRGCTXDataType)3, 25788 },
	{ (Il2CppRGCTXDataType)3, 25789 },
	{ (Il2CppRGCTXDataType)3, 25790 },
	{ (Il2CppRGCTXDataType)3, 25791 },
	{ (Il2CppRGCTXDataType)1, 34881 },
	{ (Il2CppRGCTXDataType)1, 29750 },
	{ (Il2CppRGCTXDataType)2, 29750 },
	{ (Il2CppRGCTXDataType)1, 34882 },
	{ (Il2CppRGCTXDataType)1, 34883 },
	{ (Il2CppRGCTXDataType)1, 34884 },
	{ (Il2CppRGCTXDataType)1, 29774 },
	{ (Il2CppRGCTXDataType)2, 29774 },
	{ (Il2CppRGCTXDataType)1, 29778 },
	{ (Il2CppRGCTXDataType)2, 29778 },
	{ (Il2CppRGCTXDataType)2, 29783 },
	{ (Il2CppRGCTXDataType)2, 29781 },
	{ (Il2CppRGCTXDataType)1, 29781 },
	{ (Il2CppRGCTXDataType)3, 25792 },
	{ (Il2CppRGCTXDataType)2, 29788 },
	{ (Il2CppRGCTXDataType)2, 29786 },
	{ (Il2CppRGCTXDataType)1, 29786 },
	{ (Il2CppRGCTXDataType)3, 25793 },
};
extern const Il2CppCodeGenModule g_StompyRobot_SRFCodeGenModule;
const Il2CppCodeGenModule g_StompyRobot_SRFCodeGenModule = 
{
	"StompyRobot.SRF.dll",
	489,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	26,
	s_rgctxIndices,
	86,
	s_rgctxValues,
	NULL,
};
