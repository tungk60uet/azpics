﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Int32 NativeGallery::_NativeGallery_RequestPermission(System.Int32,System.Int32)
extern void NativeGallery__NativeGallery_RequestPermission_mE2866C7E024A2AC7D8FE6A73B182BAD5DB1DE21E (void);
// 0x00000002 System.Void NativeGallery::_NativeGallery_ImageWriteToAlbum(System.String,System.String,System.Int32)
extern void NativeGallery__NativeGallery_ImageWriteToAlbum_m4EC1C752C20862A57324FE17F503726F0F2DDA9E (void);
// 0x00000003 System.Void NativeGallery::_NativeGallery_VideoWriteToAlbum(System.String,System.String,System.Int32)
extern void NativeGallery__NativeGallery_VideoWriteToAlbum_mE09AAB08D7E3F25E2ACF72690B592975ECF077B1 (void);
// 0x00000004 System.Void NativeGallery::_NativeGallery_PickMedia(System.String,System.Int32,System.Int32,System.Int32)
extern void NativeGallery__NativeGallery_PickMedia_mF6981DB065D940CAB143FEA00647C5C745E08806 (void);
// 0x00000005 System.String NativeGallery::get_SelectedMediaPath()
extern void NativeGallery_get_SelectedMediaPath_m874A1EBCB51D9C28E066F7CBA95A7E0BA7485798 (void);
// 0x00000006 NativeGallery/Permission NativeGallery::RequestPermission(NativeGallery/PermissionType)
extern void NativeGallery_RequestPermission_mEADB867EE50805E34462DBB0D1D342DB237E7941 (void);
// 0x00000007 NativeGallery/Permission NativeGallery::SaveImageToGallery(System.Byte[],System.String,System.String,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveImageToGallery_m53F332A434FCC54EC7A4EB5A7D56F5A8B564187F (void);
// 0x00000008 NativeGallery/Permission NativeGallery::GetImageFromGallery(NativeGallery/MediaPickCallback,System.String,System.String)
extern void NativeGallery_GetImageFromGallery_m34B7FD256E30889952404A3053483338642C88BC (void);
// 0x00000009 System.Boolean NativeGallery::IsMediaPickerBusy()
extern void NativeGallery_IsMediaPickerBusy_mA4FED4EA9BB612EB41E8330225CA2E6CB6B47C47 (void);
// 0x0000000A NativeGallery/Permission NativeGallery::SaveToGallery(System.Byte[],System.String,System.String,NativeGallery/MediaType,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveToGallery_mE435C19013830F12598B9FB8D164D112D44CF228 (void);
// 0x0000000B System.Void NativeGallery::SaveToGalleryInternal(System.String,System.String,NativeGallery/MediaType,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveToGalleryInternal_mE87329465A09F55110F283DB93BA4E3507A58161 (void);
// 0x0000000C System.String NativeGallery::GetTemporarySavePath(System.String)
extern void NativeGallery_GetTemporarySavePath_m114A975542060AFA440A6BB07214AEDDE63810D7 (void);
// 0x0000000D NativeGallery/Permission NativeGallery::GetMediaFromGallery(NativeGallery/MediaPickCallback,NativeGallery/MediaType,System.String,System.String)
extern void NativeGallery_GetMediaFromGallery_m2FCEBD9CE43D43AF37D45B9B46CE6A20D5269E61 (void);
// 0x0000000E System.Void NativeGallery/MediaSaveCallback::.ctor(System.Object,System.IntPtr)
extern void MediaSaveCallback__ctor_m3E8C46DE2ED46BC523E207FBEEF7A7AB8FC16A80 (void);
// 0x0000000F System.Void NativeGallery/MediaSaveCallback::Invoke(System.Boolean,System.String)
extern void MediaSaveCallback_Invoke_mDA047FC947FF711EE6134C2EC6408AE9F530A7D6 (void);
// 0x00000010 System.IAsyncResult NativeGallery/MediaSaveCallback::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern void MediaSaveCallback_BeginInvoke_mC904FBB03BA6A2CAAD0E2D2F5115CB0585797703 (void);
// 0x00000011 System.Void NativeGallery/MediaSaveCallback::EndInvoke(System.IAsyncResult)
extern void MediaSaveCallback_EndInvoke_m19447DE5B70175FA8AE99BCB8B24E70878CDA0F7 (void);
// 0x00000012 System.Void NativeGallery/MediaPickCallback::.ctor(System.Object,System.IntPtr)
extern void MediaPickCallback__ctor_mB67FD460D95DAF27F370271299F24153E52BAC56 (void);
// 0x00000013 System.Void NativeGallery/MediaPickCallback::Invoke(System.String)
extern void MediaPickCallback_Invoke_mB9D4ED65BEFC10BB41CDBE411DD062917A2AFC53 (void);
// 0x00000014 System.IAsyncResult NativeGallery/MediaPickCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MediaPickCallback_BeginInvoke_m5CFB5F9A46EB3164B4F3CC78D57B9BA0F7586B9D (void);
// 0x00000015 System.Void NativeGallery/MediaPickCallback::EndInvoke(System.IAsyncResult)
extern void MediaPickCallback_EndInvoke_mE2D644EDEF1AD085613C818BB53794E29DB85F8A (void);
// 0x00000016 System.Void NativeGallery/MediaPickMultipleCallback::.ctor(System.Object,System.IntPtr)
extern void MediaPickMultipleCallback__ctor_m29943F178B930B2B459E7B79E9B10EDB676397E6 (void);
// 0x00000017 System.Void NativeGallery/MediaPickMultipleCallback::Invoke(System.String[])
extern void MediaPickMultipleCallback_Invoke_m0EA4707EDB3DDDCCFEA1761DE00CEB4DCA9F1B27 (void);
// 0x00000018 System.IAsyncResult NativeGallery/MediaPickMultipleCallback::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
extern void MediaPickMultipleCallback_BeginInvoke_m18FDD5F327F34EBDCD99641A397FF698119EB948 (void);
// 0x00000019 System.Void NativeGallery/MediaPickMultipleCallback::EndInvoke(System.IAsyncResult)
extern void MediaPickMultipleCallback_EndInvoke_mBF22617FF36078830D13FA6887BBB6E5DD8E5086 (void);
// 0x0000001A System.Boolean NativeGalleryNamespace.NGMediaReceiveCallbackiOS::get_IsBusy()
extern void NGMediaReceiveCallbackiOS_get_IsBusy_m0DB54BD943EEFF7DAAE802DD19A8EECC674F0791 (void);
// 0x0000001B System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::set_IsBusy(System.Boolean)
extern void NGMediaReceiveCallbackiOS_set_IsBusy_m32797935FA0DD9BE7B745135A1172912C5FDEDBC (void);
// 0x0000001C System.Int32 NativeGalleryNamespace.NGMediaReceiveCallbackiOS::_NativeGallery_IsMediaPickerBusy()
extern void NGMediaReceiveCallbackiOS__NativeGallery_IsMediaPickerBusy_m805CF5392C7263D89877031D8E505FEF2312D677 (void);
// 0x0000001D System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::Initialize(NativeGallery/MediaPickCallback,NativeGallery/MediaPickMultipleCallback)
extern void NGMediaReceiveCallbackiOS_Initialize_m58DE17845F6D67664633ACD8A544343C4C690562 (void);
// 0x0000001E System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::Update()
extern void NGMediaReceiveCallbackiOS_Update_m5EB081D46A197C8255A1E5BA4AD9C2B4F253FF28 (void);
// 0x0000001F System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::OnMediaReceived(System.String)
extern void NGMediaReceiveCallbackiOS_OnMediaReceived_m18A7ADAFE4F0EE27288A531203D4718CE351DB86 (void);
// 0x00000020 System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::OnMultipleMediaReceived(System.String)
extern void NGMediaReceiveCallbackiOS_OnMultipleMediaReceived_mB0889CC69220B73CFBB89BA8EA92F46390B7B6F1 (void);
// 0x00000021 System.String[] NativeGalleryNamespace.NGMediaReceiveCallbackiOS::SplitPaths(System.String)
extern void NGMediaReceiveCallbackiOS_SplitPaths_m2EAD016045C615643062CA558B828E7102C7BFE6 (void);
// 0x00000022 System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::.ctor()
extern void NGMediaReceiveCallbackiOS__ctor_m2BB609B5D1D5A4E3A20032B4557733FC4E6FD90D (void);
// 0x00000023 System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::Initialize(NativeGallery/MediaSaveCallback)
extern void NGMediaSaveCallbackiOS_Initialize_m24F036ED03E6BF8D39CAEE8B87E735389716F3D5 (void);
// 0x00000024 System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::OnMediaSaveCompleted(System.String)
extern void NGMediaSaveCallbackiOS_OnMediaSaveCompleted_m60B7E627AF1C9C94BE2072C6332D48337C3B59D4 (void);
// 0x00000025 System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::OnMediaSaveFailed(System.String)
extern void NGMediaSaveCallbackiOS_OnMediaSaveFailed_mFE1940820CD915942B8CBA956CE37698C49FE06C (void);
// 0x00000026 System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::.ctor()
extern void NGMediaSaveCallbackiOS__ctor_mB8D10D957811AEF601ED2B4FD19104B4CAC33554 (void);
static Il2CppMethodPointer s_methodPointers[38] = 
{
	NativeGallery__NativeGallery_RequestPermission_mE2866C7E024A2AC7D8FE6A73B182BAD5DB1DE21E,
	NativeGallery__NativeGallery_ImageWriteToAlbum_m4EC1C752C20862A57324FE17F503726F0F2DDA9E,
	NativeGallery__NativeGallery_VideoWriteToAlbum_mE09AAB08D7E3F25E2ACF72690B592975ECF077B1,
	NativeGallery__NativeGallery_PickMedia_mF6981DB065D940CAB143FEA00647C5C745E08806,
	NativeGallery_get_SelectedMediaPath_m874A1EBCB51D9C28E066F7CBA95A7E0BA7485798,
	NativeGallery_RequestPermission_mEADB867EE50805E34462DBB0D1D342DB237E7941,
	NativeGallery_SaveImageToGallery_m53F332A434FCC54EC7A4EB5A7D56F5A8B564187F,
	NativeGallery_GetImageFromGallery_m34B7FD256E30889952404A3053483338642C88BC,
	NativeGallery_IsMediaPickerBusy_mA4FED4EA9BB612EB41E8330225CA2E6CB6B47C47,
	NativeGallery_SaveToGallery_mE435C19013830F12598B9FB8D164D112D44CF228,
	NativeGallery_SaveToGalleryInternal_mE87329465A09F55110F283DB93BA4E3507A58161,
	NativeGallery_GetTemporarySavePath_m114A975542060AFA440A6BB07214AEDDE63810D7,
	NativeGallery_GetMediaFromGallery_m2FCEBD9CE43D43AF37D45B9B46CE6A20D5269E61,
	MediaSaveCallback__ctor_m3E8C46DE2ED46BC523E207FBEEF7A7AB8FC16A80,
	MediaSaveCallback_Invoke_mDA047FC947FF711EE6134C2EC6408AE9F530A7D6,
	MediaSaveCallback_BeginInvoke_mC904FBB03BA6A2CAAD0E2D2F5115CB0585797703,
	MediaSaveCallback_EndInvoke_m19447DE5B70175FA8AE99BCB8B24E70878CDA0F7,
	MediaPickCallback__ctor_mB67FD460D95DAF27F370271299F24153E52BAC56,
	MediaPickCallback_Invoke_mB9D4ED65BEFC10BB41CDBE411DD062917A2AFC53,
	MediaPickCallback_BeginInvoke_m5CFB5F9A46EB3164B4F3CC78D57B9BA0F7586B9D,
	MediaPickCallback_EndInvoke_mE2D644EDEF1AD085613C818BB53794E29DB85F8A,
	MediaPickMultipleCallback__ctor_m29943F178B930B2B459E7B79E9B10EDB676397E6,
	MediaPickMultipleCallback_Invoke_m0EA4707EDB3DDDCCFEA1761DE00CEB4DCA9F1B27,
	MediaPickMultipleCallback_BeginInvoke_m18FDD5F327F34EBDCD99641A397FF698119EB948,
	MediaPickMultipleCallback_EndInvoke_mBF22617FF36078830D13FA6887BBB6E5DD8E5086,
	NGMediaReceiveCallbackiOS_get_IsBusy_m0DB54BD943EEFF7DAAE802DD19A8EECC674F0791,
	NGMediaReceiveCallbackiOS_set_IsBusy_m32797935FA0DD9BE7B745135A1172912C5FDEDBC,
	NGMediaReceiveCallbackiOS__NativeGallery_IsMediaPickerBusy_m805CF5392C7263D89877031D8E505FEF2312D677,
	NGMediaReceiveCallbackiOS_Initialize_m58DE17845F6D67664633ACD8A544343C4C690562,
	NGMediaReceiveCallbackiOS_Update_m5EB081D46A197C8255A1E5BA4AD9C2B4F253FF28,
	NGMediaReceiveCallbackiOS_OnMediaReceived_m18A7ADAFE4F0EE27288A531203D4718CE351DB86,
	NGMediaReceiveCallbackiOS_OnMultipleMediaReceived_mB0889CC69220B73CFBB89BA8EA92F46390B7B6F1,
	NGMediaReceiveCallbackiOS_SplitPaths_m2EAD016045C615643062CA558B828E7102C7BFE6,
	NGMediaReceiveCallbackiOS__ctor_m2BB609B5D1D5A4E3A20032B4557733FC4E6FD90D,
	NGMediaSaveCallbackiOS_Initialize_m24F036ED03E6BF8D39CAEE8B87E735389716F3D5,
	NGMediaSaveCallbackiOS_OnMediaSaveCompleted_m60B7E627AF1C9C94BE2072C6332D48337C3B59D4,
	NGMediaSaveCallbackiOS_OnMediaSaveFailed_mFE1940820CD915942B8CBA956CE37698C49FE06C,
	NGMediaSaveCallbackiOS__ctor_mB8D10D957811AEF601ED2B4FD19104B4CAC33554,
};
static const int32_t s_InvokerIndices[38] = 
{
	168,
	195,
	195,
	2252,
	4,
	21,
	788,
	178,
	49,
	2253,
	2254,
	0,
	2255,
	124,
	88,
	2256,
	26,
	124,
	26,
	205,
	26,
	124,
	26,
	205,
	26,
	49,
	859,
	106,
	137,
	23,
	26,
	26,
	28,
	23,
	154,
	26,
	26,
	23,
};
extern const Il2CppCodeGenModule g_NativeGallery_RuntimeCodeGenModule;
const Il2CppCodeGenModule g_NativeGallery_RuntimeCodeGenModule = 
{
	"NativeGallery.Runtime.dll",
	38,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
