﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.Color Sirenix.Utilities.ColorExtensions::Lerp(UnityEngine.Color[],System.Single)
extern void ColorExtensions_Lerp_m8720CBDC453206021255DC41EDCAD80778A120A4 (void);
// 0x00000002 UnityEngine.Color Sirenix.Utilities.ColorExtensions::MoveTowards(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void ColorExtensions_MoveTowards_m73DA04308576A388045C312344E9BA58748A1D42 (void);
// 0x00000003 System.Boolean Sirenix.Utilities.ColorExtensions::TryParseString(System.String,UnityEngine.Color&)
extern void ColorExtensions_TryParseString_m9F4F647058B0FE2103A65C422C5F746EE4527425 (void);
// 0x00000004 System.String Sirenix.Utilities.ColorExtensions::ToCSharpColor(UnityEngine.Color)
extern void ColorExtensions_ToCSharpColor_mD0578C24EAEAA0325D0FF17321A873CAC7D79990 (void);
// 0x00000005 UnityEngine.Color Sirenix.Utilities.ColorExtensions::Pow(UnityEngine.Color,System.Single)
extern void ColorExtensions_Pow_m149A66B452C04A3131BF96BC13B2CFF9FB22E780 (void);
// 0x00000006 UnityEngine.Color Sirenix.Utilities.ColorExtensions::NormalizeRGB(UnityEngine.Color)
extern void ColorExtensions_NormalizeRGB_mD5758657473D885D209F8D2B2FE261294691080E (void);
// 0x00000007 System.String Sirenix.Utilities.ColorExtensions::TrimFloat(System.Single)
extern void ColorExtensions_TrimFloat_m2438FEE187E7E855CCC4F1E5F2C63304E6F1B8C8 (void);
// 0x00000008 System.Void Sirenix.Utilities.ColorExtensions::.cctor()
extern void ColorExtensions__cctor_m2F0F9F8259A818CD849C502A15080D3842C1B423 (void);
// 0x00000009 System.Func`1<TResult> Sirenix.Utilities.DelegateExtensions::Memoize(System.Func`1<TResult>)
// 0x0000000A System.Func`2<T,TResult> Sirenix.Utilities.DelegateExtensions::Memoize(System.Func`2<T,TResult>)
// 0x0000000B System.Void Sirenix.Utilities.DelegateExtensions/<>c__DisplayClass0_0`1::.ctor()
// 0x0000000C TResult Sirenix.Utilities.DelegateExtensions/<>c__DisplayClass0_0`1::<Memoize>b__0()
// 0x0000000D System.Void Sirenix.Utilities.DelegateExtensions/<>c__DisplayClass1_0`2::.ctor()
// 0x0000000E TResult Sirenix.Utilities.DelegateExtensions/<>c__DisplayClass1_0`2::<Memoize>b__0(T)
// 0x0000000F System.Boolean Sirenix.Utilities.FieldInfoExtensions::IsAliasField(System.Reflection.FieldInfo)
extern void FieldInfoExtensions_IsAliasField_mB4C68639DCBB7BD669EFBDECD94659CF9978584A (void);
// 0x00000010 System.Reflection.FieldInfo Sirenix.Utilities.FieldInfoExtensions::DeAliasField(System.Reflection.FieldInfo,System.Boolean)
extern void FieldInfoExtensions_DeAliasField_m7635D5FE723F2A9DF12E18D1167FB981F106650E (void);
// 0x00000011 Sirenix.Utilities.GarbageFreeIterators/ListIterator`1<T> Sirenix.Utilities.GarbageFreeIterators::GFIterator(System.Collections.Generic.List`1<T>)
// 0x00000012 Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2<T1,T2> Sirenix.Utilities.GarbageFreeIterators::GFIterator(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x00000013 Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2<T1,T2> Sirenix.Utilities.GarbageFreeIterators::GFValueIterator(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x00000014 Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1<T> Sirenix.Utilities.GarbageFreeIterators::GFIterator(System.Collections.Generic.HashSet`1<T>)
// 0x00000015 System.Void Sirenix.Utilities.GarbageFreeIterators/ListIterator`1::.ctor(System.Collections.Generic.List`1<T>)
// 0x00000016 Sirenix.Utilities.GarbageFreeIterators/ListIterator`1<T> Sirenix.Utilities.GarbageFreeIterators/ListIterator`1::GetEnumerator()
// 0x00000017 T Sirenix.Utilities.GarbageFreeIterators/ListIterator`1::get_Current()
// 0x00000018 System.Boolean Sirenix.Utilities.GarbageFreeIterators/ListIterator`1::MoveNext()
// 0x00000019 System.Void Sirenix.Utilities.GarbageFreeIterators/ListIterator`1::Dispose()
// 0x0000001A System.Void Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000001B Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1<T> Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1::GetEnumerator()
// 0x0000001C T Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1::get_Current()
// 0x0000001D System.Boolean Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1::MoveNext()
// 0x0000001E System.Void Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1::Dispose()
// 0x0000001F System.Void Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2::.ctor(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x00000020 Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2<T1,T2> Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2::GetEnumerator()
// 0x00000021 System.Collections.Generic.KeyValuePair`2<T1,T2> Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2::get_Current()
// 0x00000022 System.Boolean Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2::MoveNext()
// 0x00000023 System.Void Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2::Dispose()
// 0x00000024 System.Void Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::.ctor(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x00000025 Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2<T1,T2> Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::GetEnumerator()
// 0x00000026 T2 Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::get_Current()
// 0x00000027 System.Boolean Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::MoveNext()
// 0x00000028 System.Void Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::Dispose()
// 0x00000029 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::Examine(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
// 0x0000002A System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::ForEach(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
// 0x0000002B System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::ForEach(System.Collections.Generic.IEnumerable`1<T>,System.Action`2<T,System.Int32>)
// 0x0000002C System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::Convert(System.Collections.IEnumerable,System.Func`2<System.Object,T>)
// 0x0000002D System.Collections.Generic.HashSet`1<T> Sirenix.Utilities.LinqExtensions::ToHashSet(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000002E System.Collections.Generic.HashSet`1<T> Sirenix.Utilities.LinqExtensions::ToHashSet(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000002F Sirenix.Utilities.ImmutableList`1<T> Sirenix.Utilities.LinqExtensions::ToImmutableList(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000030 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependWith(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<T>)
// 0x00000031 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependWith(System.Collections.Generic.IEnumerable`1<T>,T)
// 0x00000032 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependWith(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000033 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Boolean,System.Func`1<T>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Boolean,T)
// 0x00000035 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Boolean,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<System.Boolean>,System.Func`1<T>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<System.Boolean>,T)
// 0x00000038 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<System.Boolean>,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<System.Collections.Generic.IEnumerable`1<T>,System.Boolean>,System.Func`1<T>)
// 0x0000003A System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<System.Collections.Generic.IEnumerable`1<T>,System.Boolean>,T)
// 0x0000003B System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<System.Collections.Generic.IEnumerable`1<T>,System.Boolean>,System.Collections.Generic.IEnumerable`1<T>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendWith(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<T>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendWith(System.Collections.Generic.IEnumerable`1<T>,T)
// 0x0000003E System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendWith(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x0000003F System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendIf(System.Collections.Generic.IEnumerable`1<T>,System.Boolean,System.Func`1<T>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendIf(System.Collections.Generic.IEnumerable`1<T>,System.Boolean,T)
// 0x00000041 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendIf(System.Collections.Generic.IEnumerable`1<T>,System.Boolean,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<System.Boolean>,System.Func`1<T>)
// 0x00000043 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<System.Boolean>,T)
// 0x00000044 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<System.Boolean>,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000045 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::FilterCast(System.Collections.IEnumerable)
// 0x00000046 System.Void Sirenix.Utilities.LinqExtensions::AddRange(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000047 System.Boolean Sirenix.Utilities.LinqExtensions::IsNullOrEmpty(System.Collections.Generic.IList`1<T>)
// 0x00000048 System.Void Sirenix.Utilities.LinqExtensions::Populate(System.Collections.Generic.IList`1<T>,T)
// 0x00000049 System.Void Sirenix.Utilities.LinqExtensions::AddRange(System.Collections.Generic.IList`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x0000004A System.Void Sirenix.Utilities.LinqExtensions::Sort(System.Collections.Generic.IList`1<T>,System.Comparison`1<T>)
// 0x0000004B System.Void Sirenix.Utilities.LinqExtensions::Sort(System.Collections.Generic.IList`1<T>)
// 0x0000004C System.Void Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::.ctor(System.Int32)
// 0x0000004D System.Void Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::System.IDisposable.Dispose()
// 0x0000004E System.Boolean Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::MoveNext()
// 0x0000004F System.Void Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::<>m__Finally1()
// 0x00000050 T Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000051 System.Void Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::System.Collections.IEnumerator.Reset()
// 0x00000052 System.Object Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::System.Collections.IEnumerator.get_Current()
// 0x00000053 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000054 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000055 System.Void Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::.ctor(System.Int32)
// 0x00000056 System.Void Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::System.IDisposable.Dispose()
// 0x00000057 System.Boolean Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::MoveNext()
// 0x00000058 System.Void Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::<>m__Finally1()
// 0x00000059 T Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000005A System.Void Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::System.Collections.IEnumerator.Reset()
// 0x0000005B System.Object Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::System.Collections.IEnumerator.get_Current()
// 0x0000005C System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000005D System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005E System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__7`1::.ctor(System.Int32)
// 0x0000005F System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__7`1::System.IDisposable.Dispose()
// 0x00000060 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependWith>d__7`1::MoveNext()
// 0x00000061 System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__7`1::<>m__Finally1()
// 0x00000062 T Sirenix.Utilities.LinqExtensions/<PrependWith>d__7`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000063 System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__7`1::System.Collections.IEnumerator.Reset()
// 0x00000064 System.Object Sirenix.Utilities.LinqExtensions/<PrependWith>d__7`1::System.Collections.IEnumerator.get_Current()
// 0x00000065 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependWith>d__7`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000066 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependWith>d__7`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000067 System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__8`1::.ctor(System.Int32)
// 0x00000068 System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__8`1::System.IDisposable.Dispose()
// 0x00000069 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependWith>d__8`1::MoveNext()
// 0x0000006A System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__8`1::<>m__Finally1()
// 0x0000006B T Sirenix.Utilities.LinqExtensions/<PrependWith>d__8`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000006C System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__8`1::System.Collections.IEnumerator.Reset()
// 0x0000006D System.Object Sirenix.Utilities.LinqExtensions/<PrependWith>d__8`1::System.Collections.IEnumerator.get_Current()
// 0x0000006E System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependWith>d__8`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000006F System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependWith>d__8`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000070 System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__9`1::.ctor(System.Int32)
// 0x00000071 System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__9`1::System.IDisposable.Dispose()
// 0x00000072 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependWith>d__9`1::MoveNext()
// 0x00000073 System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__9`1::<>m__Finally1()
// 0x00000074 System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__9`1::<>m__Finally2()
// 0x00000075 T Sirenix.Utilities.LinqExtensions/<PrependWith>d__9`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000076 System.Void Sirenix.Utilities.LinqExtensions/<PrependWith>d__9`1::System.Collections.IEnumerator.Reset()
// 0x00000077 System.Object Sirenix.Utilities.LinqExtensions/<PrependWith>d__9`1::System.Collections.IEnumerator.get_Current()
// 0x00000078 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependWith>d__9`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000079 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependWith>d__9`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000007A System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::.ctor(System.Int32)
// 0x0000007B System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::System.IDisposable.Dispose()
// 0x0000007C System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::MoveNext()
// 0x0000007D System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::<>m__Finally1()
// 0x0000007E T Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000007F System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::System.Collections.IEnumerator.Reset()
// 0x00000080 System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::System.Collections.IEnumerator.get_Current()
// 0x00000081 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000082 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000083 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::.ctor(System.Int32)
// 0x00000084 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::System.IDisposable.Dispose()
// 0x00000085 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::MoveNext()
// 0x00000086 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::<>m__Finally1()
// 0x00000087 T Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000088 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::System.Collections.IEnumerator.Reset()
// 0x00000089 System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::System.Collections.IEnumerator.get_Current()
// 0x0000008A System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000008B System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000008C System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::.ctor(System.Int32)
// 0x0000008D System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::System.IDisposable.Dispose()
// 0x0000008E System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::MoveNext()
// 0x0000008F System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::<>m__Finally1()
// 0x00000090 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::<>m__Finally2()
// 0x00000091 T Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000092 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::System.Collections.IEnumerator.Reset()
// 0x00000093 System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::System.Collections.IEnumerator.get_Current()
// 0x00000094 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000095 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000096 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::.ctor(System.Int32)
// 0x00000097 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::System.IDisposable.Dispose()
// 0x00000098 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::MoveNext()
// 0x00000099 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::<>m__Finally1()
// 0x0000009A T Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000009B System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::System.Collections.IEnumerator.Reset()
// 0x0000009C System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::System.Collections.IEnumerator.get_Current()
// 0x0000009D System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000009E System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009F System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::.ctor(System.Int32)
// 0x000000A0 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::System.IDisposable.Dispose()
// 0x000000A1 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::MoveNext()
// 0x000000A2 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::<>m__Finally1()
// 0x000000A3 T Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000A4 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::System.Collections.IEnumerator.Reset()
// 0x000000A5 System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::System.Collections.IEnumerator.get_Current()
// 0x000000A6 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000A7 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A8 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::.ctor(System.Int32)
// 0x000000A9 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::System.IDisposable.Dispose()
// 0x000000AA System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::MoveNext()
// 0x000000AB System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::<>m__Finally1()
// 0x000000AC System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::<>m__Finally2()
// 0x000000AD T Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000AE System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::System.Collections.IEnumerator.Reset()
// 0x000000AF System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::System.Collections.IEnumerator.get_Current()
// 0x000000B0 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000B1 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B2 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::.ctor(System.Int32)
// 0x000000B3 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::System.IDisposable.Dispose()
// 0x000000B4 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::MoveNext()
// 0x000000B5 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::<>m__Finally1()
// 0x000000B6 T Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000B7 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::System.Collections.IEnumerator.Reset()
// 0x000000B8 System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::System.Collections.IEnumerator.get_Current()
// 0x000000B9 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000BA System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BB System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::.ctor(System.Int32)
// 0x000000BC System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::System.IDisposable.Dispose()
// 0x000000BD System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::MoveNext()
// 0x000000BE System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::<>m__Finally1()
// 0x000000BF T Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000C0 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::System.Collections.IEnumerator.Reset()
// 0x000000C1 System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::System.Collections.IEnumerator.get_Current()
// 0x000000C2 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000C3 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C4 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::.ctor(System.Int32)
// 0x000000C5 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::System.IDisposable.Dispose()
// 0x000000C6 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::MoveNext()
// 0x000000C7 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::<>m__Finally1()
// 0x000000C8 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::<>m__Finally2()
// 0x000000C9 T Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000CA System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::System.Collections.IEnumerator.Reset()
// 0x000000CB System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::System.Collections.IEnumerator.get_Current()
// 0x000000CC System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000CD System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000CE System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::.ctor(System.Int32)
// 0x000000CF System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::System.IDisposable.Dispose()
// 0x000000D0 System.Boolean Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::MoveNext()
// 0x000000D1 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::<>m__Finally1()
// 0x000000D2 T Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000D3 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::System.Collections.IEnumerator.Reset()
// 0x000000D4 System.Object Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::System.Collections.IEnumerator.get_Current()
// 0x000000D5 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000D6 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000D7 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::.ctor(System.Int32)
// 0x000000D8 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::System.IDisposable.Dispose()
// 0x000000D9 System.Boolean Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::MoveNext()
// 0x000000DA System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::<>m__Finally1()
// 0x000000DB T Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000DC System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::System.Collections.IEnumerator.Reset()
// 0x000000DD System.Object Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::System.Collections.IEnumerator.get_Current()
// 0x000000DE System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000DF System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000E0 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::.ctor(System.Int32)
// 0x000000E1 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::System.IDisposable.Dispose()
// 0x000000E2 System.Boolean Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::MoveNext()
// 0x000000E3 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::<>m__Finally1()
// 0x000000E4 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::<>m__Finally2()
// 0x000000E5 T Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000E6 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::System.Collections.IEnumerator.Reset()
// 0x000000E7 System.Object Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::System.Collections.IEnumerator.get_Current()
// 0x000000E8 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000E9 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000EA System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::.ctor(System.Int32)
// 0x000000EB System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::System.IDisposable.Dispose()
// 0x000000EC System.Boolean Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::MoveNext()
// 0x000000ED System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::<>m__Finally1()
// 0x000000EE T Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000EF System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::System.Collections.IEnumerator.Reset()
// 0x000000F0 System.Object Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::System.Collections.IEnumerator.get_Current()
// 0x000000F1 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000F2 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000F3 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::.ctor(System.Int32)
// 0x000000F4 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::System.IDisposable.Dispose()
// 0x000000F5 System.Boolean Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::MoveNext()
// 0x000000F6 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::<>m__Finally1()
// 0x000000F7 T Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000F8 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::System.Collections.IEnumerator.Reset()
// 0x000000F9 System.Object Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::System.Collections.IEnumerator.get_Current()
// 0x000000FA System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000FB System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000FC System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::.ctor(System.Int32)
// 0x000000FD System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::System.IDisposable.Dispose()
// 0x000000FE System.Boolean Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::MoveNext()
// 0x000000FF System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::<>m__Finally1()
// 0x00000100 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::<>m__Finally2()
// 0x00000101 T Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000102 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::System.Collections.IEnumerator.Reset()
// 0x00000103 System.Object Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::System.Collections.IEnumerator.get_Current()
// 0x00000104 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000105 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000106 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::.ctor(System.Int32)
// 0x00000107 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::System.IDisposable.Dispose()
// 0x00000108 System.Boolean Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::MoveNext()
// 0x00000109 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::<>m__Finally1()
// 0x0000010A T Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000010B System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::System.Collections.IEnumerator.Reset()
// 0x0000010C System.Object Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x0000010D System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000010E System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000010F System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::.ctor(System.Int32)
// 0x00000110 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::System.IDisposable.Dispose()
// 0x00000111 System.Boolean Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::MoveNext()
// 0x00000112 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::<>m__Finally1()
// 0x00000113 T Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000114 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::System.Collections.IEnumerator.Reset()
// 0x00000115 System.Object Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::System.Collections.IEnumerator.get_Current()
// 0x00000116 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000117 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000118 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::.ctor(System.Int32)
// 0x00000119 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::System.IDisposable.Dispose()
// 0x0000011A System.Boolean Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::MoveNext()
// 0x0000011B System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::<>m__Finally1()
// 0x0000011C System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::<>m__Finally2()
// 0x0000011D T Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000011E System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::System.Collections.IEnumerator.Reset()
// 0x0000011F System.Object Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::System.Collections.IEnumerator.get_Current()
// 0x00000120 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000121 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000122 System.Void Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::.ctor(System.Int32)
// 0x00000123 System.Void Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::System.IDisposable.Dispose()
// 0x00000124 System.Boolean Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::MoveNext()
// 0x00000125 System.Void Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::<>m__Finally1()
// 0x00000126 T Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000127 System.Void Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::System.Collections.IEnumerator.Reset()
// 0x00000128 System.Object Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::System.Collections.IEnumerator.get_Current()
// 0x00000129 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000012A System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000012B System.Boolean Sirenix.Utilities.MemberInfoExtensions::IsDefined(System.Reflection.ICustomAttributeProvider,System.Boolean)
// 0x0000012C System.Boolean Sirenix.Utilities.MemberInfoExtensions::IsDefined(System.Reflection.ICustomAttributeProvider)
// 0x0000012D T Sirenix.Utilities.MemberInfoExtensions::GetAttribute(System.Reflection.ICustomAttributeProvider,System.Boolean)
// 0x0000012E T Sirenix.Utilities.MemberInfoExtensions::GetAttribute(System.Reflection.ICustomAttributeProvider)
// 0x0000012F System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider)
// 0x00000130 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider,System.Boolean)
// 0x00000131 System.Attribute[] Sirenix.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider)
extern void MemberInfoExtensions_GetAttributes_mC67AF61F5F613E8A622E70FE37DB2A7F3C871EAB (void);
// 0x00000132 System.Attribute[] Sirenix.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider,System.Boolean)
extern void MemberInfoExtensions_GetAttributes_mDC5120BF9C047F5114974C5D5998D5E10722AF93 (void);
// 0x00000133 System.String Sirenix.Utilities.MemberInfoExtensions::GetNiceName(System.Reflection.MemberInfo)
extern void MemberInfoExtensions_GetNiceName_m4C4C96D2B0AB0B782B88F58DF49472538F3F4AE0 (void);
// 0x00000134 System.Boolean Sirenix.Utilities.MemberInfoExtensions::IsStatic(System.Reflection.MemberInfo)
extern void MemberInfoExtensions_IsStatic_m34080863A1E59E680897829BE78D658127551FBA (void);
// 0x00000135 System.Boolean Sirenix.Utilities.MemberInfoExtensions::IsAlias(System.Reflection.MemberInfo)
extern void MemberInfoExtensions_IsAlias_m875106ADBA946765F38220DDE5138856B47A33CE (void);
// 0x00000136 System.Reflection.MemberInfo Sirenix.Utilities.MemberInfoExtensions::DeAlias(System.Reflection.MemberInfo,System.Boolean)
extern void MemberInfoExtensions_DeAlias_m0EACB9804C68943D56AF92C6201AC808B9C502D5 (void);
// 0x00000137 System.Boolean Sirenix.Utilities.MemberInfoExtensions::SignaturesAreEqual(System.Reflection.MemberInfo,System.Reflection.MemberInfo)
extern void MemberInfoExtensions_SignaturesAreEqual_mB0220D7BB799E753BBF4FB4CDB8DB94826886704 (void);
// 0x00000138 System.String Sirenix.Utilities.MethodInfoExtensions::GetFullName(System.Reflection.MethodBase,System.String)
extern void MethodInfoExtensions_GetFullName_m691FC2714107F7C00596B7A3CE926C46C757F40C (void);
// 0x00000139 System.String Sirenix.Utilities.MethodInfoExtensions::GetParamsNames(System.Reflection.MethodBase)
extern void MethodInfoExtensions_GetParamsNames_m79240D291015C13AEBD2360B14D3A8F4BC472DBF (void);
// 0x0000013A System.String Sirenix.Utilities.MethodInfoExtensions::GetFullName(System.Reflection.MethodBase)
extern void MethodInfoExtensions_GetFullName_m769F55B4D93A900A3B644D7FAB0F8F101881E8E3 (void);
// 0x0000013B System.Boolean Sirenix.Utilities.MethodInfoExtensions::IsExtensionMethod(System.Reflection.MethodBase)
extern void MethodInfoExtensions_IsExtensionMethod_m65CD2EE770B00DFBDA04F2B2C42C412CCCA5AD02 (void);
// 0x0000013C System.Boolean Sirenix.Utilities.MethodInfoExtensions::IsAliasMethod(System.Reflection.MethodInfo)
extern void MethodInfoExtensions_IsAliasMethod_m633A938A0FDCDD33A7E20567F5A36E46C5BD23ED (void);
// 0x0000013D System.Reflection.MethodInfo Sirenix.Utilities.MethodInfoExtensions::DeAliasMethod(System.Reflection.MethodInfo,System.Boolean)
extern void MethodInfoExtensions_DeAliasMethod_m46FE50F76E67FE6F955D189FDD6C03509A739BED (void);
// 0x0000013E System.String Sirenix.Utilities.PathUtilities::GetDirectoryName(System.String)
extern void PathUtilities_GetDirectoryName_m043DE0319FD343A79437D8C3077F3C7D6BB822F7 (void);
// 0x0000013F System.Boolean Sirenix.Utilities.PathUtilities::HasSubDirectory(System.IO.DirectoryInfo,System.IO.DirectoryInfo)
extern void PathUtilities_HasSubDirectory_mC2CFE2899F6AF9A9A29CE0F41F1C2AE1148FF173 (void);
// 0x00000140 System.IO.DirectoryInfo Sirenix.Utilities.PathUtilities::FindParentDirectoryWithName(System.IO.DirectoryInfo,System.String)
extern void PathUtilities_FindParentDirectoryWithName_m9C21BFB384B12F97FABE7F75510A789BC7EE5321 (void);
// 0x00000141 System.Boolean Sirenix.Utilities.PathUtilities::CanMakeRelative(System.String,System.String)
extern void PathUtilities_CanMakeRelative_mBB41C2ED1246D8B4BBA26375F61FD4B1DBE902F7 (void);
// 0x00000142 System.String Sirenix.Utilities.PathUtilities::MakeRelative(System.String,System.String)
extern void PathUtilities_MakeRelative_m0C634F205BEEC8030703E326017839125AF300FE (void);
// 0x00000143 System.Boolean Sirenix.Utilities.PathUtilities::TryMakeRelative(System.String,System.String,System.String&)
extern void PathUtilities_TryMakeRelative_m2CCEB4584F3119FCB776864DA25C5DEA8B9695BA (void);
// 0x00000144 System.String Sirenix.Utilities.PathUtilities::Combine(System.String,System.String)
extern void PathUtilities_Combine_mA326E8DD584D486B67F55DEBF436606F50510C07 (void);
// 0x00000145 System.Boolean Sirenix.Utilities.PropertyInfoExtensions::IsAutoProperty(System.Reflection.PropertyInfo,System.Boolean)
extern void PropertyInfoExtensions_IsAutoProperty_m78B131314E2F486AF4414E88DF745682C1846BED (void);
// 0x00000146 System.Boolean Sirenix.Utilities.PropertyInfoExtensions::IsAliasProperty(System.Reflection.PropertyInfo)
extern void PropertyInfoExtensions_IsAliasProperty_mDA54D8A4C1AEA6F1327D0611046185711315A478 (void);
// 0x00000147 System.Reflection.PropertyInfo Sirenix.Utilities.PropertyInfoExtensions::DeAliasProperty(System.Reflection.PropertyInfo,System.Boolean)
extern void PropertyInfoExtensions_DeAliasProperty_m96A39DD5E5AC93F400CDCC0E38463F743EDE2DE8 (void);
// 0x00000148 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetWidth(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetWidth_m69CF523EAC6A34C33AA3FFCFC8686AB4B71AE641 (void);
// 0x00000149 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetHeight(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetHeight_m506B0534A498040EDE45E6033780AEB8638BE829 (void);
// 0x0000014A UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetSize(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_SetSize_m33D310F0B86907CB749CAB0D05FB3A32032DA3FD (void);
// 0x0000014B UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetSize(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SetSize_m77332E849C6579179AAF16098541615F32FADE91 (void);
// 0x0000014C UnityEngine.Rect Sirenix.Utilities.RectExtensions::HorizontalPadding(UnityEngine.Rect,System.Single)
extern void RectExtensions_HorizontalPadding_m40E9A2B65348E239D4F53B3F2682A4F8EFC72F38 (void);
// 0x0000014D UnityEngine.Rect Sirenix.Utilities.RectExtensions::HorizontalPadding(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_HorizontalPadding_mF5400E98C4BA88637DB612D7B19B43C34AF9102D (void);
// 0x0000014E UnityEngine.Rect Sirenix.Utilities.RectExtensions::VerticalPadding(UnityEngine.Rect,System.Single)
extern void RectExtensions_VerticalPadding_mBB7299207CEF10A7FC9BB80F8CB9EA4B51A9C45A (void);
// 0x0000014F UnityEngine.Rect Sirenix.Utilities.RectExtensions::VerticalPadding(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_VerticalPadding_mACD68D876E752DBD423AF49A0EDF563584D1FAB9 (void);
// 0x00000150 UnityEngine.Rect Sirenix.Utilities.RectExtensions::Padding(UnityEngine.Rect,System.Single)
extern void RectExtensions_Padding_mBEC2014271A4CCA549DC93EFF69CAB46FAA8D2D7 (void);
// 0x00000151 UnityEngine.Rect Sirenix.Utilities.RectExtensions::Padding(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_Padding_m37813FB32D26061D0E9475BCB6D9BA06EFDC1082 (void);
// 0x00000152 UnityEngine.Rect Sirenix.Utilities.RectExtensions::Padding(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single)
extern void RectExtensions_Padding_mFBC76FAA5D294CB444DD359CFFD97A88B8155DD5 (void);
// 0x00000153 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignLeft(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignLeft_m0567E2C5D89D516BAFA700E513B29945A3100F0A (void);
// 0x00000154 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignCenter(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignCenter_m870B46389403681459F6A312101A25E5E1DFCCE3 (void);
// 0x00000155 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignCenter(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_AlignCenter_mD93A613D578F553DFFD06C6D6BB09EBFCB31AD98 (void);
// 0x00000156 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignRight(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignRight_m99D45974D9E57C19AB1D345CBDA03577783188F3 (void);
// 0x00000157 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignRight(UnityEngine.Rect,System.Single,System.Boolean)
extern void RectExtensions_AlignRight_mABBF469967EF7492916B6A983ED06F3A4CC2DE28 (void);
// 0x00000158 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignTop(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignTop_m074B39472F8270D93BEC35F2B01833D5576D9EB7 (void);
// 0x00000159 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignMiddle(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignMiddle_mBAECD2906BE0488F4E6C852543E707051CC996A7 (void);
// 0x0000015A UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignBottom(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignBottom_m82A57F38D61437057CE370DD2F6EFC318CFEC283 (void);
// 0x0000015B UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignCenterX(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignCenterX_m0AD1FF1FCFC42E3EB8D87B037C5265A26BA9EE4A (void);
// 0x0000015C UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignCenterY(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignCenterY_mC6D062A1262386DC84A10D80D2D557B08D968ACE (void);
// 0x0000015D UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignCenterXY(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignCenterXY_m6BAE2956C5BE0350C4A4D1A54E27F31738D73923 (void);
// 0x0000015E UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignCenterXY(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_AlignCenterXY_m31868BDCB2066600551158562C9D16D216098744 (void);
// 0x0000015F UnityEngine.Rect Sirenix.Utilities.RectExtensions::Expand(UnityEngine.Rect,System.Single)
extern void RectExtensions_Expand_m7ECB0674AE6B84749594E777E1C06CAE049FF631 (void);
// 0x00000160 UnityEngine.Rect Sirenix.Utilities.RectExtensions::Expand(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_Expand_m5C8CB6CE42E33DAB75F9A19ADE371398FB1DD9EE (void);
// 0x00000161 UnityEngine.Rect Sirenix.Utilities.RectExtensions::Expand(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single)
extern void RectExtensions_Expand_m868D059441EBCE784D65912EE91E8982F65A3D05 (void);
// 0x00000162 UnityEngine.Rect Sirenix.Utilities.RectExtensions::Split(UnityEngine.Rect,System.Int32,System.Int32)
extern void RectExtensions_Split_m8498B85B3042F680956CC0CAD4B9E172654BAA10 (void);
// 0x00000163 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SplitVertical(UnityEngine.Rect,System.Int32,System.Int32)
extern void RectExtensions_SplitVertical_mDC348DA26D272EFEBD3A47576D991130F18BE6B8 (void);
// 0x00000164 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SplitGrid(UnityEngine.Rect,System.Single,System.Single,System.Int32)
extern void RectExtensions_SplitGrid_mDD29A44B513AB37844AB20093F71BDA8B255FB5E (void);
// 0x00000165 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SplitTableGrid(UnityEngine.Rect,System.Int32,System.Single,System.Int32)
extern void RectExtensions_SplitTableGrid_mBF5ABF975DB20FF5D990002F3FE19F955576FB73 (void);
// 0x00000166 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetCenterX(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetCenterX_mEB413A4A360A104F5B59D9537CEF243CC9F2C4CE (void);
// 0x00000167 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetCenterY(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetCenterY_m4977DBEDECCDAB450811F1CF801341766001FEA9 (void);
// 0x00000168 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetCenter(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_SetCenter_mB3DDA607B2F9F0D30BD50E6A22AC9D9DC8EA8F20 (void);
// 0x00000169 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetCenter(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SetCenter_mAC7ECCF3C94CCC3BC3D056379C0C4C2CDF3EA7E0 (void);
// 0x0000016A UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetPosition(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SetPosition_m09C88CEE5C7DBD17925C7913F7CB0596003758B3 (void);
// 0x0000016B UnityEngine.Rect Sirenix.Utilities.RectExtensions::ResetPosition(UnityEngine.Rect)
extern void RectExtensions_ResetPosition_m1B049C9AE1D10788E3F3651ED61611C251857EE9 (void);
// 0x0000016C UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddPosition(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_AddPosition_m6B6BE590A267F522AF0C96087BFDC47C602132EA (void);
// 0x0000016D UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddPosition(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_AddPosition_mB099E38EE661375D2C0368AE71B2E3B5B9ED9E67 (void);
// 0x0000016E UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetX(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetX_mB5C44EE347E61D0DA3E674817955121D6FB4C7D1 (void);
// 0x0000016F UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddX(UnityEngine.Rect,System.Single)
extern void RectExtensions_AddX_mFE44A35C702AAC8131AD782E6A7A0D17AA3F5F68 (void);
// 0x00000170 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubX(UnityEngine.Rect,System.Single)
extern void RectExtensions_SubX_m241BD947ADCEE437FDABEC65993BDCF6AD74AE57 (void);
// 0x00000171 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetY(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetY_m1DC8DA1C70D789128A8750E82F0BBBD68D09494B (void);
// 0x00000172 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddY(UnityEngine.Rect,System.Single)
extern void RectExtensions_AddY_m34D9EED90402B2B28E3A90B6C61E6EFA385BA450 (void);
// 0x00000173 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubY(UnityEngine.Rect,System.Single)
extern void RectExtensions_SubY_m64C3AA4112903ECF49CF6BA666E1299495535D59 (void);
// 0x00000174 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetMin(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SetMin_m4B63F926FDFE2EAE085CE45279242C8F257926DB (void);
// 0x00000175 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddMin(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_AddMin_m5A48E49590A3C028D5F648DD672419EF7AF541D0 (void);
// 0x00000176 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubMin(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SubMin_mDF1C320D2FD6EF27588A0A4D0DD829D8F8733D6F (void);
// 0x00000177 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetMax(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SetMax_m0A7CAED9DA27C89C29CD83A56DA73276DA89141A (void);
// 0x00000178 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddMax(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_AddMax_m554B9A78C0BC8BA224FDBD88B55303D45532EA00 (void);
// 0x00000179 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubMax(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SubMax_m0C47C7BBD6E44857132B680FD65791F6C1FE498C (void);
// 0x0000017A UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetXMin(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetXMin_mDC3EF8BBBD19D11B6C2FC43FDDB2552D2B47D916 (void);
// 0x0000017B UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddXMin(UnityEngine.Rect,System.Single)
extern void RectExtensions_AddXMin_mF7E01410A0774329AACE7355938CB69557FCE69A (void);
// 0x0000017C UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubXMin(UnityEngine.Rect,System.Single)
extern void RectExtensions_SubXMin_m03C4A5FCD2475806CCD54BEBA93436468B4EA2F0 (void);
// 0x0000017D UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetXMax(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetXMax_m8AF050E22802D9827228B241188BF6E746356D37 (void);
// 0x0000017E UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddXMax(UnityEngine.Rect,System.Single)
extern void RectExtensions_AddXMax_mE7597128E980376700BF85196BF2716FADF4F508 (void);
// 0x0000017F UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubXMax(UnityEngine.Rect,System.Single)
extern void RectExtensions_SubXMax_mF32A698159791AD4768065C43F1AF07D1B6A4BBD (void);
// 0x00000180 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetYMin(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetYMin_m47B9FD6B785F3906C973F8284383420CC68973FF (void);
// 0x00000181 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddYMin(UnityEngine.Rect,System.Single)
extern void RectExtensions_AddYMin_m9ADC635148B7C93295279F5B54337BC833336F97 (void);
// 0x00000182 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubYMin(UnityEngine.Rect,System.Single)
extern void RectExtensions_SubYMin_m7EB4B2B44C19183982942EFD56697E5ECC3E733B (void);
// 0x00000183 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetYMax(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetYMax_mFF54ADAA81D0D33AAA4835B63CCDE3C2A5E6A214 (void);
// 0x00000184 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddYMax(UnityEngine.Rect,System.Single)
extern void RectExtensions_AddYMax_mAFF59FE7BFF7AD58182765E419B1BEFC7F0286BE (void);
// 0x00000185 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubYMax(UnityEngine.Rect,System.Single)
extern void RectExtensions_SubYMax_m62763B948FAB91266E54922064BE5E96695AB786 (void);
// 0x00000186 UnityEngine.Rect Sirenix.Utilities.RectExtensions::MinWidth(UnityEngine.Rect,System.Single)
extern void RectExtensions_MinWidth_mEE5A7F7E7ED7A1671338411F16C7A726EB57EBFF (void);
// 0x00000187 UnityEngine.Rect Sirenix.Utilities.RectExtensions::MaxWidth(UnityEngine.Rect,System.Single)
extern void RectExtensions_MaxWidth_mF32385BD1955090106420BEAD91CDE5A0AE127CF (void);
// 0x00000188 UnityEngine.Rect Sirenix.Utilities.RectExtensions::MinHeight(UnityEngine.Rect,System.Single)
extern void RectExtensions_MinHeight_m1DC4AF78A97A12996512DFF8B6589C58C37B34E3 (void);
// 0x00000189 UnityEngine.Rect Sirenix.Utilities.RectExtensions::MaxHeight(UnityEngine.Rect,System.Single)
extern void RectExtensions_MaxHeight_m334669D6808352FE029F25A01A769C92C7A4C27A (void);
// 0x0000018A UnityEngine.Rect Sirenix.Utilities.RectExtensions::ExpandTo(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_ExpandTo_mFABBE0B7E524BDE1EFFD7148E4965994EAEDDC59 (void);
// 0x0000018B System.String Sirenix.Utilities.StringExtensions::ToTitleCase(System.String)
extern void StringExtensions_ToTitleCase_mB9B316832D46FE73E51A9663E469C7293AED41C9 (void);
// 0x0000018C System.Boolean Sirenix.Utilities.StringExtensions::Contains(System.String,System.String,System.StringComparison)
extern void StringExtensions_Contains_mD4E8705337729736E32CD3AC79F25FF9465218BE (void);
// 0x0000018D System.String Sirenix.Utilities.StringExtensions::SplitPascalCase(System.String)
extern void StringExtensions_SplitPascalCase_mBF2018941241885F3B1A5E5E74FC5AA03A72872C (void);
// 0x0000018E System.Boolean Sirenix.Utilities.StringExtensions::IsNullOrWhitespace(System.String)
extern void StringExtensions_IsNullOrWhitespace_m0595931B5E1321BE0488D986DC3DAFBA87879031 (void);
// 0x0000018F System.String Sirenix.Utilities.TypeExtensions::GetCachedNiceName(System.Type)
extern void TypeExtensions_GetCachedNiceName_mB31E4F835E6E803BD222C847A4F9E9A5CF00B933 (void);
// 0x00000190 System.String Sirenix.Utilities.TypeExtensions::CreateNiceName(System.Type)
extern void TypeExtensions_CreateNiceName_m50AEA1CCA0413092726B1632F8025C2BC80BAC29 (void);
// 0x00000191 System.Boolean Sirenix.Utilities.TypeExtensions::HasCastDefined(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_HasCastDefined_mB133D445B3BF6F2C4B82B19781D146F057279EDF (void);
// 0x00000192 System.Boolean Sirenix.Utilities.TypeExtensions::IsValidIdentifier(System.String)
extern void TypeExtensions_IsValidIdentifier_m47E8197DAB361AA566DF0477781DF257216AE9E4 (void);
// 0x00000193 System.Boolean Sirenix.Utilities.TypeExtensions::IsValidIdentifierStartCharacter(System.Char)
extern void TypeExtensions_IsValidIdentifierStartCharacter_m1A7729893C8F48B364B80AFC277D39B221F4953A (void);
// 0x00000194 System.Boolean Sirenix.Utilities.TypeExtensions::IsValidIdentifierPartCharacter(System.Char)
extern void TypeExtensions_IsValidIdentifierPartCharacter_mCE9FBF6524454FFE8745464BC5DE1003B841F663 (void);
// 0x00000195 System.Boolean Sirenix.Utilities.TypeExtensions::IsCastableTo(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_IsCastableTo_mB328E0B03BD1059CE38164C92D5BFA9B599616D6 (void);
// 0x00000196 System.Func`2<System.Object,System.Object> Sirenix.Utilities.TypeExtensions::GetCastMethodDelegate(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_GetCastMethodDelegate_m45B494F42E4689E53E932F01962A0AF8FF560CE3 (void);
// 0x00000197 System.Func`2<TFrom,TTo> Sirenix.Utilities.TypeExtensions::GetCastMethodDelegate(System.Boolean)
// 0x00000198 System.Reflection.MethodInfo Sirenix.Utilities.TypeExtensions::GetCastMethod(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_GetCastMethod_m1E45DF17AC9C43E804CA496424241079AA92C57C (void);
// 0x00000199 System.Boolean Sirenix.Utilities.TypeExtensions::FloatEqualityComparer(System.Single,System.Single)
extern void TypeExtensions_FloatEqualityComparer_m7697C086CE77E3A7A8D97B9B81DC26F57936B8CE (void);
// 0x0000019A System.Boolean Sirenix.Utilities.TypeExtensions::DoubleEqualityComparer(System.Double,System.Double)
extern void TypeExtensions_DoubleEqualityComparer_m531ACEF34394BDD2B3BA69A2A85BD89B4B73DAF8 (void);
// 0x0000019B System.Boolean Sirenix.Utilities.TypeExtensions::QuaternionEqualityComparer(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void TypeExtensions_QuaternionEqualityComparer_m9F9925CDEF821367232A22589A977BDCBB674464 (void);
// 0x0000019C System.Func`3<T,T,System.Boolean> Sirenix.Utilities.TypeExtensions::GetEqualityComparerDelegate()
// 0x0000019D T Sirenix.Utilities.TypeExtensions::GetAttribute(System.Type,System.Boolean)
// 0x0000019E System.Boolean Sirenix.Utilities.TypeExtensions::ImplementsOrInherits(System.Type,System.Type)
extern void TypeExtensions_ImplementsOrInherits_m8E23334851CA981915ABB06D42A8006EB1E71E2D (void);
// 0x0000019F System.Boolean Sirenix.Utilities.TypeExtensions::ImplementsOpenGenericType(System.Type,System.Type)
extern void TypeExtensions_ImplementsOpenGenericType_m1847799486A6B8BF59900F17A0E89D53705CCA8F (void);
// 0x000001A0 System.Boolean Sirenix.Utilities.TypeExtensions::ImplementsOpenGenericInterface(System.Type,System.Type)
extern void TypeExtensions_ImplementsOpenGenericInterface_m0FD4B21E28C47C239425F66E1E67493BC6C4755E (void);
// 0x000001A1 System.Boolean Sirenix.Utilities.TypeExtensions::ImplementsOpenGenericClass(System.Type,System.Type)
extern void TypeExtensions_ImplementsOpenGenericClass_m965DC03969A412EBDC18BB5D86E970E4545F4AB7 (void);
// 0x000001A2 System.Type[] Sirenix.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericType(System.Type,System.Type)
extern void TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m95FF1DADBC80D367744A4E5CBCF24AA8A192641F (void);
// 0x000001A3 System.Type[] Sirenix.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericClass(System.Type,System.Type)
extern void TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_mA17992E3F2A3A0B34D9B5618C13A5A8BBB146CCB (void);
// 0x000001A4 System.Type[] Sirenix.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericInterface(System.Type,System.Type)
extern void TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m34AA0B9FD29009CCF61B4730328B7D905C92F5A5 (void);
// 0x000001A5 System.Reflection.MethodInfo Sirenix.Utilities.TypeExtensions::GetOperatorMethod(System.Type,Sirenix.Utilities.Operator,System.Type,System.Type)
extern void TypeExtensions_GetOperatorMethod_m53F8A003511735F3450F2D090240E951A31577BA (void);
// 0x000001A6 System.Reflection.MethodInfo Sirenix.Utilities.TypeExtensions::GetOperatorMethod(System.Type,Sirenix.Utilities.Operator)
extern void TypeExtensions_GetOperatorMethod_m9E43DACCFA5A920923CE6E11BF063C1201ECD30D (void);
// 0x000001A7 System.Reflection.MethodInfo[] Sirenix.Utilities.TypeExtensions::GetOperatorMethods(System.Type,Sirenix.Utilities.Operator)
extern void TypeExtensions_GetOperatorMethods_m81F2EB856D14490DCF9D389765C8BE9A83ECC502 (void);
// 0x000001A8 System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Sirenix.Utilities.TypeExtensions::GetAllMembers(System.Type,System.Reflection.BindingFlags)
extern void TypeExtensions_GetAllMembers_m42F29BA220A4DC676E5E1A6E977B4B7F4F36AF5E (void);
// 0x000001A9 System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Sirenix.Utilities.TypeExtensions::GetAllMembers(System.Type,System.String,System.Reflection.BindingFlags)
extern void TypeExtensions_GetAllMembers_mF06823B17C872322362C5497C49A5B0640DF90A0 (void);
// 0x000001AA System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.TypeExtensions::GetAllMembers(System.Type,System.Reflection.BindingFlags)
// 0x000001AB System.Type Sirenix.Utilities.TypeExtensions::GetGenericBaseType(System.Type,System.Type)
extern void TypeExtensions_GetGenericBaseType_mB8440ED4542248F0CCFB747B2462DB2973A8D576 (void);
// 0x000001AC System.Type Sirenix.Utilities.TypeExtensions::GetGenericBaseType(System.Type,System.Type,System.Int32&)
extern void TypeExtensions_GetGenericBaseType_m23B0DC843BA54F392EBF17D265ACD23EE7E40C52 (void);
// 0x000001AD System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Utilities.TypeExtensions::GetBaseTypes(System.Type,System.Boolean)
extern void TypeExtensions_GetBaseTypes_m88F667C6C77571F5796C1D8EFD8B820021482FB0 (void);
// 0x000001AE System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Utilities.TypeExtensions::GetBaseClasses(System.Type,System.Boolean)
extern void TypeExtensions_GetBaseClasses_mD3A1D3413EC8865CC7F0F4AAD2E86BC9378F4083 (void);
// 0x000001AF System.String Sirenix.Utilities.TypeExtensions::TypeNameGauntlet(System.Type)
extern void TypeExtensions_TypeNameGauntlet_mD36134A2F32258085D95038D638C73A724B5BE30 (void);
// 0x000001B0 System.String Sirenix.Utilities.TypeExtensions::GetNiceName(System.Type)
extern void TypeExtensions_GetNiceName_m983B921831835109AC6E22EA98393B61A45D965A (void);
// 0x000001B1 System.String Sirenix.Utilities.TypeExtensions::GetNiceFullName(System.Type)
extern void TypeExtensions_GetNiceFullName_m4128E1678161C1BF028D4A769E374886D0045077 (void);
// 0x000001B2 System.String Sirenix.Utilities.TypeExtensions::GetCompilableNiceName(System.Type)
extern void TypeExtensions_GetCompilableNiceName_m0CFABA952AEBC7601065DB82766595CCF4E63828 (void);
// 0x000001B3 System.String Sirenix.Utilities.TypeExtensions::GetCompilableNiceFullName(System.Type)
extern void TypeExtensions_GetCompilableNiceFullName_mAA85B6E2BF337D7B2872003C9853A64C30569EAF (void);
// 0x000001B4 T Sirenix.Utilities.TypeExtensions::GetCustomAttribute(System.Type,System.Boolean)
// 0x000001B5 T Sirenix.Utilities.TypeExtensions::GetCustomAttribute(System.Type)
// 0x000001B6 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.TypeExtensions::GetCustomAttributes(System.Type)
// 0x000001B7 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.TypeExtensions::GetCustomAttributes(System.Type,System.Boolean)
// 0x000001B8 System.Boolean Sirenix.Utilities.TypeExtensions::IsDefined(System.Type)
// 0x000001B9 System.Boolean Sirenix.Utilities.TypeExtensions::IsDefined(System.Type,System.Boolean)
// 0x000001BA System.Boolean Sirenix.Utilities.TypeExtensions::InheritsFrom(System.Type)
// 0x000001BB System.Boolean Sirenix.Utilities.TypeExtensions::InheritsFrom(System.Type,System.Type)
extern void TypeExtensions_InheritsFrom_m762278654D771B22055D4F401E7C236E80BFC521 (void);
// 0x000001BC System.Int32 Sirenix.Utilities.TypeExtensions::GetInheritanceDistance(System.Type,System.Type)
extern void TypeExtensions_GetInheritanceDistance_m71C26D3B7EA359B1546F4AD79212C6584C2BE86E (void);
// 0x000001BD System.Boolean Sirenix.Utilities.TypeExtensions::HasParamaters(System.Reflection.MethodInfo,System.Collections.Generic.IList`1<System.Type>,System.Boolean)
extern void TypeExtensions_HasParamaters_mB2976B5A64C80E9FF4191BC7F6CCE00F200CA836 (void);
// 0x000001BE System.Type Sirenix.Utilities.TypeExtensions::GetReturnType(System.Reflection.MemberInfo)
extern void TypeExtensions_GetReturnType_m291D3DCFFF1E0BC47D77C68EC57B54BA12C35C9F (void);
// 0x000001BF System.Object Sirenix.Utilities.TypeExtensions::GetMemberValue(System.Reflection.MemberInfo,System.Object)
extern void TypeExtensions_GetMemberValue_mEC3A4278CB6E2B7F74BE32065AE5378F294255CA (void);
// 0x000001C0 System.Void Sirenix.Utilities.TypeExtensions::SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)
extern void TypeExtensions_SetMemberValue_m41BF920FBA355BA316E9A4DB69034CC97E9D4D1C (void);
// 0x000001C1 System.Boolean Sirenix.Utilities.TypeExtensions::TryInferGenericParameters(System.Type,System.Type[]&,System.Type[])
extern void TypeExtensions_TryInferGenericParameters_m616BF0356A54815A95874A35349DE8E1C1A386E8 (void);
// 0x000001C2 System.Boolean Sirenix.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Type,System.Type[])
extern void TypeExtensions_AreGenericConstraintsSatisfiedBy_mE18B30C962AA15EC643C6CF40A36AD7469C53E08 (void);
// 0x000001C3 System.Boolean Sirenix.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Reflection.MethodBase,System.Type[])
extern void TypeExtensions_AreGenericConstraintsSatisfiedBy_m93E8C890160A065BA9260820A197BC0E79BB27E8 (void);
// 0x000001C4 System.Boolean Sirenix.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Type[],System.Type[])
extern void TypeExtensions_AreGenericConstraintsSatisfiedBy_m1193BE222CCCC25CEADAC6E1768E40FBB09004BA (void);
// 0x000001C5 System.Boolean Sirenix.Utilities.TypeExtensions::GenericParameterIsFulfilledBy(System.Type,System.Type)
extern void TypeExtensions_GenericParameterIsFulfilledBy_m9099CDA05D4394848D7F5C5008FF219537693C94 (void);
// 0x000001C6 System.Boolean Sirenix.Utilities.TypeExtensions::GenericParameterIsFulfilledBy(System.Type,System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Type>,System.Collections.Generic.HashSet`1<System.Type>)
extern void TypeExtensions_GenericParameterIsFulfilledBy_m2BC87AC545C8C8274233A88F3EBBD216B5B4860A (void);
// 0x000001C7 System.String Sirenix.Utilities.TypeExtensions::GetGenericConstraintsString(System.Type,System.Boolean)
extern void TypeExtensions_GetGenericConstraintsString_mD1AAC4E266DC668B985793D40AE9D20EE96DEF27 (void);
// 0x000001C8 System.String Sirenix.Utilities.TypeExtensions::GetGenericParameterConstraintsString(System.Type,System.Boolean)
extern void TypeExtensions_GetGenericParameterConstraintsString_m92101CDFB675059CE4937378BE45EEB7D8B2C2FA (void);
// 0x000001C9 System.Boolean Sirenix.Utilities.TypeExtensions::GenericArgumentsContainsTypes(System.Type,System.Type[])
extern void TypeExtensions_GenericArgumentsContainsTypes_m1554521EC1814E889071C4A627BBC530D40F97D5 (void);
// 0x000001CA System.Boolean Sirenix.Utilities.TypeExtensions::IsFullyConstructedGenericType(System.Type)
extern void TypeExtensions_IsFullyConstructedGenericType_m40930F66B306AF33E5198EE8816CE38AB451ECA5 (void);
// 0x000001CB System.Boolean Sirenix.Utilities.TypeExtensions::IsNullableType(System.Type)
extern void TypeExtensions_IsNullableType_m0D2F26F7AC9C21752CFA969F73F3BB03F012E8F4 (void);
// 0x000001CC System.UInt64 Sirenix.Utilities.TypeExtensions::GetEnumBitmask(System.Object,System.Type)
extern void TypeExtensions_GetEnumBitmask_m036AACF48FB1E2DF3366EF4ACF05E6BE9FF41918 (void);
// 0x000001CD System.Boolean Sirenix.Utilities.TypeExtensions::IsCSharpKeyword(System.String)
extern void TypeExtensions_IsCSharpKeyword_m5DAA06A44F83CB45BDB1F4F3D8233AEAC9D040B7 (void);
// 0x000001CE System.Type[] Sirenix.Utilities.TypeExtensions::SafeGetTypes(System.Reflection.Assembly)
extern void TypeExtensions_SafeGetTypes_m8C7C79CBE78F0A2CE27DE2CE90B22DD5178DD6B4 (void);
// 0x000001CF System.Boolean Sirenix.Utilities.TypeExtensions::SafeIsDefined(System.Reflection.Assembly,System.Type,System.Boolean)
extern void TypeExtensions_SafeIsDefined_mB2886AC4FE511CF0E058C5FDADD379321216B418 (void);
// 0x000001D0 System.Object[] Sirenix.Utilities.TypeExtensions::SafeGetCustomAttributes(System.Reflection.Assembly,System.Type,System.Boolean)
extern void TypeExtensions_SafeGetCustomAttributes_m1C27BF57A9D5231695C7AA397E1A96AB439BA587 (void);
// 0x000001D1 System.Void Sirenix.Utilities.TypeExtensions::.cctor()
extern void TypeExtensions__cctor_m4C48D5A750AB56CE24DFD17CC28E81374C2C8667 (void);
// 0x000001D2 System.Void Sirenix.Utilities.TypeExtensions/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_mC8D394A379A62D07F430EBE5AE4CE39A8FDE8C6A (void);
// 0x000001D3 System.Object Sirenix.Utilities.TypeExtensions/<>c__DisplayClass29_0::<GetCastMethodDelegate>b__0(System.Object)
extern void U3CU3Ec__DisplayClass29_0_U3CGetCastMethodDelegateU3Eb__0_mE26E2A61B4BEE3D76B89D07CE760696A203E3279 (void);
// 0x000001D4 System.Void Sirenix.Utilities.TypeExtensions/<>c__35`1::.cctor()
// 0x000001D5 System.Void Sirenix.Utilities.TypeExtensions/<>c__35`1::.ctor()
// 0x000001D6 System.Boolean Sirenix.Utilities.TypeExtensions/<>c__35`1::<GetEqualityComparerDelegate>b__35_0(T,T)
// 0x000001D7 System.Boolean Sirenix.Utilities.TypeExtensions/<>c__35`1::<GetEqualityComparerDelegate>b__35_1(T,T)
// 0x000001D8 System.Void Sirenix.Utilities.TypeExtensions/<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_mE2344F83F9E0797A0FFA95A87AC38B9DC734F21C (void);
// 0x000001D9 System.Boolean Sirenix.Utilities.TypeExtensions/<>c__DisplayClass45_0::<GetOperatorMethod>b__0(System.Reflection.MethodInfo)
extern void U3CU3Ec__DisplayClass45_0_U3CGetOperatorMethodU3Eb__0_m10CE18C90803214FDA5AB8294FBBCADBA0CE40ED (void);
// 0x000001DA System.Void Sirenix.Utilities.TypeExtensions/<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_m35E9204597BCF911437B6D002456A67454196934 (void);
// 0x000001DB System.Boolean Sirenix.Utilities.TypeExtensions/<>c__DisplayClass46_0::<GetOperatorMethods>b__0(System.Reflection.MethodInfo)
extern void U3CU3Ec__DisplayClass46_0_U3CGetOperatorMethodsU3Eb__0_mE4E4F5922DFA099C791F069D80F7164A3A7D7475 (void);
// 0x000001DC System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__47::.ctor(System.Int32)
extern void U3CGetAllMembersU3Ed__47__ctor_m35859B5371788C990C18EF2D4189E75649FC6FB5 (void);
// 0x000001DD System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__47::System.IDisposable.Dispose()
extern void U3CGetAllMembersU3Ed__47_System_IDisposable_Dispose_m9E592A6C17D363B9C256AB7215BA1B1201622D9C (void);
// 0x000001DE System.Boolean Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__47::MoveNext()
extern void U3CGetAllMembersU3Ed__47_MoveNext_mEBC0272EF45C07191B767EA40B0CDC28FB852511 (void);
// 0x000001DF System.Reflection.MemberInfo Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__47::System.Collections.Generic.IEnumerator<System.Reflection.MemberInfo>.get_Current()
extern void U3CGetAllMembersU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mCFCA39437526D8F5F821AA01E6F091D427DC83E8 (void);
// 0x000001E0 System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__47::System.Collections.IEnumerator.Reset()
extern void U3CGetAllMembersU3Ed__47_System_Collections_IEnumerator_Reset_m302E849F1B3FF4F2D4DBD91F1DD290096360BCB8 (void);
// 0x000001E1 System.Object Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__47::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllMembersU3Ed__47_System_Collections_IEnumerator_get_Current_mF153491D0B84D4441736381581CC68B3A98BA0A2 (void);
// 0x000001E2 System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__47::System.Collections.Generic.IEnumerable<System.Reflection.MemberInfo>.GetEnumerator()
extern void U3CGetAllMembersU3Ed__47_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mA8F9DE86C331963278FDF18DF93DB6EB7EB53279 (void);
// 0x000001E3 System.Collections.IEnumerator Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__47::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllMembersU3Ed__47_System_Collections_IEnumerable_GetEnumerator_m928D1703B964CE1EE20B12BA2D97EEB4BAB57CC0 (void);
// 0x000001E4 System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__48::.ctor(System.Int32)
extern void U3CGetAllMembersU3Ed__48__ctor_m74FB21539C0CE45F69B20F16B136778209100131 (void);
// 0x000001E5 System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__48::System.IDisposable.Dispose()
extern void U3CGetAllMembersU3Ed__48_System_IDisposable_Dispose_mDC6CF1ED05A6886A31305EDB0ECAFDF69BD5F281 (void);
// 0x000001E6 System.Boolean Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__48::MoveNext()
extern void U3CGetAllMembersU3Ed__48_MoveNext_m0CD7B58F20AC6E1F71EB37705EAF5A3050CCF0D6 (void);
// 0x000001E7 System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__48::<>m__Finally1()
extern void U3CGetAllMembersU3Ed__48_U3CU3Em__Finally1_m5FA5AFDF6BBE696301FB2826352E559C8D8328DC (void);
// 0x000001E8 System.Reflection.MemberInfo Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__48::System.Collections.Generic.IEnumerator<System.Reflection.MemberInfo>.get_Current()
extern void U3CGetAllMembersU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mEBECBE836F8C46DF252F7AA27C8C834030E7E552 (void);
// 0x000001E9 System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__48::System.Collections.IEnumerator.Reset()
extern void U3CGetAllMembersU3Ed__48_System_Collections_IEnumerator_Reset_m355DF348D89CD24F596B9550FAB9F20EB378A9DE (void);
// 0x000001EA System.Object Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllMembersU3Ed__48_System_Collections_IEnumerator_get_Current_mA5CF1188BE7A66F80559A2A964C8A823DAC091FC (void);
// 0x000001EB System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__48::System.Collections.Generic.IEnumerable<System.Reflection.MemberInfo>.GetEnumerator()
extern void U3CGetAllMembersU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m038BE30DF1E5EA486E587D21F6248A4EA40A9A8A (void);
// 0x000001EC System.Collections.IEnumerator Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__48::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllMembersU3Ed__48_System_Collections_IEnumerable_GetEnumerator_mF8F4B8204B8DEC5EC9AD604CF1CFBD49EF6A1A4B (void);
// 0x000001ED System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__49`1::.ctor(System.Int32)
// 0x000001EE System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__49`1::System.IDisposable.Dispose()
// 0x000001EF System.Boolean Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__49`1::MoveNext()
// 0x000001F0 T Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__49`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000001F1 System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__49`1::System.Collections.IEnumerator.Reset()
// 0x000001F2 System.Object Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__49`1::System.Collections.IEnumerator.get_Current()
// 0x000001F3 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__49`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000001F4 System.Collections.IEnumerator Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__49`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000001F5 System.Void Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__53::.ctor(System.Int32)
extern void U3CGetBaseClassesU3Ed__53__ctor_m3860E822204D69032B9DF35BAA96A7221E7642B4 (void);
// 0x000001F6 System.Void Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__53::System.IDisposable.Dispose()
extern void U3CGetBaseClassesU3Ed__53_System_IDisposable_Dispose_mBB81E192B159F326D4337A450092C9277824DD7E (void);
// 0x000001F7 System.Boolean Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__53::MoveNext()
extern void U3CGetBaseClassesU3Ed__53_MoveNext_mA041FA869D04FBE7B2869A456361872172A09518 (void);
// 0x000001F8 System.Type Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__53::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern void U3CGetBaseClassesU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m7468303E90A96DBEB6AE1EA4DB0997787BDBEC76 (void);
// 0x000001F9 System.Void Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__53::System.Collections.IEnumerator.Reset()
extern void U3CGetBaseClassesU3Ed__53_System_Collections_IEnumerator_Reset_m88D4630F95FC04044E87AA5863FDC816880E9594 (void);
// 0x000001FA System.Object Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__53::System.Collections.IEnumerator.get_Current()
extern void U3CGetBaseClassesU3Ed__53_System_Collections_IEnumerator_get_Current_mA07CBB08458521DA21FB33BE8817F9273175D8DD (void);
// 0x000001FB System.Collections.Generic.IEnumerator`1<System.Type> Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__53::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern void U3CGetBaseClassesU3Ed__53_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_mDB62F4AE52A8FFACCE29476CD7A179B2E696B51E (void);
// 0x000001FC System.Collections.IEnumerator Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__53::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetBaseClassesU3Ed__53_System_Collections_IEnumerable_GetEnumerator_m6FC6E1FA9371736CB587416C661FE05B88ED8946 (void);
// 0x000001FD System.Void Sirenix.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::.ctor(System.Int32)
// 0x000001FE System.Void Sirenix.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::System.IDisposable.Dispose()
// 0x000001FF System.Boolean Sirenix.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::MoveNext()
// 0x00000200 T Sirenix.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000201 System.Void Sirenix.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::System.Collections.IEnumerator.Reset()
// 0x00000202 System.Object Sirenix.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::System.Collections.IEnumerator.get_Current()
// 0x00000203 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000204 System.Collections.IEnumerator Sirenix.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000205 System.Void Sirenix.Utilities.UnityExtensions::.cctor()
extern void UnityExtensions__cctor_m0D187706219372FD960055C9FC3AEB3879E4403E (void);
// 0x00000206 System.Boolean Sirenix.Utilities.UnityExtensions::SafeIsUnityNull(UnityEngine.Object)
extern void UnityExtensions_SafeIsUnityNull_m13F6AF07636FB33A74D33249B94ECA358E62DCE1 (void);
// 0x00000207 T[] Sirenix.Utilities.ArrayUtilities::CreateNewArrayWithAddedElement(T[],T)
// 0x00000208 T[] Sirenix.Utilities.ArrayUtilities::CreateNewArrayWithInsertedElement(T[],System.Int32,T)
// 0x00000209 T[] Sirenix.Utilities.ArrayUtilities::CreateNewArrayWithRemovedElement(T[],System.Int32)
// 0x0000020A System.Object Sirenix.Utilities.ICache::get_Value()
// 0x0000020B System.Int32 Sirenix.Utilities.Cache`1::get_MaxCacheSize()
// 0x0000020C System.Void Sirenix.Utilities.Cache`1::set_MaxCacheSize(System.Int32)
// 0x0000020D System.Void Sirenix.Utilities.Cache`1::.ctor()
// 0x0000020E System.Boolean Sirenix.Utilities.Cache`1::get_IsFree()
// 0x0000020F System.Object Sirenix.Utilities.Cache`1::Sirenix.Utilities.ICache.get_Value()
// 0x00000210 Sirenix.Utilities.Cache`1<T> Sirenix.Utilities.Cache`1::Claim()
// 0x00000211 System.Void Sirenix.Utilities.Cache`1::Release(Sirenix.Utilities.Cache`1<T>)
// 0x00000212 T Sirenix.Utilities.Cache`1::op_Implicit(Sirenix.Utilities.Cache`1<T>)
// 0x00000213 System.Void Sirenix.Utilities.Cache`1::Release()
// 0x00000214 System.Void Sirenix.Utilities.Cache`1::System.IDisposable.Dispose()
// 0x00000215 System.Void Sirenix.Utilities.Cache`1::.cctor()
// 0x00000216 System.Func`1<System.Object> Sirenix.Utilities.DeepReflection::CreateWeakStaticValueGetter(System.Type,System.Type,System.String,System.Boolean)
extern void DeepReflection_CreateWeakStaticValueGetter_m7CB798E0F0E1084181C127DF6859BA9C91E438BA (void);
// 0x00000217 System.Func`2<System.Object,System.Object> Sirenix.Utilities.DeepReflection::CreateWeakInstanceValueGetter(System.Type,System.Type,System.String,System.Boolean)
extern void DeepReflection_CreateWeakInstanceValueGetter_mD5E0EA98E92907DF00A46D9763DEF73D7E4654EF (void);
// 0x00000218 System.Action`2<System.Object,System.Object> Sirenix.Utilities.DeepReflection::CreateWeakInstanceValueSetter(System.Type,System.Type,System.String,System.Boolean)
extern void DeepReflection_CreateWeakInstanceValueSetter_m465072E6F95F97281E2381F3BEB767812C277BA7 (void);
// 0x00000219 System.Func`2<System.Object,TResult> Sirenix.Utilities.DeepReflection::CreateWeakInstanceValueGetter(System.Type,System.String,System.Boolean)
// 0x0000021A System.Func`1<TResult> Sirenix.Utilities.DeepReflection::CreateValueGetter(System.Type,System.String,System.Boolean)
// 0x0000021B System.Func`2<TTarget,TResult> Sirenix.Utilities.DeepReflection::CreateValueGetter(System.String,System.Boolean)
// 0x0000021C System.Func`2<System.Object,System.Object> Sirenix.Utilities.DeepReflection::CreateWeakAliasForInstanceGetDelegate1(System.Func`2<TTarget,TResult>)
// 0x0000021D System.Func`2<System.Object,TResult> Sirenix.Utilities.DeepReflection::CreateWeakAliasForInstanceGetDelegate2(System.Func`2<TTarget,TResult>)
// 0x0000021E System.Func`1<System.Object> Sirenix.Utilities.DeepReflection::CreateWeakAliasForStaticGetDelegate(System.Func`1<TResult>)
// 0x0000021F System.Action`2<System.Object,System.Object> Sirenix.Utilities.DeepReflection::CreateWeakAliasForInstanceSetDelegate1(System.Action`2<TTarget,TArg1>)
// 0x00000220 System.Action`2<System.Object,TArg1> Sirenix.Utilities.DeepReflection::CreateWeakAliasForInstanceSetDelegate2(System.Action`2<TTarget,TArg1>)
// 0x00000221 System.Action`1<System.Object> Sirenix.Utilities.DeepReflection::CreateWeakAliasForStaticSetDelegate(System.Action`1<TArg1>)
// 0x00000222 System.Delegate Sirenix.Utilities.DeepReflection::CreateEmittedDeepValueGetterDelegate(System.String,System.Type,System.Type,System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep>,System.Boolean)
extern void DeepReflection_CreateEmittedDeepValueGetterDelegate_m73147D746464D99B1BF220532CA007AE67092E27 (void);
// 0x00000223 System.Func`1<System.Object> Sirenix.Utilities.DeepReflection::CreateSlowDeepStaticValueGetterDelegate(System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep>)
extern void DeepReflection_CreateSlowDeepStaticValueGetterDelegate_mD1507C26516B56887AD361F319F21423CD00F0FD (void);
// 0x00000224 System.Func`2<System.Object,System.Object> Sirenix.Utilities.DeepReflection::CreateSlowDeepInstanceValueGetterDelegate(System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep>)
extern void DeepReflection_CreateSlowDeepInstanceValueGetterDelegate_m914B614497033FBF0155446FCF4A2C6770C69481 (void);
// 0x00000225 System.Action`2<System.Object,System.Object> Sirenix.Utilities.DeepReflection::CreateSlowDeepInstanceValueSetterDelegate(System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep>)
extern void DeepReflection_CreateSlowDeepInstanceValueSetterDelegate_mD2B015723D061E6B52259AEF3A8B63EAFF8AFE46 (void);
// 0x00000226 System.Object Sirenix.Utilities.DeepReflection::SlowGetMemberValue(Sirenix.Utilities.DeepReflection/PathStep,System.Object)
extern void DeepReflection_SlowGetMemberValue_m5AB0C302750A2A2FC2BF9460419C69730291F2C7 (void);
// 0x00000227 System.Void Sirenix.Utilities.DeepReflection::SlowSetMemberValue(Sirenix.Utilities.DeepReflection/PathStep,System.Object,System.Object)
extern void DeepReflection_SlowSetMemberValue_mB84D330B8C18183F0E463F4460BB70CA78477657 (void);
// 0x00000228 System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep> Sirenix.Utilities.DeepReflection::GetMemberPath(System.Type,System.Type&,System.String,System.Boolean&,System.Boolean)
extern void DeepReflection_GetMemberPath_m30C1E442AB295E8DCBFA643B40F382F0312F4882 (void);
// 0x00000229 System.Reflection.MemberInfo Sirenix.Utilities.DeepReflection::GetStepMember(System.Type,System.String,System.Boolean)
extern void DeepReflection_GetStepMember_m17710C7EFA9C95343450A2984874D5657C82AE75 (void);
// 0x0000022A System.Void Sirenix.Utilities.DeepReflection::.cctor()
extern void DeepReflection__cctor_m321AD4286CF1C87262CD38F1A185AFF046F95C59 (void);
// 0x0000022B System.Void Sirenix.Utilities.DeepReflection/PathStep::.ctor(System.Reflection.MemberInfo)
extern void PathStep__ctor_mB52683D0E76A1108856405623013CD634DD0045C (void);
// 0x0000022C System.Void Sirenix.Utilities.DeepReflection/PathStep::.ctor(System.Int32)
extern void PathStep__ctor_mDDB9DFE8DC648E7D5A6A1ED81FDDC568AFB85B7D (void);
// 0x0000022D System.Void Sirenix.Utilities.DeepReflection/PathStep::.ctor(System.Int32,System.Type,System.Boolean)
extern void PathStep__ctor_m13D1858B708B2DC0C87519F5D97241B1D4C8DF8E (void);
// 0x0000022E System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass11_0`1::.ctor()
// 0x0000022F TResult Sirenix.Utilities.DeepReflection/<>c__DisplayClass11_0`1::<CreateWeakInstanceValueGetter>b__0(System.Object)
// 0x00000230 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass12_0`1::.ctor()
// 0x00000231 TResult Sirenix.Utilities.DeepReflection/<>c__DisplayClass12_0`1::<CreateValueGetter>b__0()
// 0x00000232 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass13_0`2::.ctor()
// 0x00000233 TResult Sirenix.Utilities.DeepReflection/<>c__DisplayClass13_0`2::<CreateValueGetter>b__0(TTarget)
// 0x00000234 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass14_0`2::.ctor()
// 0x00000235 System.Object Sirenix.Utilities.DeepReflection/<>c__DisplayClass14_0`2::<CreateWeakAliasForInstanceGetDelegate1>b__0(System.Object)
// 0x00000236 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass15_0`2::.ctor()
// 0x00000237 TResult Sirenix.Utilities.DeepReflection/<>c__DisplayClass15_0`2::<CreateWeakAliasForInstanceGetDelegate2>b__0(System.Object)
// 0x00000238 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass16_0`1::.ctor()
// 0x00000239 System.Object Sirenix.Utilities.DeepReflection/<>c__DisplayClass16_0`1::<CreateWeakAliasForStaticGetDelegate>b__0()
// 0x0000023A System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass17_0`2::.ctor()
// 0x0000023B System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass17_0`2::<CreateWeakAliasForInstanceSetDelegate1>b__0(System.Object,System.Object)
// 0x0000023C System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass18_0`2::.ctor()
// 0x0000023D System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass18_0`2::<CreateWeakAliasForInstanceSetDelegate2>b__0(System.Object,TArg1)
// 0x0000023E System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass19_0`1::.ctor()
// 0x0000023F System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass19_0`1::<CreateWeakAliasForStaticSetDelegate>b__0(System.Object)
// 0x00000240 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mE8326B8DD7F367F8621D35E382A3671B563E07AF (void);
// 0x00000241 System.Object Sirenix.Utilities.DeepReflection/<>c__DisplayClass21_0::<CreateSlowDeepStaticValueGetterDelegate>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CCreateSlowDeepStaticValueGetterDelegateU3Eb__0_m99D8C06D4886A529C9AB0B9842B44D19C0446F98 (void);
// 0x00000242 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_mAD37F46F1CEF3B99ECF3762E73E90C4333B7DD65 (void);
// 0x00000243 System.Object Sirenix.Utilities.DeepReflection/<>c__DisplayClass22_0::<CreateSlowDeepInstanceValueGetterDelegate>b__0(System.Object)
extern void U3CU3Ec__DisplayClass22_0_U3CCreateSlowDeepInstanceValueGetterDelegateU3Eb__0_m58B5D11EF60292BC90EA2E6DC8EF9EC07F7BC706 (void);
// 0x00000244 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mD7EABEAD23B7296A6F36AA0F4CD424F50620D16A (void);
// 0x00000245 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass23_0::<CreateSlowDeepInstanceValueSetterDelegate>b__0(System.Object,System.Object)
extern void U3CU3Ec__DisplayClass23_0_U3CCreateSlowDeepInstanceValueSetterDelegateU3Eb__0_m001D8499A30A599738D407CC4F33CC6CBCCF767A (void);
// 0x00000246 System.Void Sirenix.Utilities.DoubleLookupDictionary`3::.ctor()
// 0x00000247 System.Void Sirenix.Utilities.DoubleLookupDictionary`3::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirstKey>,System.Collections.Generic.IEqualityComparer`1<TSecondKey>)
// 0x00000248 System.Collections.Generic.Dictionary`2<TSecondKey,TValue> Sirenix.Utilities.DoubleLookupDictionary`3::get_Item(TFirstKey)
// 0x00000249 System.Int32 Sirenix.Utilities.DoubleLookupDictionary`3::InnerCount(TFirstKey)
// 0x0000024A System.Int32 Sirenix.Utilities.DoubleLookupDictionary`3::TotalInnerCount()
// 0x0000024B System.Boolean Sirenix.Utilities.DoubleLookupDictionary`3::ContainsKeys(TFirstKey,TSecondKey)
// 0x0000024C System.Boolean Sirenix.Utilities.DoubleLookupDictionary`3::TryGetInnerValue(TFirstKey,TSecondKey,TValue&)
// 0x0000024D TValue Sirenix.Utilities.DoubleLookupDictionary`3::AddInner(TFirstKey,TSecondKey,TValue)
// 0x0000024E System.Boolean Sirenix.Utilities.DoubleLookupDictionary`3::RemoveInner(TFirstKey,TSecondKey)
// 0x0000024F System.Void Sirenix.Utilities.DoubleLookupDictionary`3::RemoveWhere(System.Func`2<TValue,System.Boolean>)
// 0x00000250 System.Void Sirenix.Utilities.WeakValueGetter::.ctor(System.Object,System.IntPtr)
extern void WeakValueGetter__ctor_mF70F2B34AAE2E2142E2C154E7E47653254AAE0E9 (void);
// 0x00000251 System.Object Sirenix.Utilities.WeakValueGetter::Invoke(System.Object&)
extern void WeakValueGetter_Invoke_m7343377557FD99EC7DA973A5A838DF8C763D9445 (void);
// 0x00000252 System.IAsyncResult Sirenix.Utilities.WeakValueGetter::BeginInvoke(System.Object&,System.AsyncCallback,System.Object)
extern void WeakValueGetter_BeginInvoke_m25DAA67B6CF910CFFA8566D5F02468CC8444226F (void);
// 0x00000253 System.Object Sirenix.Utilities.WeakValueGetter::EndInvoke(System.Object&,System.IAsyncResult)
extern void WeakValueGetter_EndInvoke_m5325EB1F451D5B1D1A7C0DC3A60B154BB1BBC44C (void);
// 0x00000254 System.Void Sirenix.Utilities.WeakValueSetter::.ctor(System.Object,System.IntPtr)
extern void WeakValueSetter__ctor_m820EE8552D61007F82C368AE697E6EBD323C091A (void);
// 0x00000255 System.Void Sirenix.Utilities.WeakValueSetter::Invoke(System.Object&,System.Object)
extern void WeakValueSetter_Invoke_mDC51ADD44D4B6E5EBC6FBA28C320644AC6FC8E06 (void);
// 0x00000256 System.IAsyncResult Sirenix.Utilities.WeakValueSetter::BeginInvoke(System.Object&,System.Object,System.AsyncCallback,System.Object)
extern void WeakValueSetter_BeginInvoke_mAB8B0370E3503087745FDEEEA1687673C783492B (void);
// 0x00000257 System.Void Sirenix.Utilities.WeakValueSetter::EndInvoke(System.Object&,System.IAsyncResult)
extern void WeakValueSetter_EndInvoke_m352BB4577611876F53B63A5574AB284868EFF3D8 (void);
// 0x00000258 System.Void Sirenix.Utilities.WeakValueGetter`1::.ctor(System.Object,System.IntPtr)
// 0x00000259 FieldType Sirenix.Utilities.WeakValueGetter`1::Invoke(System.Object&)
// 0x0000025A System.IAsyncResult Sirenix.Utilities.WeakValueGetter`1::BeginInvoke(System.Object&,System.AsyncCallback,System.Object)
// 0x0000025B FieldType Sirenix.Utilities.WeakValueGetter`1::EndInvoke(System.Object&,System.IAsyncResult)
// 0x0000025C System.Void Sirenix.Utilities.WeakValueSetter`1::.ctor(System.Object,System.IntPtr)
// 0x0000025D System.Void Sirenix.Utilities.WeakValueSetter`1::Invoke(System.Object&,FieldType)
// 0x0000025E System.IAsyncResult Sirenix.Utilities.WeakValueSetter`1::BeginInvoke(System.Object&,FieldType,System.AsyncCallback,System.Object)
// 0x0000025F System.Void Sirenix.Utilities.WeakValueSetter`1::EndInvoke(System.Object&,System.IAsyncResult)
// 0x00000260 System.Void Sirenix.Utilities.ValueGetter`2::.ctor(System.Object,System.IntPtr)
// 0x00000261 FieldType Sirenix.Utilities.ValueGetter`2::Invoke(InstanceType&)
// 0x00000262 System.IAsyncResult Sirenix.Utilities.ValueGetter`2::BeginInvoke(InstanceType&,System.AsyncCallback,System.Object)
// 0x00000263 FieldType Sirenix.Utilities.ValueGetter`2::EndInvoke(InstanceType&,System.IAsyncResult)
// 0x00000264 System.Void Sirenix.Utilities.ValueSetter`2::.ctor(System.Object,System.IntPtr)
// 0x00000265 System.Void Sirenix.Utilities.ValueSetter`2::Invoke(InstanceType&,FieldType)
// 0x00000266 System.IAsyncResult Sirenix.Utilities.ValueSetter`2::BeginInvoke(InstanceType&,FieldType,System.AsyncCallback,System.Object)
// 0x00000267 System.Void Sirenix.Utilities.ValueSetter`2::EndInvoke(InstanceType&,System.IAsyncResult)
// 0x00000268 System.Boolean Sirenix.Utilities.EmitUtilities::get_CanEmit()
extern void EmitUtilities_get_CanEmit_mEEE05DA6926EAA48D4DA77B2B0511A7B5F60FC9D (void);
// 0x00000269 System.Func`1<FieldType> Sirenix.Utilities.EmitUtilities::CreateStaticFieldGetter(System.Reflection.FieldInfo)
// 0x0000026A System.Func`1<System.Object> Sirenix.Utilities.EmitUtilities::CreateWeakStaticFieldGetter(System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakStaticFieldGetter_m12ACB9D134C2EFB21B24F7D215DD20C4B3D3BEF4 (void);
// 0x0000026B System.Action`1<FieldType> Sirenix.Utilities.EmitUtilities::CreateStaticFieldSetter(System.Reflection.FieldInfo)
// 0x0000026C System.Action`1<System.Object> Sirenix.Utilities.EmitUtilities::CreateWeakStaticFieldSetter(System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakStaticFieldSetter_mB43A2E2F392A9658431A8FAC332D83B904ABBC4E (void);
// 0x0000026D Sirenix.Utilities.ValueGetter`2<InstanceType,FieldType> Sirenix.Utilities.EmitUtilities::CreateInstanceFieldGetter(System.Reflection.FieldInfo)
// 0x0000026E Sirenix.Utilities.WeakValueGetter`1<FieldType> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceFieldGetter(System.Type,System.Reflection.FieldInfo)
// 0x0000026F Sirenix.Utilities.WeakValueGetter Sirenix.Utilities.EmitUtilities::CreateWeakInstanceFieldGetter(System.Type,System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakInstanceFieldGetter_m6FF91BE2964C5E89931E29E0A53F68D71669CDF4 (void);
// 0x00000270 Sirenix.Utilities.ValueSetter`2<InstanceType,FieldType> Sirenix.Utilities.EmitUtilities::CreateInstanceFieldSetter(System.Reflection.FieldInfo)
// 0x00000271 Sirenix.Utilities.WeakValueSetter`1<FieldType> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceFieldSetter(System.Type,System.Reflection.FieldInfo)
// 0x00000272 Sirenix.Utilities.WeakValueSetter Sirenix.Utilities.EmitUtilities::CreateWeakInstanceFieldSetter(System.Type,System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakInstanceFieldSetter_m493885E1D5C6331544B03CB4F3AE879BC40F093C (void);
// 0x00000273 Sirenix.Utilities.WeakValueGetter Sirenix.Utilities.EmitUtilities::CreateWeakInstancePropertyGetter(System.Type,System.Reflection.PropertyInfo)
extern void EmitUtilities_CreateWeakInstancePropertyGetter_mD8DD83D429C91BDEA82634330C878DA09957D10F (void);
// 0x00000274 Sirenix.Utilities.WeakValueSetter Sirenix.Utilities.EmitUtilities::CreateWeakInstancePropertySetter(System.Type,System.Reflection.PropertyInfo)
extern void EmitUtilities_CreateWeakInstancePropertySetter_mD3191C704184E1F84E03467DAC0BC0F300360369 (void);
// 0x00000275 System.Action`1<PropType> Sirenix.Utilities.EmitUtilities::CreateStaticPropertySetter(System.Reflection.PropertyInfo)
// 0x00000276 System.Func`1<PropType> Sirenix.Utilities.EmitUtilities::CreateStaticPropertyGetter(System.Reflection.PropertyInfo)
// 0x00000277 Sirenix.Utilities.ValueSetter`2<InstanceType,PropType> Sirenix.Utilities.EmitUtilities::CreateInstancePropertySetter(System.Reflection.PropertyInfo)
// 0x00000278 Sirenix.Utilities.ValueGetter`2<InstanceType,PropType> Sirenix.Utilities.EmitUtilities::CreateInstancePropertyGetter(System.Reflection.PropertyInfo)
// 0x00000279 System.Func`2<InstanceType,ReturnType> Sirenix.Utilities.EmitUtilities::CreateMethodReturner(System.Reflection.MethodInfo)
// 0x0000027A System.Action Sirenix.Utilities.EmitUtilities::CreateStaticMethodCaller(System.Reflection.MethodInfo)
extern void EmitUtilities_CreateStaticMethodCaller_m0751556DFD7D6F381FDB49BEBF732D9448676FBC (void);
// 0x0000027B System.Action`2<System.Object,TArg1> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x0000027C System.Action`1<System.Object> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceMethodCaller(System.Reflection.MethodInfo)
extern void EmitUtilities_CreateWeakInstanceMethodCaller_m62A20A4052B5B1EDBB6325871BCBF8E5456ACC64 (void);
// 0x0000027D System.Func`3<System.Object,TArg1,TResult> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x0000027E System.Func`2<System.Object,TResult> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceMethodCallerFunc(System.Reflection.MethodInfo)
// 0x0000027F System.Func`3<System.Object,TArg,TResult> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceMethodCallerFunc(System.Reflection.MethodInfo)
// 0x00000280 System.Action`1<InstanceType> Sirenix.Utilities.EmitUtilities::CreateInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x00000281 System.Action`2<InstanceType,Arg1> Sirenix.Utilities.EmitUtilities::CreateInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x00000282 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass2_0`1::.ctor()
// 0x00000283 FieldType Sirenix.Utilities.EmitUtilities/<>c__DisplayClass2_0`1::<CreateStaticFieldGetter>b__0()
// 0x00000284 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass2_1`1::.ctor()
// 0x00000285 FieldType Sirenix.Utilities.EmitUtilities/<>c__DisplayClass2_1`1::<CreateStaticFieldGetter>b__1()
// 0x00000286 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m11E41571289025E78307FB4EBBE3548356441A00 (void);
// 0x00000287 System.Object Sirenix.Utilities.EmitUtilities/<>c__DisplayClass3_0::<CreateWeakStaticFieldGetter>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CCreateWeakStaticFieldGetterU3Eb__0_mBF92B435913B6873671BED19EE397E49D3DA9841 (void);
// 0x00000288 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass4_0`1::.ctor()
// 0x00000289 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass4_0`1::<CreateStaticFieldSetter>b__0(FieldType)
// 0x0000028A System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m6E0E3439FE97D07B9ABEF22223D6704B34F68182 (void);
// 0x0000028B System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass5_0::<CreateWeakStaticFieldSetter>b__0(System.Object)
extern void U3CU3Ec__DisplayClass5_0_U3CCreateWeakStaticFieldSetterU3Eb__0_m9404E80A3EE5F70BCECA37E12BC062C9C1471402 (void);
// 0x0000028C System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass6_0`2::.ctor()
// 0x0000028D FieldType Sirenix.Utilities.EmitUtilities/<>c__DisplayClass6_0`2::<CreateInstanceFieldGetter>b__0(InstanceType&)
// 0x0000028E System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass7_0`1::.ctor()
// 0x0000028F FieldType Sirenix.Utilities.EmitUtilities/<>c__DisplayClass7_0`1::<CreateWeakInstanceFieldGetter>b__0(System.Object&)
// 0x00000290 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mE35A54E871C8C15C0298DF03B3A9B1A520B12C39 (void);
// 0x00000291 System.Object Sirenix.Utilities.EmitUtilities/<>c__DisplayClass8_0::<CreateWeakInstanceFieldGetter>b__0(System.Object&)
extern void U3CU3Ec__DisplayClass8_0_U3CCreateWeakInstanceFieldGetterU3Eb__0_m24D8FC28CFD2DFCE3F646A39D4BEFE81092072C2 (void);
// 0x00000292 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass9_0`2::.ctor()
// 0x00000293 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass9_0`2::<CreateInstanceFieldSetter>b__0(InstanceType&,FieldType)
// 0x00000294 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass10_0`1::.ctor()
// 0x00000295 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass10_0`1::<CreateWeakInstanceFieldSetter>b__0(System.Object&,FieldType)
// 0x00000296 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m5C44409E6876809A816D37968354A9DD21429580 (void);
// 0x00000297 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass11_0::<CreateWeakInstanceFieldSetter>b__0(System.Object&,System.Object)
extern void U3CU3Ec__DisplayClass11_0_U3CCreateWeakInstanceFieldSetterU3Eb__0_mD6D53102BD0E3F8C6786A4BA35DA5E6225CAE2CE (void);
// 0x00000298 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mF308CA2ABF7D747C8AF08729277EA3F6D4AFA2D6 (void);
// 0x00000299 System.Object Sirenix.Utilities.EmitUtilities/<>c__DisplayClass12_0::<CreateWeakInstancePropertyGetter>b__0(System.Object&)
extern void U3CU3Ec__DisplayClass12_0_U3CCreateWeakInstancePropertyGetterU3Eb__0_m6A31165FF2D41AEC9A8EE37A01BC6B4AF9D72CD7 (void);
// 0x0000029A System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m2FCFA4B0FEFFF0891FDDB450F35337E43FCF5ECF (void);
// 0x0000029B System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass13_0::<CreateWeakInstancePropertySetter>b__0(System.Object&,System.Object)
extern void U3CU3Ec__DisplayClass13_0_U3CCreateWeakInstancePropertySetterU3Eb__0_m30D08F80BFB6D4D675D0748D0FCD56D3E17D5F74 (void);
// 0x0000029C System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass14_0`1::.ctor()
// 0x0000029D System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass14_0`1::<CreateStaticPropertySetter>b__0(PropType)
// 0x0000029E System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass15_0`1::.ctor()
// 0x0000029F PropType Sirenix.Utilities.EmitUtilities/<>c__DisplayClass15_0`1::<CreateStaticPropertyGetter>b__0()
// 0x000002A0 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass16_0`2::.ctor()
// 0x000002A1 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass16_0`2::<CreateInstancePropertySetter>b__0(InstanceType&,PropType)
// 0x000002A2 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass17_0`2::.ctor()
// 0x000002A3 PropType Sirenix.Utilities.EmitUtilities/<>c__DisplayClass17_0`2::<CreateInstancePropertyGetter>b__0(InstanceType&)
// 0x000002A4 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass20_0`1::.ctor()
// 0x000002A5 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass20_0`1::<CreateWeakInstanceMethodCaller>b__0(System.Object,TArg1)
// 0x000002A6 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mBB5540B4858B6686D3B2846893148E2CBF6B260B (void);
// 0x000002A7 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass21_0::<CreateWeakInstanceMethodCaller>b__0(System.Object)
extern void U3CU3Ec__DisplayClass21_0_U3CCreateWeakInstanceMethodCallerU3Eb__0_mF9ADFF9980EADEFD9F7FAB20B083F39C65CB3EB2 (void);
// 0x000002A8 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass22_0`2::.ctor()
// 0x000002A9 TResult Sirenix.Utilities.EmitUtilities/<>c__DisplayClass22_0`2::<CreateWeakInstanceMethodCaller>b__0(System.Object,TArg1)
// 0x000002AA System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass23_0`1::.ctor()
// 0x000002AB TResult Sirenix.Utilities.EmitUtilities/<>c__DisplayClass23_0`1::<CreateWeakInstanceMethodCallerFunc>b__0(System.Object)
// 0x000002AC System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass24_0`2::.ctor()
// 0x000002AD TResult Sirenix.Utilities.EmitUtilities/<>c__DisplayClass24_0`2::<CreateWeakInstanceMethodCallerFunc>b__0(System.Object,TArg)
// 0x000002AE Sirenix.Utilities.GlobalConfigAttribute Sirenix.Utilities.GlobalConfig`1::get_ConfigAttribute()
// 0x000002AF System.Boolean Sirenix.Utilities.GlobalConfig`1::get_HasInstanceLoaded()
// 0x000002B0 T Sirenix.Utilities.GlobalConfig`1::get_Instance()
// 0x000002B1 System.Void Sirenix.Utilities.GlobalConfig`1::LoadInstanceIfAssetExists()
// 0x000002B2 System.Void Sirenix.Utilities.GlobalConfig`1::OpenInEditor()
// 0x000002B3 System.Void Sirenix.Utilities.GlobalConfig`1::OnConfigAutoCreated()
// 0x000002B4 System.Void Sirenix.Utilities.GlobalConfig`1::.ctor()
// 0x000002B5 System.String Sirenix.Utilities.GlobalConfigAttribute::get_FullPath()
extern void GlobalConfigAttribute_get_FullPath_m07FC67A8EDA4A84F33D19A32CEC093BEEE0A6D22 (void);
// 0x000002B6 System.String Sirenix.Utilities.GlobalConfigAttribute::get_AssetPath()
extern void GlobalConfigAttribute_get_AssetPath_m427BBDBB1149BA9F468A8CDA286595188C492056 (void);
// 0x000002B7 System.String Sirenix.Utilities.GlobalConfigAttribute::get_AssetPathWithAssetsPrefix()
extern void GlobalConfigAttribute_get_AssetPathWithAssetsPrefix_mA5A6A0003C711786C45B717B336AFF025FA6DC20 (void);
// 0x000002B8 System.String Sirenix.Utilities.GlobalConfigAttribute::get_AssetPathWithoutAssetsPrefix()
extern void GlobalConfigAttribute_get_AssetPathWithoutAssetsPrefix_m5D3C44A53E103C68DC9D940B4CE1368CAC21FE90 (void);
// 0x000002B9 System.String Sirenix.Utilities.GlobalConfigAttribute::get_ResourcesPath()
extern void GlobalConfigAttribute_get_ResourcesPath_mB30337DEA7AD51CB4BFEDAB7E4FA26EA7CB21454 (void);
// 0x000002BA System.Boolean Sirenix.Utilities.GlobalConfigAttribute::get_UseAsset()
extern void GlobalConfigAttribute_get_UseAsset_m9856D2CB6C77E94FE7B3912A8B737B037915BA9C (void);
// 0x000002BB System.Void Sirenix.Utilities.GlobalConfigAttribute::set_UseAsset(System.Boolean)
extern void GlobalConfigAttribute_set_UseAsset_m93174EE36A6B1104173D79C950F42B2F26F23F5A (void);
// 0x000002BC System.Boolean Sirenix.Utilities.GlobalConfigAttribute::get_IsInResourcesFolder()
extern void GlobalConfigAttribute_get_IsInResourcesFolder_mAC8EC0816E0082A69C5466052F3967F3D7AE9D0C (void);
// 0x000002BD System.Void Sirenix.Utilities.GlobalConfigAttribute::.ctor()
extern void GlobalConfigAttribute__ctor_mF902E491F687BAE3659492840D2177106137C202 (void);
// 0x000002BE System.Void Sirenix.Utilities.GlobalConfigAttribute::.ctor(System.String)
extern void GlobalConfigAttribute__ctor_mC3412D28ED4D5644F652B3DC0D38EB333BC32C61 (void);
// 0x000002BF System.Void Sirenix.Utilities.GUILayoutOptions::.cctor()
extern void GUILayoutOptions__cctor_m476A43587EB7F1AB2F4F205488C4622C5FE58445 (void);
// 0x000002C0 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::Width(System.Single)
extern void GUILayoutOptions_Width_mA2D5B0EA8DB946BDE0AAEC6F7F566E2981EB5FD0 (void);
// 0x000002C1 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::Height(System.Single)
extern void GUILayoutOptions_Height_m6D046D7D797DAC01C86DB8F12468F39DDC31ABE7 (void);
// 0x000002C2 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::MaxHeight(System.Single)
extern void GUILayoutOptions_MaxHeight_m1B9834D060BDD8617985B8FE02D964629BFED9F6 (void);
// 0x000002C3 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::MaxWidth(System.Single)
extern void GUILayoutOptions_MaxWidth_m656793C57F6835B7651CCF41853B979190E3F2B6 (void);
// 0x000002C4 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::MinWidth(System.Single)
extern void GUILayoutOptions_MinWidth_mB8B3103776789E2CAA55F0461694581A84CAF483 (void);
// 0x000002C5 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::MinHeight(System.Single)
extern void GUILayoutOptions_MinHeight_mF3265FFDC7E3D9909A0D9B46BAE91F50AEDCEAB1 (void);
// 0x000002C6 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::ExpandHeight(System.Boolean)
extern void GUILayoutOptions_ExpandHeight_m2417E4DFAA2117877F5627EE9718F6AD389B119B (void);
// 0x000002C7 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::ExpandWidth(System.Boolean)
extern void GUILayoutOptions_ExpandWidth_mBB4033514B29B1117426E5780C6D918648F0FFC6 (void);
// 0x000002C8 UnityEngine.GUILayoutOption[] Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::GetCachedOptions()
extern void GUILayoutOptionsInstance_GetCachedOptions_m7D4F9C3AD94A8E947601FF654236D3C4AAE7F589 (void);
// 0x000002C9 UnityEngine.GUILayoutOption[] Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::op_Implicit(Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance)
extern void GUILayoutOptionsInstance_op_Implicit_mAEA3D249416FB375D3690A6A0E76A0D6CE0F9A2D (void);
// 0x000002CA UnityEngine.GUILayoutOption[] Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::CreateOptionsArary()
extern void GUILayoutOptionsInstance_CreateOptionsArary_m3A418EC638AAB846F4C7BE7B429E71FD206036E1 (void);
// 0x000002CB Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::Clone()
extern void GUILayoutOptionsInstance_Clone_mD51C24C022503E715B70EF9A6C63E8F4EDB38529 (void);
// 0x000002CC System.Void Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::.ctor()
extern void GUILayoutOptionsInstance__ctor_mACB4369C1F5BBDD40D0FE54B4B0E19D2C4F7ED5E (void);
// 0x000002CD Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::Width(System.Single)
extern void GUILayoutOptionsInstance_Width_m285EEAAA11A7EE470B835DA9420A73EA16F349DD (void);
// 0x000002CE Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::Height(System.Single)
extern void GUILayoutOptionsInstance_Height_m1CCA12E04F9D14F9802A2B7D6C168F7AE31A45FA (void);
// 0x000002CF Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::MaxHeight(System.Single)
extern void GUILayoutOptionsInstance_MaxHeight_m75C2927279F6119B03144E927AE3AD9C58772D5D (void);
// 0x000002D0 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::MaxWidth(System.Single)
extern void GUILayoutOptionsInstance_MaxWidth_mE57D9D290339E0A9DFBC47BD5E54A00639725958 (void);
// 0x000002D1 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::MinHeight(System.Single)
extern void GUILayoutOptionsInstance_MinHeight_m6B5ECBB1753A763903BA4452817B89CBF36E4AEA (void);
// 0x000002D2 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::MinWidth(System.Single)
extern void GUILayoutOptionsInstance_MinWidth_mC055C2D48A3651A1F230F9F77E93842C5707C350 (void);
// 0x000002D3 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::ExpandHeight(System.Boolean)
extern void GUILayoutOptionsInstance_ExpandHeight_m7F0B05372FED5BB8CC8C3A0D2A79E5662AE4E88E (void);
// 0x000002D4 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::ExpandWidth(System.Boolean)
extern void GUILayoutOptionsInstance_ExpandWidth_m12B9AEC4DCF98ACA50DDF71B0D134F5CAD4DD4B9 (void);
// 0x000002D5 System.Void Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::SetValue(Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionType,System.Single)
extern void GUILayoutOptionsInstance_SetValue_mAEA1B52B86FEA7742ACA48E47EAD897D51274F19 (void);
// 0x000002D6 System.Void Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::SetValue(Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionType,System.Boolean)
extern void GUILayoutOptionsInstance_SetValue_m62582CDD8E478FE5BC93051A974BDF563C003AA2 (void);
// 0x000002D7 System.Boolean Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::Equals(Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance)
extern void GUILayoutOptionsInstance_Equals_mDB344B170317F987100BA55DADA16EC8F4350DC9 (void);
// 0x000002D8 System.Int32 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::GetHashCode()
extern void GUILayoutOptionsInstance_GetHashCode_m3CF8A827F96FC884013426376D313C3F6AD2E340 (void);
// 0x000002D9 System.Void Sirenix.Utilities.ICacheNotificationReceiver::OnFreed()
// 0x000002DA System.Void Sirenix.Utilities.ICacheNotificationReceiver::OnClaimed()
// 0x000002DB System.Void Sirenix.Utilities.ImmutableHashSet`1::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000002DC System.Boolean Sirenix.Utilities.ImmutableHashSet`1::Contains(T)
// 0x000002DD System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.ImmutableHashSet`1::GetEnumerator()
// 0x000002DE System.Collections.IEnumerator Sirenix.Utilities.ImmutableHashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000002DF T Sirenix.Utilities.IImmutableList`1::get_Item(System.Int32)
// 0x000002E0 System.Void Sirenix.Utilities.ImmutableList::.ctor(System.Collections.IList)
extern void ImmutableList__ctor_m3A86E3041EF3FA6B4A457545AC8209BA3004E626 (void);
// 0x000002E1 System.Int32 Sirenix.Utilities.ImmutableList::get_Count()
extern void ImmutableList_get_Count_m022B612F98D5E10DD1778661DC2A92D481C8F7BB (void);
// 0x000002E2 System.Boolean Sirenix.Utilities.ImmutableList::get_IsFixedSize()
extern void ImmutableList_get_IsFixedSize_m30BF44E714C285B10AADD1F60773E07CF647D6AB (void);
// 0x000002E3 System.Boolean Sirenix.Utilities.ImmutableList::get_IsReadOnly()
extern void ImmutableList_get_IsReadOnly_m226A23AB281FBE2FEAE3A193D7E92CE0B2107248 (void);
// 0x000002E4 System.Boolean Sirenix.Utilities.ImmutableList::get_IsSynchronized()
extern void ImmutableList_get_IsSynchronized_m307640C287150EBD9E46072A25A525E642169FFF (void);
// 0x000002E5 System.Object Sirenix.Utilities.ImmutableList::get_SyncRoot()
extern void ImmutableList_get_SyncRoot_mF38CE30571B823471F6FAD29CBAEF763898680EB (void);
// 0x000002E6 System.Object Sirenix.Utilities.ImmutableList::System.Collections.IList.get_Item(System.Int32)
extern void ImmutableList_System_Collections_IList_get_Item_mA4D9CB5C723608304FEB6B89087389B0E12024DB (void);
// 0x000002E7 System.Void Sirenix.Utilities.ImmutableList::System.Collections.IList.set_Item(System.Int32,System.Object)
extern void ImmutableList_System_Collections_IList_set_Item_m4FC71C93B753F4956BDF02BB2CAA74FA1BFC00B7 (void);
// 0x000002E8 System.Object Sirenix.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.get_Item(System.Int32)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_get_Item_m667B25A5D3AB12D69E3DECD98C9218B31E1C3BBF (void);
// 0x000002E9 System.Void Sirenix.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.set_Item(System.Int32,System.Object)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_set_Item_m8DF41063F7D14E93EFCE87CE2F2E64EDA20E3A3E (void);
// 0x000002EA System.Object Sirenix.Utilities.ImmutableList::get_Item(System.Int32)
extern void ImmutableList_get_Item_mA505123D05C082B726104B9CCBA0DFE46C325B6C (void);
// 0x000002EB System.Boolean Sirenix.Utilities.ImmutableList::Contains(System.Object)
extern void ImmutableList_Contains_m1302761C267A54B73DB7C1775FF93ADB47F1A3C1 (void);
// 0x000002EC System.Void Sirenix.Utilities.ImmutableList::CopyTo(System.Object[],System.Int32)
extern void ImmutableList_CopyTo_mD90DDB89555209C8D96EAF860A82E99EB130E070 (void);
// 0x000002ED System.Void Sirenix.Utilities.ImmutableList::CopyTo(System.Array,System.Int32)
extern void ImmutableList_CopyTo_m48EE4ED3A35585ABF769C77161D19E4330126545 (void);
// 0x000002EE System.Collections.IEnumerator Sirenix.Utilities.ImmutableList::GetEnumerator()
extern void ImmutableList_GetEnumerator_m66C41FDF6A2E078837658672A98773E949C5CC7B (void);
// 0x000002EF System.Collections.IEnumerator Sirenix.Utilities.ImmutableList::System.Collections.IEnumerable.GetEnumerator()
extern void ImmutableList_System_Collections_IEnumerable_GetEnumerator_m6BC2144DC7020F7C16F2AD9D05E3C3D5F9988992 (void);
// 0x000002F0 System.Collections.Generic.IEnumerator`1<System.Object> Sirenix.Utilities.ImmutableList::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
extern void ImmutableList_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m0E35F57B4068265055F4956A5D47CA90FDD1CFB3 (void);
// 0x000002F1 System.Int32 Sirenix.Utilities.ImmutableList::System.Collections.IList.Add(System.Object)
extern void ImmutableList_System_Collections_IList_Add_m9BB676E06BCC478C6028780AB735F45FD8D4E151 (void);
// 0x000002F2 System.Void Sirenix.Utilities.ImmutableList::System.Collections.IList.Clear()
extern void ImmutableList_System_Collections_IList_Clear_m4BF7931F9ED3F05DB4F895E3755D16CC80697844 (void);
// 0x000002F3 System.Void Sirenix.Utilities.ImmutableList::System.Collections.IList.Insert(System.Int32,System.Object)
extern void ImmutableList_System_Collections_IList_Insert_mFD10BBF3202AC23CF934EA7D9A2264A248BC0EA9 (void);
// 0x000002F4 System.Void Sirenix.Utilities.ImmutableList::System.Collections.IList.Remove(System.Object)
extern void ImmutableList_System_Collections_IList_Remove_m31C5D8B6EE9311FD453AF711CC202DF8F3DAAFA6 (void);
// 0x000002F5 System.Void Sirenix.Utilities.ImmutableList::System.Collections.IList.RemoveAt(System.Int32)
extern void ImmutableList_System_Collections_IList_RemoveAt_mF07593D4830DAB5EE31632BED6898552BD3C6DB3 (void);
// 0x000002F6 System.Int32 Sirenix.Utilities.ImmutableList::IndexOf(System.Object)
extern void ImmutableList_IndexOf_m26A37AB82763EA4EC38B24D2E1EA4D67CB9468D5 (void);
// 0x000002F7 System.Void Sirenix.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.RemoveAt(System.Int32)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_RemoveAt_m25DF0E4242E333A5CE846043A7F2AD46D47D696F (void);
// 0x000002F8 System.Void Sirenix.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.Insert(System.Int32,System.Object)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_Insert_m14CA7010AB4805E17943E848372682D21F07064A (void);
// 0x000002F9 System.Void Sirenix.Utilities.ImmutableList::System.Collections.Generic.ICollection<System.Object>.Add(System.Object)
extern void ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Add_mEFF85DCC759305309E4FB98CD10C48D22EB677E5 (void);
// 0x000002FA System.Void Sirenix.Utilities.ImmutableList::System.Collections.Generic.ICollection<System.Object>.Clear()
extern void ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Clear_mBF95AD7AF28E208E0C1B1EF96B7223EA163E644D (void);
// 0x000002FB System.Boolean Sirenix.Utilities.ImmutableList::System.Collections.Generic.ICollection<System.Object>.Remove(System.Object)
extern void ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Remove_m81CD560F60BE64EC7A64CFF80EE98BA8B16559CB (void);
// 0x000002FC System.Void Sirenix.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::.ctor(System.Int32)
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_m4BB1374E87B307242E06C3503D7A46BFA383852A (void);
// 0x000002FD System.Void Sirenix.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.IDisposable.Dispose()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m2947B87D0F37D72931BB8C4C51F0408295B1B24C (void);
// 0x000002FE System.Boolean Sirenix.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::MoveNext()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_MoveNext_mBDC67E39FD4E5F170B201FE45711A78DC6CF2ABE (void);
// 0x000002FF System.Void Sirenix.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>m__Finally1()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_mC4BEE0754AB86918AE3C1FAF99195A83D081CDBB (void);
// 0x00000300 System.Object Sirenix.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21E45AD31E3B8130252713DEB5382BBD58A4017C (void);
// 0x00000301 System.Void Sirenix.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.Collections.IEnumerator.Reset()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m7A40897EDC5F324764DE3C1F18356A11189E2EE8 (void);
// 0x00000302 System.Object Sirenix.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m27BE8890BAAA820C03048FFCD6B7FE30B6444920 (void);
// 0x00000303 System.Void Sirenix.Utilities.ImmutableList`1::.ctor(System.Collections.Generic.IList`1<T>)
// 0x00000304 System.Int32 Sirenix.Utilities.ImmutableList`1::get_Count()
// 0x00000305 System.Boolean Sirenix.Utilities.ImmutableList`1::System.Collections.ICollection.get_IsSynchronized()
// 0x00000306 System.Object Sirenix.Utilities.ImmutableList`1::System.Collections.ICollection.get_SyncRoot()
// 0x00000307 System.Boolean Sirenix.Utilities.ImmutableList`1::System.Collections.IList.get_IsFixedSize()
// 0x00000308 System.Boolean Sirenix.Utilities.ImmutableList`1::System.Collections.IList.get_IsReadOnly()
// 0x00000309 System.Boolean Sirenix.Utilities.ImmutableList`1::get_IsReadOnly()
// 0x0000030A System.Object Sirenix.Utilities.ImmutableList`1::System.Collections.IList.get_Item(System.Int32)
// 0x0000030B System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.IList.set_Item(System.Int32,System.Object)
// 0x0000030C T Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.get_Item(System.Int32)
// 0x0000030D System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
// 0x0000030E T Sirenix.Utilities.ImmutableList`1::get_Item(System.Int32)
// 0x0000030F System.Boolean Sirenix.Utilities.ImmutableList`1::Contains(T)
// 0x00000310 System.Void Sirenix.Utilities.ImmutableList`1::CopyTo(T[],System.Int32)
// 0x00000311 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.ImmutableList`1::GetEnumerator()
// 0x00000312 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000313 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000314 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.ICollection<T>.Clear()
// 0x00000315 System.Boolean Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.ICollection<T>.Remove(T)
// 0x00000316 System.Collections.IEnumerator Sirenix.Utilities.ImmutableList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000317 System.Int32 Sirenix.Utilities.ImmutableList`1::System.Collections.IList.Add(System.Object)
// 0x00000318 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.IList.Clear()
// 0x00000319 System.Boolean Sirenix.Utilities.ImmutableList`1::System.Collections.IList.Contains(System.Object)
// 0x0000031A System.Int32 Sirenix.Utilities.ImmutableList`1::System.Collections.IList.IndexOf(System.Object)
// 0x0000031B System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.IList.Insert(System.Int32,System.Object)
// 0x0000031C System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.IList.Remove(System.Object)
// 0x0000031D System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
// 0x0000031E System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.IList.RemoveAt(System.Int32)
// 0x0000031F System.Int32 Sirenix.Utilities.ImmutableList`1::IndexOf(T)
// 0x00000320 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
// 0x00000321 System.Void Sirenix.Utilities.ImmutableList`2::.ctor(TList)
// 0x00000322 System.Int32 Sirenix.Utilities.ImmutableList`2::get_Count()
// 0x00000323 System.Boolean Sirenix.Utilities.ImmutableList`2::System.Collections.ICollection.get_IsSynchronized()
// 0x00000324 System.Object Sirenix.Utilities.ImmutableList`2::System.Collections.ICollection.get_SyncRoot()
// 0x00000325 System.Boolean Sirenix.Utilities.ImmutableList`2::System.Collections.IList.get_IsFixedSize()
// 0x00000326 System.Boolean Sirenix.Utilities.ImmutableList`2::System.Collections.IList.get_IsReadOnly()
// 0x00000327 System.Boolean Sirenix.Utilities.ImmutableList`2::get_IsReadOnly()
// 0x00000328 System.Object Sirenix.Utilities.ImmutableList`2::System.Collections.IList.get_Item(System.Int32)
// 0x00000329 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.IList.set_Item(System.Int32,System.Object)
// 0x0000032A TElement Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x0000032B System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x0000032C TElement Sirenix.Utilities.ImmutableList`2::get_Item(System.Int32)
// 0x0000032D System.Boolean Sirenix.Utilities.ImmutableList`2::Contains(TElement)
// 0x0000032E System.Void Sirenix.Utilities.ImmutableList`2::CopyTo(TElement[],System.Int32)
// 0x0000032F System.Collections.Generic.IEnumerator`1<TElement> Sirenix.Utilities.ImmutableList`2::GetEnumerator()
// 0x00000330 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000331 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x00000332 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x00000333 System.Boolean Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x00000334 System.Collections.IEnumerator Sirenix.Utilities.ImmutableList`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000335 System.Int32 Sirenix.Utilities.ImmutableList`2::System.Collections.IList.Add(System.Object)
// 0x00000336 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.IList.Clear()
// 0x00000337 System.Boolean Sirenix.Utilities.ImmutableList`2::System.Collections.IList.Contains(System.Object)
// 0x00000338 System.Int32 Sirenix.Utilities.ImmutableList`2::System.Collections.IList.IndexOf(System.Object)
// 0x00000339 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.IList.Insert(System.Int32,System.Object)
// 0x0000033A System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.IList.Remove(System.Object)
// 0x0000033B System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x0000033C System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.IList.RemoveAt(System.Int32)
// 0x0000033D System.Int32 Sirenix.Utilities.ImmutableList`2::IndexOf(TElement)
// 0x0000033E System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x0000033F System.Void Sirenix.Utilities.ListExtensions::SetLength(System.Collections.Generic.IList`1<T>&,System.Int32)
// 0x00000340 System.Void Sirenix.Utilities.ListExtensions::SetLength(System.Collections.Generic.IList`1<T>&,System.Int32,System.Func`1<T>)
// 0x00000341 System.Void Sirenix.Utilities.ListExtensions::SetLength(System.Collections.Generic.IList`1<T>,System.Int32)
// 0x00000342 System.Void Sirenix.Utilities.ListExtensions::SetLength(System.Collections.Generic.IList`1<T>,System.Int32,System.Func`1<T>)
// 0x00000343 System.Single Sirenix.Utilities.MathUtilities::PointDistanceToLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MathUtilities_PointDistanceToLine_mECCAE353427826C4E3AF68C42889ABFBD61CF055 (void);
// 0x00000344 System.Single Sirenix.Utilities.MathUtilities::Hermite(System.Single,System.Single,System.Single)
extern void MathUtilities_Hermite_mAEC14C01C7B5223178183D0873C2A3A927F28EE0 (void);
// 0x00000345 System.Single Sirenix.Utilities.MathUtilities::StackHermite(System.Single,System.Single,System.Single,System.Int32)
extern void MathUtilities_StackHermite_mDF7A4BF2A2F3B2FD38FE58A398A17C0FCD229111 (void);
// 0x00000346 System.Single Sirenix.Utilities.MathUtilities::Fract(System.Single)
extern void MathUtilities_Fract_mE218E70D376BDE6CB7E32E4A1B128937CADB9024 (void);
// 0x00000347 UnityEngine.Vector2 Sirenix.Utilities.MathUtilities::Fract(UnityEngine.Vector2)
extern void MathUtilities_Fract_m75D765D629E3448718AA9AF2173C889C7E46A378 (void);
// 0x00000348 UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::Fract(UnityEngine.Vector3)
extern void MathUtilities_Fract_m5ECE85AA5D40BA2CE404E0C2E6C80A1AC8F021B3 (void);
// 0x00000349 System.Single Sirenix.Utilities.MathUtilities::BounceEaseInFastOut(System.Single)
extern void MathUtilities_BounceEaseInFastOut_m4DBD5FA130041834EAB86E1959A379A2B43E4EE7 (void);
// 0x0000034A System.Single Sirenix.Utilities.MathUtilities::Hermite01(System.Single)
extern void MathUtilities_Hermite01_m658A8E5577356F0F930026D4AD2CB2CD44308330 (void);
// 0x0000034B System.Single Sirenix.Utilities.MathUtilities::StackHermite01(System.Single,System.Int32)
extern void MathUtilities_StackHermite01_m06A36D8A6355EB1C219CEB6F70FC75EE07C6C8B5 (void);
// 0x0000034C UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::LerpUnclamped(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void MathUtilities_LerpUnclamped_mEBC6CF6B0497E63887B036312BC58719A947D0B3 (void);
// 0x0000034D UnityEngine.Vector2 Sirenix.Utilities.MathUtilities::LerpUnclamped(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void MathUtilities_LerpUnclamped_mCB26B95223EF8A3938039AC8B85A1590C8069656 (void);
// 0x0000034E System.Single Sirenix.Utilities.MathUtilities::Bounce(System.Single)
extern void MathUtilities_Bounce_m387F269021F722588B3C00589D3E391755FEC87B (void);
// 0x0000034F System.Single Sirenix.Utilities.MathUtilities::EaseInElastic(System.Single,System.Single,System.Single)
extern void MathUtilities_EaseInElastic_mCAC804C66FC6D20E51D24825EFEDF9508918BA9C (void);
// 0x00000350 UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::Pow(UnityEngine.Vector3,System.Single)
extern void MathUtilities_Pow_mC24BF2057699AAECCC14A3E925C909BB65420FD1 (void);
// 0x00000351 UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::Abs(UnityEngine.Vector3)
extern void MathUtilities_Abs_mD9793B239B31D3152B688A12332B068194971356 (void);
// 0x00000352 UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::Sign(UnityEngine.Vector3)
extern void MathUtilities_Sign_mE828BF27DCE0AA4875CF9C1F786D1368C0987C09 (void);
// 0x00000353 System.Single Sirenix.Utilities.MathUtilities::EaseOutElastic(System.Single,System.Single,System.Single)
extern void MathUtilities_EaseOutElastic_m7100232D382537F53782AC80BF99AD2F48B7510B (void);
// 0x00000354 System.Single Sirenix.Utilities.MathUtilities::EaseInOut(System.Single)
extern void MathUtilities_EaseInOut_m47261C60E7A9B14F9BF2AEC9FBA89C8C5B27CF7A (void);
// 0x00000355 UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::Clamp(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MathUtilities_Clamp_mD209E6E22DE6B012FBF6FDB520DD6CB838938135 (void);
// 0x00000356 UnityEngine.Vector2 Sirenix.Utilities.MathUtilities::Clamp(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void MathUtilities_Clamp_m2F5273420C5743FC4298F515F58441A3A45B9397 (void);
// 0x00000357 System.Int32 Sirenix.Utilities.MathUtilities::ComputeByteArrayHash(System.Byte[])
extern void MathUtilities_ComputeByteArrayHash_m770C66275712427E1DA4C0A55B7D4D26857E67C6 (void);
// 0x00000358 UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::InterpolatePoints(UnityEngine.Vector3[],System.Single)
extern void MathUtilities_InterpolatePoints_mA27020C45BABAFA61F8986688010519E2093820D (void);
// 0x00000359 System.Boolean Sirenix.Utilities.MathUtilities::LineIntersectsLine(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2&)
extern void MathUtilities_LineIntersectsLine_m7A9438FE9AC96F398D94DA612980E203046E5FA4 (void);
// 0x0000035A UnityEngine.Vector2 Sirenix.Utilities.MathUtilities::InfiniteLineIntersect(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void MathUtilities_InfiniteLineIntersect_m91342D3D2A7830BE98ED029AED275F02A633CCCB (void);
// 0x0000035B System.Single Sirenix.Utilities.MathUtilities::LineDistToPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MathUtilities_LineDistToPlane_m6463EEBC27DECC1F966E036459375A38705CCEA8 (void);
// 0x0000035C System.Single Sirenix.Utilities.MathUtilities::RayDistToPlane(UnityEngine.Ray,UnityEngine.Plane)
extern void MathUtilities_RayDistToPlane_mC3BC1544A22F0DFA732D55401E52D1C218AEDFC9 (void);
// 0x0000035D UnityEngine.Vector2 Sirenix.Utilities.MathUtilities::RotatePoint(UnityEngine.Vector2,System.Single)
extern void MathUtilities_RotatePoint_mB5F66BA092BAA3F9523A10559E152B26C66BB33D (void);
// 0x0000035E UnityEngine.Vector2 Sirenix.Utilities.MathUtilities::RotatePoint(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void MathUtilities_RotatePoint_m7AB1A7EB89A309E638918B7787A70FDA305892DD (void);
// 0x0000035F System.Single Sirenix.Utilities.MathUtilities::SmoothStep(System.Single,System.Single,System.Single)
extern void MathUtilities_SmoothStep_m3A61D9D5F657B43E8D78B1B6E4B74D6220E6C9C9 (void);
// 0x00000360 System.Single Sirenix.Utilities.MathUtilities::LinearStep(System.Single,System.Single,System.Single)
extern void MathUtilities_LinearStep_m01975B4D09CE83EA54430520E7F7D74837730E9D (void);
// 0x00000361 System.Double Sirenix.Utilities.MathUtilities::Wrap(System.Double,System.Double,System.Double)
extern void MathUtilities_Wrap_mF4407A877B05B12D64A88719C5B7EC4C69868CD3 (void);
// 0x00000362 System.Single Sirenix.Utilities.MathUtilities::Wrap(System.Single,System.Single,System.Single)
extern void MathUtilities_Wrap_m4C935F50AF413153E373296D9FEC5C85BD239161 (void);
// 0x00000363 System.Int32 Sirenix.Utilities.MathUtilities::Wrap(System.Int32,System.Int32,System.Int32)
extern void MathUtilities_Wrap_mA3D53BC08DD3350AADA524C17552B9069C826BF7 (void);
// 0x00000364 System.Double Sirenix.Utilities.MathUtilities::RoundBasedOnMinimumDifference(System.Double,System.Double)
extern void MathUtilities_RoundBasedOnMinimumDifference_mEB43E6F62C4E4CCE7469805E920ED4D61F7A9BC8 (void);
// 0x00000365 System.Double Sirenix.Utilities.MathUtilities::DiscardLeastSignificantDecimal(System.Double)
extern void MathUtilities_DiscardLeastSignificantDecimal_mC4332BA05E24F088FAFBB5F0BC0A4F0D541D508E (void);
// 0x00000366 System.Single Sirenix.Utilities.MathUtilities::ClampWrapAngle(System.Single,System.Single,System.Single)
extern void MathUtilities_ClampWrapAngle_m3B3ABE897B244B506EC97B3BD35C7DE9AA5D4B9F (void);
// 0x00000367 System.Int32 Sirenix.Utilities.MathUtilities::GetNumberOfDecimalsForMinimumDifference(System.Double)
extern void MathUtilities_GetNumberOfDecimalsForMinimumDifference_m47FEE6F9B093A1E29FA8B7A13DCE6B55884077BD (void);
// 0x00000368 TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::InsertOneColumnLeft(TElement[0...,0...],System.Int32)
// 0x00000369 TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::InsertOneColumnRight(TElement[0...,0...],System.Int32)
// 0x0000036A TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::InsertOneRowAbove(TElement[0...,0...],System.Int32)
// 0x0000036B TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::InsertOneRowBelow(TElement[0...,0...],System.Int32)
// 0x0000036C TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::DuplicateColumn(TElement[0...,0...],System.Int32)
// 0x0000036D TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::DuplicateRow(TElement[0...,0...],System.Int32)
// 0x0000036E TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::MoveColumn(TElement[0...,0...],System.Int32,System.Int32)
// 0x0000036F TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::MoveRow(TElement[0...,0...],System.Int32,System.Int32)
// 0x00000370 TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::DeleteColumn(TElement[0...,0...],System.Int32)
// 0x00000371 TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::DeleteRow(TElement[0...,0...],System.Int32)
// 0x00000372 System.Void Sirenix.Utilities.PersistentAssemblyAttribute::.ctor()
extern void PersistentAssemblyAttribute__ctor_m3FAE02AF2158F07B3139934DE577F690BC616837 (void);
// 0x00000373 System.Void Sirenix.Utilities.MemberAliasFieldInfo::.ctor(System.Reflection.FieldInfo,System.String)
extern void MemberAliasFieldInfo__ctor_m7F1F8CCC9218DC6DD601F4DECE32E7A4E23B1124 (void);
// 0x00000374 System.Void Sirenix.Utilities.MemberAliasFieldInfo::.ctor(System.Reflection.FieldInfo,System.String,System.String)
extern void MemberAliasFieldInfo__ctor_m689CBC3B1F160155C22D22705776B2F509904E46 (void);
// 0x00000375 System.Reflection.FieldInfo Sirenix.Utilities.MemberAliasFieldInfo::get_AliasedField()
extern void MemberAliasFieldInfo_get_AliasedField_mCF6792CB78A4E12A31A47B7692E208F42A25E4BC (void);
// 0x00000376 System.Reflection.Module Sirenix.Utilities.MemberAliasFieldInfo::get_Module()
extern void MemberAliasFieldInfo_get_Module_m967F20933DE077A43137562B3E8E90002C4A7507 (void);
// 0x00000377 System.Int32 Sirenix.Utilities.MemberAliasFieldInfo::get_MetadataToken()
extern void MemberAliasFieldInfo_get_MetadataToken_mFD230E83F9CB7EBEFA53EC3BD4C5D1508D83F343 (void);
// 0x00000378 System.String Sirenix.Utilities.MemberAliasFieldInfo::get_Name()
extern void MemberAliasFieldInfo_get_Name_m7958C05205747FF45F5FF2043C29B4E82C95068E (void);
// 0x00000379 System.Type Sirenix.Utilities.MemberAliasFieldInfo::get_DeclaringType()
extern void MemberAliasFieldInfo_get_DeclaringType_m0CA1A39EFB2994459257AE8365B39F2DF484BADC (void);
// 0x0000037A System.Type Sirenix.Utilities.MemberAliasFieldInfo::get_ReflectedType()
extern void MemberAliasFieldInfo_get_ReflectedType_mF7EF922323C545A9F0E056DC9F8C763BCC651B4C (void);
// 0x0000037B System.Type Sirenix.Utilities.MemberAliasFieldInfo::get_FieldType()
extern void MemberAliasFieldInfo_get_FieldType_mB5BB2FF53B46742BB1010CA24DDA44EAC57EB41D (void);
// 0x0000037C System.RuntimeFieldHandle Sirenix.Utilities.MemberAliasFieldInfo::get_FieldHandle()
extern void MemberAliasFieldInfo_get_FieldHandle_m78D86DDCCE4E73ED9C4228B243815DC20BA33D51 (void);
// 0x0000037D System.Reflection.FieldAttributes Sirenix.Utilities.MemberAliasFieldInfo::get_Attributes()
extern void MemberAliasFieldInfo_get_Attributes_mC639BC47EF1DCB681CFFDE6BE90F15C8BC23BF5E (void);
// 0x0000037E System.Object[] Sirenix.Utilities.MemberAliasFieldInfo::GetCustomAttributes(System.Boolean)
extern void MemberAliasFieldInfo_GetCustomAttributes_mBFD30F074E7C3C71CB49A9675D5C9498E0137658 (void);
// 0x0000037F System.Object[] Sirenix.Utilities.MemberAliasFieldInfo::GetCustomAttributes(System.Type,System.Boolean)
extern void MemberAliasFieldInfo_GetCustomAttributes_mC988A1D459D5C7C8B533F64EDC0A1E80C0F4BC55 (void);
// 0x00000380 System.Boolean Sirenix.Utilities.MemberAliasFieldInfo::IsDefined(System.Type,System.Boolean)
extern void MemberAliasFieldInfo_IsDefined_mC4AB0AF29B26D6B03DD7508658A022D75BECB72A (void);
// 0x00000381 System.Object Sirenix.Utilities.MemberAliasFieldInfo::GetValue(System.Object)
extern void MemberAliasFieldInfo_GetValue_m076F01BF95D1B1A81F2C9776F74A8A6C98ADA6B7 (void);
// 0x00000382 System.Void Sirenix.Utilities.MemberAliasFieldInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo)
extern void MemberAliasFieldInfo_SetValue_m65FEC1521B2D8DD764B53AA62DE879EC4A73E894 (void);
// 0x00000383 System.Void Sirenix.Utilities.MemberAliasMethodInfo::.ctor(System.Reflection.MethodInfo,System.String)
extern void MemberAliasMethodInfo__ctor_mDFE6D288860CD2AC26A0F9D20C09B083C3582923 (void);
// 0x00000384 System.Void Sirenix.Utilities.MemberAliasMethodInfo::.ctor(System.Reflection.MethodInfo,System.String,System.String)
extern void MemberAliasMethodInfo__ctor_m920A2C5254778046D8B91966DE747731199F095F (void);
// 0x00000385 System.Reflection.MethodInfo Sirenix.Utilities.MemberAliasMethodInfo::get_AliasedMethod()
extern void MemberAliasMethodInfo_get_AliasedMethod_mC3C268B262DF1E0B55593AFCF73F78D65ECDCF7C (void);
// 0x00000386 System.Reflection.ICustomAttributeProvider Sirenix.Utilities.MemberAliasMethodInfo::get_ReturnTypeCustomAttributes()
extern void MemberAliasMethodInfo_get_ReturnTypeCustomAttributes_m53F9625127315D522FF9128F6974101236F0948F (void);
// 0x00000387 System.RuntimeMethodHandle Sirenix.Utilities.MemberAliasMethodInfo::get_MethodHandle()
extern void MemberAliasMethodInfo_get_MethodHandle_m8ADFCA7E06CA6EA34EFA6EAFCD63E85A59EE0578 (void);
// 0x00000388 System.Reflection.MethodAttributes Sirenix.Utilities.MemberAliasMethodInfo::get_Attributes()
extern void MemberAliasMethodInfo_get_Attributes_m296C0BF440F414B6CD25BFE02B0C2BF58E945642 (void);
// 0x00000389 System.Type Sirenix.Utilities.MemberAliasMethodInfo::get_ReturnType()
extern void MemberAliasMethodInfo_get_ReturnType_mB591F11265E82258246BD5B1131D95537467A4BE (void);
// 0x0000038A System.Type Sirenix.Utilities.MemberAliasMethodInfo::get_DeclaringType()
extern void MemberAliasMethodInfo_get_DeclaringType_mF96C84D3F6F2446C5C0C04DC404E5A95C86D7C41 (void);
// 0x0000038B System.String Sirenix.Utilities.MemberAliasMethodInfo::get_Name()
extern void MemberAliasMethodInfo_get_Name_m8AAAA2740BC4BAAE349720F2FDBA26C9A274AE05 (void);
// 0x0000038C System.Type Sirenix.Utilities.MemberAliasMethodInfo::get_ReflectedType()
extern void MemberAliasMethodInfo_get_ReflectedType_m2B5FD961697975A7A39835A81AC27EF4F8E59BF2 (void);
// 0x0000038D System.Reflection.MethodInfo Sirenix.Utilities.MemberAliasMethodInfo::GetBaseDefinition()
extern void MemberAliasMethodInfo_GetBaseDefinition_m367A6EC75FADFAB7F8ADA34E0152758D218445D7 (void);
// 0x0000038E System.Object[] Sirenix.Utilities.MemberAliasMethodInfo::GetCustomAttributes(System.Boolean)
extern void MemberAliasMethodInfo_GetCustomAttributes_m011FAF5CE60A6362123B748EE5B673DB63EECBBD (void);
// 0x0000038F System.Object[] Sirenix.Utilities.MemberAliasMethodInfo::GetCustomAttributes(System.Type,System.Boolean)
extern void MemberAliasMethodInfo_GetCustomAttributes_m2CCA5174E49B52D3BDEB7214FDF289F15571904C (void);
// 0x00000390 System.Reflection.MethodImplAttributes Sirenix.Utilities.MemberAliasMethodInfo::GetMethodImplementationFlags()
extern void MemberAliasMethodInfo_GetMethodImplementationFlags_mE33BB4660418351ADCF127699ACA0EA318D411B7 (void);
// 0x00000391 System.Reflection.ParameterInfo[] Sirenix.Utilities.MemberAliasMethodInfo::GetParameters()
extern void MemberAliasMethodInfo_GetParameters_mA341EECB4DDF72E049F7322C7836CBC85AEA6A55 (void);
// 0x00000392 System.Object Sirenix.Utilities.MemberAliasMethodInfo::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern void MemberAliasMethodInfo_Invoke_m9FC3272E9ADAC29549A8FBB88E5C1EBC6A332DAA (void);
// 0x00000393 System.Boolean Sirenix.Utilities.MemberAliasMethodInfo::IsDefined(System.Type,System.Boolean)
extern void MemberAliasMethodInfo_IsDefined_mC87F4450B971BC3C67B81CCF0A069A453E4FA1E6 (void);
// 0x00000394 System.Void Sirenix.Utilities.MemberAliasPropertyInfo::.ctor(System.Reflection.PropertyInfo,System.String)
extern void MemberAliasPropertyInfo__ctor_mA2D89283CA83A1A4E9006381C85DCABE332F11EC (void);
// 0x00000395 System.Void Sirenix.Utilities.MemberAliasPropertyInfo::.ctor(System.Reflection.PropertyInfo,System.String,System.String)
extern void MemberAliasPropertyInfo__ctor_m3415416072338EB557C0382A4102C627C925F05D (void);
// 0x00000396 System.Reflection.PropertyInfo Sirenix.Utilities.MemberAliasPropertyInfo::get_AliasedProperty()
extern void MemberAliasPropertyInfo_get_AliasedProperty_m43939436AF91312C7CB265EF9124B5FEAECF5AD4 (void);
// 0x00000397 System.Reflection.Module Sirenix.Utilities.MemberAliasPropertyInfo::get_Module()
extern void MemberAliasPropertyInfo_get_Module_m2BB0AB57E7C613AEF68011E4D1C218F32E6EF5EF (void);
// 0x00000398 System.Int32 Sirenix.Utilities.MemberAliasPropertyInfo::get_MetadataToken()
extern void MemberAliasPropertyInfo_get_MetadataToken_m60502075878AD3E4D121104FB4357980C97E6E7C (void);
// 0x00000399 System.String Sirenix.Utilities.MemberAliasPropertyInfo::get_Name()
extern void MemberAliasPropertyInfo_get_Name_mEC97801686137FD552D7DA5654FAF4A308B47F4A (void);
// 0x0000039A System.Type Sirenix.Utilities.MemberAliasPropertyInfo::get_DeclaringType()
extern void MemberAliasPropertyInfo_get_DeclaringType_mD4D26247B273389E689BA98902A383CD1285E130 (void);
// 0x0000039B System.Type Sirenix.Utilities.MemberAliasPropertyInfo::get_ReflectedType()
extern void MemberAliasPropertyInfo_get_ReflectedType_m2AC06C96ECE0CD5D0967BCB9CC5B56820464D996 (void);
// 0x0000039C System.Type Sirenix.Utilities.MemberAliasPropertyInfo::get_PropertyType()
extern void MemberAliasPropertyInfo_get_PropertyType_m1FA174F70B0140611092D33DBDC26ACBBD70BCDB (void);
// 0x0000039D System.Reflection.PropertyAttributes Sirenix.Utilities.MemberAliasPropertyInfo::get_Attributes()
extern void MemberAliasPropertyInfo_get_Attributes_m95EE091CAABE46A27CD158050F3850538E4A00B1 (void);
// 0x0000039E System.Boolean Sirenix.Utilities.MemberAliasPropertyInfo::get_CanRead()
extern void MemberAliasPropertyInfo_get_CanRead_m56C4B6D408F4EB2501B3C04EF6EBC7404A753AC9 (void);
// 0x0000039F System.Boolean Sirenix.Utilities.MemberAliasPropertyInfo::get_CanWrite()
extern void MemberAliasPropertyInfo_get_CanWrite_m840343CA9A7766CA30528B6A38469462F1E0B6FD (void);
// 0x000003A0 System.Object[] Sirenix.Utilities.MemberAliasPropertyInfo::GetCustomAttributes(System.Boolean)
extern void MemberAliasPropertyInfo_GetCustomAttributes_mBD1D704D2F33E35D5F156D74A27877ACA53D9571 (void);
// 0x000003A1 System.Object[] Sirenix.Utilities.MemberAliasPropertyInfo::GetCustomAttributes(System.Type,System.Boolean)
extern void MemberAliasPropertyInfo_GetCustomAttributes_mB0AEF877D21DB7C5C79847DD10C87B301E364DD2 (void);
// 0x000003A2 System.Boolean Sirenix.Utilities.MemberAliasPropertyInfo::IsDefined(System.Type,System.Boolean)
extern void MemberAliasPropertyInfo_IsDefined_mDFA358D34DCDEEDA6B8ACCDAA418BF29FF83F155 (void);
// 0x000003A3 System.Reflection.MethodInfo[] Sirenix.Utilities.MemberAliasPropertyInfo::GetAccessors(System.Boolean)
extern void MemberAliasPropertyInfo_GetAccessors_mAD1E7E77F4C85F7EED905B0CC7A74B7551CDFE7F (void);
// 0x000003A4 System.Reflection.MethodInfo Sirenix.Utilities.MemberAliasPropertyInfo::GetGetMethod(System.Boolean)
extern void MemberAliasPropertyInfo_GetGetMethod_mF23C8DA518A9A0658C3E9C98616526D5C4B0B255 (void);
// 0x000003A5 System.Reflection.ParameterInfo[] Sirenix.Utilities.MemberAliasPropertyInfo::GetIndexParameters()
extern void MemberAliasPropertyInfo_GetIndexParameters_mDAB2CE638402687701B3F18E0E83849617AB4F7F (void);
// 0x000003A6 System.Reflection.MethodInfo Sirenix.Utilities.MemberAliasPropertyInfo::GetSetMethod(System.Boolean)
extern void MemberAliasPropertyInfo_GetSetMethod_mF3AABEE78A859BF71F29BFA4F11F874E805CD71D (void);
// 0x000003A7 System.Object Sirenix.Utilities.MemberAliasPropertyInfo::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern void MemberAliasPropertyInfo_GetValue_m94AF671CA2ACECF9350C7F7E04F210BB5787DF4F (void);
// 0x000003A8 System.Void Sirenix.Utilities.MemberAliasPropertyInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern void MemberAliasPropertyInfo_SetValue_mB288F5C43A859E61AAC5E17AA11FD6D780BCE497 (void);
// 0x000003A9 System.Void Sirenix.Utilities.ProjectPathFinder::.ctor()
extern void ProjectPathFinder__ctor_m21A935F34675629AB691349121C503DE2D4B6A9E (void);
// 0x000003AA System.Void Sirenix.Utilities.SirenixAssetPaths::.cctor()
extern void SirenixAssetPaths__cctor_m0C805983566629D240D6E4DE9E46B5CEA97D7717 (void);
// 0x000003AB System.String Sirenix.Utilities.SirenixAssetPaths::ToPathSafeString(System.String,System.Char)
extern void SirenixAssetPaths_ToPathSafeString_mFC4DB86F6F5A3B298B37C026752DAC63F59BEDCD (void);
// 0x000003AC System.Void Sirenix.Utilities.SirenixAssetPaths/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mE3735CED949904218E19A344742A70919D9A052E (void);
// 0x000003AD System.Char Sirenix.Utilities.SirenixAssetPaths/<>c__DisplayClass12_0::<ToPathSafeString>b__0(System.Char)
extern void U3CU3Ec__DisplayClass12_0_U3CToPathSafeStringU3Eb__0_mBC49F4EB4A50B6ACBF4B08AAEF340BA6BA5DB274 (void);
// 0x000003AE System.String Sirenix.Utilities.SirenixBuildNameAttribute::get_BuildName()
extern void SirenixBuildNameAttribute_get_BuildName_m6A6471920F0D0BE07D648C7630F7D92F672DA826 (void);
// 0x000003AF System.Void Sirenix.Utilities.SirenixBuildNameAttribute::set_BuildName(System.String)
extern void SirenixBuildNameAttribute_set_BuildName_m7E1559901D13E2E9B18227B4959E8FB9EA090E0B (void);
// 0x000003B0 System.Void Sirenix.Utilities.SirenixBuildNameAttribute::.ctor(System.String)
extern void SirenixBuildNameAttribute__ctor_mAD7C4444D93F954F2F0534EC907F1B3C6DDF9B91 (void);
// 0x000003B1 System.String Sirenix.Utilities.SirenixBuildVersionAttribute::get_Version()
extern void SirenixBuildVersionAttribute_get_Version_m5B4095D2A8633E2D7A09FAE2DA3728DC52E51671 (void);
// 0x000003B2 System.Void Sirenix.Utilities.SirenixBuildVersionAttribute::set_Version(System.String)
extern void SirenixBuildVersionAttribute_set_Version_mE037801F39B45C45076F7D0F32E98905F430C129 (void);
// 0x000003B3 System.Void Sirenix.Utilities.SirenixBuildVersionAttribute::.ctor(System.String)
extern void SirenixBuildVersionAttribute__ctor_mB01FF1D3895F96A87DA85B3B9060A6DC02AB574E (void);
// 0x000003B4 System.Void Sirenix.Utilities.SirenixEditorConfigAttribute::.ctor()
extern void SirenixEditorConfigAttribute__ctor_m7F535FC2498C442C1417BDE90F9B7AED6D2827E8 (void);
// 0x000003B5 System.Void Sirenix.Utilities.SirenixGlobalConfigAttribute::.ctor()
extern void SirenixGlobalConfigAttribute__ctor_m41A63774EA77E1DFCF1DD463FE4C550C82AFBA70 (void);
// 0x000003B6 System.String Sirenix.Utilities.StringUtilities::NicifyByteSize(System.Int32,System.Int32)
extern void StringUtilities_NicifyByteSize_m11CB909F38D2115EBE6D2C613964E61ACF2ACBAF (void);
// 0x000003B7 System.Boolean Sirenix.Utilities.StringUtilities::FastEndsWith(System.String,System.String)
extern void StringUtilities_FastEndsWith_mF7B8C034F595DBC9D34AF61B566E9535C7EC0CB3 (void);
// 0x000003B8 System.Void Sirenix.Utilities.UnityVersion::.cctor()
extern void UnityVersion__cctor_m1D69DE00E7984BE313CCAAF5E7C78D1DD9383576 (void);
// 0x000003B9 System.Void Sirenix.Utilities.UnityVersion::EnsureLoaded()
extern void UnityVersion_EnsureLoaded_m0A48330D08515576EDFDA5CDF60CB041BFBB0C72 (void);
// 0x000003BA System.Boolean Sirenix.Utilities.UnityVersion::IsVersionOrGreater(System.Int32,System.Int32)
extern void UnityVersion_IsVersionOrGreater_m8650838D70B6B827F91D540410F5E1964D40F19C (void);
// 0x000003BB System.Boolean Sirenix.Utilities.ReferenceEqualityComparer`1::Equals(T,T)
// 0x000003BC System.Int32 Sirenix.Utilities.ReferenceEqualityComparer`1::GetHashCode(T)
// 0x000003BD System.Void Sirenix.Utilities.ReferenceEqualityComparer`1::.ctor()
// 0x000003BE System.Void Sirenix.Utilities.ReferenceEqualityComparer`1::.cctor()
// 0x000003BF T[] Sirenix.Utilities.Unsafe.UnsafeUtilities::StructArrayFromBytes(System.Byte[],System.Int32)
// 0x000003C0 T[] Sirenix.Utilities.Unsafe.UnsafeUtilities::StructArrayFromBytes(System.Byte[],System.Int32,System.Int32)
// 0x000003C1 System.Byte[] Sirenix.Utilities.Unsafe.UnsafeUtilities::StructArrayToBytes(T[])
// 0x000003C2 System.Byte[] Sirenix.Utilities.Unsafe.UnsafeUtilities::StructArrayToBytes(T[],System.Byte[]&,System.Int32)
// 0x000003C3 System.String Sirenix.Utilities.Unsafe.UnsafeUtilities::StringFromBytes(System.Byte[],System.Int32,System.Boolean)
extern void UnsafeUtilities_StringFromBytes_mBAD899386A78E9887F8F18E15761881F04CEED11 (void);
// 0x000003C4 System.Int32 Sirenix.Utilities.Unsafe.UnsafeUtilities::StringToBytes(System.Byte[],System.String,System.Boolean)
extern void UnsafeUtilities_StringToBytes_m7F228372A7683612AC69C54232CBD792488E673B (void);
// 0x000003C5 System.Void Sirenix.Utilities.Unsafe.UnsafeUtilities::MemoryCopy(System.Object,System.Object,System.Int32,System.Int32,System.Int32)
extern void UnsafeUtilities_MemoryCopy_m6AE1EE2677ADAD57C174AC51B0DE22F192DE71A8 (void);
static Il2CppMethodPointer s_methodPointers[965] = 
{
	ColorExtensions_Lerp_m8720CBDC453206021255DC41EDCAD80778A120A4,
	ColorExtensions_MoveTowards_m73DA04308576A388045C312344E9BA58748A1D42,
	ColorExtensions_TryParseString_m9F4F647058B0FE2103A65C422C5F746EE4527425,
	ColorExtensions_ToCSharpColor_mD0578C24EAEAA0325D0FF17321A873CAC7D79990,
	ColorExtensions_Pow_m149A66B452C04A3131BF96BC13B2CFF9FB22E780,
	ColorExtensions_NormalizeRGB_mD5758657473D885D209F8D2B2FE261294691080E,
	ColorExtensions_TrimFloat_m2438FEE187E7E855CCC4F1E5F2C63304E6F1B8C8,
	ColorExtensions__cctor_m2F0F9F8259A818CD849C502A15080D3842C1B423,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FieldInfoExtensions_IsAliasField_mB4C68639DCBB7BD669EFBDECD94659CF9978584A,
	FieldInfoExtensions_DeAliasField_m7635D5FE723F2A9DF12E18D1167FB981F106650E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MemberInfoExtensions_GetAttributes_mC67AF61F5F613E8A622E70FE37DB2A7F3C871EAB,
	MemberInfoExtensions_GetAttributes_mDC5120BF9C047F5114974C5D5998D5E10722AF93,
	MemberInfoExtensions_GetNiceName_m4C4C96D2B0AB0B782B88F58DF49472538F3F4AE0,
	MemberInfoExtensions_IsStatic_m34080863A1E59E680897829BE78D658127551FBA,
	MemberInfoExtensions_IsAlias_m875106ADBA946765F38220DDE5138856B47A33CE,
	MemberInfoExtensions_DeAlias_m0EACB9804C68943D56AF92C6201AC808B9C502D5,
	MemberInfoExtensions_SignaturesAreEqual_mB0220D7BB799E753BBF4FB4CDB8DB94826886704,
	MethodInfoExtensions_GetFullName_m691FC2714107F7C00596B7A3CE926C46C757F40C,
	MethodInfoExtensions_GetParamsNames_m79240D291015C13AEBD2360B14D3A8F4BC472DBF,
	MethodInfoExtensions_GetFullName_m769F55B4D93A900A3B644D7FAB0F8F101881E8E3,
	MethodInfoExtensions_IsExtensionMethod_m65CD2EE770B00DFBDA04F2B2C42C412CCCA5AD02,
	MethodInfoExtensions_IsAliasMethod_m633A938A0FDCDD33A7E20567F5A36E46C5BD23ED,
	MethodInfoExtensions_DeAliasMethod_m46FE50F76E67FE6F955D189FDD6C03509A739BED,
	PathUtilities_GetDirectoryName_m043DE0319FD343A79437D8C3077F3C7D6BB822F7,
	PathUtilities_HasSubDirectory_mC2CFE2899F6AF9A9A29CE0F41F1C2AE1148FF173,
	PathUtilities_FindParentDirectoryWithName_m9C21BFB384B12F97FABE7F75510A789BC7EE5321,
	PathUtilities_CanMakeRelative_mBB41C2ED1246D8B4BBA26375F61FD4B1DBE902F7,
	PathUtilities_MakeRelative_m0C634F205BEEC8030703E326017839125AF300FE,
	PathUtilities_TryMakeRelative_m2CCEB4584F3119FCB776864DA25C5DEA8B9695BA,
	PathUtilities_Combine_mA326E8DD584D486B67F55DEBF436606F50510C07,
	PropertyInfoExtensions_IsAutoProperty_m78B131314E2F486AF4414E88DF745682C1846BED,
	PropertyInfoExtensions_IsAliasProperty_mDA54D8A4C1AEA6F1327D0611046185711315A478,
	PropertyInfoExtensions_DeAliasProperty_m96A39DD5E5AC93F400CDCC0E38463F743EDE2DE8,
	RectExtensions_SetWidth_m69CF523EAC6A34C33AA3FFCFC8686AB4B71AE641,
	RectExtensions_SetHeight_m506B0534A498040EDE45E6033780AEB8638BE829,
	RectExtensions_SetSize_m33D310F0B86907CB749CAB0D05FB3A32032DA3FD,
	RectExtensions_SetSize_m77332E849C6579179AAF16098541615F32FADE91,
	RectExtensions_HorizontalPadding_m40E9A2B65348E239D4F53B3F2682A4F8EFC72F38,
	RectExtensions_HorizontalPadding_mF5400E98C4BA88637DB612D7B19B43C34AF9102D,
	RectExtensions_VerticalPadding_mBB7299207CEF10A7FC9BB80F8CB9EA4B51A9C45A,
	RectExtensions_VerticalPadding_mACD68D876E752DBD423AF49A0EDF563584D1FAB9,
	RectExtensions_Padding_mBEC2014271A4CCA549DC93EFF69CAB46FAA8D2D7,
	RectExtensions_Padding_m37813FB32D26061D0E9475BCB6D9BA06EFDC1082,
	RectExtensions_Padding_mFBC76FAA5D294CB444DD359CFFD97A88B8155DD5,
	RectExtensions_AlignLeft_m0567E2C5D89D516BAFA700E513B29945A3100F0A,
	RectExtensions_AlignCenter_m870B46389403681459F6A312101A25E5E1DFCCE3,
	RectExtensions_AlignCenter_mD93A613D578F553DFFD06C6D6BB09EBFCB31AD98,
	RectExtensions_AlignRight_m99D45974D9E57C19AB1D345CBDA03577783188F3,
	RectExtensions_AlignRight_mABBF469967EF7492916B6A983ED06F3A4CC2DE28,
	RectExtensions_AlignTop_m074B39472F8270D93BEC35F2B01833D5576D9EB7,
	RectExtensions_AlignMiddle_mBAECD2906BE0488F4E6C852543E707051CC996A7,
	RectExtensions_AlignBottom_m82A57F38D61437057CE370DD2F6EFC318CFEC283,
	RectExtensions_AlignCenterX_m0AD1FF1FCFC42E3EB8D87B037C5265A26BA9EE4A,
	RectExtensions_AlignCenterY_mC6D062A1262386DC84A10D80D2D557B08D968ACE,
	RectExtensions_AlignCenterXY_m6BAE2956C5BE0350C4A4D1A54E27F31738D73923,
	RectExtensions_AlignCenterXY_m31868BDCB2066600551158562C9D16D216098744,
	RectExtensions_Expand_m7ECB0674AE6B84749594E777E1C06CAE049FF631,
	RectExtensions_Expand_m5C8CB6CE42E33DAB75F9A19ADE371398FB1DD9EE,
	RectExtensions_Expand_m868D059441EBCE784D65912EE91E8982F65A3D05,
	RectExtensions_Split_m8498B85B3042F680956CC0CAD4B9E172654BAA10,
	RectExtensions_SplitVertical_mDC348DA26D272EFEBD3A47576D991130F18BE6B8,
	RectExtensions_SplitGrid_mDD29A44B513AB37844AB20093F71BDA8B255FB5E,
	RectExtensions_SplitTableGrid_mBF5ABF975DB20FF5D990002F3FE19F955576FB73,
	RectExtensions_SetCenterX_mEB413A4A360A104F5B59D9537CEF243CC9F2C4CE,
	RectExtensions_SetCenterY_m4977DBEDECCDAB450811F1CF801341766001FEA9,
	RectExtensions_SetCenter_mB3DDA607B2F9F0D30BD50E6A22AC9D9DC8EA8F20,
	RectExtensions_SetCenter_mAC7ECCF3C94CCC3BC3D056379C0C4C2CDF3EA7E0,
	RectExtensions_SetPosition_m09C88CEE5C7DBD17925C7913F7CB0596003758B3,
	RectExtensions_ResetPosition_m1B049C9AE1D10788E3F3651ED61611C251857EE9,
	RectExtensions_AddPosition_m6B6BE590A267F522AF0C96087BFDC47C602132EA,
	RectExtensions_AddPosition_mB099E38EE661375D2C0368AE71B2E3B5B9ED9E67,
	RectExtensions_SetX_mB5C44EE347E61D0DA3E674817955121D6FB4C7D1,
	RectExtensions_AddX_mFE44A35C702AAC8131AD782E6A7A0D17AA3F5F68,
	RectExtensions_SubX_m241BD947ADCEE437FDABEC65993BDCF6AD74AE57,
	RectExtensions_SetY_m1DC8DA1C70D789128A8750E82F0BBBD68D09494B,
	RectExtensions_AddY_m34D9EED90402B2B28E3A90B6C61E6EFA385BA450,
	RectExtensions_SubY_m64C3AA4112903ECF49CF6BA666E1299495535D59,
	RectExtensions_SetMin_m4B63F926FDFE2EAE085CE45279242C8F257926DB,
	RectExtensions_AddMin_m5A48E49590A3C028D5F648DD672419EF7AF541D0,
	RectExtensions_SubMin_mDF1C320D2FD6EF27588A0A4D0DD829D8F8733D6F,
	RectExtensions_SetMax_m0A7CAED9DA27C89C29CD83A56DA73276DA89141A,
	RectExtensions_AddMax_m554B9A78C0BC8BA224FDBD88B55303D45532EA00,
	RectExtensions_SubMax_m0C47C7BBD6E44857132B680FD65791F6C1FE498C,
	RectExtensions_SetXMin_mDC3EF8BBBD19D11B6C2FC43FDDB2552D2B47D916,
	RectExtensions_AddXMin_mF7E01410A0774329AACE7355938CB69557FCE69A,
	RectExtensions_SubXMin_m03C4A5FCD2475806CCD54BEBA93436468B4EA2F0,
	RectExtensions_SetXMax_m8AF050E22802D9827228B241188BF6E746356D37,
	RectExtensions_AddXMax_mE7597128E980376700BF85196BF2716FADF4F508,
	RectExtensions_SubXMax_mF32A698159791AD4768065C43F1AF07D1B6A4BBD,
	RectExtensions_SetYMin_m47B9FD6B785F3906C973F8284383420CC68973FF,
	RectExtensions_AddYMin_m9ADC635148B7C93295279F5B54337BC833336F97,
	RectExtensions_SubYMin_m7EB4B2B44C19183982942EFD56697E5ECC3E733B,
	RectExtensions_SetYMax_mFF54ADAA81D0D33AAA4835B63CCDE3C2A5E6A214,
	RectExtensions_AddYMax_mAFF59FE7BFF7AD58182765E419B1BEFC7F0286BE,
	RectExtensions_SubYMax_m62763B948FAB91266E54922064BE5E96695AB786,
	RectExtensions_MinWidth_mEE5A7F7E7ED7A1671338411F16C7A726EB57EBFF,
	RectExtensions_MaxWidth_mF32385BD1955090106420BEAD91CDE5A0AE127CF,
	RectExtensions_MinHeight_m1DC4AF78A97A12996512DFF8B6589C58C37B34E3,
	RectExtensions_MaxHeight_m334669D6808352FE029F25A01A769C92C7A4C27A,
	RectExtensions_ExpandTo_mFABBE0B7E524BDE1EFFD7148E4965994EAEDDC59,
	StringExtensions_ToTitleCase_mB9B316832D46FE73E51A9663E469C7293AED41C9,
	StringExtensions_Contains_mD4E8705337729736E32CD3AC79F25FF9465218BE,
	StringExtensions_SplitPascalCase_mBF2018941241885F3B1A5E5E74FC5AA03A72872C,
	StringExtensions_IsNullOrWhitespace_m0595931B5E1321BE0488D986DC3DAFBA87879031,
	TypeExtensions_GetCachedNiceName_mB31E4F835E6E803BD222C847A4F9E9A5CF00B933,
	TypeExtensions_CreateNiceName_m50AEA1CCA0413092726B1632F8025C2BC80BAC29,
	TypeExtensions_HasCastDefined_mB133D445B3BF6F2C4B82B19781D146F057279EDF,
	TypeExtensions_IsValidIdentifier_m47E8197DAB361AA566DF0477781DF257216AE9E4,
	TypeExtensions_IsValidIdentifierStartCharacter_m1A7729893C8F48B364B80AFC277D39B221F4953A,
	TypeExtensions_IsValidIdentifierPartCharacter_mCE9FBF6524454FFE8745464BC5DE1003B841F663,
	TypeExtensions_IsCastableTo_mB328E0B03BD1059CE38164C92D5BFA9B599616D6,
	TypeExtensions_GetCastMethodDelegate_m45B494F42E4689E53E932F01962A0AF8FF560CE3,
	NULL,
	TypeExtensions_GetCastMethod_m1E45DF17AC9C43E804CA496424241079AA92C57C,
	TypeExtensions_FloatEqualityComparer_m7697C086CE77E3A7A8D97B9B81DC26F57936B8CE,
	TypeExtensions_DoubleEqualityComparer_m531ACEF34394BDD2B3BA69A2A85BD89B4B73DAF8,
	TypeExtensions_QuaternionEqualityComparer_m9F9925CDEF821367232A22589A977BDCBB674464,
	NULL,
	NULL,
	TypeExtensions_ImplementsOrInherits_m8E23334851CA981915ABB06D42A8006EB1E71E2D,
	TypeExtensions_ImplementsOpenGenericType_m1847799486A6B8BF59900F17A0E89D53705CCA8F,
	TypeExtensions_ImplementsOpenGenericInterface_m0FD4B21E28C47C239425F66E1E67493BC6C4755E,
	TypeExtensions_ImplementsOpenGenericClass_m965DC03969A412EBDC18BB5D86E970E4545F4AB7,
	TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m95FF1DADBC80D367744A4E5CBCF24AA8A192641F,
	TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_mA17992E3F2A3A0B34D9B5618C13A5A8BBB146CCB,
	TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m34AA0B9FD29009CCF61B4730328B7D905C92F5A5,
	TypeExtensions_GetOperatorMethod_m53F8A003511735F3450F2D090240E951A31577BA,
	TypeExtensions_GetOperatorMethod_m9E43DACCFA5A920923CE6E11BF063C1201ECD30D,
	TypeExtensions_GetOperatorMethods_m81F2EB856D14490DCF9D389765C8BE9A83ECC502,
	TypeExtensions_GetAllMembers_m42F29BA220A4DC676E5E1A6E977B4B7F4F36AF5E,
	TypeExtensions_GetAllMembers_mF06823B17C872322362C5497C49A5B0640DF90A0,
	NULL,
	TypeExtensions_GetGenericBaseType_mB8440ED4542248F0CCFB747B2462DB2973A8D576,
	TypeExtensions_GetGenericBaseType_m23B0DC843BA54F392EBF17D265ACD23EE7E40C52,
	TypeExtensions_GetBaseTypes_m88F667C6C77571F5796C1D8EFD8B820021482FB0,
	TypeExtensions_GetBaseClasses_mD3A1D3413EC8865CC7F0F4AAD2E86BC9378F4083,
	TypeExtensions_TypeNameGauntlet_mD36134A2F32258085D95038D638C73A724B5BE30,
	TypeExtensions_GetNiceName_m983B921831835109AC6E22EA98393B61A45D965A,
	TypeExtensions_GetNiceFullName_m4128E1678161C1BF028D4A769E374886D0045077,
	TypeExtensions_GetCompilableNiceName_m0CFABA952AEBC7601065DB82766595CCF4E63828,
	TypeExtensions_GetCompilableNiceFullName_mAA85B6E2BF337D7B2872003C9853A64C30569EAF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TypeExtensions_InheritsFrom_m762278654D771B22055D4F401E7C236E80BFC521,
	TypeExtensions_GetInheritanceDistance_m71C26D3B7EA359B1546F4AD79212C6584C2BE86E,
	TypeExtensions_HasParamaters_mB2976B5A64C80E9FF4191BC7F6CCE00F200CA836,
	TypeExtensions_GetReturnType_m291D3DCFFF1E0BC47D77C68EC57B54BA12C35C9F,
	TypeExtensions_GetMemberValue_mEC3A4278CB6E2B7F74BE32065AE5378F294255CA,
	TypeExtensions_SetMemberValue_m41BF920FBA355BA316E9A4DB69034CC97E9D4D1C,
	TypeExtensions_TryInferGenericParameters_m616BF0356A54815A95874A35349DE8E1C1A386E8,
	TypeExtensions_AreGenericConstraintsSatisfiedBy_mE18B30C962AA15EC643C6CF40A36AD7469C53E08,
	TypeExtensions_AreGenericConstraintsSatisfiedBy_m93E8C890160A065BA9260820A197BC0E79BB27E8,
	TypeExtensions_AreGenericConstraintsSatisfiedBy_m1193BE222CCCC25CEADAC6E1768E40FBB09004BA,
	TypeExtensions_GenericParameterIsFulfilledBy_m9099CDA05D4394848D7F5C5008FF219537693C94,
	TypeExtensions_GenericParameterIsFulfilledBy_m2BC87AC545C8C8274233A88F3EBBD216B5B4860A,
	TypeExtensions_GetGenericConstraintsString_mD1AAC4E266DC668B985793D40AE9D20EE96DEF27,
	TypeExtensions_GetGenericParameterConstraintsString_m92101CDFB675059CE4937378BE45EEB7D8B2C2FA,
	TypeExtensions_GenericArgumentsContainsTypes_m1554521EC1814E889071C4A627BBC530D40F97D5,
	TypeExtensions_IsFullyConstructedGenericType_m40930F66B306AF33E5198EE8816CE38AB451ECA5,
	TypeExtensions_IsNullableType_m0D2F26F7AC9C21752CFA969F73F3BB03F012E8F4,
	TypeExtensions_GetEnumBitmask_m036AACF48FB1E2DF3366EF4ACF05E6BE9FF41918,
	TypeExtensions_IsCSharpKeyword_m5DAA06A44F83CB45BDB1F4F3D8233AEAC9D040B7,
	TypeExtensions_SafeGetTypes_m8C7C79CBE78F0A2CE27DE2CE90B22DD5178DD6B4,
	TypeExtensions_SafeIsDefined_mB2886AC4FE511CF0E058C5FDADD379321216B418,
	TypeExtensions_SafeGetCustomAttributes_m1C27BF57A9D5231695C7AA397E1A96AB439BA587,
	TypeExtensions__cctor_m4C48D5A750AB56CE24DFD17CC28E81374C2C8667,
	U3CU3Ec__DisplayClass29_0__ctor_mC8D394A379A62D07F430EBE5AE4CE39A8FDE8C6A,
	U3CU3Ec__DisplayClass29_0_U3CGetCastMethodDelegateU3Eb__0_mE26E2A61B4BEE3D76B89D07CE760696A203E3279,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass45_0__ctor_mE2344F83F9E0797A0FFA95A87AC38B9DC734F21C,
	U3CU3Ec__DisplayClass45_0_U3CGetOperatorMethodU3Eb__0_m10CE18C90803214FDA5AB8294FBBCADBA0CE40ED,
	U3CU3Ec__DisplayClass46_0__ctor_m35E9204597BCF911437B6D002456A67454196934,
	U3CU3Ec__DisplayClass46_0_U3CGetOperatorMethodsU3Eb__0_mE4E4F5922DFA099C791F069D80F7164A3A7D7475,
	U3CGetAllMembersU3Ed__47__ctor_m35859B5371788C990C18EF2D4189E75649FC6FB5,
	U3CGetAllMembersU3Ed__47_System_IDisposable_Dispose_m9E592A6C17D363B9C256AB7215BA1B1201622D9C,
	U3CGetAllMembersU3Ed__47_MoveNext_mEBC0272EF45C07191B767EA40B0CDC28FB852511,
	U3CGetAllMembersU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mCFCA39437526D8F5F821AA01E6F091D427DC83E8,
	U3CGetAllMembersU3Ed__47_System_Collections_IEnumerator_Reset_m302E849F1B3FF4F2D4DBD91F1DD290096360BCB8,
	U3CGetAllMembersU3Ed__47_System_Collections_IEnumerator_get_Current_mF153491D0B84D4441736381581CC68B3A98BA0A2,
	U3CGetAllMembersU3Ed__47_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mA8F9DE86C331963278FDF18DF93DB6EB7EB53279,
	U3CGetAllMembersU3Ed__47_System_Collections_IEnumerable_GetEnumerator_m928D1703B964CE1EE20B12BA2D97EEB4BAB57CC0,
	U3CGetAllMembersU3Ed__48__ctor_m74FB21539C0CE45F69B20F16B136778209100131,
	U3CGetAllMembersU3Ed__48_System_IDisposable_Dispose_mDC6CF1ED05A6886A31305EDB0ECAFDF69BD5F281,
	U3CGetAllMembersU3Ed__48_MoveNext_m0CD7B58F20AC6E1F71EB37705EAF5A3050CCF0D6,
	U3CGetAllMembersU3Ed__48_U3CU3Em__Finally1_m5FA5AFDF6BBE696301FB2826352E559C8D8328DC,
	U3CGetAllMembersU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mEBECBE836F8C46DF252F7AA27C8C834030E7E552,
	U3CGetAllMembersU3Ed__48_System_Collections_IEnumerator_Reset_m355DF348D89CD24F596B9550FAB9F20EB378A9DE,
	U3CGetAllMembersU3Ed__48_System_Collections_IEnumerator_get_Current_mA5CF1188BE7A66F80559A2A964C8A823DAC091FC,
	U3CGetAllMembersU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m038BE30DF1E5EA486E587D21F6248A4EA40A9A8A,
	U3CGetAllMembersU3Ed__48_System_Collections_IEnumerable_GetEnumerator_mF8F4B8204B8DEC5EC9AD604CF1CFBD49EF6A1A4B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CGetBaseClassesU3Ed__53__ctor_m3860E822204D69032B9DF35BAA96A7221E7642B4,
	U3CGetBaseClassesU3Ed__53_System_IDisposable_Dispose_mBB81E192B159F326D4337A450092C9277824DD7E,
	U3CGetBaseClassesU3Ed__53_MoveNext_mA041FA869D04FBE7B2869A456361872172A09518,
	U3CGetBaseClassesU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m7468303E90A96DBEB6AE1EA4DB0997787BDBEC76,
	U3CGetBaseClassesU3Ed__53_System_Collections_IEnumerator_Reset_m88D4630F95FC04044E87AA5863FDC816880E9594,
	U3CGetBaseClassesU3Ed__53_System_Collections_IEnumerator_get_Current_mA07CBB08458521DA21FB33BE8817F9273175D8DD,
	U3CGetBaseClassesU3Ed__53_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_mDB62F4AE52A8FFACCE29476CD7A179B2E696B51E,
	U3CGetBaseClassesU3Ed__53_System_Collections_IEnumerable_GetEnumerator_m6FC6E1FA9371736CB587416C661FE05B88ED8946,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UnityExtensions__cctor_m0D187706219372FD960055C9FC3AEB3879E4403E,
	UnityExtensions_SafeIsUnityNull_m13F6AF07636FB33A74D33249B94ECA358E62DCE1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DeepReflection_CreateWeakStaticValueGetter_m7CB798E0F0E1084181C127DF6859BA9C91E438BA,
	DeepReflection_CreateWeakInstanceValueGetter_mD5E0EA98E92907DF00A46D9763DEF73D7E4654EF,
	DeepReflection_CreateWeakInstanceValueSetter_m465072E6F95F97281E2381F3BEB767812C277BA7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DeepReflection_CreateEmittedDeepValueGetterDelegate_m73147D746464D99B1BF220532CA007AE67092E27,
	DeepReflection_CreateSlowDeepStaticValueGetterDelegate_mD1507C26516B56887AD361F319F21423CD00F0FD,
	DeepReflection_CreateSlowDeepInstanceValueGetterDelegate_m914B614497033FBF0155446FCF4A2C6770C69481,
	DeepReflection_CreateSlowDeepInstanceValueSetterDelegate_mD2B015723D061E6B52259AEF3A8B63EAFF8AFE46,
	DeepReflection_SlowGetMemberValue_m5AB0C302750A2A2FC2BF9460419C69730291F2C7,
	DeepReflection_SlowSetMemberValue_mB84D330B8C18183F0E463F4460BB70CA78477657,
	DeepReflection_GetMemberPath_m30C1E442AB295E8DCBFA643B40F382F0312F4882,
	DeepReflection_GetStepMember_m17710C7EFA9C95343450A2984874D5657C82AE75,
	DeepReflection__cctor_m321AD4286CF1C87262CD38F1A185AFF046F95C59,
	PathStep__ctor_mB52683D0E76A1108856405623013CD634DD0045C,
	PathStep__ctor_mDDB9DFE8DC648E7D5A6A1ED81FDDC568AFB85B7D,
	PathStep__ctor_m13D1858B708B2DC0C87519F5D97241B1D4C8DF8E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass21_0__ctor_mE8326B8DD7F367F8621D35E382A3671B563E07AF,
	U3CU3Ec__DisplayClass21_0_U3CCreateSlowDeepStaticValueGetterDelegateU3Eb__0_m99D8C06D4886A529C9AB0B9842B44D19C0446F98,
	U3CU3Ec__DisplayClass22_0__ctor_mAD37F46F1CEF3B99ECF3762E73E90C4333B7DD65,
	U3CU3Ec__DisplayClass22_0_U3CCreateSlowDeepInstanceValueGetterDelegateU3Eb__0_m58B5D11EF60292BC90EA2E6DC8EF9EC07F7BC706,
	U3CU3Ec__DisplayClass23_0__ctor_mD7EABEAD23B7296A6F36AA0F4CD424F50620D16A,
	U3CU3Ec__DisplayClass23_0_U3CCreateSlowDeepInstanceValueSetterDelegateU3Eb__0_m001D8499A30A599738D407CC4F33CC6CBCCF767A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WeakValueGetter__ctor_mF70F2B34AAE2E2142E2C154E7E47653254AAE0E9,
	WeakValueGetter_Invoke_m7343377557FD99EC7DA973A5A838DF8C763D9445,
	WeakValueGetter_BeginInvoke_m25DAA67B6CF910CFFA8566D5F02468CC8444226F,
	WeakValueGetter_EndInvoke_m5325EB1F451D5B1D1A7C0DC3A60B154BB1BBC44C,
	WeakValueSetter__ctor_m820EE8552D61007F82C368AE697E6EBD323C091A,
	WeakValueSetter_Invoke_mDC51ADD44D4B6E5EBC6FBA28C320644AC6FC8E06,
	WeakValueSetter_BeginInvoke_mAB8B0370E3503087745FDEEEA1687673C783492B,
	WeakValueSetter_EndInvoke_m352BB4577611876F53B63A5574AB284868EFF3D8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmitUtilities_get_CanEmit_mEEE05DA6926EAA48D4DA77B2B0511A7B5F60FC9D,
	NULL,
	EmitUtilities_CreateWeakStaticFieldGetter_m12ACB9D134C2EFB21B24F7D215DD20C4B3D3BEF4,
	NULL,
	EmitUtilities_CreateWeakStaticFieldSetter_mB43A2E2F392A9658431A8FAC332D83B904ABBC4E,
	NULL,
	NULL,
	EmitUtilities_CreateWeakInstanceFieldGetter_m6FF91BE2964C5E89931E29E0A53F68D71669CDF4,
	NULL,
	NULL,
	EmitUtilities_CreateWeakInstanceFieldSetter_m493885E1D5C6331544B03CB4F3AE879BC40F093C,
	EmitUtilities_CreateWeakInstancePropertyGetter_mD8DD83D429C91BDEA82634330C878DA09957D10F,
	EmitUtilities_CreateWeakInstancePropertySetter_mD3191C704184E1F84E03467DAC0BC0F300360369,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmitUtilities_CreateStaticMethodCaller_m0751556DFD7D6F381FDB49BEBF732D9448676FBC,
	NULL,
	EmitUtilities_CreateWeakInstanceMethodCaller_m62A20A4052B5B1EDBB6325871BCBF8E5456ACC64,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass3_0__ctor_m11E41571289025E78307FB4EBBE3548356441A00,
	U3CU3Ec__DisplayClass3_0_U3CCreateWeakStaticFieldGetterU3Eb__0_mBF92B435913B6873671BED19EE397E49D3DA9841,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass5_0__ctor_m6E0E3439FE97D07B9ABEF22223D6704B34F68182,
	U3CU3Ec__DisplayClass5_0_U3CCreateWeakStaticFieldSetterU3Eb__0_m9404E80A3EE5F70BCECA37E12BC062C9C1471402,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass8_0__ctor_mE35A54E871C8C15C0298DF03B3A9B1A520B12C39,
	U3CU3Ec__DisplayClass8_0_U3CCreateWeakInstanceFieldGetterU3Eb__0_m24D8FC28CFD2DFCE3F646A39D4BEFE81092072C2,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass11_0__ctor_m5C44409E6876809A816D37968354A9DD21429580,
	U3CU3Ec__DisplayClass11_0_U3CCreateWeakInstanceFieldSetterU3Eb__0_mD6D53102BD0E3F8C6786A4BA35DA5E6225CAE2CE,
	U3CU3Ec__DisplayClass12_0__ctor_mF308CA2ABF7D747C8AF08729277EA3F6D4AFA2D6,
	U3CU3Ec__DisplayClass12_0_U3CCreateWeakInstancePropertyGetterU3Eb__0_m6A31165FF2D41AEC9A8EE37A01BC6B4AF9D72CD7,
	U3CU3Ec__DisplayClass13_0__ctor_m2FCFA4B0FEFFF0891FDDB450F35337E43FCF5ECF,
	U3CU3Ec__DisplayClass13_0_U3CCreateWeakInstancePropertySetterU3Eb__0_m30D08F80BFB6D4D675D0748D0FCD56D3E17D5F74,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass21_0__ctor_mBB5540B4858B6686D3B2846893148E2CBF6B260B,
	U3CU3Ec__DisplayClass21_0_U3CCreateWeakInstanceMethodCallerU3Eb__0_mF9ADFF9980EADEFD9F7FAB20B083F39C65CB3EB2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	GlobalConfigAttribute_get_FullPath_m07FC67A8EDA4A84F33D19A32CEC093BEEE0A6D22,
	GlobalConfigAttribute_get_AssetPath_m427BBDBB1149BA9F468A8CDA286595188C492056,
	GlobalConfigAttribute_get_AssetPathWithAssetsPrefix_mA5A6A0003C711786C45B717B336AFF025FA6DC20,
	GlobalConfigAttribute_get_AssetPathWithoutAssetsPrefix_m5D3C44A53E103C68DC9D940B4CE1368CAC21FE90,
	GlobalConfigAttribute_get_ResourcesPath_mB30337DEA7AD51CB4BFEDAB7E4FA26EA7CB21454,
	GlobalConfigAttribute_get_UseAsset_m9856D2CB6C77E94FE7B3912A8B737B037915BA9C,
	GlobalConfigAttribute_set_UseAsset_m93174EE36A6B1104173D79C950F42B2F26F23F5A,
	GlobalConfigAttribute_get_IsInResourcesFolder_mAC8EC0816E0082A69C5466052F3967F3D7AE9D0C,
	GlobalConfigAttribute__ctor_mF902E491F687BAE3659492840D2177106137C202,
	GlobalConfigAttribute__ctor_mC3412D28ED4D5644F652B3DC0D38EB333BC32C61,
	GUILayoutOptions__cctor_m476A43587EB7F1AB2F4F205488C4622C5FE58445,
	GUILayoutOptions_Width_mA2D5B0EA8DB946BDE0AAEC6F7F566E2981EB5FD0,
	GUILayoutOptions_Height_m6D046D7D797DAC01C86DB8F12468F39DDC31ABE7,
	GUILayoutOptions_MaxHeight_m1B9834D060BDD8617985B8FE02D964629BFED9F6,
	GUILayoutOptions_MaxWidth_m656793C57F6835B7651CCF41853B979190E3F2B6,
	GUILayoutOptions_MinWidth_mB8B3103776789E2CAA55F0461694581A84CAF483,
	GUILayoutOptions_MinHeight_mF3265FFDC7E3D9909A0D9B46BAE91F50AEDCEAB1,
	GUILayoutOptions_ExpandHeight_m2417E4DFAA2117877F5627EE9718F6AD389B119B,
	GUILayoutOptions_ExpandWidth_mBB4033514B29B1117426E5780C6D918648F0FFC6,
	GUILayoutOptionsInstance_GetCachedOptions_m7D4F9C3AD94A8E947601FF654236D3C4AAE7F589,
	GUILayoutOptionsInstance_op_Implicit_mAEA3D249416FB375D3690A6A0E76A0D6CE0F9A2D,
	GUILayoutOptionsInstance_CreateOptionsArary_m3A418EC638AAB846F4C7BE7B429E71FD206036E1,
	GUILayoutOptionsInstance_Clone_mD51C24C022503E715B70EF9A6C63E8F4EDB38529,
	GUILayoutOptionsInstance__ctor_mACB4369C1F5BBDD40D0FE54B4B0E19D2C4F7ED5E,
	GUILayoutOptionsInstance_Width_m285EEAAA11A7EE470B835DA9420A73EA16F349DD,
	GUILayoutOptionsInstance_Height_m1CCA12E04F9D14F9802A2B7D6C168F7AE31A45FA,
	GUILayoutOptionsInstance_MaxHeight_m75C2927279F6119B03144E927AE3AD9C58772D5D,
	GUILayoutOptionsInstance_MaxWidth_mE57D9D290339E0A9DFBC47BD5E54A00639725958,
	GUILayoutOptionsInstance_MinHeight_m6B5ECBB1753A763903BA4452817B89CBF36E4AEA,
	GUILayoutOptionsInstance_MinWidth_mC055C2D48A3651A1F230F9F77E93842C5707C350,
	GUILayoutOptionsInstance_ExpandHeight_m7F0B05372FED5BB8CC8C3A0D2A79E5662AE4E88E,
	GUILayoutOptionsInstance_ExpandWidth_m12B9AEC4DCF98ACA50DDF71B0D134F5CAD4DD4B9,
	GUILayoutOptionsInstance_SetValue_mAEA1B52B86FEA7742ACA48E47EAD897D51274F19,
	GUILayoutOptionsInstance_SetValue_m62582CDD8E478FE5BC93051A974BDF563C003AA2,
	GUILayoutOptionsInstance_Equals_mDB344B170317F987100BA55DADA16EC8F4350DC9,
	GUILayoutOptionsInstance_GetHashCode_m3CF8A827F96FC884013426376D313C3F6AD2E340,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ImmutableList__ctor_m3A86E3041EF3FA6B4A457545AC8209BA3004E626,
	ImmutableList_get_Count_m022B612F98D5E10DD1778661DC2A92D481C8F7BB,
	ImmutableList_get_IsFixedSize_m30BF44E714C285B10AADD1F60773E07CF647D6AB,
	ImmutableList_get_IsReadOnly_m226A23AB281FBE2FEAE3A193D7E92CE0B2107248,
	ImmutableList_get_IsSynchronized_m307640C287150EBD9E46072A25A525E642169FFF,
	ImmutableList_get_SyncRoot_mF38CE30571B823471F6FAD29CBAEF763898680EB,
	ImmutableList_System_Collections_IList_get_Item_mA4D9CB5C723608304FEB6B89087389B0E12024DB,
	ImmutableList_System_Collections_IList_set_Item_m4FC71C93B753F4956BDF02BB2CAA74FA1BFC00B7,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_get_Item_m667B25A5D3AB12D69E3DECD98C9218B31E1C3BBF,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_set_Item_m8DF41063F7D14E93EFCE87CE2F2E64EDA20E3A3E,
	ImmutableList_get_Item_mA505123D05C082B726104B9CCBA0DFE46C325B6C,
	ImmutableList_Contains_m1302761C267A54B73DB7C1775FF93ADB47F1A3C1,
	ImmutableList_CopyTo_mD90DDB89555209C8D96EAF860A82E99EB130E070,
	ImmutableList_CopyTo_m48EE4ED3A35585ABF769C77161D19E4330126545,
	ImmutableList_GetEnumerator_m66C41FDF6A2E078837658672A98773E949C5CC7B,
	ImmutableList_System_Collections_IEnumerable_GetEnumerator_m6BC2144DC7020F7C16F2AD9D05E3C3D5F9988992,
	ImmutableList_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m0E35F57B4068265055F4956A5D47CA90FDD1CFB3,
	ImmutableList_System_Collections_IList_Add_m9BB676E06BCC478C6028780AB735F45FD8D4E151,
	ImmutableList_System_Collections_IList_Clear_m4BF7931F9ED3F05DB4F895E3755D16CC80697844,
	ImmutableList_System_Collections_IList_Insert_mFD10BBF3202AC23CF934EA7D9A2264A248BC0EA9,
	ImmutableList_System_Collections_IList_Remove_m31C5D8B6EE9311FD453AF711CC202DF8F3DAAFA6,
	ImmutableList_System_Collections_IList_RemoveAt_mF07593D4830DAB5EE31632BED6898552BD3C6DB3,
	ImmutableList_IndexOf_m26A37AB82763EA4EC38B24D2E1EA4D67CB9468D5,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_RemoveAt_m25DF0E4242E333A5CE846043A7F2AD46D47D696F,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_Insert_m14CA7010AB4805E17943E848372682D21F07064A,
	ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Add_mEFF85DCC759305309E4FB98CD10C48D22EB677E5,
	ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Clear_mBF95AD7AF28E208E0C1B1EF96B7223EA163E644D,
	ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Remove_m81CD560F60BE64EC7A64CFF80EE98BA8B16559CB,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_m4BB1374E87B307242E06C3503D7A46BFA383852A,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m2947B87D0F37D72931BB8C4C51F0408295B1B24C,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_MoveNext_mBDC67E39FD4E5F170B201FE45711A78DC6CF2ABE,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_mC4BEE0754AB86918AE3C1FAF99195A83D081CDBB,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21E45AD31E3B8130252713DEB5382BBD58A4017C,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m7A40897EDC5F324764DE3C1F18356A11189E2EE8,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m27BE8890BAAA820C03048FFCD6B7FE30B6444920,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MathUtilities_PointDistanceToLine_mECCAE353427826C4E3AF68C42889ABFBD61CF055,
	MathUtilities_Hermite_mAEC14C01C7B5223178183D0873C2A3A927F28EE0,
	MathUtilities_StackHermite_mDF7A4BF2A2F3B2FD38FE58A398A17C0FCD229111,
	MathUtilities_Fract_mE218E70D376BDE6CB7E32E4A1B128937CADB9024,
	MathUtilities_Fract_m75D765D629E3448718AA9AF2173C889C7E46A378,
	MathUtilities_Fract_m5ECE85AA5D40BA2CE404E0C2E6C80A1AC8F021B3,
	MathUtilities_BounceEaseInFastOut_m4DBD5FA130041834EAB86E1959A379A2B43E4EE7,
	MathUtilities_Hermite01_m658A8E5577356F0F930026D4AD2CB2CD44308330,
	MathUtilities_StackHermite01_m06A36D8A6355EB1C219CEB6F70FC75EE07C6C8B5,
	MathUtilities_LerpUnclamped_mEBC6CF6B0497E63887B036312BC58719A947D0B3,
	MathUtilities_LerpUnclamped_mCB26B95223EF8A3938039AC8B85A1590C8069656,
	MathUtilities_Bounce_m387F269021F722588B3C00589D3E391755FEC87B,
	MathUtilities_EaseInElastic_mCAC804C66FC6D20E51D24825EFEDF9508918BA9C,
	MathUtilities_Pow_mC24BF2057699AAECCC14A3E925C909BB65420FD1,
	MathUtilities_Abs_mD9793B239B31D3152B688A12332B068194971356,
	MathUtilities_Sign_mE828BF27DCE0AA4875CF9C1F786D1368C0987C09,
	MathUtilities_EaseOutElastic_m7100232D382537F53782AC80BF99AD2F48B7510B,
	MathUtilities_EaseInOut_m47261C60E7A9B14F9BF2AEC9FBA89C8C5B27CF7A,
	MathUtilities_Clamp_mD209E6E22DE6B012FBF6FDB520DD6CB838938135,
	MathUtilities_Clamp_m2F5273420C5743FC4298F515F58441A3A45B9397,
	MathUtilities_ComputeByteArrayHash_m770C66275712427E1DA4C0A55B7D4D26857E67C6,
	MathUtilities_InterpolatePoints_mA27020C45BABAFA61F8986688010519E2093820D,
	MathUtilities_LineIntersectsLine_m7A9438FE9AC96F398D94DA612980E203046E5FA4,
	MathUtilities_InfiniteLineIntersect_m91342D3D2A7830BE98ED029AED275F02A633CCCB,
	MathUtilities_LineDistToPlane_m6463EEBC27DECC1F966E036459375A38705CCEA8,
	MathUtilities_RayDistToPlane_mC3BC1544A22F0DFA732D55401E52D1C218AEDFC9,
	MathUtilities_RotatePoint_mB5F66BA092BAA3F9523A10559E152B26C66BB33D,
	MathUtilities_RotatePoint_m7AB1A7EB89A309E638918B7787A70FDA305892DD,
	MathUtilities_SmoothStep_m3A61D9D5F657B43E8D78B1B6E4B74D6220E6C9C9,
	MathUtilities_LinearStep_m01975B4D09CE83EA54430520E7F7D74837730E9D,
	MathUtilities_Wrap_mF4407A877B05B12D64A88719C5B7EC4C69868CD3,
	MathUtilities_Wrap_m4C935F50AF413153E373296D9FEC5C85BD239161,
	MathUtilities_Wrap_mA3D53BC08DD3350AADA524C17552B9069C826BF7,
	MathUtilities_RoundBasedOnMinimumDifference_mEB43E6F62C4E4CCE7469805E920ED4D61F7A9BC8,
	MathUtilities_DiscardLeastSignificantDecimal_mC4332BA05E24F088FAFBB5F0BC0A4F0D541D508E,
	MathUtilities_ClampWrapAngle_m3B3ABE897B244B506EC97B3BD35C7DE9AA5D4B9F,
	MathUtilities_GetNumberOfDecimalsForMinimumDifference_m47FEE6F9B093A1E29FA8B7A13DCE6B55884077BD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PersistentAssemblyAttribute__ctor_m3FAE02AF2158F07B3139934DE577F690BC616837,
	MemberAliasFieldInfo__ctor_m7F1F8CCC9218DC6DD601F4DECE32E7A4E23B1124,
	MemberAliasFieldInfo__ctor_m689CBC3B1F160155C22D22705776B2F509904E46,
	MemberAliasFieldInfo_get_AliasedField_mCF6792CB78A4E12A31A47B7692E208F42A25E4BC,
	MemberAliasFieldInfo_get_Module_m967F20933DE077A43137562B3E8E90002C4A7507,
	MemberAliasFieldInfo_get_MetadataToken_mFD230E83F9CB7EBEFA53EC3BD4C5D1508D83F343,
	MemberAliasFieldInfo_get_Name_m7958C05205747FF45F5FF2043C29B4E82C95068E,
	MemberAliasFieldInfo_get_DeclaringType_m0CA1A39EFB2994459257AE8365B39F2DF484BADC,
	MemberAliasFieldInfo_get_ReflectedType_mF7EF922323C545A9F0E056DC9F8C763BCC651B4C,
	MemberAliasFieldInfo_get_FieldType_mB5BB2FF53B46742BB1010CA24DDA44EAC57EB41D,
	MemberAliasFieldInfo_get_FieldHandle_m78D86DDCCE4E73ED9C4228B243815DC20BA33D51,
	MemberAliasFieldInfo_get_Attributes_mC639BC47EF1DCB681CFFDE6BE90F15C8BC23BF5E,
	MemberAliasFieldInfo_GetCustomAttributes_mBFD30F074E7C3C71CB49A9675D5C9498E0137658,
	MemberAliasFieldInfo_GetCustomAttributes_mC988A1D459D5C7C8B533F64EDC0A1E80C0F4BC55,
	MemberAliasFieldInfo_IsDefined_mC4AB0AF29B26D6B03DD7508658A022D75BECB72A,
	MemberAliasFieldInfo_GetValue_m076F01BF95D1B1A81F2C9776F74A8A6C98ADA6B7,
	MemberAliasFieldInfo_SetValue_m65FEC1521B2D8DD764B53AA62DE879EC4A73E894,
	MemberAliasMethodInfo__ctor_mDFE6D288860CD2AC26A0F9D20C09B083C3582923,
	MemberAliasMethodInfo__ctor_m920A2C5254778046D8B91966DE747731199F095F,
	MemberAliasMethodInfo_get_AliasedMethod_mC3C268B262DF1E0B55593AFCF73F78D65ECDCF7C,
	MemberAliasMethodInfo_get_ReturnTypeCustomAttributes_m53F9625127315D522FF9128F6974101236F0948F,
	MemberAliasMethodInfo_get_MethodHandle_m8ADFCA7E06CA6EA34EFA6EAFCD63E85A59EE0578,
	MemberAliasMethodInfo_get_Attributes_m296C0BF440F414B6CD25BFE02B0C2BF58E945642,
	MemberAliasMethodInfo_get_ReturnType_mB591F11265E82258246BD5B1131D95537467A4BE,
	MemberAliasMethodInfo_get_DeclaringType_mF96C84D3F6F2446C5C0C04DC404E5A95C86D7C41,
	MemberAliasMethodInfo_get_Name_m8AAAA2740BC4BAAE349720F2FDBA26C9A274AE05,
	MemberAliasMethodInfo_get_ReflectedType_m2B5FD961697975A7A39835A81AC27EF4F8E59BF2,
	MemberAliasMethodInfo_GetBaseDefinition_m367A6EC75FADFAB7F8ADA34E0152758D218445D7,
	MemberAliasMethodInfo_GetCustomAttributes_m011FAF5CE60A6362123B748EE5B673DB63EECBBD,
	MemberAliasMethodInfo_GetCustomAttributes_m2CCA5174E49B52D3BDEB7214FDF289F15571904C,
	MemberAliasMethodInfo_GetMethodImplementationFlags_mE33BB4660418351ADCF127699ACA0EA318D411B7,
	MemberAliasMethodInfo_GetParameters_mA341EECB4DDF72E049F7322C7836CBC85AEA6A55,
	MemberAliasMethodInfo_Invoke_m9FC3272E9ADAC29549A8FBB88E5C1EBC6A332DAA,
	MemberAliasMethodInfo_IsDefined_mC87F4450B971BC3C67B81CCF0A069A453E4FA1E6,
	MemberAliasPropertyInfo__ctor_mA2D89283CA83A1A4E9006381C85DCABE332F11EC,
	MemberAliasPropertyInfo__ctor_m3415416072338EB557C0382A4102C627C925F05D,
	MemberAliasPropertyInfo_get_AliasedProperty_m43939436AF91312C7CB265EF9124B5FEAECF5AD4,
	MemberAliasPropertyInfo_get_Module_m2BB0AB57E7C613AEF68011E4D1C218F32E6EF5EF,
	MemberAliasPropertyInfo_get_MetadataToken_m60502075878AD3E4D121104FB4357980C97E6E7C,
	MemberAliasPropertyInfo_get_Name_mEC97801686137FD552D7DA5654FAF4A308B47F4A,
	MemberAliasPropertyInfo_get_DeclaringType_mD4D26247B273389E689BA98902A383CD1285E130,
	MemberAliasPropertyInfo_get_ReflectedType_m2AC06C96ECE0CD5D0967BCB9CC5B56820464D996,
	MemberAliasPropertyInfo_get_PropertyType_m1FA174F70B0140611092D33DBDC26ACBBD70BCDB,
	MemberAliasPropertyInfo_get_Attributes_m95EE091CAABE46A27CD158050F3850538E4A00B1,
	MemberAliasPropertyInfo_get_CanRead_m56C4B6D408F4EB2501B3C04EF6EBC7404A753AC9,
	MemberAliasPropertyInfo_get_CanWrite_m840343CA9A7766CA30528B6A38469462F1E0B6FD,
	MemberAliasPropertyInfo_GetCustomAttributes_mBD1D704D2F33E35D5F156D74A27877ACA53D9571,
	MemberAliasPropertyInfo_GetCustomAttributes_mB0AEF877D21DB7C5C79847DD10C87B301E364DD2,
	MemberAliasPropertyInfo_IsDefined_mDFA358D34DCDEEDA6B8ACCDAA418BF29FF83F155,
	MemberAliasPropertyInfo_GetAccessors_mAD1E7E77F4C85F7EED905B0CC7A74B7551CDFE7F,
	MemberAliasPropertyInfo_GetGetMethod_mF23C8DA518A9A0658C3E9C98616526D5C4B0B255,
	MemberAliasPropertyInfo_GetIndexParameters_mDAB2CE638402687701B3F18E0E83849617AB4F7F,
	MemberAliasPropertyInfo_GetSetMethod_mF3AABEE78A859BF71F29BFA4F11F874E805CD71D,
	MemberAliasPropertyInfo_GetValue_m94AF671CA2ACECF9350C7F7E04F210BB5787DF4F,
	MemberAliasPropertyInfo_SetValue_mB288F5C43A859E61AAC5E17AA11FD6D780BCE497,
	ProjectPathFinder__ctor_m21A935F34675629AB691349121C503DE2D4B6A9E,
	SirenixAssetPaths__cctor_m0C805983566629D240D6E4DE9E46B5CEA97D7717,
	SirenixAssetPaths_ToPathSafeString_mFC4DB86F6F5A3B298B37C026752DAC63F59BEDCD,
	U3CU3Ec__DisplayClass12_0__ctor_mE3735CED949904218E19A344742A70919D9A052E,
	U3CU3Ec__DisplayClass12_0_U3CToPathSafeStringU3Eb__0_mBC49F4EB4A50B6ACBF4B08AAEF340BA6BA5DB274,
	SirenixBuildNameAttribute_get_BuildName_m6A6471920F0D0BE07D648C7630F7D92F672DA826,
	SirenixBuildNameAttribute_set_BuildName_m7E1559901D13E2E9B18227B4959E8FB9EA090E0B,
	SirenixBuildNameAttribute__ctor_mAD7C4444D93F954F2F0534EC907F1B3C6DDF9B91,
	SirenixBuildVersionAttribute_get_Version_m5B4095D2A8633E2D7A09FAE2DA3728DC52E51671,
	SirenixBuildVersionAttribute_set_Version_mE037801F39B45C45076F7D0F32E98905F430C129,
	SirenixBuildVersionAttribute__ctor_mB01FF1D3895F96A87DA85B3B9060A6DC02AB574E,
	SirenixEditorConfigAttribute__ctor_m7F535FC2498C442C1417BDE90F9B7AED6D2827E8,
	SirenixGlobalConfigAttribute__ctor_m41A63774EA77E1DFCF1DD463FE4C550C82AFBA70,
	StringUtilities_NicifyByteSize_m11CB909F38D2115EBE6D2C613964E61ACF2ACBAF,
	StringUtilities_FastEndsWith_mF7B8C034F595DBC9D34AF61B566E9535C7EC0CB3,
	UnityVersion__cctor_m1D69DE00E7984BE313CCAAF5E7C78D1DD9383576,
	UnityVersion_EnsureLoaded_m0A48330D08515576EDFDA5CDF60CB041BFBB0C72,
	UnityVersion_IsVersionOrGreater_m8650838D70B6B827F91D540410F5E1964D40F19C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UnsafeUtilities_StringFromBytes_mBAD899386A78E9887F8F18E15761881F04CEED11,
	UnsafeUtilities_StringToBytes_m7F228372A7683612AC69C54232CBD792488E673B,
	UnsafeUtilities_MemoryCopy_m6AE1EE2677ADAD57C174AC51B0DE22F192DE71A8,
};
extern void PathStep__ctor_mB52683D0E76A1108856405623013CD634DD0045C_AdjustorThunk (void);
extern void PathStep__ctor_mDDB9DFE8DC648E7D5A6A1ED81FDDC568AFB85B7D_AdjustorThunk (void);
extern void PathStep__ctor_m13D1858B708B2DC0C87519F5D97241B1D4C8DF8E_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[3] = 
{
	{ 0x0600022B, PathStep__ctor_mB52683D0E76A1108856405623013CD634DD0045C_AdjustorThunk },
	{ 0x0600022C, PathStep__ctor_mDDB9DFE8DC648E7D5A6A1ED81FDDC568AFB85B7D_AdjustorThunk },
	{ 0x0600022D, PathStep__ctor_m13D1858B708B2DC0C87519F5D97241B1D4C8DF8E_AdjustorThunk },
};
static const int32_t s_InvokerIndices[965] = 
{
	1918,
	1554,
	216,
	1919,
	1552,
	1920,
	97,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	114,
	153,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	0,
	153,
	0,
	114,
	114,
	153,
	135,
	1,
	0,
	0,
	114,
	114,
	153,
	0,
	135,
	1,
	135,
	1,
	391,
	1,
	632,
	114,
	153,
	1921,
	1921,
	1922,
	1923,
	1921,
	1922,
	1921,
	1922,
	1921,
	1922,
	1924,
	1921,
	1921,
	1922,
	1921,
	1925,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1922,
	1921,
	1922,
	1924,
	1926,
	1926,
	1927,
	1928,
	1921,
	1921,
	1922,
	1923,
	1923,
	1445,
	1923,
	1922,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1923,
	1923,
	1923,
	1923,
	1923,
	1923,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1921,
	1923,
	0,
	495,
	0,
	114,
	0,
	0,
	207,
	114,
	48,
	48,
	207,
	206,
	-1,
	206,
	1589,
	1929,
	1582,
	-1,
	-1,
	135,
	135,
	135,
	135,
	1,
	1,
	1,
	1930,
	119,
	119,
	119,
	563,
	-1,
	1,
	720,
	153,
	153,
	0,
	0,
	0,
	0,
	0,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	135,
	138,
	207,
	0,
	1,
	186,
	1931,
	135,
	135,
	135,
	135,
	1932,
	153,
	153,
	135,
	114,
	114,
	257,
	114,
	0,
	207,
	206,
	3,
	23,
	28,
	-1,
	-1,
	-1,
	-1,
	23,
	9,
	23,
	9,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3,
	114,
	-1,
	-1,
	-1,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	578,
	578,
	578,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1327,
	0,
	0,
	0,
	1933,
	1934,
	1935,
	206,
	3,
	26,
	32,
	1062,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	14,
	23,
	28,
	23,
	27,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	124,
	503,
	1180,
	1111,
	124,
	358,
	1936,
	358,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	49,
	-1,
	0,
	-1,
	0,
	-1,
	-1,
	1,
	-1,
	-1,
	1,
	1,
	1,
	-1,
	-1,
	-1,
	-1,
	-1,
	0,
	-1,
	0,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	14,
	-1,
	-1,
	23,
	26,
	-1,
	-1,
	-1,
	-1,
	23,
	503,
	-1,
	-1,
	-1,
	-1,
	23,
	358,
	23,
	503,
	23,
	358,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	14,
	14,
	14,
	14,
	89,
	31,
	89,
	23,
	26,
	3,
	97,
	97,
	97,
	97,
	97,
	97,
	825,
	825,
	14,
	0,
	14,
	14,
	23,
	1138,
	1138,
	1138,
	1138,
	1138,
	1138,
	123,
	123,
	1020,
	133,
	9,
	10,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	10,
	89,
	89,
	89,
	14,
	34,
	62,
	34,
	62,
	34,
	9,
	130,
	130,
	14,
	14,
	14,
	112,
	23,
	62,
	26,
	32,
	112,
	32,
	62,
	26,
	23,
	9,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1937,
	1588,
	1938,
	444,
	1890,
	1453,
	444,
	444,
	1939,
	1565,
	1591,
	444,
	1588,
	1568,
	1453,
	1453,
	1588,
	444,
	1940,
	1941,
	94,
	1942,
	1943,
	1944,
	1945,
	1946,
	1594,
	1591,
	1588,
	1588,
	1947,
	1588,
	198,
	440,
	439,
	1588,
	255,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	27,
	197,
	14,
	14,
	10,
	14,
	14,
	14,
	14,
	715,
	10,
	123,
	148,
	481,
	28,
	716,
	27,
	197,
	14,
	14,
	703,
	10,
	14,
	14,
	14,
	14,
	14,
	123,
	148,
	10,
	14,
	581,
	481,
	27,
	197,
	14,
	14,
	10,
	14,
	14,
	14,
	14,
	10,
	89,
	89,
	123,
	148,
	481,
	123,
	123,
	14,
	123,
	581,
	728,
	23,
	3,
	369,
	23,
	665,
	14,
	26,
	26,
	14,
	26,
	26,
	23,
	23,
	593,
	135,
	3,
	3,
	53,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	704,
	506,
	1948,
};
static const Il2CppTokenRangePair s_rgctxIndices[168] = 
{
	{ 0x02000004, { 12, 1 } },
	{ 0x02000005, { 13, 3 } },
	{ 0x02000008, { 24, 4 } },
	{ 0x02000009, { 28, 4 } },
	{ 0x0200000A, { 32, 4 } },
	{ 0x0200000B, { 36, 5 } },
	{ 0x0200000D, { 126, 9 } },
	{ 0x0200000E, { 135, 7 } },
	{ 0x0200000F, { 142, 9 } },
	{ 0x02000010, { 151, 8 } },
	{ 0x02000011, { 159, 9 } },
	{ 0x02000012, { 168, 9 } },
	{ 0x02000013, { 177, 8 } },
	{ 0x02000014, { 185, 9 } },
	{ 0x02000015, { 194, 9 } },
	{ 0x02000016, { 203, 8 } },
	{ 0x02000017, { 211, 9 } },
	{ 0x02000018, { 220, 10 } },
	{ 0x02000019, { 230, 9 } },
	{ 0x0200001A, { 239, 10 } },
	{ 0x0200001B, { 249, 9 } },
	{ 0x0200001C, { 258, 8 } },
	{ 0x0200001D, { 266, 9 } },
	{ 0x0200001E, { 275, 9 } },
	{ 0x0200001F, { 284, 8 } },
	{ 0x02000020, { 292, 9 } },
	{ 0x02000021, { 301, 9 } },
	{ 0x02000022, { 310, 8 } },
	{ 0x02000023, { 318, 9 } },
	{ 0x02000024, { 327, 6 } },
	{ 0x0200002E, { 370, 5 } },
	{ 0x02000033, { 375, 4 } },
	{ 0x02000035, { 379, 5 } },
	{ 0x02000039, { 387, 8 } },
	{ 0x0200003D, { 436, 1 } },
	{ 0x0200003E, { 437, 1 } },
	{ 0x0200003F, { 438, 2 } },
	{ 0x02000040, { 440, 3 } },
	{ 0x02000041, { 443, 2 } },
	{ 0x02000042, { 445, 2 } },
	{ 0x02000043, { 447, 3 } },
	{ 0x02000044, { 450, 2 } },
	{ 0x02000045, { 452, 2 } },
	{ 0x02000049, { 454, 48 } },
	{ 0x02000052, { 588, 1 } },
	{ 0x02000054, { 589, 1 } },
	{ 0x02000056, { 590, 2 } },
	{ 0x02000057, { 592, 1 } },
	{ 0x02000059, { 593, 3 } },
	{ 0x0200005A, { 596, 1 } },
	{ 0x0200005E, { 597, 1 } },
	{ 0x0200005F, { 598, 1 } },
	{ 0x02000060, { 599, 3 } },
	{ 0x02000061, { 602, 2 } },
	{ 0x02000062, { 604, 1 } },
	{ 0x02000064, { 605, 2 } },
	{ 0x02000065, { 607, 1 } },
	{ 0x02000066, { 608, 2 } },
	{ 0x02000068, { 610, 7 } },
	{ 0x0200006E, { 617, 3 } },
	{ 0x02000073, { 620, 7 } },
	{ 0x02000074, { 627, 14 } },
	{ 0x02000085, { 665, 4 } },
	{ 0x06000009, { 0, 5 } },
	{ 0x0600000A, { 5, 7 } },
	{ 0x06000011, { 16, 2 } },
	{ 0x06000012, { 18, 2 } },
	{ 0x06000013, { 20, 2 } },
	{ 0x06000014, { 22, 2 } },
	{ 0x06000029, { 41, 2 } },
	{ 0x0600002A, { 43, 3 } },
	{ 0x0600002B, { 46, 3 } },
	{ 0x0600002C, { 49, 2 } },
	{ 0x0600002D, { 51, 2 } },
	{ 0x0600002E, { 53, 2 } },
	{ 0x0600002F, { 55, 4 } },
	{ 0x06000030, { 59, 2 } },
	{ 0x06000031, { 61, 2 } },
	{ 0x06000032, { 63, 2 } },
	{ 0x06000033, { 65, 2 } },
	{ 0x06000034, { 67, 2 } },
	{ 0x06000035, { 69, 2 } },
	{ 0x06000036, { 71, 2 } },
	{ 0x06000037, { 73, 2 } },
	{ 0x06000038, { 75, 2 } },
	{ 0x06000039, { 77, 2 } },
	{ 0x0600003A, { 79, 2 } },
	{ 0x0600003B, { 81, 2 } },
	{ 0x0600003C, { 83, 2 } },
	{ 0x0600003D, { 85, 2 } },
	{ 0x0600003E, { 87, 2 } },
	{ 0x0600003F, { 89, 2 } },
	{ 0x06000040, { 91, 2 } },
	{ 0x06000041, { 93, 2 } },
	{ 0x06000042, { 95, 2 } },
	{ 0x06000043, { 97, 2 } },
	{ 0x06000044, { 99, 2 } },
	{ 0x06000045, { 101, 2 } },
	{ 0x06000046, { 103, 3 } },
	{ 0x06000047, { 106, 1 } },
	{ 0x06000048, { 107, 2 } },
	{ 0x06000049, { 109, 5 } },
	{ 0x0600004A, { 114, 6 } },
	{ 0x0600004B, { 120, 6 } },
	{ 0x0600012B, { 333, 1 } },
	{ 0x0600012C, { 334, 1 } },
	{ 0x0600012D, { 335, 2 } },
	{ 0x0600012E, { 337, 1 } },
	{ 0x0600012F, { 338, 1 } },
	{ 0x06000130, { 339, 3 } },
	{ 0x06000197, { 342, 4 } },
	{ 0x0600019C, { 346, 11 } },
	{ 0x0600019D, { 357, 2 } },
	{ 0x060001AA, { 359, 2 } },
	{ 0x060001B4, { 361, 2 } },
	{ 0x060001B5, { 363, 1 } },
	{ 0x060001B6, { 364, 1 } },
	{ 0x060001B7, { 365, 2 } },
	{ 0x060001B8, { 367, 1 } },
	{ 0x060001B9, { 368, 1 } },
	{ 0x060001BA, { 369, 1 } },
	{ 0x06000207, { 384, 1 } },
	{ 0x06000208, { 385, 1 } },
	{ 0x06000209, { 386, 1 } },
	{ 0x06000219, { 395, 6 } },
	{ 0x0600021A, { 401, 6 } },
	{ 0x0600021B, { 407, 7 } },
	{ 0x0600021C, { 414, 3 } },
	{ 0x0600021D, { 417, 5 } },
	{ 0x0600021E, { 422, 3 } },
	{ 0x0600021F, { 425, 3 } },
	{ 0x06000220, { 428, 5 } },
	{ 0x06000221, { 433, 3 } },
	{ 0x06000269, { 502, 9 } },
	{ 0x0600026B, { 511, 5 } },
	{ 0x0600026D, { 516, 5 } },
	{ 0x0600026E, { 521, 5 } },
	{ 0x06000270, { 526, 5 } },
	{ 0x06000271, { 531, 5 } },
	{ 0x06000275, { 536, 5 } },
	{ 0x06000276, { 541, 5 } },
	{ 0x06000277, { 546, 5 } },
	{ 0x06000278, { 551, 5 } },
	{ 0x06000279, { 556, 2 } },
	{ 0x0600027B, { 558, 6 } },
	{ 0x0600027D, { 564, 7 } },
	{ 0x0600027E, { 571, 6 } },
	{ 0x0600027F, { 577, 7 } },
	{ 0x06000280, { 584, 2 } },
	{ 0x06000281, { 586, 2 } },
	{ 0x0600033F, { 641, 4 } },
	{ 0x06000340, { 645, 5 } },
	{ 0x06000341, { 650, 2 } },
	{ 0x06000342, { 652, 3 } },
	{ 0x06000368, { 655, 1 } },
	{ 0x06000369, { 656, 1 } },
	{ 0x0600036A, { 657, 1 } },
	{ 0x0600036B, { 658, 1 } },
	{ 0x0600036C, { 659, 1 } },
	{ 0x0600036D, { 660, 1 } },
	{ 0x0600036E, { 661, 1 } },
	{ 0x0600036F, { 662, 1 } },
	{ 0x06000370, { 663, 1 } },
	{ 0x06000371, { 664, 1 } },
	{ 0x060003BF, { 669, 1 } },
	{ 0x060003C0, { 670, 2 } },
	{ 0x060003C1, { 672, 1 } },
	{ 0x060003C2, { 673, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[674] = 
{
	{ (Il2CppRGCTXDataType)2, 34738 },
	{ (Il2CppRGCTXDataType)3, 25313 },
	{ (Il2CppRGCTXDataType)3, 25314 },
	{ (Il2CppRGCTXDataType)2, 26824 },
	{ (Il2CppRGCTXDataType)3, 25315 },
	{ (Il2CppRGCTXDataType)2, 34739 },
	{ (Il2CppRGCTXDataType)3, 25316 },
	{ (Il2CppRGCTXDataType)2, 34740 },
	{ (Il2CppRGCTXDataType)3, 25317 },
	{ (Il2CppRGCTXDataType)3, 25318 },
	{ (Il2CppRGCTXDataType)2, 26826 },
	{ (Il2CppRGCTXDataType)3, 25319 },
	{ (Il2CppRGCTXDataType)3, 25320 },
	{ (Il2CppRGCTXDataType)3, 25321 },
	{ (Il2CppRGCTXDataType)3, 25322 },
	{ (Il2CppRGCTXDataType)3, 25323 },
	{ (Il2CppRGCTXDataType)2, 26846 },
	{ (Il2CppRGCTXDataType)3, 25324 },
	{ (Il2CppRGCTXDataType)2, 26850 },
	{ (Il2CppRGCTXDataType)3, 25325 },
	{ (Il2CppRGCTXDataType)2, 26854 },
	{ (Il2CppRGCTXDataType)3, 25326 },
	{ (Il2CppRGCTXDataType)2, 26857 },
	{ (Il2CppRGCTXDataType)3, 25327 },
	{ (Il2CppRGCTXDataType)3, 25328 },
	{ (Il2CppRGCTXDataType)3, 25329 },
	{ (Il2CppRGCTXDataType)3, 25330 },
	{ (Il2CppRGCTXDataType)3, 25331 },
	{ (Il2CppRGCTXDataType)3, 25332 },
	{ (Il2CppRGCTXDataType)3, 25333 },
	{ (Il2CppRGCTXDataType)3, 25334 },
	{ (Il2CppRGCTXDataType)3, 25335 },
	{ (Il2CppRGCTXDataType)3, 25336 },
	{ (Il2CppRGCTXDataType)3, 25337 },
	{ (Il2CppRGCTXDataType)3, 25338 },
	{ (Il2CppRGCTXDataType)3, 25339 },
	{ (Il2CppRGCTXDataType)3, 25340 },
	{ (Il2CppRGCTXDataType)3, 25341 },
	{ (Il2CppRGCTXDataType)3, 25342 },
	{ (Il2CppRGCTXDataType)3, 25343 },
	{ (Il2CppRGCTXDataType)3, 25344 },
	{ (Il2CppRGCTXDataType)2, 34741 },
	{ (Il2CppRGCTXDataType)3, 25345 },
	{ (Il2CppRGCTXDataType)2, 26894 },
	{ (Il2CppRGCTXDataType)2, 34742 },
	{ (Il2CppRGCTXDataType)3, 25346 },
	{ (Il2CppRGCTXDataType)2, 26897 },
	{ (Il2CppRGCTXDataType)2, 34743 },
	{ (Il2CppRGCTXDataType)3, 25347 },
	{ (Il2CppRGCTXDataType)2, 34744 },
	{ (Il2CppRGCTXDataType)3, 25348 },
	{ (Il2CppRGCTXDataType)2, 26905 },
	{ (Il2CppRGCTXDataType)3, 25349 },
	{ (Il2CppRGCTXDataType)2, 26909 },
	{ (Il2CppRGCTXDataType)3, 25350 },
	{ (Il2CppRGCTXDataType)2, 34745 },
	{ (Il2CppRGCTXDataType)3, 25351 },
	{ (Il2CppRGCTXDataType)2, 26912 },
	{ (Il2CppRGCTXDataType)3, 25352 },
	{ (Il2CppRGCTXDataType)2, 34746 },
	{ (Il2CppRGCTXDataType)3, 25353 },
	{ (Il2CppRGCTXDataType)2, 34747 },
	{ (Il2CppRGCTXDataType)3, 25354 },
	{ (Il2CppRGCTXDataType)2, 34748 },
	{ (Il2CppRGCTXDataType)3, 25355 },
	{ (Il2CppRGCTXDataType)2, 34749 },
	{ (Il2CppRGCTXDataType)3, 25356 },
	{ (Il2CppRGCTXDataType)2, 34750 },
	{ (Il2CppRGCTXDataType)3, 25357 },
	{ (Il2CppRGCTXDataType)2, 34751 },
	{ (Il2CppRGCTXDataType)3, 25358 },
	{ (Il2CppRGCTXDataType)2, 34752 },
	{ (Il2CppRGCTXDataType)3, 25359 },
	{ (Il2CppRGCTXDataType)2, 34753 },
	{ (Il2CppRGCTXDataType)3, 25360 },
	{ (Il2CppRGCTXDataType)2, 34754 },
	{ (Il2CppRGCTXDataType)3, 25361 },
	{ (Il2CppRGCTXDataType)2, 34755 },
	{ (Il2CppRGCTXDataType)3, 25362 },
	{ (Il2CppRGCTXDataType)2, 34756 },
	{ (Il2CppRGCTXDataType)3, 25363 },
	{ (Il2CppRGCTXDataType)2, 34757 },
	{ (Il2CppRGCTXDataType)3, 25364 },
	{ (Il2CppRGCTXDataType)2, 34758 },
	{ (Il2CppRGCTXDataType)3, 25365 },
	{ (Il2CppRGCTXDataType)2, 34759 },
	{ (Il2CppRGCTXDataType)3, 25366 },
	{ (Il2CppRGCTXDataType)2, 34760 },
	{ (Il2CppRGCTXDataType)3, 25367 },
	{ (Il2CppRGCTXDataType)2, 34761 },
	{ (Il2CppRGCTXDataType)3, 25368 },
	{ (Il2CppRGCTXDataType)2, 34762 },
	{ (Il2CppRGCTXDataType)3, 25369 },
	{ (Il2CppRGCTXDataType)2, 34763 },
	{ (Il2CppRGCTXDataType)3, 25370 },
	{ (Il2CppRGCTXDataType)2, 34764 },
	{ (Il2CppRGCTXDataType)3, 25371 },
	{ (Il2CppRGCTXDataType)2, 34765 },
	{ (Il2CppRGCTXDataType)3, 25372 },
	{ (Il2CppRGCTXDataType)2, 34766 },
	{ (Il2CppRGCTXDataType)3, 25373 },
	{ (Il2CppRGCTXDataType)2, 34767 },
	{ (Il2CppRGCTXDataType)3, 25374 },
	{ (Il2CppRGCTXDataType)2, 26969 },
	{ (Il2CppRGCTXDataType)2, 34768 },
	{ (Il2CppRGCTXDataType)3, 25375 },
	{ (Il2CppRGCTXDataType)2, 34769 },
	{ (Il2CppRGCTXDataType)2, 34770 },
	{ (Il2CppRGCTXDataType)2, 26972 },
	{ (Il2CppRGCTXDataType)2, 34771 },
	{ (Il2CppRGCTXDataType)3, 25376 },
	{ (Il2CppRGCTXDataType)2, 26976 },
	{ (Il2CppRGCTXDataType)2, 34772 },
	{ (Il2CppRGCTXDataType)2, 34773 },
	{ (Il2CppRGCTXDataType)2, 34774 },
	{ (Il2CppRGCTXDataType)3, 25377 },
	{ (Il2CppRGCTXDataType)3, 25378 },
	{ (Il2CppRGCTXDataType)3, 25379 },
	{ (Il2CppRGCTXDataType)2, 26977 },
	{ (Il2CppRGCTXDataType)2, 34775 },
	{ (Il2CppRGCTXDataType)2, 34776 },
	{ (Il2CppRGCTXDataType)3, 25380 },
	{ (Il2CppRGCTXDataType)3, 25381 },
	{ (Il2CppRGCTXDataType)3, 25382 },
	{ (Il2CppRGCTXDataType)2, 26980 },
	{ (Il2CppRGCTXDataType)2, 34777 },
	{ (Il2CppRGCTXDataType)3, 25383 },
	{ (Il2CppRGCTXDataType)2, 26992 },
	{ (Il2CppRGCTXDataType)2, 26985 },
	{ (Il2CppRGCTXDataType)3, 25384 },
	{ (Il2CppRGCTXDataType)3, 25385 },
	{ (Il2CppRGCTXDataType)2, 26984 },
	{ (Il2CppRGCTXDataType)2, 34778 },
	{ (Il2CppRGCTXDataType)3, 25386 },
	{ (Il2CppRGCTXDataType)3, 25387 },
	{ (Il2CppRGCTXDataType)3, 25388 },
	{ (Il2CppRGCTXDataType)3, 25389 },
	{ (Il2CppRGCTXDataType)3, 25390 },
	{ (Il2CppRGCTXDataType)2, 26998 },
	{ (Il2CppRGCTXDataType)2, 34779 },
	{ (Il2CppRGCTXDataType)3, 25391 },
	{ (Il2CppRGCTXDataType)3, 25392 },
	{ (Il2CppRGCTXDataType)3, 25393 },
	{ (Il2CppRGCTXDataType)3, 25394 },
	{ (Il2CppRGCTXDataType)2, 27015 },
	{ (Il2CppRGCTXDataType)2, 27008 },
	{ (Il2CppRGCTXDataType)3, 25395 },
	{ (Il2CppRGCTXDataType)2, 27007 },
	{ (Il2CppRGCTXDataType)2, 34780 },
	{ (Il2CppRGCTXDataType)3, 25396 },
	{ (Il2CppRGCTXDataType)3, 25397 },
	{ (Il2CppRGCTXDataType)3, 25398 },
	{ (Il2CppRGCTXDataType)2, 27026 },
	{ (Il2CppRGCTXDataType)2, 27020 },
	{ (Il2CppRGCTXDataType)3, 25399 },
	{ (Il2CppRGCTXDataType)2, 27019 },
	{ (Il2CppRGCTXDataType)2, 34781 },
	{ (Il2CppRGCTXDataType)3, 25400 },
	{ (Il2CppRGCTXDataType)3, 25401 },
	{ (Il2CppRGCTXDataType)3, 25402 },
	{ (Il2CppRGCTXDataType)3, 25403 },
	{ (Il2CppRGCTXDataType)2, 27036 },
	{ (Il2CppRGCTXDataType)2, 27031 },
	{ (Il2CppRGCTXDataType)3, 25404 },
	{ (Il2CppRGCTXDataType)2, 27030 },
	{ (Il2CppRGCTXDataType)2, 34782 },
	{ (Il2CppRGCTXDataType)3, 25405 },
	{ (Il2CppRGCTXDataType)3, 25406 },
	{ (Il2CppRGCTXDataType)3, 25407 },
	{ (Il2CppRGCTXDataType)3, 25408 },
	{ (Il2CppRGCTXDataType)2, 27048 },
	{ (Il2CppRGCTXDataType)2, 27041 },
	{ (Il2CppRGCTXDataType)3, 25409 },
	{ (Il2CppRGCTXDataType)2, 27040 },
	{ (Il2CppRGCTXDataType)2, 34783 },
	{ (Il2CppRGCTXDataType)3, 25410 },
	{ (Il2CppRGCTXDataType)3, 25411 },
	{ (Il2CppRGCTXDataType)3, 25412 },
	{ (Il2CppRGCTXDataType)2, 27059 },
	{ (Il2CppRGCTXDataType)2, 27053 },
	{ (Il2CppRGCTXDataType)3, 25413 },
	{ (Il2CppRGCTXDataType)2, 27052 },
	{ (Il2CppRGCTXDataType)2, 34784 },
	{ (Il2CppRGCTXDataType)3, 25414 },
	{ (Il2CppRGCTXDataType)3, 25415 },
	{ (Il2CppRGCTXDataType)3, 25416 },
	{ (Il2CppRGCTXDataType)3, 25417 },
	{ (Il2CppRGCTXDataType)2, 27069 },
	{ (Il2CppRGCTXDataType)2, 27064 },
	{ (Il2CppRGCTXDataType)3, 25418 },
	{ (Il2CppRGCTXDataType)2, 27063 },
	{ (Il2CppRGCTXDataType)2, 34785 },
	{ (Il2CppRGCTXDataType)3, 25419 },
	{ (Il2CppRGCTXDataType)3, 25420 },
	{ (Il2CppRGCTXDataType)3, 25421 },
	{ (Il2CppRGCTXDataType)3, 25422 },
	{ (Il2CppRGCTXDataType)2, 27083 },
	{ (Il2CppRGCTXDataType)2, 27074 },
	{ (Il2CppRGCTXDataType)3, 25423 },
	{ (Il2CppRGCTXDataType)2, 27073 },
	{ (Il2CppRGCTXDataType)2, 34786 },
	{ (Il2CppRGCTXDataType)3, 25424 },
	{ (Il2CppRGCTXDataType)3, 25425 },
	{ (Il2CppRGCTXDataType)3, 25426 },
	{ (Il2CppRGCTXDataType)2, 27094 },
	{ (Il2CppRGCTXDataType)2, 27088 },
	{ (Il2CppRGCTXDataType)3, 25427 },
	{ (Il2CppRGCTXDataType)2, 27087 },
	{ (Il2CppRGCTXDataType)2, 34787 },
	{ (Il2CppRGCTXDataType)3, 25428 },
	{ (Il2CppRGCTXDataType)3, 25429 },
	{ (Il2CppRGCTXDataType)3, 25430 },
	{ (Il2CppRGCTXDataType)3, 25431 },
	{ (Il2CppRGCTXDataType)2, 27104 },
	{ (Il2CppRGCTXDataType)2, 27099 },
	{ (Il2CppRGCTXDataType)3, 25432 },
	{ (Il2CppRGCTXDataType)2, 27098 },
	{ (Il2CppRGCTXDataType)2, 34788 },
	{ (Il2CppRGCTXDataType)3, 25433 },
	{ (Il2CppRGCTXDataType)3, 25434 },
	{ (Il2CppRGCTXDataType)3, 25435 },
	{ (Il2CppRGCTXDataType)3, 25436 },
	{ (Il2CppRGCTXDataType)3, 25437 },
	{ (Il2CppRGCTXDataType)2, 27112 },
	{ (Il2CppRGCTXDataType)2, 27109 },
	{ (Il2CppRGCTXDataType)3, 25438 },
	{ (Il2CppRGCTXDataType)2, 27108 },
	{ (Il2CppRGCTXDataType)2, 34789 },
	{ (Il2CppRGCTXDataType)3, 25439 },
	{ (Il2CppRGCTXDataType)3, 25440 },
	{ (Il2CppRGCTXDataType)3, 25441 },
	{ (Il2CppRGCTXDataType)3, 25442 },
	{ (Il2CppRGCTXDataType)2, 27126 },
	{ (Il2CppRGCTXDataType)2, 27123 },
	{ (Il2CppRGCTXDataType)3, 25443 },
	{ (Il2CppRGCTXDataType)2, 27122 },
	{ (Il2CppRGCTXDataType)2, 34790 },
	{ (Il2CppRGCTXDataType)3, 25444 },
	{ (Il2CppRGCTXDataType)3, 25445 },
	{ (Il2CppRGCTXDataType)3, 25446 },
	{ (Il2CppRGCTXDataType)3, 25447 },
	{ (Il2CppRGCTXDataType)3, 25448 },
	{ (Il2CppRGCTXDataType)2, 27139 },
	{ (Il2CppRGCTXDataType)2, 27136 },
	{ (Il2CppRGCTXDataType)3, 25449 },
	{ (Il2CppRGCTXDataType)2, 27135 },
	{ (Il2CppRGCTXDataType)2, 34791 },
	{ (Il2CppRGCTXDataType)3, 25450 },
	{ (Il2CppRGCTXDataType)3, 25451 },
	{ (Il2CppRGCTXDataType)3, 25452 },
	{ (Il2CppRGCTXDataType)2, 27155 },
	{ (Il2CppRGCTXDataType)2, 27148 },
	{ (Il2CppRGCTXDataType)3, 25453 },
	{ (Il2CppRGCTXDataType)3, 25454 },
	{ (Il2CppRGCTXDataType)2, 27147 },
	{ (Il2CppRGCTXDataType)2, 34792 },
	{ (Il2CppRGCTXDataType)3, 25455 },
	{ (Il2CppRGCTXDataType)3, 25456 },
	{ (Il2CppRGCTXDataType)3, 25457 },
	{ (Il2CppRGCTXDataType)2, 27166 },
	{ (Il2CppRGCTXDataType)2, 27160 },
	{ (Il2CppRGCTXDataType)3, 25458 },
	{ (Il2CppRGCTXDataType)2, 27159 },
	{ (Il2CppRGCTXDataType)2, 34793 },
	{ (Il2CppRGCTXDataType)3, 25459 },
	{ (Il2CppRGCTXDataType)3, 25460 },
	{ (Il2CppRGCTXDataType)3, 25461 },
	{ (Il2CppRGCTXDataType)3, 25462 },
	{ (Il2CppRGCTXDataType)2, 27176 },
	{ (Il2CppRGCTXDataType)2, 27171 },
	{ (Il2CppRGCTXDataType)3, 25463 },
	{ (Il2CppRGCTXDataType)2, 27170 },
	{ (Il2CppRGCTXDataType)2, 34794 },
	{ (Il2CppRGCTXDataType)3, 25464 },
	{ (Il2CppRGCTXDataType)3, 25465 },
	{ (Il2CppRGCTXDataType)3, 25466 },
	{ (Il2CppRGCTXDataType)2, 27188 },
	{ (Il2CppRGCTXDataType)2, 27181 },
	{ (Il2CppRGCTXDataType)3, 25467 },
	{ (Il2CppRGCTXDataType)3, 25468 },
	{ (Il2CppRGCTXDataType)2, 27180 },
	{ (Il2CppRGCTXDataType)2, 34795 },
	{ (Il2CppRGCTXDataType)3, 25469 },
	{ (Il2CppRGCTXDataType)3, 25470 },
	{ (Il2CppRGCTXDataType)3, 25471 },
	{ (Il2CppRGCTXDataType)2, 27199 },
	{ (Il2CppRGCTXDataType)2, 27193 },
	{ (Il2CppRGCTXDataType)3, 25472 },
	{ (Il2CppRGCTXDataType)2, 27192 },
	{ (Il2CppRGCTXDataType)2, 34796 },
	{ (Il2CppRGCTXDataType)3, 25473 },
	{ (Il2CppRGCTXDataType)3, 25474 },
	{ (Il2CppRGCTXDataType)3, 25475 },
	{ (Il2CppRGCTXDataType)3, 25476 },
	{ (Il2CppRGCTXDataType)2, 27209 },
	{ (Il2CppRGCTXDataType)2, 27204 },
	{ (Il2CppRGCTXDataType)3, 25477 },
	{ (Il2CppRGCTXDataType)2, 27203 },
	{ (Il2CppRGCTXDataType)2, 34797 },
	{ (Il2CppRGCTXDataType)3, 25478 },
	{ (Il2CppRGCTXDataType)3, 25479 },
	{ (Il2CppRGCTXDataType)3, 25480 },
	{ (Il2CppRGCTXDataType)2, 27221 },
	{ (Il2CppRGCTXDataType)2, 27214 },
	{ (Il2CppRGCTXDataType)3, 25481 },
	{ (Il2CppRGCTXDataType)3, 25482 },
	{ (Il2CppRGCTXDataType)2, 27213 },
	{ (Il2CppRGCTXDataType)2, 34798 },
	{ (Il2CppRGCTXDataType)3, 25483 },
	{ (Il2CppRGCTXDataType)3, 25484 },
	{ (Il2CppRGCTXDataType)3, 25485 },
	{ (Il2CppRGCTXDataType)2, 27232 },
	{ (Il2CppRGCTXDataType)2, 27226 },
	{ (Il2CppRGCTXDataType)3, 25486 },
	{ (Il2CppRGCTXDataType)2, 27225 },
	{ (Il2CppRGCTXDataType)2, 34799 },
	{ (Il2CppRGCTXDataType)3, 25487 },
	{ (Il2CppRGCTXDataType)3, 25488 },
	{ (Il2CppRGCTXDataType)3, 25489 },
	{ (Il2CppRGCTXDataType)3, 25490 },
	{ (Il2CppRGCTXDataType)2, 27242 },
	{ (Il2CppRGCTXDataType)2, 27237 },
	{ (Il2CppRGCTXDataType)3, 25491 },
	{ (Il2CppRGCTXDataType)2, 27236 },
	{ (Il2CppRGCTXDataType)2, 34800 },
	{ (Il2CppRGCTXDataType)3, 25492 },
	{ (Il2CppRGCTXDataType)3, 25493 },
	{ (Il2CppRGCTXDataType)3, 25494 },
	{ (Il2CppRGCTXDataType)2, 27246 },
	{ (Il2CppRGCTXDataType)3, 25495 },
	{ (Il2CppRGCTXDataType)2, 34801 },
	{ (Il2CppRGCTXDataType)3, 25496 },
	{ (Il2CppRGCTXDataType)3, 25497 },
	{ (Il2CppRGCTXDataType)1, 34802 },
	{ (Il2CppRGCTXDataType)3, 25498 },
	{ (Il2CppRGCTXDataType)3, 25499 },
	{ (Il2CppRGCTXDataType)3, 25500 },
	{ (Il2CppRGCTXDataType)3, 25501 },
	{ (Il2CppRGCTXDataType)3, 25502 },
	{ (Il2CppRGCTXDataType)1, 27258 },
	{ (Il2CppRGCTXDataType)3, 25503 },
	{ (Il2CppRGCTXDataType)2, 34804 },
	{ (Il2CppRGCTXDataType)1, 27275 },
	{ (Il2CppRGCTXDataType)1, 27276 },
	{ (Il2CppRGCTXDataType)1, 27274 },
	{ (Il2CppRGCTXDataType)2, 27274 },
	{ (Il2CppRGCTXDataType)1, 27278 },
	{ (Il2CppRGCTXDataType)2, 27277 },
	{ (Il2CppRGCTXDataType)1, 34805 },
	{ (Il2CppRGCTXDataType)2, 34806 },
	{ (Il2CppRGCTXDataType)3, 25504 },
	{ (Il2CppRGCTXDataType)3, 25505 },
	{ (Il2CppRGCTXDataType)3, 25506 },
	{ (Il2CppRGCTXDataType)1, 27277 },
	{ (Il2CppRGCTXDataType)3, 25507 },
	{ (Il2CppRGCTXDataType)2, 34807 },
	{ (Il2CppRGCTXDataType)3, 25508 },
	{ (Il2CppRGCTXDataType)1, 27279 },
	{ (Il2CppRGCTXDataType)2, 27279 },
	{ (Il2CppRGCTXDataType)2, 34808 },
	{ (Il2CppRGCTXDataType)3, 25509 },
	{ (Il2CppRGCTXDataType)1, 27283 },
	{ (Il2CppRGCTXDataType)2, 27283 },
	{ (Il2CppRGCTXDataType)3, 25510 },
	{ (Il2CppRGCTXDataType)3, 25511 },
	{ (Il2CppRGCTXDataType)2, 34809 },
	{ (Il2CppRGCTXDataType)3, 25512 },
	{ (Il2CppRGCTXDataType)1, 34810 },
	{ (Il2CppRGCTXDataType)1, 34811 },
	{ (Il2CppRGCTXDataType)1, 34812 },
	{ (Il2CppRGCTXDataType)2, 34813 },
	{ (Il2CppRGCTXDataType)3, 25513 },
	{ (Il2CppRGCTXDataType)2, 34813 },
	{ (Il2CppRGCTXDataType)2, 27306 },
	{ (Il2CppRGCTXDataType)2, 34814 },
	{ (Il2CppRGCTXDataType)2, 27324 },
	{ (Il2CppRGCTXDataType)2, 34815 },
	{ (Il2CppRGCTXDataType)3, 25514 },
	{ (Il2CppRGCTXDataType)3, 25515 },
	{ (Il2CppRGCTXDataType)1, 27334 },
	{ (Il2CppRGCTXDataType)2, 27334 },
	{ (Il2CppRGCTXDataType)2, 34816 },
	{ (Il2CppRGCTXDataType)3, 25516 },
	{ (Il2CppRGCTXDataType)3, 25517 },
	{ (Il2CppRGCTXDataType)2, 27344 },
	{ (Il2CppRGCTXDataType)2, 27346 },
	{ (Il2CppRGCTXDataType)2, 27348 },
	{ (Il2CppRGCTXDataType)2, 27354 },
	{ (Il2CppRGCTXDataType)3, 25518 },
	{ (Il2CppRGCTXDataType)2, 27355 },
	{ (Il2CppRGCTXDataType)2, 27354 },
	{ (Il2CppRGCTXDataType)3, 25519 },
	{ (Il2CppRGCTXDataType)3, 25520 },
	{ (Il2CppRGCTXDataType)3, 25521 },
	{ (Il2CppRGCTXDataType)1, 27355 },
	{ (Il2CppRGCTXDataType)2, 34817 },
	{ (Il2CppRGCTXDataType)3, 25522 },
	{ (Il2CppRGCTXDataType)1, 27359 },
	{ (Il2CppRGCTXDataType)3, 25523 },
	{ (Il2CppRGCTXDataType)2, 27358 },
	{ (Il2CppRGCTXDataType)3, 25524 },
	{ (Il2CppRGCTXDataType)2, 34818 },
	{ (Il2CppRGCTXDataType)3, 25525 },
	{ (Il2CppRGCTXDataType)1, 27361 },
	{ (Il2CppRGCTXDataType)3, 25526 },
	{ (Il2CppRGCTXDataType)2, 27360 },
	{ (Il2CppRGCTXDataType)3, 25527 },
	{ (Il2CppRGCTXDataType)2, 34819 },
	{ (Il2CppRGCTXDataType)3, 25528 },
	{ (Il2CppRGCTXDataType)1, 27364 },
	{ (Il2CppRGCTXDataType)1, 27363 },
	{ (Il2CppRGCTXDataType)3, 25529 },
	{ (Il2CppRGCTXDataType)2, 27362 },
	{ (Il2CppRGCTXDataType)3, 25530 },
	{ (Il2CppRGCTXDataType)2, 34820 },
	{ (Il2CppRGCTXDataType)3, 25531 },
	{ (Il2CppRGCTXDataType)3, 25532 },
	{ (Il2CppRGCTXDataType)2, 34821 },
	{ (Il2CppRGCTXDataType)3, 25533 },
	{ (Il2CppRGCTXDataType)3, 25534 },
	{ (Il2CppRGCTXDataType)2, 27371 },
	{ (Il2CppRGCTXDataType)3, 25535 },
	{ (Il2CppRGCTXDataType)2, 34822 },
	{ (Il2CppRGCTXDataType)3, 25536 },
	{ (Il2CppRGCTXDataType)3, 25537 },
	{ (Il2CppRGCTXDataType)2, 34823 },
	{ (Il2CppRGCTXDataType)3, 25538 },
	{ (Il2CppRGCTXDataType)3, 25539 },
	{ (Il2CppRGCTXDataType)2, 34824 },
	{ (Il2CppRGCTXDataType)3, 25540 },
	{ (Il2CppRGCTXDataType)3, 25541 },
	{ (Il2CppRGCTXDataType)2, 27380 },
	{ (Il2CppRGCTXDataType)3, 25542 },
	{ (Il2CppRGCTXDataType)2, 34825 },
	{ (Il2CppRGCTXDataType)3, 25543 },
	{ (Il2CppRGCTXDataType)3, 25544 },
	{ (Il2CppRGCTXDataType)2, 27393 },
	{ (Il2CppRGCTXDataType)2, 27397 },
	{ (Il2CppRGCTXDataType)2, 27401 },
	{ (Il2CppRGCTXDataType)2, 27402 },
	{ (Il2CppRGCTXDataType)2, 27406 },
	{ (Il2CppRGCTXDataType)3, 25545 },
	{ (Il2CppRGCTXDataType)2, 27407 },
	{ (Il2CppRGCTXDataType)2, 27412 },
	{ (Il2CppRGCTXDataType)3, 25546 },
	{ (Il2CppRGCTXDataType)3, 25547 },
	{ (Il2CppRGCTXDataType)2, 27416 },
	{ (Il2CppRGCTXDataType)2, 27420 },
	{ (Il2CppRGCTXDataType)2, 27421 },
	{ (Il2CppRGCTXDataType)3, 25548 },
	{ (Il2CppRGCTXDataType)2, 27426 },
	{ (Il2CppRGCTXDataType)3, 25549 },
	{ (Il2CppRGCTXDataType)2, 27430 },
	{ (Il2CppRGCTXDataType)3, 25550 },
	{ (Il2CppRGCTXDataType)3, 25551 },
	{ (Il2CppRGCTXDataType)2, 27440 },
	{ (Il2CppRGCTXDataType)3, 25552 },
	{ (Il2CppRGCTXDataType)2, 34826 },
	{ (Il2CppRGCTXDataType)3, 25553 },
	{ (Il2CppRGCTXDataType)3, 23929 },
	{ (Il2CppRGCTXDataType)2, 27442 },
	{ (Il2CppRGCTXDataType)3, 25554 },
	{ (Il2CppRGCTXDataType)3, 23927 },
	{ (Il2CppRGCTXDataType)3, 25555 },
	{ (Il2CppRGCTXDataType)3, 23930 },
	{ (Il2CppRGCTXDataType)3, 25556 },
	{ (Il2CppRGCTXDataType)3, 25557 },
	{ (Il2CppRGCTXDataType)3, 25558 },
	{ (Il2CppRGCTXDataType)3, 25559 },
	{ (Il2CppRGCTXDataType)2, 34827 },
	{ (Il2CppRGCTXDataType)3, 25560 },
	{ (Il2CppRGCTXDataType)3, 25561 },
	{ (Il2CppRGCTXDataType)3, 25562 },
	{ (Il2CppRGCTXDataType)3, 25563 },
	{ (Il2CppRGCTXDataType)3, 25564 },
	{ (Il2CppRGCTXDataType)3, 25565 },
	{ (Il2CppRGCTXDataType)3, 23928 },
	{ (Il2CppRGCTXDataType)2, 34828 },
	{ (Il2CppRGCTXDataType)3, 25566 },
	{ (Il2CppRGCTXDataType)2, 34829 },
	{ (Il2CppRGCTXDataType)3, 25567 },
	{ (Il2CppRGCTXDataType)3, 25568 },
	{ (Il2CppRGCTXDataType)3, 25569 },
	{ (Il2CppRGCTXDataType)3, 25570 },
	{ (Il2CppRGCTXDataType)3, 25571 },
	{ (Il2CppRGCTXDataType)3, 25572 },
	{ (Il2CppRGCTXDataType)3, 25573 },
	{ (Il2CppRGCTXDataType)3, 25574 },
	{ (Il2CppRGCTXDataType)3, 25575 },
	{ (Il2CppRGCTXDataType)3, 25576 },
	{ (Il2CppRGCTXDataType)3, 25577 },
	{ (Il2CppRGCTXDataType)3, 25578 },
	{ (Il2CppRGCTXDataType)3, 25579 },
	{ (Il2CppRGCTXDataType)3, 25580 },
	{ (Il2CppRGCTXDataType)3, 25581 },
	{ (Il2CppRGCTXDataType)2, 34830 },
	{ (Il2CppRGCTXDataType)3, 25582 },
	{ (Il2CppRGCTXDataType)2, 34831 },
	{ (Il2CppRGCTXDataType)3, 25583 },
	{ (Il2CppRGCTXDataType)3, 25584 },
	{ (Il2CppRGCTXDataType)3, 25585 },
	{ (Il2CppRGCTXDataType)3, 25586 },
	{ (Il2CppRGCTXDataType)2, 34832 },
	{ (Il2CppRGCTXDataType)3, 25587 },
	{ (Il2CppRGCTXDataType)2, 34833 },
	{ (Il2CppRGCTXDataType)3, 25588 },
	{ (Il2CppRGCTXDataType)2, 27473 },
	{ (Il2CppRGCTXDataType)3, 25589 },
	{ (Il2CppRGCTXDataType)2, 27472 },
	{ (Il2CppRGCTXDataType)3, 25590 },
	{ (Il2CppRGCTXDataType)3, 25591 },
	{ (Il2CppRGCTXDataType)2, 34834 },
	{ (Il2CppRGCTXDataType)3, 25592 },
	{ (Il2CppRGCTXDataType)3, 25593 },
	{ (Il2CppRGCTXDataType)2, 27474 },
	{ (Il2CppRGCTXDataType)3, 25594 },
	{ (Il2CppRGCTXDataType)2, 34835 },
	{ (Il2CppRGCTXDataType)3, 25595 },
	{ (Il2CppRGCTXDataType)3, 25596 },
	{ (Il2CppRGCTXDataType)2, 27476 },
	{ (Il2CppRGCTXDataType)3, 25597 },
	{ (Il2CppRGCTXDataType)2, 34836 },
	{ (Il2CppRGCTXDataType)3, 25598 },
	{ (Il2CppRGCTXDataType)3, 25599 },
	{ (Il2CppRGCTXDataType)2, 27479 },
	{ (Il2CppRGCTXDataType)3, 25600 },
	{ (Il2CppRGCTXDataType)2, 34837 },
	{ (Il2CppRGCTXDataType)3, 25601 },
	{ (Il2CppRGCTXDataType)3, 25602 },
	{ (Il2CppRGCTXDataType)2, 27481 },
	{ (Il2CppRGCTXDataType)3, 25603 },
	{ (Il2CppRGCTXDataType)2, 34838 },
	{ (Il2CppRGCTXDataType)3, 25604 },
	{ (Il2CppRGCTXDataType)3, 25605 },
	{ (Il2CppRGCTXDataType)2, 27484 },
	{ (Il2CppRGCTXDataType)3, 25606 },
	{ (Il2CppRGCTXDataType)2, 34839 },
	{ (Il2CppRGCTXDataType)3, 25607 },
	{ (Il2CppRGCTXDataType)3, 25608 },
	{ (Il2CppRGCTXDataType)2, 27486 },
	{ (Il2CppRGCTXDataType)3, 25609 },
	{ (Il2CppRGCTXDataType)2, 34840 },
	{ (Il2CppRGCTXDataType)3, 25610 },
	{ (Il2CppRGCTXDataType)3, 25611 },
	{ (Il2CppRGCTXDataType)2, 27488 },
	{ (Il2CppRGCTXDataType)3, 25612 },
	{ (Il2CppRGCTXDataType)2, 34841 },
	{ (Il2CppRGCTXDataType)3, 25613 },
	{ (Il2CppRGCTXDataType)3, 25614 },
	{ (Il2CppRGCTXDataType)2, 27490 },
	{ (Il2CppRGCTXDataType)3, 25615 },
	{ (Il2CppRGCTXDataType)2, 34842 },
	{ (Il2CppRGCTXDataType)3, 25616 },
	{ (Il2CppRGCTXDataType)3, 25617 },
	{ (Il2CppRGCTXDataType)2, 27493 },
	{ (Il2CppRGCTXDataType)3, 25618 },
	{ (Il2CppRGCTXDataType)1, 27496 },
	{ (Il2CppRGCTXDataType)2, 27496 },
	{ (Il2CppRGCTXDataType)2, 34843 },
	{ (Il2CppRGCTXDataType)3, 25619 },
	{ (Il2CppRGCTXDataType)1, 27500 },
	{ (Il2CppRGCTXDataType)3, 25620 },
	{ (Il2CppRGCTXDataType)2, 27499 },
	{ (Il2CppRGCTXDataType)3, 25621 },
	{ (Il2CppRGCTXDataType)2, 34844 },
	{ (Il2CppRGCTXDataType)3, 25622 },
	{ (Il2CppRGCTXDataType)1, 27503 },
	{ (Il2CppRGCTXDataType)1, 27502 },
	{ (Il2CppRGCTXDataType)3, 25623 },
	{ (Il2CppRGCTXDataType)2, 27501 },
	{ (Il2CppRGCTXDataType)3, 25624 },
	{ (Il2CppRGCTXDataType)2, 34845 },
	{ (Il2CppRGCTXDataType)3, 25625 },
	{ (Il2CppRGCTXDataType)1, 27505 },
	{ (Il2CppRGCTXDataType)3, 25626 },
	{ (Il2CppRGCTXDataType)2, 27504 },
	{ (Il2CppRGCTXDataType)3, 25627 },
	{ (Il2CppRGCTXDataType)2, 34846 },
	{ (Il2CppRGCTXDataType)3, 25628 },
	{ (Il2CppRGCTXDataType)1, 27508 },
	{ (Il2CppRGCTXDataType)1, 27507 },
	{ (Il2CppRGCTXDataType)3, 25629 },
	{ (Il2CppRGCTXDataType)2, 27506 },
	{ (Il2CppRGCTXDataType)3, 25630 },
	{ (Il2CppRGCTXDataType)1, 27509 },
	{ (Il2CppRGCTXDataType)2, 27509 },
	{ (Il2CppRGCTXDataType)1, 27511 },
	{ (Il2CppRGCTXDataType)2, 27511 },
	{ (Il2CppRGCTXDataType)2, 27520 },
	{ (Il2CppRGCTXDataType)2, 27526 },
	{ (Il2CppRGCTXDataType)2, 27532 },
	{ (Il2CppRGCTXDataType)2, 27533 },
	{ (Il2CppRGCTXDataType)2, 27536 },
	{ (Il2CppRGCTXDataType)1, 27542 },
	{ (Il2CppRGCTXDataType)2, 27542 },
	{ (Il2CppRGCTXDataType)2, 27543 },
	{ (Il2CppRGCTXDataType)2, 27546 },
	{ (Il2CppRGCTXDataType)2, 27556 },
	{ (Il2CppRGCTXDataType)2, 27559 },
	{ (Il2CppRGCTXDataType)1, 27563 },
	{ (Il2CppRGCTXDataType)2, 27563 },
	{ (Il2CppRGCTXDataType)2, 27564 },
	{ (Il2CppRGCTXDataType)2, 27568 },
	{ (Il2CppRGCTXDataType)2, 27569 },
	{ (Il2CppRGCTXDataType)2, 27572 },
	{ (Il2CppRGCTXDataType)2, 27577 },
	{ (Il2CppRGCTXDataType)2, 27578 },
	{ (Il2CppRGCTXDataType)2, 27581 },
	{ (Il2CppRGCTXDataType)2, 27584 },
	{ (Il2CppRGCTXDataType)2, 27585 },
	{ (Il2CppRGCTXDataType)2, 27593 },
	{ (Il2CppRGCTXDataType)1, 27590 },
	{ (Il2CppRGCTXDataType)2, 27590 },
	{ (Il2CppRGCTXDataType)3, 25631 },
	{ (Il2CppRGCTXDataType)3, 25632 },
	{ (Il2CppRGCTXDataType)3, 25633 },
	{ (Il2CppRGCTXDataType)3, 25634 },
	{ (Il2CppRGCTXDataType)3, 25635 },
	{ (Il2CppRGCTXDataType)3, 25636 },
	{ (Il2CppRGCTXDataType)2, 34847 },
	{ (Il2CppRGCTXDataType)2, 34477 },
	{ (Il2CppRGCTXDataType)3, 25637 },
	{ (Il2CppRGCTXDataType)2, 27635 },
	{ (Il2CppRGCTXDataType)2, 27634 },
	{ (Il2CppRGCTXDataType)2, 34478 },
	{ (Il2CppRGCTXDataType)2, 27636 },
	{ (Il2CppRGCTXDataType)3, 25638 },
	{ (Il2CppRGCTXDataType)2, 27645 },
	{ (Il2CppRGCTXDataType)2, 34479 },
	{ (Il2CppRGCTXDataType)3, 25639 },
	{ (Il2CppRGCTXDataType)3, 25640 },
	{ (Il2CppRGCTXDataType)2, 27646 },
	{ (Il2CppRGCTXDataType)2, 27654 },
	{ (Il2CppRGCTXDataType)3, 25641 },
	{ (Il2CppRGCTXDataType)3, 25642 },
	{ (Il2CppRGCTXDataType)3, 25643 },
	{ (Il2CppRGCTXDataType)2, 34480 },
	{ (Il2CppRGCTXDataType)3, 25644 },
	{ (Il2CppRGCTXDataType)2, 27647 },
	{ (Il2CppRGCTXDataType)3, 25645 },
	{ (Il2CppRGCTXDataType)3, 25646 },
	{ (Il2CppRGCTXDataType)2, 34848 },
	{ (Il2CppRGCTXDataType)2, 34849 },
	{ (Il2CppRGCTXDataType)3, 25647 },
	{ (Il2CppRGCTXDataType)2, 27658 },
	{ (Il2CppRGCTXDataType)2, 34850 },
	{ (Il2CppRGCTXDataType)2, 34851 },
	{ (Il2CppRGCTXDataType)3, 25648 },
	{ (Il2CppRGCTXDataType)3, 25649 },
	{ (Il2CppRGCTXDataType)2, 27661 },
	{ (Il2CppRGCTXDataType)2, 34852 },
	{ (Il2CppRGCTXDataType)2, 27664 },
	{ (Il2CppRGCTXDataType)3, 25650 },
	{ (Il2CppRGCTXDataType)2, 34853 },
	{ (Il2CppRGCTXDataType)2, 27666 },
	{ (Il2CppRGCTXDataType)2, 27674 },
	{ (Il2CppRGCTXDataType)2, 27676 },
	{ (Il2CppRGCTXDataType)2, 27678 },
	{ (Il2CppRGCTXDataType)2, 27680 },
	{ (Il2CppRGCTXDataType)2, 27682 },
	{ (Il2CppRGCTXDataType)2, 27684 },
	{ (Il2CppRGCTXDataType)2, 27686 },
	{ (Il2CppRGCTXDataType)2, 27688 },
	{ (Il2CppRGCTXDataType)2, 27690 },
	{ (Il2CppRGCTXDataType)2, 27692 },
	{ (Il2CppRGCTXDataType)2, 27723 },
	{ (Il2CppRGCTXDataType)2, 34854 },
	{ (Il2CppRGCTXDataType)3, 25651 },
	{ (Il2CppRGCTXDataType)2, 34854 },
	{ (Il2CppRGCTXDataType)3, 25652 },
	{ (Il2CppRGCTXDataType)1, 27731 },
	{ (Il2CppRGCTXDataType)2, 27730 },
	{ (Il2CppRGCTXDataType)3, 25653 },
	{ (Il2CppRGCTXDataType)1, 27735 },
};
extern const Il2CppCodeGenModule g_Sirenix_UtilitiesCodeGenModule;
const Il2CppCodeGenModule g_Sirenix_UtilitiesCodeGenModule = 
{
	"Sirenix.Utilities.dll",
	965,
	s_methodPointers,
	3,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	168,
	s_rgctxIndices,
	674,
	s_rgctxValues,
	NULL,
};
