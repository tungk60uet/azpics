﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Collections.Generic.List`1<PackedPlayModeBuildLogs/RuntimeBuildLog> PackedPlayModeBuildLogs::get_RuntimeBuildLogs()
extern void PackedPlayModeBuildLogs_get_RuntimeBuildLogs_m884C21F959D9C0DFB2BC26BC6AF0E8BB07CB7C9C (void);
// 0x00000002 System.Void PackedPlayModeBuildLogs::set_RuntimeBuildLogs(System.Collections.Generic.List`1<PackedPlayModeBuildLogs/RuntimeBuildLog>)
extern void PackedPlayModeBuildLogs_set_RuntimeBuildLogs_mE1BAC7A249F55DA5CAFC7031DE55BF4BBE3693BA (void);
// 0x00000003 System.Void PackedPlayModeBuildLogs::.ctor()
extern void PackedPlayModeBuildLogs__ctor_mD6D6C46CBF5179339ED9AD995B20A7553DDFC27C (void);
// 0x00000004 System.Void PackedPlayModeBuildLogs/RuntimeBuildLog::.ctor(UnityEngine.LogType,System.String)
extern void RuntimeBuildLog__ctor_m014EB6EEDCCB7F6A3DF6F5176829414750CC3D21 (void);
// 0x00000005 System.Boolean UnityEngine.AssetReferenceUIRestriction::ValidateAsset(UnityEngine.Object)
extern void AssetReferenceUIRestriction_ValidateAsset_mB16598DB91529807CDE4D8797E026B939A7754C1 (void);
// 0x00000006 System.Boolean UnityEngine.AssetReferenceUIRestriction::ValidateAsset(System.String)
extern void AssetReferenceUIRestriction_ValidateAsset_mB29C9B428ABE11D9553EB14B7C3D5A84B09FFD0A (void);
// 0x00000007 System.Void UnityEngine.AssetReferenceUIRestriction::.ctor()
extern void AssetReferenceUIRestriction__ctor_m22E79885F35583A38F38085048423B6B69FAD3FC (void);
// 0x00000008 System.Void UnityEngine.AssetReferenceUILabelRestriction::.ctor(System.String[])
extern void AssetReferenceUILabelRestriction__ctor_m4AF8ACAD8F5638454D69CC43ABC0597E381D9479 (void);
// 0x00000009 System.Boolean UnityEngine.AssetReferenceUILabelRestriction::ValidateAsset(UnityEngine.Object)
extern void AssetReferenceUILabelRestriction_ValidateAsset_m0FC3607B4B8291BAC2F0DBDA50832C28E6862EAF (void);
// 0x0000000A System.Boolean UnityEngine.AssetReferenceUILabelRestriction::ValidateAsset(System.String)
extern void AssetReferenceUILabelRestriction_ValidateAsset_m81585357FB02801962010AE448EBE2C6309300E4 (void);
// 0x0000000B System.String UnityEngine.AssetReferenceUILabelRestriction::ToString()
extern void AssetReferenceUILabelRestriction_ToString_m5DE963979BB3001AFBCC5FF6DF76423C8BC85BA6 (void);
// 0x0000000C System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData>,UnityEngine.AddressableAssets.AddressablesImpl)
extern void InitalizationObjectsOperation_Init_m08F5EC686FDDA4FCF0A4018C505BF435F7F7B2E7 (void);
// 0x0000000D System.String UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::get_DebugName()
extern void InitalizationObjectsOperation_get_DebugName_mD8E85727C2B72304B21EAD512EF63D716E3EAFF8 (void);
// 0x0000000E System.Boolean UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::LogRuntimeWarnings(System.String)
extern void InitalizationObjectsOperation_LogRuntimeWarnings_m4B371DB371E58B2D4C6BF2F915EABF9E896FC622 (void);
// 0x0000000F System.Boolean UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::InvokeWaitForCompletion()
extern void InitalizationObjectsOperation_InvokeWaitForCompletion_m542FC6F672C943D15CC703E2B808D487224C6EA6 (void);
// 0x00000010 System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::Execute()
extern void InitalizationObjectsOperation_Execute_m57D25A512B8B7F18B7E07625179726C4142B00FF (void);
// 0x00000011 System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::.ctor()
extern void InitalizationObjectsOperation__ctor_m4A866392D28009599D93EA895B2F860D8D0CB902 (void);
// 0x00000012 System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::<Execute>b__8_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
extern void InitalizationObjectsOperation_U3CExecuteU3Eb__8_0_mD4874C57111ECE1C34D3F15120002BB4737E5A1F (void);
// 0x00000013 System.Object UnityEngine.AddressableAssets.InvalidKeyException::get_Key()
extern void InvalidKeyException_get_Key_m872576045BF896E1B0887237E64D517B6BDBC9EA (void);
// 0x00000014 System.Void UnityEngine.AddressableAssets.InvalidKeyException::set_Key(System.Object)
extern void InvalidKeyException_set_Key_mC8B77BD80FC0951B0F6C6E0F368CB8E6616AC6A8 (void);
// 0x00000015 System.Type UnityEngine.AddressableAssets.InvalidKeyException::get_Type()
extern void InvalidKeyException_get_Type_mF8B3BEA2A018C67B54571CFB7E8144E3B190EE31 (void);
// 0x00000016 System.Void UnityEngine.AddressableAssets.InvalidKeyException::set_Type(System.Type)
extern void InvalidKeyException_set_Type_m15AFCA253042591FCA6F3A5D669956279B296C26 (void);
// 0x00000017 System.Nullable`1<UnityEngine.AddressableAssets.Addressables/MergeMode> UnityEngine.AddressableAssets.InvalidKeyException::get_MergeMode()
extern void InvalidKeyException_get_MergeMode_m5EFDEE95D9C5F8186A57FD2AC8264237DA546537 (void);
// 0x00000018 System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor(System.Object)
extern void InvalidKeyException__ctor_mE74BFA8EBEAE325FBD3442446EAED3C0F6852CF9 (void);
// 0x00000019 System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor(System.Object,System.Type)
extern void InvalidKeyException__ctor_mE30E4658A05BECF9B47C09D7F103C55BAAC9B59F (void);
// 0x0000001A System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor(System.Object,System.Type,UnityEngine.AddressableAssets.Addressables/MergeMode)
extern void InvalidKeyException__ctor_m925338AE377060E4E8E68A11A091816239F09107 (void);
// 0x0000001B System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor()
extern void InvalidKeyException__ctor_mD1D96140BF1D84CE9D8DF483C1B3811A5C26C7DA (void);
// 0x0000001C System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor(System.String)
extern void InvalidKeyException__ctor_m4AA54BDB846621DA317D561C503710A73E5EA3ED (void);
// 0x0000001D System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor(System.String,System.Exception)
extern void InvalidKeyException__ctor_m50AC2BFADF05F1EAB22807171535CEACE7255D44 (void);
// 0x0000001E System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void InvalidKeyException__ctor_mF5F562E2C0B67F506AE930633DDAD44A335C207A (void);
// 0x0000001F System.String UnityEngine.AddressableAssets.InvalidKeyException::get_Message()
extern void InvalidKeyException_get_Message_m26EE585F4AFE7101AE663B382066F11040674EA8 (void);
// 0x00000020 UnityEngine.AddressableAssets.AddressablesImpl UnityEngine.AddressableAssets.Addressables::get_m_Addressables()
extern void Addressables_get_m_Addressables_m4B5B344E036E113B2320ED1C55C9BC30DFE5DF03 (void);
// 0x00000021 UnityEngine.ResourceManagement.ResourceManager UnityEngine.AddressableAssets.Addressables::get_ResourceManager()
extern void Addressables_get_ResourceManager_m559415A56917C68B7B442D4CDBBC7D7793380CCA (void);
// 0x00000022 UnityEngine.AddressableAssets.AddressablesImpl UnityEngine.AddressableAssets.Addressables::get_Instance()
extern void Addressables_get_Instance_m79F2DCCE86CBD9DC0A7664684D60BFE110E2C4CB (void);
// 0x00000023 UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider UnityEngine.AddressableAssets.Addressables::get_InstanceProvider()
extern void Addressables_get_InstanceProvider_m596D5E60D0D76D85163E04F5E09D91A9CEACF85F (void);
// 0x00000024 System.String UnityEngine.AddressableAssets.Addressables::ResolveInternalId(System.String)
extern void Addressables_ResolveInternalId_m0C314663A669C049F659E9EA4657E58B2561A460 (void);
// 0x00000025 System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String> UnityEngine.AddressableAssets.Addressables::get_InternalIdTransformFunc()
extern void Addressables_get_InternalIdTransformFunc_mA2ACECB5E09FD27620B990C6F6B106EB46F6A019 (void);
// 0x00000026 System.Void UnityEngine.AddressableAssets.Addressables::set_InternalIdTransformFunc(System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String>)
extern void Addressables_set_InternalIdTransformFunc_m2A27854C892E04D7D9105C326366ECB9D8AF9107 (void);
// 0x00000027 System.Action`1<UnityEngine.Networking.UnityWebRequest> UnityEngine.AddressableAssets.Addressables::get_WebRequestOverride()
extern void Addressables_get_WebRequestOverride_m3CC84FE878C51342775C3D6EBF7F03AADA68FA66 (void);
// 0x00000028 System.Void UnityEngine.AddressableAssets.Addressables::set_WebRequestOverride(System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void Addressables_set_WebRequestOverride_m6280DD93646A390B7846C88FAB425F06EEB2468E (void);
// 0x00000029 System.String UnityEngine.AddressableAssets.Addressables::get_StreamingAssetsSubFolder()
extern void Addressables_get_StreamingAssetsSubFolder_m4555804C014F7A71B0B5F9A1A407E4EDC57E5CC1 (void);
// 0x0000002A System.String UnityEngine.AddressableAssets.Addressables::get_BuildPath()
extern void Addressables_get_BuildPath_mAC2055EBA6A001636B90EE893682AF9A1076959E (void);
// 0x0000002B System.String UnityEngine.AddressableAssets.Addressables::get_PlayerBuildDataPath()
extern void Addressables_get_PlayerBuildDataPath_m1629A6A3C90CD71001FFFE8F40D4E43374A2C0D8 (void);
// 0x0000002C System.String UnityEngine.AddressableAssets.Addressables::get_RuntimePath()
extern void Addressables_get_RuntimePath_m5C188FC78940D15F822CAA1DB81BE452E05D83DE (void);
// 0x0000002D System.Collections.Generic.IEnumerable`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::get_ResourceLocators()
extern void Addressables_get_ResourceLocators_m3B761852A07068D214FC2AB5E0B6405F71EC25F5 (void);
// 0x0000002E System.Void UnityEngine.AddressableAssets.Addressables::InternalSafeSerializationLog(System.String,UnityEngine.LogType)
extern void Addressables_InternalSafeSerializationLog_m067E645987683EFACB60B859D94941A7C1DBF262 (void);
// 0x0000002F System.Void UnityEngine.AddressableAssets.Addressables::InternalSafeSerializationLogFormat(System.String,UnityEngine.LogType,System.Object[])
extern void Addressables_InternalSafeSerializationLogFormat_m472AFA27C6C189E1A2DB2017998114C74A68E099 (void);
// 0x00000030 System.Void UnityEngine.AddressableAssets.Addressables::Log(System.String)
extern void Addressables_Log_mB865441616E8AEFD706A46C9973463F8DE782D8D (void);
// 0x00000031 System.Void UnityEngine.AddressableAssets.Addressables::LogFormat(System.String,System.Object[])
extern void Addressables_LogFormat_m5CC2D1088DEC3386AE6B26250DF30F5E9268668F (void);
// 0x00000032 System.Void UnityEngine.AddressableAssets.Addressables::LogWarning(System.String)
extern void Addressables_LogWarning_mA94CA0617D9C4F4CD822A3EDADCD51E04D973386 (void);
// 0x00000033 System.Void UnityEngine.AddressableAssets.Addressables::LogWarningFormat(System.String,System.Object[])
extern void Addressables_LogWarningFormat_mD58F7621494A8CFE3D0A7A52A52672EFEC0B8072 (void);
// 0x00000034 System.Void UnityEngine.AddressableAssets.Addressables::LogError(System.String)
extern void Addressables_LogError_mC84FC8496D63A3CD6EF3D8C1FA43C5B608A8DCEC (void);
// 0x00000035 System.Void UnityEngine.AddressableAssets.Addressables::LogException(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception)
extern void Addressables_LogException_m999610C7A08ABCCB7C501D754D8F4E15308F4C3C (void);
// 0x00000036 System.Void UnityEngine.AddressableAssets.Addressables::LogException(System.Exception)
extern void Addressables_LogException_m4F49254DAF3733DB7C6B65F7EBC24C15042BA440 (void);
// 0x00000037 System.Void UnityEngine.AddressableAssets.Addressables::LogErrorFormat(System.String,System.Object[])
extern void Addressables_LogErrorFormat_mB95D820554CA6DC6C93F8B85439E15491C90DA2E (void);
// 0x00000038 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::Initialize()
extern void Addressables_Initialize_mCC26D4A61B062FB38D22A1D52693E701F76FCE5C (void);
// 0x00000039 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::InitializeAsync()
extern void Addressables_InitializeAsync_mF5F1D985415748D2FBB8B6A7968172231D28EB4B (void);
// 0x0000003A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::InitializeAsync(System.Boolean)
extern void Addressables_InitializeAsync_m4FE6ADB9183F5CF56E2F42F6821097613991DD6D (void);
// 0x0000003B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::LoadContentCatalog(System.String,System.String)
extern void Addressables_LoadContentCatalog_m0580F63F705908A4259AC945EA94F385A2019902 (void);
// 0x0000003C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::LoadContentCatalogAsync(System.String,System.String)
extern void Addressables_LoadContentCatalogAsync_m528222717A9B4AA6B02F06E368BFA5E4AF89A0A4 (void);
// 0x0000003D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::LoadContentCatalogAsync(System.String,System.Boolean,System.String)
extern void Addressables_LoadContentCatalogAsync_m1D1A85B2F2979D985BB0683FE5B22B2A132E4561 (void);
// 0x0000003E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::get_InitializationOperation()
extern void Addressables_get_InitializationOperation_mEFB504954577C24D51998F7B1E09FCE4211BD114 (void);
// 0x0000003F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.Addressables::LoadAsset(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000040 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.Addressables::LoadAsset(System.Object)
// 0x00000041 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.Addressables::LoadAssetAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000042 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.Addressables::LoadAssetAsync(System.Object)
// 0x00000043 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.Addressables::LoadResourceLocations(System.Collections.Generic.IList`1<System.Object>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Type)
extern void Addressables_LoadResourceLocations_m62EC51337AE6967A5632C20740D74CD67717900C (void);
// 0x00000044 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.Addressables::LoadResourceLocationsAsync(System.Collections.Generic.IList`1<System.Object>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Type)
extern void Addressables_LoadResourceLocationsAsync_mDDD2318A9512FE6ED8336D5F9861ACB92CFFAF62 (void);
// 0x00000045 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.Addressables::LoadResourceLocationsAsync(System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Type)
extern void Addressables_LoadResourceLocationsAsync_mCC2DBD081D0ABDFF104E338A8ADEF0CC7FFD5286 (void);
// 0x00000046 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.Addressables::LoadResourceLocations(System.Object,System.Type)
extern void Addressables_LoadResourceLocations_mFB1F0E350828431BCB16AC9191B433A8B4F5486B (void);
// 0x00000047 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.Addressables::LoadResourceLocationsAsync(System.Object,System.Type)
extern void Addressables_LoadResourceLocationsAsync_m38A9B8D08D669F0F3A9D8B0E78DF1CA2CEFD101B (void);
// 0x00000048 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssets(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Action`1<TObject>)
// 0x00000049 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Action`1<TObject>)
// 0x0000004A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Action`1<TObject>,System.Boolean)
// 0x0000004B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssets(System.Collections.Generic.IList`1<System.Object>,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode)
// 0x0000004C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Collections.Generic.IList`1<System.Object>,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode)
// 0x0000004D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Collections.IEnumerable,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode)
// 0x0000004E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Collections.Generic.IList`1<System.Object>,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
// 0x0000004F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Collections.IEnumerable,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
// 0x00000050 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssets(System.Object,System.Action`1<TObject>)
// 0x00000051 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Object,System.Action`1<TObject>)
// 0x00000052 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Object,System.Action`1<TObject>,System.Boolean)
// 0x00000053 System.Void UnityEngine.AddressableAssets.Addressables::Release(TObject)
// 0x00000054 System.Void UnityEngine.AddressableAssets.Addressables::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000055 System.Void UnityEngine.AddressableAssets.Addressables::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void Addressables_Release_m24B87B9557F507472D48CD78314FF151BC1D5818 (void);
// 0x00000056 System.Boolean UnityEngine.AddressableAssets.Addressables::ReleaseInstance(UnityEngine.GameObject)
extern void Addressables_ReleaseInstance_m00D2F96E655DCF72BF1FFCEC907C4B66DF0101F3 (void);
// 0x00000057 System.Boolean UnityEngine.AddressableAssets.Addressables::ReleaseInstance(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void Addressables_ReleaseInstance_mC43B6B4D14372B872AC5D92287A6765A26FF9D05 (void);
// 0x00000058 System.Boolean UnityEngine.AddressableAssets.Addressables::ReleaseInstance(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void Addressables_ReleaseInstance_m7615E57C58D027F1ECE9CFA588E145FCE322B88A (void);
// 0x00000059 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.Addressables::GetDownloadSize(System.Object)
extern void Addressables_GetDownloadSize_m88F151CE488F6390664A9547D036E52FFAAD573E (void);
// 0x0000005A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.Addressables::GetDownloadSizeAsync(System.Object)
extern void Addressables_GetDownloadSizeAsync_m257108E2E9594466FCCE7BC7CC1B9A20320C5E74 (void);
// 0x0000005B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.Addressables::GetDownloadSizeAsync(System.String)
extern void Addressables_GetDownloadSizeAsync_mC68F974C6B6C5DB6CC010A764C5AE4ED3A66899C (void);
// 0x0000005C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.Addressables::GetDownloadSizeAsync(System.Collections.Generic.IList`1<System.Object>)
extern void Addressables_GetDownloadSizeAsync_m84DF8E7205B50A955C9B3210FC80E877989930C2 (void);
// 0x0000005D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.Addressables::GetDownloadSizeAsync(System.Collections.IEnumerable)
extern void Addressables_GetDownloadSizeAsync_m98BED1B2E8D00820433E8D09951C2EE86D67A5BF (void);
// 0x0000005E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.Addressables::DownloadDependencies(System.Object)
extern void Addressables_DownloadDependencies_m55A9DB08B4FE8FB7C532D0548E46C429700640AA (void);
// 0x0000005F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.Addressables::DownloadDependenciesAsync(System.Object,System.Boolean)
extern void Addressables_DownloadDependenciesAsync_m4639D5F7770C7FED3BF78AAF2157EDF4C2A9301D (void);
// 0x00000060 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.Addressables::DownloadDependenciesAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean)
extern void Addressables_DownloadDependenciesAsync_m24F16F248A4658FDD20647484F0EFBABF84DF6C3 (void);
// 0x00000061 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.Addressables::DownloadDependenciesAsync(System.Collections.Generic.IList`1<System.Object>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
extern void Addressables_DownloadDependenciesAsync_m95F80568FAE3417DEE5E85236340E2800F782AA1 (void);
// 0x00000062 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.Addressables::DownloadDependenciesAsync(System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
extern void Addressables_DownloadDependenciesAsync_mA0DBE78983B5ED86848278A2EA20EAAEAF52BCFD (void);
// 0x00000063 System.Void UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Object)
extern void Addressables_ClearDependencyCacheAsync_m7DDCC8A4B8C592639DACD808FFEB2DE62F7E2053 (void);
// 0x00000064 System.Void UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>)
extern void Addressables_ClearDependencyCacheAsync_m417EE6E9C797831063D5BD38AAB07184668A786A (void);
// 0x00000065 System.Void UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Collections.Generic.IList`1<System.Object>)
extern void Addressables_ClearDependencyCacheAsync_m12F999E2CB96B01AC4673BBFC1F86B83BBDA0806 (void);
// 0x00000066 System.Void UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Collections.IEnumerable)
extern void Addressables_ClearDependencyCacheAsync_m3230B6CA41E478DE3DEA4B752020B4FC15ECC379 (void);
// 0x00000067 System.Void UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.String)
extern void Addressables_ClearDependencyCacheAsync_m0AE12FA30D94952D1FDF1434F095E84E2AF70D4C (void);
// 0x00000068 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Object,System.Boolean)
extern void Addressables_ClearDependencyCacheAsync_m5A8CA10E5B957D71E91DBE9B9CC91046DFE906D3 (void);
// 0x00000069 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean)
extern void Addressables_ClearDependencyCacheAsync_m945F674B36985872315C69B1DBBDEAEA693C9829 (void);
// 0x0000006A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Collections.Generic.IList`1<System.Object>,System.Boolean)
extern void Addressables_ClearDependencyCacheAsync_m6AC33D59705CBC441CAF9DD1425ED8DF6D2DE969 (void);
// 0x0000006B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Collections.IEnumerable,System.Boolean)
extern void Addressables_ClearDependencyCacheAsync_m8080D82AF29FCF5F438CFD3286DAD9CB3507ADE2 (void);
// 0x0000006C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.String,System.Boolean)
extern void Addressables_ClearDependencyCacheAsync_m572235A79C4F9397F53E6E90D5CA6C1B7FAB0A29 (void);
// 0x0000006D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::Instantiate(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void Addressables_Instantiate_mFEC691390D685D11D6FD70B97396069A63EEEF9E (void);
// 0x0000006E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::Instantiate(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform,System.Boolean)
extern void Addressables_Instantiate_mD19177F6398F8FD772F0E0D82495C9DC4F6878C8 (void);
// 0x0000006F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::Instantiate(System.Object,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void Addressables_Instantiate_m9431BDFDBADF511E93CD3AAF30B8F061E7A3ECBA (void);
// 0x00000070 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::Instantiate(System.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform,System.Boolean)
extern void Addressables_Instantiate_mB735FE52B1C20FCB081CFFB5BFB49BD0BAD8C145 (void);
// 0x00000071 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::Instantiate(System.Object,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void Addressables_Instantiate_mA28382A948C698495F972E8BA1C6071BBF601162 (void);
// 0x00000072 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::Instantiate(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void Addressables_Instantiate_m11AA22C6417407760CEE88FF89F721D6B485A7A1 (void);
// 0x00000073 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::InstantiateAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void Addressables_InstantiateAsync_m5D775562FB929D6E4CCE3B82059B8E76E58F6699 (void);
// 0x00000074 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::InstantiateAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform,System.Boolean)
extern void Addressables_InstantiateAsync_m762C64C031FAD4608B06C8AD5269EB55FA78EAF8 (void);
// 0x00000075 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::InstantiateAsync(System.Object,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void Addressables_InstantiateAsync_m46AB98E1867839586FD2979E59E189B847CDEE44 (void);
// 0x00000076 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::InstantiateAsync(System.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform,System.Boolean)
extern void Addressables_InstantiateAsync_mBDC3061278EFFA1B4003AD491521EFB9C243EF1D (void);
// 0x00000077 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::InstantiateAsync(System.Object,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void Addressables_InstantiateAsync_m18995125F0C69EE612B7FCFC1D03A3E9A30641A6 (void);
// 0x00000078 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::InstantiateAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void Addressables_InstantiateAsync_m517CAB1C5B9F75E06EC537824265EF71E0568D3F (void);
// 0x00000079 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::LoadScene(System.Object,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void Addressables_LoadScene_m1208181A694E45B027D5D730CF7D26A95591AA3B (void);
// 0x0000007A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::LoadScene(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void Addressables_LoadScene_m1381D4454EE2D363C942A6E387BF2E77A93E7EB1 (void);
// 0x0000007B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::LoadSceneAsync(System.Object,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void Addressables_LoadSceneAsync_m757E6892B8E8220658788E239EA95C4D1559B9E0 (void);
// 0x0000007C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::LoadSceneAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void Addressables_LoadSceneAsync_m5235CFD68FC15F0303D7996084A4E07B953F3257 (void);
// 0x0000007D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadScene(UnityEngine.ResourceManagement.ResourceProviders.SceneInstance,System.Boolean)
extern void Addressables_UnloadScene_m30C4E405312E4A23E7467FB72B029FDB893DCB1F (void);
// 0x0000007E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadScene(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Boolean)
extern void Addressables_UnloadScene_mAFC43027294AE407D2A2066768F4DC73037AFB10 (void);
// 0x0000007F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadScene(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,System.Boolean)
extern void Addressables_UnloadScene_m25BFEF4B6136CC11DC071315E6321E8335B838DA (void);
// 0x00000080 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadSceneAsync(UnityEngine.ResourceManagement.ResourceProviders.SceneInstance,UnityEngine.SceneManagement.UnloadSceneOptions,System.Boolean)
extern void Addressables_UnloadSceneAsync_m8BCE2250FEBFF72FD5C6C388EB0FDA4BFD2670E0 (void);
// 0x00000081 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadSceneAsync(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.SceneManagement.UnloadSceneOptions,System.Boolean)
extern void Addressables_UnloadSceneAsync_m96AC8612343B0F54447B84FAFCBC681BDD065784 (void);
// 0x00000082 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadScene(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,UnityEngine.SceneManagement.UnloadSceneOptions,System.Boolean)
extern void Addressables_UnloadScene_m0C94BB81D40267E611C5B5A95BE3A86FEC89CC2C (void);
// 0x00000083 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadSceneAsync(UnityEngine.ResourceManagement.ResourceProviders.SceneInstance,System.Boolean)
extern void Addressables_UnloadSceneAsync_m142CFA0EDA563CEDD34AB20E7B948F913561644E (void);
// 0x00000084 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadSceneAsync(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Boolean)
extern void Addressables_UnloadSceneAsync_mD943EA608025BAAED685BB2BC090800BEB5B395F (void);
// 0x00000085 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadSceneAsync(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,System.Boolean)
extern void Addressables_UnloadSceneAsync_mF04083B9F9C6EFFEC25AE34321BA178CA0275DF5 (void);
// 0x00000086 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<System.String>> UnityEngine.AddressableAssets.Addressables::CheckForCatalogUpdates(System.Boolean)
extern void Addressables_CheckForCatalogUpdates_m8DE13D868BEBE3346CD3EBC75FE9FE103F9C655B (void);
// 0x00000087 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>> UnityEngine.AddressableAssets.Addressables::UpdateCatalogs(System.Collections.Generic.IEnumerable`1<System.String>,System.Boolean)
extern void Addressables_UpdateCatalogs_mFF17D984EF399502D05E602F98A493B29C96DFB9 (void);
// 0x00000088 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>> UnityEngine.AddressableAssets.Addressables::UpdateCatalogs(System.Boolean,System.Collections.Generic.IEnumerable`1<System.String>,System.Boolean)
extern void Addressables_UpdateCatalogs_m4A1BDCD62E08E543DEEAE08ECFEBCCF00FC52C9B (void);
// 0x00000089 System.Void UnityEngine.AddressableAssets.Addressables::AddResourceLocator(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void Addressables_AddResourceLocator_m950946E23EBA0E9725A098F541542AA663A30E0F (void);
// 0x0000008A System.Void UnityEngine.AddressableAssets.Addressables::RemoveResourceLocator(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator)
extern void Addressables_RemoveResourceLocator_mD13D2102870BAC55AF724265B7CDD04CCE24F292 (void);
// 0x0000008B System.Void UnityEngine.AddressableAssets.Addressables::ClearResourceLocators()
extern void Addressables_ClearResourceLocators_mC4EBDF34123D492BE2386BBAABE4A20A6D5F7DE3 (void);
// 0x0000008C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Addressables::CleanBundleCache(System.Collections.Generic.IEnumerable`1<System.String>)
extern void Addressables_CleanBundleCache_m51A22EB038A6A36D863E032B3D4CC166462F1755 (void);
// 0x0000008D System.Void UnityEngine.AddressableAssets.Addressables::.cctor()
extern void Addressables__cctor_m3BF5C4B4F988F854F894EF7848F813347469544D (void);
// 0x0000008E UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider UnityEngine.AddressableAssets.AddressablesImpl::get_InstanceProvider()
extern void AddressablesImpl_get_InstanceProvider_m60B8111043831A5C192F0E2E793C464437F8AF05 (void);
// 0x0000008F System.Void UnityEngine.AddressableAssets.AddressablesImpl::set_InstanceProvider(UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider)
extern void AddressablesImpl_set_InstanceProvider_m664E2242364A031D635785701A9FE7E6245519C5 (void);
// 0x00000090 UnityEngine.ResourceManagement.ResourceManager UnityEngine.AddressableAssets.AddressablesImpl::get_ResourceManager()
extern void AddressablesImpl_get_ResourceManager_mE53CB81CFC445C16556F59FA7AFDA7166A2500AF (void);
// 0x00000091 System.Int32 UnityEngine.AddressableAssets.AddressablesImpl::get_CatalogRequestsTimeout()
extern void AddressablesImpl_get_CatalogRequestsTimeout_mC00AF41F0746C33CF3AB0897C6473012D8293908 (void);
// 0x00000092 System.Void UnityEngine.AddressableAssets.AddressablesImpl::set_CatalogRequestsTimeout(System.Int32)
extern void AddressablesImpl_set_CatalogRequestsTimeout_m5119D389263F81622B7FB9A77F6D2C9F74FFE088 (void);
// 0x00000093 System.Int32 UnityEngine.AddressableAssets.AddressablesImpl::get_SceneOperationCount()
extern void AddressablesImpl_get_SceneOperationCount_mE35044FF31BE68092BA0BFA10E2209D986514AAE (void);
// 0x00000094 System.Int32 UnityEngine.AddressableAssets.AddressablesImpl::get_TrackedHandleCount()
extern void AddressablesImpl_get_TrackedHandleCount_m5B0F12CA8E02F30B15BF457E808428306BD8D670 (void);
// 0x00000095 System.Void UnityEngine.AddressableAssets.AddressablesImpl::.ctor(UnityEngine.ResourceManagement.Util.IAllocationStrategy)
extern void AddressablesImpl__ctor_m80B4E1BFC74D6BA840B39210609C141C104D1E98 (void);
// 0x00000096 System.Void UnityEngine.AddressableAssets.AddressablesImpl::ReleaseSceneManagerOperation()
extern void AddressablesImpl_ReleaseSceneManagerOperation_mAEFF479E8CDDE9DF9BED2535DCBB7FB59454DB53 (void);
// 0x00000097 System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String> UnityEngine.AddressableAssets.AddressablesImpl::get_InternalIdTransformFunc()
extern void AddressablesImpl_get_InternalIdTransformFunc_m9E994E640C0ED50B0C6B4989BF2BD563D37380F1 (void);
// 0x00000098 System.Void UnityEngine.AddressableAssets.AddressablesImpl::set_InternalIdTransformFunc(System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String>)
extern void AddressablesImpl_set_InternalIdTransformFunc_m5DACF74FCB603E09E6B1DA59FDCC6F1C0905DC53 (void);
// 0x00000099 System.Action`1<UnityEngine.Networking.UnityWebRequest> UnityEngine.AddressableAssets.AddressablesImpl::get_WebRequestOverride()
extern void AddressablesImpl_get_WebRequestOverride_m2C9F84E3554A5186AFA1F2DB203919D07380E473 (void);
// 0x0000009A System.Void UnityEngine.AddressableAssets.AddressablesImpl::set_WebRequestOverride(System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void AddressablesImpl_set_WebRequestOverride_m5A3B461070AB5F7ACA61C86BB6312F78A1F8DA68 (void);
// 0x0000009B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::get_ChainOperation()
extern void AddressablesImpl_get_ChainOperation_mC1FCCFDA2FC6033081B38EDB8BC4FE11298E2FA6 (void);
// 0x0000009C System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::get_ShouldChainRequest()
extern void AddressablesImpl_get_ShouldChainRequest_mDE67D51FF90E874643BE1A9B6A88126865629715 (void);
// 0x0000009D System.Void UnityEngine.AddressableAssets.AddressablesImpl::OnSceneUnloaded(UnityEngine.SceneManagement.Scene)
extern void AddressablesImpl_OnSceneUnloaded_m02FEDEA046674CA6415CB951970D345AF2F1E237 (void);
// 0x0000009E System.String UnityEngine.AddressableAssets.AddressablesImpl::get_StreamingAssetsSubFolder()
extern void AddressablesImpl_get_StreamingAssetsSubFolder_m316C89384356A78100479D43D7D91AD5BFF77BD6 (void);
// 0x0000009F System.String UnityEngine.AddressableAssets.AddressablesImpl::get_BuildPath()
extern void AddressablesImpl_get_BuildPath_m963EE20170AACA747BFCCA7B6F6EB16DB91995E7 (void);
// 0x000000A0 System.String UnityEngine.AddressableAssets.AddressablesImpl::get_PlayerBuildDataPath()
extern void AddressablesImpl_get_PlayerBuildDataPath_mC92B228C9F7464FE640EB4307AF2C9A51F530963 (void);
// 0x000000A1 System.String UnityEngine.AddressableAssets.AddressablesImpl::get_RuntimePath()
extern void AddressablesImpl_get_RuntimePath_mB1937AEB6BB0B183EA22C3FEE5B2D01D6E8BC1AF (void);
// 0x000000A2 System.Void UnityEngine.AddressableAssets.AddressablesImpl::Log(System.String)
extern void AddressablesImpl_Log_m33102DD76D147E5C2352A6E01E1DA4D99321E4D2 (void);
// 0x000000A3 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogFormat(System.String,System.Object[])
extern void AddressablesImpl_LogFormat_m2C8A599541F36A17B75310D1D79300843AC71A82 (void);
// 0x000000A4 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogWarning(System.String)
extern void AddressablesImpl_LogWarning_m792CC621EE43CC7AF59895CC45ACCF0389050DD4 (void);
// 0x000000A5 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogWarningFormat(System.String,System.Object[])
extern void AddressablesImpl_LogWarningFormat_m2F3C58F2B4391E7EC1537C7BEDB50DCB45E8B225 (void);
// 0x000000A6 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogError(System.String)
extern void AddressablesImpl_LogError_m1E07B4F626A839AEB6F79EBBF65F3FCFDF2F83FA (void);
// 0x000000A7 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogException(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception)
extern void AddressablesImpl_LogException_m3B05E2A930AA99799110595AE94EEFE1BECE3570 (void);
// 0x000000A8 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogException(System.Exception)
extern void AddressablesImpl_LogException_mF07BD7A6111E2D212DEB5A13F29929CBD41529B8 (void);
// 0x000000A9 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogErrorFormat(System.String,System.Object[])
extern void AddressablesImpl_LogErrorFormat_m82A3630667EC364AA602B8975626C052EE7D8411 (void);
// 0x000000AA System.String UnityEngine.AddressableAssets.AddressablesImpl::ResolveInternalId(System.String)
extern void AddressablesImpl_ResolveInternalId_m7D64AB8275566AD49DE7F51732826AD59A6F5190 (void);
// 0x000000AB System.Collections.Generic.IEnumerable`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl::get_ResourceLocators()
extern void AddressablesImpl_get_ResourceLocators_m4B169926280BDA97DA9BC2CBEE26B457FBB1AE60 (void);
// 0x000000AC System.Void UnityEngine.AddressableAssets.AddressablesImpl::AddResourceLocator(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AddressablesImpl_AddResourceLocator_mA5AA1B1CB77E05FD412ED10F070670D6E68421CD (void);
// 0x000000AD System.Void UnityEngine.AddressableAssets.AddressablesImpl::RemoveResourceLocator(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator)
extern void AddressablesImpl_RemoveResourceLocator_m924ECB0F4DEF8A7E9C166D4A64B044C7CF5E8AE8 (void);
// 0x000000AE System.Void UnityEngine.AddressableAssets.AddressablesImpl::ClearResourceLocators()
extern void AddressablesImpl_ClearResourceLocators_m161D6CEA0A8A9C628579D480F519ED9AC4B79666 (void);
// 0x000000AF System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::GetResourceLocations(System.Object,System.Type,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>&)
extern void AddressablesImpl_GetResourceLocations_m31926E84C7B5E28B978E32B72461BE085960A567 (void);
// 0x000000B0 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::GetResourceLocations(System.Collections.IEnumerable,System.Type,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>&)
extern void AddressablesImpl_GetResourceLocations_m036AC0E419FCE4116870541F74C3AF14255A485C (void);
// 0x000000B1 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl::InitializeAsync(System.String,System.String,System.Boolean)
extern void AddressablesImpl_InitializeAsync_m7AA5ED5316F1188603B8588F205123E9ABB22086 (void);
// 0x000000B2 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl::InitializeAsync()
extern void AddressablesImpl_InitializeAsync_m4A4F934AF1E0BD4A41E309E55BF6EFBE6CED9E2C (void);
// 0x000000B3 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl::InitializeAsync(System.Boolean)
extern void AddressablesImpl_InitializeAsync_m7462945023764AE7CC3561018AC4D8BE64F35933 (void);
// 0x000000B4 UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase UnityEngine.AddressableAssets.AddressablesImpl::CreateCatalogLocationWithHashDependencies(System.String,System.String)
extern void AddressablesImpl_CreateCatalogLocationWithHashDependencies_m159DCC46932B06430AEA44FC1DD607D632467317 (void);
// 0x000000B5 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl::LoadContentCatalogAsync(System.String,System.Boolean,System.String)
extern void AddressablesImpl_LoadContentCatalogAsync_mB8908538088C868A61A9CA80ED53630B87E82614 (void);
// 0x000000B6 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::TrackHandle(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void AddressablesImpl_TrackHandle_m0EB206D85412C5EC04C3E119DCFA2C357C09B504 (void);
// 0x000000B7 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AddressablesImpl::TrackHandle(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x000000B8 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::TrackHandle(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_TrackHandle_m87874D354073A48745A80A9857A523BF9C0E0560 (void);
// 0x000000B9 System.Void UnityEngine.AddressableAssets.AddressablesImpl::ClearTrackHandles()
extern void AddressablesImpl_ClearTrackHandles_m3AEB8A71FDA919D76E325656BC81B48D3AA805EB (void);
// 0x000000BA UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x000000BB UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object)
// 0x000000BC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetAsync(System.Object)
// 0x000000BD UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.AddressablesImpl::LoadResourceLocationsWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Type)
extern void AddressablesImpl_LoadResourceLocationsWithChain_m84066677120C8EC269580B746F4A6EAC2E7E7BCE (void);
// 0x000000BE UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.AddressablesImpl::LoadResourceLocationsAsync(System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Type)
extern void AddressablesImpl_LoadResourceLocationsAsync_mBD686E2E019933C381BE8FB45630A0DE48623757 (void);
// 0x000000BF UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.AddressablesImpl::LoadResourceLocationsWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object,System.Type)
extern void AddressablesImpl_LoadResourceLocationsWithChain_mDD2DD0C6044E08C3D8E467C5FAB656389D30AED6 (void);
// 0x000000C0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.AddressablesImpl::LoadResourceLocationsAsync(System.Object,System.Type)
extern void AddressablesImpl_LoadResourceLocationsAsync_mF9CED2AF8C17B85062C9452D2F11E30D86D66912 (void);
// 0x000000C1 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetsAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Action`1<TObject>,System.Boolean)
// 0x000000C2 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetsWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Collections.IEnumerable,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
// 0x000000C3 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetsAsync(System.Collections.IEnumerable,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
// 0x000000C4 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetsWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object,System.Action`1<TObject>,System.Boolean)
// 0x000000C5 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetsAsync(System.Object,System.Action`1<TObject>,System.Boolean)
// 0x000000C6 System.Void UnityEngine.AddressableAssets.AddressablesImpl::OnHandleDestroyed(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_OnHandleDestroyed_m140C3E16883E60604BA171725F40DD097AEC750F (void);
// 0x000000C7 System.Void UnityEngine.AddressableAssets.AddressablesImpl::OnSceneHandleCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_OnSceneHandleCompleted_m811671C4E1BED50CD5EA8916DD32E082BEB5C348 (void);
// 0x000000C8 System.Void UnityEngine.AddressableAssets.AddressablesImpl::OnHandleCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_OnHandleCompleted_m5FAA0F89CBA9EAD36D6E47E4E26AA72C4A23CEF6 (void);
// 0x000000C9 System.Void UnityEngine.AddressableAssets.AddressablesImpl::Release(TObject)
// 0x000000CA System.Void UnityEngine.AddressableAssets.AddressablesImpl::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x000000CB System.Void UnityEngine.AddressableAssets.AddressablesImpl::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_Release_m2EEDAAA0AAA113466C4FAD4D08B19C12781F0FFC (void);
// 0x000000CC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.AddressablesImpl::GetDownloadSizeWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object)
extern void AddressablesImpl_GetDownloadSizeWithChain_m93952C3CECC56C843BF22C5CD013BEFF0D2DA2E1 (void);
// 0x000000CD UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.AddressablesImpl::GetDownloadSizeWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Collections.IEnumerable)
extern void AddressablesImpl_GetDownloadSizeWithChain_mFC1DBECFB487E157666E247A18C56310F78BDB0B (void);
// 0x000000CE UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.AddressablesImpl::GetDownloadSizeAsync(System.Object)
extern void AddressablesImpl_GetDownloadSizeAsync_m29410C074A20BDA9F5635A40F75AAEA21E403F76 (void);
// 0x000000CF UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.AddressablesImpl::GetDownloadSizeAsync(System.Collections.IEnumerable)
extern void AddressablesImpl_GetDownloadSizeAsync_mCE9F070B71222699187176FAAE59FBD7F0385D5B (void);
// 0x000000D0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::DownloadDependenciesAsyncWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object,System.Boolean)
extern void AddressablesImpl_DownloadDependenciesAsyncWithChain_mA63F7A9A6BAA8B7F41D661CA835F8753EBA8BF6C (void);
// 0x000000D1 System.Void UnityEngine.AddressableAssets.AddressablesImpl::WrapAsDownloadLocations(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>)
extern void AddressablesImpl_WrapAsDownloadLocations_m1A393450B5BEF65FA990585E73EE88CA601D32A9 (void);
// 0x000000D2 System.Collections.Generic.List`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.AddressableAssets.AddressablesImpl::GatherDependenciesFromLocations(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>)
extern void AddressablesImpl_GatherDependenciesFromLocations_m317B74F49C1C51E73ACC91F23190775F44897613 (void);
// 0x000000D3 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::DownloadDependenciesAsync(System.Object,System.Boolean)
extern void AddressablesImpl_DownloadDependenciesAsync_m79B738A636E75FFDD04F4E75EAA10A2E4A97A588 (void);
// 0x000000D4 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::DownloadDependenciesAsyncWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean)
extern void AddressablesImpl_DownloadDependenciesAsyncWithChain_m167EEB628090B024A123964ED5ED879F6A8FFBAE (void);
// 0x000000D5 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::DownloadDependenciesAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean)
extern void AddressablesImpl_DownloadDependenciesAsync_m715644B2F2A5447E35884A54929C20AFD4EF9D23 (void);
// 0x000000D6 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::DownloadDependenciesAsyncWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
extern void AddressablesImpl_DownloadDependenciesAsyncWithChain_m9EBA73B07E1D1A3EFBC5E14B322FD5C77A35F6C7 (void);
// 0x000000D7 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::DownloadDependenciesAsync(System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
extern void AddressablesImpl_DownloadDependenciesAsync_mC6D9D409B507678CFFF79E92A606705B3A48003E (void);
// 0x000000D8 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::ClearDependencyCacheForKey(System.Object)
extern void AddressablesImpl_ClearDependencyCacheForKey_m97C168E3949496B492600F8729CE5CF9C6D76D8E (void);
// 0x000000D9 System.Void UnityEngine.AddressableAssets.AddressablesImpl::AutoReleaseHandleOnCompletion(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_AutoReleaseHandleOnCompletion_m1FCAE5461E78F1726FDD91E9964F17A9654B09A4 (void);
// 0x000000DA System.Void UnityEngine.AddressableAssets.AddressablesImpl::AutoReleaseHandleOnCompletion(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x000000DB System.Void UnityEngine.AddressableAssets.AddressablesImpl::AutoReleaseHandleOnCompletion(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>,System.Boolean)
// 0x000000DC System.Void UnityEngine.AddressableAssets.AddressablesImpl::AutoReleaseHandleOnTypelessCompletion(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x000000DD UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl::ClearDependencyCacheAsync(System.Object,System.Boolean)
extern void AddressablesImpl_ClearDependencyCacheAsync_mE13532658783AF76B1E059DE0B3E0A5F0AC71280 (void);
// 0x000000DE UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl::ClearDependencyCacheAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean)
extern void AddressablesImpl_ClearDependencyCacheAsync_mA39C55250630E7304373C8485C9D56B344625682 (void);
// 0x000000DF UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl::ClearDependencyCacheAsync(System.Collections.IEnumerable,System.Boolean)
extern void AddressablesImpl_ClearDependencyCacheAsync_mE3F995626E528742FF26A1A558FC635E0D95A561 (void);
// 0x000000E0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void AddressablesImpl_InstantiateAsync_m2D72F6BF23BE69063C7EB57E4164F37D3980C201 (void);
// 0x000000E1 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform,System.Boolean)
extern void AddressablesImpl_InstantiateAsync_m31D33EA66B9E575EBF7FD5CD4AE6EFC843FE2B05 (void);
// 0x000000E2 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateAsync(System.Object,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void AddressablesImpl_InstantiateAsync_m9F0BC0FC5D4E333D0808B00490ABA1EC8732EDDA (void);
// 0x000000E3 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateAsync(System.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform,System.Boolean)
extern void AddressablesImpl_InstantiateAsync_m61EF3B1F338605448BA6B6E87B139818ECA8A68E (void);
// 0x000000E4 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void AddressablesImpl_InstantiateWithChain_mAB9A68E8FBFE6B2BB24C8D96B3CFE58696867EF2 (void);
// 0x000000E5 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateAsync(System.Object,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void AddressablesImpl_InstantiateAsync_m86CFF5195360D94673C41A5B8A0219BB26911901 (void);
// 0x000000E6 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void AddressablesImpl_InstantiateWithChain_mC2AA8122F7653338963A6476D72337A7CAD962B3 (void);
// 0x000000E7 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void AddressablesImpl_InstantiateAsync_mFEC16CD5EB47E0BD6E466DEA277BE4B03813F226 (void);
// 0x000000E8 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::ReleaseInstance(UnityEngine.GameObject)
extern void AddressablesImpl_ReleaseInstance_m3E8C497665F6158232290F49BCEE4882D1CDBE8E (void);
// 0x000000E9 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::LoadSceneWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void AddressablesImpl_LoadSceneWithChain_mECE1597DADF1E4D541D5EF2BBFAC5B76C302BA35 (void);
// 0x000000EA UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::LoadSceneAsync(System.Object,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32,System.Boolean)
extern void AddressablesImpl_LoadSceneAsync_m55B3A837E49F6E996EABF5AA66A3AC4DDEBC00FB (void);
// 0x000000EB UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::LoadSceneAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32,System.Boolean)
extern void AddressablesImpl_LoadSceneAsync_m86B583C45B1EC2DA854BF992BFCF7B3282FE5FB8 (void);
// 0x000000EC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::UnloadSceneAsync(UnityEngine.ResourceManagement.ResourceProviders.SceneInstance,UnityEngine.SceneManagement.UnloadSceneOptions,System.Boolean)
extern void AddressablesImpl_UnloadSceneAsync_m35E7537D3CD26B5604DBB0F06E096F911B18BB58 (void);
// 0x000000ED UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::UnloadSceneAsync(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.SceneManagement.UnloadSceneOptions,System.Boolean)
extern void AddressablesImpl_UnloadSceneAsync_mFEBCC78A8B783850AD1F8064FC0DDCBFECAB839E (void);
// 0x000000EE UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::UnloadSceneAsync(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,UnityEngine.SceneManagement.UnloadSceneOptions,System.Boolean)
extern void AddressablesImpl_UnloadSceneAsync_mB1838A7BE89F5F6A19FA82DA2E25CC675BA8824D (void);
// 0x000000EF UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::CreateUnloadSceneWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.SceneManagement.UnloadSceneOptions,System.Boolean)
extern void AddressablesImpl_CreateUnloadSceneWithChain_m3E90ECF466F96724C83989A3FAA6AE4E4D8921B0 (void);
// 0x000000F0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::CreateUnloadSceneWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,UnityEngine.SceneManagement.UnloadSceneOptions,System.Boolean)
extern void AddressablesImpl_CreateUnloadSceneWithChain_m06EB0B565489B3F2E1460779CE3723BC95749B36 (void);
// 0x000000F1 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::InternalUnloadScene(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,UnityEngine.SceneManagement.UnloadSceneOptions,System.Boolean)
extern void AddressablesImpl_InternalUnloadScene_mAB57CF105B5F4720542CC5BCFC5CA6B58309BBE0 (void);
// 0x000000F2 System.Object UnityEngine.AddressableAssets.AddressablesImpl::EvaluateKey(System.Object)
extern void AddressablesImpl_EvaluateKey_m787E940E38F9A8BEC8F9707990D9F44EE2EE110A (void);
// 0x000000F3 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<System.String>> UnityEngine.AddressableAssets.AddressablesImpl::CheckForCatalogUpdates(System.Boolean)
extern void AddressablesImpl_CheckForCatalogUpdates_mAAE1AD89B3810C0921D6B8AB5F620DF5150DDCE6 (void);
// 0x000000F4 UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo UnityEngine.AddressableAssets.AddressablesImpl::GetLocatorInfo(System.String)
extern void AddressablesImpl_GetLocatorInfo_m102A3AB6F143E664CBC51978C4DFE82D244D6EF8 (void);
// 0x000000F5 System.Collections.Generic.IEnumerable`1<System.String> UnityEngine.AddressableAssets.AddressablesImpl::get_CatalogsWithAvailableUpdates()
extern void AddressablesImpl_get_CatalogsWithAvailableUpdates_mB7A297BA24FDC5C707560CAA65A7A7B2F5719F00 (void);
// 0x000000F6 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>> UnityEngine.AddressableAssets.AddressablesImpl::UpdateCatalogs(System.Collections.Generic.IEnumerable`1<System.String>,System.Boolean,System.Boolean)
extern void AddressablesImpl_UpdateCatalogs_mC6CB947AE102560F2DD295E62BE5CC19E17225E4 (void);
// 0x000000F7 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::Equals(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AddressablesImpl_Equals_m4E02BA3D94370FBA733EFEAA98D0717E355E8367 (void);
// 0x000000F8 System.Int32 UnityEngine.AddressableAssets.AddressablesImpl::GetHashCode(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AddressablesImpl_GetHashCode_m127191DB805C9590D31DE79DDD951F4E2A64C8E1 (void);
// 0x000000F9 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl::CleanBundleCache(System.Collections.Generic.IEnumerable`1<System.String>)
extern void AddressablesImpl_CleanBundleCache_mA56A81E4204F66A356F6C0CF5F9F2542A399FE15 (void);
// 0x000000FA UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl::CleanBundleCache(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
extern void AddressablesImpl_CleanBundleCache_m6F7D31738DCA8C0D9FB2314E0C163A80EAA27F16 (void);
// 0x000000FB System.Void UnityEngine.AddressableAssets.AddressablesImpl::<TrackHandle>b__71_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void AddressablesImpl_U3CTrackHandleU3Eb__71_0_m10D466DB12937F64FCD526F3A31B494E1F507519 (void);
// 0x000000FC System.Void UnityEngine.AddressableAssets.AddressablesImpl::<AutoReleaseHandleOnCompletion>b__108_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_U3CAutoReleaseHandleOnCompletionU3Eb__108_0_m0FFC5C5A2651F96CE41A449C1EF87AD6AB556E96 (void);
// 0x000000FD System.Void UnityEngine.AddressableAssets.AddressablesImpl::<AutoReleaseHandleOnCompletion>b__109_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x000000FE System.Void UnityEngine.AddressableAssets.AddressablesImpl::<AutoReleaseHandleOnTypelessCompletion>b__111_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x000000FF UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::get_Locator()
extern void ResourceLocatorInfo_get_Locator_m730E272473D7C040F4F841E8551408D7CD7972BC (void);
// 0x00000100 System.Void UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::set_Locator(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator)
extern void ResourceLocatorInfo_set_Locator_m3921F8F279FEC8A513EE05B3D6CC1722250BCB15 (void);
// 0x00000101 System.String UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::get_LocalHash()
extern void ResourceLocatorInfo_get_LocalHash_m4E8BAABD41646644B4A33073BC5E3FA3C7D40A57 (void);
// 0x00000102 System.Void UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::set_LocalHash(System.String)
extern void ResourceLocatorInfo_set_LocalHash_mD8D005960BD3FE6BF7A01214D5587C676AA5618D (void);
// 0x00000103 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::get_CatalogLocation()
extern void ResourceLocatorInfo_get_CatalogLocation_mD0A97F02CAD8D1108EE0DD40C801EC3146EE8CE9 (void);
// 0x00000104 System.Void UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::set_CatalogLocation(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceLocatorInfo_set_CatalogLocation_mC562686F535D1D3A76C1DF01D71EC3EB6F0CEF62 (void);
// 0x00000105 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::get_ContentUpdateAvailable()
extern void ResourceLocatorInfo_get_ContentUpdateAvailable_m2E67EC7CDC2B9CCACB53AB161E61BA56F1D69B49 (void);
// 0x00000106 System.Void UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::set_ContentUpdateAvailable(System.Boolean)
extern void ResourceLocatorInfo_set_ContentUpdateAvailable_m4C4F99927F6A00E29435A3DE367B0F631395395A (void);
// 0x00000107 System.Void UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::.ctor(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceLocatorInfo__ctor_m05CFDCA03B55B515E04614A6D32E88A59FD87634 (void);
// 0x00000108 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::get_HashLocation()
extern void ResourceLocatorInfo_get_HashLocation_m06E961C27F73FFF9147F8A2E683BBDED55111B10 (void);
// 0x00000109 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::get_CanUpdateContent()
extern void ResourceLocatorInfo_get_CanUpdateContent_m47C32C7FAB98A3099547912E7003DA5EFAC76154 (void);
// 0x0000010A System.Void UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::UpdateContent(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceLocatorInfo_UpdateContent_mF40872FBC49B6EEA52F973A3998296062B82BAF7 (void);
// 0x0000010B System.String UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeyOp::get_DebugName()
extern void LoadResourceLocationKeyOp_get_DebugName_m3C99D2179177C93712843035A3661A90B0DBCB2B (void);
// 0x0000010C System.Void UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeyOp::Init(UnityEngine.AddressableAssets.AddressablesImpl,System.Type,System.Object)
extern void LoadResourceLocationKeyOp_Init_mB676EA75534F2D833BCACDFA1F2184EC3662301E (void);
// 0x0000010D System.Boolean UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeyOp::InvokeWaitForCompletion()
extern void LoadResourceLocationKeyOp_InvokeWaitForCompletion_m1F125028593FD03B58D417C63FAFE960A7E51C0D (void);
// 0x0000010E System.Void UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeyOp::Execute()
extern void LoadResourceLocationKeyOp_Execute_mAC0599205D47CE023E1743D6BC50CBD459D832BC (void);
// 0x0000010F System.Void UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeyOp::.ctor()
extern void LoadResourceLocationKeyOp__ctor_mC48D04213A7FE2A6ED96385E52B85BFA418A8A6A (void);
// 0x00000110 System.String UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeysOp::get_DebugName()
extern void LoadResourceLocationKeysOp_get_DebugName_mC9FD4FE13F90D7A6AB8490104BE0C28E5CE66ADE (void);
// 0x00000111 System.Void UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeysOp::Init(UnityEngine.AddressableAssets.AddressablesImpl,System.Type,System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode)
extern void LoadResourceLocationKeysOp_Init_m99B3503C4D11C7F86649E59CACCC254F4AB57307 (void);
// 0x00000112 System.Void UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeysOp::Execute()
extern void LoadResourceLocationKeysOp_Execute_m0F79E672AD5C55324CBC66E582C1A571A5875BA7 (void);
// 0x00000113 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeysOp::InvokeWaitForCompletion()
extern void LoadResourceLocationKeysOp_InvokeWaitForCompletion_mA945C41530DB5504AE5438008D4EB902CDC6495D (void);
// 0x00000114 System.Void UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeysOp::.ctor()
extern void LoadResourceLocationKeysOp__ctor_m2A34034DC186107A0F73BB776501342AF0310B96 (void);
// 0x00000115 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c::.cctor()
extern void U3CU3Ec__cctor_m1873F4A126FBA0084FFD04E62AA5528F65BB2C00 (void);
// 0x00000116 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c::.ctor()
extern void U3CU3Ec__ctor_m0793C932810B6198A138DDA2789DB1AF177D1D22 (void);
// 0x00000117 UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator UnityEngine.AddressableAssets.AddressablesImpl/<>c::<get_ResourceLocators>b__60_0(UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo)
extern void U3CU3Ec_U3Cget_ResourceLocatorsU3Eb__60_0_m5F6315A23819453680E3569CE35FDE9FA9BAEED6 (void);
// 0x00000118 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl/<>c::<get_CatalogsWithAvailableUpdates>b__137_0(UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo)
extern void U3CU3Ec_U3Cget_CatalogsWithAvailableUpdatesU3Eb__137_0_m26C99AE0476EB1A49B7CB4EE2BF14605096D41F6 (void);
// 0x00000119 System.String UnityEngine.AddressableAssets.AddressablesImpl/<>c::<get_CatalogsWithAvailableUpdates>b__137_1(UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo)
extern void U3CU3Ec_U3Cget_CatalogsWithAvailableUpdatesU3Eb__137_1_m0189D232CFD9C709CB273969C5C3464C294D9FE0 (void);
// 0x0000011A System.String UnityEngine.AddressableAssets.AddressablesImpl/<>c::<CleanBundleCache>b__141_0(UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo)
extern void U3CU3Ec_U3CCleanBundleCacheU3Eb__141_0_m6516B17BF35843C5E1EE7D625C9FC1D822874AA2 (void);
// 0x0000011B System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_mD4DA14859ADF43E85776257098904A3773A7F630 (void);
// 0x0000011C System.Boolean UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass62_0::<RemoveResourceLocator>b__0(UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo)
extern void U3CU3Ec__DisplayClass62_0_U3CRemoveResourceLocatorU3Eb__0_mB12BA0F449A00DF341FC1484F1E7C267D8C35F2A (void);
// 0x0000011D System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_m20149DBB88A2836309F9F539228433DC77584197 (void);
// 0x0000011E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass70_0::<LoadContentCatalogAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass70_0_U3CLoadContentCatalogAsyncU3Eb__0_mA133F98E86FC76119C993003400F01E00361F0C0 (void);
// 0x0000011F System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass76_0`1::.ctor()
// 0x00000120 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass76_0`1::<LoadAssetWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000121 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass80_0::.ctor()
extern void U3CU3Ec__DisplayClass80_0__ctor_m7CECE01B7B82857448648BCB24004A0EBA09DDAE (void);
// 0x00000122 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass80_0::<LoadResourceLocationsWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass80_0_U3CLoadResourceLocationsWithChainU3Eb__0_m5645E252366EF6B0B837C37AE5BA4DEA6657847A (void);
// 0x00000123 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass82_0::.ctor()
extern void U3CU3Ec__DisplayClass82_0__ctor_m441BDBBE607707EDB10A6675A917C8D2C2A8ACFE (void);
// 0x00000124 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass82_0::<LoadResourceLocationsWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass82_0_U3CLoadResourceLocationsWithChainU3Eb__0_m58B705F74F02F1744B5D2397AB8F129827031B7A (void);
// 0x00000125 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass85_0`1::.ctor()
// 0x00000126 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass85_0`1::<LoadAssetsWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000127 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass87_0`1::.ctor()
// 0x00000128 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass87_0`1::<LoadAssetsWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000129 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass95_0::.ctor()
extern void U3CU3Ec__DisplayClass95_0__ctor_m274728CD9B2F7AF250D8700AC985BAEBC5D1CB97 (void);
// 0x0000012A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass95_0::<GetDownloadSizeWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass95_0_U3CGetDownloadSizeWithChainU3Eb__0_m035EF68D63AA9925D048B84B195EFBC7ED1B40D3 (void);
// 0x0000012B System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass96_0::.ctor()
extern void U3CU3Ec__DisplayClass96_0__ctor_m67BB5E50AC15F6ECDF23BA5B09E2CB6FF043E9FD (void);
// 0x0000012C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass96_0::<GetDownloadSizeWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass96_0_U3CGetDownloadSizeWithChainU3Eb__0_mBCF6460774A428DC547B2715C03DF8804BBEF8E4 (void);
// 0x0000012D System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass99_0::.ctor()
extern void U3CU3Ec__DisplayClass99_0__ctor_m2AFAF95B979113A608A9510FAA4A144960F0AE7A (void);
// 0x0000012E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass99_0::<DownloadDependenciesAsyncWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass99_0_U3CDownloadDependenciesAsyncWithChainU3Eb__0_mB05D2373865AF579ABC6EDBBE1F81EA210EFDD97 (void);
// 0x0000012F System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass103_0::.ctor()
extern void U3CU3Ec__DisplayClass103_0__ctor_m571D237F99230FF084F8F211F6E3599ECF9C3763 (void);
// 0x00000130 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass103_0::<DownloadDependenciesAsyncWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass103_0_U3CDownloadDependenciesAsyncWithChainU3Eb__0_m5CC694573EF8CE94052215A7921847E0DDF1558A (void);
// 0x00000131 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass105_0::.ctor()
extern void U3CU3Ec__DisplayClass105_0__ctor_mC9E819DEB639C856EE15DCF2843E26505F264206 (void);
// 0x00000132 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass105_0::<DownloadDependenciesAsyncWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass105_0_U3CDownloadDependenciesAsyncWithChainU3Eb__0_m7782ACA88E269B60766CFE61C58B19618A2DB166 (void);
// 0x00000133 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass110_0`1::.ctor()
// 0x00000134 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass110_0`1::<AutoReleaseHandleOnCompletion>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000135 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass112_0::.ctor()
extern void U3CU3Ec__DisplayClass112_0__ctor_m16342634513EE3E5B417B5424045434825BC15ED (void);
// 0x00000136 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass112_0::<ClearDependencyCacheAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass112_0_U3CClearDependencyCacheAsyncU3Eb__0_m4460C657CA6608E427473FA77548AC6795CFB0C4 (void);
// 0x00000137 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass113_0::.ctor()
extern void U3CU3Ec__DisplayClass113_0__ctor_m28DA08AABE09774767186AEFDC1F3E90947F4712 (void);
// 0x00000138 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass113_0::<ClearDependencyCacheAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass113_0_U3CClearDependencyCacheAsyncU3Eb__0_mF9B0D4381711F7B5BBDDE6C1902FD9AC3D87975C (void);
// 0x00000139 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass114_0::.ctor()
extern void U3CU3Ec__DisplayClass114_0__ctor_m696EFB1877F3ACE37732E16C357A4A09DF9C66E8 (void);
// 0x0000013A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass114_0::<ClearDependencyCacheAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass114_0_U3CClearDependencyCacheAsyncU3Eb__0_mBBD1442C40B78223EA490D654F3A51A2FF83681B (void);
// 0x0000013B System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass119_0::.ctor()
extern void U3CU3Ec__DisplayClass119_0__ctor_m12831188ACFF28D2CE4435364F7661AD7D632D0A (void);
// 0x0000013C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass119_0::<InstantiateWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass119_0_U3CInstantiateWithChainU3Eb__0_mB92058EEEBD5609B5EE537726EE475E50FB0DE30 (void);
// 0x0000013D System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass121_0::.ctor()
extern void U3CU3Ec__DisplayClass121_0__ctor_m68E05C232B16EF14AD7FB6721CC7127A1F4E5D75 (void);
// 0x0000013E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass121_0::<InstantiateWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass121_0_U3CInstantiateWithChainU3Eb__0_mC898C32386319181928454F36E15717D2C95E51B (void);
// 0x0000013F System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass124_0::.ctor()
extern void U3CU3Ec__DisplayClass124_0__ctor_m89777E267B856F2A4BF17E10E758A67069E1271B (void);
// 0x00000140 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass124_0::<LoadSceneWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass124_0_U3CLoadSceneWithChainU3Eb__0_m966AA1153A14CF7822BB164C4050E71332BB39C8 (void);
// 0x00000141 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass130_0::.ctor()
extern void U3CU3Ec__DisplayClass130_0__ctor_m3FFF77BA900E8025B586A63E8799CA5BB89468C7 (void);
// 0x00000142 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass130_0::<CreateUnloadSceneWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass130_0_U3CCreateUnloadSceneWithChainU3Eb__0_mA13502A156C351CF8264F36090FE1520BA2E4ADD (void);
// 0x00000143 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass131_0::.ctor()
extern void U3CU3Ec__DisplayClass131_0__ctor_m31B13597367815C8A07D77EE8ED8C5A1168C3CF3 (void);
// 0x00000144 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass131_0::<CreateUnloadSceneWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void U3CU3Ec__DisplayClass131_0_U3CCreateUnloadSceneWithChainU3Eb__0_m4BD0640390D16FBE17EE4826E2F6630033347EEE (void);
// 0x00000145 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass138_0::.ctor()
extern void U3CU3Ec__DisplayClass138_0__ctor_m9C050EE8C1974EE3D54B4F7C51D8077A7C3AAB1B (void);
// 0x00000146 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass138_0::<UpdateCatalogs>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<System.String>>)
extern void U3CU3Ec__DisplayClass138_0_U3CUpdateCatalogsU3Eb__0_mCD7158FE5C76D25FF276919403E2039E3A09E276 (void);
// 0x00000147 System.String UnityEngine.AddressableAssets.AssetLabelReference::get_labelString()
extern void AssetLabelReference_get_labelString_m50D0391EBEB48D27931552DCF414D193826C036A (void);
// 0x00000148 System.Void UnityEngine.AddressableAssets.AssetLabelReference::set_labelString(System.String)
extern void AssetLabelReference_set_labelString_m3F45F368E74F1E08A870E03C58D2EC86AEED7D5C (void);
// 0x00000149 System.Object UnityEngine.AddressableAssets.AssetLabelReference::get_RuntimeKey()
extern void AssetLabelReference_get_RuntimeKey_m12853735396918F9D95730708B2DA9AA0CB836E4 (void);
// 0x0000014A System.Boolean UnityEngine.AddressableAssets.AssetLabelReference::RuntimeKeyIsValid()
extern void AssetLabelReference_RuntimeKeyIsValid_m2C39EC7D33AFED52BA6061DEFCE9D58C42D7B4FB (void);
// 0x0000014B System.Int32 UnityEngine.AddressableAssets.AssetLabelReference::GetHashCode()
extern void AssetLabelReference_GetHashCode_m7D4FAAB50F4D3192CDF02EBE0E8CF6FABF32752A (void);
// 0x0000014C System.Void UnityEngine.AddressableAssets.AssetLabelReference::.ctor()
extern void AssetLabelReference__ctor_m4804B7C444E9A02C2E50E22B4EE92E5CC99089F6 (void);
// 0x0000014D System.Void UnityEngine.AddressableAssets.AssetReferenceT`1::.ctor(System.String)
// 0x0000014E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AssetReferenceT`1::LoadAsset()
// 0x0000014F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AssetReferenceT`1::LoadAssetAsync()
// 0x00000150 System.Boolean UnityEngine.AddressableAssets.AssetReferenceT`1::ValidateAsset(UnityEngine.Object)
// 0x00000151 System.Boolean UnityEngine.AddressableAssets.AssetReferenceT`1::ValidateAsset(System.String)
// 0x00000152 System.Void UnityEngine.AddressableAssets.AssetReferenceGameObject::.ctor(System.String)
extern void AssetReferenceGameObject__ctor_mB4776F9B8496516169070EDBCE3BADCCA8450A6D (void);
// 0x00000153 System.Void UnityEngine.AddressableAssets.AssetReferenceTexture::.ctor(System.String)
extern void AssetReferenceTexture__ctor_mFF94A2C32A07209D870FB8DE9EE4C7D37C7D9598 (void);
// 0x00000154 System.Void UnityEngine.AddressableAssets.AssetReferenceTexture2D::.ctor(System.String)
extern void AssetReferenceTexture2D__ctor_m44EC43BF4CDBF0D033D63338D4F2ABA7D7E66725 (void);
// 0x00000155 System.Void UnityEngine.AddressableAssets.AssetReferenceTexture3D::.ctor(System.String)
extern void AssetReferenceTexture3D__ctor_m10345FF060C3FD55A034AD09F01705B270DD6D6F (void);
// 0x00000156 System.Void UnityEngine.AddressableAssets.AssetReferenceSprite::.ctor(System.String)
extern void AssetReferenceSprite__ctor_m5BB0B8B036649204BA33C0A72262868D00C92F67 (void);
// 0x00000157 System.Boolean UnityEngine.AddressableAssets.AssetReferenceSprite::ValidateAsset(System.String)
extern void AssetReferenceSprite_ValidateAsset_mFE52D04BEE4E4B33584EDECE7AF4054F9E313246 (void);
// 0x00000158 System.Void UnityEngine.AddressableAssets.AssetReferenceAtlasedSprite::.ctor(System.String)
extern void AssetReferenceAtlasedSprite__ctor_m8F513BC363CC9E6A8907BB8333AC91495CBAF393 (void);
// 0x00000159 System.Boolean UnityEngine.AddressableAssets.AssetReferenceAtlasedSprite::ValidateAsset(UnityEngine.Object)
extern void AssetReferenceAtlasedSprite_ValidateAsset_mE38720BE580A06C9D813F8B04B490E5C4530A8DA (void);
// 0x0000015A System.Boolean UnityEngine.AddressableAssets.AssetReferenceAtlasedSprite::ValidateAsset(System.String)
extern void AssetReferenceAtlasedSprite_ValidateAsset_mA579F68BF61B2A0DEFC52674D18D3F19DBA3FD18 (void);
// 0x0000015B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AssetReference::get_OperationHandle()
extern void AssetReference_get_OperationHandle_mB4BAAB57C9A519BDDA6E2308BA157FF8929728AF (void);
// 0x0000015C System.Object UnityEngine.AddressableAssets.AssetReference::get_RuntimeKey()
extern void AssetReference_get_RuntimeKey_mFFB92EE1592D1CADC55291597F24C8AC2585555A (void);
// 0x0000015D System.String UnityEngine.AddressableAssets.AssetReference::get_AssetGUID()
extern void AssetReference_get_AssetGUID_mA1783241545A64F4988B844A83EFA6B477C4E723 (void);
// 0x0000015E System.String UnityEngine.AddressableAssets.AssetReference::get_SubObjectName()
extern void AssetReference_get_SubObjectName_mBDCF0C4180464DC53D890212CFB4132DA8888AA1 (void);
// 0x0000015F System.Void UnityEngine.AddressableAssets.AssetReference::set_SubObjectName(System.String)
extern void AssetReference_set_SubObjectName_mD9D7B8E2F9522A9D6571CED8ABA3B580820D7E69 (void);
// 0x00000160 System.Type UnityEngine.AddressableAssets.AssetReference::get_SubOjbectType()
extern void AssetReference_get_SubOjbectType_m39E6BE620D15FD77DE37F9C1B3DD8606C304DAB9 (void);
// 0x00000161 System.Boolean UnityEngine.AddressableAssets.AssetReference::IsValid()
extern void AssetReference_IsValid_m5D83F03F369F0ACBB7355911C660F4A9C350F4F8 (void);
// 0x00000162 System.Boolean UnityEngine.AddressableAssets.AssetReference::get_IsDone()
extern void AssetReference_get_IsDone_m0659579AE490C8B0F57F1A9641C3FFB88336BB9F (void);
// 0x00000163 System.Void UnityEngine.AddressableAssets.AssetReference::.ctor()
extern void AssetReference__ctor_mE38B54E67E5FC625BA69DC50A8DABE0FE3422031 (void);
// 0x00000164 System.Void UnityEngine.AddressableAssets.AssetReference::.ctor(System.String)
extern void AssetReference__ctor_mC3A12AE436F1FD5C2E5379F96D7CD651B35BCD46 (void);
// 0x00000165 UnityEngine.Object UnityEngine.AddressableAssets.AssetReference::get_Asset()
extern void AssetReference_get_Asset_mF316D0153DAB80E3593997C854E96A21B80D092C (void);
// 0x00000166 System.String UnityEngine.AddressableAssets.AssetReference::ToString()
extern void AssetReference_ToString_m86D7865D2BCA234413BE43C7EFF3A0F69C9D6617 (void);
// 0x00000167 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<T> UnityEngine.AddressableAssets.AssetReference::CreateFailedOperation()
// 0x00000168 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AssetReference::LoadAsset()
// 0x00000169 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AssetReference::LoadScene()
extern void AssetReference_LoadScene_m6AFBBDBF59A4BA4DC807CA75047119D9693C1D6F (void);
// 0x0000016A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AssetReference::Instantiate(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void AssetReference_Instantiate_mC320C78B3EB3684BE0CF806FDAF041174F77E11F (void);
// 0x0000016B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AssetReference::Instantiate(UnityEngine.Transform,System.Boolean)
extern void AssetReference_Instantiate_m5B54B1EA245D1216147D849C2F837B413ADF4379 (void);
// 0x0000016C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AssetReference::LoadAssetAsync()
// 0x0000016D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AssetReference::LoadSceneAsync(UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void AssetReference_LoadSceneAsync_m774FE932EC09AA91127BE50A14C26D970B44ABB2 (void);
// 0x0000016E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AssetReference::UnLoadScene()
extern void AssetReference_UnLoadScene_m5D3FC5DD78B5533A4D10EA16436914A105C4C1F8 (void);
// 0x0000016F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AssetReference::InstantiateAsync(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void AssetReference_InstantiateAsync_m639FCE542FB03D1F21D46380FAB6D15E5F2D5AF7 (void);
// 0x00000170 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AssetReference::InstantiateAsync(UnityEngine.Transform,System.Boolean)
extern void AssetReference_InstantiateAsync_m483670275A9C6E31E8DF1D57FB24BE0E3C4BCB26 (void);
// 0x00000171 System.Boolean UnityEngine.AddressableAssets.AssetReference::RuntimeKeyIsValid()
extern void AssetReference_RuntimeKeyIsValid_m6D9D44A2DD0ABE262765A3565651357113226BEF (void);
// 0x00000172 System.Void UnityEngine.AddressableAssets.AssetReference::ReleaseAsset()
extern void AssetReference_ReleaseAsset_m969FC5ED646CBEAC2C830C66DC44E196577C7A46 (void);
// 0x00000173 System.Void UnityEngine.AddressableAssets.AssetReference::ReleaseInstance(UnityEngine.GameObject)
extern void AssetReference_ReleaseInstance_mF4B27017208E6319472B7E6F059B238E547291C1 (void);
// 0x00000174 System.Boolean UnityEngine.AddressableAssets.AssetReference::ValidateAsset(UnityEngine.Object)
extern void AssetReference_ValidateAsset_m35FB5E9DFF3569B2AC42293F88E5B9CA4E384807 (void);
// 0x00000175 System.Boolean UnityEngine.AddressableAssets.AssetReference::ValidateAsset(System.String)
extern void AssetReference_ValidateAsset_m0901C2C1CD6F19A6C9B1AFD4F7479A4B7C51FAC7 (void);
// 0x00000176 System.Object UnityEngine.AddressableAssets.IKeyEvaluator::get_RuntimeKey()
// 0x00000177 System.Boolean UnityEngine.AddressableAssets.IKeyEvaluator::RuntimeKeyIsValid()
// 0x00000178 System.Void UnityEngine.AddressableAssets.CheckCatalogsOperation::.ctor(UnityEngine.AddressableAssets.AddressablesImpl)
extern void CheckCatalogsOperation__ctor_m7FC966B7C226BB5069AEE70FED6DDB16A5C53F2F (void);
// 0x00000179 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<System.String>> UnityEngine.AddressableAssets.CheckCatalogsOperation::Start(System.Collections.Generic.List`1<UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo>)
extern void CheckCatalogsOperation_Start_mA40B6D4597ED04E99745F48865533E54D774A9D2 (void);
// 0x0000017A System.Boolean UnityEngine.AddressableAssets.CheckCatalogsOperation::InvokeWaitForCompletion()
extern void CheckCatalogsOperation_InvokeWaitForCompletion_mF9A9E1FC79FFB4080ED623B498EB0628D9BE5980 (void);
// 0x0000017B System.Void UnityEngine.AddressableAssets.CheckCatalogsOperation::Destroy()
extern void CheckCatalogsOperation_Destroy_mF67296A68840B3A77713636A6FA63D82B7D845AE (void);
// 0x0000017C System.Void UnityEngine.AddressableAssets.CheckCatalogsOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void CheckCatalogsOperation_GetDependencies_m5EDB01084C3BBBD8D9C1716CB9654BBDE8413FC0 (void);
// 0x0000017D System.Collections.Generic.List`1<System.String> UnityEngine.AddressableAssets.CheckCatalogsOperation::ProcessDependentOpResults(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Collections.Generic.List`1<UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo>,System.Collections.Generic.List`1<System.String>,System.String&,System.Boolean&)
extern void CheckCatalogsOperation_ProcessDependentOpResults_m194CD9D40988357CB47BB2C3C43BDFDA89FA360B (void);
// 0x0000017E System.Void UnityEngine.AddressableAssets.CheckCatalogsOperation::Execute()
extern void CheckCatalogsOperation_Execute_m33B0C6478586D57273E4BE93263F398162165358 (void);
// 0x0000017F System.Void UnityEngine.AddressableAssets.CheckCatalogsOperation/<>c::.cctor()
extern void U3CU3Ec__cctor_m81E1395B6796338DB61527B69B0AEE8736B982D2 (void);
// 0x00000180 System.Void UnityEngine.AddressableAssets.CheckCatalogsOperation/<>c::.ctor()
extern void U3CU3Ec__ctor_m6E6B1522F10F8BAC1ECCA1052E3B1AB15A97A841 (void);
// 0x00000181 System.Boolean UnityEngine.AddressableAssets.CheckCatalogsOperation/<>c::<Start>b__5_0(UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider)
extern void U3CU3Ec_U3CStartU3Eb__5_0_m80140E81FA504FD96B3E75ECD80F859C90F5595E (void);
// 0x00000182 System.Void UnityEngine.AddressableAssets.CleanBundleCacheOperation::.ctor(UnityEngine.AddressableAssets.AddressablesImpl)
extern void CleanBundleCacheOperation__ctor_mD74612701243658021F6F1C91692C695C08D4C78 (void);
// 0x00000183 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.CleanBundleCacheOperation::Start(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
extern void CleanBundleCacheOperation_Start_mE70204E1127D73E8D8987D5D3B0FCDAB786410B6 (void);
// 0x00000184 System.Void UnityEngine.AddressableAssets.CleanBundleCacheOperation::CompleteInternal(System.Boolean,System.Boolean,System.String)
extern void CleanBundleCacheOperation_CompleteInternal_m5E03B8B322A22792C96A9A97E471ED7B3DFD3C2B (void);
// 0x00000185 System.Boolean UnityEngine.AddressableAssets.CleanBundleCacheOperation::InvokeWaitForCompletion()
extern void CleanBundleCacheOperation_InvokeWaitForCompletion_m3B822E3B915A0DDDA1CEE158160F4CBA0242FE24 (void);
// 0x00000186 System.Void UnityEngine.AddressableAssets.CleanBundleCacheOperation::Destroy()
extern void CleanBundleCacheOperation_Destroy_m7B5990431E9EF4B587609C5E42FEAA4EFC43EA6D (void);
// 0x00000187 System.Void UnityEngine.AddressableAssets.CleanBundleCacheOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void CleanBundleCacheOperation_GetDependencies_m904C7D5BD95A2DCD6B865980264CB2B84F0FA9DB (void);
// 0x00000188 System.Void UnityEngine.AddressableAssets.CleanBundleCacheOperation::Execute()
extern void CleanBundleCacheOperation_Execute_m8341F081883459C23AB968CD881EB09D8F32C410 (void);
// 0x00000189 System.Void UnityEngine.AddressableAssets.CleanBundleCacheOperation::UnityEngine.ResourceManagement.IUpdateReceiver.Update(System.Single)
extern void CleanBundleCacheOperation_UnityEngine_ResourceManagement_IUpdateReceiver_Update_m8A2EBE27CB8872A104278F25001993E3EFC93CA8 (void);
// 0x0000018A System.Void UnityEngine.AddressableAssets.CleanBundleCacheOperation::RemoveCacheEntries()
extern void CleanBundleCacheOperation_RemoveCacheEntries_mDF863BE4126498457703B1ABEE32076E515CE061 (void);
// 0x0000018B System.Void UnityEngine.AddressableAssets.CleanBundleCacheOperation::DetermineCacheDirsNotInUse(System.Object)
extern void CleanBundleCacheOperation_DetermineCacheDirsNotInUse_m581EF6B004281F5A332A376343B704B24D57BE13 (void);
// 0x0000018C System.Collections.Generic.HashSet`1<System.String> UnityEngine.AddressableAssets.CleanBundleCacheOperation::GetCacheDirsInUse(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void CleanBundleCacheOperation_GetCacheDirsInUse_mED52DCD562381F6D44B51329BC0828C24D2C0AD9 (void);
// 0x0000018D System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation::.ctor(UnityEngine.AddressableAssets.AddressablesImpl)
extern void UpdateCatalogsOperation__ctor_m980CB3CCA2DD2B7EA1C9EEDF2276D93C07AA383A (void);
// 0x0000018E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>> UnityEngine.AddressableAssets.UpdateCatalogsOperation::Start(System.Collections.Generic.IEnumerable`1<System.String>,System.Boolean)
extern void UpdateCatalogsOperation_Start_m6ACA9C832974D47EE94C5C8B4E44E469E092D1DD (void);
// 0x0000018F System.Boolean UnityEngine.AddressableAssets.UpdateCatalogsOperation::InvokeWaitForCompletion()
extern void UpdateCatalogsOperation_InvokeWaitForCompletion_mE1353B05726EAA36074AD56087804D935B953F3D (void);
// 0x00000190 System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation::Destroy()
extern void UpdateCatalogsOperation_Destroy_m0124A4CD4F734E1935B5AB11D7A4CB9AA9C19699 (void);
// 0x00000191 System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void UpdateCatalogsOperation_GetDependencies_mC0A2567771A8CEAEA491380EF208414E4BED2D16 (void);
// 0x00000192 System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation::Execute()
extern void UpdateCatalogsOperation_Execute_mDF70F3ED39ECE6E0F88559689DAFA1CCF3931753 (void);
// 0x00000193 System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation::OnCleanCacheCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean>,System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>)
extern void UpdateCatalogsOperation_OnCleanCacheCompleted_m0985D2E9A899D1D456481940DBA2871DE7E0A20D (void);
// 0x00000194 System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation/<>c::.cctor()
extern void U3CU3Ec__cctor_m79E6CA846BD2DB473F975AE7020D35607A210E3E (void);
// 0x00000195 System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation/<>c::.ctor()
extern void U3CU3Ec__ctor_m396385D48E372D649B0A8F408E6B3747A588CC0C (void);
// 0x00000196 System.Boolean UnityEngine.AddressableAssets.UpdateCatalogsOperation/<>c::<Start>b__6_0(UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider)
extern void U3CU3Ec_U3CStartU3Eb__6_0_m94EEEF78F0686E0458654FC58F688EA31D8E375D (void);
// 0x00000197 System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mB2460F0D47EC265545C51A29FF2EA4CA745D9E88 (void);
// 0x00000198 System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation/<>c__DisplayClass11_0::<OnCleanCacheCompleted>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean>)
extern void U3CU3Ec__DisplayClass11_0_U3COnCleanCacheCompletedU3Eb__0_mB0C710EDF34DB43437A1027545BBCA01BE7203AF (void);
// 0x00000199 System.String UnityEngine.AddressableAssets.DynamicResourceLocator::get_LocatorId()
extern void DynamicResourceLocator_get_LocatorId_m68F756B076B6AA037CDE8B7CE2F77E0F79D7AD6B (void);
// 0x0000019A System.Collections.Generic.IEnumerable`1<System.Object> UnityEngine.AddressableAssets.DynamicResourceLocator::get_Keys()
extern void DynamicResourceLocator_get_Keys_m2C36F8723D7F4579A9430E921EFDB734F9B7C1EA (void);
// 0x0000019B System.String UnityEngine.AddressableAssets.DynamicResourceLocator::get_AtlasSpriteProviderId()
extern void DynamicResourceLocator_get_AtlasSpriteProviderId_mBE84D197DDBB70DD7CD48EB2427D3EB5A99A44EA (void);
// 0x0000019C System.Void UnityEngine.AddressableAssets.DynamicResourceLocator::.ctor(UnityEngine.AddressableAssets.AddressablesImpl)
extern void DynamicResourceLocator__ctor_m7C978206B096517078313B8D47F18E4EDF396AFA (void);
// 0x0000019D System.Boolean UnityEngine.AddressableAssets.DynamicResourceLocator::Locate(System.Object,System.Type,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>&)
extern void DynamicResourceLocator_Locate_m33B8ADFDE9BB6B4CC3598F914C20B5A5D31662C9 (void);
// 0x0000019E System.Void UnityEngine.AddressableAssets.DynamicResourceLocator::CreateDynamicLocations(System.Type,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.String,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void DynamicResourceLocator_CreateDynamicLocations_m96D22BC6E0D6817D093C312C4BA99F098A83FE9A (void);
// 0x0000019F UnityEngine.AddressableAssets.AddressablesPlatform UnityEngine.AddressableAssets.PlatformMappingService::GetAddressablesPlatformInternal(UnityEngine.RuntimePlatform)
extern void PlatformMappingService_GetAddressablesPlatformInternal_m97FC778BFAA18E1D123FDC738A0C717531F908E2 (void);
// 0x000001A0 System.String UnityEngine.AddressableAssets.PlatformMappingService::GetAddressablesPlatformPathInternal(UnityEngine.RuntimePlatform)
extern void PlatformMappingService_GetAddressablesPlatformPathInternal_mD2265CEDCBD642E769E448BB788B8148A3E39937 (void);
// 0x000001A1 UnityEngine.AddressableAssets.AddressablesPlatform UnityEngine.AddressableAssets.PlatformMappingService::GetPlatform()
extern void PlatformMappingService_GetPlatform_m640E7D9426E0615A3DC3D5436A0CDBB6DBDDFB50 (void);
// 0x000001A2 System.String UnityEngine.AddressableAssets.PlatformMappingService::GetPlatformPathSubFolder()
extern void PlatformMappingService_GetPlatformPathSubFolder_mE2C6054C0C1E6FB819AC41E4BE42427E76693554 (void);
// 0x000001A3 System.Void UnityEngine.AddressableAssets.PlatformMappingService::.ctor()
extern void PlatformMappingService__ctor_mD3E14BB80434644C46DC1B92BBA4E028F4D04621 (void);
// 0x000001A4 System.Void UnityEngine.AddressableAssets.PlatformMappingService::.cctor()
extern void PlatformMappingService__cctor_m103FA01AB13C412E6146247EB838BDF9AFCC2D5A (void);
// 0x000001A5 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent UnityEngine.AddressableAssets.Utility.DiagnosticInfo::CreateEvent(System.String,UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventType,System.Int32,System.Int32)
extern void DiagnosticInfo_CreateEvent_mED2A79D2E7203810FDB447FBCA56B01EBAAF0605 (void);
// 0x000001A6 System.Void UnityEngine.AddressableAssets.Utility.DiagnosticInfo::.ctor()
extern void DiagnosticInfo__ctor_mF8DC31D77D60B56C86A0C10FB3A6DBAC1F68EDFA (void);
// 0x000001A7 System.Void UnityEngine.AddressableAssets.Utility.ResourceManagerDiagnostics::.ctor(UnityEngine.ResourceManagement.ResourceManager)
extern void ResourceManagerDiagnostics__ctor_mD87E45FC905706C92E838A9713F638677650AD98 (void);
// 0x000001A8 System.Int32 UnityEngine.AddressableAssets.Utility.ResourceManagerDiagnostics::SumDependencyNameHashCodes(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManagerDiagnostics_SumDependencyNameHashCodes_m11A34B0DBCCF159DEB73303445C038BBAB4A564C (void);
// 0x000001A9 System.Int32 UnityEngine.AddressableAssets.Utility.ResourceManagerDiagnostics::CalculateHashCode(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManagerDiagnostics_CalculateHashCode_mA02167B20B3C7563BC71D4D2F7C3D6EAAADE1D52 (void);
// 0x000001AA System.Int32 UnityEngine.AddressableAssets.Utility.ResourceManagerDiagnostics::CalculateCompletedOperationHashcode(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManagerDiagnostics_CalculateCompletedOperationHashcode_m84D150B1B01FB4CD6E06AFC1BBF25684770DCB71 (void);
// 0x000001AB System.String UnityEngine.AddressableAssets.Utility.ResourceManagerDiagnostics::GenerateCompletedOperationDisplayName(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManagerDiagnostics_GenerateCompletedOperationDisplayName_mBFDE570A0ABB5D226567C9A1796A055BF468C044 (void);
// 0x000001AC System.Void UnityEngine.AddressableAssets.Utility.ResourceManagerDiagnostics::OnResourceManagerDiagnosticEvent(UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext)
extern void ResourceManagerDiagnostics_OnResourceManagerDiagnosticEvent_mAA092F781B041FA18E1E76C2080E49F0688B05AA (void);
// 0x000001AD System.Void UnityEngine.AddressableAssets.Utility.ResourceManagerDiagnostics::Dispose()
extern void ResourceManagerDiagnostics_Dispose_mA44A97DBF2724CF358F4DDCF8BFB73E7B65E345A (void);
// 0x000001AE System.Int32 UnityEngine.AddressableAssets.Utility.SerializationUtilities::ReadInt32FromByteArray(System.Byte[],System.Int32)
extern void SerializationUtilities_ReadInt32FromByteArray_m7208EB75D52FBA8050C4242E5804121150BD163E (void);
// 0x000001AF System.Int32 UnityEngine.AddressableAssets.Utility.SerializationUtilities::WriteInt32ToByteArray(System.Byte[],System.Int32,System.Int32)
extern void SerializationUtilities_WriteInt32ToByteArray_m62347A957BACBFBC3C6B45F27DE5A14B92C1130D (void);
// 0x000001B0 System.Object UnityEngine.AddressableAssets.Utility.SerializationUtilities::ReadObjectFromByteArray(System.Byte[],System.Int32)
extern void SerializationUtilities_ReadObjectFromByteArray_m25B6A5CF707F49ED4A89581E08CB0B411A7E8736 (void);
// 0x000001B1 System.Int32 UnityEngine.AddressableAssets.Utility.SerializationUtilities::WriteObjectToByteList(System.Object,System.Collections.Generic.List`1<System.Byte>)
extern void SerializationUtilities_WriteObjectToByteList_mE1FCA90B8FB3045812E47C7C3E8F2B4AC670AB8D (void);
// 0x000001B2 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider::.ctor(UnityEngine.ResourceManagement.ResourceManager)
extern void ContentCatalogProvider__ctor_m99CF9FAED9037A9DF4C56F29008588003553F33B (void);
// 0x000001B3 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void ContentCatalogProvider_Release_m14251E05057F6755043D9C6B7BFB9ADFCA0C1F3A (void);
// 0x000001B4 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void ContentCatalogProvider_Provide_m310BF05B17183E5D6DC6232B0A5720E6EBBD90FB (void);
// 0x000001B5 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle,System.Boolean,System.Boolean)
extern void InternalOp_Start_mBA319941DAF93A1B6F10D648AFC976C90355FC1B (void);
// 0x000001B6 System.Boolean UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::WaitForCompletionCallback()
extern void InternalOp_WaitForCompletionCallback_mB65317CF60A9D44F17F346D2F728B49E86BC7E43 (void);
// 0x000001B7 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::Release()
extern void InternalOp_Release_mF75964C2A20590986E276485E0623291163CCBF7 (void);
// 0x000001B8 System.Boolean UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::CanLoadCatalogFromBundle(System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void InternalOp_CanLoadCatalogFromBundle_m81E0C2D0483BC7C657F02CE3F683130B6FDE4402 (void);
// 0x000001B9 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::LoadCatalog(System.String,System.Boolean)
extern void InternalOp_LoadCatalog_m3BD2EA9695428117D8183A87619A4F00D533ABED (void);
// 0x000001BA System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::CatalogLoadOpCompleteCallback(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData>)
extern void InternalOp_CatalogLoadOpCompleteCallback_mF540E9E36BE81C8EEDEBE651ADC13221DD4D2BDA (void);
// 0x000001BB System.String UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::GetTransformedInternalId(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void InternalOp_GetTransformedInternalId_m2B79066D18B1CA0A43625A07FAD8C2E46F4DCFCA (void);
// 0x000001BC System.String UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::DetermineIdToLoad(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Collections.Generic.IList`1<System.Object>,System.Boolean)
extern void InternalOp_DetermineIdToLoad_m22719600717614A82404ED8517863E294B73E894 (void);
// 0x000001BD System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::OnCatalogLoaded(UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData)
extern void InternalOp_OnCatalogLoaded_mDCCCFFD35BF36D018065687D5478D71959A03064 (void);
// 0x000001BE System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::.ctor()
extern void InternalOp__ctor_m70A31EAA81C793A72A33540F50B464140510D827 (void);
// 0x000001BF System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::<LoadCatalog>b__14_0(UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData)
extern void InternalOp_U3CLoadCatalogU3Eb__14_0_m386E5D96E7E40EC21121029208C75C673E98914E (void);
// 0x000001C0 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::add_OnLoaded(System.Action`1<UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData>)
extern void BundledCatalog_add_OnLoaded_m89F013E1D4A53A2F3E06242E60457E2759B70D99 (void);
// 0x000001C1 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::remove_OnLoaded(System.Action`1<UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData>)
extern void BundledCatalog_remove_OnLoaded_m8CFABBF3864BF236E48FB319D73BD47A38E3BBBC (void);
// 0x000001C2 System.Boolean UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::get_OpInProgress()
extern void BundledCatalog_get_OpInProgress_mC5ECE605A75AD026A6D1F67B6E605EF09CB5818D (void);
// 0x000001C3 System.Boolean UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::get_OpIsSuccess()
extern void BundledCatalog_get_OpIsSuccess_m3DF65BA8CF1F5002982EA14631DCC9FB23B5F85F (void);
// 0x000001C4 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::.ctor(System.String,System.Int32)
extern void BundledCatalog__ctor_m77CAF3A45BDB4D627B27B3AEFFA114C18957F9B9 (void);
// 0x000001C5 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::Finalize()
extern void BundledCatalog_Finalize_mFA2BAB8D3568564FDD74C7EBEA72A28C19C80E98 (void);
// 0x000001C6 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::Unload()
extern void BundledCatalog_Unload_m528F5870F82A7655283EF0628DFE7CEB1A7BEC9C (void);
// 0x000001C7 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::LoadCatalogFromBundleAsync()
extern void BundledCatalog_LoadCatalogFromBundleAsync_m2638C265C422736FFE49C19D0C5061ECAFA8663B (void);
// 0x000001C8 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::WebRequestOperationCompleted(UnityEngine.AsyncOperation)
extern void BundledCatalog_WebRequestOperationCompleted_mF5355237E02D42B314B8C0BAF208D912C1FB73BF (void);
// 0x000001C9 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::LoadTextAssetRequestComplete(UnityEngine.AsyncOperation)
extern void BundledCatalog_LoadTextAssetRequestComplete_m7A2DB37A2EA178267AAEFD928DAC0B277623746A (void);
// 0x000001CA System.Boolean UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::WaitForCompletion()
extern void BundledCatalog_WaitForCompletion_m707B9D2DAB28FB1CD2686CF91320710FCFC5F951 (void);
// 0x000001CB System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::<LoadCatalogFromBundleAsync>b__19_1(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void BundledCatalog_U3CLoadCatalogFromBundleAsyncU3Eb__19_1_m7D79920FA26E4A39734963B744101D0BF3B626DB (void);
// 0x000001CC System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::<LoadCatalogFromBundleAsync>b__19_0(UnityEngine.AsyncOperation)
extern void BundledCatalog_U3CLoadCatalogFromBundleAsyncU3Eb__19_0_m5C73C7830DD18FE551D3F4AFBE83472522444BAC (void);
// 0x000001CD System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::get_InternalId()
extern void ContentCatalogDataEntry_get_InternalId_m88B81554FCA77225587E72F774FE251B518E7741 (void);
// 0x000001CE System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::set_InternalId(System.String)
extern void ContentCatalogDataEntry_set_InternalId_m027ED3747C84D1E55ABA4635D1D515E819A79A6B (void);
// 0x000001CF System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::get_Provider()
extern void ContentCatalogDataEntry_get_Provider_m2CFE124B48280AA5498AA25E71C11A7BC0532F64 (void);
// 0x000001D0 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::set_Provider(System.String)
extern void ContentCatalogDataEntry_set_Provider_mA02C4D8225B1DE0DCE543BF8449373BCC0184F4B (void);
// 0x000001D1 System.Collections.Generic.List`1<System.Object> UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::get_Keys()
extern void ContentCatalogDataEntry_get_Keys_m6244DF2B99A239706E6BBA15734D1990F25F7A94 (void);
// 0x000001D2 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::set_Keys(System.Collections.Generic.List`1<System.Object>)
extern void ContentCatalogDataEntry_set_Keys_m4CC317F6F88E77D3873B3C3EDE493DD4E772CE09 (void);
// 0x000001D3 System.Collections.Generic.List`1<System.Object> UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::get_Dependencies()
extern void ContentCatalogDataEntry_get_Dependencies_mC61A64CB8C2449B553A51FFC35B6638A036F087D (void);
// 0x000001D4 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::set_Dependencies(System.Collections.Generic.List`1<System.Object>)
extern void ContentCatalogDataEntry_set_Dependencies_m2FE695C51C371E338674CB04541EDE141C679041 (void);
// 0x000001D5 System.Object UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::get_Data()
extern void ContentCatalogDataEntry_get_Data_m680BC7499C1CCA6907A677F8B2A7109E4A90706D (void);
// 0x000001D6 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::set_Data(System.Object)
extern void ContentCatalogDataEntry_set_Data_mE68BADE0ADEDD06B4BE919EC716E5E39B83D96E9 (void);
// 0x000001D7 System.Type UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::get_ResourceType()
extern void ContentCatalogDataEntry_get_ResourceType_m63B916D24760620D0433B3E23D1E1E494FCCE671 (void);
// 0x000001D8 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::set_ResourceType(System.Type)
extern void ContentCatalogDataEntry_set_ResourceType_m9FC6D88B5E237906D5D9EBCADAE179C4FC430147 (void);
// 0x000001D9 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::.ctor(System.Type,System.String,System.String,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Object)
extern void ContentCatalogDataEntry__ctor_m6F9DEDFFC197BDC93E6F3C8081237EA76F042FFC (void);
// 0x000001DA System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::get_ProviderId()
extern void ContentCatalogData_get_ProviderId_mF4F3B98EF5B864883E687CCB2EF81C7418B65A4F (void);
// 0x000001DB System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::set_ProviderId(System.String)
extern void ContentCatalogData_set_ProviderId_mAD7599F3376242D73B9AF1B889707C9D2A7741FC (void);
// 0x000001DC UnityEngine.ResourceManagement.Util.ObjectInitializationData UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::get_InstanceProviderData()
extern void ContentCatalogData_get_InstanceProviderData_m1C8F75FC83CA8BCAAD687DCC3DA7A03D27C1181C (void);
// 0x000001DD System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::set_InstanceProviderData(UnityEngine.ResourceManagement.Util.ObjectInitializationData)
extern void ContentCatalogData_set_InstanceProviderData_m6EEBE5EB6281CB3C0E68A26F573D499C36E3222A (void);
// 0x000001DE UnityEngine.ResourceManagement.Util.ObjectInitializationData UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::get_SceneProviderData()
extern void ContentCatalogData_get_SceneProviderData_m5BC5272A27857E6603B0EA4D2901BBE3BA6CEDBA (void);
// 0x000001DF System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::set_SceneProviderData(UnityEngine.ResourceManagement.Util.ObjectInitializationData)
extern void ContentCatalogData_set_SceneProviderData_m8396F8999AFE0099A3199EEE8824C8ECB89613E9 (void);
// 0x000001E0 System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData> UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::get_ResourceProviderData()
extern void ContentCatalogData_get_ResourceProviderData_m0743A4E670303C0CFA9ECC27C6BEF3DA4760DBD0 (void);
// 0x000001E1 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::set_ResourceProviderData(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData>)
extern void ContentCatalogData_set_ResourceProviderData_mB8284C2E422B0FB2BBCDDF54C0C6BCA4D8D1E31C (void);
// 0x000001E2 System.String[] UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::get_ProviderIds()
extern void ContentCatalogData_get_ProviderIds_m5096D0A3509057E74245060BF0D0FDCEF95B8924 (void);
// 0x000001E3 System.String[] UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::get_InternalIds()
extern void ContentCatalogData_get_InternalIds_m94EEF66683216658704396B9B9538DFE9CB3220D (void);
// 0x000001E4 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::CleanData()
extern void ContentCatalogData_CleanData_mE00F4DE80C610B0416004EEB6A3C012648A79C0C (void);
// 0x000001E5 UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::CreateCustomLocator(System.String,System.String)
extern void ContentCatalogData_CreateCustomLocator_m5911B4CBFB08928BB221DC6BC7E1C0BA90EA7EA3 (void);
// 0x000001E6 UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::CreateLocator(System.String)
extern void ContentCatalogData_CreateLocator_mA934DD4D237BB91D8A9D89F399E49176F7B04358 (void);
// 0x000001E7 System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::ExpandInternalId(System.String[],System.String)
extern void ContentCatalogData_ExpandInternalId_mC99623AABA1FFCBA39679DA3ED4FCAC080E52058 (void);
// 0x000001E8 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::.ctor()
extern void ContentCatalogData__ctor_m0B697DAC27F186A36AAEA0ED393A788054F7794E (void);
// 0x000001E9 System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_InternalId()
extern void CompactLocation_get_InternalId_m738E3539D54523E8C6D22C3AEA7B853A1B5214C7 (void);
// 0x000001EA System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_ProviderId()
extern void CompactLocation_get_ProviderId_m5B9901D6A2D455414B183CF944FE9BDC69215F9B (void);
// 0x000001EB System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_Dependencies()
extern void CompactLocation_get_Dependencies_m05DB0C02FC5363E09A81F5A80E99DD789FE8D8CD (void);
// 0x000001EC System.Boolean UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_HasDependencies()
extern void CompactLocation_get_HasDependencies_mA792160B6F43E12D5EBB2508CFCBF59F2C713FAD (void);
// 0x000001ED System.Int32 UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_DependencyHashCode()
extern void CompactLocation_get_DependencyHashCode_m99A9EF38CD8B8056BDE9D328C78C14208FA31ECD (void);
// 0x000001EE System.Object UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_Data()
extern void CompactLocation_get_Data_mE7B41058311E50BE1A2804C552AB483FD58F6B74 (void);
// 0x000001EF System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_PrimaryKey()
extern void CompactLocation_get_PrimaryKey_mB6C7C918BC79E2160CFE6C24CA65E2A13DEF4E92 (void);
// 0x000001F0 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::set_PrimaryKey(System.String)
extern void CompactLocation_set_PrimaryKey_mBE1F165D33633409D968F188031605C0A7248B8F (void);
// 0x000001F1 System.Type UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_ResourceType()
extern void CompactLocation_get_ResourceType_mE0D0AC4437BBDF1A91FA45E119AF175C7C7090D6 (void);
// 0x000001F2 System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::ToString()
extern void CompactLocation_ToString_mE2FE2F271E201F10E002144D5CB9E235D1B53489 (void);
// 0x000001F3 System.Int32 UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::Hash(System.Type)
extern void CompactLocation_Hash_m7BC3BD16DE3F6111E3E783BF8C77AAC5B0D253E8 (void);
// 0x000001F4 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::.ctor(UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap,System.String,System.String,System.Object,System.Object,System.Int32,System.String,System.Type)
extern void CompactLocation__ctor_m886D60D95BC695AC34D4BE28EF7F428AB03B4B0B (void);
// 0x000001F5 System.String UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator::get_LocatorId()
// 0x000001F6 System.Collections.Generic.IEnumerable`1<System.Object> UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator::get_Keys()
// 0x000001F7 System.Boolean UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator::Locate(System.Object,System.Type,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>&)
// 0x000001F8 System.Boolean UnityEngine.AddressableAssets.ResourceLocators.LegacyResourcesLocator::Locate(System.Object,System.Type,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>&)
extern void LegacyResourcesLocator_Locate_m30FFAE3B0B1FD2B5D69C73514DD58960411CBBF2 (void);
// 0x000001F9 System.Collections.Generic.IEnumerable`1<System.Object> UnityEngine.AddressableAssets.ResourceLocators.LegacyResourcesLocator::get_Keys()
extern void LegacyResourcesLocator_get_Keys_mE9F6997869B6D2BED7AD49BFD5EA614D8E3F491E (void);
// 0x000001FA System.String UnityEngine.AddressableAssets.ResourceLocators.LegacyResourcesLocator::get_LocatorId()
extern void LegacyResourcesLocator_get_LocatorId_mAD5A28CBCD892CF1DC71A0BC88723039CC30B3AD (void);
// 0x000001FB System.Void UnityEngine.AddressableAssets.ResourceLocators.LegacyResourcesLocator::.ctor()
extern void LegacyResourcesLocator__ctor_m7E2F2B3EE4D19CE907033538381CF6B0AB3A2ED4 (void);
// 0x000001FC System.String[] UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::get_Keys()
extern void ResourceLocationData_get_Keys_m2C1E479B14E5470CBAD96FF11E2062068C025A9F (void);
// 0x000001FD System.String UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::get_InternalId()
extern void ResourceLocationData_get_InternalId_mD05C523F4F7B8F22F3C253A15A11A4EBA2334025 (void);
// 0x000001FE System.String UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::get_Provider()
extern void ResourceLocationData_get_Provider_mD8517600C7A7BF4EC16D266EC06C53A657D2DB48 (void);
// 0x000001FF System.String[] UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::get_Dependencies()
extern void ResourceLocationData_get_Dependencies_mC034D349982CD293D110712DAE4017DE072C11CA (void);
// 0x00000200 System.Type UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::get_ResourceType()
extern void ResourceLocationData_get_ResourceType_mA9B848722D46B61E2AE989746375061F44638BEC (void);
// 0x00000201 System.Object UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::get_Data()
extern void ResourceLocationData_get_Data_m49C40E3635E2B0B902F2A629720E807961C5A711 (void);
// 0x00000202 System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::set_Data(System.Object)
extern void ResourceLocationData_set_Data_m5AC6BEC5BB8A3D8F1228D4130306AA2D4E810C25 (void);
// 0x00000203 System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::.ctor(System.String[],System.String,System.Type,System.Type,System.String[])
extern void ResourceLocationData__ctor_m692150BBC70638AA080BE88A52DD33F0D4153DAF (void);
// 0x00000204 System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::.ctor(System.String,System.Int32)
extern void ResourceLocationMap__ctor_m42536CDA89B99C6E15A735B8967B7EDE334B641A (void);
// 0x00000205 System.String UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::get_LocatorId()
extern void ResourceLocationMap_get_LocatorId_m6DD3DEA27A3669D54790577849F312962B7F20F6 (void);
// 0x00000206 System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::set_LocatorId(System.String)
extern void ResourceLocationMap_set_LocatorId_m981702812D893BAA110F890F9BF7567CBE921E10 (void);
// 0x00000207 System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::.ctor(System.String,System.Collections.Generic.IList`1<UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData>)
extern void ResourceLocationMap__ctor_mA84AB49C3978999C3104A4FC013FA23183D13FAE (void);
// 0x00000208 System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::get_Locations()
extern void ResourceLocationMap_get_Locations_m103289EC8AF326CF3456FC27B38650D17DD3B4EB (void);
// 0x00000209 System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::set_Locations(System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>>)
extern void ResourceLocationMap_set_Locations_m8A1C936292C144B061EB196B54E43F581F13E823 (void);
// 0x0000020A System.Collections.Generic.IEnumerable`1<System.Object> UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::get_Keys()
extern void ResourceLocationMap_get_Keys_m20284F66B83205D4118A47357012DCB424EB5BCE (void);
// 0x0000020B System.Boolean UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::Locate(System.Object,System.Type,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>&)
extern void ResourceLocationMap_Locate_m0FD3ED42E565211E352D8A71FF30F02BA0BCFE2B (void);
// 0x0000020C System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::Add(System.Object,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceLocationMap_Add_m1F644466D3C8AEE69DF0CE77936E5380AEC03A30 (void);
// 0x0000020D System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::Add(System.Object,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>)
extern void ResourceLocationMap_Add_mA31AF37A4F023C5052A483F39E4F457C348AC6C5 (void);
// 0x0000020E System.Reflection.Assembly[] UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::GetAssemblies()
extern void AddressablesRuntimeProperties_GetAssemblies_mD0DF89F46D215F9E86193E89715CFFF86E893B90 (void);
// 0x0000020F System.Int32 UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::GetCachedValueCount()
extern void AddressablesRuntimeProperties_GetCachedValueCount_mE4DD844E61291321DB37092770349B0C48B61BCC (void);
// 0x00000210 System.Void UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::SetPropertyValue(System.String,System.String)
extern void AddressablesRuntimeProperties_SetPropertyValue_m7B873826B0E67BE8809A38737C58EB6E24519D7B (void);
// 0x00000211 System.Void UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::ClearCachedPropertyValues()
extern void AddressablesRuntimeProperties_ClearCachedPropertyValues_m4E61DE517278ED567089426FBF94805005FAE89D (void);
// 0x00000212 System.String UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::EvaluateProperty(System.String)
extern void AddressablesRuntimeProperties_EvaluateProperty_mC9DE5E9EB3D52F162BDD3F4453DB10D69C07813E (void);
// 0x00000213 System.String UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::EvaluateString(System.String)
extern void AddressablesRuntimeProperties_EvaluateString_m09D987F1662FBF58992BED59F2B25A7F26309057 (void);
// 0x00000214 System.String UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::EvaluateString(System.String,System.Char,System.Char,System.Func`2<System.String,System.String>)
extern void AddressablesRuntimeProperties_EvaluateString_m1E58500E7512870A9906BBAADA7E16BC912E85EE (void);
// 0x00000215 System.Void UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::.cctor()
extern void AddressablesRuntimeProperties__cctor_m81C596E4ADACBBB4F648DC4FC8C6FC959142D402 (void);
// 0x00000216 System.Boolean UnityEngine.AddressableAssets.Initialization.CacheInitialization::Initialize(System.String,System.String)
extern void CacheInitialization_Initialize_m043176BE3308606AE0EFD34AA3454A7C1B7DB408 (void);
// 0x00000217 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Initialization.CacheInitialization::InitializeAsync(UnityEngine.ResourceManagement.ResourceManager,System.String,System.String)
extern void CacheInitialization_InitializeAsync_m1FB035B45681C362143C803D7C7BAAFDAF774DBD (void);
// 0x00000218 System.String UnityEngine.AddressableAssets.Initialization.CacheInitialization::get_RootPath()
extern void CacheInitialization_get_RootPath_m4BDB173F10EC89E65C019A13B092030051675180 (void);
// 0x00000219 System.Void UnityEngine.AddressableAssets.Initialization.CacheInitialization::.ctor()
extern void CacheInitialization__ctor_m15147548976A6A48265C7ED182871EB4E67A74B5 (void);
// 0x0000021A System.Void UnityEngine.AddressableAssets.Initialization.CacheInitialization/CacheInitOp::Init(System.Func`1<System.Boolean>)
extern void CacheInitOp_Init_m45BB077C83F6B0EA8829CE0B7ACFA5E3B86FE22D (void);
// 0x0000021B System.Boolean UnityEngine.AddressableAssets.Initialization.CacheInitialization/CacheInitOp::InvokeWaitForCompletion()
extern void CacheInitOp_InvokeWaitForCompletion_m39DDE30A04FE93540B346DCF56A13F0E14C68A51 (void);
// 0x0000021C System.Void UnityEngine.AddressableAssets.Initialization.CacheInitialization/CacheInitOp::Update(System.Single)
extern void CacheInitOp_Update_m70AE29B18FFD5183A73802FFC2FFB9A912E82F3D (void);
// 0x0000021D System.Void UnityEngine.AddressableAssets.Initialization.CacheInitialization/CacheInitOp::Execute()
extern void CacheInitOp_Execute_m57E6222D65EC5A94AF80329D11CAB676DC630640 (void);
// 0x0000021E System.Void UnityEngine.AddressableAssets.Initialization.CacheInitialization/CacheInitOp::.ctor()
extern void CacheInitOp__ctor_mFCB55DF57D9B371B76D833EF097C4DCFC7743575 (void);
// 0x0000021F System.Void UnityEngine.AddressableAssets.Initialization.CacheInitialization/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mDEF17A6EEEC394BD845B922C6679566D70A37DDC (void);
// 0x00000220 System.Boolean UnityEngine.AddressableAssets.Initialization.CacheInitialization/<>c__DisplayClass1_0::<InitializeAsync>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CInitializeAsyncU3Eb__0_m4D58B4B3BB1941E794A9799EE4588AE536783C82 (void);
// 0x00000221 System.Boolean UnityEngine.AddressableAssets.Initialization.CacheInitializationData::get_CompressionEnabled()
extern void CacheInitializationData_get_CompressionEnabled_mDE7727B28A390E559AEA0C5BF1BB5F26F02B7F47 (void);
// 0x00000222 System.Void UnityEngine.AddressableAssets.Initialization.CacheInitializationData::set_CompressionEnabled(System.Boolean)
extern void CacheInitializationData_set_CompressionEnabled_mF13BEE13E4DD5A92ED24E92E7A758348F99D59DB (void);
// 0x00000223 System.String UnityEngine.AddressableAssets.Initialization.CacheInitializationData::get_CacheDirectoryOverride()
extern void CacheInitializationData_get_CacheDirectoryOverride_m8C1C5A96FA4DA1F2867571217C9CD5ABFD8E85A4 (void);
// 0x00000224 System.Void UnityEngine.AddressableAssets.Initialization.CacheInitializationData::set_CacheDirectoryOverride(System.String)
extern void CacheInitializationData_set_CacheDirectoryOverride_m7DF8064B3302A5D971CF03274FF06DFF7257F44E (void);
// 0x00000225 System.Int32 UnityEngine.AddressableAssets.Initialization.CacheInitializationData::get_ExpirationDelay()
extern void CacheInitializationData_get_ExpirationDelay_mF2E8EF7956621254B0D67C1D4C623B285943A680 (void);
// 0x00000226 System.Void UnityEngine.AddressableAssets.Initialization.CacheInitializationData::set_ExpirationDelay(System.Int32)
extern void CacheInitializationData_set_ExpirationDelay_mD797B793BF96A63E666B628823B92B2B58BD0330 (void);
// 0x00000227 System.Boolean UnityEngine.AddressableAssets.Initialization.CacheInitializationData::get_LimitCacheSize()
extern void CacheInitializationData_get_LimitCacheSize_mF3811D5468607D532A9D711366EB4157DAEF1911 (void);
// 0x00000228 System.Void UnityEngine.AddressableAssets.Initialization.CacheInitializationData::set_LimitCacheSize(System.Boolean)
extern void CacheInitializationData_set_LimitCacheSize_mB56ED15D41C914A6FA73C779738727C9B60090F0 (void);
// 0x00000229 System.Int64 UnityEngine.AddressableAssets.Initialization.CacheInitializationData::get_MaximumCacheSize()
extern void CacheInitializationData_get_MaximumCacheSize_m693786CE02BF1B8FE03277A4A5ED0A7719D807CB (void);
// 0x0000022A System.Void UnityEngine.AddressableAssets.Initialization.CacheInitializationData::set_MaximumCacheSize(System.Int64)
extern void CacheInitializationData_set_MaximumCacheSize_mE5A6F0ECD75ADDDA3CCDACA4A3E83C612B815275 (void);
// 0x0000022B System.Void UnityEngine.AddressableAssets.Initialization.CacheInitializationData::.ctor()
extern void CacheInitializationData__ctor_m49D8D33FCA342CB6460244FF627D04F76DEC4E86 (void);
// 0x0000022C System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation::.ctor(UnityEngine.AddressableAssets.AddressablesImpl)
extern void InitializationOperation__ctor_mF0FFB347D76F1B58CF52F27321536C0A080FF39B (void);
// 0x0000022D System.Single UnityEngine.AddressableAssets.Initialization.InitializationOperation::get_Progress()
extern void InitializationOperation_get_Progress_mE204B2808FA40DBC034B0175DA849611F24A4C07 (void);
// 0x0000022E System.String UnityEngine.AddressableAssets.Initialization.InitializationOperation::get_DebugName()
extern void InitializationOperation_get_DebugName_m6CC0A154DE6DDE764F5BB9EAD40F7A564D9AA798 (void);
// 0x0000022F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Initialization.InitializationOperation::CreateInitializationOperation(UnityEngine.AddressableAssets.AddressablesImpl,System.String,System.String)
extern void InitializationOperation_CreateInitializationOperation_mEC2364AD89CA770A2BE2AFE369AF2A4F760F646A (void);
// 0x00000230 System.Boolean UnityEngine.AddressableAssets.Initialization.InitializationOperation::InvokeWaitForCompletion()
extern void InitializationOperation_InvokeWaitForCompletion_mB02865B5F0CB2DB49725182A0DC0800E92D36E7E (void);
// 0x00000231 System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation::Execute()
extern void InitializationOperation_Execute_mF086EECAA0F8C6C4FB7A2A3ED14D832C73AA76A7 (void);
// 0x00000232 System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation::LoadProvider(UnityEngine.AddressableAssets.AddressablesImpl,UnityEngine.ResourceManagement.Util.ObjectInitializationData,System.String)
extern void InitializationOperation_LoadProvider_mF024312382D0DB0966B01C4C3CBACD955347492D (void);
// 0x00000233 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Initialization.InitializationOperation::OnCatalogDataLoaded(UnityEngine.AddressableAssets.AddressablesImpl,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData>,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void InitializationOperation_OnCatalogDataLoaded_mD6710C04B2A6CF604D5C54C952A4D6191E4775FB (void);
// 0x00000234 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Initialization.InitializationOperation::LoadContentCatalog(UnityEngine.AddressableAssets.AddressablesImpl,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void InitializationOperation_LoadContentCatalog_mFF2FC5C623E9D55A23A608B472D72FF7A69B3CE9 (void);
// 0x00000235 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Initialization.InitializationOperation::LoadContentCatalog(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void InitializationOperation_LoadContentCatalog_mE21AEFB03C7143832DBE3328BC0C3E9DC1BD5C80 (void);
// 0x00000236 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Initialization.InitializationOperation::LoadContentCatalogInternal(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Int32,UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void InitializationOperation_LoadContentCatalogInternal_m2143386F987003C115BD67D981CE294723DC687E (void);
// 0x00000237 System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation::LoadOpComplete(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap,System.Int32,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void InitializationOperation_LoadOpComplete_m7EBD01DE5D7E2B89F1E3A813D05B4EA727060145 (void);
// 0x00000238 System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c::.cctor()
extern void U3CU3Ec__cctor_mB573B3CE8F105B13D3C3F1775297C6804B43245A (void);
// 0x00000239 System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c::.ctor()
extern void U3CU3Ec__ctor_m5E93AA74E4E35D426421AE2D7C566873D1F45269 (void);
// 0x0000023A System.Boolean UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c::<Execute>b__13_0(UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider)
extern void U3CU3Ec_U3CExecuteU3Eb__13_0_mB531A590C24637E1CDD4817909CE0C3C408A82CA (void);
// 0x0000023B System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m42F97EEDB0571819ED4D155A48BF0B16E38687E2 (void);
// 0x0000023C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c__DisplayClass16_0::<LoadContentCatalog>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData>)
extern void U3CU3Ec__DisplayClass16_0_U3CLoadContentCatalogU3Eb__0_mA4DBBD2AF81C0923F10873BBA6AAE509250046FA (void);
// 0x0000023D System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mAB8FC8B6AED17CDF499D0426A80E5E63F76353C1 (void);
// 0x0000023E System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c__DisplayClass18_0::<LoadContentCatalogInternal>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>)
extern void U3CU3Ec__DisplayClass18_0_U3CLoadContentCatalogInternalU3Eb__0_m038FD7BBA1EB254F8E6BB5F90F8F19B3BAA97B30 (void);
// 0x0000023F System.String UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_BuildTarget()
extern void ResourceManagerRuntimeData_get_BuildTarget_mA086F9E8833B970B4CC2CA8EA451943A7916C887 (void);
// 0x00000240 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_BuildTarget(System.String)
extern void ResourceManagerRuntimeData_set_BuildTarget_m9A14BF8D8F9B34503BE283BE235051F7FE8421B6 (void);
// 0x00000241 System.String UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_SettingsHash()
extern void ResourceManagerRuntimeData_get_SettingsHash_m993577DFF8C02D492A19984C6C9D44DF3A66DF32 (void);
// 0x00000242 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_SettingsHash(System.String)
extern void ResourceManagerRuntimeData_set_SettingsHash_mEB2C6A6C6660C552EA56B78DCD8AC5E23A803FF2 (void);
// 0x00000243 System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData> UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_CatalogLocations()
extern void ResourceManagerRuntimeData_get_CatalogLocations_mF0A230795FB77AFE64D8A49266F267D207A3FCD4 (void);
// 0x00000244 System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_ProfileEvents()
extern void ResourceManagerRuntimeData_get_ProfileEvents_mEDDD57F70E4338C90E8A8903C704DFA29E647A04 (void);
// 0x00000245 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_ProfileEvents(System.Boolean)
extern void ResourceManagerRuntimeData_set_ProfileEvents_m497E71165681D9A23C73D18361C0650A85766D68 (void);
// 0x00000246 System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_LogResourceManagerExceptions()
extern void ResourceManagerRuntimeData_get_LogResourceManagerExceptions_mE2744B0993B3504853CD2927FDA2D043DABB7DE6 (void);
// 0x00000247 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_LogResourceManagerExceptions(System.Boolean)
extern void ResourceManagerRuntimeData_set_LogResourceManagerExceptions_mFB0E1C31E6AA24282F9463124B5DB9A5BD5A2063 (void);
// 0x00000248 System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData> UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_InitializationObjects()
extern void ResourceManagerRuntimeData_get_InitializationObjects_m33078507C6F754A39577BCC08D6E6E82EF89D612 (void);
// 0x00000249 System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_DisableCatalogUpdateOnStartup()
extern void ResourceManagerRuntimeData_get_DisableCatalogUpdateOnStartup_mD20A121032896B85CE68D51220D7329091AB5D5D (void);
// 0x0000024A System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_DisableCatalogUpdateOnStartup(System.Boolean)
extern void ResourceManagerRuntimeData_set_DisableCatalogUpdateOnStartup_m464691EC09EB18A64A47367B15EBE2A9F4C65BA0 (void);
// 0x0000024B System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_IsLocalCatalogInBundle()
extern void ResourceManagerRuntimeData_get_IsLocalCatalogInBundle_mEB8FB9130B8D318B17E24254803A9879608DDE15 (void);
// 0x0000024C System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_IsLocalCatalogInBundle(System.Boolean)
extern void ResourceManagerRuntimeData_set_IsLocalCatalogInBundle_m611A294DBBAA9B21FF48E483EA2054C469EB4060 (void);
// 0x0000024D System.Type UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_CertificateHandlerType()
extern void ResourceManagerRuntimeData_get_CertificateHandlerType_mE5E59C482B4A8ADB7415F454D8A6507CFE5CDAC5 (void);
// 0x0000024E System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_CertificateHandlerType(System.Type)
extern void ResourceManagerRuntimeData_set_CertificateHandlerType_m3AB31801B759EF5C15A054963E7F8A074099CB2A (void);
// 0x0000024F System.String UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_AddressablesVersion()
extern void ResourceManagerRuntimeData_get_AddressablesVersion_m191AD04B6D8F94ECC1A3A86146244E577A7ACE2A (void);
// 0x00000250 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_AddressablesVersion(System.String)
extern void ResourceManagerRuntimeData_set_AddressablesVersion_m5CAEBE23916D1E108E7A083B2D2AA5A1EB332BB3 (void);
// 0x00000251 System.Int32 UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_MaxConcurrentWebRequests()
extern void ResourceManagerRuntimeData_get_MaxConcurrentWebRequests_m1EA06475277D89F221D1E541570A1A6477BAE902 (void);
// 0x00000252 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_MaxConcurrentWebRequests(System.Int32)
extern void ResourceManagerRuntimeData_set_MaxConcurrentWebRequests_m8CA9A51936EB303138A3E69BE39021A18A9D2DE4 (void);
// 0x00000253 System.Int32 UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_CatalogRequestsTimeout()
extern void ResourceManagerRuntimeData_get_CatalogRequestsTimeout_mD2FCE622A84649C5189D997FAC59ACB4DF5CF3ED (void);
// 0x00000254 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_CatalogRequestsTimeout(System.Int32)
extern void ResourceManagerRuntimeData_set_CatalogRequestsTimeout_m541A0A437071BCF23BF7638CA0C3CAFD281AF23B (void);
// 0x00000255 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::.ctor()
extern void ResourceManagerRuntimeData__ctor_mEA8C0EEF77BB53654E36D8F9697F068A27E42D69 (void);
static Il2CppMethodPointer s_methodPointers[597] = 
{
	PackedPlayModeBuildLogs_get_RuntimeBuildLogs_m884C21F959D9C0DFB2BC26BC6AF0E8BB07CB7C9C,
	PackedPlayModeBuildLogs_set_RuntimeBuildLogs_mE1BAC7A249F55DA5CAFC7031DE55BF4BBE3693BA,
	PackedPlayModeBuildLogs__ctor_mD6D6C46CBF5179339ED9AD995B20A7553DDFC27C,
	RuntimeBuildLog__ctor_m014EB6EEDCCB7F6A3DF6F5176829414750CC3D21,
	AssetReferenceUIRestriction_ValidateAsset_mB16598DB91529807CDE4D8797E026B939A7754C1,
	AssetReferenceUIRestriction_ValidateAsset_mB29C9B428ABE11D9553EB14B7C3D5A84B09FFD0A,
	AssetReferenceUIRestriction__ctor_m22E79885F35583A38F38085048423B6B69FAD3FC,
	AssetReferenceUILabelRestriction__ctor_m4AF8ACAD8F5638454D69CC43ABC0597E381D9479,
	AssetReferenceUILabelRestriction_ValidateAsset_m0FC3607B4B8291BAC2F0DBDA50832C28E6862EAF,
	AssetReferenceUILabelRestriction_ValidateAsset_m81585357FB02801962010AE448EBE2C6309300E4,
	AssetReferenceUILabelRestriction_ToString_m5DE963979BB3001AFBCC5FF6DF76423C8BC85BA6,
	InitalizationObjectsOperation_Init_m08F5EC686FDDA4FCF0A4018C505BF435F7F7B2E7,
	InitalizationObjectsOperation_get_DebugName_mD8E85727C2B72304B21EAD512EF63D716E3EAFF8,
	InitalizationObjectsOperation_LogRuntimeWarnings_m4B371DB371E58B2D4C6BF2F915EABF9E896FC622,
	InitalizationObjectsOperation_InvokeWaitForCompletion_m542FC6F672C943D15CC703E2B808D487224C6EA6,
	InitalizationObjectsOperation_Execute_m57D25A512B8B7F18B7E07625179726C4142B00FF,
	InitalizationObjectsOperation__ctor_m4A866392D28009599D93EA895B2F860D8D0CB902,
	InitalizationObjectsOperation_U3CExecuteU3Eb__8_0_mD4874C57111ECE1C34D3F15120002BB4737E5A1F,
	InvalidKeyException_get_Key_m872576045BF896E1B0887237E64D517B6BDBC9EA,
	InvalidKeyException_set_Key_mC8B77BD80FC0951B0F6C6E0F368CB8E6616AC6A8,
	InvalidKeyException_get_Type_mF8B3BEA2A018C67B54571CFB7E8144E3B190EE31,
	InvalidKeyException_set_Type_m15AFCA253042591FCA6F3A5D669956279B296C26,
	InvalidKeyException_get_MergeMode_m5EFDEE95D9C5F8186A57FD2AC8264237DA546537,
	InvalidKeyException__ctor_mE74BFA8EBEAE325FBD3442446EAED3C0F6852CF9,
	InvalidKeyException__ctor_mE30E4658A05BECF9B47C09D7F103C55BAAC9B59F,
	InvalidKeyException__ctor_m925338AE377060E4E8E68A11A091816239F09107,
	InvalidKeyException__ctor_mD1D96140BF1D84CE9D8DF483C1B3811A5C26C7DA,
	InvalidKeyException__ctor_m4AA54BDB846621DA317D561C503710A73E5EA3ED,
	InvalidKeyException__ctor_m50AC2BFADF05F1EAB22807171535CEACE7255D44,
	InvalidKeyException__ctor_mF5F562E2C0B67F506AE930633DDAD44A335C207A,
	InvalidKeyException_get_Message_m26EE585F4AFE7101AE663B382066F11040674EA8,
	Addressables_get_m_Addressables_m4B5B344E036E113B2320ED1C55C9BC30DFE5DF03,
	Addressables_get_ResourceManager_m559415A56917C68B7B442D4CDBBC7D7793380CCA,
	Addressables_get_Instance_m79F2DCCE86CBD9DC0A7664684D60BFE110E2C4CB,
	Addressables_get_InstanceProvider_m596D5E60D0D76D85163E04F5E09D91A9CEACF85F,
	Addressables_ResolveInternalId_m0C314663A669C049F659E9EA4657E58B2561A460,
	Addressables_get_InternalIdTransformFunc_mA2ACECB5E09FD27620B990C6F6B106EB46F6A019,
	Addressables_set_InternalIdTransformFunc_m2A27854C892E04D7D9105C326366ECB9D8AF9107,
	Addressables_get_WebRequestOverride_m3CC84FE878C51342775C3D6EBF7F03AADA68FA66,
	Addressables_set_WebRequestOverride_m6280DD93646A390B7846C88FAB425F06EEB2468E,
	Addressables_get_StreamingAssetsSubFolder_m4555804C014F7A71B0B5F9A1A407E4EDC57E5CC1,
	Addressables_get_BuildPath_mAC2055EBA6A001636B90EE893682AF9A1076959E,
	Addressables_get_PlayerBuildDataPath_m1629A6A3C90CD71001FFFE8F40D4E43374A2C0D8,
	Addressables_get_RuntimePath_m5C188FC78940D15F822CAA1DB81BE452E05D83DE,
	Addressables_get_ResourceLocators_m3B761852A07068D214FC2AB5E0B6405F71EC25F5,
	Addressables_InternalSafeSerializationLog_m067E645987683EFACB60B859D94941A7C1DBF262,
	Addressables_InternalSafeSerializationLogFormat_m472AFA27C6C189E1A2DB2017998114C74A68E099,
	Addressables_Log_mB865441616E8AEFD706A46C9973463F8DE782D8D,
	Addressables_LogFormat_m5CC2D1088DEC3386AE6B26250DF30F5E9268668F,
	Addressables_LogWarning_mA94CA0617D9C4F4CD822A3EDADCD51E04D973386,
	Addressables_LogWarningFormat_mD58F7621494A8CFE3D0A7A52A52672EFEC0B8072,
	Addressables_LogError_mC84FC8496D63A3CD6EF3D8C1FA43C5B608A8DCEC,
	Addressables_LogException_m999610C7A08ABCCB7C501D754D8F4E15308F4C3C,
	Addressables_LogException_m4F49254DAF3733DB7C6B65F7EBC24C15042BA440,
	Addressables_LogErrorFormat_mB95D820554CA6DC6C93F8B85439E15491C90DA2E,
	Addressables_Initialize_mCC26D4A61B062FB38D22A1D52693E701F76FCE5C,
	Addressables_InitializeAsync_mF5F1D985415748D2FBB8B6A7968172231D28EB4B,
	Addressables_InitializeAsync_m4FE6ADB9183F5CF56E2F42F6821097613991DD6D,
	Addressables_LoadContentCatalog_m0580F63F705908A4259AC945EA94F385A2019902,
	Addressables_LoadContentCatalogAsync_m528222717A9B4AA6B02F06E368BFA5E4AF89A0A4,
	Addressables_LoadContentCatalogAsync_m1D1A85B2F2979D985BB0683FE5B22B2A132E4561,
	Addressables_get_InitializationOperation_mEFB504954577C24D51998F7B1E09FCE4211BD114,
	NULL,
	NULL,
	NULL,
	NULL,
	Addressables_LoadResourceLocations_m62EC51337AE6967A5632C20740D74CD67717900C,
	Addressables_LoadResourceLocationsAsync_mDDD2318A9512FE6ED8336D5F9861ACB92CFFAF62,
	Addressables_LoadResourceLocationsAsync_mCC2DBD081D0ABDFF104E338A8ADEF0CC7FFD5286,
	Addressables_LoadResourceLocations_mFB1F0E350828431BCB16AC9191B433A8B4F5486B,
	Addressables_LoadResourceLocationsAsync_m38A9B8D08D669F0F3A9D8B0E78DF1CA2CEFD101B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Addressables_Release_m24B87B9557F507472D48CD78314FF151BC1D5818,
	Addressables_ReleaseInstance_m00D2F96E655DCF72BF1FFCEC907C4B66DF0101F3,
	Addressables_ReleaseInstance_mC43B6B4D14372B872AC5D92287A6765A26FF9D05,
	Addressables_ReleaseInstance_m7615E57C58D027F1ECE9CFA588E145FCE322B88A,
	Addressables_GetDownloadSize_m88F151CE488F6390664A9547D036E52FFAAD573E,
	Addressables_GetDownloadSizeAsync_m257108E2E9594466FCCE7BC7CC1B9A20320C5E74,
	Addressables_GetDownloadSizeAsync_mC68F974C6B6C5DB6CC010A764C5AE4ED3A66899C,
	Addressables_GetDownloadSizeAsync_m84DF8E7205B50A955C9B3210FC80E877989930C2,
	Addressables_GetDownloadSizeAsync_m98BED1B2E8D00820433E8D09951C2EE86D67A5BF,
	Addressables_DownloadDependencies_m55A9DB08B4FE8FB7C532D0548E46C429700640AA,
	Addressables_DownloadDependenciesAsync_m4639D5F7770C7FED3BF78AAF2157EDF4C2A9301D,
	Addressables_DownloadDependenciesAsync_m24F16F248A4658FDD20647484F0EFBABF84DF6C3,
	Addressables_DownloadDependenciesAsync_m95F80568FAE3417DEE5E85236340E2800F782AA1,
	Addressables_DownloadDependenciesAsync_mA0DBE78983B5ED86848278A2EA20EAAEAF52BCFD,
	Addressables_ClearDependencyCacheAsync_m7DDCC8A4B8C592639DACD808FFEB2DE62F7E2053,
	Addressables_ClearDependencyCacheAsync_m417EE6E9C797831063D5BD38AAB07184668A786A,
	Addressables_ClearDependencyCacheAsync_m12F999E2CB96B01AC4673BBFC1F86B83BBDA0806,
	Addressables_ClearDependencyCacheAsync_m3230B6CA41E478DE3DEA4B752020B4FC15ECC379,
	Addressables_ClearDependencyCacheAsync_m0AE12FA30D94952D1FDF1434F095E84E2AF70D4C,
	Addressables_ClearDependencyCacheAsync_m5A8CA10E5B957D71E91DBE9B9CC91046DFE906D3,
	Addressables_ClearDependencyCacheAsync_m945F674B36985872315C69B1DBBDEAEA693C9829,
	Addressables_ClearDependencyCacheAsync_m6AC33D59705CBC441CAF9DD1425ED8DF6D2DE969,
	Addressables_ClearDependencyCacheAsync_m8080D82AF29FCF5F438CFD3286DAD9CB3507ADE2,
	Addressables_ClearDependencyCacheAsync_m572235A79C4F9397F53E6E90D5CA6C1B7FAB0A29,
	Addressables_Instantiate_mFEC691390D685D11D6FD70B97396069A63EEEF9E,
	Addressables_Instantiate_mD19177F6398F8FD772F0E0D82495C9DC4F6878C8,
	Addressables_Instantiate_m9431BDFDBADF511E93CD3AAF30B8F061E7A3ECBA,
	Addressables_Instantiate_mB735FE52B1C20FCB081CFFB5BFB49BD0BAD8C145,
	Addressables_Instantiate_mA28382A948C698495F972E8BA1C6071BBF601162,
	Addressables_Instantiate_m11AA22C6417407760CEE88FF89F721D6B485A7A1,
	Addressables_InstantiateAsync_m5D775562FB929D6E4CCE3B82059B8E76E58F6699,
	Addressables_InstantiateAsync_m762C64C031FAD4608B06C8AD5269EB55FA78EAF8,
	Addressables_InstantiateAsync_m46AB98E1867839586FD2979E59E189B847CDEE44,
	Addressables_InstantiateAsync_mBDC3061278EFFA1B4003AD491521EFB9C243EF1D,
	Addressables_InstantiateAsync_m18995125F0C69EE612B7FCFC1D03A3E9A30641A6,
	Addressables_InstantiateAsync_m517CAB1C5B9F75E06EC537824265EF71E0568D3F,
	Addressables_LoadScene_m1208181A694E45B027D5D730CF7D26A95591AA3B,
	Addressables_LoadScene_m1381D4454EE2D363C942A6E387BF2E77A93E7EB1,
	Addressables_LoadSceneAsync_m757E6892B8E8220658788E239EA95C4D1559B9E0,
	Addressables_LoadSceneAsync_m5235CFD68FC15F0303D7996084A4E07B953F3257,
	Addressables_UnloadScene_m30C4E405312E4A23E7467FB72B029FDB893DCB1F,
	Addressables_UnloadScene_mAFC43027294AE407D2A2066768F4DC73037AFB10,
	Addressables_UnloadScene_m25BFEF4B6136CC11DC071315E6321E8335B838DA,
	Addressables_UnloadSceneAsync_m8BCE2250FEBFF72FD5C6C388EB0FDA4BFD2670E0,
	Addressables_UnloadSceneAsync_m96AC8612343B0F54447B84FAFCBC681BDD065784,
	Addressables_UnloadScene_m0C94BB81D40267E611C5B5A95BE3A86FEC89CC2C,
	Addressables_UnloadSceneAsync_m142CFA0EDA563CEDD34AB20E7B948F913561644E,
	Addressables_UnloadSceneAsync_mD943EA608025BAAED685BB2BC090800BEB5B395F,
	Addressables_UnloadSceneAsync_mF04083B9F9C6EFFEC25AE34321BA178CA0275DF5,
	Addressables_CheckForCatalogUpdates_m8DE13D868BEBE3346CD3EBC75FE9FE103F9C655B,
	Addressables_UpdateCatalogs_mFF17D984EF399502D05E602F98A493B29C96DFB9,
	Addressables_UpdateCatalogs_m4A1BDCD62E08E543DEEAE08ECFEBCCF00FC52C9B,
	Addressables_AddResourceLocator_m950946E23EBA0E9725A098F541542AA663A30E0F,
	Addressables_RemoveResourceLocator_mD13D2102870BAC55AF724265B7CDD04CCE24F292,
	Addressables_ClearResourceLocators_mC4EBDF34123D492BE2386BBAABE4A20A6D5F7DE3,
	Addressables_CleanBundleCache_m51A22EB038A6A36D863E032B3D4CC166462F1755,
	Addressables__cctor_m3BF5C4B4F988F854F894EF7848F813347469544D,
	AddressablesImpl_get_InstanceProvider_m60B8111043831A5C192F0E2E793C464437F8AF05,
	AddressablesImpl_set_InstanceProvider_m664E2242364A031D635785701A9FE7E6245519C5,
	AddressablesImpl_get_ResourceManager_mE53CB81CFC445C16556F59FA7AFDA7166A2500AF,
	AddressablesImpl_get_CatalogRequestsTimeout_mC00AF41F0746C33CF3AB0897C6473012D8293908,
	AddressablesImpl_set_CatalogRequestsTimeout_m5119D389263F81622B7FB9A77F6D2C9F74FFE088,
	AddressablesImpl_get_SceneOperationCount_mE35044FF31BE68092BA0BFA10E2209D986514AAE,
	AddressablesImpl_get_TrackedHandleCount_m5B0F12CA8E02F30B15BF457E808428306BD8D670,
	AddressablesImpl__ctor_m80B4E1BFC74D6BA840B39210609C141C104D1E98,
	AddressablesImpl_ReleaseSceneManagerOperation_mAEFF479E8CDDE9DF9BED2535DCBB7FB59454DB53,
	AddressablesImpl_get_InternalIdTransformFunc_m9E994E640C0ED50B0C6B4989BF2BD563D37380F1,
	AddressablesImpl_set_InternalIdTransformFunc_m5DACF74FCB603E09E6B1DA59FDCC6F1C0905DC53,
	AddressablesImpl_get_WebRequestOverride_m2C9F84E3554A5186AFA1F2DB203919D07380E473,
	AddressablesImpl_set_WebRequestOverride_m5A3B461070AB5F7ACA61C86BB6312F78A1F8DA68,
	AddressablesImpl_get_ChainOperation_mC1FCCFDA2FC6033081B38EDB8BC4FE11298E2FA6,
	AddressablesImpl_get_ShouldChainRequest_mDE67D51FF90E874643BE1A9B6A88126865629715,
	AddressablesImpl_OnSceneUnloaded_m02FEDEA046674CA6415CB951970D345AF2F1E237,
	AddressablesImpl_get_StreamingAssetsSubFolder_m316C89384356A78100479D43D7D91AD5BFF77BD6,
	AddressablesImpl_get_BuildPath_m963EE20170AACA747BFCCA7B6F6EB16DB91995E7,
	AddressablesImpl_get_PlayerBuildDataPath_mC92B228C9F7464FE640EB4307AF2C9A51F530963,
	AddressablesImpl_get_RuntimePath_mB1937AEB6BB0B183EA22C3FEE5B2D01D6E8BC1AF,
	AddressablesImpl_Log_m33102DD76D147E5C2352A6E01E1DA4D99321E4D2,
	AddressablesImpl_LogFormat_m2C8A599541F36A17B75310D1D79300843AC71A82,
	AddressablesImpl_LogWarning_m792CC621EE43CC7AF59895CC45ACCF0389050DD4,
	AddressablesImpl_LogWarningFormat_m2F3C58F2B4391E7EC1537C7BEDB50DCB45E8B225,
	AddressablesImpl_LogError_m1E07B4F626A839AEB6F79EBBF65F3FCFDF2F83FA,
	AddressablesImpl_LogException_m3B05E2A930AA99799110595AE94EEFE1BECE3570,
	AddressablesImpl_LogException_mF07BD7A6111E2D212DEB5A13F29929CBD41529B8,
	AddressablesImpl_LogErrorFormat_m82A3630667EC364AA602B8975626C052EE7D8411,
	AddressablesImpl_ResolveInternalId_m7D64AB8275566AD49DE7F51732826AD59A6F5190,
	AddressablesImpl_get_ResourceLocators_m4B169926280BDA97DA9BC2CBEE26B457FBB1AE60,
	AddressablesImpl_AddResourceLocator_mA5AA1B1CB77E05FD412ED10F070670D6E68421CD,
	AddressablesImpl_RemoveResourceLocator_m924ECB0F4DEF8A7E9C166D4A64B044C7CF5E8AE8,
	AddressablesImpl_ClearResourceLocators_m161D6CEA0A8A9C628579D480F519ED9AC4B79666,
	AddressablesImpl_GetResourceLocations_m31926E84C7B5E28B978E32B72461BE085960A567,
	AddressablesImpl_GetResourceLocations_m036AC0E419FCE4116870541F74C3AF14255A485C,
	AddressablesImpl_InitializeAsync_m7AA5ED5316F1188603B8588F205123E9ABB22086,
	AddressablesImpl_InitializeAsync_m4A4F934AF1E0BD4A41E309E55BF6EFBE6CED9E2C,
	AddressablesImpl_InitializeAsync_m7462945023764AE7CC3561018AC4D8BE64F35933,
	AddressablesImpl_CreateCatalogLocationWithHashDependencies_m159DCC46932B06430AEA44FC1DD607D632467317,
	AddressablesImpl_LoadContentCatalogAsync_mB8908538088C868A61A9CA80ED53630B87E82614,
	AddressablesImpl_TrackHandle_m0EB206D85412C5EC04C3E119DCFA2C357C09B504,
	NULL,
	AddressablesImpl_TrackHandle_m87874D354073A48745A80A9857A523BF9C0E0560,
	AddressablesImpl_ClearTrackHandles_m3AEB8A71FDA919D76E325656BC81B48D3AA805EB,
	NULL,
	NULL,
	NULL,
	AddressablesImpl_LoadResourceLocationsWithChain_m84066677120C8EC269580B746F4A6EAC2E7E7BCE,
	AddressablesImpl_LoadResourceLocationsAsync_mBD686E2E019933C381BE8FB45630A0DE48623757,
	AddressablesImpl_LoadResourceLocationsWithChain_mDD2DD0C6044E08C3D8E467C5FAB656389D30AED6,
	AddressablesImpl_LoadResourceLocationsAsync_mF9CED2AF8C17B85062C9452D2F11E30D86D66912,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AddressablesImpl_OnHandleDestroyed_m140C3E16883E60604BA171725F40DD097AEC750F,
	AddressablesImpl_OnSceneHandleCompleted_m811671C4E1BED50CD5EA8916DD32E082BEB5C348,
	AddressablesImpl_OnHandleCompleted_m5FAA0F89CBA9EAD36D6E47E4E26AA72C4A23CEF6,
	NULL,
	NULL,
	AddressablesImpl_Release_m2EEDAAA0AAA113466C4FAD4D08B19C12781F0FFC,
	AddressablesImpl_GetDownloadSizeWithChain_m93952C3CECC56C843BF22C5CD013BEFF0D2DA2E1,
	AddressablesImpl_GetDownloadSizeWithChain_mFC1DBECFB487E157666E247A18C56310F78BDB0B,
	AddressablesImpl_GetDownloadSizeAsync_m29410C074A20BDA9F5635A40F75AAEA21E403F76,
	AddressablesImpl_GetDownloadSizeAsync_mCE9F070B71222699187176FAAE59FBD7F0385D5B,
	AddressablesImpl_DownloadDependenciesAsyncWithChain_mA63F7A9A6BAA8B7F41D661CA835F8753EBA8BF6C,
	AddressablesImpl_WrapAsDownloadLocations_m1A393450B5BEF65FA990585E73EE88CA601D32A9,
	AddressablesImpl_GatherDependenciesFromLocations_m317B74F49C1C51E73ACC91F23190775F44897613,
	AddressablesImpl_DownloadDependenciesAsync_m79B738A636E75FFDD04F4E75EAA10A2E4A97A588,
	AddressablesImpl_DownloadDependenciesAsyncWithChain_m167EEB628090B024A123964ED5ED879F6A8FFBAE,
	AddressablesImpl_DownloadDependenciesAsync_m715644B2F2A5447E35884A54929C20AFD4EF9D23,
	AddressablesImpl_DownloadDependenciesAsyncWithChain_m9EBA73B07E1D1A3EFBC5E14B322FD5C77A35F6C7,
	AddressablesImpl_DownloadDependenciesAsync_mC6D9D409B507678CFFF79E92A606705B3A48003E,
	AddressablesImpl_ClearDependencyCacheForKey_m97C168E3949496B492600F8729CE5CF9C6D76D8E,
	AddressablesImpl_AutoReleaseHandleOnCompletion_m1FCAE5461E78F1726FDD91E9964F17A9654B09A4,
	NULL,
	NULL,
	NULL,
	AddressablesImpl_ClearDependencyCacheAsync_mE13532658783AF76B1E059DE0B3E0A5F0AC71280,
	AddressablesImpl_ClearDependencyCacheAsync_mA39C55250630E7304373C8485C9D56B344625682,
	AddressablesImpl_ClearDependencyCacheAsync_mE3F995626E528742FF26A1A558FC635E0D95A561,
	AddressablesImpl_InstantiateAsync_m2D72F6BF23BE69063C7EB57E4164F37D3980C201,
	AddressablesImpl_InstantiateAsync_m31D33EA66B9E575EBF7FD5CD4AE6EFC843FE2B05,
	AddressablesImpl_InstantiateAsync_m9F0BC0FC5D4E333D0808B00490ABA1EC8732EDDA,
	AddressablesImpl_InstantiateAsync_m61EF3B1F338605448BA6B6E87B139818ECA8A68E,
	AddressablesImpl_InstantiateWithChain_mAB9A68E8FBFE6B2BB24C8D96B3CFE58696867EF2,
	AddressablesImpl_InstantiateAsync_m86CFF5195360D94673C41A5B8A0219BB26911901,
	AddressablesImpl_InstantiateWithChain_mC2AA8122F7653338963A6476D72337A7CAD962B3,
	AddressablesImpl_InstantiateAsync_mFEC16CD5EB47E0BD6E466DEA277BE4B03813F226,
	AddressablesImpl_ReleaseInstance_m3E8C497665F6158232290F49BCEE4882D1CDBE8E,
	AddressablesImpl_LoadSceneWithChain_mECE1597DADF1E4D541D5EF2BBFAC5B76C302BA35,
	AddressablesImpl_LoadSceneAsync_m55B3A837E49F6E996EABF5AA66A3AC4DDEBC00FB,
	AddressablesImpl_LoadSceneAsync_m86B583C45B1EC2DA854BF992BFCF7B3282FE5FB8,
	AddressablesImpl_UnloadSceneAsync_m35E7537D3CD26B5604DBB0F06E096F911B18BB58,
	AddressablesImpl_UnloadSceneAsync_mFEBCC78A8B783850AD1F8064FC0DDCBFECAB839E,
	AddressablesImpl_UnloadSceneAsync_mB1838A7BE89F5F6A19FA82DA2E25CC675BA8824D,
	AddressablesImpl_CreateUnloadSceneWithChain_m3E90ECF466F96724C83989A3FAA6AE4E4D8921B0,
	AddressablesImpl_CreateUnloadSceneWithChain_m06EB0B565489B3F2E1460779CE3723BC95749B36,
	AddressablesImpl_InternalUnloadScene_mAB57CF105B5F4720542CC5BCFC5CA6B58309BBE0,
	AddressablesImpl_EvaluateKey_m787E940E38F9A8BEC8F9707990D9F44EE2EE110A,
	AddressablesImpl_CheckForCatalogUpdates_mAAE1AD89B3810C0921D6B8AB5F620DF5150DDCE6,
	AddressablesImpl_GetLocatorInfo_m102A3AB6F143E664CBC51978C4DFE82D244D6EF8,
	AddressablesImpl_get_CatalogsWithAvailableUpdates_mB7A297BA24FDC5C707560CAA65A7A7B2F5719F00,
	AddressablesImpl_UpdateCatalogs_mC6CB947AE102560F2DD295E62BE5CC19E17225E4,
	AddressablesImpl_Equals_m4E02BA3D94370FBA733EFEAA98D0717E355E8367,
	AddressablesImpl_GetHashCode_m127191DB805C9590D31DE79DDD951F4E2A64C8E1,
	AddressablesImpl_CleanBundleCache_mA56A81E4204F66A356F6C0CF5F9F2542A399FE15,
	AddressablesImpl_CleanBundleCache_m6F7D31738DCA8C0D9FB2314E0C163A80EAA27F16,
	AddressablesImpl_U3CTrackHandleU3Eb__71_0_m10D466DB12937F64FCD526F3A31B494E1F507519,
	AddressablesImpl_U3CAutoReleaseHandleOnCompletionU3Eb__108_0_m0FFC5C5A2651F96CE41A449C1EF87AD6AB556E96,
	NULL,
	NULL,
	ResourceLocatorInfo_get_Locator_m730E272473D7C040F4F841E8551408D7CD7972BC,
	ResourceLocatorInfo_set_Locator_m3921F8F279FEC8A513EE05B3D6CC1722250BCB15,
	ResourceLocatorInfo_get_LocalHash_m4E8BAABD41646644B4A33073BC5E3FA3C7D40A57,
	ResourceLocatorInfo_set_LocalHash_mD8D005960BD3FE6BF7A01214D5587C676AA5618D,
	ResourceLocatorInfo_get_CatalogLocation_mD0A97F02CAD8D1108EE0DD40C801EC3146EE8CE9,
	ResourceLocatorInfo_set_CatalogLocation_mC562686F535D1D3A76C1DF01D71EC3EB6F0CEF62,
	ResourceLocatorInfo_get_ContentUpdateAvailable_m2E67EC7CDC2B9CCACB53AB161E61BA56F1D69B49,
	ResourceLocatorInfo_set_ContentUpdateAvailable_m4C4F99927F6A00E29435A3DE367B0F631395395A,
	ResourceLocatorInfo__ctor_m05CFDCA03B55B515E04614A6D32E88A59FD87634,
	ResourceLocatorInfo_get_HashLocation_m06E961C27F73FFF9147F8A2E683BBDED55111B10,
	ResourceLocatorInfo_get_CanUpdateContent_m47C32C7FAB98A3099547912E7003DA5EFAC76154,
	ResourceLocatorInfo_UpdateContent_mF40872FBC49B6EEA52F973A3998296062B82BAF7,
	LoadResourceLocationKeyOp_get_DebugName_m3C99D2179177C93712843035A3661A90B0DBCB2B,
	LoadResourceLocationKeyOp_Init_mB676EA75534F2D833BCACDFA1F2184EC3662301E,
	LoadResourceLocationKeyOp_InvokeWaitForCompletion_m1F125028593FD03B58D417C63FAFE960A7E51C0D,
	LoadResourceLocationKeyOp_Execute_mAC0599205D47CE023E1743D6BC50CBD459D832BC,
	LoadResourceLocationKeyOp__ctor_mC48D04213A7FE2A6ED96385E52B85BFA418A8A6A,
	LoadResourceLocationKeysOp_get_DebugName_mC9FD4FE13F90D7A6AB8490104BE0C28E5CE66ADE,
	LoadResourceLocationKeysOp_Init_m99B3503C4D11C7F86649E59CACCC254F4AB57307,
	LoadResourceLocationKeysOp_Execute_m0F79E672AD5C55324CBC66E582C1A571A5875BA7,
	LoadResourceLocationKeysOp_InvokeWaitForCompletion_mA945C41530DB5504AE5438008D4EB902CDC6495D,
	LoadResourceLocationKeysOp__ctor_m2A34034DC186107A0F73BB776501342AF0310B96,
	U3CU3Ec__cctor_m1873F4A126FBA0084FFD04E62AA5528F65BB2C00,
	U3CU3Ec__ctor_m0793C932810B6198A138DDA2789DB1AF177D1D22,
	U3CU3Ec_U3Cget_ResourceLocatorsU3Eb__60_0_m5F6315A23819453680E3569CE35FDE9FA9BAEED6,
	U3CU3Ec_U3Cget_CatalogsWithAvailableUpdatesU3Eb__137_0_m26C99AE0476EB1A49B7CB4EE2BF14605096D41F6,
	U3CU3Ec_U3Cget_CatalogsWithAvailableUpdatesU3Eb__137_1_m0189D232CFD9C709CB273969C5C3464C294D9FE0,
	U3CU3Ec_U3CCleanBundleCacheU3Eb__141_0_m6516B17BF35843C5E1EE7D625C9FC1D822874AA2,
	U3CU3Ec__DisplayClass62_0__ctor_mD4DA14859ADF43E85776257098904A3773A7F630,
	U3CU3Ec__DisplayClass62_0_U3CRemoveResourceLocatorU3Eb__0_mB12BA0F449A00DF341FC1484F1E7C267D8C35F2A,
	U3CU3Ec__DisplayClass70_0__ctor_m20149DBB88A2836309F9F539228433DC77584197,
	U3CU3Ec__DisplayClass70_0_U3CLoadContentCatalogAsyncU3Eb__0_mA133F98E86FC76119C993003400F01E00361F0C0,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass80_0__ctor_m7CECE01B7B82857448648BCB24004A0EBA09DDAE,
	U3CU3Ec__DisplayClass80_0_U3CLoadResourceLocationsWithChainU3Eb__0_m5645E252366EF6B0B837C37AE5BA4DEA6657847A,
	U3CU3Ec__DisplayClass82_0__ctor_m441BDBBE607707EDB10A6675A917C8D2C2A8ACFE,
	U3CU3Ec__DisplayClass82_0_U3CLoadResourceLocationsWithChainU3Eb__0_m58B705F74F02F1744B5D2397AB8F129827031B7A,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass95_0__ctor_m274728CD9B2F7AF250D8700AC985BAEBC5D1CB97,
	U3CU3Ec__DisplayClass95_0_U3CGetDownloadSizeWithChainU3Eb__0_m035EF68D63AA9925D048B84B195EFBC7ED1B40D3,
	U3CU3Ec__DisplayClass96_0__ctor_m67BB5E50AC15F6ECDF23BA5B09E2CB6FF043E9FD,
	U3CU3Ec__DisplayClass96_0_U3CGetDownloadSizeWithChainU3Eb__0_mBCF6460774A428DC547B2715C03DF8804BBEF8E4,
	U3CU3Ec__DisplayClass99_0__ctor_m2AFAF95B979113A608A9510FAA4A144960F0AE7A,
	U3CU3Ec__DisplayClass99_0_U3CDownloadDependenciesAsyncWithChainU3Eb__0_mB05D2373865AF579ABC6EDBBE1F81EA210EFDD97,
	U3CU3Ec__DisplayClass103_0__ctor_m571D237F99230FF084F8F211F6E3599ECF9C3763,
	U3CU3Ec__DisplayClass103_0_U3CDownloadDependenciesAsyncWithChainU3Eb__0_m5CC694573EF8CE94052215A7921847E0DDF1558A,
	U3CU3Ec__DisplayClass105_0__ctor_mC9E819DEB639C856EE15DCF2843E26505F264206,
	U3CU3Ec__DisplayClass105_0_U3CDownloadDependenciesAsyncWithChainU3Eb__0_m7782ACA88E269B60766CFE61C58B19618A2DB166,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass112_0__ctor_m16342634513EE3E5B417B5424045434825BC15ED,
	U3CU3Ec__DisplayClass112_0_U3CClearDependencyCacheAsyncU3Eb__0_m4460C657CA6608E427473FA77548AC6795CFB0C4,
	U3CU3Ec__DisplayClass113_0__ctor_m28DA08AABE09774767186AEFDC1F3E90947F4712,
	U3CU3Ec__DisplayClass113_0_U3CClearDependencyCacheAsyncU3Eb__0_mF9B0D4381711F7B5BBDDE6C1902FD9AC3D87975C,
	U3CU3Ec__DisplayClass114_0__ctor_m696EFB1877F3ACE37732E16C357A4A09DF9C66E8,
	U3CU3Ec__DisplayClass114_0_U3CClearDependencyCacheAsyncU3Eb__0_mBBD1442C40B78223EA490D654F3A51A2FF83681B,
	U3CU3Ec__DisplayClass119_0__ctor_m12831188ACFF28D2CE4435364F7661AD7D632D0A,
	U3CU3Ec__DisplayClass119_0_U3CInstantiateWithChainU3Eb__0_mB92058EEEBD5609B5EE537726EE475E50FB0DE30,
	U3CU3Ec__DisplayClass121_0__ctor_m68E05C232B16EF14AD7FB6721CC7127A1F4E5D75,
	U3CU3Ec__DisplayClass121_0_U3CInstantiateWithChainU3Eb__0_mC898C32386319181928454F36E15717D2C95E51B,
	U3CU3Ec__DisplayClass124_0__ctor_m89777E267B856F2A4BF17E10E758A67069E1271B,
	U3CU3Ec__DisplayClass124_0_U3CLoadSceneWithChainU3Eb__0_m966AA1153A14CF7822BB164C4050E71332BB39C8,
	U3CU3Ec__DisplayClass130_0__ctor_m3FFF77BA900E8025B586A63E8799CA5BB89468C7,
	U3CU3Ec__DisplayClass130_0_U3CCreateUnloadSceneWithChainU3Eb__0_mA13502A156C351CF8264F36090FE1520BA2E4ADD,
	U3CU3Ec__DisplayClass131_0__ctor_m31B13597367815C8A07D77EE8ED8C5A1168C3CF3,
	U3CU3Ec__DisplayClass131_0_U3CCreateUnloadSceneWithChainU3Eb__0_m4BD0640390D16FBE17EE4826E2F6630033347EEE,
	U3CU3Ec__DisplayClass138_0__ctor_m9C050EE8C1974EE3D54B4F7C51D8077A7C3AAB1B,
	U3CU3Ec__DisplayClass138_0_U3CUpdateCatalogsU3Eb__0_mCD7158FE5C76D25FF276919403E2039E3A09E276,
	AssetLabelReference_get_labelString_m50D0391EBEB48D27931552DCF414D193826C036A,
	AssetLabelReference_set_labelString_m3F45F368E74F1E08A870E03C58D2EC86AEED7D5C,
	AssetLabelReference_get_RuntimeKey_m12853735396918F9D95730708B2DA9AA0CB836E4,
	AssetLabelReference_RuntimeKeyIsValid_m2C39EC7D33AFED52BA6061DEFCE9D58C42D7B4FB,
	AssetLabelReference_GetHashCode_m7D4FAAB50F4D3192CDF02EBE0E8CF6FABF32752A,
	AssetLabelReference__ctor_m4804B7C444E9A02C2E50E22B4EE92E5CC99089F6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AssetReferenceGameObject__ctor_mB4776F9B8496516169070EDBCE3BADCCA8450A6D,
	AssetReferenceTexture__ctor_mFF94A2C32A07209D870FB8DE9EE4C7D37C7D9598,
	AssetReferenceTexture2D__ctor_m44EC43BF4CDBF0D033D63338D4F2ABA7D7E66725,
	AssetReferenceTexture3D__ctor_m10345FF060C3FD55A034AD09F01705B270DD6D6F,
	AssetReferenceSprite__ctor_m5BB0B8B036649204BA33C0A72262868D00C92F67,
	AssetReferenceSprite_ValidateAsset_mFE52D04BEE4E4B33584EDECE7AF4054F9E313246,
	AssetReferenceAtlasedSprite__ctor_m8F513BC363CC9E6A8907BB8333AC91495CBAF393,
	AssetReferenceAtlasedSprite_ValidateAsset_mE38720BE580A06C9D813F8B04B490E5C4530A8DA,
	AssetReferenceAtlasedSprite_ValidateAsset_mA579F68BF61B2A0DEFC52674D18D3F19DBA3FD18,
	AssetReference_get_OperationHandle_mB4BAAB57C9A519BDDA6E2308BA157FF8929728AF,
	AssetReference_get_RuntimeKey_mFFB92EE1592D1CADC55291597F24C8AC2585555A,
	AssetReference_get_AssetGUID_mA1783241545A64F4988B844A83EFA6B477C4E723,
	AssetReference_get_SubObjectName_mBDCF0C4180464DC53D890212CFB4132DA8888AA1,
	AssetReference_set_SubObjectName_mD9D7B8E2F9522A9D6571CED8ABA3B580820D7E69,
	AssetReference_get_SubOjbectType_m39E6BE620D15FD77DE37F9C1B3DD8606C304DAB9,
	AssetReference_IsValid_m5D83F03F369F0ACBB7355911C660F4A9C350F4F8,
	AssetReference_get_IsDone_m0659579AE490C8B0F57F1A9641C3FFB88336BB9F,
	AssetReference__ctor_mE38B54E67E5FC625BA69DC50A8DABE0FE3422031,
	AssetReference__ctor_mC3A12AE436F1FD5C2E5379F96D7CD651B35BCD46,
	AssetReference_get_Asset_mF316D0153DAB80E3593997C854E96A21B80D092C,
	AssetReference_ToString_m86D7865D2BCA234413BE43C7EFF3A0F69C9D6617,
	NULL,
	NULL,
	AssetReference_LoadScene_m6AFBBDBF59A4BA4DC807CA75047119D9693C1D6F,
	AssetReference_Instantiate_mC320C78B3EB3684BE0CF806FDAF041174F77E11F,
	AssetReference_Instantiate_m5B54B1EA245D1216147D849C2F837B413ADF4379,
	NULL,
	AssetReference_LoadSceneAsync_m774FE932EC09AA91127BE50A14C26D970B44ABB2,
	AssetReference_UnLoadScene_m5D3FC5DD78B5533A4D10EA16436914A105C4C1F8,
	AssetReference_InstantiateAsync_m639FCE542FB03D1F21D46380FAB6D15E5F2D5AF7,
	AssetReference_InstantiateAsync_m483670275A9C6E31E8DF1D57FB24BE0E3C4BCB26,
	AssetReference_RuntimeKeyIsValid_m6D9D44A2DD0ABE262765A3565651357113226BEF,
	AssetReference_ReleaseAsset_m969FC5ED646CBEAC2C830C66DC44E196577C7A46,
	AssetReference_ReleaseInstance_mF4B27017208E6319472B7E6F059B238E547291C1,
	AssetReference_ValidateAsset_m35FB5E9DFF3569B2AC42293F88E5B9CA4E384807,
	AssetReference_ValidateAsset_m0901C2C1CD6F19A6C9B1AFD4F7479A4B7C51FAC7,
	NULL,
	NULL,
	CheckCatalogsOperation__ctor_m7FC966B7C226BB5069AEE70FED6DDB16A5C53F2F,
	CheckCatalogsOperation_Start_mA40B6D4597ED04E99745F48865533E54D774A9D2,
	CheckCatalogsOperation_InvokeWaitForCompletion_mF9A9E1FC79FFB4080ED623B498EB0628D9BE5980,
	CheckCatalogsOperation_Destroy_mF67296A68840B3A77713636A6FA63D82B7D845AE,
	CheckCatalogsOperation_GetDependencies_m5EDB01084C3BBBD8D9C1716CB9654BBDE8413FC0,
	CheckCatalogsOperation_ProcessDependentOpResults_m194CD9D40988357CB47BB2C3C43BDFDA89FA360B,
	CheckCatalogsOperation_Execute_m33B0C6478586D57273E4BE93263F398162165358,
	U3CU3Ec__cctor_m81E1395B6796338DB61527B69B0AEE8736B982D2,
	U3CU3Ec__ctor_m6E6B1522F10F8BAC1ECCA1052E3B1AB15A97A841,
	U3CU3Ec_U3CStartU3Eb__5_0_m80140E81FA504FD96B3E75ECD80F859C90F5595E,
	CleanBundleCacheOperation__ctor_mD74612701243658021F6F1C91692C695C08D4C78,
	CleanBundleCacheOperation_Start_mE70204E1127D73E8D8987D5D3B0FCDAB786410B6,
	CleanBundleCacheOperation_CompleteInternal_m5E03B8B322A22792C96A9A97E471ED7B3DFD3C2B,
	CleanBundleCacheOperation_InvokeWaitForCompletion_m3B822E3B915A0DDDA1CEE158160F4CBA0242FE24,
	CleanBundleCacheOperation_Destroy_m7B5990431E9EF4B587609C5E42FEAA4EFC43EA6D,
	CleanBundleCacheOperation_GetDependencies_m904C7D5BD95A2DCD6B865980264CB2B84F0FA9DB,
	CleanBundleCacheOperation_Execute_m8341F081883459C23AB968CD881EB09D8F32C410,
	CleanBundleCacheOperation_UnityEngine_ResourceManagement_IUpdateReceiver_Update_m8A2EBE27CB8872A104278F25001993E3EFC93CA8,
	CleanBundleCacheOperation_RemoveCacheEntries_mDF863BE4126498457703B1ABEE32076E515CE061,
	CleanBundleCacheOperation_DetermineCacheDirsNotInUse_m581EF6B004281F5A332A376343B704B24D57BE13,
	CleanBundleCacheOperation_GetCacheDirsInUse_mED52DCD562381F6D44B51329BC0828C24D2C0AD9,
	UpdateCatalogsOperation__ctor_m980CB3CCA2DD2B7EA1C9EEDF2276D93C07AA383A,
	UpdateCatalogsOperation_Start_m6ACA9C832974D47EE94C5C8B4E44E469E092D1DD,
	UpdateCatalogsOperation_InvokeWaitForCompletion_mE1353B05726EAA36074AD56087804D935B953F3D,
	UpdateCatalogsOperation_Destroy_m0124A4CD4F734E1935B5AB11D7A4CB9AA9C19699,
	UpdateCatalogsOperation_GetDependencies_mC0A2567771A8CEAEA491380EF208414E4BED2D16,
	UpdateCatalogsOperation_Execute_mDF70F3ED39ECE6E0F88559689DAFA1CCF3931753,
	UpdateCatalogsOperation_OnCleanCacheCompleted_m0985D2E9A899D1D456481940DBA2871DE7E0A20D,
	U3CU3Ec__cctor_m79E6CA846BD2DB473F975AE7020D35607A210E3E,
	U3CU3Ec__ctor_m396385D48E372D649B0A8F408E6B3747A588CC0C,
	U3CU3Ec_U3CStartU3Eb__6_0_m94EEEF78F0686E0458654FC58F688EA31D8E375D,
	U3CU3Ec__DisplayClass11_0__ctor_mB2460F0D47EC265545C51A29FF2EA4CA745D9E88,
	U3CU3Ec__DisplayClass11_0_U3COnCleanCacheCompletedU3Eb__0_mB0C710EDF34DB43437A1027545BBCA01BE7203AF,
	DynamicResourceLocator_get_LocatorId_m68F756B076B6AA037CDE8B7CE2F77E0F79D7AD6B,
	DynamicResourceLocator_get_Keys_m2C36F8723D7F4579A9430E921EFDB734F9B7C1EA,
	DynamicResourceLocator_get_AtlasSpriteProviderId_mBE84D197DDBB70DD7CD48EB2427D3EB5A99A44EA,
	DynamicResourceLocator__ctor_m7C978206B096517078313B8D47F18E4EDF396AFA,
	DynamicResourceLocator_Locate_m33B8ADFDE9BB6B4CC3598F914C20B5A5D31662C9,
	DynamicResourceLocator_CreateDynamicLocations_m96D22BC6E0D6817D093C312C4BA99F098A83FE9A,
	PlatformMappingService_GetAddressablesPlatformInternal_m97FC778BFAA18E1D123FDC738A0C717531F908E2,
	PlatformMappingService_GetAddressablesPlatformPathInternal_mD2265CEDCBD642E769E448BB788B8148A3E39937,
	PlatformMappingService_GetPlatform_m640E7D9426E0615A3DC3D5436A0CDBB6DBDDFB50,
	PlatformMappingService_GetPlatformPathSubFolder_mE2C6054C0C1E6FB819AC41E4BE42427E76693554,
	PlatformMappingService__ctor_mD3E14BB80434644C46DC1B92BBA4E028F4D04621,
	PlatformMappingService__cctor_m103FA01AB13C412E6146247EB838BDF9AFCC2D5A,
	DiagnosticInfo_CreateEvent_mED2A79D2E7203810FDB447FBCA56B01EBAAF0605,
	DiagnosticInfo__ctor_mF8DC31D77D60B56C86A0C10FB3A6DBAC1F68EDFA,
	ResourceManagerDiagnostics__ctor_mD87E45FC905706C92E838A9713F638677650AD98,
	ResourceManagerDiagnostics_SumDependencyNameHashCodes_m11A34B0DBCCF159DEB73303445C038BBAB4A564C,
	ResourceManagerDiagnostics_CalculateHashCode_mA02167B20B3C7563BC71D4D2F7C3D6EAAADE1D52,
	ResourceManagerDiagnostics_CalculateCompletedOperationHashcode_m84D150B1B01FB4CD6E06AFC1BBF25684770DCB71,
	ResourceManagerDiagnostics_GenerateCompletedOperationDisplayName_mBFDE570A0ABB5D226567C9A1796A055BF468C044,
	ResourceManagerDiagnostics_OnResourceManagerDiagnosticEvent_mAA092F781B041FA18E1E76C2080E49F0688B05AA,
	ResourceManagerDiagnostics_Dispose_mA44A97DBF2724CF358F4DDCF8BFB73E7B65E345A,
	SerializationUtilities_ReadInt32FromByteArray_m7208EB75D52FBA8050C4242E5804121150BD163E,
	SerializationUtilities_WriteInt32ToByteArray_m62347A957BACBFBC3C6B45F27DE5A14B92C1130D,
	SerializationUtilities_ReadObjectFromByteArray_m25B6A5CF707F49ED4A89581E08CB0B411A7E8736,
	SerializationUtilities_WriteObjectToByteList_mE1FCA90B8FB3045812E47C7C3E8F2B4AC670AB8D,
	ContentCatalogProvider__ctor_m99CF9FAED9037A9DF4C56F29008588003553F33B,
	ContentCatalogProvider_Release_m14251E05057F6755043D9C6B7BFB9ADFCA0C1F3A,
	ContentCatalogProvider_Provide_m310BF05B17183E5D6DC6232B0A5720E6EBBD90FB,
	InternalOp_Start_mBA319941DAF93A1B6F10D648AFC976C90355FC1B,
	InternalOp_WaitForCompletionCallback_mB65317CF60A9D44F17F346D2F728B49E86BC7E43,
	InternalOp_Release_mF75964C2A20590986E276485E0623291163CCBF7,
	InternalOp_CanLoadCatalogFromBundle_m81E0C2D0483BC7C657F02CE3F683130B6FDE4402,
	InternalOp_LoadCatalog_m3BD2EA9695428117D8183A87619A4F00D533ABED,
	InternalOp_CatalogLoadOpCompleteCallback_mF540E9E36BE81C8EEDEBE651ADC13221DD4D2BDA,
	InternalOp_GetTransformedInternalId_m2B79066D18B1CA0A43625A07FAD8C2E46F4DCFCA,
	InternalOp_DetermineIdToLoad_m22719600717614A82404ED8517863E294B73E894,
	InternalOp_OnCatalogLoaded_mDCCCFFD35BF36D018065687D5478D71959A03064,
	InternalOp__ctor_m70A31EAA81C793A72A33540F50B464140510D827,
	InternalOp_U3CLoadCatalogU3Eb__14_0_m386E5D96E7E40EC21121029208C75C673E98914E,
	BundledCatalog_add_OnLoaded_m89F013E1D4A53A2F3E06242E60457E2759B70D99,
	BundledCatalog_remove_OnLoaded_m8CFABBF3864BF236E48FB319D73BD47A38E3BBBC,
	BundledCatalog_get_OpInProgress_mC5ECE605A75AD026A6D1F67B6E605EF09CB5818D,
	BundledCatalog_get_OpIsSuccess_m3DF65BA8CF1F5002982EA14631DCC9FB23B5F85F,
	BundledCatalog__ctor_m77CAF3A45BDB4D627B27B3AEFFA114C18957F9B9,
	BundledCatalog_Finalize_mFA2BAB8D3568564FDD74C7EBEA72A28C19C80E98,
	BundledCatalog_Unload_m528F5870F82A7655283EF0628DFE7CEB1A7BEC9C,
	BundledCatalog_LoadCatalogFromBundleAsync_m2638C265C422736FFE49C19D0C5061ECAFA8663B,
	BundledCatalog_WebRequestOperationCompleted_mF5355237E02D42B314B8C0BAF208D912C1FB73BF,
	BundledCatalog_LoadTextAssetRequestComplete_m7A2DB37A2EA178267AAEFD928DAC0B277623746A,
	BundledCatalog_WaitForCompletion_m707B9D2DAB28FB1CD2686CF91320710FCFC5F951,
	BundledCatalog_U3CLoadCatalogFromBundleAsyncU3Eb__19_1_m7D79920FA26E4A39734963B744101D0BF3B626DB,
	BundledCatalog_U3CLoadCatalogFromBundleAsyncU3Eb__19_0_m5C73C7830DD18FE551D3F4AFBE83472522444BAC,
	ContentCatalogDataEntry_get_InternalId_m88B81554FCA77225587E72F774FE251B518E7741,
	ContentCatalogDataEntry_set_InternalId_m027ED3747C84D1E55ABA4635D1D515E819A79A6B,
	ContentCatalogDataEntry_get_Provider_m2CFE124B48280AA5498AA25E71C11A7BC0532F64,
	ContentCatalogDataEntry_set_Provider_mA02C4D8225B1DE0DCE543BF8449373BCC0184F4B,
	ContentCatalogDataEntry_get_Keys_m6244DF2B99A239706E6BBA15734D1990F25F7A94,
	ContentCatalogDataEntry_set_Keys_m4CC317F6F88E77D3873B3C3EDE493DD4E772CE09,
	ContentCatalogDataEntry_get_Dependencies_mC61A64CB8C2449B553A51FFC35B6638A036F087D,
	ContentCatalogDataEntry_set_Dependencies_m2FE695C51C371E338674CB04541EDE141C679041,
	ContentCatalogDataEntry_get_Data_m680BC7499C1CCA6907A677F8B2A7109E4A90706D,
	ContentCatalogDataEntry_set_Data_mE68BADE0ADEDD06B4BE919EC716E5E39B83D96E9,
	ContentCatalogDataEntry_get_ResourceType_m63B916D24760620D0433B3E23D1E1E494FCCE671,
	ContentCatalogDataEntry_set_ResourceType_m9FC6D88B5E237906D5D9EBCADAE179C4FC430147,
	ContentCatalogDataEntry__ctor_m6F9DEDFFC197BDC93E6F3C8081237EA76F042FFC,
	ContentCatalogData_get_ProviderId_mF4F3B98EF5B864883E687CCB2EF81C7418B65A4F,
	ContentCatalogData_set_ProviderId_mAD7599F3376242D73B9AF1B889707C9D2A7741FC,
	ContentCatalogData_get_InstanceProviderData_m1C8F75FC83CA8BCAAD687DCC3DA7A03D27C1181C,
	ContentCatalogData_set_InstanceProviderData_m6EEBE5EB6281CB3C0E68A26F573D499C36E3222A,
	ContentCatalogData_get_SceneProviderData_m5BC5272A27857E6603B0EA4D2901BBE3BA6CEDBA,
	ContentCatalogData_set_SceneProviderData_m8396F8999AFE0099A3199EEE8824C8ECB89613E9,
	ContentCatalogData_get_ResourceProviderData_m0743A4E670303C0CFA9ECC27C6BEF3DA4760DBD0,
	ContentCatalogData_set_ResourceProviderData_mB8284C2E422B0FB2BBCDDF54C0C6BCA4D8D1E31C,
	ContentCatalogData_get_ProviderIds_m5096D0A3509057E74245060BF0D0FDCEF95B8924,
	ContentCatalogData_get_InternalIds_m94EEF66683216658704396B9B9538DFE9CB3220D,
	ContentCatalogData_CleanData_mE00F4DE80C610B0416004EEB6A3C012648A79C0C,
	ContentCatalogData_CreateCustomLocator_m5911B4CBFB08928BB221DC6BC7E1C0BA90EA7EA3,
	ContentCatalogData_CreateLocator_mA934DD4D237BB91D8A9D89F399E49176F7B04358,
	ContentCatalogData_ExpandInternalId_mC99623AABA1FFCBA39679DA3ED4FCAC080E52058,
	ContentCatalogData__ctor_m0B697DAC27F186A36AAEA0ED393A788054F7794E,
	CompactLocation_get_InternalId_m738E3539D54523E8C6D22C3AEA7B853A1B5214C7,
	CompactLocation_get_ProviderId_m5B9901D6A2D455414B183CF944FE9BDC69215F9B,
	CompactLocation_get_Dependencies_m05DB0C02FC5363E09A81F5A80E99DD789FE8D8CD,
	CompactLocation_get_HasDependencies_mA792160B6F43E12D5EBB2508CFCBF59F2C713FAD,
	CompactLocation_get_DependencyHashCode_m99A9EF38CD8B8056BDE9D328C78C14208FA31ECD,
	CompactLocation_get_Data_mE7B41058311E50BE1A2804C552AB483FD58F6B74,
	CompactLocation_get_PrimaryKey_mB6C7C918BC79E2160CFE6C24CA65E2A13DEF4E92,
	CompactLocation_set_PrimaryKey_mBE1F165D33633409D968F188031605C0A7248B8F,
	CompactLocation_get_ResourceType_mE0D0AC4437BBDF1A91FA45E119AF175C7C7090D6,
	CompactLocation_ToString_mE2FE2F271E201F10E002144D5CB9E235D1B53489,
	CompactLocation_Hash_m7BC3BD16DE3F6111E3E783BF8C77AAC5B0D253E8,
	CompactLocation__ctor_m886D60D95BC695AC34D4BE28EF7F428AB03B4B0B,
	NULL,
	NULL,
	NULL,
	LegacyResourcesLocator_Locate_m30FFAE3B0B1FD2B5D69C73514DD58960411CBBF2,
	LegacyResourcesLocator_get_Keys_mE9F6997869B6D2BED7AD49BFD5EA614D8E3F491E,
	LegacyResourcesLocator_get_LocatorId_mAD5A28CBCD892CF1DC71A0BC88723039CC30B3AD,
	LegacyResourcesLocator__ctor_m7E2F2B3EE4D19CE907033538381CF6B0AB3A2ED4,
	ResourceLocationData_get_Keys_m2C1E479B14E5470CBAD96FF11E2062068C025A9F,
	ResourceLocationData_get_InternalId_mD05C523F4F7B8F22F3C253A15A11A4EBA2334025,
	ResourceLocationData_get_Provider_mD8517600C7A7BF4EC16D266EC06C53A657D2DB48,
	ResourceLocationData_get_Dependencies_mC034D349982CD293D110712DAE4017DE072C11CA,
	ResourceLocationData_get_ResourceType_mA9B848722D46B61E2AE989746375061F44638BEC,
	ResourceLocationData_get_Data_m49C40E3635E2B0B902F2A629720E807961C5A711,
	ResourceLocationData_set_Data_m5AC6BEC5BB8A3D8F1228D4130306AA2D4E810C25,
	ResourceLocationData__ctor_m692150BBC70638AA080BE88A52DD33F0D4153DAF,
	ResourceLocationMap__ctor_m42536CDA89B99C6E15A735B8967B7EDE334B641A,
	ResourceLocationMap_get_LocatorId_m6DD3DEA27A3669D54790577849F312962B7F20F6,
	ResourceLocationMap_set_LocatorId_m981702812D893BAA110F890F9BF7567CBE921E10,
	ResourceLocationMap__ctor_mA84AB49C3978999C3104A4FC013FA23183D13FAE,
	ResourceLocationMap_get_Locations_m103289EC8AF326CF3456FC27B38650D17DD3B4EB,
	ResourceLocationMap_set_Locations_m8A1C936292C144B061EB196B54E43F581F13E823,
	ResourceLocationMap_get_Keys_m20284F66B83205D4118A47357012DCB424EB5BCE,
	ResourceLocationMap_Locate_m0FD3ED42E565211E352D8A71FF30F02BA0BCFE2B,
	ResourceLocationMap_Add_m1F644466D3C8AEE69DF0CE77936E5380AEC03A30,
	ResourceLocationMap_Add_mA31AF37A4F023C5052A483F39E4F457C348AC6C5,
	AddressablesRuntimeProperties_GetAssemblies_mD0DF89F46D215F9E86193E89715CFFF86E893B90,
	AddressablesRuntimeProperties_GetCachedValueCount_mE4DD844E61291321DB37092770349B0C48B61BCC,
	AddressablesRuntimeProperties_SetPropertyValue_m7B873826B0E67BE8809A38737C58EB6E24519D7B,
	AddressablesRuntimeProperties_ClearCachedPropertyValues_m4E61DE517278ED567089426FBF94805005FAE89D,
	AddressablesRuntimeProperties_EvaluateProperty_mC9DE5E9EB3D52F162BDD3F4453DB10D69C07813E,
	AddressablesRuntimeProperties_EvaluateString_m09D987F1662FBF58992BED59F2B25A7F26309057,
	AddressablesRuntimeProperties_EvaluateString_m1E58500E7512870A9906BBAADA7E16BC912E85EE,
	AddressablesRuntimeProperties__cctor_m81C596E4ADACBBB4F648DC4FC8C6FC959142D402,
	CacheInitialization_Initialize_m043176BE3308606AE0EFD34AA3454A7C1B7DB408,
	CacheInitialization_InitializeAsync_m1FB035B45681C362143C803D7C7BAAFDAF774DBD,
	CacheInitialization_get_RootPath_m4BDB173F10EC89E65C019A13B092030051675180,
	CacheInitialization__ctor_m15147548976A6A48265C7ED182871EB4E67A74B5,
	CacheInitOp_Init_m45BB077C83F6B0EA8829CE0B7ACFA5E3B86FE22D,
	CacheInitOp_InvokeWaitForCompletion_m39DDE30A04FE93540B346DCF56A13F0E14C68A51,
	CacheInitOp_Update_m70AE29B18FFD5183A73802FFC2FFB9A912E82F3D,
	CacheInitOp_Execute_m57E6222D65EC5A94AF80329D11CAB676DC630640,
	CacheInitOp__ctor_mFCB55DF57D9B371B76D833EF097C4DCFC7743575,
	U3CU3Ec__DisplayClass1_0__ctor_mDEF17A6EEEC394BD845B922C6679566D70A37DDC,
	U3CU3Ec__DisplayClass1_0_U3CInitializeAsyncU3Eb__0_m4D58B4B3BB1941E794A9799EE4588AE536783C82,
	CacheInitializationData_get_CompressionEnabled_mDE7727B28A390E559AEA0C5BF1BB5F26F02B7F47,
	CacheInitializationData_set_CompressionEnabled_mF13BEE13E4DD5A92ED24E92E7A758348F99D59DB,
	CacheInitializationData_get_CacheDirectoryOverride_m8C1C5A96FA4DA1F2867571217C9CD5ABFD8E85A4,
	CacheInitializationData_set_CacheDirectoryOverride_m7DF8064B3302A5D971CF03274FF06DFF7257F44E,
	CacheInitializationData_get_ExpirationDelay_mF2E8EF7956621254B0D67C1D4C623B285943A680,
	CacheInitializationData_set_ExpirationDelay_mD797B793BF96A63E666B628823B92B2B58BD0330,
	CacheInitializationData_get_LimitCacheSize_mF3811D5468607D532A9D711366EB4157DAEF1911,
	CacheInitializationData_set_LimitCacheSize_mB56ED15D41C914A6FA73C779738727C9B60090F0,
	CacheInitializationData_get_MaximumCacheSize_m693786CE02BF1B8FE03277A4A5ED0A7719D807CB,
	CacheInitializationData_set_MaximumCacheSize_mE5A6F0ECD75ADDDA3CCDACA4A3E83C612B815275,
	CacheInitializationData__ctor_m49D8D33FCA342CB6460244FF627D04F76DEC4E86,
	InitializationOperation__ctor_mF0FFB347D76F1B58CF52F27321536C0A080FF39B,
	InitializationOperation_get_Progress_mE204B2808FA40DBC034B0175DA849611F24A4C07,
	InitializationOperation_get_DebugName_m6CC0A154DE6DDE764F5BB9EAD40F7A564D9AA798,
	InitializationOperation_CreateInitializationOperation_mEC2364AD89CA770A2BE2AFE369AF2A4F760F646A,
	InitializationOperation_InvokeWaitForCompletion_mB02865B5F0CB2DB49725182A0DC0800E92D36E7E,
	InitializationOperation_Execute_mF086EECAA0F8C6C4FB7A2A3ED14D832C73AA76A7,
	InitializationOperation_LoadProvider_mF024312382D0DB0966B01C4C3CBACD955347492D,
	InitializationOperation_OnCatalogDataLoaded_mD6710C04B2A6CF604D5C54C952A4D6191E4775FB,
	InitializationOperation_LoadContentCatalog_mFF2FC5C623E9D55A23A608B472D72FF7A69B3CE9,
	InitializationOperation_LoadContentCatalog_mE21AEFB03C7143832DBE3328BC0C3E9DC1BD5C80,
	InitializationOperation_LoadContentCatalogInternal_m2143386F987003C115BD67D981CE294723DC687E,
	InitializationOperation_LoadOpComplete_m7EBD01DE5D7E2B89F1E3A813D05B4EA727060145,
	U3CU3Ec__cctor_mB573B3CE8F105B13D3C3F1775297C6804B43245A,
	U3CU3Ec__ctor_m5E93AA74E4E35D426421AE2D7C566873D1F45269,
	U3CU3Ec_U3CExecuteU3Eb__13_0_mB531A590C24637E1CDD4817909CE0C3C408A82CA,
	U3CU3Ec__DisplayClass16_0__ctor_m42F97EEDB0571819ED4D155A48BF0B16E38687E2,
	U3CU3Ec__DisplayClass16_0_U3CLoadContentCatalogU3Eb__0_mA4DBBD2AF81C0923F10873BBA6AAE509250046FA,
	U3CU3Ec__DisplayClass18_0__ctor_mAB8FC8B6AED17CDF499D0426A80E5E63F76353C1,
	U3CU3Ec__DisplayClass18_0_U3CLoadContentCatalogInternalU3Eb__0_m038FD7BBA1EB254F8E6BB5F90F8F19B3BAA97B30,
	ResourceManagerRuntimeData_get_BuildTarget_mA086F9E8833B970B4CC2CA8EA451943A7916C887,
	ResourceManagerRuntimeData_set_BuildTarget_m9A14BF8D8F9B34503BE283BE235051F7FE8421B6,
	ResourceManagerRuntimeData_get_SettingsHash_m993577DFF8C02D492A19984C6C9D44DF3A66DF32,
	ResourceManagerRuntimeData_set_SettingsHash_mEB2C6A6C6660C552EA56B78DCD8AC5E23A803FF2,
	ResourceManagerRuntimeData_get_CatalogLocations_mF0A230795FB77AFE64D8A49266F267D207A3FCD4,
	ResourceManagerRuntimeData_get_ProfileEvents_mEDDD57F70E4338C90E8A8903C704DFA29E647A04,
	ResourceManagerRuntimeData_set_ProfileEvents_m497E71165681D9A23C73D18361C0650A85766D68,
	ResourceManagerRuntimeData_get_LogResourceManagerExceptions_mE2744B0993B3504853CD2927FDA2D043DABB7DE6,
	ResourceManagerRuntimeData_set_LogResourceManagerExceptions_mFB0E1C31E6AA24282F9463124B5DB9A5BD5A2063,
	ResourceManagerRuntimeData_get_InitializationObjects_m33078507C6F754A39577BCC08D6E6E82EF89D612,
	ResourceManagerRuntimeData_get_DisableCatalogUpdateOnStartup_mD20A121032896B85CE68D51220D7329091AB5D5D,
	ResourceManagerRuntimeData_set_DisableCatalogUpdateOnStartup_m464691EC09EB18A64A47367B15EBE2A9F4C65BA0,
	ResourceManagerRuntimeData_get_IsLocalCatalogInBundle_mEB8FB9130B8D318B17E24254803A9879608DDE15,
	ResourceManagerRuntimeData_set_IsLocalCatalogInBundle_m611A294DBBAA9B21FF48E483EA2054C469EB4060,
	ResourceManagerRuntimeData_get_CertificateHandlerType_mE5E59C482B4A8ADB7415F454D8A6507CFE5CDAC5,
	ResourceManagerRuntimeData_set_CertificateHandlerType_m3AB31801B759EF5C15A054963E7F8A074099CB2A,
	ResourceManagerRuntimeData_get_AddressablesVersion_m191AD04B6D8F94ECC1A3A86146244E577A7ACE2A,
	ResourceManagerRuntimeData_set_AddressablesVersion_m5CAEBE23916D1E108E7A083B2D2AA5A1EB332BB3,
	ResourceManagerRuntimeData_get_MaxConcurrentWebRequests_m1EA06475277D89F221D1E541570A1A6477BAE902,
	ResourceManagerRuntimeData_set_MaxConcurrentWebRequests_m8CA9A51936EB303138A3E69BE39021A18A9D2DE4,
	ResourceManagerRuntimeData_get_CatalogRequestsTimeout_mD2FCE622A84649C5189D997FAC59ACB4DF5CF3ED,
	ResourceManagerRuntimeData_set_CatalogRequestsTimeout_m541A0A437071BCF23BF7638CA0C3CAFD281AF23B,
	ResourceManagerRuntimeData__ctor_mEA8C0EEF77BB53654E36D8F9697F068A27E42D69,
};
extern void RuntimeBuildLog__ctor_m014EB6EEDCCB7F6A3DF6F5176829414750CC3D21_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x06000004, RuntimeBuildLog__ctor_m014EB6EEDCCB7F6A3DF6F5176829414750CC3D21_AdjustorThunk },
};
static const int32_t s_InvokerIndices[597] = 
{
	14,
	26,
	23,
	62,
	9,
	9,
	23,
	26,
	9,
	9,
	14,
	2583,
	14,
	9,
	89,
	23,
	23,
	2584,
	14,
	26,
	14,
	26,
	2585,
	26,
	27,
	118,
	23,
	26,
	27,
	111,
	14,
	4,
	4,
	4,
	4,
	0,
	4,
	154,
	4,
	154,
	4,
	4,
	4,
	4,
	4,
	373,
	501,
	154,
	137,
	154,
	137,
	154,
	2586,
	154,
	137,
	2587,
	2587,
	2588,
	2589,
	2589,
	2590,
	2587,
	-1,
	-1,
	-1,
	-1,
	2591,
	2591,
	2591,
	2592,
	2592,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2593,
	114,
	2594,
	2595,
	2596,
	2596,
	2596,
	2596,
	2596,
	2597,
	2598,
	2598,
	2599,
	2599,
	154,
	154,
	154,
	154,
	154,
	2600,
	2600,
	2600,
	2600,
	2600,
	2601,
	2602,
	2601,
	2602,
	2603,
	2603,
	2601,
	2602,
	2601,
	2602,
	2603,
	2603,
	2604,
	2604,
	2604,
	2604,
	2605,
	2606,
	2607,
	2608,
	2609,
	2610,
	2605,
	2606,
	2607,
	2611,
	2612,
	2613,
	186,
	154,
	3,
	2614,
	3,
	14,
	26,
	14,
	10,
	32,
	10,
	10,
	26,
	23,
	14,
	26,
	14,
	26,
	2451,
	89,
	2450,
	14,
	14,
	14,
	14,
	26,
	27,
	26,
	27,
	26,
	2615,
	26,
	27,
	28,
	14,
	197,
	26,
	23,
	2616,
	2617,
	2618,
	2619,
	2620,
	105,
	2621,
	2622,
	-1,
	2623,
	23,
	-1,
	-1,
	-1,
	2624,
	2625,
	2626,
	2627,
	-1,
	-1,
	-1,
	-1,
	-1,
	2444,
	2444,
	2444,
	-1,
	-1,
	2444,
	2628,
	2628,
	2629,
	2629,
	2630,
	154,
	0,
	2631,
	2630,
	2631,
	2632,
	2633,
	9,
	2444,
	-1,
	-1,
	-1,
	2634,
	2634,
	2634,
	2635,
	2636,
	2635,
	2636,
	2637,
	2638,
	2637,
	2638,
	9,
	2639,
	2640,
	2640,
	2641,
	2642,
	2643,
	2642,
	2643,
	2643,
	28,
	2644,
	28,
	14,
	2645,
	90,
	112,
	2646,
	2647,
	2648,
	2444,
	-1,
	-1,
	14,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	197,
	14,
	89,
	197,
	14,
	197,
	89,
	23,
	23,
	14,
	115,
	23,
	89,
	23,
	3,
	23,
	28,
	9,
	28,
	28,
	23,
	9,
	23,
	2649,
	-1,
	-1,
	23,
	2650,
	23,
	2650,
	-1,
	-1,
	-1,
	-1,
	23,
	2651,
	23,
	2651,
	23,
	2652,
	23,
	2652,
	23,
	2652,
	-1,
	-1,
	23,
	2653,
	23,
	2653,
	23,
	2653,
	23,
	2654,
	23,
	2654,
	23,
	2655,
	23,
	2655,
	23,
	2622,
	23,
	2656,
	14,
	26,
	14,
	89,
	10,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	26,
	26,
	26,
	9,
	26,
	9,
	9,
	2451,
	14,
	14,
	14,
	26,
	14,
	89,
	89,
	23,
	26,
	14,
	14,
	-1,
	-1,
	2657,
	2658,
	2659,
	-1,
	2660,
	2657,
	2658,
	2659,
	89,
	23,
	26,
	9,
	9,
	14,
	89,
	26,
	2661,
	89,
	23,
	26,
	2662,
	23,
	3,
	23,
	9,
	26,
	2647,
	1304,
	89,
	23,
	26,
	23,
	337,
	23,
	26,
	28,
	26,
	2663,
	89,
	23,
	26,
	23,
	2664,
	3,
	23,
	9,
	23,
	2665,
	14,
	14,
	14,
	26,
	2616,
	829,
	21,
	43,
	106,
	4,
	23,
	3,
	2666,
	23,
	26,
	2667,
	2667,
	2667,
	2668,
	2441,
	23,
	131,
	594,
	119,
	138,
	26,
	27,
	2461,
	2669,
	89,
	23,
	90,
	459,
	2670,
	28,
	143,
	26,
	23,
	26,
	26,
	26,
	89,
	89,
	130,
	23,
	23,
	23,
	26,
	26,
	89,
	26,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	980,
	14,
	26,
	2456,
	2671,
	2456,
	2671,
	14,
	26,
	14,
	14,
	23,
	105,
	28,
	1,
	23,
	14,
	14,
	14,
	89,
	10,
	14,
	14,
	26,
	14,
	14,
	112,
	1299,
	14,
	14,
	2616,
	2616,
	14,
	14,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	26,
	829,
	130,
	14,
	26,
	27,
	14,
	26,
	14,
	2616,
	27,
	27,
	4,
	106,
	137,
	3,
	0,
	0,
	2672,
	3,
	90,
	2455,
	4,
	23,
	26,
	89,
	337,
	23,
	23,
	23,
	89,
	89,
	31,
	14,
	26,
	10,
	32,
	89,
	31,
	172,
	200,
	23,
	26,
	731,
	14,
	2673,
	89,
	23,
	2674,
	2675,
	2676,
	2677,
	2678,
	2679,
	3,
	23,
	9,
	23,
	2680,
	23,
	2681,
	14,
	26,
	14,
	26,
	14,
	89,
	31,
	89,
	31,
	14,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[40] = 
{
	{ 0x02000011, { 77, 1 } },
	{ 0x02000014, { 78, 1 } },
	{ 0x02000015, { 79, 1 } },
	{ 0x0200001B, { 80, 2 } },
	{ 0x02000026, { 82, 3 } },
	{ 0x0600003F, { 0, 1 } },
	{ 0x06000040, { 1, 1 } },
	{ 0x06000041, { 2, 1 } },
	{ 0x06000042, { 3, 1 } },
	{ 0x06000048, { 4, 1 } },
	{ 0x06000049, { 5, 1 } },
	{ 0x0600004A, { 6, 1 } },
	{ 0x0600004B, { 7, 1 } },
	{ 0x0600004C, { 8, 1 } },
	{ 0x0600004D, { 9, 1 } },
	{ 0x0600004E, { 10, 1 } },
	{ 0x0600004F, { 11, 1 } },
	{ 0x06000050, { 12, 1 } },
	{ 0x06000051, { 13, 1 } },
	{ 0x06000052, { 14, 1 } },
	{ 0x06000053, { 15, 1 } },
	{ 0x06000054, { 16, 1 } },
	{ 0x060000B7, { 17, 1 } },
	{ 0x060000BA, { 18, 2 } },
	{ 0x060000BB, { 20, 6 } },
	{ 0x060000BC, { 26, 5 } },
	{ 0x060000C1, { 31, 2 } },
	{ 0x060000C2, { 33, 6 } },
	{ 0x060000C3, { 39, 5 } },
	{ 0x060000C4, { 44, 6 } },
	{ 0x060000C5, { 50, 5 } },
	{ 0x060000C9, { 55, 1 } },
	{ 0x060000CA, { 56, 8 } },
	{ 0x060000DA, { 64, 4 } },
	{ 0x060000DB, { 68, 6 } },
	{ 0x060000DC, { 74, 2 } },
	{ 0x060000FD, { 76, 1 } },
	{ 0x06000167, { 85, 1 } },
	{ 0x06000168, { 86, 1 } },
	{ 0x0600016C, { 87, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[90] = 
{
	{ (Il2CppRGCTXDataType)3, 26665 },
	{ (Il2CppRGCTXDataType)3, 26666 },
	{ (Il2CppRGCTXDataType)3, 26667 },
	{ (Il2CppRGCTXDataType)3, 26668 },
	{ (Il2CppRGCTXDataType)3, 26669 },
	{ (Il2CppRGCTXDataType)3, 26670 },
	{ (Il2CppRGCTXDataType)3, 26671 },
	{ (Il2CppRGCTXDataType)3, 26672 },
	{ (Il2CppRGCTXDataType)3, 26673 },
	{ (Il2CppRGCTXDataType)3, 26674 },
	{ (Il2CppRGCTXDataType)3, 26675 },
	{ (Il2CppRGCTXDataType)3, 26676 },
	{ (Il2CppRGCTXDataType)3, 26677 },
	{ (Il2CppRGCTXDataType)3, 26678 },
	{ (Il2CppRGCTXDataType)3, 26679 },
	{ (Il2CppRGCTXDataType)3, 26680 },
	{ (Il2CppRGCTXDataType)3, 26681 },
	{ (Il2CppRGCTXDataType)3, 26682 },
	{ (Il2CppRGCTXDataType)3, 26683 },
	{ (Il2CppRGCTXDataType)3, 26684 },
	{ (Il2CppRGCTXDataType)2, 35077 },
	{ (Il2CppRGCTXDataType)3, 26685 },
	{ (Il2CppRGCTXDataType)3, 26686 },
	{ (Il2CppRGCTXDataType)2, 35078 },
	{ (Il2CppRGCTXDataType)3, 26687 },
	{ (Il2CppRGCTXDataType)3, 26688 },
	{ (Il2CppRGCTXDataType)3, 26689 },
	{ (Il2CppRGCTXDataType)3, 26690 },
	{ (Il2CppRGCTXDataType)1, 33683 },
	{ (Il2CppRGCTXDataType)3, 26691 },
	{ (Il2CppRGCTXDataType)3, 26692 },
	{ (Il2CppRGCTXDataType)3, 26693 },
	{ (Il2CppRGCTXDataType)3, 26694 },
	{ (Il2CppRGCTXDataType)2, 35079 },
	{ (Il2CppRGCTXDataType)3, 26695 },
	{ (Il2CppRGCTXDataType)3, 26696 },
	{ (Il2CppRGCTXDataType)2, 35080 },
	{ (Il2CppRGCTXDataType)3, 26697 },
	{ (Il2CppRGCTXDataType)3, 26698 },
	{ (Il2CppRGCTXDataType)3, 26699 },
	{ (Il2CppRGCTXDataType)3, 26700 },
	{ (Il2CppRGCTXDataType)1, 33693 },
	{ (Il2CppRGCTXDataType)3, 26701 },
	{ (Il2CppRGCTXDataType)3, 26702 },
	{ (Il2CppRGCTXDataType)2, 35081 },
	{ (Il2CppRGCTXDataType)3, 26703 },
	{ (Il2CppRGCTXDataType)3, 26704 },
	{ (Il2CppRGCTXDataType)2, 35082 },
	{ (Il2CppRGCTXDataType)3, 26705 },
	{ (Il2CppRGCTXDataType)3, 26706 },
	{ (Il2CppRGCTXDataType)3, 26707 },
	{ (Il2CppRGCTXDataType)3, 26708 },
	{ (Il2CppRGCTXDataType)1, 33701 },
	{ (Il2CppRGCTXDataType)3, 26709 },
	{ (Il2CppRGCTXDataType)3, 26710 },
	{ (Il2CppRGCTXDataType)2, 33704 },
	{ (Il2CppRGCTXDataType)1, 33706 },
	{ (Il2CppRGCTXDataType)3, 26711 },
	{ (Il2CppRGCTXDataType)2, 33706 },
	{ (Il2CppRGCTXDataType)3, 26712 },
	{ (Il2CppRGCTXDataType)3, 26713 },
	{ (Il2CppRGCTXDataType)2, 33705 },
	{ (Il2CppRGCTXDataType)3, 26714 },
	{ (Il2CppRGCTXDataType)3, 26715 },
	{ (Il2CppRGCTXDataType)3, 26716 },
	{ (Il2CppRGCTXDataType)2, 35083 },
	{ (Il2CppRGCTXDataType)3, 26717 },
	{ (Il2CppRGCTXDataType)3, 26718 },
	{ (Il2CppRGCTXDataType)2, 35084 },
	{ (Il2CppRGCTXDataType)3, 26719 },
	{ (Il2CppRGCTXDataType)3, 26720 },
	{ (Il2CppRGCTXDataType)2, 35085 },
	{ (Il2CppRGCTXDataType)3, 26721 },
	{ (Il2CppRGCTXDataType)3, 26722 },
	{ (Il2CppRGCTXDataType)3, 26723 },
	{ (Il2CppRGCTXDataType)3, 26724 },
	{ (Il2CppRGCTXDataType)3, 26725 },
	{ (Il2CppRGCTXDataType)3, 26726 },
	{ (Il2CppRGCTXDataType)3, 26727 },
	{ (Il2CppRGCTXDataType)3, 26728 },
	{ (Il2CppRGCTXDataType)3, 26729 },
	{ (Il2CppRGCTXDataType)3, 26730 },
	{ (Il2CppRGCTXDataType)3, 26731 },
	{ (Il2CppRGCTXDataType)3, 26732 },
	{ (Il2CppRGCTXDataType)1, 33809 },
	{ (Il2CppRGCTXDataType)3, 26733 },
	{ (Il2CppRGCTXDataType)3, 26734 },
	{ (Il2CppRGCTXDataType)3, 26735 },
	{ (Il2CppRGCTXDataType)3, 26736 },
	{ (Il2CppRGCTXDataType)2, 33827 },
};
extern const Il2CppCodeGenModule g_Unity_AddressablesCodeGenModule;
const Il2CppCodeGenModule g_Unity_AddressablesCodeGenModule = 
{
	"Unity.Addressables.dll",
	597,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	40,
	s_rgctxIndices,
	90,
	s_rgctxValues,
	NULL,
};
