﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Sirenix.Serialization.Vector2IntFormatter::Read(UnityEngine.Vector2Int&,Sirenix.Serialization.IDataReader)
extern void Vector2IntFormatter_Read_mA21C3E659A6CB0AFBC3BE449F5D89F3371113A87 (void);
// 0x00000002 System.Void Sirenix.Serialization.Vector2IntFormatter::Write(UnityEngine.Vector2Int&,Sirenix.Serialization.IDataWriter)
extern void Vector2IntFormatter_Write_mAA3650B402D444F7BE06C1204A4AF1E388D48703 (void);
// 0x00000003 System.Void Sirenix.Serialization.Vector2IntFormatter::.ctor()
extern void Vector2IntFormatter__ctor_m1D5A85222610FB68191253A3825786F60B1603A7 (void);
// 0x00000004 System.Void Sirenix.Serialization.Vector2IntFormatter::.cctor()
extern void Vector2IntFormatter__cctor_m54F4F1A8A1CEEB68DD3CE89A41785CEB29B90C10 (void);
// 0x00000005 System.Void Sirenix.Serialization.Vector3IntFormatter::Read(UnityEngine.Vector3Int&,Sirenix.Serialization.IDataReader)
extern void Vector3IntFormatter_Read_m59CA0ED5844C30BD42ECA01ECCD0C6CC7383E925 (void);
// 0x00000006 System.Void Sirenix.Serialization.Vector3IntFormatter::Write(UnityEngine.Vector3Int&,Sirenix.Serialization.IDataWriter)
extern void Vector3IntFormatter_Write_mD7A7F868B17C028C774E1A6BF2E7BA1482FAA3A3 (void);
// 0x00000007 System.Void Sirenix.Serialization.Vector3IntFormatter::.ctor()
extern void Vector3IntFormatter__ctor_mA9EE43DE6766A23E0FCD16A00613433E760A8531 (void);
// 0x00000008 System.Void Sirenix.Serialization.Vector3IntFormatter::.cctor()
extern void Vector3IntFormatter__cctor_m20468D942547499DA49A6DAC5AB8F8BA1F7D0429 (void);
static Il2CppMethodPointer s_methodPointers[8] = 
{
	Vector2IntFormatter_Read_mA21C3E659A6CB0AFBC3BE449F5D89F3371113A87,
	Vector2IntFormatter_Write_mAA3650B402D444F7BE06C1204A4AF1E388D48703,
	Vector2IntFormatter__ctor_m1D5A85222610FB68191253A3825786F60B1603A7,
	Vector2IntFormatter__cctor_m54F4F1A8A1CEEB68DD3CE89A41785CEB29B90C10,
	Vector3IntFormatter_Read_m59CA0ED5844C30BD42ECA01ECCD0C6CC7383E925,
	Vector3IntFormatter_Write_mD7A7F868B17C028C774E1A6BF2E7BA1482FAA3A3,
	Vector3IntFormatter__ctor_mA9EE43DE6766A23E0FCD16A00613433E760A8531,
	Vector3IntFormatter__cctor_m20468D942547499DA49A6DAC5AB8F8BA1F7D0429,
};
static const int32_t s_InvokerIndices[8] = 
{
	358,
	358,
	23,
	3,
	358,
	358,
	23,
	3,
};
extern const Il2CppCodeGenModule g_Sirenix_OdinInspector_CompatibilityLayerCodeGenModule;
const Il2CppCodeGenModule g_Sirenix_OdinInspector_CompatibilityLayerCodeGenModule = 
{
	"Sirenix.OdinInspector.CompatibilityLayer.dll",
	8,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
