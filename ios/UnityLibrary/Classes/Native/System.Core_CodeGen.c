﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B (void);
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 (void);
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 (void);
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E (void);
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 (void);
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 (void);
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC (void);
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 (void);
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 (void);
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA (void);
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 (void);
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 (void);
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 (void);
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD (void);
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 (void);
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 (void);
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 (void);
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED (void);
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC (void);
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 (void);
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E (void);
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC (void);
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 (void);
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 (void);
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 (void);
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F (void);
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C (void);
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A (void);
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 (void);
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA (void);
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B (void);
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F (void);
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F (void);
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA (void);
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 (void);
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 (void);
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 (void);
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA (void);
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F (void);
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD (void);
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA (void);
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 (void);
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA (void);
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 (void);
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 (void);
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 (void);
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneElement()
extern void Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863 (void);
// 0x00000031 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000032 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000033 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D (void);
// 0x00000034 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000036 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000037 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000003A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Skip(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::SkipIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003D System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003E System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Concat(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ConcatIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000041 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000043 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000044 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000045 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000046 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000047 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000048 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000049 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x0000004A System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000004B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x0000004C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x0000004D TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004E TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004F TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000050 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000051 TSource System.Linq.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000052 TSource System.Linq.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000053 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000054 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000055 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x00000056 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000057 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000058 System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000059 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000005A System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000005B System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000005C System.Single System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<System.Single>)
extern void Enumerable_Max_mBE99D67A0AF1147E3C46DCF7D489129134248262 (void);
// 0x0000005D System.Single System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Single>)
// 0x0000005E System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x0000005F TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000060 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x00000061 System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x00000062 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x00000063 System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x00000064 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000065 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000066 System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000067 System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000068 System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000069 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000006A System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x0000006B System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x0000006C System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x0000006D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000006E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006F System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000070 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x00000071 System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x00000072 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000073 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000074 System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000075 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x00000076 System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x00000077 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000078 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000079 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000007A System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x0000007B System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x0000007C System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000007D System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000007E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007F System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000080 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x00000081 System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x00000082 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000083 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000084 System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000085 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x00000086 System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x00000087 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000088 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000089 System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x0000008A System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000008B System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x0000008C TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000008D System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000008E System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x0000008F System.Boolean System.Linq.Enumerable/<SelectManyIterator>d__17`2::MoveNext()
// 0x00000090 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000091 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000092 TResult System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000093 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000094 System.Object System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000095 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000096 System.Collections.IEnumerator System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000097 System.Void System.Linq.Enumerable/<SkipIterator>d__31`1::.ctor(System.Int32)
// 0x00000098 System.Void System.Linq.Enumerable/<SkipIterator>d__31`1::System.IDisposable.Dispose()
// 0x00000099 System.Boolean System.Linq.Enumerable/<SkipIterator>d__31`1::MoveNext()
// 0x0000009A System.Void System.Linq.Enumerable/<SkipIterator>d__31`1::<>m__Finally1()
// 0x0000009B TSource System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000009C System.Void System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.IEnumerator.Reset()
// 0x0000009D System.Object System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.IEnumerator.get_Current()
// 0x0000009E System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000009F System.Collections.IEnumerator System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A0 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::.ctor(System.Int32)
// 0x000000A1 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::System.IDisposable.Dispose()
// 0x000000A2 System.Boolean System.Linq.Enumerable/<ConcatIterator>d__59`1::MoveNext()
// 0x000000A3 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::<>m__Finally1()
// 0x000000A4 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::<>m__Finally2()
// 0x000000A5 TSource System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000A6 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerator.Reset()
// 0x000000A7 System.Object System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerator.get_Current()
// 0x000000A8 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000A9 System.Collections.IEnumerator System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AA System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x000000AB System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x000000AC System.Boolean System.Linq.Enumerable/<DistinctIterator>d__68`1::MoveNext()
// 0x000000AD System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::<>m__Finally1()
// 0x000000AE TSource System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000AF System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x000000B0 System.Object System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x000000B1 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000B2 System.Collections.IEnumerator System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B3 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x000000B4 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x000000B5 System.Boolean System.Linq.Enumerable/<UnionIterator>d__71`1::MoveNext()
// 0x000000B6 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally1()
// 0x000000B7 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally2()
// 0x000000B8 TSource System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000B9 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x000000BA System.Object System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x000000BB System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000BC System.Collections.IEnumerator System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BD System.Void System.Linq.Enumerable/<CastIterator>d__99`1::.ctor(System.Int32)
// 0x000000BE System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x000000BF System.Boolean System.Linq.Enumerable/<CastIterator>d__99`1::MoveNext()
// 0x000000C0 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::<>m__Finally1()
// 0x000000C1 TResult System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000C2 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x000000C3 System.Object System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x000000C4 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000C5 System.Collections.IEnumerator System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C6 System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x000000C7 System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x000000C8 System.Void System.Linq.IdentityFunction`1/<>c::.cctor()
// 0x000000C9 System.Void System.Linq.IdentityFunction`1/<>c::.ctor()
// 0x000000CA TElement System.Linq.IdentityFunction`1/<>c::<get_Instance>b__1_0(TElement)
// 0x000000CB System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000CC System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000CD System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000CE System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x000000CF System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000000D0 System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x000000D1 System.Linq.Lookup`2/Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x000000D2 System.Void System.Linq.Lookup`2::Resize()
// 0x000000D3 System.Void System.Linq.Lookup`2/Grouping::Add(TElement)
// 0x000000D4 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2/Grouping::GetEnumerator()
// 0x000000D5 System.Collections.IEnumerator System.Linq.Lookup`2/Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x000000D6 System.Int32 System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x000000D7 System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x000000D8 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x000000D9 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x000000DA System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x000000DB System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x000000DC System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x000000DD System.Int32 System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x000000DE System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x000000DF System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x000000E0 TElement System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x000000E1 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x000000E2 System.Void System.Linq.Lookup`2/Grouping::.ctor()
// 0x000000E3 System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::.ctor(System.Int32)
// 0x000000E4 System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x000000E5 System.Boolean System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::MoveNext()
// 0x000000E6 TElement System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000E7 System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x000000E8 System.Object System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x000000E9 System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::.ctor(System.Int32)
// 0x000000EA System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x000000EB System.Boolean System.Linq.Lookup`2/<GetEnumerator>d__12::MoveNext()
// 0x000000EC System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x000000ED System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x000000EE System.Object System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x000000EF System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000F0 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000F1 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000F2 System.Void System.Linq.Set`1::Resize()
// 0x000000F3 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000F4 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000F5 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x000000F6 System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x000000F7 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000F8 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000F9 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000FA System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000FB System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000FC System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000FD System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000FE System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x000000FF TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000100 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000101 System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000102 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000103 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000104 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000105 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000106 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000107 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000108 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000109 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x0000010A System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x0000010B System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x0000010C System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000010D TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000010E System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32*,System.Int32)
extern void BitHelper__ctor_m7D010B426005B48D8C03139EB9248E927293BF3F (void);
// 0x0000010F System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32[],System.Int32)
extern void BitHelper__ctor_m8F1FB12BE0C611E499E3E03A151A52FE02A468C5 (void);
// 0x00000110 System.Void System.Collections.Generic.BitHelper::MarkBit(System.Int32)
extern void BitHelper_MarkBit_mADF9FABA576B57C4BB5BD616ACAA28C613045802 (void);
// 0x00000111 System.Boolean System.Collections.Generic.BitHelper::IsMarked(System.Int32)
extern void BitHelper_IsMarked_m2DB877B8A728264DE8A02E20BDFA4C886CAB4345 (void);
// 0x00000112 System.Int32 System.Collections.Generic.BitHelper::ToIntArrayLength(System.Int32)
extern void BitHelper_ToIntArrayLength_mAFCF611F107D1DDA8FA965FA24A027715E1D3991 (void);
// 0x00000113 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000114 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000115 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000116 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000117 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000118 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000119 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000011A System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000011B System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000011C System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000011D System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000011E System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000011F System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000120 System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000121 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000122 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000123 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000124 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000125 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000126 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000127 System.Void System.Collections.Generic.HashSet`1::IntersectWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000128 System.Boolean System.Collections.Generic.HashSet`1::SetEquals(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000129 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000012A System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000012B System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x0000012C System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x0000012D System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000012E System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000012F System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000130 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000131 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x00000132 System.Boolean System.Collections.Generic.HashSet`1::ContainsAllElements(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000133 System.Void System.Collections.Generic.HashSet`1::IntersectWithHashSetWithSameEC(System.Collections.Generic.HashSet`1<T>)
// 0x00000134 System.Void System.Collections.Generic.HashSet`1::IntersectWithEnumerable(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000135 System.Int32 System.Collections.Generic.HashSet`1::InternalIndexOf(T)
// 0x00000136 System.Collections.Generic.HashSet`1/ElementCount<T> System.Collections.Generic.HashSet`1::CheckUniqueAndUnfoundElements(System.Collections.Generic.IEnumerable`1<T>,System.Boolean)
// 0x00000137 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x00000138 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000139 System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000013A System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x0000013B System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x0000013C T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x0000013D System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000013E System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[318] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Max_mBE99D67A0AF1147E3C46DCF7D489129134248262,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BitHelper__ctor_m7D010B426005B48D8C03139EB9248E927293BF3F,
	BitHelper__ctor_m8F1FB12BE0C611E499E3E03A151A52FE02A468C5,
	BitHelper_MarkBit_mADF9FABA576B57C4BB5BD616ACAA28C613045802,
	BitHelper_IsMarked_m2DB877B8A728264DE8A02E20BDFA4C886CAB4345,
	BitHelper_ToIntArrayLength_mAFCF611F107D1DDA8FA965FA24A027715E1D3991,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[318] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	105,
	14,
	105,
	31,
	23,
	23,
	23,
	23,
	23,
	105,
	105,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	926,
	27,
	37,
	197,
	197,
	3,
	0,
	0,
	4,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	493,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	64,
	130,
	32,
	30,
	21,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[80] = 
{
	{ 0x02000008, { 122, 4 } },
	{ 0x02000009, { 126, 9 } },
	{ 0x0200000A, { 137, 7 } },
	{ 0x0200000B, { 146, 10 } },
	{ 0x0200000C, { 158, 11 } },
	{ 0x0200000D, { 172, 9 } },
	{ 0x0200000E, { 184, 12 } },
	{ 0x0200000F, { 199, 1 } },
	{ 0x02000010, { 200, 2 } },
	{ 0x02000011, { 202, 12 } },
	{ 0x02000012, { 214, 8 } },
	{ 0x02000013, { 222, 9 } },
	{ 0x02000014, { 231, 11 } },
	{ 0x02000015, { 242, 12 } },
	{ 0x02000016, { 254, 6 } },
	{ 0x02000017, { 260, 2 } },
	{ 0x02000018, { 262, 4 } },
	{ 0x02000019, { 266, 3 } },
	{ 0x0200001C, { 269, 17 } },
	{ 0x0200001D, { 290, 5 } },
	{ 0x0200001E, { 295, 1 } },
	{ 0x02000020, { 296, 8 } },
	{ 0x02000022, { 304, 4 } },
	{ 0x02000023, { 308, 3 } },
	{ 0x02000024, { 313, 5 } },
	{ 0x02000025, { 318, 7 } },
	{ 0x02000026, { 325, 3 } },
	{ 0x02000027, { 328, 7 } },
	{ 0x02000028, { 335, 4 } },
	{ 0x0200002A, { 339, 43 } },
	{ 0x0200002D, { 382, 2 } },
	{ 0x06000034, { 0, 10 } },
	{ 0x06000035, { 10, 10 } },
	{ 0x06000036, { 20, 5 } },
	{ 0x06000037, { 25, 5 } },
	{ 0x06000038, { 30, 1 } },
	{ 0x06000039, { 31, 2 } },
	{ 0x0600003A, { 33, 1 } },
	{ 0x0600003B, { 34, 2 } },
	{ 0x0600003C, { 36, 2 } },
	{ 0x0600003D, { 38, 1 } },
	{ 0x0600003E, { 39, 4 } },
	{ 0x0600003F, { 43, 1 } },
	{ 0x06000040, { 44, 2 } },
	{ 0x06000041, { 46, 1 } },
	{ 0x06000042, { 47, 2 } },
	{ 0x06000043, { 49, 1 } },
	{ 0x06000044, { 50, 2 } },
	{ 0x06000045, { 52, 1 } },
	{ 0x06000046, { 53, 5 } },
	{ 0x06000047, { 58, 3 } },
	{ 0x06000048, { 61, 2 } },
	{ 0x06000049, { 63, 1 } },
	{ 0x0600004A, { 64, 7 } },
	{ 0x0600004B, { 71, 2 } },
	{ 0x0600004C, { 73, 2 } },
	{ 0x0600004D, { 75, 4 } },
	{ 0x0600004E, { 79, 4 } },
	{ 0x0600004F, { 83, 3 } },
	{ 0x06000050, { 86, 4 } },
	{ 0x06000051, { 90, 3 } },
	{ 0x06000052, { 93, 4 } },
	{ 0x06000053, { 97, 4 } },
	{ 0x06000054, { 101, 3 } },
	{ 0x06000055, { 104, 1 } },
	{ 0x06000056, { 105, 1 } },
	{ 0x06000057, { 106, 3 } },
	{ 0x06000058, { 109, 3 } },
	{ 0x06000059, { 112, 2 } },
	{ 0x0600005A, { 114, 2 } },
	{ 0x0600005B, { 116, 5 } },
	{ 0x0600005D, { 121, 1 } },
	{ 0x0600006D, { 135, 2 } },
	{ 0x06000072, { 144, 2 } },
	{ 0x06000077, { 156, 2 } },
	{ 0x0600007D, { 169, 3 } },
	{ 0x06000082, { 181, 3 } },
	{ 0x06000087, { 196, 3 } },
	{ 0x060000CC, { 286, 4 } },
	{ 0x060000FA, { 311, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[384] = 
{
	{ (Il2CppRGCTXDataType)2, 34617 },
	{ (Il2CppRGCTXDataType)3, 25034 },
	{ (Il2CppRGCTXDataType)2, 34618 },
	{ (Il2CppRGCTXDataType)2, 34619 },
	{ (Il2CppRGCTXDataType)3, 25035 },
	{ (Il2CppRGCTXDataType)2, 34620 },
	{ (Il2CppRGCTXDataType)2, 34621 },
	{ (Il2CppRGCTXDataType)3, 25036 },
	{ (Il2CppRGCTXDataType)2, 34622 },
	{ (Il2CppRGCTXDataType)3, 25037 },
	{ (Il2CppRGCTXDataType)2, 34623 },
	{ (Il2CppRGCTXDataType)3, 25038 },
	{ (Il2CppRGCTXDataType)2, 34624 },
	{ (Il2CppRGCTXDataType)2, 34625 },
	{ (Il2CppRGCTXDataType)3, 25039 },
	{ (Il2CppRGCTXDataType)2, 34626 },
	{ (Il2CppRGCTXDataType)2, 34627 },
	{ (Il2CppRGCTXDataType)3, 25040 },
	{ (Il2CppRGCTXDataType)2, 34628 },
	{ (Il2CppRGCTXDataType)3, 25041 },
	{ (Il2CppRGCTXDataType)2, 34629 },
	{ (Il2CppRGCTXDataType)3, 25042 },
	{ (Il2CppRGCTXDataType)3, 25043 },
	{ (Il2CppRGCTXDataType)2, 24114 },
	{ (Il2CppRGCTXDataType)3, 25044 },
	{ (Il2CppRGCTXDataType)2, 34630 },
	{ (Il2CppRGCTXDataType)3, 25045 },
	{ (Il2CppRGCTXDataType)3, 25046 },
	{ (Il2CppRGCTXDataType)2, 24121 },
	{ (Il2CppRGCTXDataType)3, 25047 },
	{ (Il2CppRGCTXDataType)3, 25048 },
	{ (Il2CppRGCTXDataType)2, 34631 },
	{ (Il2CppRGCTXDataType)3, 25049 },
	{ (Il2CppRGCTXDataType)3, 25050 },
	{ (Il2CppRGCTXDataType)2, 34632 },
	{ (Il2CppRGCTXDataType)3, 25051 },
	{ (Il2CppRGCTXDataType)2, 34633 },
	{ (Il2CppRGCTXDataType)3, 25052 },
	{ (Il2CppRGCTXDataType)3, 25053 },
	{ (Il2CppRGCTXDataType)3, 25054 },
	{ (Il2CppRGCTXDataType)2, 34634 },
	{ (Il2CppRGCTXDataType)2, 34635 },
	{ (Il2CppRGCTXDataType)3, 25055 },
	{ (Il2CppRGCTXDataType)3, 25056 },
	{ (Il2CppRGCTXDataType)2, 34636 },
	{ (Il2CppRGCTXDataType)3, 25057 },
	{ (Il2CppRGCTXDataType)3, 25058 },
	{ (Il2CppRGCTXDataType)2, 34637 },
	{ (Il2CppRGCTXDataType)3, 25059 },
	{ (Il2CppRGCTXDataType)3, 25060 },
	{ (Il2CppRGCTXDataType)2, 34638 },
	{ (Il2CppRGCTXDataType)3, 25061 },
	{ (Il2CppRGCTXDataType)3, 25062 },
	{ (Il2CppRGCTXDataType)3, 25063 },
	{ (Il2CppRGCTXDataType)2, 34639 },
	{ (Il2CppRGCTXDataType)2, 24167 },
	{ (Il2CppRGCTXDataType)2, 34640 },
	{ (Il2CppRGCTXDataType)2, 24169 },
	{ (Il2CppRGCTXDataType)2, 34641 },
	{ (Il2CppRGCTXDataType)3, 25064 },
	{ (Il2CppRGCTXDataType)3, 25065 },
	{ (Il2CppRGCTXDataType)2, 24175 },
	{ (Il2CppRGCTXDataType)3, 25066 },
	{ (Il2CppRGCTXDataType)3, 25067 },
	{ (Il2CppRGCTXDataType)2, 24190 },
	{ (Il2CppRGCTXDataType)3, 25068 },
	{ (Il2CppRGCTXDataType)2, 24183 },
	{ (Il2CppRGCTXDataType)2, 34642 },
	{ (Il2CppRGCTXDataType)3, 25069 },
	{ (Il2CppRGCTXDataType)3, 25070 },
	{ (Il2CppRGCTXDataType)3, 25071 },
	{ (Il2CppRGCTXDataType)2, 24191 },
	{ (Il2CppRGCTXDataType)3, 25072 },
	{ (Il2CppRGCTXDataType)2, 34643 },
	{ (Il2CppRGCTXDataType)3, 25073 },
	{ (Il2CppRGCTXDataType)2, 34644 },
	{ (Il2CppRGCTXDataType)2, 34645 },
	{ (Il2CppRGCTXDataType)2, 24195 },
	{ (Il2CppRGCTXDataType)2, 34646 },
	{ (Il2CppRGCTXDataType)2, 34647 },
	{ (Il2CppRGCTXDataType)2, 34648 },
	{ (Il2CppRGCTXDataType)2, 24197 },
	{ (Il2CppRGCTXDataType)2, 34649 },
	{ (Il2CppRGCTXDataType)2, 24199 },
	{ (Il2CppRGCTXDataType)2, 34650 },
	{ (Il2CppRGCTXDataType)3, 25074 },
	{ (Il2CppRGCTXDataType)2, 34651 },
	{ (Il2CppRGCTXDataType)2, 34652 },
	{ (Il2CppRGCTXDataType)2, 24202 },
	{ (Il2CppRGCTXDataType)2, 34653 },
	{ (Il2CppRGCTXDataType)2, 24204 },
	{ (Il2CppRGCTXDataType)2, 34654 },
	{ (Il2CppRGCTXDataType)3, 25075 },
	{ (Il2CppRGCTXDataType)2, 34655 },
	{ (Il2CppRGCTXDataType)2, 34656 },
	{ (Il2CppRGCTXDataType)2, 24207 },
	{ (Il2CppRGCTXDataType)2, 34657 },
	{ (Il2CppRGCTXDataType)2, 34658 },
	{ (Il2CppRGCTXDataType)2, 34659 },
	{ (Il2CppRGCTXDataType)2, 24209 },
	{ (Il2CppRGCTXDataType)2, 34660 },
	{ (Il2CppRGCTXDataType)2, 24211 },
	{ (Il2CppRGCTXDataType)2, 34661 },
	{ (Il2CppRGCTXDataType)3, 25076 },
	{ (Il2CppRGCTXDataType)2, 34662 },
	{ (Il2CppRGCTXDataType)2, 24216 },
	{ (Il2CppRGCTXDataType)2, 24218 },
	{ (Il2CppRGCTXDataType)2, 34663 },
	{ (Il2CppRGCTXDataType)3, 25077 },
	{ (Il2CppRGCTXDataType)2, 24221 },
	{ (Il2CppRGCTXDataType)2, 34664 },
	{ (Il2CppRGCTXDataType)3, 25078 },
	{ (Il2CppRGCTXDataType)2, 34665 },
	{ (Il2CppRGCTXDataType)2, 24224 },
	{ (Il2CppRGCTXDataType)2, 34666 },
	{ (Il2CppRGCTXDataType)3, 25079 },
	{ (Il2CppRGCTXDataType)3, 25080 },
	{ (Il2CppRGCTXDataType)2, 34667 },
	{ (Il2CppRGCTXDataType)2, 24228 },
	{ (Il2CppRGCTXDataType)2, 34668 },
	{ (Il2CppRGCTXDataType)2, 24230 },
	{ (Il2CppRGCTXDataType)3, 25081 },
	{ (Il2CppRGCTXDataType)3, 25082 },
	{ (Il2CppRGCTXDataType)3, 25083 },
	{ (Il2CppRGCTXDataType)2, 24236 },
	{ (Il2CppRGCTXDataType)3, 25084 },
	{ (Il2CppRGCTXDataType)3, 25085 },
	{ (Il2CppRGCTXDataType)2, 24248 },
	{ (Il2CppRGCTXDataType)2, 34669 },
	{ (Il2CppRGCTXDataType)3, 25086 },
	{ (Il2CppRGCTXDataType)3, 25087 },
	{ (Il2CppRGCTXDataType)2, 24250 },
	{ (Il2CppRGCTXDataType)2, 34452 },
	{ (Il2CppRGCTXDataType)3, 25088 },
	{ (Il2CppRGCTXDataType)3, 25089 },
	{ (Il2CppRGCTXDataType)2, 34670 },
	{ (Il2CppRGCTXDataType)3, 25090 },
	{ (Il2CppRGCTXDataType)3, 25091 },
	{ (Il2CppRGCTXDataType)2, 24260 },
	{ (Il2CppRGCTXDataType)2, 34671 },
	{ (Il2CppRGCTXDataType)3, 25092 },
	{ (Il2CppRGCTXDataType)3, 25093 },
	{ (Il2CppRGCTXDataType)3, 23865 },
	{ (Il2CppRGCTXDataType)3, 25094 },
	{ (Il2CppRGCTXDataType)2, 34672 },
	{ (Il2CppRGCTXDataType)3, 25095 },
	{ (Il2CppRGCTXDataType)3, 25096 },
	{ (Il2CppRGCTXDataType)2, 24272 },
	{ (Il2CppRGCTXDataType)2, 34673 },
	{ (Il2CppRGCTXDataType)3, 25097 },
	{ (Il2CppRGCTXDataType)3, 25098 },
	{ (Il2CppRGCTXDataType)3, 25099 },
	{ (Il2CppRGCTXDataType)3, 25100 },
	{ (Il2CppRGCTXDataType)3, 25101 },
	{ (Il2CppRGCTXDataType)3, 23871 },
	{ (Il2CppRGCTXDataType)3, 25102 },
	{ (Il2CppRGCTXDataType)2, 34674 },
	{ (Il2CppRGCTXDataType)3, 25103 },
	{ (Il2CppRGCTXDataType)3, 25104 },
	{ (Il2CppRGCTXDataType)2, 24285 },
	{ (Il2CppRGCTXDataType)2, 34675 },
	{ (Il2CppRGCTXDataType)3, 25105 },
	{ (Il2CppRGCTXDataType)3, 25106 },
	{ (Il2CppRGCTXDataType)2, 24287 },
	{ (Il2CppRGCTXDataType)2, 34676 },
	{ (Il2CppRGCTXDataType)3, 25107 },
	{ (Il2CppRGCTXDataType)3, 25108 },
	{ (Il2CppRGCTXDataType)2, 34677 },
	{ (Il2CppRGCTXDataType)3, 25109 },
	{ (Il2CppRGCTXDataType)3, 25110 },
	{ (Il2CppRGCTXDataType)2, 34678 },
	{ (Il2CppRGCTXDataType)3, 25111 },
	{ (Il2CppRGCTXDataType)3, 25112 },
	{ (Il2CppRGCTXDataType)2, 24302 },
	{ (Il2CppRGCTXDataType)2, 34679 },
	{ (Il2CppRGCTXDataType)3, 25113 },
	{ (Il2CppRGCTXDataType)3, 25114 },
	{ (Il2CppRGCTXDataType)3, 25115 },
	{ (Il2CppRGCTXDataType)3, 23882 },
	{ (Il2CppRGCTXDataType)2, 34680 },
	{ (Il2CppRGCTXDataType)3, 25116 },
	{ (Il2CppRGCTXDataType)3, 25117 },
	{ (Il2CppRGCTXDataType)2, 34681 },
	{ (Il2CppRGCTXDataType)3, 25118 },
	{ (Il2CppRGCTXDataType)3, 25119 },
	{ (Il2CppRGCTXDataType)2, 24318 },
	{ (Il2CppRGCTXDataType)2, 34682 },
	{ (Il2CppRGCTXDataType)3, 25120 },
	{ (Il2CppRGCTXDataType)3, 25121 },
	{ (Il2CppRGCTXDataType)3, 25122 },
	{ (Il2CppRGCTXDataType)3, 25123 },
	{ (Il2CppRGCTXDataType)3, 25124 },
	{ (Il2CppRGCTXDataType)3, 25125 },
	{ (Il2CppRGCTXDataType)3, 23888 },
	{ (Il2CppRGCTXDataType)2, 34683 },
	{ (Il2CppRGCTXDataType)3, 25126 },
	{ (Il2CppRGCTXDataType)3, 25127 },
	{ (Il2CppRGCTXDataType)2, 34684 },
	{ (Il2CppRGCTXDataType)3, 25128 },
	{ (Il2CppRGCTXDataType)3, 25129 },
	{ (Il2CppRGCTXDataType)3, 25130 },
	{ (Il2CppRGCTXDataType)3, 25131 },
	{ (Il2CppRGCTXDataType)3, 25132 },
	{ (Il2CppRGCTXDataType)3, 25133 },
	{ (Il2CppRGCTXDataType)2, 34685 },
	{ (Il2CppRGCTXDataType)2, 34686 },
	{ (Il2CppRGCTXDataType)3, 25134 },
	{ (Il2CppRGCTXDataType)2, 24353 },
	{ (Il2CppRGCTXDataType)2, 24347 },
	{ (Il2CppRGCTXDataType)3, 25135 },
	{ (Il2CppRGCTXDataType)2, 24346 },
	{ (Il2CppRGCTXDataType)2, 34687 },
	{ (Il2CppRGCTXDataType)3, 25136 },
	{ (Il2CppRGCTXDataType)3, 25137 },
	{ (Il2CppRGCTXDataType)3, 25138 },
	{ (Il2CppRGCTXDataType)2, 24366 },
	{ (Il2CppRGCTXDataType)2, 24361 },
	{ (Il2CppRGCTXDataType)3, 25139 },
	{ (Il2CppRGCTXDataType)2, 24360 },
	{ (Il2CppRGCTXDataType)2, 34688 },
	{ (Il2CppRGCTXDataType)3, 25140 },
	{ (Il2CppRGCTXDataType)3, 25141 },
	{ (Il2CppRGCTXDataType)3, 25142 },
	{ (Il2CppRGCTXDataType)3, 25143 },
	{ (Il2CppRGCTXDataType)2, 24376 },
	{ (Il2CppRGCTXDataType)2, 24371 },
	{ (Il2CppRGCTXDataType)3, 25144 },
	{ (Il2CppRGCTXDataType)2, 24370 },
	{ (Il2CppRGCTXDataType)2, 34689 },
	{ (Il2CppRGCTXDataType)3, 25145 },
	{ (Il2CppRGCTXDataType)3, 25146 },
	{ (Il2CppRGCTXDataType)3, 25147 },
	{ (Il2CppRGCTXDataType)2, 34690 },
	{ (Il2CppRGCTXDataType)3, 25148 },
	{ (Il2CppRGCTXDataType)2, 24389 },
	{ (Il2CppRGCTXDataType)2, 24381 },
	{ (Il2CppRGCTXDataType)3, 25149 },
	{ (Il2CppRGCTXDataType)3, 25150 },
	{ (Il2CppRGCTXDataType)2, 24380 },
	{ (Il2CppRGCTXDataType)2, 34691 },
	{ (Il2CppRGCTXDataType)3, 25151 },
	{ (Il2CppRGCTXDataType)3, 25152 },
	{ (Il2CppRGCTXDataType)3, 25153 },
	{ (Il2CppRGCTXDataType)3, 25154 },
	{ (Il2CppRGCTXDataType)2, 34692 },
	{ (Il2CppRGCTXDataType)3, 25155 },
	{ (Il2CppRGCTXDataType)2, 24402 },
	{ (Il2CppRGCTXDataType)2, 24394 },
	{ (Il2CppRGCTXDataType)3, 25156 },
	{ (Il2CppRGCTXDataType)3, 25157 },
	{ (Il2CppRGCTXDataType)2, 24393 },
	{ (Il2CppRGCTXDataType)2, 34693 },
	{ (Il2CppRGCTXDataType)3, 25158 },
	{ (Il2CppRGCTXDataType)3, 25159 },
	{ (Il2CppRGCTXDataType)3, 25160 },
	{ (Il2CppRGCTXDataType)2, 24406 },
	{ (Il2CppRGCTXDataType)3, 25161 },
	{ (Il2CppRGCTXDataType)2, 34694 },
	{ (Il2CppRGCTXDataType)3, 25162 },
	{ (Il2CppRGCTXDataType)3, 25163 },
	{ (Il2CppRGCTXDataType)2, 34695 },
	{ (Il2CppRGCTXDataType)2, 34696 },
	{ (Il2CppRGCTXDataType)2, 34697 },
	{ (Il2CppRGCTXDataType)3, 25164 },
	{ (Il2CppRGCTXDataType)2, 24418 },
	{ (Il2CppRGCTXDataType)3, 25165 },
	{ (Il2CppRGCTXDataType)2, 34698 },
	{ (Il2CppRGCTXDataType)3, 25166 },
	{ (Il2CppRGCTXDataType)2, 34698 },
	{ (Il2CppRGCTXDataType)2, 24446 },
	{ (Il2CppRGCTXDataType)3, 25167 },
	{ (Il2CppRGCTXDataType)3, 25168 },
	{ (Il2CppRGCTXDataType)3, 25169 },
	{ (Il2CppRGCTXDataType)3, 25170 },
	{ (Il2CppRGCTXDataType)2, 34699 },
	{ (Il2CppRGCTXDataType)2, 34700 },
	{ (Il2CppRGCTXDataType)2, 34701 },
	{ (Il2CppRGCTXDataType)3, 25171 },
	{ (Il2CppRGCTXDataType)3, 25172 },
	{ (Il2CppRGCTXDataType)2, 24442 },
	{ (Il2CppRGCTXDataType)2, 24445 },
	{ (Il2CppRGCTXDataType)3, 25173 },
	{ (Il2CppRGCTXDataType)3, 25174 },
	{ (Il2CppRGCTXDataType)2, 24449 },
	{ (Il2CppRGCTXDataType)3, 25175 },
	{ (Il2CppRGCTXDataType)2, 34702 },
	{ (Il2CppRGCTXDataType)2, 24439 },
	{ (Il2CppRGCTXDataType)2, 34703 },
	{ (Il2CppRGCTXDataType)3, 25176 },
	{ (Il2CppRGCTXDataType)3, 25177 },
	{ (Il2CppRGCTXDataType)3, 25178 },
	{ (Il2CppRGCTXDataType)2, 34704 },
	{ (Il2CppRGCTXDataType)3, 25179 },
	{ (Il2CppRGCTXDataType)3, 25180 },
	{ (Il2CppRGCTXDataType)3, 25181 },
	{ (Il2CppRGCTXDataType)2, 24464 },
	{ (Il2CppRGCTXDataType)3, 25182 },
	{ (Il2CppRGCTXDataType)2, 34705 },
	{ (Il2CppRGCTXDataType)2, 34706 },
	{ (Il2CppRGCTXDataType)3, 25183 },
	{ (Il2CppRGCTXDataType)3, 25184 },
	{ (Il2CppRGCTXDataType)2, 24485 },
	{ (Il2CppRGCTXDataType)3, 25185 },
	{ (Il2CppRGCTXDataType)2, 24486 },
	{ (Il2CppRGCTXDataType)3, 25186 },
	{ (Il2CppRGCTXDataType)2, 34707 },
	{ (Il2CppRGCTXDataType)3, 25187 },
	{ (Il2CppRGCTXDataType)3, 25188 },
	{ (Il2CppRGCTXDataType)2, 34708 },
	{ (Il2CppRGCTXDataType)3, 25189 },
	{ (Il2CppRGCTXDataType)3, 25190 },
	{ (Il2CppRGCTXDataType)2, 34709 },
	{ (Il2CppRGCTXDataType)3, 25191 },
	{ (Il2CppRGCTXDataType)2, 34710 },
	{ (Il2CppRGCTXDataType)3, 25192 },
	{ (Il2CppRGCTXDataType)3, 25193 },
	{ (Il2CppRGCTXDataType)3, 25194 },
	{ (Il2CppRGCTXDataType)2, 24521 },
	{ (Il2CppRGCTXDataType)3, 25195 },
	{ (Il2CppRGCTXDataType)2, 24529 },
	{ (Il2CppRGCTXDataType)3, 25196 },
	{ (Il2CppRGCTXDataType)2, 34711 },
	{ (Il2CppRGCTXDataType)2, 34712 },
	{ (Il2CppRGCTXDataType)3, 25197 },
	{ (Il2CppRGCTXDataType)3, 25198 },
	{ (Il2CppRGCTXDataType)3, 25199 },
	{ (Il2CppRGCTXDataType)3, 25200 },
	{ (Il2CppRGCTXDataType)3, 25201 },
	{ (Il2CppRGCTXDataType)3, 25202 },
	{ (Il2CppRGCTXDataType)2, 24545 },
	{ (Il2CppRGCTXDataType)2, 34713 },
	{ (Il2CppRGCTXDataType)3, 25203 },
	{ (Il2CppRGCTXDataType)3, 25204 },
	{ (Il2CppRGCTXDataType)2, 24549 },
	{ (Il2CppRGCTXDataType)3, 25205 },
	{ (Il2CppRGCTXDataType)2, 34714 },
	{ (Il2CppRGCTXDataType)2, 24559 },
	{ (Il2CppRGCTXDataType)2, 24557 },
	{ (Il2CppRGCTXDataType)2, 34715 },
	{ (Il2CppRGCTXDataType)3, 25206 },
	{ (Il2CppRGCTXDataType)2, 34716 },
	{ (Il2CppRGCTXDataType)3, 25207 },
	{ (Il2CppRGCTXDataType)3, 25208 },
	{ (Il2CppRGCTXDataType)2, 24569 },
	{ (Il2CppRGCTXDataType)3, 25209 },
	{ (Il2CppRGCTXDataType)2, 24569 },
	{ (Il2CppRGCTXDataType)3, 25210 },
	{ (Il2CppRGCTXDataType)2, 24589 },
	{ (Il2CppRGCTXDataType)3, 25211 },
	{ (Il2CppRGCTXDataType)3, 25212 },
	{ (Il2CppRGCTXDataType)3, 25213 },
	{ (Il2CppRGCTXDataType)2, 34717 },
	{ (Il2CppRGCTXDataType)3, 25214 },
	{ (Il2CppRGCTXDataType)3, 25215 },
	{ (Il2CppRGCTXDataType)3, 25216 },
	{ (Il2CppRGCTXDataType)2, 24566 },
	{ (Il2CppRGCTXDataType)3, 25217 },
	{ (Il2CppRGCTXDataType)3, 25218 },
	{ (Il2CppRGCTXDataType)2, 24571 },
	{ (Il2CppRGCTXDataType)3, 25219 },
	{ (Il2CppRGCTXDataType)1, 34718 },
	{ (Il2CppRGCTXDataType)2, 24570 },
	{ (Il2CppRGCTXDataType)3, 25220 },
	{ (Il2CppRGCTXDataType)1, 24570 },
	{ (Il2CppRGCTXDataType)1, 24566 },
	{ (Il2CppRGCTXDataType)2, 34717 },
	{ (Il2CppRGCTXDataType)2, 24570 },
	{ (Il2CppRGCTXDataType)2, 24568 },
	{ (Il2CppRGCTXDataType)2, 24572 },
	{ (Il2CppRGCTXDataType)3, 25221 },
	{ (Il2CppRGCTXDataType)3, 25222 },
	{ (Il2CppRGCTXDataType)3, 25223 },
	{ (Il2CppRGCTXDataType)3, 25224 },
	{ (Il2CppRGCTXDataType)3, 25225 },
	{ (Il2CppRGCTXDataType)3, 25226 },
	{ (Il2CppRGCTXDataType)3, 25227 },
	{ (Il2CppRGCTXDataType)3, 25228 },
	{ (Il2CppRGCTXDataType)3, 25229 },
	{ (Il2CppRGCTXDataType)3, 25230 },
	{ (Il2CppRGCTXDataType)3, 25231 },
	{ (Il2CppRGCTXDataType)3, 25232 },
	{ (Il2CppRGCTXDataType)2, 24567 },
	{ (Il2CppRGCTXDataType)3, 25233 },
	{ (Il2CppRGCTXDataType)2, 24585 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	318,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	80,
	s_rgctxIndices,
	384,
	s_rgctxValues,
	NULL,
};
