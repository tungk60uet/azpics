﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void DelegateList`1::.ctor(System.Func`2<System.Action`1<T>,System.Collections.Generic.LinkedListNode`1<System.Action`1<T>>>,System.Action`1<System.Collections.Generic.LinkedListNode`1<System.Action`1<T>>>)
// 0x00000002 System.Int32 DelegateList`1::get_Count()
// 0x00000003 System.Void DelegateList`1::Add(System.Action`1<T>)
// 0x00000004 System.Void DelegateList`1::Remove(System.Action`1<T>)
// 0x00000005 System.Void DelegateList`1::Invoke(T)
// 0x00000006 System.Void DelegateList`1::Clear()
// 0x00000007 DelegateList`1<T> DelegateList`1::CreateWithGlobalCache()
// 0x00000008 System.Void ListWithEvents`1::add_OnElementAdded(System.Action`1<T>)
// 0x00000009 System.Void ListWithEvents`1::remove_OnElementAdded(System.Action`1<T>)
// 0x0000000A System.Void ListWithEvents`1::add_OnElementRemoved(System.Action`1<T>)
// 0x0000000B System.Void ListWithEvents`1::remove_OnElementRemoved(System.Action`1<T>)
// 0x0000000C System.Void ListWithEvents`1::InvokeAdded(T)
// 0x0000000D System.Void ListWithEvents`1::InvokeRemoved(T)
// 0x0000000E T ListWithEvents`1::get_Item(System.Int32)
// 0x0000000F System.Void ListWithEvents`1::set_Item(System.Int32,T)
// 0x00000010 System.Int32 ListWithEvents`1::get_Count()
// 0x00000011 System.Boolean ListWithEvents`1::get_IsReadOnly()
// 0x00000012 System.Void ListWithEvents`1::Add(T)
// 0x00000013 System.Void ListWithEvents`1::Clear()
// 0x00000014 System.Boolean ListWithEvents`1::Contains(T)
// 0x00000015 System.Void ListWithEvents`1::CopyTo(T[],System.Int32)
// 0x00000016 System.Collections.Generic.IEnumerator`1<T> ListWithEvents`1::GetEnumerator()
// 0x00000017 System.Int32 ListWithEvents`1::IndexOf(T)
// 0x00000018 System.Void ListWithEvents`1::Insert(System.Int32,T)
// 0x00000019 System.Boolean ListWithEvents`1::Remove(T)
// 0x0000001A System.Void ListWithEvents`1::RemoveAt(System.Int32)
// 0x0000001B System.Collections.IEnumerator ListWithEvents`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000001C System.Void ListWithEvents`1::.ctor()
// 0x0000001D System.Void MonoBehaviourCallbackHooks::add_OnUpdateDelegate(System.Action`1<System.Single>)
extern void MonoBehaviourCallbackHooks_add_OnUpdateDelegate_mF0A7D9A91F2790E2B5E411D31ECC9B5B4B3DCD5D (void);
// 0x0000001E System.Void MonoBehaviourCallbackHooks::remove_OnUpdateDelegate(System.Action`1<System.Single>)
extern void MonoBehaviourCallbackHooks_remove_OnUpdateDelegate_m00215EA5426D369E8D7C7198819192B433D0E99C (void);
// 0x0000001F System.String MonoBehaviourCallbackHooks::GetGameObjectName()
extern void MonoBehaviourCallbackHooks_GetGameObjectName_mC4C165B4ECA1E1A5FAD50AAA95C7DDBD95F09BFB (void);
// 0x00000020 System.Void MonoBehaviourCallbackHooks::Update()
extern void MonoBehaviourCallbackHooks_Update_mCC7B226851A48F80003FEB67801607356597F5A6 (void);
// 0x00000021 System.Void MonoBehaviourCallbackHooks::.ctor()
extern void MonoBehaviourCallbackHooks__ctor_mD71C42E2866DE9CE172E4E61EB0214A9D4BFEB6E (void);
// 0x00000022 System.Void UnityEngine.ResourceManagement.ChainOperation`2::.ctor()
// 0x00000023 System.String UnityEngine.ResourceManagement.ChainOperation`2::get_DebugName()
// 0x00000024 System.Void UnityEngine.ResourceManagement.ChainOperation`2::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000025 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>,System.Boolean)
// 0x00000026 System.Boolean UnityEngine.ResourceManagement.ChainOperation`2::InvokeWaitForCompletion()
// 0x00000027 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Execute()
// 0x00000028 System.Void UnityEngine.ResourceManagement.ChainOperation`2::OnWrappedCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000029 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Destroy()
// 0x0000002A System.Void UnityEngine.ResourceManagement.ChainOperation`2::ReleaseDependencies()
// 0x0000002B UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.ChainOperation`2::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x0000002C System.Void UnityEngine.ResourceManagement.ChainOperation`2::RefreshDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x0000002D System.Single UnityEngine.ResourceManagement.ChainOperation`2::get_Progress()
// 0x0000002E System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::.ctor()
// 0x0000002F System.String UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::get_DebugName()
// 0x00000030 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000031 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>,System.Boolean)
// 0x00000032 System.Boolean UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::InvokeWaitForCompletion()
// 0x00000033 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Execute()
// 0x00000034 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::OnWrappedCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000035 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Destroy()
// 0x00000036 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::ReleaseDependencies()
// 0x00000037 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x00000038 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::RefreshDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x00000039 System.Single UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::get_Progress()
// 0x0000003A System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception> UnityEngine.ResourceManagement.ResourceManager::get_ExceptionHandler()
extern void ResourceManager_get_ExceptionHandler_m1594AC5FC006CC770B2A0E15C739EB845A9C8306 (void);
// 0x0000003B System.Void UnityEngine.ResourceManagement.ResourceManager::set_ExceptionHandler(System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception>)
extern void ResourceManager_set_ExceptionHandler_m2CD5AEF214219E421AF7BAA9057274336F72C6AF (void);
// 0x0000003C System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String> UnityEngine.ResourceManagement.ResourceManager::get_InternalIdTransformFunc()
extern void ResourceManager_get_InternalIdTransformFunc_mB5CBFF62F538F2E9387BC31FA5DF4FDBC39813AB (void);
// 0x0000003D System.Void UnityEngine.ResourceManagement.ResourceManager::set_InternalIdTransformFunc(System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String>)
extern void ResourceManager_set_InternalIdTransformFunc_m8F62669979EAECF8ED0E7FA2CDE7D219826A250B (void);
// 0x0000003E System.String UnityEngine.ResourceManagement.ResourceManager::TransformInternalId(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_TransformInternalId_m5B3836837396CC2CBF26C9D1B77809A73A835669 (void);
// 0x0000003F System.Action`1<UnityEngine.Networking.UnityWebRequest> UnityEngine.ResourceManagement.ResourceManager::get_WebRequestOverride()
extern void ResourceManager_get_WebRequestOverride_m973A2421D534233FDB86FA23BA756F3AEFA0FECB (void);
// 0x00000040 System.Void UnityEngine.ResourceManagement.ResourceManager::set_WebRequestOverride(System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void ResourceManager_set_WebRequestOverride_m5BA4765700261D0E917CC8F52215973E54ACD870 (void);
// 0x00000041 System.Int32 UnityEngine.ResourceManagement.ResourceManager::get_OperationCacheCount()
extern void ResourceManager_get_OperationCacheCount_m4E3CB779AE01BD54B04544B20A761188B7C3DC40 (void);
// 0x00000042 System.Int32 UnityEngine.ResourceManagement.ResourceManager::get_InstanceOperationCount()
extern void ResourceManager_get_InstanceOperationCount_m7FF092E82B500F42DD10FBB204AA6D5113BF979D (void);
// 0x00000043 System.Void UnityEngine.ResourceManagement.ResourceManager::AddUpdateReceiver(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_AddUpdateReceiver_mD35433816B5B3C8F3D986F8E9DE70872341CE4F3 (void);
// 0x00000044 System.Void UnityEngine.ResourceManagement.ResourceManager::RemoveUpdateReciever(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_RemoveUpdateReciever_m53AAAD4F31B434164DCF15F4CF787FD104F490D8 (void);
// 0x00000045 UnityEngine.ResourceManagement.Util.IAllocationStrategy UnityEngine.ResourceManagement.ResourceManager::get_Allocator()
extern void ResourceManager_get_Allocator_m617C32C69B3AF34BA88127E5EE31B8B2731AE685 (void);
// 0x00000046 System.Void UnityEngine.ResourceManagement.ResourceManager::set_Allocator(UnityEngine.ResourceManagement.Util.IAllocationStrategy)
extern void ResourceManager_set_Allocator_m605775DC52E410670040187897F0C17ED45BC6FB (void);
// 0x00000047 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider> UnityEngine.ResourceManagement.ResourceManager::get_ResourceProviders()
extern void ResourceManager_get_ResourceProviders_m0B0336D1A403DDDB560EB832375B0205AC2B914F (void);
// 0x00000048 UnityEngine.Networking.CertificateHandler UnityEngine.ResourceManagement.ResourceManager::get_CertificateHandlerInstance()
extern void ResourceManager_get_CertificateHandlerInstance_m2E6389B8CA321D6850C1EF792F7F30CB85E6AD53 (void);
// 0x00000049 System.Void UnityEngine.ResourceManagement.ResourceManager::set_CertificateHandlerInstance(UnityEngine.Networking.CertificateHandler)
extern void ResourceManager_set_CertificateHandlerInstance_m58A06D27892464F5DCB0CF162A4491F63D75BD95 (void);
// 0x0000004A System.Void UnityEngine.ResourceManagement.ResourceManager::.ctor(UnityEngine.ResourceManagement.Util.IAllocationStrategy)
extern void ResourceManager__ctor_m8FC110F5D15AE6BB0225B82EE23B6FE589384A63 (void);
// 0x0000004B System.Void UnityEngine.ResourceManagement.ResourceManager::OnObjectAdded(System.Object)
extern void ResourceManager_OnObjectAdded_mB1214C5B5A091942F1BC9F78791FEFF111A5FA3D (void);
// 0x0000004C System.Void UnityEngine.ResourceManagement.ResourceManager::OnObjectRemoved(System.Object)
extern void ResourceManager_OnObjectRemoved_m65985C8F6D034E3125CC0A590CC5C986BEAC36C6 (void);
// 0x0000004D System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterForCallbacks()
extern void ResourceManager_RegisterForCallbacks_m446989CFD30CEAE21A30EDBC7CF5F19293BD57CB (void);
// 0x0000004E System.Void UnityEngine.ResourceManagement.ResourceManager::ClearDiagnosticsCallback()
extern void ResourceManager_ClearDiagnosticsCallback_m10B6405E9C6F22FB92FC2F4DF590237A2129D696 (void);
// 0x0000004F System.Void UnityEngine.ResourceManagement.ResourceManager::ClearDiagnosticCallbacks()
extern void ResourceManager_ClearDiagnosticCallbacks_mFCFC4DB25C14D5D116EF3C20048126747AB38B4F (void);
// 0x00000050 System.Void UnityEngine.ResourceManagement.ResourceManager::UnregisterDiagnosticCallback(System.Action`1<UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext>)
extern void ResourceManager_UnregisterDiagnosticCallback_m2F39BED801D8540D2BC2E4DFC0585A9B7C06F77A (void);
// 0x00000051 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterDiagnosticCallback(System.Action`4<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventType,System.Int32,System.Object>)
extern void ResourceManager_RegisterDiagnosticCallback_m670CAA7C86A37081D1D46BB9ACF98BA9DB7E04EA (void);
// 0x00000052 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterDiagnosticCallback(System.Action`1<UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext>)
extern void ResourceManager_RegisterDiagnosticCallback_m5668FFB5E61FADC8859DE00A516B53183A493D83 (void);
// 0x00000053 System.Void UnityEngine.ResourceManagement.ResourceManager::PostDiagnosticEvent(UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext)
extern void ResourceManager_PostDiagnosticEvent_m7F57B61B13435BAFBF1D99DCC0D1B6579E904218 (void);
// 0x00000054 UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider UnityEngine.ResourceManagement.ResourceManager::GetResourceProvider(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_GetResourceProvider_m5C1B277D488DA44F80077476F23451C8B9BB9840 (void);
// 0x00000055 System.Type UnityEngine.ResourceManagement.ResourceManager::GetDefaultTypeForLocation(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_GetDefaultTypeForLocation_mACD27E5C1DB210607222FC55B6728A9FB96C9B00 (void);
// 0x00000056 System.Int32 UnityEngine.ResourceManagement.ResourceManager::CalculateLocationsHash(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Type)
extern void ResourceManager_CalculateLocationsHash_mE546C5ECB0C13363246BED722B7E5FC899918BA0 (void);
// 0x00000057 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager::ProvideResource(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Type,System.Boolean)
extern void ResourceManager_ProvideResource_mA287F265CE43E2806FC06637F07A4CE3BDD6C00E (void);
// 0x00000058 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::ProvideResource(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000059 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::StartOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x0000005A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager::StartOperation(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_StartOperation_m5F3A0530300FE62FCCA0AA55ADB4AF825AC0E630 (void);
// 0x0000005B System.Void UnityEngine.ResourceManagement.ResourceManager::OnInstanceOperationDestroy(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnInstanceOperationDestroy_m51046A99199FE1384F610C8B2D4F9BB45909282F (void);
// 0x0000005C System.Void UnityEngine.ResourceManagement.ResourceManager::OnOperationDestroyNonCached(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnOperationDestroyNonCached_m6BDCF67A3CFC9BDD9A33B7F8693B8E583C00182E (void);
// 0x0000005D System.Void UnityEngine.ResourceManagement.ResourceManager::OnOperationDestroyCached(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnOperationDestroyCached_m68BA43A0BC60683A5FB3DC9CCFC003D03C4CF383 (void);
// 0x0000005E T UnityEngine.ResourceManagement.ResourceManager::CreateOperation(System.Type,System.Int32,UnityEngine.ResourceManagement.Util.IOperationCacheKey,System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x0000005F System.Void UnityEngine.ResourceManagement.ResourceManager::AddOperationToCache(UnityEngine.ResourceManagement.Util.IOperationCacheKey,UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_AddOperationToCache_m5B8925E6779BCAE8A5C33FFB7F10658EDEF64C8B (void);
// 0x00000060 System.Boolean UnityEngine.ResourceManagement.ResourceManager::RemoveOperationFromCache(UnityEngine.ResourceManagement.Util.IOperationCacheKey)
extern void ResourceManager_RemoveOperationFromCache_m5A131A13A900F20064CBF84CAF3AF0E00251771A (void);
// 0x00000061 System.Boolean UnityEngine.ResourceManagement.ResourceManager::IsOperationCached(UnityEngine.ResourceManagement.Util.IOperationCacheKey)
extern void ResourceManager_IsOperationCached_m9F43FA5BA0182B2BC19518C543431C9089CEC5D5 (void);
// 0x00000062 System.Int32 UnityEngine.ResourceManagement.ResourceManager::CachedOperationCount()
extern void ResourceManager_CachedOperationCount_m4B99483D311D33CE8FF7E38F87116FD602BF7F77 (void);
// 0x00000063 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateCompletedOperation(TObject,System.String)
// 0x00000064 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateCompletedOperationWithException(TObject,System.Exception)
// 0x00000065 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateCompletedOperationInternal(TObject,System.Boolean,System.Exception,System.Boolean)
// 0x00000066 System.Void UnityEngine.ResourceManagement.ResourceManager::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_Release_m1BF99536656867FEEFBEF3A3F4A290AA66402EB7 (void);
// 0x00000067 System.Void UnityEngine.ResourceManagement.ResourceManager::Acquire(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_Acquire_m071783042B4ACA5A506D43DCAF92DE8EBF94E5C2 (void);
// 0x00000068 UnityEngine.ResourceManagement.AsyncOperations.GroupOperation UnityEngine.ResourceManagement.ResourceManager::AcquireGroupOpFromCache(UnityEngine.ResourceManagement.Util.IOperationCacheKey)
extern void ResourceManager_AcquireGroupOpFromCache_m75CB8FE11064B47C4923C2830D58C9287E368C77 (void);
// 0x00000069 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGroupOperation(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>)
// 0x0000006A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGroupOperation(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean)
// 0x0000006B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGenericGroupOperation(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean)
extern void ResourceManager_CreateGenericGroupOperation_m637EFAA4DDCCCB4DB5BDE75BBB4B53C61848D8F2 (void);
// 0x0000006C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::ProvideResourceGroupCached(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Int32,System.Type,System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean)
extern void ResourceManager_ProvideResourceGroupCached_mE1057C4DB61854D82B4056E22EA6E37D9B4A2B37 (void);
// 0x0000006D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.ResourceManagement.ResourceManager::ProvideResources(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Action`1<TObject>)
// 0x0000006E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.ResourceManagement.ResourceManager::ProvideResources(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean,System.Action`1<TObject>)
// 0x0000006F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000070 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000071 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>,System.Boolean)
// 0x00000072 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>,System.Boolean)
// 0x00000073 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceManager::ProvideScene(UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void ResourceManager_ProvideScene_mB6328DC75FFFD9CEF4B60511F895F1E2CF709A5D (void);
// 0x00000074 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceManager::ReleaseScene(UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void ResourceManager_ReleaseScene_m459CB8CAAB851FA3495F881AEFAD1C3FF29106FA (void);
// 0x00000075 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.ResourceManagement.ResourceManager::ProvideInstance(UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
extern void ResourceManager_ProvideInstance_m9C1635807FC0ED58FB88BEF2B803BC9C1AAD14F3 (void);
// 0x00000076 System.Void UnityEngine.ResourceManagement.ResourceManager::CleanupSceneInstances(UnityEngine.SceneManagement.Scene)
extern void ResourceManager_CleanupSceneInstances_m9B491B22C7532958A6751CDD08384E7474D1E8CC (void);
// 0x00000077 System.Void UnityEngine.ResourceManagement.ResourceManager::ExecuteDeferredCallbacks()
extern void ResourceManager_ExecuteDeferredCallbacks_mA834F0420F4C269DC21BCB39AFF67C51325AE373 (void);
// 0x00000078 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterForDeferredCallback(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Boolean)
extern void ResourceManager_RegisterForDeferredCallback_mDE63306BA1891888B6E2C57E74D4CF8AF8F51952 (void);
// 0x00000079 System.Void UnityEngine.ResourceManagement.ResourceManager::Update(System.Single)
extern void ResourceManager_Update_mA2F03AF254D8794D37A7821F905AA8FF8EC97FFF (void);
// 0x0000007A System.Void UnityEngine.ResourceManagement.ResourceManager::Dispose()
extern void ResourceManager_Dispose_m57783F295F3FE72496F8B2CC2BC06C671643CF53 (void);
// 0x0000007B System.Void UnityEngine.ResourceManagement.ResourceManager::.cctor()
extern void ResourceManager__cctor_m75EFB1F6063CB219CCEBBC30CF586872EEAC4F82 (void);
// 0x0000007C System.Void UnityEngine.ResourceManagement.ResourceManager::<.ctor>b__53_0(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_U3C_ctorU3Eb__53_0_m8C9E434B0D88F1BD078FB518418F84516976C57E (void);
// 0x0000007D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::get_OperationHandle()
extern void DiagnosticEventContext_get_OperationHandle_m870675A1B170E90324CB13D4320F2DEF29D6A1BD (void);
// 0x0000007E UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventType UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::get_Type()
extern void DiagnosticEventContext_get_Type_m3C77CCA8778F7D55230D59FD0B6DEB37F910B15E (void);
// 0x0000007F System.Int32 UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::get_EventValue()
extern void DiagnosticEventContext_get_EventValue_m186FB7D8DFACBD40B9A7FC254685B544C5FC5448 (void);
// 0x00000080 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::get_Location()
extern void DiagnosticEventContext_get_Location_mC7B8C8151070981ACEF34A2110E16024587691B7 (void);
// 0x00000081 System.Object UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::get_Context()
extern void DiagnosticEventContext_get_Context_mB145AF762ACC916F5CBCD03B392B36CB6BBD5D91 (void);
// 0x00000082 System.String UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::get_Error()
extern void DiagnosticEventContext_get_Error_m6758C6A5F0F1C59C8AFB8A5D5FEF7547C806D808 (void);
// 0x00000083 System.Void UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::.ctor(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventType,System.Int32,System.String,System.Object)
extern void DiagnosticEventContext__ctor_m46A56FA95388629125EDEE74C865060584274238 (void);
// 0x00000084 System.Void UnityEngine.ResourceManagement.ResourceManager/CompletedOperation`1::.ctor()
// 0x00000085 System.Void UnityEngine.ResourceManagement.ResourceManager/CompletedOperation`1::Init(TObject,System.Boolean,System.String,System.Boolean)
// 0x00000086 System.Void UnityEngine.ResourceManagement.ResourceManager/CompletedOperation`1::Init(TObject,System.Boolean,System.Exception,System.Boolean)
// 0x00000087 System.String UnityEngine.ResourceManagement.ResourceManager/CompletedOperation`1::get_DebugName()
// 0x00000088 System.Boolean UnityEngine.ResourceManagement.ResourceManager/CompletedOperation`1::InvokeWaitForCompletion()
// 0x00000089 System.Void UnityEngine.ResourceManagement.ResourceManager/CompletedOperation`1::Execute()
// 0x0000008A System.Void UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void InstanceOperation_Init_mE5EE6B621B9B927D75F2281EDDEC91E5DE16A9CA (void);
// 0x0000008B UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
extern void InstanceOperation_GetDownloadStatus_mA1C48F14B892497D46715D8F8A0160232A97B90F (void);
// 0x0000008C System.Void UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void InstanceOperation_GetDependencies_m26D3CB8DE0FCFC784B2C9663A482F66BD92C72E5 (void);
// 0x0000008D System.String UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::get_DebugName()
extern void InstanceOperation_get_DebugName_mE303D6480456152374BCE927EE4737D33E70E7FE (void);
// 0x0000008E UnityEngine.SceneManagement.Scene UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::InstanceScene()
extern void InstanceOperation_InstanceScene_m6AF4F1151492C24B07C214A98DBAB5CBE64E3DD6 (void);
// 0x0000008F System.Void UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::Destroy()
extern void InstanceOperation_Destroy_mC17ABC3FF150BAE7C7C6DD031B741D1099C174AE (void);
// 0x00000090 System.Single UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::get_Progress()
extern void InstanceOperation_get_Progress_mFB3EC02597F244CEE84815611AFD4074E1028883 (void);
// 0x00000091 System.Boolean UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::InvokeWaitForCompletion()
extern void InstanceOperation_InvokeWaitForCompletion_m47D017281ACE110F97759C17837B48FD06827D5D (void);
// 0x00000092 System.Void UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::Execute()
extern void InstanceOperation_Execute_mAED30650EF9D31AB1A5CC87410964DFB4149BF2C (void);
// 0x00000093 System.Void UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::.ctor()
extern void InstanceOperation__ctor_mBE633769231DE956B1D301282CC4D488F52495C8 (void);
// 0x00000094 System.Void UnityEngine.ResourceManagement.ResourceManager/<>c__DisplayClass92_0`1::.ctor()
// 0x00000095 System.Void UnityEngine.ResourceManagement.ResourceManager/<>c__DisplayClass92_0`1::<ProvideResources>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000096 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.ResourceManagement.ResourceManager/<>c__DisplayClass92_0`1::<ProvideResources>b__1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000097 System.Void UnityEngine.ResourceManagement.IUpdateReceiver::Update(System.Single)
// 0x00000098 System.Boolean UnityEngine.ResourceManagement.WebRequestQueueOperation::get_IsDone()
extern void WebRequestQueueOperation_get_IsDone_mA24CD38542F301A96BA73352520D86348D483079 (void);
// 0x00000099 System.Void UnityEngine.ResourceManagement.WebRequestQueueOperation::.ctor(UnityEngine.Networking.UnityWebRequest)
extern void WebRequestQueueOperation__ctor_mE3E3145649464B791DDC02D0BA75AEC6A5CA0B7E (void);
// 0x0000009A System.Void UnityEngine.ResourceManagement.WebRequestQueueOperation::Complete(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void WebRequestQueueOperation_Complete_mBF0118DBD2907500B5A87F494B14672DBF534763 (void);
// 0x0000009B System.Void UnityEngine.ResourceManagement.WebRequestQueue::SetMaxConcurrentRequests(System.Int32)
extern void WebRequestQueue_SetMaxConcurrentRequests_m717A2C2435BDA305652D8A6E8CE898EF6185F09E (void);
// 0x0000009C UnityEngine.ResourceManagement.WebRequestQueueOperation UnityEngine.ResourceManagement.WebRequestQueue::QueueRequest(UnityEngine.Networking.UnityWebRequest)
extern void WebRequestQueue_QueueRequest_m54F1A56E5082365292141AC67CEFEBEEA52F04AE (void);
// 0x0000009D System.Void UnityEngine.ResourceManagement.WebRequestQueue::OnWebAsyncOpComplete(UnityEngine.AsyncOperation)
extern void WebRequestQueue_OnWebAsyncOpComplete_m27F37E235E00801CCD54A4D8ED49538371FC0180 (void);
// 0x0000009E System.Void UnityEngine.ResourceManagement.WebRequestQueue::.cctor()
extern void WebRequestQueue__cctor_m3B750ADE0715FB14DEBF388CA01266DF0B455925 (void);
// 0x0000009F System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor()
extern void ResourceManagerException__ctor_m846D7FCC2C06373694800B5220DF73E6D26F6BA7 (void);
// 0x000000A0 System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor(System.String)
extern void ResourceManagerException__ctor_m9181C112F2C8E32E77CEDE21C4B5DF0F1CBB5948 (void);
// 0x000000A1 System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor(System.String,System.Exception)
extern void ResourceManagerException__ctor_mE76839DF077E4AB88010D559E270AB53D2509A66 (void);
// 0x000000A2 System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void ResourceManagerException__ctor_mCE13F6D2BC0FAE0B8B66A9C13CFABA3EF7879F04 (void);
// 0x000000A3 System.String UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::ToString()
extern void ResourceManagerException_ToString_m7411781F2735E93662661A41DF3A92E29E637DC8 (void);
// 0x000000A4 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::get_Location()
extern void UnknownResourceProviderException_get_Location_mC37FB4E0339053A4F3C5DFDEFAC15F8B4E1A7CA6 (void);
// 0x000000A5 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::set_Location(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void UnknownResourceProviderException_set_Location_mDE78145787A8CDF8058E1FE350F79F23C6E0121E (void);
// 0x000000A6 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void UnknownResourceProviderException__ctor_mA08582F29A5DF6471FEE1170D7EB38CAFEAB5C5F (void);
// 0x000000A7 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor()
extern void UnknownResourceProviderException__ctor_m4B7176BAE46F30C73450E5306F911BD53EDD151E (void);
// 0x000000A8 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(System.String)
extern void UnknownResourceProviderException__ctor_m365634FE703F611EDC55F3E4142B4E69EB72F682 (void);
// 0x000000A9 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(System.String,System.Exception)
extern void UnknownResourceProviderException__ctor_m682FE80405FA7A8FAFEBBA1F7605379338B7BB97 (void);
// 0x000000AA System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void UnknownResourceProviderException__ctor_m0729FF6A25B6CB353ECE181A7C907E15B64879FF (void);
// 0x000000AB System.String UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::get_Message()
extern void UnknownResourceProviderException_get_Message_mF411A2558453686665445094929B223C43FB6C80 (void);
// 0x000000AC System.String UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::ToString()
extern void UnknownResourceProviderException_ToString_m8E3FCBA2D2A41E951C60958FC604DC7D4A83C26E (void);
// 0x000000AD System.Void UnityEngine.ResourceManagement.Exceptions.OperationException::.ctor(System.String,System.Exception)
extern void OperationException__ctor_m59B04AEC4F7E07F7CAEEF3ABB2BB4E0CDE1C6C2B (void);
// 0x000000AE System.String UnityEngine.ResourceManagement.Exceptions.OperationException::ToString()
extern void OperationException_ToString_mD1EDB81DD592A9B2BB2FCCB74535194AB82135B8 (void);
// 0x000000AF System.Void UnityEngine.ResourceManagement.Exceptions.ProviderException::.ctor(System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Exception)
extern void ProviderException__ctor_m03D782F952F7BCE889D2F37690183D3B74E9579A (void);
// 0x000000B0 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.Exceptions.ProviderException::get_Location()
extern void ProviderException_get_Location_m77203D80F7C090A29D74481729AF9AB1C7026385 (void);
// 0x000000B1 System.Void UnityEngine.ResourceManagement.Exceptions.RemoteProviderException::.ctor(System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.Util.UnityWebRequestResult,System.Exception)
extern void RemoteProviderException__ctor_m4E8E891B128CE300B1972CCC8486576F6263A335 (void);
// 0x000000B2 UnityEngine.ResourceManagement.Util.UnityWebRequestResult UnityEngine.ResourceManagement.Exceptions.RemoteProviderException::get_WebRequestResult()
extern void RemoteProviderException_get_WebRequestResult_mC841D99699ACAAF04B768618B6A76360B291158F (void);
// 0x000000B3 System.String UnityEngine.ResourceManagement.Exceptions.RemoteProviderException::ToString()
extern void RemoteProviderException_ToString_mC7DE76A1E17748A03D7714F21AD779F74F096598 (void);
// 0x000000B4 System.Boolean UnityEngine.ResourceManagement.Util.ComponentSingleton`1::get_Exists()
// 0x000000B5 T UnityEngine.ResourceManagement.Util.ComponentSingleton`1::get_Instance()
// 0x000000B6 T UnityEngine.ResourceManagement.Util.ComponentSingleton`1::FindInstance()
// 0x000000B7 System.String UnityEngine.ResourceManagement.Util.ComponentSingleton`1::GetGameObjectName()
// 0x000000B8 T UnityEngine.ResourceManagement.Util.ComponentSingleton`1::CreateNewSingleton()
// 0x000000B9 System.Void UnityEngine.ResourceManagement.Util.ComponentSingleton`1::Awake()
// 0x000000BA System.Void UnityEngine.ResourceManagement.Util.ComponentSingleton`1::DestroySingleton()
// 0x000000BB System.Void UnityEngine.ResourceManagement.Util.ComponentSingleton`1::.ctor()
// 0x000000BC System.Collections.Generic.LinkedListNode`1<UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo> UnityEngine.ResourceManagement.Util.DelayedActionManager::GetNode(UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo&)
extern void DelayedActionManager_GetNode_m0DE3584622A131A6793E55EACD16CA4019947088 (void);
// 0x000000BD System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::Clear()
extern void DelayedActionManager_Clear_mCE2BE0F40CA9566F3AD0FBC7506D2AAE6FC99380 (void);
// 0x000000BE System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::DestroyWhenComplete()
extern void DelayedActionManager_DestroyWhenComplete_mA1D76FBA64E4BBB878914741B403BF2904F9DA01 (void);
// 0x000000BF System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::AddAction(System.Delegate,System.Single,System.Object[])
extern void DelayedActionManager_AddAction_m50BBD4B75371CB8B2A2230DF09216C2F85CF9262 (void);
// 0x000000C0 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::AddActionInternal(System.Delegate,System.Single,System.Object[])
extern void DelayedActionManager_AddActionInternal_m613DDDB8D8F5A44CBE0793FEDE3EA80E0FD0AC0B (void);
// 0x000000C1 System.Boolean UnityEngine.ResourceManagement.Util.DelayedActionManager::get_IsActive()
extern void DelayedActionManager_get_IsActive_mF1D78D5B0C9E9EA99B953C6F26868C53A02AC32F (void);
// 0x000000C2 System.Boolean UnityEngine.ResourceManagement.Util.DelayedActionManager::Wait(System.Single,System.Single)
extern void DelayedActionManager_Wait_m18E9BC16EFA84B02DCFCF3C56C31EF2F4B356F8F (void);
// 0x000000C3 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::LateUpdate()
extern void DelayedActionManager_LateUpdate_m54908B1CB07F9901C208ECD97A82BB62A9B70D91 (void);
// 0x000000C4 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::InternalLateUpdate(System.Single)
extern void DelayedActionManager_InternalLateUpdate_mD6B2568B45B54B79717B73422415240BFA28F37C (void);
// 0x000000C5 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::OnApplicationQuit()
extern void DelayedActionManager_OnApplicationQuit_m209580E2C7D3265295F1B1048880C96416F5E198 (void);
// 0x000000C6 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::.ctor()
extern void DelayedActionManager__ctor_mC4435A115C4DD5A9112A7B40CFD330F7D8340320 (void);
// 0x000000C7 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo::.ctor(System.Delegate,System.Single,System.Object[])
extern void DelegateInfo__ctor_mBB59764DCA627416685191C4BFFDF7E6B3F9EEB0 (void);
// 0x000000C8 System.Single UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo::get_InvocationTime()
extern void DelegateInfo_get_InvocationTime_mD426371CDDBF73985C41649D481C1C8032731780 (void);
// 0x000000C9 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo::set_InvocationTime(System.Single)
extern void DelegateInfo_set_InvocationTime_mDDF970305686BAF18D3644E6F8A0FBFAFF868A75 (void);
// 0x000000CA System.String UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo::ToString()
extern void DelegateInfo_ToString_m088DA64541A8A73664E1C101FE2726B1325AC5BC (void);
// 0x000000CB System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo::Invoke()
extern void DelegateInfo_Invoke_m9010FEA078121055ACEBF2CD5049762D439DDD8F (void);
// 0x000000CC System.Void UnityEngine.ResourceManagement.Util.LocationCacheKey::.ctor(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Type)
extern void LocationCacheKey__ctor_mD8A86424D643E0CE544415BA56B1498C93D828E6 (void);
// 0x000000CD System.Int32 UnityEngine.ResourceManagement.Util.LocationCacheKey::GetHashCode()
extern void LocationCacheKey_GetHashCode_m81D30AD7850BBD10C2E8B8CC0C9BD7D75E11F482 (void);
// 0x000000CE System.Boolean UnityEngine.ResourceManagement.Util.LocationCacheKey::Equals(System.Object)
extern void LocationCacheKey_Equals_m4787FEB8DCD0DECCF9D802B7725D466D4FCEE53E (void);
// 0x000000CF System.Boolean UnityEngine.ResourceManagement.Util.LocationCacheKey::Equals(UnityEngine.ResourceManagement.Util.IOperationCacheKey)
extern void LocationCacheKey_Equals_m845F8372C30A66C9FE2CD5B9492BEF6BF511C24B (void);
// 0x000000D0 System.Boolean UnityEngine.ResourceManagement.Util.LocationCacheKey::Equals(UnityEngine.ResourceManagement.Util.LocationCacheKey)
extern void LocationCacheKey_Equals_m2C6339CC5C0E2787022952381AE1B19F7937900F (void);
// 0x000000D1 System.Void UnityEngine.ResourceManagement.Util.DependenciesCacheKey::.ctor(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Int32)
extern void DependenciesCacheKey__ctor_m8310CEAE00B35CE8ED5B164EE015922CB36B4056 (void);
// 0x000000D2 System.Int32 UnityEngine.ResourceManagement.Util.DependenciesCacheKey::GetHashCode()
extern void DependenciesCacheKey_GetHashCode_m756877F9727C1D27037788709F55D7F037FB5793 (void);
// 0x000000D3 System.Boolean UnityEngine.ResourceManagement.Util.DependenciesCacheKey::Equals(System.Object)
extern void DependenciesCacheKey_Equals_m95B4A80078B035E0241DA96F5C0154268DF79048 (void);
// 0x000000D4 System.Boolean UnityEngine.ResourceManagement.Util.DependenciesCacheKey::Equals(UnityEngine.ResourceManagement.Util.IOperationCacheKey)
extern void DependenciesCacheKey_Equals_m7769DCAB37B09832B486E3F2C6913C3787AD4694 (void);
// 0x000000D5 System.Boolean UnityEngine.ResourceManagement.Util.DependenciesCacheKey::Equals(UnityEngine.ResourceManagement.Util.DependenciesCacheKey)
extern void DependenciesCacheKey_Equals_m7551BB00E98F881DE4B9F902E04D6C5F51C4E66B (void);
// 0x000000D6 System.Void UnityEngine.ResourceManagement.Util.AsyncOpHandlesCacheKey::.ctor(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOpHandlesCacheKey__ctor_m9215A7B7270B8BC67626E7E23DC671134BD16770 (void);
// 0x000000D7 System.Int32 UnityEngine.ResourceManagement.Util.AsyncOpHandlesCacheKey::GetHashCode()
extern void AsyncOpHandlesCacheKey_GetHashCode_mBE006B60A6F6BBC8E9B2D46EE53F4A2EBDD1D869 (void);
// 0x000000D8 System.Boolean UnityEngine.ResourceManagement.Util.AsyncOpHandlesCacheKey::Equals(System.Object)
extern void AsyncOpHandlesCacheKey_Equals_m8ACA6D8CC059FAB6EC656C7ADF99DF0046EFE086 (void);
// 0x000000D9 System.Boolean UnityEngine.ResourceManagement.Util.AsyncOpHandlesCacheKey::Equals(UnityEngine.ResourceManagement.Util.IOperationCacheKey)
extern void AsyncOpHandlesCacheKey_Equals_m6B07F82C191F639D3CE866ABC95FBF162B251904 (void);
// 0x000000DA System.Boolean UnityEngine.ResourceManagement.Util.AsyncOpHandlesCacheKey::Equals(UnityEngine.ResourceManagement.Util.AsyncOpHandlesCacheKey)
extern void AsyncOpHandlesCacheKey_Equals_mF0F4D09486DA5EECFFA9D1651AE5C9D980F0CCA5 (void);
// 0x000000DB System.Boolean UnityEngine.ResourceManagement.Util.LocationUtils::LocationEquals(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void LocationUtils_LocationEquals_m8B9AC694D195DEACE9F8A54BC6920C2AA565E6FF (void);
// 0x000000DC System.Boolean UnityEngine.ResourceManagement.Util.LocationUtils::DependenciesEqual(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>)
extern void LocationUtils_DependenciesEqual_mCE6FC543947C36086C53A2D3DA63854C83AD2F79 (void);
// 0x000000DD System.Boolean UnityEngine.ResourceManagement.Util.IInitializableObject::Initialize(System.String,System.String)
// 0x000000DE UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.ResourceManagement.Util.IInitializableObject::InitializeAsync(UnityEngine.ResourceManagement.ResourceManager,System.String,System.String)
// 0x000000DF System.String UnityEngine.ResourceManagement.Util.IObjectInitializationDataProvider::get_Name()
// 0x000000E0 UnityEngine.ResourceManagement.Util.ObjectInitializationData UnityEngine.ResourceManagement.Util.IObjectInitializationDataProvider::CreateObjectInitializationData()
// 0x000000E1 System.Object UnityEngine.ResourceManagement.Util.IAllocationStrategy::New(System.Type,System.Int32)
// 0x000000E2 System.Void UnityEngine.ResourceManagement.Util.IAllocationStrategy::Release(System.Int32,System.Object)
// 0x000000E3 System.Object UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::New(System.Type,System.Int32)
extern void DefaultAllocationStrategy_New_m08D4B93377956FCA42653F39C2675229759A80D1 (void);
// 0x000000E4 System.Void UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::Release(System.Int32,System.Object)
extern void DefaultAllocationStrategy_Release_m8AD07C016B04002C080A2745A6892A6852DD6D06 (void);
// 0x000000E5 System.Void UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::.ctor()
extern void DefaultAllocationStrategy__ctor_mAA0485502871FF659EED040E6835C0C9053344B9 (void);
// 0x000000E6 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void LRUCacheAllocationStrategy__ctor_m6AB5B8D1E389B9300ABFC78F7C1B9F33FE0977C9 (void);
// 0x000000E7 System.Collections.Generic.List`1<System.Object> UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::GetPool()
extern void LRUCacheAllocationStrategy_GetPool_mB24B50035BCC44F88A78479974237784205D6914 (void);
// 0x000000E8 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::ReleasePool(System.Collections.Generic.List`1<System.Object>)
extern void LRUCacheAllocationStrategy_ReleasePool_mA171571331A0C7B2C747FAC28483247D5F83B674 (void);
// 0x000000E9 System.Object UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::New(System.Type,System.Int32)
extern void LRUCacheAllocationStrategy_New_m85F22B7C23EB9A073651189EA98D08B0292AF3F4 (void);
// 0x000000EA System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::Release(System.Int32,System.Object)
extern void LRUCacheAllocationStrategy_Release_m37E39356812AF84792B4E23D0D34453DEB84E528 (void);
// 0x000000EB System.Void UnityEngine.ResourceManagement.Util.SerializedTypeRestrictionAttribute::.ctor()
extern void SerializedTypeRestrictionAttribute__ctor_m4060A47D9718A425D3C39090B02C1967B3A1D0E4 (void);
// 0x000000EC System.Collections.Generic.LinkedListNode`1<T> UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::Acquire(T)
// 0x000000ED System.Void UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::Release(System.Collections.Generic.LinkedListNode`1<T>)
// 0x000000EE System.Int32 UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::get_CreatedNodeCount()
// 0x000000EF System.Int32 UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::get_CachedNodeCount()
// 0x000000F0 System.Void UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::.ctor()
// 0x000000F1 System.Collections.Generic.LinkedListNode`1<T> UnityEngine.ResourceManagement.Util.GlobalLinkedListNodeCache`1::Acquire(T)
// 0x000000F2 System.Void UnityEngine.ResourceManagement.Util.GlobalLinkedListNodeCache`1::Release(System.Collections.Generic.LinkedListNode`1<T>)
// 0x000000F3 System.String UnityEngine.ResourceManagement.Util.SerializedType::get_AssemblyName()
extern void SerializedType_get_AssemblyName_m2DBDDEF51A9338BF298BCBBD2EE398D72E963635 (void);
// 0x000000F4 System.String UnityEngine.ResourceManagement.Util.SerializedType::get_ClassName()
extern void SerializedType_get_ClassName_m06DB26816F632CCAF380F818EA666D85C409D8B2 (void);
// 0x000000F5 System.String UnityEngine.ResourceManagement.Util.SerializedType::ToString()
extern void SerializedType_ToString_m85C9B44C29867CD01DAA7CB5A365577818B4212A (void);
// 0x000000F6 System.Type UnityEngine.ResourceManagement.Util.SerializedType::get_Value()
extern void SerializedType_get_Value_m412D3B47698A74EFA8A3B29B4D102842714BD6B0 (void);
// 0x000000F7 System.Void UnityEngine.ResourceManagement.Util.SerializedType::set_Value(System.Type)
extern void SerializedType_set_Value_m0E1021AEE9B0E8B98800E79734E8B140032B9E54 (void);
// 0x000000F8 System.Boolean UnityEngine.ResourceManagement.Util.SerializedType::get_ValueChanged()
extern void SerializedType_get_ValueChanged_m1B7B62A60583EE0D14418C391367985DBE6E7562 (void);
// 0x000000F9 System.Void UnityEngine.ResourceManagement.Util.SerializedType::set_ValueChanged(System.Boolean)
extern void SerializedType_set_ValueChanged_m7EB0C0A5F56B0594CC3578191F4E26CAC1D08D7D (void);
// 0x000000FA System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_Id()
extern void ObjectInitializationData_get_Id_m1455892EB3BA6CD3ED9F9A73F6401285E5486C65 (void);
// 0x000000FB UnityEngine.ResourceManagement.Util.SerializedType UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_ObjectType()
extern void ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532 (void);
// 0x000000FC System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_Data()
extern void ObjectInitializationData_get_Data_m026CC155470843EE675C47D80DDE91CFA7B05AA0 (void);
// 0x000000FD System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::ToString()
extern void ObjectInitializationData_ToString_m43DC1ED8748A5B962C707EA72E529243ABDA37D6 (void);
// 0x000000FE TObject UnityEngine.ResourceManagement.Util.ObjectInitializationData::CreateInstance(System.String)
// 0x000000FF UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.Util.ObjectInitializationData::GetAsyncInitHandle(UnityEngine.ResourceManagement.ResourceManager,System.String)
extern void ObjectInitializationData_GetAsyncInitHandle_m3050F492A6972087E9AA5371C7946CBE5F29AC42 (void);
// 0x00000100 System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::ExtractKeyAndSubKey(System.Object,System.String&,System.String&)
extern void ResourceManagerConfig_ExtractKeyAndSubKey_m48B90CB062DF102F56698425FE9F75795239ED15 (void);
// 0x00000101 System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::IsPathRemote(System.String)
extern void ResourceManagerConfig_IsPathRemote_m4A0EC7B1CEFE8C1B172D2D7CA1FCC2EDDF09FA12 (void);
// 0x00000102 System.String UnityEngine.ResourceManagement.Util.ResourceManagerConfig::StripQueryParameters(System.String)
extern void ResourceManagerConfig_StripQueryParameters_m246B6D7D06C982C37967D2D2B519A388D5C1982C (void);
// 0x00000103 System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::ShouldPathUseWebRequest(System.String)
extern void ResourceManagerConfig_ShouldPathUseWebRequest_mC7BD63058F88117DA2622430B22DB7B80D138D76 (void);
// 0x00000104 System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::PlatformCanLoadLocallyFromUrlPath()
extern void ResourceManagerConfig_PlatformCanLoadLocallyFromUrlPath_mD57B260F40C413C2DE40A28CD7E3237763DB203A (void);
// 0x00000105 System.Array UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateArrayResult(System.Type,UnityEngine.Object[])
extern void ResourceManagerConfig_CreateArrayResult_mE028A19D4D9E4908BE5911B8E7A21A85D18237B6 (void);
// 0x00000106 TObject UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateArrayResult(UnityEngine.Object[])
// 0x00000107 System.Collections.IList UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateListResult(System.Type,UnityEngine.Object[])
extern void ResourceManagerConfig_CreateListResult_mE82A475A890F6091076CAC624DF32496CD75CF1F (void);
// 0x00000108 TObject UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateListResult(UnityEngine.Object[])
// 0x00000109 System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::IsInstance()
// 0x0000010A System.Boolean UnityEngine.ResourceManagement.Util.UnityWebRequestUtilities::RequestHasErrors(UnityEngine.Networking.UnityWebRequest,UnityEngine.ResourceManagement.Util.UnityWebRequestResult&)
extern void UnityWebRequestUtilities_RequestHasErrors_m28EF4B6F3522720AAF6FB3BD92E43018E38CFDE0 (void);
// 0x0000010B System.Void UnityEngine.ResourceManagement.Util.UnityWebRequestUtilities::.ctor()
extern void UnityWebRequestUtilities__ctor_mBBF82A3247026EEA4DBDFD2D5A02434C4E3F3687 (void);
// 0x0000010C System.Void UnityEngine.ResourceManagement.Util.UnityWebRequestResult::.ctor(UnityEngine.Networking.UnityWebRequest)
extern void UnityWebRequestResult__ctor_m0AFC547859BBBEABF97E9C8724FF7026CC5C5C56 (void);
// 0x0000010D System.String UnityEngine.ResourceManagement.Util.UnityWebRequestResult::ToString()
extern void UnityWebRequestResult_ToString_m1EB1F18F0AA739DC863DE424508CAEE4E8A0814B (void);
// 0x0000010E System.String UnityEngine.ResourceManagement.Util.UnityWebRequestResult::get_Error()
extern void UnityWebRequestResult_get_Error_m1B7977325F245AAC94E787466A19E155475D1459 (void);
// 0x0000010F System.Void UnityEngine.ResourceManagement.Util.UnityWebRequestResult::set_Error(System.String)
extern void UnityWebRequestResult_set_Error_m60D36D971598B57CBEC9F212C5D3E407109D09FB (void);
// 0x00000110 System.Int64 UnityEngine.ResourceManagement.Util.UnityWebRequestResult::get_ResponseCode()
extern void UnityWebRequestResult_get_ResponseCode_m8870B4512CF753942C4A697FC2B1AA18177EF8D0 (void);
// 0x00000111 System.String UnityEngine.ResourceManagement.Util.UnityWebRequestResult::get_Method()
extern void UnityWebRequestResult_get_Method_m4E86DCE65AE49CE851CA8AE4E26611AB42089D47 (void);
// 0x00000112 System.String UnityEngine.ResourceManagement.Util.UnityWebRequestResult::get_Url()
extern void UnityWebRequestResult_get_Url_mC042408942366A6D90B9ACA42FAC66086577A3E0 (void);
// 0x00000113 System.Void UnityEngine.ResourceManagement.ResourceProviders.DownloadOnlyLocation::.ctor(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void DownloadOnlyLocation__ctor_mCFDD58E4623B02114520A4E0698DC42FD230461D (void);
// 0x00000114 UnityEngine.AssetBundle UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource::GetAssetBundle()
// 0x00000115 System.String UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_Hash()
extern void AssetBundleRequestOptions_get_Hash_mB4B18C05B59265AA39B3A02C0688DE1D1C88711A (void);
// 0x00000116 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_Hash(System.String)
extern void AssetBundleRequestOptions_set_Hash_mE0924B6592A1DFC9D594E11FF135AD1E87B41D3F (void);
// 0x00000117 System.UInt32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_Crc()
extern void AssetBundleRequestOptions_get_Crc_m2BA7D512A54EA6075A0CF024D012B6433C8A9A4E (void);
// 0x00000118 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_Crc(System.UInt32)
extern void AssetBundleRequestOptions_set_Crc_m2FB9FEAAEBC071272C46D9F8C02AE3BC5FBCD635 (void);
// 0x00000119 System.Int32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_Timeout()
extern void AssetBundleRequestOptions_get_Timeout_m42D9843C4E83D152DAE0CE4CD3E8E65E0623080B (void);
// 0x0000011A System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_Timeout(System.Int32)
extern void AssetBundleRequestOptions_set_Timeout_m40BFCFEF67A5C0D73A9F289EF8AA6A885479FC50 (void);
// 0x0000011B System.Boolean UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_ChunkedTransfer()
extern void AssetBundleRequestOptions_get_ChunkedTransfer_m5D4E14BA8EB284968D3BD0CFC5473B69A1FB04CD (void);
// 0x0000011C System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_ChunkedTransfer(System.Boolean)
extern void AssetBundleRequestOptions_set_ChunkedTransfer_m790165DE615D3A3D0CE204FEE2F4133D719CE499 (void);
// 0x0000011D System.Int32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_RedirectLimit()
extern void AssetBundleRequestOptions_get_RedirectLimit_m099ACB2C16C55D0C615FF15E71908E3125D7F8AC (void);
// 0x0000011E System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_RedirectLimit(System.Int32)
extern void AssetBundleRequestOptions_set_RedirectLimit_m0B11D7F0C5B94AD17B0C7E1729130E8B5BE5D7E7 (void);
// 0x0000011F System.Int32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_RetryCount()
extern void AssetBundleRequestOptions_get_RetryCount_m19487390C594350D557346716F1CB3C0B38C2B3F (void);
// 0x00000120 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_RetryCount(System.Int32)
extern void AssetBundleRequestOptions_set_RetryCount_mA0A092315042B6625A1B8FBC5383491B7C414CD3 (void);
// 0x00000121 System.String UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_BundleName()
extern void AssetBundleRequestOptions_get_BundleName_mC794C3CB5F0DD2ED320B34F44E2F586A55C24CF8 (void);
// 0x00000122 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_BundleName(System.String)
extern void AssetBundleRequestOptions_set_BundleName_m9C66005D818F3CFDA4B79FDF4011CF27F0C2A72E (void);
// 0x00000123 UnityEngine.ResourceManagement.ResourceProviders.AssetLoadMode UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_AssetLoadMode()
extern void AssetBundleRequestOptions_get_AssetLoadMode_m65323BD2C2036B68237820458D8755B8FAC634E3 (void);
// 0x00000124 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_AssetLoadMode(UnityEngine.ResourceManagement.ResourceProviders.AssetLoadMode)
extern void AssetBundleRequestOptions_set_AssetLoadMode_mAAEED5FA4AE47A36F36F65678BF3E9A9A60B9475 (void);
// 0x00000125 System.Int64 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_BundleSize()
extern void AssetBundleRequestOptions_get_BundleSize_m01BFA314FF232985809F91FA0D1711984582AD5E (void);
// 0x00000126 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_BundleSize(System.Int64)
extern void AssetBundleRequestOptions_set_BundleSize_m26D31A8E9D68404B7F5977B490BFE815D5D01D02 (void);
// 0x00000127 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_UseCrcForCachedBundle()
extern void AssetBundleRequestOptions_get_UseCrcForCachedBundle_m9509A29234CA8EF1ABB637FEB3996CAB3633AD17 (void);
// 0x00000128 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_UseCrcForCachedBundle(System.Boolean)
extern void AssetBundleRequestOptions_set_UseCrcForCachedBundle_mA4BF24C012E9D1191FD0DFDCDD2B2E9500696459 (void);
// 0x00000129 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_UseUnityWebRequestForLocalBundles()
extern void AssetBundleRequestOptions_get_UseUnityWebRequestForLocalBundles_m59F9377203E4DB22D8F4895D434837525EB38002 (void);
// 0x0000012A System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_UseUnityWebRequestForLocalBundles(System.Boolean)
extern void AssetBundleRequestOptions_set_UseUnityWebRequestForLocalBundles_m0B3863B045E1BB34CF48FE63F67256DD9F576820 (void);
// 0x0000012B System.Boolean UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_ClearOtherCachedVersionsWhenLoaded()
extern void AssetBundleRequestOptions_get_ClearOtherCachedVersionsWhenLoaded_mA4B29E0A6CD5A0AD02D07652BFDB8FEA8BBBAACC (void);
// 0x0000012C System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_ClearOtherCachedVersionsWhenLoaded(System.Boolean)
extern void AssetBundleRequestOptions_set_ClearOtherCachedVersionsWhenLoaded_m73A8ACC0FBB4D029AB35494A4768C5A1D83B72D2 (void);
// 0x0000012D System.Int64 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::ComputeSize(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceManager)
extern void AssetBundleRequestOptions_ComputeSize_m2E82047D611D69C7AADC6FD0151E2EF610E4D202 (void);
// 0x0000012E System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::.ctor()
extern void AssetBundleRequestOptions__ctor_m478D494DCB048FCC2462FE9D87ED34991E0D0869 (void);
// 0x0000012F System.Boolean UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::get_HasTimedOut()
extern void AssetBundleResource_get_HasTimedOut_mEB56C4FD339E732E126EAC1308CD5382205AF807 (void);
// 0x00000130 System.Int64 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::get_BytesToDownload()
extern void AssetBundleResource_get_BytesToDownload_m74F57D1F457D1FEC59FFB0E36B42987E72C6E4FF (void);
// 0x00000131 UnityEngine.Networking.UnityWebRequest UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::CreateWebRequest(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AssetBundleResource_CreateWebRequest_m008113D1236E539144C558988C2D6F9D71881962 (void);
// 0x00000132 UnityEngine.Networking.UnityWebRequest UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::CreateWebRequest(System.String)
extern void AssetBundleResource_CreateWebRequest_mE7E91444E6A63D7C6B557CEBD5BD21FFD4CC5CC4 (void);
// 0x00000133 UnityEngine.AssetBundleRequest UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::GetAssetPreloadRequest()
extern void AssetBundleResource_GetAssetPreloadRequest_mD30D88065725E48072AA5298A88789BFF1D759B3 (void);
// 0x00000134 System.Single UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::PercentComplete()
extern void AssetBundleResource_PercentComplete_m83436C87C7EE6E0FF6C28FB7E05B841308DFA8AB (void);
// 0x00000135 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::GetDownloadStatus()
extern void AssetBundleResource_GetDownloadStatus_mD477BF4665169B01680B78FCB57CE9095BEF6F13 (void);
// 0x00000136 UnityEngine.AssetBundle UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::GetAssetBundle()
extern void AssetBundleResource_GetAssetBundle_mEDCC866A7AF1A532F40B40B821DD457AC73FB8D0 (void);
// 0x00000137 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AssetBundleResource_Start_m165368F7AD26E5F1C74190C95817A9D5757D2BA4 (void);
// 0x00000138 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::WaitForCompletionHandler()
extern void AssetBundleResource_WaitForCompletionHandler_m33BCD14B7534789D0F4895E8C4B7329736EB404A (void);
// 0x00000139 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::AddCallbackInvokeIfDone(UnityEngine.AsyncOperation,System.Action`1<UnityEngine.AsyncOperation>)
extern void AssetBundleResource_AddCallbackInvokeIfDone_mBE568F524737A2CABE082BCD5C7C90F889377997 (void);
// 0x0000013A System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::GetLoadInfo(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle,UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource/LoadType&,System.String&)
extern void AssetBundleResource_GetLoadInfo_m07B22D0C0EBF139FB9E1EDDECFCDECC5F30410AE (void);
// 0x0000013B System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::GetLoadInfo(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource/LoadType&,System.String&)
extern void AssetBundleResource_GetLoadInfo_m31EAFB0F163FC44B785266F67D2DD891BE82F961 (void);
// 0x0000013C System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::BeginOperation()
extern void AssetBundleResource_BeginOperation_m8A9873499D80D88F50FC5F20FA039F0AEA50DC12 (void);
// 0x0000013D System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::BeginWebRequestOperation(UnityEngine.AsyncOperation)
extern void AssetBundleResource_BeginWebRequestOperation_m8A9EF8065A3261F869AB86670EC356A5BFB5E062 (void);
// 0x0000013E System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::Update(System.Single)
extern void AssetBundleResource_Update_m3D23022F4E2C939083A0E7D12FFA94EE6AC9066D (void);
// 0x0000013F System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::LocalRequestOperationCompleted(UnityEngine.AsyncOperation)
extern void AssetBundleResource_LocalRequestOperationCompleted_m5B5121EDE02A34809BF275AD11A3D8785686FF5F (void);
// 0x00000140 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::CompleteBundleLoad(UnityEngine.AssetBundle)
extern void AssetBundleResource_CompleteBundleLoad_m03C8DC0761FB6F73DD8891D40055B21BF3667DE3 (void);
// 0x00000141 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::WebRequestOperationCompleted(UnityEngine.AsyncOperation)
extern void AssetBundleResource_WebRequestOperationCompleted_m36359C93B7E68673166D558680F003D314E4AD1C (void);
// 0x00000142 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::Unload()
extern void AssetBundleResource_Unload_m141896873F3F31F8C587361583C8F71DC3F7049E (void);
// 0x00000143 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::.ctor()
extern void AssetBundleResource__ctor_mD5B4085DF1B5D6935198B89C651FDF6818C43835 (void);
// 0x00000144 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::<GetAssetPreloadRequest>b__25_0(UnityEngine.AsyncOperation)
extern void AssetBundleResource_U3CGetAssetPreloadRequestU3Eb__25_0_m9136E232EDA239DB42142C70CCEE6341D5E7FEF6 (void);
// 0x00000145 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::<BeginOperation>b__34_0(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void AssetBundleResource_U3CBeginOperationU3Eb__34_0_m29DEADBD1103682676E7ABA67B2D7760EF34BD8E (void);
// 0x00000146 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AssetBundleProvider_Provide_mD46D28C6CA670CA740FE87912320632FDB659C1E (void);
// 0x00000147 System.Type UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AssetBundleProvider_GetDefaultType_m79830BEE6CE701C7DF95E65F33C5AF8B74A83852 (void);
// 0x00000148 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void AssetBundleProvider_Release_m48DDAE8401ADD5932DAAC61C4D161155FF43B3E6 (void);
// 0x00000149 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::.ctor()
extern void AssetBundleProvider__ctor_m5C55E1DE6E0E02A232B307350CA4D76C187402B1 (void);
// 0x0000014A System.Void UnityEngine.ResourceManagement.ResourceProviders.AtlasSpriteProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AtlasSpriteProvider_Provide_mF045676D19D97CD57A7BC0807027C8BB78217E15 (void);
// 0x0000014B System.Void UnityEngine.ResourceManagement.ResourceProviders.AtlasSpriteProvider::.ctor()
extern void AtlasSpriteProvider__ctor_m05488EDAFF03B092471403E9999CD0266C5AACED (void);
// 0x0000014C System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void BundledAssetProvider_Provide_m8C2EDDF78D3CCC187CC41F63E8FC695597C4A12E (void);
// 0x0000014D System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider::.ctor()
extern void BundledAssetProvider__ctor_mC25705358F17D4412D359E73C5F4578CF85192EB (void);
// 0x0000014E UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::LoadBundleFromDependecies(System.Collections.Generic.IList`1<System.Object>)
extern void InternalOp_LoadBundleFromDependecies_mDB83267FA2999729FCFAA33B52C09EC47B91F73C (void);
// 0x0000014F System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void InternalOp_Start_mA10ADF4F26C477CD58C6E8F808165CCF1270A74D (void);
// 0x00000150 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::BeginAssetLoad()
extern void InternalOp_BeginAssetLoad_m5BDAE4D0596E4E634EDF56570572FA10C33EA555 (void);
// 0x00000151 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::WaitForCompletionHandler()
extern void InternalOp_WaitForCompletionHandler_mB09AA0D8EFAE4799B258E318EFF94F523CDECEFA (void);
// 0x00000152 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::ActionComplete(UnityEngine.AsyncOperation)
extern void InternalOp_ActionComplete_m2F628707CEA05C810AE258C5549C47EECABBCA9D (void);
// 0x00000153 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::GetArrayResult(UnityEngine.Object[])
extern void InternalOp_GetArrayResult_mE65E76ED9737FC08A944AFD8EED9160F77D054DF (void);
// 0x00000154 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::GetListResult(UnityEngine.Object[])
extern void InternalOp_GetListResult_mBCB64E34114928CEF32174FCB1C90AFE96471BD3 (void);
// 0x00000155 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::GetAssetResult(UnityEngine.Object)
extern void InternalOp_GetAssetResult_m91F7D4D3CB8A5EA5DEAC395A84A87A3FE1193810 (void);
// 0x00000156 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::GetAssetSubObjectResult(UnityEngine.Object[])
extern void InternalOp_GetAssetSubObjectResult_m58606D07640F409140212A5C07318E7729C2DEAD (void);
// 0x00000157 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::CompleteOperation()
extern void InternalOp_CompleteOperation_mCC9E4E3A80A3E8F61A7597370A8B367DCCF31693 (void);
// 0x00000158 System.Single UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::ProgressCallback()
extern void InternalOp_ProgressCallback_mFD402D64CE77420108145A6E6DB428E914D7BF57 (void);
// 0x00000159 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::.ctor()
extern void InternalOp__ctor_mB8936B16062FBD1BCDE173A781968358E492FB7F (void);
// 0x0000015A System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::<Start>b__7_0(UnityEngine.AsyncOperation)
extern void InternalOp_U3CStartU3Eb__7_0_mA908139CCF38B2A3C1DF3C448AB79D37205A85D6 (void);
// 0x0000015B UnityEngine.Vector3 UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_Position()
extern void InstantiationParameters_get_Position_m9569A28B575846AD814790114A6CFAB8FC3E32DC (void);
// 0x0000015C UnityEngine.Quaternion UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_Rotation()
extern void InstantiationParameters_get_Rotation_m155362C3F3442CA4CC53C063FA8B4D09B753B012 (void);
// 0x0000015D UnityEngine.Transform UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_Parent()
extern void InstantiationParameters_get_Parent_mD6BC05225FF561D2123B1745B838D147D9B6A621 (void);
// 0x0000015E System.Boolean UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_InstantiateInWorldPosition()
extern void InstantiationParameters_get_InstantiateInWorldPosition_mF6ACF2A60E6C89C55CC36D82220910EEA9CC65F4 (void);
// 0x0000015F System.Boolean UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_SetPositionRotation()
extern void InstantiationParameters_get_SetPositionRotation_m3B5F3C4065F65ED2D4FD75B3B81D311B15EB70D1 (void);
// 0x00000160 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::.ctor(UnityEngine.Transform,System.Boolean)
extern void InstantiationParameters__ctor_m89D89058D329E9C651528936FD098282F1589225 (void);
// 0x00000161 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void InstantiationParameters__ctor_mE86F10CB3D40AC7EA1883000E381AEAEA1E16A33 (void);
// 0x00000162 TObject UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::Instantiate(TObject)
// 0x00000163 UnityEngine.GameObject UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider::ProvideInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
// 0x00000164 System.Void UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider::ReleaseInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.GameObject)
// 0x00000165 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::.ctor(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation)
extern void ProvideHandle__ctor_mB9CCF5CA6F8DD98AB72B4659076385051038795C (void);
// 0x00000166 UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_InternalOp()
extern void ProvideHandle_get_InternalOp_mDCF56F543329C2078BA6B8CE99C39B6EF3BEA41F (void);
// 0x00000167 UnityEngine.ResourceManagement.ResourceManager UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_ResourceManager()
extern void ProvideHandle_get_ResourceManager_mD9EAFA3333B5A459E1AD6EA76E15558C531ACD76 (void);
// 0x00000168 System.Type UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_Type()
extern void ProvideHandle_get_Type_mC36FCA0E5806E401F98BC3D30511DE5015A81EFF (void);
// 0x00000169 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_Location()
extern void ProvideHandle_get_Location_m38F31BC3081090D72C7EFA9C6CDE73ED45B7D978 (void);
// 0x0000016A System.Int32 UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_DependencyCount()
extern void ProvideHandle_get_DependencyCount_m56B0737197C7470C2A5FF8E8E2BBDA151E572BA4 (void);
// 0x0000016B TDepObject UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::GetDependency(System.Int32)
// 0x0000016C System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
extern void ProvideHandle_GetDependencies_mC65A5900B3EA9F0874C3D19BEFFC022DFC9AD8E8 (void);
// 0x0000016D System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::SetProgressCallback(System.Func`1<System.Single>)
extern void ProvideHandle_SetProgressCallback_m3B3F019C3AFF1A6B0F37B5B28F9A1121EBB73FD2 (void);
// 0x0000016E System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::SetDownloadProgressCallbacks(System.Func`1<UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus>)
extern void ProvideHandle_SetDownloadProgressCallbacks_mE3370C49221CFB6CDED35F5FE373ADE5AD9270EF (void);
// 0x0000016F System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::SetWaitForCompletionCallback(System.Func`1<System.Boolean>)
extern void ProvideHandle_SetWaitForCompletionCallback_m96207FCBF579C5988A651BBECB844F0E37DB9B25 (void);
// 0x00000170 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::Complete(T,System.Boolean,System.Exception)
// 0x00000171 System.String UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::get_ProviderId()
// 0x00000172 System.Type UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000173 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::CanProvide(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000174 System.Void UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
// 0x00000175 System.Void UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
// 0x00000176 UnityEngine.ResourceManagement.ResourceProviders.ProviderBehaviourFlags UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::get_BehaviourFlags()
// 0x00000177 UnityEngine.SceneManagement.Scene UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::get_Scene()
extern void SceneInstance_get_Scene_mFF895316B46CBB8126ABE0FE8F85985D1232CCA3 (void);
// 0x00000178 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::set_Scene(UnityEngine.SceneManagement.Scene)
extern void SceneInstance_set_Scene_m8AD506582C450F4C678C7265ED0A94FFDA9179E5 (void);
// 0x00000179 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::Activate()
extern void SceneInstance_Activate_m52A0E7A28EEA007FF2529340E980D798374E5B0E (void);
// 0x0000017A UnityEngine.AsyncOperation UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::ActivateAsync()
extern void SceneInstance_ActivateAsync_m0F5677DAD4ED2EC1EB027E261B4EBF40418A3DC3 (void);
// 0x0000017B System.Int32 UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::GetHashCode()
extern void SceneInstance_GetHashCode_m9B65494AC6ED027B091B9FF2DE58DEE6E46FFF0B (void);
// 0x0000017C System.Boolean UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::Equals(System.Object)
extern void SceneInstance_Equals_mFD3D668A674D5838BF6F9ED892C0452E48729A43 (void);
// 0x0000017D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider::ProvideScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
// 0x0000017E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider::ReleaseScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
// 0x0000017F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider2::ReleaseScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,UnityEngine.SceneManagement.UnloadSceneOptions)
// 0x00000180 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.SceneProviderExtensions::ReleaseScene(UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider,UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,UnityEngine.SceneManagement.UnloadSceneOptions)
extern void SceneProviderExtensions_ReleaseScene_m6E365DB71BF68925751FC3DE6BD654F216F0D1DE (void);
// 0x00000181 UnityEngine.GameObject UnityEngine.ResourceManagement.ResourceProviders.InstanceProvider::ProvideInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
extern void InstanceProvider_ProvideInstance_m530D2258769CC2B97647ECFBCF34659BD93C2C6B (void);
// 0x00000182 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstanceProvider::ReleaseInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.GameObject)
extern void InstanceProvider_ReleaseInstance_m9DB74D96301B8DD3263AF1AB860E46D85EE08690 (void);
// 0x00000183 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstanceProvider::.ctor()
extern void InstanceProvider__ctor_m35BAD5E188BFB18C9BDCC7AE47F6B9338A7B5D2F (void);
// 0x00000184 System.Object UnityEngine.ResourceManagement.ResourceProviders.JsonAssetProvider::Convert(System.Type,System.String)
extern void JsonAssetProvider_Convert_m97EC94F47C2FA697A38522205E3C972497113AE2 (void);
// 0x00000185 System.Void UnityEngine.ResourceManagement.ResourceProviders.JsonAssetProvider::.ctor()
extern void JsonAssetProvider__ctor_m15637BC90EF129F6F31918CDAE728E5D9560F303 (void);
// 0x00000186 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void LegacyResourcesProvider_Provide_m4253335CC4BDC849FAD018D5BBDBD3392D03691F (void);
// 0x00000187 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void LegacyResourcesProvider_Release_m330D49267272717059BD526433B358E581083253 (void);
// 0x00000188 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider::.ctor()
extern void LegacyResourcesProvider__ctor_m578CAD18E82E4F66CEFCA1A30B00338FB677A4C6 (void);
// 0x00000189 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider/InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void InternalOp_Start_m8CF6A6B5D9675C182B0AAB762BC3E30752CA8EA5 (void);
// 0x0000018A System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider/InternalOp::AsyncOperationCompleted(UnityEngine.AsyncOperation)
extern void InternalOp_AsyncOperationCompleted_m5821F76BA226C04C5188E80E48DC97EF186A72DA (void);
// 0x0000018B System.Single UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider/InternalOp::PercentComplete()
extern void InternalOp_PercentComplete_m3E24E8827F98571B4532D1A36382047CC296EBD8 (void);
// 0x0000018C System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider/InternalOp::.ctor()
extern void InternalOp__ctor_mF1AFFC0E9D8BF2049B3255E65A87883BDDE2EF04 (void);
// 0x0000018D System.String UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::get_ProviderId()
extern void ResourceProviderBase_get_ProviderId_m39F6890BE274231ADB091ED135F06B475C7AD842 (void);
// 0x0000018E System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Initialize(System.String,System.String)
extern void ResourceProviderBase_Initialize_mB88D098C7CA16F9B29CD25F127E586C60271DEC6 (void);
// 0x0000018F System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::CanProvide(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceProviderBase_CanProvide_m124E66DC2469547DA7E58A5396A1FCAC3B470D85 (void);
// 0x00000190 System.String UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::ToString()
extern void ResourceProviderBase_ToString_m4ACCDF1D1E881C7CC1537F638B29F9BCC9654065 (void);
// 0x00000191 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void ResourceProviderBase_Release_mA4EA7CE2F575DA1E9B1A6B8EA9F21896DDC411A7 (void);
// 0x00000192 System.Type UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceProviderBase_GetDefaultType_m00E90DFA2818C81077829B7AE9F9847834520E02 (void);
// 0x00000193 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
// 0x00000194 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::InitializeAsync(UnityEngine.ResourceManagement.ResourceManager,System.String,System.String)
extern void ResourceProviderBase_InitializeAsync_mA971647D0097330ACCD75D035481D16D8B8EEFFA (void);
// 0x00000195 UnityEngine.ResourceManagement.ResourceProviders.ProviderBehaviourFlags UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider.get_BehaviourFlags()
extern void ResourceProviderBase_UnityEngine_ResourceManagement_ResourceProviders_IResourceProvider_get_BehaviourFlags_mFDEC1DF913DFD712F248FAD2A36DB610F7580F25 (void);
// 0x00000196 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::.ctor()
extern void ResourceProviderBase__ctor_mA8589431AF866EE209169DA0E84D70845D0F7C1A (void);
// 0x00000197 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase/BaseInitAsyncOp::Init(System.Func`1<System.Boolean>)
extern void BaseInitAsyncOp_Init_m84C2E47DCA1E087F49D35279878445687E3E16F4 (void);
// 0x00000198 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase/BaseInitAsyncOp::InvokeWaitForCompletion()
extern void BaseInitAsyncOp_InvokeWaitForCompletion_mEB21D8F33ADDB24034254C2DFD3CE28DA4819EF6 (void);
// 0x00000199 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase/BaseInitAsyncOp::Execute()
extern void BaseInitAsyncOp_Execute_mB78A5ED65E7B68D61754EFBF6A73055F64F7A129 (void);
// 0x0000019A System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase/BaseInitAsyncOp::.ctor()
extern void BaseInitAsyncOp__ctor_m7D92010EA1BB2C67B605DC95E8D91E500AC7FEDA (void);
// 0x0000019B System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m3B6FAF943AAB23C2BD7DF7E6AB9D1B3699703856 (void);
// 0x0000019C System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase/<>c__DisplayClass10_0::<InitializeAsync>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CInitializeAsyncU3Eb__0_m19A259CF5D733F72D104DEFAD3348199EDF35F35 (void);
// 0x0000019D UnityEngine.ResourceManagement.ResourceProviders.ProviderLoadRequestOptions UnityEngine.ResourceManagement.ResourceProviders.ProviderLoadRequestOptions::Copy()
extern void ProviderLoadRequestOptions_Copy_m43E827A86A7B8E8AE9FFFDC166DEAAED0A338AA9 (void);
// 0x0000019E System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ProviderLoadRequestOptions::get_IgnoreFailures()
extern void ProviderLoadRequestOptions_get_IgnoreFailures_m536D7EA26D23AC3A8D3106AC22B3D7E8D10144F2 (void);
// 0x0000019F System.Void UnityEngine.ResourceManagement.ResourceProviders.ProviderLoadRequestOptions::set_IgnoreFailures(System.Boolean)
extern void ProviderLoadRequestOptions_set_IgnoreFailures_m42288FE2F8E5C77DB7CDC484C6629C1893DDC300 (void);
// 0x000001A0 System.Int32 UnityEngine.ResourceManagement.ResourceProviders.ProviderLoadRequestOptions::get_WebRequestTimeout()
extern void ProviderLoadRequestOptions_get_WebRequestTimeout_mE66FAD12D6DF46237874EA1771D66C65F1AC3575 (void);
// 0x000001A1 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProviderLoadRequestOptions::set_WebRequestTimeout(System.Int32)
extern void ProviderLoadRequestOptions_set_WebRequestTimeout_mB6169D94811EE00AD32534426A8C15C894E2140A (void);
// 0x000001A2 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProviderLoadRequestOptions::.ctor()
extern void ProviderLoadRequestOptions__ctor_mAF174C23E45D4832B9FDAC7EE698D5CCA00B2B1E (void);
// 0x000001A3 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::ProvideScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void SceneProvider_ProvideScene_mCB31C5F58277AC5B97E3B0E10317FE34EDFD58F8 (void);
// 0x000001A4 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::ReleaseScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void SceneProvider_ReleaseScene_m1A7A7B799DFEF64C68E7101546579CC907E0048A (void);
// 0x000001A5 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider2.ReleaseScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,UnityEngine.SceneManagement.UnloadSceneOptions)
extern void SceneProvider_UnityEngine_ResourceManagement_ResourceProviders_ISceneProvider2_ReleaseScene_m78F1024F222BE45D5C97D5E9D458A93AA2B64972 (void);
// 0x000001A6 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::.ctor()
extern void SceneProvider__ctor_m1B35416831C52CC189753B5624976389772A45F7 (void);
// 0x000001A7 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::.ctor(UnityEngine.ResourceManagement.ResourceManager)
extern void SceneOp__ctor_m5B0A2397AF0B1ACF6E2FD5323BA6EC60B4DA0147 (void);
// 0x000001A8 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
extern void SceneOp_GetDownloadStatus_m9612A40426B520094350AB785F09D6CDE3621767 (void);
// 0x000001A9 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::Init(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
extern void SceneOp_Init_mB918CB5783BF2635D2970748F35C9D2DE689304A (void);
// 0x000001AA System.Boolean UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::InvokeWaitForCompletion()
extern void SceneOp_InvokeWaitForCompletion_m9F7563A2D5335D3DE45383E0E2F0AF0D5ADA0E04 (void);
// 0x000001AB System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void SceneOp_GetDependencies_mF478AD3651950802CCDD736B1FBC44C32FE692EB (void);
// 0x000001AC System.String UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::get_DebugName()
extern void SceneOp_get_DebugName_m7260EF67264EC70C626B11F4FFB8E73A0965610D (void);
// 0x000001AD System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::Execute()
extern void SceneOp_Execute_mCAB604D31CBC76E96284377779B79C74E78E2C6E (void);
// 0x000001AE UnityEngine.ResourceManagement.ResourceProviders.SceneInstance UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::InternalLoadScene(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Boolean,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void SceneOp_InternalLoadScene_m9C5B15DCB1405A84EA255ECE59B90D419D6C5653 (void);
// 0x000001AF UnityEngine.AsyncOperation UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::InternalLoad(System.String,System.Boolean,UnityEngine.SceneManagement.LoadSceneMode)
extern void SceneOp_InternalLoad_m157B04FAA5544C6CB3221855FD67178212D6E201 (void);
// 0x000001B0 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::Destroy()
extern void SceneOp_Destroy_m701CC2DE4463C54C16757EB3D0A0A94E71077696 (void);
// 0x000001B1 System.Single UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::get_Progress()
extern void SceneOp_get_Progress_m378936898C1A6DB059D093337B1DCC400689FB5B (void);
// 0x000001B2 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::UnityEngine.ResourceManagement.IUpdateReceiver.Update(System.Single)
extern void SceneOp_UnityEngine_ResourceManagement_IUpdateReceiver_Update_m27FD39CB9D11C53DD786CDB722DD249F7D35217B (void);
// 0x000001B3 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/UnloadSceneOp::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,UnityEngine.SceneManagement.UnloadSceneOptions)
extern void UnloadSceneOp_Init_mEE09B761AF71428AA24E3C26D0E52670A362977D (void);
// 0x000001B4 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/UnloadSceneOp::Execute()
extern void UnloadSceneOp_Execute_m1DFB8028F12E302AFE304F67E5BAAB3820B0EC4D (void);
// 0x000001B5 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/UnloadSceneOp::InvokeWaitForCompletion()
extern void UnloadSceneOp_InvokeWaitForCompletion_m1D581DA5E4DEC1B4D688D8D5027DB85AA10B17A1 (void);
// 0x000001B6 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/UnloadSceneOp::UnloadSceneCompleted(UnityEngine.AsyncOperation)
extern void UnloadSceneOp_UnloadSceneCompleted_m357E9D44463E1A1E3E87A29FFF300C11D3E862F9 (void);
// 0x000001B7 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/UnloadSceneOp::UnloadSceneCompletedNoRelease(UnityEngine.AsyncOperation)
extern void UnloadSceneOp_UnloadSceneCompletedNoRelease_m45468FA61E0BB5E58457DEDEC3CC783C52F7689C (void);
// 0x000001B8 System.Single UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/UnloadSceneOp::get_Progress()
extern void UnloadSceneOp_get_Progress_m42967C82BB0C649B805E107569933C78AAE18628 (void);
// 0x000001B9 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/UnloadSceneOp::.ctor()
extern void UnloadSceneOp__ctor_m051B216E7A8E5F7ED061FF3D0986D0BD9E9DF7A5 (void);
// 0x000001BA System.Boolean UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::get_IgnoreFailures()
extern void TextDataProvider_get_IgnoreFailures_m57066898F24CD1829FBD8BB87E0DEAA1CDC20C8B (void);
// 0x000001BB System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::set_IgnoreFailures(System.Boolean)
extern void TextDataProvider_set_IgnoreFailures_m5787E1C9DA529EF415D30ADDA2B74DBE78E6D825 (void);
// 0x000001BC System.Object UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::Convert(System.Type,System.String)
extern void TextDataProvider_Convert_mEBE0237D88E5C2D2F50D7441367D63906D0CDF75 (void);
// 0x000001BD System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void TextDataProvider_Provide_m97C0755B7CB3C55D2D5CE99A26A0A650F887A69B (void);
// 0x000001BE System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::.ctor()
extern void TextDataProvider__ctor_m7AC08BFDB0D65AFFF44638ADB897882432BF92DB (void);
// 0x000001BF System.Single UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::GetPercentComplete()
extern void InternalOp_GetPercentComplete_m9ED8C8B9A92A008FC832003493795968212F040F (void);
// 0x000001C0 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle,UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider)
extern void InternalOp_Start_mF752565834A11A0E10B24509297A70DDC5991B6A (void);
// 0x000001C1 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::WaitForCompletionHandler()
extern void InternalOp_WaitForCompletionHandler_m949B7900A9FF2FA537D5F559A495BA004400F3B6 (void);
// 0x000001C2 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::RequestOperation_completed(UnityEngine.AsyncOperation)
extern void InternalOp_RequestOperation_completed_m888C6797A4FF695796EAE9B6329A6B1EE896604B (void);
// 0x000001C3 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::CompleteOperation(System.String,System.Exception)
extern void InternalOp_CompleteOperation_m441DF029F5CAC4BE6A4A77EC65ADBBAF9FD02DD9 (void);
// 0x000001C4 System.Object UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::ConvertText(System.String)
extern void InternalOp_ConvertText_m55ECA959135C093079D59450942A86DB4B7D6037 (void);
// 0x000001C5 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::SendWebRequest(System.String)
extern void InternalOp_SendWebRequest_m0B6B316F494B02FF73DF843F1F5AFEE0EB3A17A2 (void);
// 0x000001C6 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::.ctor()
extern void InternalOp__ctor_m761A51452CEFD8FAD7575FC1940F3A2E0709DB54 (void);
// 0x000001C7 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::<SendWebRequest>b__13_0(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void InternalOp_U3CSendWebRequestU3Eb__13_0_m72B7D00BDC91CA1F1FE36DA48CAB6026345CBF35 (void);
// 0x000001C8 System.Int64 UnityEngine.ResourceManagement.ResourceLocations.ILocationSizeData::ComputeSize(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceManager)
// 0x000001C9 System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_InternalId()
// 0x000001CA System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_ProviderId()
// 0x000001CB System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_Dependencies()
// 0x000001CC System.Int32 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::Hash(System.Type)
// 0x000001CD System.Int32 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_DependencyHashCode()
// 0x000001CE System.Boolean UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_HasDependencies()
// 0x000001CF System.Object UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_Data()
// 0x000001D0 System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_PrimaryKey()
// 0x000001D1 System.Type UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_ResourceType()
// 0x000001D2 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_InternalId()
extern void ResourceLocationBase_get_InternalId_mDD026BD02BC9C4BFE13018163D5C3333F87AC6CF (void);
// 0x000001D3 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_ProviderId()
extern void ResourceLocationBase_get_ProviderId_m3C0A8FFDD223BA3DB83C2BF36FA16A58F7D56603 (void);
// 0x000001D4 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_Dependencies()
extern void ResourceLocationBase_get_Dependencies_mF557A83CF1A117CBCE78814D350BC09135BF4DF9 (void);
// 0x000001D5 System.Boolean UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_HasDependencies()
extern void ResourceLocationBase_get_HasDependencies_mF968DBFE910DC0F2653AE1D3582149AF8D25DDD7 (void);
// 0x000001D6 System.Object UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_Data()
extern void ResourceLocationBase_get_Data_m1FBDAD7E83CA4FE35DF61546CB4864F234760358 (void);
// 0x000001D7 System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::set_Data(System.Object)
extern void ResourceLocationBase_set_Data_mD899B389E585E1F781CD02CC2EEDFD38D80664BC (void);
// 0x000001D8 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_PrimaryKey()
extern void ResourceLocationBase_get_PrimaryKey_m2C276908F4E098BCED0F019A30872A7D2A02ACED (void);
// 0x000001D9 System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::set_PrimaryKey(System.String)
extern void ResourceLocationBase_set_PrimaryKey_m989397E9A03B8CFDCBD151E087125BCC0C949145 (void);
// 0x000001DA System.Int32 UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_DependencyHashCode()
extern void ResourceLocationBase_get_DependencyHashCode_m1D0F112D82E12492981006CC695486AA85CF7A47 (void);
// 0x000001DB System.Type UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_ResourceType()
extern void ResourceLocationBase_get_ResourceType_m1C6C8FA00487D034B8661EBB5323DEA49E76F631 (void);
// 0x000001DC System.Int32 UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::Hash(System.Type)
extern void ResourceLocationBase_Hash_mFD5291AA954FA36C6C833C3940F8A72A9C2AED77 (void);
// 0x000001DD System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::ToString()
extern void ResourceLocationBase_ToString_m61190854FAFA7A73E0A9DE1647806A6491BE94A7 (void);
// 0x000001DE System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::.ctor(System.String,System.String,System.String,System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation[])
extern void ResourceLocationBase__ctor_m8487509965168D66DD0079D6A096845204088A7F (void);
// 0x000001DF System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::ComputeDependencyHash()
extern void ResourceLocationBase_ComputeDependencyHash_m9BB3035DD2ACAA3680109BD2DEB756F2DC1C1963 (void);
// 0x000001E0 System.Void UnityEngine.ResourceManagement.ResourceLocations.LocationWrapper::.ctor(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void LocationWrapper__ctor_m0747F501E4CDB3A39111CB33A653D4D430F6CA3C (void);
// 0x000001E1 System.String UnityEngine.ResourceManagement.ResourceLocations.LocationWrapper::get_InternalId()
extern void LocationWrapper_get_InternalId_m355AFE360DA988066AAB1BAA69019707B500F554 (void);
// 0x000001E2 System.String UnityEngine.ResourceManagement.ResourceLocations.LocationWrapper::get_ProviderId()
extern void LocationWrapper_get_ProviderId_m2DBD62DD53ADC6312CB31A47F1ACE4D92573225A (void);
// 0x000001E3 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.ResourceManagement.ResourceLocations.LocationWrapper::get_Dependencies()
extern void LocationWrapper_get_Dependencies_m095B91D85514EB181D7F99F14595A9B9872DFF06 (void);
// 0x000001E4 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.LocationWrapper::get_DependencyHashCode()
extern void LocationWrapper_get_DependencyHashCode_mF48306CE95987FD5065C0488B0872C6A543DD15D (void);
// 0x000001E5 System.Boolean UnityEngine.ResourceManagement.ResourceLocations.LocationWrapper::get_HasDependencies()
extern void LocationWrapper_get_HasDependencies_m6EA0F7E06A3DFD4BFB3B230EA9A930879770484B (void);
// 0x000001E6 System.Object UnityEngine.ResourceManagement.ResourceLocations.LocationWrapper::get_Data()
extern void LocationWrapper_get_Data_m59FFC0A1E723AECBD0F2DC66757E9AA5703069CA (void);
// 0x000001E7 System.String UnityEngine.ResourceManagement.ResourceLocations.LocationWrapper::get_PrimaryKey()
extern void LocationWrapper_get_PrimaryKey_m862EE0B991542554FE9B04044AEBF5CE01B3A957 (void);
// 0x000001E8 System.Type UnityEngine.ResourceManagement.ResourceLocations.LocationWrapper::get_ResourceType()
extern void LocationWrapper_get_ResourceType_m628F5012DC99E805E6300C58E938F380C042F23C (void);
// 0x000001E9 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.LocationWrapper::Hash(System.Type)
extern void LocationWrapper_Hash_mFAECBE3373E3FB222087669F861E4C1FF01496B4 (void);
// 0x000001EA System.String UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Graph()
extern void DiagnosticEvent_get_Graph_m710E1F0680B4D39DE0264D295E28650421AEEF6D (void);
// 0x000001EB System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_ObjectId()
extern void DiagnosticEvent_get_ObjectId_mCA40B5EFDB9F507CAB17F2E19DA3F62D68B45537 (void);
// 0x000001EC System.String UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_DisplayName()
extern void DiagnosticEvent_get_DisplayName_mCBA5175616B9E1B45C12C51A8FBD01B88E5A78EE (void);
// 0x000001ED System.Int32[] UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Dependencies()
extern void DiagnosticEvent_get_Dependencies_m2751628387892A62E54CD3A32FD77DDD18626B27 (void);
// 0x000001EE System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Stream()
extern void DiagnosticEvent_get_Stream_mD35CADC32CA28797FEB25F744975712D808874D2 (void);
// 0x000001EF System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Frame()
extern void DiagnosticEvent_get_Frame_m80FE7279F8A049A54DF5D34EC80B0B8F4D33F9EB (void);
// 0x000001F0 System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Value()
extern void DiagnosticEvent_get_Value_mA831F1827F784283AF76FBB76D768ED1C00B9410 (void);
// 0x000001F1 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::.ctor(System.String,System.String,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern void DiagnosticEvent__ctor_m7010F47B7515B5EA548729B2F3E25EBFAEAEB217 (void);
// 0x000001F2 System.Byte[] UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::Serialize()
extern void DiagnosticEvent_Serialize_m02EC58094A8703C0C89430022D36A3EE3C304607 (void);
// 0x000001F3 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::Deserialize(System.Byte[])
extern void DiagnosticEvent_Deserialize_m80E559A631C1CADB8B5EE0B03A70D790C1459F3F (void);
// 0x000001F4 System.Guid UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::get_PlayerConnectionGuid()
extern void DiagnosticEventCollectorSingleton_get_PlayerConnectionGuid_mE4F64040FF6A340206986791B677A32D1AF40A06 (void);
// 0x000001F5 System.String UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::GetGameObjectName()
extern void DiagnosticEventCollectorSingleton_GetGameObjectName_m7454D83F6B10B46176467ED64EEB0B3E4FCB34A4 (void);
// 0x000001F6 System.Boolean UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::RegisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>,System.Boolean,System.Boolean)
extern void DiagnosticEventCollectorSingleton_RegisterEventHandler_m1C16B6445A83009C43667AD853E1F3FD6FCEFF15 (void);
// 0x000001F7 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::RegisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>)
extern void DiagnosticEventCollectorSingleton_RegisterEventHandler_mCDFC17E30EE53B7FCC0CF28BE6AD60A15433F825 (void);
// 0x000001F8 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::UnregisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>)
extern void DiagnosticEventCollectorSingleton_UnregisterEventHandler_m173AC1759DCE404F090B33C5B5E23856C7A1F962 (void);
// 0x000001F9 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::PostEvent(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void DiagnosticEventCollectorSingleton_PostEvent_m764F9DC4D7130EABF91C9370004FB1CEA5D4B9D1 (void);
// 0x000001FA System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::Awake()
extern void DiagnosticEventCollectorSingleton_Awake_m788CBE28ACBF8886D836CF86D41B1C208173A71D (void);
// 0x000001FB System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::Update()
extern void DiagnosticEventCollectorSingleton_Update_m1CCB4B5842B102C996FF57D9E2394891B15A6E09 (void);
// 0x000001FC System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::.ctor()
extern void DiagnosticEventCollectorSingleton__ctor_mC9A6C5013D561491DC56CF092E06C0C14FE44848 (void);
// 0x000001FD System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton/<>c::.cctor()
extern void U3CU3Ec__cctor_m4C8DEF7665FD738A20E75923001C729501E85F8F (void);
// 0x000001FE System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton/<>c::.ctor()
extern void U3CU3Ec__ctor_mDC7AA0A80AADA03CB9CA3A18F80594A9A1D56E1E (void);
// 0x000001FF System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton/<>c::<RegisterEventHandler>b__8_0(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void U3CU3Ec_U3CRegisterEventHandlerU3Eb__8_0_mA276498C4E15EE1A6D3D7892FA6241AA212620D6 (void);
// 0x00000200 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton/<>c::<Awake>b__11_0(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void U3CU3Ec_U3CAwakeU3Eb__11_0_m3B098EDF909F7D5B2470B7C10A321BF9E4D5E485 (void);
// 0x00000201 System.Guid UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::get_PlayerConnectionGuid()
extern void DiagnosticEventCollector_get_PlayerConnectionGuid_mCDC64A92E85E48453A28C92A4EF9652C49E593C1 (void);
// 0x00000202 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::FindOrCreateGlobalInstance()
extern void DiagnosticEventCollector_FindOrCreateGlobalInstance_m606D0B93253933F64A4F126E952F4AACAD95F1A6 (void);
// 0x00000203 System.Boolean UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::RegisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>,System.Boolean,System.Boolean)
extern void DiagnosticEventCollector_RegisterEventHandler_mD8110A8AE08413AA61A28352CD219880663BE7C3 (void);
// 0x00000204 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::UnregisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>)
extern void DiagnosticEventCollector_UnregisterEventHandler_m39277B914A0C123866E0FA4320B82CA7662B5725 (void);
// 0x00000205 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::PostEvent(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void DiagnosticEventCollector_PostEvent_m66DEEEEFF15701593B3B600159AE12085330B151 (void);
// 0x00000206 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::.ctor()
extern void DiagnosticEventCollector__ctor_m31BB8D4B02E0F321D9D94F54D6DBBF07B4A91581 (void);
// 0x00000207 UnityEngine.ResourceManagement.Util.IOperationCacheKey UnityEngine.ResourceManagement.AsyncOperations.ICachable::get_Key()
// 0x00000208 System.Void UnityEngine.ResourceManagement.AsyncOperations.ICachable::set_Key(UnityEngine.ResourceManagement.Util.IOperationCacheKey)
// 0x00000209 System.Object UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetResultAsObject()
// 0x0000020A System.Type UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_ResultType()
// 0x0000020B System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Version()
// 0x0000020C System.String UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_DebugName()
// 0x0000020D System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::DecrementReferenceCount()
// 0x0000020E System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::IncrementReferenceCount()
// 0x0000020F System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_ReferenceCount()
// 0x00000210 System.Single UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_PercentComplete()
// 0x00000211 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x00000212 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Status()
// 0x00000213 System.Exception UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_OperationException()
// 0x00000214 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_IsDone()
// 0x00000215 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x00000216 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000217 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_IsRunning()
// 0x00000218 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000219 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000021A System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000021B System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000021C System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::InvokeCompletionEvent()
// 0x0000021D System.Threading.Tasks.Task`1<System.Object> UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Task()
// 0x0000021E System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x0000021F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Handle()
// 0x00000220 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::WaitForCompletion()
// 0x00000221 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Execute()
// 0x00000222 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Destroy()
// 0x00000223 System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Progress()
// 0x00000224 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_DebugName()
// 0x00000225 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000226 TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Result()
// 0x00000227 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_Result(TObject)
// 0x00000228 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Version()
// 0x00000229 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_CompletedEventHasListeners()
// 0x0000022A System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_DestroyedEventHasListeners()
// 0x0000022B System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x0000022C System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_ReferenceCount()
// 0x0000022D System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_IsRunning()
// 0x0000022E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_IsRunning(System.Boolean)
// 0x0000022F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::.ctor()
// 0x00000230 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::ShortenPath(System.String,System.Boolean)
// 0x00000231 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::IncrementReferenceCount()
// 0x00000232 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::WaitForCompletion()
// 0x00000233 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::InvokeWaitForCompletion()
// 0x00000234 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::DecrementReferenceCount()
// 0x00000235 System.Threading.Tasks.Task`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Task()
// 0x00000236 System.Threading.Tasks.Task`1<System.Object> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Task()
// 0x00000237 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::ToString()
// 0x00000238 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::RegisterForDeferredCallbackEvent(System.Boolean)
// 0x00000239 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x0000023A System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x0000023B System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000023C System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000023D System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000023E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000023F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Status()
// 0x00000240 System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_OperationException()
// 0x00000241 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_OperationException(System.Exception)
// 0x00000242 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::MoveNext()
// 0x00000243 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Reset()
// 0x00000244 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Current()
// 0x00000245 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_IsDone()
// 0x00000246 System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_PercentComplete()
// 0x00000247 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::InvokeCompletionEvent()
// 0x00000248 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Handle()
// 0x00000249 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UpdateCallback(System.Single)
// 0x0000024A System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Complete(TObject,System.Boolean,System.String)
// 0x0000024B System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Complete(TObject,System.Boolean,System.String,System.Boolean)
// 0x0000024C System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Complete(TObject,System.Boolean,System.Exception,System.Boolean)
// 0x0000024D System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x0000024E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::InvokeExecute()
// 0x0000024F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000250 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000251 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000252 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000253 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Version()
// 0x00000254 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_ReferenceCount()
// 0x00000255 System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_PercentComplete()
// 0x00000256 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Status()
// 0x00000257 System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_OperationException()
// 0x00000258 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_IsDone()
// 0x00000259 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Handle()
// 0x0000025A System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x0000025B System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_DebugName()
// 0x0000025C System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetResultAsObject()
// 0x0000025D System.Type UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_ResultType()
// 0x0000025E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000025F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.DecrementReferenceCount()
// 0x00000260 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.IncrementReferenceCount()
// 0x00000261 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.InvokeCompletionEvent()
// 0x00000262 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x00000263 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::ReleaseDependencies()
// 0x00000264 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x00000265 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x00000266 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::<.ctor>b__35_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000267 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1/<>c__DisplayClass57_0::.ctor()
// 0x00000268 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1/<>c__DisplayClass57_0::<add_CompletedTypeless>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000269 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1/<>c__DisplayClass58_0::.ctor()
// 0x0000026A System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1/<>c__DisplayClass58_0::<remove_CompletedTypeless>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x0000026B System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_LocationName()
// 0x0000026C System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::set_LocationName(System.String)
// 0x0000026D System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_UnloadSceneOpExcludeReleaseCallback()
// 0x0000026E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::set_UnloadSceneOpExcludeReleaseCallback(System.Boolean)
// 0x0000026F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::op_Implicit(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000270 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject>)
// 0x00000271 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::GetDownloadStatus()
// 0x00000272 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::InternalGetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x00000273 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
// 0x00000274 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32)
// 0x00000275 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.String)
// 0x00000276 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32,System.String)
// 0x00000277 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Acquire()
// 0x00000278 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000279 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x0000027A System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000027B System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000027C System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_DebugName()
// 0x0000027D System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000027E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000027F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000280 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Equals(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000281 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::GetHashCode()
// 0x00000282 TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::WaitForCompletion()
// 0x00000283 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_InternalOp()
// 0x00000284 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_IsDone()
// 0x00000285 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::IsValid()
// 0x00000286 System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_OperationException()
// 0x00000287 System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_PercentComplete()
// 0x00000288 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_ReferenceCount()
// 0x00000289 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Release()
// 0x0000028A TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Result()
// 0x0000028B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Status()
// 0x0000028C System.Threading.Tasks.Task`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Task()
// 0x0000028D System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.get_Current()
// 0x0000028E System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.MoveNext()
// 0x0000028F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.Reset()
// 0x00000290 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_IsWaitingForCompletion()
extern void AsyncOperationHandle_get_IsWaitingForCompletion_m5A486EC21B5DDD25E406142BCB2BDABC04E79FCE (void);
// 0x00000291 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::set_IsWaitingForCompletion(System.Boolean)
extern void AsyncOperationHandle_set_IsWaitingForCompletion_m6B8EBA1E5DC2F39DB3510F0CD0D82A9E1A1207D3 (void);
// 0x00000292 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_LocationName()
extern void AsyncOperationHandle_get_LocationName_mC0E107351D4ADDDC32F0E9AC60390F1E2B244075 (void);
// 0x00000293 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::set_LocationName(System.String)
extern void AsyncOperationHandle_set_LocationName_mF020B238870AC6E2F8BC2DC27B8ACCA0A7BCFDCF (void);
// 0x00000294 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void AsyncOperationHandle__ctor_m5CAF8329748264C0D78320A562DAEAC3AE5869C8 (void);
// 0x00000295 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32)
extern void AsyncOperationHandle__ctor_m625B3C164EFE603B9C7F4444294CAAE1752B8A0F (void);
// 0x00000296 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.String)
extern void AsyncOperationHandle__ctor_m07D0BFA4D61DF19972B0CF2B007ACB96FF867FCB (void);
// 0x00000297 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32,System.String)
extern void AsyncOperationHandle__ctor_mB7A5E4B968464EF080A43D1759265AB39EF1D91C (void);
// 0x00000298 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Acquire()
extern void AsyncOperationHandle_Acquire_m3F33353A41ED6BF37906DEA40CAA59E1C5755707 (void);
// 0x00000299 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_add_Completed_mDC2AF6183B32C55FF890E226583D7E4A0FB35F09 (void);
// 0x0000029A System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_remove_Completed_mDF8507A627AF13A1381BE1BF4C26890047E86EA5 (void);
// 0x0000029B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<T> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Convert()
// 0x0000029C System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Equals(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AsyncOperationHandle_Equals_m80AD6C46CC59FE4B03A7962CE925262C49CACE16 (void);
// 0x0000029D System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_DebugName()
extern void AsyncOperationHandle_get_DebugName_m0324680BFDD4A65C585B3EA9E3F244186552C0C6 (void);
// 0x0000029E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_add_Destroyed_mAF6A1270B2A69BE84318FEB73D36A19437EE3008 (void);
// 0x0000029F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_remove_Destroyed_m6AB38E753AF3537B4F8DB8DC8D85FD6DF068D55D (void);
// 0x000002A0 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_GetDependencies_m35D1693217A8FCD56ECDAA6C933DF2A085AEE16E (void);
// 0x000002A1 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::GetHashCode()
extern void AsyncOperationHandle_GetHashCode_mACAE683D3FAFE3F49C50C2E9F64D97C1F5D1624B (void);
// 0x000002A2 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_InternalOp()
extern void AsyncOperationHandle_get_InternalOp_m4EA78DBA5091741358AF08A77C4F74B7EB00463B (void);
// 0x000002A3 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_IsDone()
extern void AsyncOperationHandle_get_IsDone_mAC819A5E254D87077EB8D9BE8F36AFC30EBA0C4B (void);
// 0x000002A4 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::IsValid()
extern void AsyncOperationHandle_IsValid_m2D904B19EBBC50451B441161A762EE7703E5463C (void);
// 0x000002A5 System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_OperationException()
extern void AsyncOperationHandle_get_OperationException_mC27EB297729268618ADC7DDC3504B01CF3CA9695 (void);
// 0x000002A6 System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_PercentComplete()
extern void AsyncOperationHandle_get_PercentComplete_m8020EBDC295B192EAA493D4E571E2501FE7AFA28 (void);
// 0x000002A7 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::GetDownloadStatus()
extern void AsyncOperationHandle_GetDownloadStatus_m0C0F49BC92003068201C090DF8E7EB6B2E7167D4 (void);
// 0x000002A8 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::InternalGetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
extern void AsyncOperationHandle_InternalGetDownloadStatus_m0D555CA8DBD18A501A708C7CB081E08CDB096D22 (void);
// 0x000002A9 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_ReferenceCount()
extern void AsyncOperationHandle_get_ReferenceCount_m5D87AFB468776BBA67295B48381C78452566245D (void);
// 0x000002AA System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Release()
extern void AsyncOperationHandle_Release_mF869257E566A31A9F6906CD0CC44DF88228C625C (void);
// 0x000002AB System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Result()
extern void AsyncOperationHandle_get_Result_m28D82A4A3AB9E7635A56AB16B7AE9FB631B682FC (void);
// 0x000002AC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Status()
extern void AsyncOperationHandle_get_Status_m518EE3BF10E74A8895F7E5699D5F1F2EAD5C0458 (void);
// 0x000002AD System.Threading.Tasks.Task`1<System.Object> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Task()
extern void AsyncOperationHandle_get_Task_m2213154D1F81A13237EDBA72185106E6F772DB9F (void);
// 0x000002AE System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.get_Current()
extern void AsyncOperationHandle_System_Collections_IEnumerator_get_Current_m0D8EC2DBF3380292BD75E7776B65EA38EDEBF1FD (void);
// 0x000002AF System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.MoveNext()
extern void AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m25C17DDC68A5908D26C0D82A0EB210209B607725 (void);
// 0x000002B0 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.Reset()
extern void AsyncOperationHandle_System_Collections_IEnumerator_Reset_m4375BF3606971221ACE039AA1254743A85B27C80 (void);
// 0x000002B1 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::WaitForCompletion()
extern void AsyncOperationHandle_WaitForCompletion_mF2981A93A4D0597322B631C7BDBE81EBE96AB019 (void);
// 0x000002B2 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.cctor()
extern void AsyncOperationHandle__cctor_m5E39F156ABC389DED65B841DE0445E74D5AD7D16 (void);
// 0x000002B3 System.Single UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus::get_Percent()
extern void DownloadStatus_get_Percent_mE7E07B2D92779392BE939F2D10027391076FEF9B (void);
// 0x000002B4 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::.ctor()
extern void GroupOperation__ctor_m8BA1E7986541D04D4627F113549B525B69A19DCD (void);
// 0x000002B5 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::InvokeWaitForCompletion()
extern void GroupOperation_InvokeWaitForCompletion_mFEE866D25649060A973B097A2FB4C44935A461E6 (void);
// 0x000002B6 UnityEngine.ResourceManagement.Util.IOperationCacheKey UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::UnityEngine.ResourceManagement.AsyncOperations.ICachable.get_Key()
extern void GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_get_Key_m311D252A628C86052418A2DDD542964F90ED600F (void);
// 0x000002B7 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::UnityEngine.ResourceManagement.AsyncOperations.ICachable.set_Key(UnityEngine.ResourceManagement.Util.IOperationCacheKey)
extern void GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_set_Key_mB676970E9ECFF566F3F8681D82CCA6EE2462F619 (void);
// 0x000002B8 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::GetDependentOps()
extern void GroupOperation_GetDependentOps_m46BB9E741E15BEBC2AFC3FF3D650BCB4C90A2428 (void);
// 0x000002B9 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void GroupOperation_GetDependencies_mC1D69E1844CED59D542EBCB9253ABDCF45DF8AF9 (void);
// 0x000002BA System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::ReleaseDependencies()
extern void GroupOperation_ReleaseDependencies_mAC5DF932239F1CE59D2BCF41B346DE0106EB733D (void);
// 0x000002BB UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
extern void GroupOperation_GetDownloadStatus_m569F2E77B438458FE8943DDFD487F9E9A665AFA6 (void);
// 0x000002BC System.Boolean UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::DependenciesAreUnchanged(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void GroupOperation_DependenciesAreUnchanged_m3F32CF0F69EA8FD043CD8897BE0321C94C02D67B (void);
// 0x000002BD System.String UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::get_DebugName()
extern void GroupOperation_get_DebugName_m9832D4D4CA8BDD84DB23334C9941A0E525A0A318 (void);
// 0x000002BE System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Execute()
extern void GroupOperation_Execute_mFCA40CC8EC876FEE011CFFEEA02A3CDBE0D0B61A (void);
// 0x000002BF System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::CompleteIfDependenciesComplete()
extern void GroupOperation_CompleteIfDependenciesComplete_m7227335A4896D7F6DAD05E27EF1DAE7D6CAFC1FC (void);
// 0x000002C0 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Destroy()
extern void GroupOperation_Destroy_mF4C74B691E92BFC415938EBE09CFB456AD36FA73 (void);
// 0x000002C1 System.Single UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::get_Progress()
extern void GroupOperation_get_Progress_m06AE4CE1CC3A9487D832554002641CBA8E94DFDB (void);
// 0x000002C2 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Init(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean,System.Boolean)
extern void GroupOperation_Init_mEE131C7BCAE181CB8F78004C510B041A0A9EDEBD (void);
// 0x000002C3 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Init(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,UnityEngine.ResourceManagement.AsyncOperations.GroupOperation/GroupOperationSettings)
extern void GroupOperation_Init_m4727FD5451CF5CB82F643BECCBA9200247B20179 (void);
// 0x000002C4 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::OnOperationCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void GroupOperation_OnOperationCompleted_m98CA3532152B8FE12B9E48B75361E9BF4FE5F5B5 (void);
// 0x000002C5 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
// 0x000002C6 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>,System.Boolean)
// 0x000002C7 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_ProvideHandleVersion()
// 0x000002C8 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_Location()
// 0x000002C9 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_DependencyCount()
// 0x000002CA System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
// 0x000002CB TDepObject UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::GetDependency(System.Int32)
// 0x000002CC System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::SetProgressCallback(System.Func`1<System.Single>)
// 0x000002CD System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::ProviderCompleted(T,System.Boolean,System.Exception)
// 0x000002CE System.Type UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_RequestedType()
// 0x000002CF System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::SetDownloadProgressCallback(System.Func`1<UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus>)
// 0x000002D0 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::SetWaitForCompletionCallback(System.Func`1<System.Boolean>)
// 0x000002D1 UnityEngine.ResourceManagement.Util.IOperationCacheKey UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::UnityEngine.ResourceManagement.AsyncOperations.ICachable.get_Key()
// 0x000002D2 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::UnityEngine.ResourceManagement.AsyncOperations.ICachable.set_Key(UnityEngine.ResourceManagement.Util.IOperationCacheKey)
// 0x000002D3 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_ProvideHandleVersion()
// 0x000002D4 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_Location()
// 0x000002D5 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::SetDownloadProgressCallback(System.Func`1<UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus>)
// 0x000002D6 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::SetWaitForCompletionCallback(System.Func`1<System.Boolean>)
// 0x000002D7 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::InvokeWaitForCompletion()
// 0x000002D8 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x000002D9 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::.ctor()
// 0x000002DA System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000002DB System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::ReleaseDependencies()
// 0x000002DC System.String UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_DebugName()
// 0x000002DD System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
// 0x000002DE System.Type UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_RequestedType()
// 0x000002DF System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_DependencyCount()
// 0x000002E0 TDepObject UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDependency(System.Int32)
// 0x000002E1 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::SetProgressCallback(System.Func`1<System.Single>)
// 0x000002E2 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::ProviderCompleted(T,System.Boolean,System.Exception)
// 0x000002E3 System.Single UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_Progress()
// 0x000002E4 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Execute()
// 0x000002E5 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
// 0x000002E6 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>,System.Boolean)
// 0x000002E7 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::WaitForCompletionHandler()
// 0x000002E8 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Destroy()
static Il2CppMethodPointer s_methodPointers[744] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MonoBehaviourCallbackHooks_add_OnUpdateDelegate_mF0A7D9A91F2790E2B5E411D31ECC9B5B4B3DCD5D,
	MonoBehaviourCallbackHooks_remove_OnUpdateDelegate_m00215EA5426D369E8D7C7198819192B433D0E99C,
	MonoBehaviourCallbackHooks_GetGameObjectName_mC4C165B4ECA1E1A5FAD50AAA95C7DDBD95F09BFB,
	MonoBehaviourCallbackHooks_Update_mCC7B226851A48F80003FEB67801607356597F5A6,
	MonoBehaviourCallbackHooks__ctor_mD71C42E2866DE9CE172E4E61EB0214A9D4BFEB6E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceManager_get_ExceptionHandler_m1594AC5FC006CC770B2A0E15C739EB845A9C8306,
	ResourceManager_set_ExceptionHandler_m2CD5AEF214219E421AF7BAA9057274336F72C6AF,
	ResourceManager_get_InternalIdTransformFunc_mB5CBFF62F538F2E9387BC31FA5DF4FDBC39813AB,
	ResourceManager_set_InternalIdTransformFunc_m8F62669979EAECF8ED0E7FA2CDE7D219826A250B,
	ResourceManager_TransformInternalId_m5B3836837396CC2CBF26C9D1B77809A73A835669,
	ResourceManager_get_WebRequestOverride_m973A2421D534233FDB86FA23BA756F3AEFA0FECB,
	ResourceManager_set_WebRequestOverride_m5BA4765700261D0E917CC8F52215973E54ACD870,
	ResourceManager_get_OperationCacheCount_m4E3CB779AE01BD54B04544B20A761188B7C3DC40,
	ResourceManager_get_InstanceOperationCount_m7FF092E82B500F42DD10FBB204AA6D5113BF979D,
	ResourceManager_AddUpdateReceiver_mD35433816B5B3C8F3D986F8E9DE70872341CE4F3,
	ResourceManager_RemoveUpdateReciever_m53AAAD4F31B434164DCF15F4CF787FD104F490D8,
	ResourceManager_get_Allocator_m617C32C69B3AF34BA88127E5EE31B8B2731AE685,
	ResourceManager_set_Allocator_m605775DC52E410670040187897F0C17ED45BC6FB,
	ResourceManager_get_ResourceProviders_m0B0336D1A403DDDB560EB832375B0205AC2B914F,
	ResourceManager_get_CertificateHandlerInstance_m2E6389B8CA321D6850C1EF792F7F30CB85E6AD53,
	ResourceManager_set_CertificateHandlerInstance_m58A06D27892464F5DCB0CF162A4491F63D75BD95,
	ResourceManager__ctor_m8FC110F5D15AE6BB0225B82EE23B6FE589384A63,
	ResourceManager_OnObjectAdded_mB1214C5B5A091942F1BC9F78791FEFF111A5FA3D,
	ResourceManager_OnObjectRemoved_m65985C8F6D034E3125CC0A590CC5C986BEAC36C6,
	ResourceManager_RegisterForCallbacks_m446989CFD30CEAE21A30EDBC7CF5F19293BD57CB,
	ResourceManager_ClearDiagnosticsCallback_m10B6405E9C6F22FB92FC2F4DF590237A2129D696,
	ResourceManager_ClearDiagnosticCallbacks_mFCFC4DB25C14D5D116EF3C20048126747AB38B4F,
	ResourceManager_UnregisterDiagnosticCallback_m2F39BED801D8540D2BC2E4DFC0585A9B7C06F77A,
	ResourceManager_RegisterDiagnosticCallback_m670CAA7C86A37081D1D46BB9ACF98BA9DB7E04EA,
	ResourceManager_RegisterDiagnosticCallback_m5668FFB5E61FADC8859DE00A516B53183A493D83,
	ResourceManager_PostDiagnosticEvent_m7F57B61B13435BAFBF1D99DCC0D1B6579E904218,
	ResourceManager_GetResourceProvider_m5C1B277D488DA44F80077476F23451C8B9BB9840,
	ResourceManager_GetDefaultTypeForLocation_mACD27E5C1DB210607222FC55B6728A9FB96C9B00,
	ResourceManager_CalculateLocationsHash_mE546C5ECB0C13363246BED722B7E5FC899918BA0,
	ResourceManager_ProvideResource_mA287F265CE43E2806FC06637F07A4CE3BDD6C00E,
	NULL,
	NULL,
	ResourceManager_StartOperation_m5F3A0530300FE62FCCA0AA55ADB4AF825AC0E630,
	ResourceManager_OnInstanceOperationDestroy_m51046A99199FE1384F610C8B2D4F9BB45909282F,
	ResourceManager_OnOperationDestroyNonCached_m6BDCF67A3CFC9BDD9A33B7F8693B8E583C00182E,
	ResourceManager_OnOperationDestroyCached_m68BA43A0BC60683A5FB3DC9CCFC003D03C4CF383,
	NULL,
	ResourceManager_AddOperationToCache_m5B8925E6779BCAE8A5C33FFB7F10658EDEF64C8B,
	ResourceManager_RemoveOperationFromCache_m5A131A13A900F20064CBF84CAF3AF0E00251771A,
	ResourceManager_IsOperationCached_m9F43FA5BA0182B2BC19518C543431C9089CEC5D5,
	ResourceManager_CachedOperationCount_m4B99483D311D33CE8FF7E38F87116FD602BF7F77,
	NULL,
	NULL,
	NULL,
	ResourceManager_Release_m1BF99536656867FEEFBEF3A3F4A290AA66402EB7,
	ResourceManager_Acquire_m071783042B4ACA5A506D43DCAF92DE8EBF94E5C2,
	ResourceManager_AcquireGroupOpFromCache_m75CB8FE11064B47C4923C2830D58C9287E368C77,
	NULL,
	NULL,
	ResourceManager_CreateGenericGroupOperation_m637EFAA4DDCCCB4DB5BDE75BBB4B53C61848D8F2,
	ResourceManager_ProvideResourceGroupCached_mE1057C4DB61854D82B4056E22EA6E37D9B4A2B37,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceManager_ProvideScene_mB6328DC75FFFD9CEF4B60511F895F1E2CF709A5D,
	ResourceManager_ReleaseScene_m459CB8CAAB851FA3495F881AEFAD1C3FF29106FA,
	ResourceManager_ProvideInstance_m9C1635807FC0ED58FB88BEF2B803BC9C1AAD14F3,
	ResourceManager_CleanupSceneInstances_m9B491B22C7532958A6751CDD08384E7474D1E8CC,
	ResourceManager_ExecuteDeferredCallbacks_mA834F0420F4C269DC21BCB39AFF67C51325AE373,
	ResourceManager_RegisterForDeferredCallback_mDE63306BA1891888B6E2C57E74D4CF8AF8F51952,
	ResourceManager_Update_mA2F03AF254D8794D37A7821F905AA8FF8EC97FFF,
	ResourceManager_Dispose_m57783F295F3FE72496F8B2CC2BC06C671643CF53,
	ResourceManager__cctor_m75EFB1F6063CB219CCEBBC30CF586872EEAC4F82,
	ResourceManager_U3C_ctorU3Eb__53_0_m8C9E434B0D88F1BD078FB518418F84516976C57E,
	DiagnosticEventContext_get_OperationHandle_m870675A1B170E90324CB13D4320F2DEF29D6A1BD,
	DiagnosticEventContext_get_Type_m3C77CCA8778F7D55230D59FD0B6DEB37F910B15E,
	DiagnosticEventContext_get_EventValue_m186FB7D8DFACBD40B9A7FC254685B544C5FC5448,
	DiagnosticEventContext_get_Location_mC7B8C8151070981ACEF34A2110E16024587691B7,
	DiagnosticEventContext_get_Context_mB145AF762ACC916F5CBCD03B392B36CB6BBD5D91,
	DiagnosticEventContext_get_Error_m6758C6A5F0F1C59C8AFB8A5D5FEF7547C806D808,
	DiagnosticEventContext__ctor_m46A56FA95388629125EDEE74C865060584274238,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InstanceOperation_Init_mE5EE6B621B9B927D75F2281EDDEC91E5DE16A9CA,
	InstanceOperation_GetDownloadStatus_mA1C48F14B892497D46715D8F8A0160232A97B90F,
	InstanceOperation_GetDependencies_m26D3CB8DE0FCFC784B2C9663A482F66BD92C72E5,
	InstanceOperation_get_DebugName_mE303D6480456152374BCE927EE4737D33E70E7FE,
	InstanceOperation_InstanceScene_m6AF4F1151492C24B07C214A98DBAB5CBE64E3DD6,
	InstanceOperation_Destroy_mC17ABC3FF150BAE7C7C6DD031B741D1099C174AE,
	InstanceOperation_get_Progress_mFB3EC02597F244CEE84815611AFD4074E1028883,
	InstanceOperation_InvokeWaitForCompletion_m47D017281ACE110F97759C17837B48FD06827D5D,
	InstanceOperation_Execute_mAED30650EF9D31AB1A5CC87410964DFB4149BF2C,
	InstanceOperation__ctor_mBE633769231DE956B1D301282CC4D488F52495C8,
	NULL,
	NULL,
	NULL,
	NULL,
	WebRequestQueueOperation_get_IsDone_mA24CD38542F301A96BA73352520D86348D483079,
	WebRequestQueueOperation__ctor_mE3E3145649464B791DDC02D0BA75AEC6A5CA0B7E,
	WebRequestQueueOperation_Complete_mBF0118DBD2907500B5A87F494B14672DBF534763,
	WebRequestQueue_SetMaxConcurrentRequests_m717A2C2435BDA305652D8A6E8CE898EF6185F09E,
	WebRequestQueue_QueueRequest_m54F1A56E5082365292141AC67CEFEBEEA52F04AE,
	WebRequestQueue_OnWebAsyncOpComplete_m27F37E235E00801CCD54A4D8ED49538371FC0180,
	WebRequestQueue__cctor_m3B750ADE0715FB14DEBF388CA01266DF0B455925,
	ResourceManagerException__ctor_m846D7FCC2C06373694800B5220DF73E6D26F6BA7,
	ResourceManagerException__ctor_m9181C112F2C8E32E77CEDE21C4B5DF0F1CBB5948,
	ResourceManagerException__ctor_mE76839DF077E4AB88010D559E270AB53D2509A66,
	ResourceManagerException__ctor_mCE13F6D2BC0FAE0B8B66A9C13CFABA3EF7879F04,
	ResourceManagerException_ToString_m7411781F2735E93662661A41DF3A92E29E637DC8,
	UnknownResourceProviderException_get_Location_mC37FB4E0339053A4F3C5DFDEFAC15F8B4E1A7CA6,
	UnknownResourceProviderException_set_Location_mDE78145787A8CDF8058E1FE350F79F23C6E0121E,
	UnknownResourceProviderException__ctor_mA08582F29A5DF6471FEE1170D7EB38CAFEAB5C5F,
	UnknownResourceProviderException__ctor_m4B7176BAE46F30C73450E5306F911BD53EDD151E,
	UnknownResourceProviderException__ctor_m365634FE703F611EDC55F3E4142B4E69EB72F682,
	UnknownResourceProviderException__ctor_m682FE80405FA7A8FAFEBBA1F7605379338B7BB97,
	UnknownResourceProviderException__ctor_m0729FF6A25B6CB353ECE181A7C907E15B64879FF,
	UnknownResourceProviderException_get_Message_mF411A2558453686665445094929B223C43FB6C80,
	UnknownResourceProviderException_ToString_m8E3FCBA2D2A41E951C60958FC604DC7D4A83C26E,
	OperationException__ctor_m59B04AEC4F7E07F7CAEEF3ABB2BB4E0CDE1C6C2B,
	OperationException_ToString_mD1EDB81DD592A9B2BB2FCCB74535194AB82135B8,
	ProviderException__ctor_m03D782F952F7BCE889D2F37690183D3B74E9579A,
	ProviderException_get_Location_m77203D80F7C090A29D74481729AF9AB1C7026385,
	RemoteProviderException__ctor_m4E8E891B128CE300B1972CCC8486576F6263A335,
	RemoteProviderException_get_WebRequestResult_mC841D99699ACAAF04B768618B6A76360B291158F,
	RemoteProviderException_ToString_mC7DE76A1E17748A03D7714F21AD779F74F096598,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DelayedActionManager_GetNode_m0DE3584622A131A6793E55EACD16CA4019947088,
	DelayedActionManager_Clear_mCE2BE0F40CA9566F3AD0FBC7506D2AAE6FC99380,
	DelayedActionManager_DestroyWhenComplete_mA1D76FBA64E4BBB878914741B403BF2904F9DA01,
	DelayedActionManager_AddAction_m50BBD4B75371CB8B2A2230DF09216C2F85CF9262,
	DelayedActionManager_AddActionInternal_m613DDDB8D8F5A44CBE0793FEDE3EA80E0FD0AC0B,
	DelayedActionManager_get_IsActive_mF1D78D5B0C9E9EA99B953C6F26868C53A02AC32F,
	DelayedActionManager_Wait_m18E9BC16EFA84B02DCFCF3C56C31EF2F4B356F8F,
	DelayedActionManager_LateUpdate_m54908B1CB07F9901C208ECD97A82BB62A9B70D91,
	DelayedActionManager_InternalLateUpdate_mD6B2568B45B54B79717B73422415240BFA28F37C,
	DelayedActionManager_OnApplicationQuit_m209580E2C7D3265295F1B1048880C96416F5E198,
	DelayedActionManager__ctor_mC4435A115C4DD5A9112A7B40CFD330F7D8340320,
	DelegateInfo__ctor_mBB59764DCA627416685191C4BFFDF7E6B3F9EEB0,
	DelegateInfo_get_InvocationTime_mD426371CDDBF73985C41649D481C1C8032731780,
	DelegateInfo_set_InvocationTime_mDDF970305686BAF18D3644E6F8A0FBFAFF868A75,
	DelegateInfo_ToString_m088DA64541A8A73664E1C101FE2726B1325AC5BC,
	DelegateInfo_Invoke_m9010FEA078121055ACEBF2CD5049762D439DDD8F,
	LocationCacheKey__ctor_mD8A86424D643E0CE544415BA56B1498C93D828E6,
	LocationCacheKey_GetHashCode_m81D30AD7850BBD10C2E8B8CC0C9BD7D75E11F482,
	LocationCacheKey_Equals_m4787FEB8DCD0DECCF9D802B7725D466D4FCEE53E,
	LocationCacheKey_Equals_m845F8372C30A66C9FE2CD5B9492BEF6BF511C24B,
	LocationCacheKey_Equals_m2C6339CC5C0E2787022952381AE1B19F7937900F,
	DependenciesCacheKey__ctor_m8310CEAE00B35CE8ED5B164EE015922CB36B4056,
	DependenciesCacheKey_GetHashCode_m756877F9727C1D27037788709F55D7F037FB5793,
	DependenciesCacheKey_Equals_m95B4A80078B035E0241DA96F5C0154268DF79048,
	DependenciesCacheKey_Equals_m7769DCAB37B09832B486E3F2C6913C3787AD4694,
	DependenciesCacheKey_Equals_m7551BB00E98F881DE4B9F902E04D6C5F51C4E66B,
	AsyncOpHandlesCacheKey__ctor_m9215A7B7270B8BC67626E7E23DC671134BD16770,
	AsyncOpHandlesCacheKey_GetHashCode_mBE006B60A6F6BBC8E9B2D46EE53F4A2EBDD1D869,
	AsyncOpHandlesCacheKey_Equals_m8ACA6D8CC059FAB6EC656C7ADF99DF0046EFE086,
	AsyncOpHandlesCacheKey_Equals_m6B07F82C191F639D3CE866ABC95FBF162B251904,
	AsyncOpHandlesCacheKey_Equals_mF0F4D09486DA5EECFFA9D1651AE5C9D980F0CCA5,
	LocationUtils_LocationEquals_m8B9AC694D195DEACE9F8A54BC6920C2AA565E6FF,
	LocationUtils_DependenciesEqual_mCE6FC543947C36086C53A2D3DA63854C83AD2F79,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DefaultAllocationStrategy_New_m08D4B93377956FCA42653F39C2675229759A80D1,
	DefaultAllocationStrategy_Release_m8AD07C016B04002C080A2745A6892A6852DD6D06,
	DefaultAllocationStrategy__ctor_mAA0485502871FF659EED040E6835C0C9053344B9,
	LRUCacheAllocationStrategy__ctor_m6AB5B8D1E389B9300ABFC78F7C1B9F33FE0977C9,
	LRUCacheAllocationStrategy_GetPool_mB24B50035BCC44F88A78479974237784205D6914,
	LRUCacheAllocationStrategy_ReleasePool_mA171571331A0C7B2C747FAC28483247D5F83B674,
	LRUCacheAllocationStrategy_New_m85F22B7C23EB9A073651189EA98D08B0292AF3F4,
	LRUCacheAllocationStrategy_Release_m37E39356812AF84792B4E23D0D34453DEB84E528,
	SerializedTypeRestrictionAttribute__ctor_m4060A47D9718A425D3C39090B02C1967B3A1D0E4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SerializedType_get_AssemblyName_m2DBDDEF51A9338BF298BCBBD2EE398D72E963635,
	SerializedType_get_ClassName_m06DB26816F632CCAF380F818EA666D85C409D8B2,
	SerializedType_ToString_m85C9B44C29867CD01DAA7CB5A365577818B4212A,
	SerializedType_get_Value_m412D3B47698A74EFA8A3B29B4D102842714BD6B0,
	SerializedType_set_Value_m0E1021AEE9B0E8B98800E79734E8B140032B9E54,
	SerializedType_get_ValueChanged_m1B7B62A60583EE0D14418C391367985DBE6E7562,
	SerializedType_set_ValueChanged_m7EB0C0A5F56B0594CC3578191F4E26CAC1D08D7D,
	ObjectInitializationData_get_Id_m1455892EB3BA6CD3ED9F9A73F6401285E5486C65,
	ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532,
	ObjectInitializationData_get_Data_m026CC155470843EE675C47D80DDE91CFA7B05AA0,
	ObjectInitializationData_ToString_m43DC1ED8748A5B962C707EA72E529243ABDA37D6,
	NULL,
	ObjectInitializationData_GetAsyncInitHandle_m3050F492A6972087E9AA5371C7946CBE5F29AC42,
	ResourceManagerConfig_ExtractKeyAndSubKey_m48B90CB062DF102F56698425FE9F75795239ED15,
	ResourceManagerConfig_IsPathRemote_m4A0EC7B1CEFE8C1B172D2D7CA1FCC2EDDF09FA12,
	ResourceManagerConfig_StripQueryParameters_m246B6D7D06C982C37967D2D2B519A388D5C1982C,
	ResourceManagerConfig_ShouldPathUseWebRequest_mC7BD63058F88117DA2622430B22DB7B80D138D76,
	ResourceManagerConfig_PlatformCanLoadLocallyFromUrlPath_mD57B260F40C413C2DE40A28CD7E3237763DB203A,
	ResourceManagerConfig_CreateArrayResult_mE028A19D4D9E4908BE5911B8E7A21A85D18237B6,
	NULL,
	ResourceManagerConfig_CreateListResult_mE82A475A890F6091076CAC624DF32496CD75CF1F,
	NULL,
	NULL,
	UnityWebRequestUtilities_RequestHasErrors_m28EF4B6F3522720AAF6FB3BD92E43018E38CFDE0,
	UnityWebRequestUtilities__ctor_mBBF82A3247026EEA4DBDFD2D5A02434C4E3F3687,
	UnityWebRequestResult__ctor_m0AFC547859BBBEABF97E9C8724FF7026CC5C5C56,
	UnityWebRequestResult_ToString_m1EB1F18F0AA739DC863DE424508CAEE4E8A0814B,
	UnityWebRequestResult_get_Error_m1B7977325F245AAC94E787466A19E155475D1459,
	UnityWebRequestResult_set_Error_m60D36D971598B57CBEC9F212C5D3E407109D09FB,
	UnityWebRequestResult_get_ResponseCode_m8870B4512CF753942C4A697FC2B1AA18177EF8D0,
	UnityWebRequestResult_get_Method_m4E86DCE65AE49CE851CA8AE4E26611AB42089D47,
	UnityWebRequestResult_get_Url_mC042408942366A6D90B9ACA42FAC66086577A3E0,
	DownloadOnlyLocation__ctor_mCFDD58E4623B02114520A4E0698DC42FD230461D,
	NULL,
	AssetBundleRequestOptions_get_Hash_mB4B18C05B59265AA39B3A02C0688DE1D1C88711A,
	AssetBundleRequestOptions_set_Hash_mE0924B6592A1DFC9D594E11FF135AD1E87B41D3F,
	AssetBundleRequestOptions_get_Crc_m2BA7D512A54EA6075A0CF024D012B6433C8A9A4E,
	AssetBundleRequestOptions_set_Crc_m2FB9FEAAEBC071272C46D9F8C02AE3BC5FBCD635,
	AssetBundleRequestOptions_get_Timeout_m42D9843C4E83D152DAE0CE4CD3E8E65E0623080B,
	AssetBundleRequestOptions_set_Timeout_m40BFCFEF67A5C0D73A9F289EF8AA6A885479FC50,
	AssetBundleRequestOptions_get_ChunkedTransfer_m5D4E14BA8EB284968D3BD0CFC5473B69A1FB04CD,
	AssetBundleRequestOptions_set_ChunkedTransfer_m790165DE615D3A3D0CE204FEE2F4133D719CE499,
	AssetBundleRequestOptions_get_RedirectLimit_m099ACB2C16C55D0C615FF15E71908E3125D7F8AC,
	AssetBundleRequestOptions_set_RedirectLimit_m0B11D7F0C5B94AD17B0C7E1729130E8B5BE5D7E7,
	AssetBundleRequestOptions_get_RetryCount_m19487390C594350D557346716F1CB3C0B38C2B3F,
	AssetBundleRequestOptions_set_RetryCount_mA0A092315042B6625A1B8FBC5383491B7C414CD3,
	AssetBundleRequestOptions_get_BundleName_mC794C3CB5F0DD2ED320B34F44E2F586A55C24CF8,
	AssetBundleRequestOptions_set_BundleName_m9C66005D818F3CFDA4B79FDF4011CF27F0C2A72E,
	AssetBundleRequestOptions_get_AssetLoadMode_m65323BD2C2036B68237820458D8755B8FAC634E3,
	AssetBundleRequestOptions_set_AssetLoadMode_mAAEED5FA4AE47A36F36F65678BF3E9A9A60B9475,
	AssetBundleRequestOptions_get_BundleSize_m01BFA314FF232985809F91FA0D1711984582AD5E,
	AssetBundleRequestOptions_set_BundleSize_m26D31A8E9D68404B7F5977B490BFE815D5D01D02,
	AssetBundleRequestOptions_get_UseCrcForCachedBundle_m9509A29234CA8EF1ABB637FEB3996CAB3633AD17,
	AssetBundleRequestOptions_set_UseCrcForCachedBundle_mA4BF24C012E9D1191FD0DFDCDD2B2E9500696459,
	AssetBundleRequestOptions_get_UseUnityWebRequestForLocalBundles_m59F9377203E4DB22D8F4895D434837525EB38002,
	AssetBundleRequestOptions_set_UseUnityWebRequestForLocalBundles_m0B3863B045E1BB34CF48FE63F67256DD9F576820,
	AssetBundleRequestOptions_get_ClearOtherCachedVersionsWhenLoaded_mA4B29E0A6CD5A0AD02D07652BFDB8FEA8BBBAACC,
	AssetBundleRequestOptions_set_ClearOtherCachedVersionsWhenLoaded_m73A8ACC0FBB4D029AB35494A4768C5A1D83B72D2,
	AssetBundleRequestOptions_ComputeSize_m2E82047D611D69C7AADC6FD0151E2EF610E4D202,
	AssetBundleRequestOptions__ctor_m478D494DCB048FCC2462FE9D87ED34991E0D0869,
	AssetBundleResource_get_HasTimedOut_mEB56C4FD339E732E126EAC1308CD5382205AF807,
	AssetBundleResource_get_BytesToDownload_m74F57D1F457D1FEC59FFB0E36B42987E72C6E4FF,
	AssetBundleResource_CreateWebRequest_m008113D1236E539144C558988C2D6F9D71881962,
	AssetBundleResource_CreateWebRequest_mE7E91444E6A63D7C6B557CEBD5BD21FFD4CC5CC4,
	AssetBundleResource_GetAssetPreloadRequest_mD30D88065725E48072AA5298A88789BFF1D759B3,
	AssetBundleResource_PercentComplete_m83436C87C7EE6E0FF6C28FB7E05B841308DFA8AB,
	AssetBundleResource_GetDownloadStatus_mD477BF4665169B01680B78FCB57CE9095BEF6F13,
	AssetBundleResource_GetAssetBundle_mEDCC866A7AF1A532F40B40B821DD457AC73FB8D0,
	AssetBundleResource_Start_m165368F7AD26E5F1C74190C95817A9D5757D2BA4,
	AssetBundleResource_WaitForCompletionHandler_m33BCD14B7534789D0F4895E8C4B7329736EB404A,
	AssetBundleResource_AddCallbackInvokeIfDone_mBE568F524737A2CABE082BCD5C7C90F889377997,
	AssetBundleResource_GetLoadInfo_m07B22D0C0EBF139FB9E1EDDECFCDECC5F30410AE,
	AssetBundleResource_GetLoadInfo_m31EAFB0F163FC44B785266F67D2DD891BE82F961,
	AssetBundleResource_BeginOperation_m8A9873499D80D88F50FC5F20FA039F0AEA50DC12,
	AssetBundleResource_BeginWebRequestOperation_m8A9EF8065A3261F869AB86670EC356A5BFB5E062,
	AssetBundleResource_Update_m3D23022F4E2C939083A0E7D12FFA94EE6AC9066D,
	AssetBundleResource_LocalRequestOperationCompleted_m5B5121EDE02A34809BF275AD11A3D8785686FF5F,
	AssetBundleResource_CompleteBundleLoad_m03C8DC0761FB6F73DD8891D40055B21BF3667DE3,
	AssetBundleResource_WebRequestOperationCompleted_m36359C93B7E68673166D558680F003D314E4AD1C,
	AssetBundleResource_Unload_m141896873F3F31F8C587361583C8F71DC3F7049E,
	AssetBundleResource__ctor_mD5B4085DF1B5D6935198B89C651FDF6818C43835,
	AssetBundleResource_U3CGetAssetPreloadRequestU3Eb__25_0_m9136E232EDA239DB42142C70CCEE6341D5E7FEF6,
	AssetBundleResource_U3CBeginOperationU3Eb__34_0_m29DEADBD1103682676E7ABA67B2D7760EF34BD8E,
	AssetBundleProvider_Provide_mD46D28C6CA670CA740FE87912320632FDB659C1E,
	AssetBundleProvider_GetDefaultType_m79830BEE6CE701C7DF95E65F33C5AF8B74A83852,
	AssetBundleProvider_Release_m48DDAE8401ADD5932DAAC61C4D161155FF43B3E6,
	AssetBundleProvider__ctor_m5C55E1DE6E0E02A232B307350CA4D76C187402B1,
	AtlasSpriteProvider_Provide_mF045676D19D97CD57A7BC0807027C8BB78217E15,
	AtlasSpriteProvider__ctor_m05488EDAFF03B092471403E9999CD0266C5AACED,
	BundledAssetProvider_Provide_m8C2EDDF78D3CCC187CC41F63E8FC695597C4A12E,
	BundledAssetProvider__ctor_mC25705358F17D4412D359E73C5F4578CF85192EB,
	InternalOp_LoadBundleFromDependecies_mDB83267FA2999729FCFAA33B52C09EC47B91F73C,
	InternalOp_Start_mA10ADF4F26C477CD58C6E8F808165CCF1270A74D,
	InternalOp_BeginAssetLoad_m5BDAE4D0596E4E634EDF56570572FA10C33EA555,
	InternalOp_WaitForCompletionHandler_mB09AA0D8EFAE4799B258E318EFF94F523CDECEFA,
	InternalOp_ActionComplete_m2F628707CEA05C810AE258C5549C47EECABBCA9D,
	InternalOp_GetArrayResult_mE65E76ED9737FC08A944AFD8EED9160F77D054DF,
	InternalOp_GetListResult_mBCB64E34114928CEF32174FCB1C90AFE96471BD3,
	InternalOp_GetAssetResult_m91F7D4D3CB8A5EA5DEAC395A84A87A3FE1193810,
	InternalOp_GetAssetSubObjectResult_m58606D07640F409140212A5C07318E7729C2DEAD,
	InternalOp_CompleteOperation_mCC9E4E3A80A3E8F61A7597370A8B367DCCF31693,
	InternalOp_ProgressCallback_mFD402D64CE77420108145A6E6DB428E914D7BF57,
	InternalOp__ctor_mB8936B16062FBD1BCDE173A781968358E492FB7F,
	InternalOp_U3CStartU3Eb__7_0_mA908139CCF38B2A3C1DF3C448AB79D37205A85D6,
	InstantiationParameters_get_Position_m9569A28B575846AD814790114A6CFAB8FC3E32DC,
	InstantiationParameters_get_Rotation_m155362C3F3442CA4CC53C063FA8B4D09B753B012,
	InstantiationParameters_get_Parent_mD6BC05225FF561D2123B1745B838D147D9B6A621,
	InstantiationParameters_get_InstantiateInWorldPosition_mF6ACF2A60E6C89C55CC36D82220910EEA9CC65F4,
	InstantiationParameters_get_SetPositionRotation_m3B5F3C4065F65ED2D4FD75B3B81D311B15EB70D1,
	InstantiationParameters__ctor_m89D89058D329E9C651528936FD098282F1589225,
	InstantiationParameters__ctor_mE86F10CB3D40AC7EA1883000E381AEAEA1E16A33,
	NULL,
	NULL,
	NULL,
	ProvideHandle__ctor_mB9CCF5CA6F8DD98AB72B4659076385051038795C,
	ProvideHandle_get_InternalOp_mDCF56F543329C2078BA6B8CE99C39B6EF3BEA41F,
	ProvideHandle_get_ResourceManager_mD9EAFA3333B5A459E1AD6EA76E15558C531ACD76,
	ProvideHandle_get_Type_mC36FCA0E5806E401F98BC3D30511DE5015A81EFF,
	ProvideHandle_get_Location_m38F31BC3081090D72C7EFA9C6CDE73ED45B7D978,
	ProvideHandle_get_DependencyCount_m56B0737197C7470C2A5FF8E8E2BBDA151E572BA4,
	NULL,
	ProvideHandle_GetDependencies_mC65A5900B3EA9F0874C3D19BEFFC022DFC9AD8E8,
	ProvideHandle_SetProgressCallback_m3B3F019C3AFF1A6B0F37B5B28F9A1121EBB73FD2,
	ProvideHandle_SetDownloadProgressCallbacks_mE3370C49221CFB6CDED35F5FE373ADE5AD9270EF,
	ProvideHandle_SetWaitForCompletionCallback_m96207FCBF579C5988A651BBECB844F0E37DB9B25,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SceneInstance_get_Scene_mFF895316B46CBB8126ABE0FE8F85985D1232CCA3,
	SceneInstance_set_Scene_m8AD506582C450F4C678C7265ED0A94FFDA9179E5,
	SceneInstance_Activate_m52A0E7A28EEA007FF2529340E980D798374E5B0E,
	SceneInstance_ActivateAsync_m0F5677DAD4ED2EC1EB027E261B4EBF40418A3DC3,
	SceneInstance_GetHashCode_m9B65494AC6ED027B091B9FF2DE58DEE6E46FFF0B,
	SceneInstance_Equals_mFD3D668A674D5838BF6F9ED892C0452E48729A43,
	NULL,
	NULL,
	NULL,
	SceneProviderExtensions_ReleaseScene_m6E365DB71BF68925751FC3DE6BD654F216F0D1DE,
	InstanceProvider_ProvideInstance_m530D2258769CC2B97647ECFBCF34659BD93C2C6B,
	InstanceProvider_ReleaseInstance_m9DB74D96301B8DD3263AF1AB860E46D85EE08690,
	InstanceProvider__ctor_m35BAD5E188BFB18C9BDCC7AE47F6B9338A7B5D2F,
	JsonAssetProvider_Convert_m97EC94F47C2FA697A38522205E3C972497113AE2,
	JsonAssetProvider__ctor_m15637BC90EF129F6F31918CDAE728E5D9560F303,
	LegacyResourcesProvider_Provide_m4253335CC4BDC849FAD018D5BBDBD3392D03691F,
	LegacyResourcesProvider_Release_m330D49267272717059BD526433B358E581083253,
	LegacyResourcesProvider__ctor_m578CAD18E82E4F66CEFCA1A30B00338FB677A4C6,
	InternalOp_Start_m8CF6A6B5D9675C182B0AAB762BC3E30752CA8EA5,
	InternalOp_AsyncOperationCompleted_m5821F76BA226C04C5188E80E48DC97EF186A72DA,
	InternalOp_PercentComplete_m3E24E8827F98571B4532D1A36382047CC296EBD8,
	InternalOp__ctor_mF1AFFC0E9D8BF2049B3255E65A87883BDDE2EF04,
	ResourceProviderBase_get_ProviderId_m39F6890BE274231ADB091ED135F06B475C7AD842,
	ResourceProviderBase_Initialize_mB88D098C7CA16F9B29CD25F127E586C60271DEC6,
	ResourceProviderBase_CanProvide_m124E66DC2469547DA7E58A5396A1FCAC3B470D85,
	ResourceProviderBase_ToString_m4ACCDF1D1E881C7CC1537F638B29F9BCC9654065,
	ResourceProviderBase_Release_mA4EA7CE2F575DA1E9B1A6B8EA9F21896DDC411A7,
	ResourceProviderBase_GetDefaultType_m00E90DFA2818C81077829B7AE9F9847834520E02,
	NULL,
	ResourceProviderBase_InitializeAsync_mA971647D0097330ACCD75D035481D16D8B8EEFFA,
	ResourceProviderBase_UnityEngine_ResourceManagement_ResourceProviders_IResourceProvider_get_BehaviourFlags_mFDEC1DF913DFD712F248FAD2A36DB610F7580F25,
	ResourceProviderBase__ctor_mA8589431AF866EE209169DA0E84D70845D0F7C1A,
	BaseInitAsyncOp_Init_m84C2E47DCA1E087F49D35279878445687E3E16F4,
	BaseInitAsyncOp_InvokeWaitForCompletion_mEB21D8F33ADDB24034254C2DFD3CE28DA4819EF6,
	BaseInitAsyncOp_Execute_mB78A5ED65E7B68D61754EFBF6A73055F64F7A129,
	BaseInitAsyncOp__ctor_m7D92010EA1BB2C67B605DC95E8D91E500AC7FEDA,
	U3CU3Ec__DisplayClass10_0__ctor_m3B6FAF943AAB23C2BD7DF7E6AB9D1B3699703856,
	U3CU3Ec__DisplayClass10_0_U3CInitializeAsyncU3Eb__0_m19A259CF5D733F72D104DEFAD3348199EDF35F35,
	ProviderLoadRequestOptions_Copy_m43E827A86A7B8E8AE9FFFDC166DEAAED0A338AA9,
	ProviderLoadRequestOptions_get_IgnoreFailures_m536D7EA26D23AC3A8D3106AC22B3D7E8D10144F2,
	ProviderLoadRequestOptions_set_IgnoreFailures_m42288FE2F8E5C77DB7CDC484C6629C1893DDC300,
	ProviderLoadRequestOptions_get_WebRequestTimeout_mE66FAD12D6DF46237874EA1771D66C65F1AC3575,
	ProviderLoadRequestOptions_set_WebRequestTimeout_mB6169D94811EE00AD32534426A8C15C894E2140A,
	ProviderLoadRequestOptions__ctor_mAF174C23E45D4832B9FDAC7EE698D5CCA00B2B1E,
	SceneProvider_ProvideScene_mCB31C5F58277AC5B97E3B0E10317FE34EDFD58F8,
	SceneProvider_ReleaseScene_m1A7A7B799DFEF64C68E7101546579CC907E0048A,
	SceneProvider_UnityEngine_ResourceManagement_ResourceProviders_ISceneProvider2_ReleaseScene_m78F1024F222BE45D5C97D5E9D458A93AA2B64972,
	SceneProvider__ctor_m1B35416831C52CC189753B5624976389772A45F7,
	SceneOp__ctor_m5B0A2397AF0B1ACF6E2FD5323BA6EC60B4DA0147,
	SceneOp_GetDownloadStatus_m9612A40426B520094350AB785F09D6CDE3621767,
	SceneOp_Init_mB918CB5783BF2635D2970748F35C9D2DE689304A,
	SceneOp_InvokeWaitForCompletion_m9F7563A2D5335D3DE45383E0E2F0AF0D5ADA0E04,
	SceneOp_GetDependencies_mF478AD3651950802CCDD736B1FBC44C32FE692EB,
	SceneOp_get_DebugName_m7260EF67264EC70C626B11F4FFB8E73A0965610D,
	SceneOp_Execute_mCAB604D31CBC76E96284377779B79C74E78E2C6E,
	SceneOp_InternalLoadScene_m9C5B15DCB1405A84EA255ECE59B90D419D6C5653,
	SceneOp_InternalLoad_m157B04FAA5544C6CB3221855FD67178212D6E201,
	SceneOp_Destroy_m701CC2DE4463C54C16757EB3D0A0A94E71077696,
	SceneOp_get_Progress_m378936898C1A6DB059D093337B1DCC400689FB5B,
	SceneOp_UnityEngine_ResourceManagement_IUpdateReceiver_Update_m27FD39CB9D11C53DD786CDB722DD249F7D35217B,
	UnloadSceneOp_Init_mEE09B761AF71428AA24E3C26D0E52670A362977D,
	UnloadSceneOp_Execute_m1DFB8028F12E302AFE304F67E5BAAB3820B0EC4D,
	UnloadSceneOp_InvokeWaitForCompletion_m1D581DA5E4DEC1B4D688D8D5027DB85AA10B17A1,
	UnloadSceneOp_UnloadSceneCompleted_m357E9D44463E1A1E3E87A29FFF300C11D3E862F9,
	UnloadSceneOp_UnloadSceneCompletedNoRelease_m45468FA61E0BB5E58457DEDEC3CC783C52F7689C,
	UnloadSceneOp_get_Progress_m42967C82BB0C649B805E107569933C78AAE18628,
	UnloadSceneOp__ctor_m051B216E7A8E5F7ED061FF3D0986D0BD9E9DF7A5,
	TextDataProvider_get_IgnoreFailures_m57066898F24CD1829FBD8BB87E0DEAA1CDC20C8B,
	TextDataProvider_set_IgnoreFailures_m5787E1C9DA529EF415D30ADDA2B74DBE78E6D825,
	TextDataProvider_Convert_mEBE0237D88E5C2D2F50D7441367D63906D0CDF75,
	TextDataProvider_Provide_m97C0755B7CB3C55D2D5CE99A26A0A650F887A69B,
	TextDataProvider__ctor_m7AC08BFDB0D65AFFF44638ADB897882432BF92DB,
	InternalOp_GetPercentComplete_m9ED8C8B9A92A008FC832003493795968212F040F,
	InternalOp_Start_mF752565834A11A0E10B24509297A70DDC5991B6A,
	InternalOp_WaitForCompletionHandler_m949B7900A9FF2FA537D5F559A495BA004400F3B6,
	InternalOp_RequestOperation_completed_m888C6797A4FF695796EAE9B6329A6B1EE896604B,
	InternalOp_CompleteOperation_m441DF029F5CAC4BE6A4A77EC65ADBBAF9FD02DD9,
	InternalOp_ConvertText_m55ECA959135C093079D59450942A86DB4B7D6037,
	InternalOp_SendWebRequest_m0B6B316F494B02FF73DF843F1F5AFEE0EB3A17A2,
	InternalOp__ctor_m761A51452CEFD8FAD7575FC1940F3A2E0709DB54,
	InternalOp_U3CSendWebRequestU3Eb__13_0_m72B7D00BDC91CA1F1FE36DA48CAB6026345CBF35,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceLocationBase_get_InternalId_mDD026BD02BC9C4BFE13018163D5C3333F87AC6CF,
	ResourceLocationBase_get_ProviderId_m3C0A8FFDD223BA3DB83C2BF36FA16A58F7D56603,
	ResourceLocationBase_get_Dependencies_mF557A83CF1A117CBCE78814D350BC09135BF4DF9,
	ResourceLocationBase_get_HasDependencies_mF968DBFE910DC0F2653AE1D3582149AF8D25DDD7,
	ResourceLocationBase_get_Data_m1FBDAD7E83CA4FE35DF61546CB4864F234760358,
	ResourceLocationBase_set_Data_mD899B389E585E1F781CD02CC2EEDFD38D80664BC,
	ResourceLocationBase_get_PrimaryKey_m2C276908F4E098BCED0F019A30872A7D2A02ACED,
	ResourceLocationBase_set_PrimaryKey_m989397E9A03B8CFDCBD151E087125BCC0C949145,
	ResourceLocationBase_get_DependencyHashCode_m1D0F112D82E12492981006CC695486AA85CF7A47,
	ResourceLocationBase_get_ResourceType_m1C6C8FA00487D034B8661EBB5323DEA49E76F631,
	ResourceLocationBase_Hash_mFD5291AA954FA36C6C833C3940F8A72A9C2AED77,
	ResourceLocationBase_ToString_m61190854FAFA7A73E0A9DE1647806A6491BE94A7,
	ResourceLocationBase__ctor_m8487509965168D66DD0079D6A096845204088A7F,
	ResourceLocationBase_ComputeDependencyHash_m9BB3035DD2ACAA3680109BD2DEB756F2DC1C1963,
	LocationWrapper__ctor_m0747F501E4CDB3A39111CB33A653D4D430F6CA3C,
	LocationWrapper_get_InternalId_m355AFE360DA988066AAB1BAA69019707B500F554,
	LocationWrapper_get_ProviderId_m2DBD62DD53ADC6312CB31A47F1ACE4D92573225A,
	LocationWrapper_get_Dependencies_m095B91D85514EB181D7F99F14595A9B9872DFF06,
	LocationWrapper_get_DependencyHashCode_mF48306CE95987FD5065C0488B0872C6A543DD15D,
	LocationWrapper_get_HasDependencies_m6EA0F7E06A3DFD4BFB3B230EA9A930879770484B,
	LocationWrapper_get_Data_m59FFC0A1E723AECBD0F2DC66757E9AA5703069CA,
	LocationWrapper_get_PrimaryKey_m862EE0B991542554FE9B04044AEBF5CE01B3A957,
	LocationWrapper_get_ResourceType_m628F5012DC99E805E6300C58E938F380C042F23C,
	LocationWrapper_Hash_mFAECBE3373E3FB222087669F861E4C1FF01496B4,
	DiagnosticEvent_get_Graph_m710E1F0680B4D39DE0264D295E28650421AEEF6D,
	DiagnosticEvent_get_ObjectId_mCA40B5EFDB9F507CAB17F2E19DA3F62D68B45537,
	DiagnosticEvent_get_DisplayName_mCBA5175616B9E1B45C12C51A8FBD01B88E5A78EE,
	DiagnosticEvent_get_Dependencies_m2751628387892A62E54CD3A32FD77DDD18626B27,
	DiagnosticEvent_get_Stream_mD35CADC32CA28797FEB25F744975712D808874D2,
	DiagnosticEvent_get_Frame_m80FE7279F8A049A54DF5D34EC80B0B8F4D33F9EB,
	DiagnosticEvent_get_Value_mA831F1827F784283AF76FBB76D768ED1C00B9410,
	DiagnosticEvent__ctor_m7010F47B7515B5EA548729B2F3E25EBFAEAEB217,
	DiagnosticEvent_Serialize_m02EC58094A8703C0C89430022D36A3EE3C304607,
	DiagnosticEvent_Deserialize_m80E559A631C1CADB8B5EE0B03A70D790C1459F3F,
	DiagnosticEventCollectorSingleton_get_PlayerConnectionGuid_mE4F64040FF6A340206986791B677A32D1AF40A06,
	DiagnosticEventCollectorSingleton_GetGameObjectName_m7454D83F6B10B46176467ED64EEB0B3E4FCB34A4,
	DiagnosticEventCollectorSingleton_RegisterEventHandler_m1C16B6445A83009C43667AD853E1F3FD6FCEFF15,
	DiagnosticEventCollectorSingleton_RegisterEventHandler_mCDFC17E30EE53B7FCC0CF28BE6AD60A15433F825,
	DiagnosticEventCollectorSingleton_UnregisterEventHandler_m173AC1759DCE404F090B33C5B5E23856C7A1F962,
	DiagnosticEventCollectorSingleton_PostEvent_m764F9DC4D7130EABF91C9370004FB1CEA5D4B9D1,
	DiagnosticEventCollectorSingleton_Awake_m788CBE28ACBF8886D836CF86D41B1C208173A71D,
	DiagnosticEventCollectorSingleton_Update_m1CCB4B5842B102C996FF57D9E2394891B15A6E09,
	DiagnosticEventCollectorSingleton__ctor_mC9A6C5013D561491DC56CF092E06C0C14FE44848,
	U3CU3Ec__cctor_m4C8DEF7665FD738A20E75923001C729501E85F8F,
	U3CU3Ec__ctor_mDC7AA0A80AADA03CB9CA3A18F80594A9A1D56E1E,
	U3CU3Ec_U3CRegisterEventHandlerU3Eb__8_0_mA276498C4E15EE1A6D3D7892FA6241AA212620D6,
	U3CU3Ec_U3CAwakeU3Eb__11_0_m3B098EDF909F7D5B2470B7C10A321BF9E4D5E485,
	DiagnosticEventCollector_get_PlayerConnectionGuid_mCDC64A92E85E48453A28C92A4EF9652C49E593C1,
	DiagnosticEventCollector_FindOrCreateGlobalInstance_m606D0B93253933F64A4F126E952F4AACAD95F1A6,
	DiagnosticEventCollector_RegisterEventHandler_mD8110A8AE08413AA61A28352CD219880663BE7C3,
	DiagnosticEventCollector_UnregisterEventHandler_m39277B914A0C123866E0FA4320B82CA7662B5725,
	DiagnosticEventCollector_PostEvent_m66DEEEEFF15701593B3B600159AE12085330B151,
	DiagnosticEventCollector__ctor_m31BB8D4B02E0F321D9D94F54D6DBBF07B4A91581,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AsyncOperationHandle_get_IsWaitingForCompletion_m5A486EC21B5DDD25E406142BCB2BDABC04E79FCE,
	AsyncOperationHandle_set_IsWaitingForCompletion_m6B8EBA1E5DC2F39DB3510F0CD0D82A9E1A1207D3,
	AsyncOperationHandle_get_LocationName_mC0E107351D4ADDDC32F0E9AC60390F1E2B244075,
	AsyncOperationHandle_set_LocationName_mF020B238870AC6E2F8BC2DC27B8ACCA0A7BCFDCF,
	AsyncOperationHandle__ctor_m5CAF8329748264C0D78320A562DAEAC3AE5869C8,
	AsyncOperationHandle__ctor_m625B3C164EFE603B9C7F4444294CAAE1752B8A0F,
	AsyncOperationHandle__ctor_m07D0BFA4D61DF19972B0CF2B007ACB96FF867FCB,
	AsyncOperationHandle__ctor_mB7A5E4B968464EF080A43D1759265AB39EF1D91C,
	AsyncOperationHandle_Acquire_m3F33353A41ED6BF37906DEA40CAA59E1C5755707,
	AsyncOperationHandle_add_Completed_mDC2AF6183B32C55FF890E226583D7E4A0FB35F09,
	AsyncOperationHandle_remove_Completed_mDF8507A627AF13A1381BE1BF4C26890047E86EA5,
	NULL,
	AsyncOperationHandle_Equals_m80AD6C46CC59FE4B03A7962CE925262C49CACE16,
	AsyncOperationHandle_get_DebugName_m0324680BFDD4A65C585B3EA9E3F244186552C0C6,
	AsyncOperationHandle_add_Destroyed_mAF6A1270B2A69BE84318FEB73D36A19437EE3008,
	AsyncOperationHandle_remove_Destroyed_m6AB38E753AF3537B4F8DB8DC8D85FD6DF068D55D,
	AsyncOperationHandle_GetDependencies_m35D1693217A8FCD56ECDAA6C933DF2A085AEE16E,
	AsyncOperationHandle_GetHashCode_mACAE683D3FAFE3F49C50C2E9F64D97C1F5D1624B,
	AsyncOperationHandle_get_InternalOp_m4EA78DBA5091741358AF08A77C4F74B7EB00463B,
	AsyncOperationHandle_get_IsDone_mAC819A5E254D87077EB8D9BE8F36AFC30EBA0C4B,
	AsyncOperationHandle_IsValid_m2D904B19EBBC50451B441161A762EE7703E5463C,
	AsyncOperationHandle_get_OperationException_mC27EB297729268618ADC7DDC3504B01CF3CA9695,
	AsyncOperationHandle_get_PercentComplete_m8020EBDC295B192EAA493D4E571E2501FE7AFA28,
	AsyncOperationHandle_GetDownloadStatus_m0C0F49BC92003068201C090DF8E7EB6B2E7167D4,
	AsyncOperationHandle_InternalGetDownloadStatus_m0D555CA8DBD18A501A708C7CB081E08CDB096D22,
	AsyncOperationHandle_get_ReferenceCount_m5D87AFB468776BBA67295B48381C78452566245D,
	AsyncOperationHandle_Release_mF869257E566A31A9F6906CD0CC44DF88228C625C,
	AsyncOperationHandle_get_Result_m28D82A4A3AB9E7635A56AB16B7AE9FB631B682FC,
	AsyncOperationHandle_get_Status_m518EE3BF10E74A8895F7E5699D5F1F2EAD5C0458,
	AsyncOperationHandle_get_Task_m2213154D1F81A13237EDBA72185106E6F772DB9F,
	AsyncOperationHandle_System_Collections_IEnumerator_get_Current_m0D8EC2DBF3380292BD75E7776B65EA38EDEBF1FD,
	AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m25C17DDC68A5908D26C0D82A0EB210209B607725,
	AsyncOperationHandle_System_Collections_IEnumerator_Reset_m4375BF3606971221ACE039AA1254743A85B27C80,
	AsyncOperationHandle_WaitForCompletion_mF2981A93A4D0597322B631C7BDBE81EBE96AB019,
	AsyncOperationHandle__cctor_m5E39F156ABC389DED65B841DE0445E74D5AD7D16,
	DownloadStatus_get_Percent_mE7E07B2D92779392BE939F2D10027391076FEF9B,
	GroupOperation__ctor_m8BA1E7986541D04D4627F113549B525B69A19DCD,
	GroupOperation_InvokeWaitForCompletion_mFEE866D25649060A973B097A2FB4C44935A461E6,
	GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_get_Key_m311D252A628C86052418A2DDD542964F90ED600F,
	GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_set_Key_mB676970E9ECFF566F3F8681D82CCA6EE2462F619,
	GroupOperation_GetDependentOps_m46BB9E741E15BEBC2AFC3FF3D650BCB4C90A2428,
	GroupOperation_GetDependencies_mC1D69E1844CED59D542EBCB9253ABDCF45DF8AF9,
	GroupOperation_ReleaseDependencies_mAC5DF932239F1CE59D2BCF41B346DE0106EB733D,
	GroupOperation_GetDownloadStatus_m569F2E77B438458FE8943DDFD487F9E9A665AFA6,
	GroupOperation_DependenciesAreUnchanged_m3F32CF0F69EA8FD043CD8897BE0321C94C02D67B,
	GroupOperation_get_DebugName_m9832D4D4CA8BDD84DB23334C9941A0E525A0A318,
	GroupOperation_Execute_mFCA40CC8EC876FEE011CFFEEA02A3CDBE0D0B61A,
	GroupOperation_CompleteIfDependenciesComplete_m7227335A4896D7F6DAD05E27EF1DAE7D6CAFC1FC,
	GroupOperation_Destroy_mF4C74B691E92BFC415938EBE09CFB456AD36FA73,
	GroupOperation_get_Progress_m06AE4CE1CC3A9487D832554002641CBA8E94DFDB,
	GroupOperation_Init_mEE131C7BCAE181CB8F78004C510B041A0A9EDEBD,
	GroupOperation_Init_m4727FD5451CF5CB82F643BECCBA9200247B20179,
	GroupOperation_OnOperationCompleted_m98CA3532152B8FE12B9E48B75361E9BF4FE5F5B5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
extern void DiagnosticEventContext_get_OperationHandle_m870675A1B170E90324CB13D4320F2DEF29D6A1BD_AdjustorThunk (void);
extern void DiagnosticEventContext_get_Type_m3C77CCA8778F7D55230D59FD0B6DEB37F910B15E_AdjustorThunk (void);
extern void DiagnosticEventContext_get_EventValue_m186FB7D8DFACBD40B9A7FC254685B544C5FC5448_AdjustorThunk (void);
extern void DiagnosticEventContext_get_Location_mC7B8C8151070981ACEF34A2110E16024587691B7_AdjustorThunk (void);
extern void DiagnosticEventContext_get_Context_mB145AF762ACC916F5CBCD03B392B36CB6BBD5D91_AdjustorThunk (void);
extern void DiagnosticEventContext_get_Error_m6758C6A5F0F1C59C8AFB8A5D5FEF7547C806D808_AdjustorThunk (void);
extern void DiagnosticEventContext__ctor_m46A56FA95388629125EDEE74C865060584274238_AdjustorThunk (void);
extern void DelegateInfo__ctor_mBB59764DCA627416685191C4BFFDF7E6B3F9EEB0_AdjustorThunk (void);
extern void DelegateInfo_get_InvocationTime_mD426371CDDBF73985C41649D481C1C8032731780_AdjustorThunk (void);
extern void DelegateInfo_set_InvocationTime_mDDF970305686BAF18D3644E6F8A0FBFAFF868A75_AdjustorThunk (void);
extern void DelegateInfo_ToString_m088DA64541A8A73664E1C101FE2726B1325AC5BC_AdjustorThunk (void);
extern void DelegateInfo_Invoke_m9010FEA078121055ACEBF2CD5049762D439DDD8F_AdjustorThunk (void);
extern void SerializedType_get_AssemblyName_m2DBDDEF51A9338BF298BCBBD2EE398D72E963635_AdjustorThunk (void);
extern void SerializedType_get_ClassName_m06DB26816F632CCAF380F818EA666D85C409D8B2_AdjustorThunk (void);
extern void SerializedType_ToString_m85C9B44C29867CD01DAA7CB5A365577818B4212A_AdjustorThunk (void);
extern void SerializedType_get_Value_m412D3B47698A74EFA8A3B29B4D102842714BD6B0_AdjustorThunk (void);
extern void SerializedType_set_Value_m0E1021AEE9B0E8B98800E79734E8B140032B9E54_AdjustorThunk (void);
extern void SerializedType_get_ValueChanged_m1B7B62A60583EE0D14418C391367985DBE6E7562_AdjustorThunk (void);
extern void SerializedType_set_ValueChanged_m7EB0C0A5F56B0594CC3578191F4E26CAC1D08D7D_AdjustorThunk (void);
extern void ObjectInitializationData_get_Id_m1455892EB3BA6CD3ED9F9A73F6401285E5486C65_AdjustorThunk (void);
extern void ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532_AdjustorThunk (void);
extern void ObjectInitializationData_get_Data_m026CC155470843EE675C47D80DDE91CFA7B05AA0_AdjustorThunk (void);
extern void ObjectInitializationData_ToString_m43DC1ED8748A5B962C707EA72E529243ABDA37D6_AdjustorThunk (void);
extern void ObjectInitializationData_GetAsyncInitHandle_m3050F492A6972087E9AA5371C7946CBE5F29AC42_AdjustorThunk (void);
extern void InstantiationParameters_get_Position_m9569A28B575846AD814790114A6CFAB8FC3E32DC_AdjustorThunk (void);
extern void InstantiationParameters_get_Rotation_m155362C3F3442CA4CC53C063FA8B4D09B753B012_AdjustorThunk (void);
extern void InstantiationParameters_get_Parent_mD6BC05225FF561D2123B1745B838D147D9B6A621_AdjustorThunk (void);
extern void InstantiationParameters_get_InstantiateInWorldPosition_mF6ACF2A60E6C89C55CC36D82220910EEA9CC65F4_AdjustorThunk (void);
extern void InstantiationParameters_get_SetPositionRotation_m3B5F3C4065F65ED2D4FD75B3B81D311B15EB70D1_AdjustorThunk (void);
extern void InstantiationParameters__ctor_m89D89058D329E9C651528936FD098282F1589225_AdjustorThunk (void);
extern void InstantiationParameters__ctor_mE86F10CB3D40AC7EA1883000E381AEAEA1E16A33_AdjustorThunk (void);
extern void ProvideHandle__ctor_mB9CCF5CA6F8DD98AB72B4659076385051038795C_AdjustorThunk (void);
extern void ProvideHandle_get_InternalOp_mDCF56F543329C2078BA6B8CE99C39B6EF3BEA41F_AdjustorThunk (void);
extern void ProvideHandle_get_ResourceManager_mD9EAFA3333B5A459E1AD6EA76E15558C531ACD76_AdjustorThunk (void);
extern void ProvideHandle_get_Type_mC36FCA0E5806E401F98BC3D30511DE5015A81EFF_AdjustorThunk (void);
extern void ProvideHandle_get_Location_m38F31BC3081090D72C7EFA9C6CDE73ED45B7D978_AdjustorThunk (void);
extern void ProvideHandle_get_DependencyCount_m56B0737197C7470C2A5FF8E8E2BBDA151E572BA4_AdjustorThunk (void);
extern void ProvideHandle_GetDependencies_mC65A5900B3EA9F0874C3D19BEFFC022DFC9AD8E8_AdjustorThunk (void);
extern void ProvideHandle_SetProgressCallback_m3B3F019C3AFF1A6B0F37B5B28F9A1121EBB73FD2_AdjustorThunk (void);
extern void ProvideHandle_SetDownloadProgressCallbacks_mE3370C49221CFB6CDED35F5FE373ADE5AD9270EF_AdjustorThunk (void);
extern void ProvideHandle_SetWaitForCompletionCallback_m96207FCBF579C5988A651BBECB844F0E37DB9B25_AdjustorThunk (void);
extern void SceneInstance_get_Scene_mFF895316B46CBB8126ABE0FE8F85985D1232CCA3_AdjustorThunk (void);
extern void SceneInstance_set_Scene_m8AD506582C450F4C678C7265ED0A94FFDA9179E5_AdjustorThunk (void);
extern void SceneInstance_Activate_m52A0E7A28EEA007FF2529340E980D798374E5B0E_AdjustorThunk (void);
extern void SceneInstance_ActivateAsync_m0F5677DAD4ED2EC1EB027E261B4EBF40418A3DC3_AdjustorThunk (void);
extern void SceneInstance_GetHashCode_m9B65494AC6ED027B091B9FF2DE58DEE6E46FFF0B_AdjustorThunk (void);
extern void SceneInstance_Equals_mFD3D668A674D5838BF6F9ED892C0452E48729A43_AdjustorThunk (void);
extern void DiagnosticEvent_get_Graph_m710E1F0680B4D39DE0264D295E28650421AEEF6D_AdjustorThunk (void);
extern void DiagnosticEvent_get_ObjectId_mCA40B5EFDB9F507CAB17F2E19DA3F62D68B45537_AdjustorThunk (void);
extern void DiagnosticEvent_get_DisplayName_mCBA5175616B9E1B45C12C51A8FBD01B88E5A78EE_AdjustorThunk (void);
extern void DiagnosticEvent_get_Dependencies_m2751628387892A62E54CD3A32FD77DDD18626B27_AdjustorThunk (void);
extern void DiagnosticEvent_get_Stream_mD35CADC32CA28797FEB25F744975712D808874D2_AdjustorThunk (void);
extern void DiagnosticEvent_get_Frame_m80FE7279F8A049A54DF5D34EC80B0B8F4D33F9EB_AdjustorThunk (void);
extern void DiagnosticEvent_get_Value_mA831F1827F784283AF76FBB76D768ED1C00B9410_AdjustorThunk (void);
extern void DiagnosticEvent__ctor_m7010F47B7515B5EA548729B2F3E25EBFAEAEB217_AdjustorThunk (void);
extern void DiagnosticEvent_Serialize_m02EC58094A8703C0C89430022D36A3EE3C304607_AdjustorThunk (void);
extern void AsyncOperationHandle_get_LocationName_mC0E107351D4ADDDC32F0E9AC60390F1E2B244075_AdjustorThunk (void);
extern void AsyncOperationHandle_set_LocationName_mF020B238870AC6E2F8BC2DC27B8ACCA0A7BCFDCF_AdjustorThunk (void);
extern void AsyncOperationHandle__ctor_m5CAF8329748264C0D78320A562DAEAC3AE5869C8_AdjustorThunk (void);
extern void AsyncOperationHandle__ctor_m625B3C164EFE603B9C7F4444294CAAE1752B8A0F_AdjustorThunk (void);
extern void AsyncOperationHandle__ctor_m07D0BFA4D61DF19972B0CF2B007ACB96FF867FCB_AdjustorThunk (void);
extern void AsyncOperationHandle__ctor_mB7A5E4B968464EF080A43D1759265AB39EF1D91C_AdjustorThunk (void);
extern void AsyncOperationHandle_Acquire_m3F33353A41ED6BF37906DEA40CAA59E1C5755707_AdjustorThunk (void);
extern void AsyncOperationHandle_add_Completed_mDC2AF6183B32C55FF890E226583D7E4A0FB35F09_AdjustorThunk (void);
extern void AsyncOperationHandle_remove_Completed_mDF8507A627AF13A1381BE1BF4C26890047E86EA5_AdjustorThunk (void);
extern void AsyncOperationHandle_Equals_m80AD6C46CC59FE4B03A7962CE925262C49CACE16_AdjustorThunk (void);
extern void AsyncOperationHandle_get_DebugName_m0324680BFDD4A65C585B3EA9E3F244186552C0C6_AdjustorThunk (void);
extern void AsyncOperationHandle_add_Destroyed_mAF6A1270B2A69BE84318FEB73D36A19437EE3008_AdjustorThunk (void);
extern void AsyncOperationHandle_remove_Destroyed_m6AB38E753AF3537B4F8DB8DC8D85FD6DF068D55D_AdjustorThunk (void);
extern void AsyncOperationHandle_GetDependencies_m35D1693217A8FCD56ECDAA6C933DF2A085AEE16E_AdjustorThunk (void);
extern void AsyncOperationHandle_GetHashCode_mACAE683D3FAFE3F49C50C2E9F64D97C1F5D1624B_AdjustorThunk (void);
extern void AsyncOperationHandle_get_InternalOp_m4EA78DBA5091741358AF08A77C4F74B7EB00463B_AdjustorThunk (void);
extern void AsyncOperationHandle_get_IsDone_mAC819A5E254D87077EB8D9BE8F36AFC30EBA0C4B_AdjustorThunk (void);
extern void AsyncOperationHandle_IsValid_m2D904B19EBBC50451B441161A762EE7703E5463C_AdjustorThunk (void);
extern void AsyncOperationHandle_get_OperationException_mC27EB297729268618ADC7DDC3504B01CF3CA9695_AdjustorThunk (void);
extern void AsyncOperationHandle_get_PercentComplete_m8020EBDC295B192EAA493D4E571E2501FE7AFA28_AdjustorThunk (void);
extern void AsyncOperationHandle_GetDownloadStatus_m0C0F49BC92003068201C090DF8E7EB6B2E7167D4_AdjustorThunk (void);
extern void AsyncOperationHandle_InternalGetDownloadStatus_m0D555CA8DBD18A501A708C7CB081E08CDB096D22_AdjustorThunk (void);
extern void AsyncOperationHandle_get_ReferenceCount_m5D87AFB468776BBA67295B48381C78452566245D_AdjustorThunk (void);
extern void AsyncOperationHandle_Release_mF869257E566A31A9F6906CD0CC44DF88228C625C_AdjustorThunk (void);
extern void AsyncOperationHandle_get_Result_m28D82A4A3AB9E7635A56AB16B7AE9FB631B682FC_AdjustorThunk (void);
extern void AsyncOperationHandle_get_Status_m518EE3BF10E74A8895F7E5699D5F1F2EAD5C0458_AdjustorThunk (void);
extern void AsyncOperationHandle_get_Task_m2213154D1F81A13237EDBA72185106E6F772DB9F_AdjustorThunk (void);
extern void AsyncOperationHandle_System_Collections_IEnumerator_get_Current_m0D8EC2DBF3380292BD75E7776B65EA38EDEBF1FD_AdjustorThunk (void);
extern void AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m25C17DDC68A5908D26C0D82A0EB210209B607725_AdjustorThunk (void);
extern void AsyncOperationHandle_System_Collections_IEnumerator_Reset_m4375BF3606971221ACE039AA1254743A85B27C80_AdjustorThunk (void);
extern void AsyncOperationHandle_WaitForCompletion_mF2981A93A4D0597322B631C7BDBE81EBE96AB019_AdjustorThunk (void);
extern void DownloadStatus_get_Percent_mE7E07B2D92779392BE939F2D10027391076FEF9B_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[88] = 
{
	{ 0x0600007D, DiagnosticEventContext_get_OperationHandle_m870675A1B170E90324CB13D4320F2DEF29D6A1BD_AdjustorThunk },
	{ 0x0600007E, DiagnosticEventContext_get_Type_m3C77CCA8778F7D55230D59FD0B6DEB37F910B15E_AdjustorThunk },
	{ 0x0600007F, DiagnosticEventContext_get_EventValue_m186FB7D8DFACBD40B9A7FC254685B544C5FC5448_AdjustorThunk },
	{ 0x06000080, DiagnosticEventContext_get_Location_mC7B8C8151070981ACEF34A2110E16024587691B7_AdjustorThunk },
	{ 0x06000081, DiagnosticEventContext_get_Context_mB145AF762ACC916F5CBCD03B392B36CB6BBD5D91_AdjustorThunk },
	{ 0x06000082, DiagnosticEventContext_get_Error_m6758C6A5F0F1C59C8AFB8A5D5FEF7547C806D808_AdjustorThunk },
	{ 0x06000083, DiagnosticEventContext__ctor_m46A56FA95388629125EDEE74C865060584274238_AdjustorThunk },
	{ 0x060000C7, DelegateInfo__ctor_mBB59764DCA627416685191C4BFFDF7E6B3F9EEB0_AdjustorThunk },
	{ 0x060000C8, DelegateInfo_get_InvocationTime_mD426371CDDBF73985C41649D481C1C8032731780_AdjustorThunk },
	{ 0x060000C9, DelegateInfo_set_InvocationTime_mDDF970305686BAF18D3644E6F8A0FBFAFF868A75_AdjustorThunk },
	{ 0x060000CA, DelegateInfo_ToString_m088DA64541A8A73664E1C101FE2726B1325AC5BC_AdjustorThunk },
	{ 0x060000CB, DelegateInfo_Invoke_m9010FEA078121055ACEBF2CD5049762D439DDD8F_AdjustorThunk },
	{ 0x060000F3, SerializedType_get_AssemblyName_m2DBDDEF51A9338BF298BCBBD2EE398D72E963635_AdjustorThunk },
	{ 0x060000F4, SerializedType_get_ClassName_m06DB26816F632CCAF380F818EA666D85C409D8B2_AdjustorThunk },
	{ 0x060000F5, SerializedType_ToString_m85C9B44C29867CD01DAA7CB5A365577818B4212A_AdjustorThunk },
	{ 0x060000F6, SerializedType_get_Value_m412D3B47698A74EFA8A3B29B4D102842714BD6B0_AdjustorThunk },
	{ 0x060000F7, SerializedType_set_Value_m0E1021AEE9B0E8B98800E79734E8B140032B9E54_AdjustorThunk },
	{ 0x060000F8, SerializedType_get_ValueChanged_m1B7B62A60583EE0D14418C391367985DBE6E7562_AdjustorThunk },
	{ 0x060000F9, SerializedType_set_ValueChanged_m7EB0C0A5F56B0594CC3578191F4E26CAC1D08D7D_AdjustorThunk },
	{ 0x060000FA, ObjectInitializationData_get_Id_m1455892EB3BA6CD3ED9F9A73F6401285E5486C65_AdjustorThunk },
	{ 0x060000FB, ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532_AdjustorThunk },
	{ 0x060000FC, ObjectInitializationData_get_Data_m026CC155470843EE675C47D80DDE91CFA7B05AA0_AdjustorThunk },
	{ 0x060000FD, ObjectInitializationData_ToString_m43DC1ED8748A5B962C707EA72E529243ABDA37D6_AdjustorThunk },
	{ 0x060000FF, ObjectInitializationData_GetAsyncInitHandle_m3050F492A6972087E9AA5371C7946CBE5F29AC42_AdjustorThunk },
	{ 0x0600015B, InstantiationParameters_get_Position_m9569A28B575846AD814790114A6CFAB8FC3E32DC_AdjustorThunk },
	{ 0x0600015C, InstantiationParameters_get_Rotation_m155362C3F3442CA4CC53C063FA8B4D09B753B012_AdjustorThunk },
	{ 0x0600015D, InstantiationParameters_get_Parent_mD6BC05225FF561D2123B1745B838D147D9B6A621_AdjustorThunk },
	{ 0x0600015E, InstantiationParameters_get_InstantiateInWorldPosition_mF6ACF2A60E6C89C55CC36D82220910EEA9CC65F4_AdjustorThunk },
	{ 0x0600015F, InstantiationParameters_get_SetPositionRotation_m3B5F3C4065F65ED2D4FD75B3B81D311B15EB70D1_AdjustorThunk },
	{ 0x06000160, InstantiationParameters__ctor_m89D89058D329E9C651528936FD098282F1589225_AdjustorThunk },
	{ 0x06000161, InstantiationParameters__ctor_mE86F10CB3D40AC7EA1883000E381AEAEA1E16A33_AdjustorThunk },
	{ 0x06000165, ProvideHandle__ctor_mB9CCF5CA6F8DD98AB72B4659076385051038795C_AdjustorThunk },
	{ 0x06000166, ProvideHandle_get_InternalOp_mDCF56F543329C2078BA6B8CE99C39B6EF3BEA41F_AdjustorThunk },
	{ 0x06000167, ProvideHandle_get_ResourceManager_mD9EAFA3333B5A459E1AD6EA76E15558C531ACD76_AdjustorThunk },
	{ 0x06000168, ProvideHandle_get_Type_mC36FCA0E5806E401F98BC3D30511DE5015A81EFF_AdjustorThunk },
	{ 0x06000169, ProvideHandle_get_Location_m38F31BC3081090D72C7EFA9C6CDE73ED45B7D978_AdjustorThunk },
	{ 0x0600016A, ProvideHandle_get_DependencyCount_m56B0737197C7470C2A5FF8E8E2BBDA151E572BA4_AdjustorThunk },
	{ 0x0600016C, ProvideHandle_GetDependencies_mC65A5900B3EA9F0874C3D19BEFFC022DFC9AD8E8_AdjustorThunk },
	{ 0x0600016D, ProvideHandle_SetProgressCallback_m3B3F019C3AFF1A6B0F37B5B28F9A1121EBB73FD2_AdjustorThunk },
	{ 0x0600016E, ProvideHandle_SetDownloadProgressCallbacks_mE3370C49221CFB6CDED35F5FE373ADE5AD9270EF_AdjustorThunk },
	{ 0x0600016F, ProvideHandle_SetWaitForCompletionCallback_m96207FCBF579C5988A651BBECB844F0E37DB9B25_AdjustorThunk },
	{ 0x06000177, SceneInstance_get_Scene_mFF895316B46CBB8126ABE0FE8F85985D1232CCA3_AdjustorThunk },
	{ 0x06000178, SceneInstance_set_Scene_m8AD506582C450F4C678C7265ED0A94FFDA9179E5_AdjustorThunk },
	{ 0x06000179, SceneInstance_Activate_m52A0E7A28EEA007FF2529340E980D798374E5B0E_AdjustorThunk },
	{ 0x0600017A, SceneInstance_ActivateAsync_m0F5677DAD4ED2EC1EB027E261B4EBF40418A3DC3_AdjustorThunk },
	{ 0x0600017B, SceneInstance_GetHashCode_m9B65494AC6ED027B091B9FF2DE58DEE6E46FFF0B_AdjustorThunk },
	{ 0x0600017C, SceneInstance_Equals_mFD3D668A674D5838BF6F9ED892C0452E48729A43_AdjustorThunk },
	{ 0x060001EA, DiagnosticEvent_get_Graph_m710E1F0680B4D39DE0264D295E28650421AEEF6D_AdjustorThunk },
	{ 0x060001EB, DiagnosticEvent_get_ObjectId_mCA40B5EFDB9F507CAB17F2E19DA3F62D68B45537_AdjustorThunk },
	{ 0x060001EC, DiagnosticEvent_get_DisplayName_mCBA5175616B9E1B45C12C51A8FBD01B88E5A78EE_AdjustorThunk },
	{ 0x060001ED, DiagnosticEvent_get_Dependencies_m2751628387892A62E54CD3A32FD77DDD18626B27_AdjustorThunk },
	{ 0x060001EE, DiagnosticEvent_get_Stream_mD35CADC32CA28797FEB25F744975712D808874D2_AdjustorThunk },
	{ 0x060001EF, DiagnosticEvent_get_Frame_m80FE7279F8A049A54DF5D34EC80B0B8F4D33F9EB_AdjustorThunk },
	{ 0x060001F0, DiagnosticEvent_get_Value_mA831F1827F784283AF76FBB76D768ED1C00B9410_AdjustorThunk },
	{ 0x060001F1, DiagnosticEvent__ctor_m7010F47B7515B5EA548729B2F3E25EBFAEAEB217_AdjustorThunk },
	{ 0x060001F2, DiagnosticEvent_Serialize_m02EC58094A8703C0C89430022D36A3EE3C304607_AdjustorThunk },
	{ 0x06000292, AsyncOperationHandle_get_LocationName_mC0E107351D4ADDDC32F0E9AC60390F1E2B244075_AdjustorThunk },
	{ 0x06000293, AsyncOperationHandle_set_LocationName_mF020B238870AC6E2F8BC2DC27B8ACCA0A7BCFDCF_AdjustorThunk },
	{ 0x06000294, AsyncOperationHandle__ctor_m5CAF8329748264C0D78320A562DAEAC3AE5869C8_AdjustorThunk },
	{ 0x06000295, AsyncOperationHandle__ctor_m625B3C164EFE603B9C7F4444294CAAE1752B8A0F_AdjustorThunk },
	{ 0x06000296, AsyncOperationHandle__ctor_m07D0BFA4D61DF19972B0CF2B007ACB96FF867FCB_AdjustorThunk },
	{ 0x06000297, AsyncOperationHandle__ctor_mB7A5E4B968464EF080A43D1759265AB39EF1D91C_AdjustorThunk },
	{ 0x06000298, AsyncOperationHandle_Acquire_m3F33353A41ED6BF37906DEA40CAA59E1C5755707_AdjustorThunk },
	{ 0x06000299, AsyncOperationHandle_add_Completed_mDC2AF6183B32C55FF890E226583D7E4A0FB35F09_AdjustorThunk },
	{ 0x0600029A, AsyncOperationHandle_remove_Completed_mDF8507A627AF13A1381BE1BF4C26890047E86EA5_AdjustorThunk },
	{ 0x0600029C, AsyncOperationHandle_Equals_m80AD6C46CC59FE4B03A7962CE925262C49CACE16_AdjustorThunk },
	{ 0x0600029D, AsyncOperationHandle_get_DebugName_m0324680BFDD4A65C585B3EA9E3F244186552C0C6_AdjustorThunk },
	{ 0x0600029E, AsyncOperationHandle_add_Destroyed_mAF6A1270B2A69BE84318FEB73D36A19437EE3008_AdjustorThunk },
	{ 0x0600029F, AsyncOperationHandle_remove_Destroyed_m6AB38E753AF3537B4F8DB8DC8D85FD6DF068D55D_AdjustorThunk },
	{ 0x060002A0, AsyncOperationHandle_GetDependencies_m35D1693217A8FCD56ECDAA6C933DF2A085AEE16E_AdjustorThunk },
	{ 0x060002A1, AsyncOperationHandle_GetHashCode_mACAE683D3FAFE3F49C50C2E9F64D97C1F5D1624B_AdjustorThunk },
	{ 0x060002A2, AsyncOperationHandle_get_InternalOp_m4EA78DBA5091741358AF08A77C4F74B7EB00463B_AdjustorThunk },
	{ 0x060002A3, AsyncOperationHandle_get_IsDone_mAC819A5E254D87077EB8D9BE8F36AFC30EBA0C4B_AdjustorThunk },
	{ 0x060002A4, AsyncOperationHandle_IsValid_m2D904B19EBBC50451B441161A762EE7703E5463C_AdjustorThunk },
	{ 0x060002A5, AsyncOperationHandle_get_OperationException_mC27EB297729268618ADC7DDC3504B01CF3CA9695_AdjustorThunk },
	{ 0x060002A6, AsyncOperationHandle_get_PercentComplete_m8020EBDC295B192EAA493D4E571E2501FE7AFA28_AdjustorThunk },
	{ 0x060002A7, AsyncOperationHandle_GetDownloadStatus_m0C0F49BC92003068201C090DF8E7EB6B2E7167D4_AdjustorThunk },
	{ 0x060002A8, AsyncOperationHandle_InternalGetDownloadStatus_m0D555CA8DBD18A501A708C7CB081E08CDB096D22_AdjustorThunk },
	{ 0x060002A9, AsyncOperationHandle_get_ReferenceCount_m5D87AFB468776BBA67295B48381C78452566245D_AdjustorThunk },
	{ 0x060002AA, AsyncOperationHandle_Release_mF869257E566A31A9F6906CD0CC44DF88228C625C_AdjustorThunk },
	{ 0x060002AB, AsyncOperationHandle_get_Result_m28D82A4A3AB9E7635A56AB16B7AE9FB631B682FC_AdjustorThunk },
	{ 0x060002AC, AsyncOperationHandle_get_Status_m518EE3BF10E74A8895F7E5699D5F1F2EAD5C0458_AdjustorThunk },
	{ 0x060002AD, AsyncOperationHandle_get_Task_m2213154D1F81A13237EDBA72185106E6F772DB9F_AdjustorThunk },
	{ 0x060002AE, AsyncOperationHandle_System_Collections_IEnumerator_get_Current_m0D8EC2DBF3380292BD75E7776B65EA38EDEBF1FD_AdjustorThunk },
	{ 0x060002AF, AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m25C17DDC68A5908D26C0D82A0EB210209B607725_AdjustorThunk },
	{ 0x060002B0, AsyncOperationHandle_System_Collections_IEnumerator_Reset_m4375BF3606971221ACE039AA1254743A85B27C80_AdjustorThunk },
	{ 0x060002B1, AsyncOperationHandle_WaitForCompletion_mF2981A93A4D0597322B631C7BDBE81EBE96AB019_AdjustorThunk },
	{ 0x060002B3, DownloadStatus_get_Percent_mE7E07B2D92779392BE939F2D10027391076FEF9B_AdjustorThunk },
};
static const int32_t s_InvokerIndices[744] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	14,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	154,
	14,
	26,
	28,
	14,
	26,
	10,
	10,
	26,
	26,
	14,
	26,
	14,
	14,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	2441,
	105,
	28,
	41,
	2442,
	-1,
	-1,
	2443,
	26,
	26,
	26,
	-1,
	27,
	9,
	9,
	10,
	-1,
	-1,
	-1,
	2444,
	2444,
	28,
	-1,
	-1,
	2445,
	2446,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2447,
	2448,
	2449,
	2450,
	23,
	459,
	337,
	23,
	3,
	26,
	2451,
	10,
	10,
	14,
	14,
	14,
	2452,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2453,
	2454,
	26,
	14,
	1617,
	23,
	731,
	89,
	23,
	23,
	-1,
	-1,
	-1,
	337,
	89,
	26,
	26,
	164,
	0,
	154,
	3,
	23,
	26,
	27,
	111,
	14,
	14,
	26,
	26,
	23,
	26,
	27,
	111,
	14,
	14,
	27,
	14,
	197,
	14,
	446,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	503,
	3,
	23,
	2236,
	1916,
	49,
	1589,
	23,
	337,
	23,
	23,
	1916,
	731,
	337,
	14,
	23,
	27,
	10,
	9,
	9,
	9,
	130,
	10,
	9,
	9,
	9,
	26,
	10,
	9,
	9,
	9,
	135,
	135,
	90,
	2455,
	14,
	2456,
	58,
	62,
	58,
	62,
	23,
	341,
	14,
	26,
	58,
	62,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	14,
	14,
	14,
	26,
	89,
	31,
	14,
	2457,
	14,
	14,
	-1,
	2458,
	366,
	114,
	0,
	114,
	49,
	1,
	-1,
	1,
	-1,
	-1,
	216,
	23,
	26,
	14,
	14,
	26,
	172,
	14,
	14,
	26,
	14,
	14,
	26,
	10,
	32,
	10,
	32,
	89,
	31,
	10,
	32,
	10,
	32,
	14,
	26,
	10,
	32,
	172,
	200,
	89,
	31,
	89,
	31,
	89,
	31,
	2459,
	23,
	89,
	172,
	28,
	28,
	14,
	731,
	2460,
	14,
	2461,
	89,
	27,
	2462,
	44,
	23,
	26,
	337,
	26,
	26,
	26,
	23,
	23,
	26,
	26,
	2461,
	28,
	27,
	23,
	2461,
	23,
	2461,
	23,
	0,
	2461,
	23,
	89,
	26,
	26,
	26,
	26,
	26,
	23,
	731,
	23,
	26,
	1433,
	1635,
	14,
	89,
	89,
	459,
	2463,
	-1,
	2464,
	27,
	27,
	14,
	14,
	14,
	14,
	10,
	-1,
	26,
	26,
	26,
	26,
	-1,
	14,
	28,
	90,
	2461,
	27,
	10,
	1617,
	2450,
	23,
	14,
	10,
	9,
	2447,
	2448,
	2465,
	2466,
	2464,
	27,
	23,
	105,
	23,
	2461,
	27,
	23,
	2461,
	26,
	731,
	23,
	14,
	90,
	90,
	14,
	27,
	28,
	2461,
	2455,
	10,
	23,
	26,
	89,
	23,
	23,
	23,
	89,
	14,
	89,
	31,
	10,
	32,
	23,
	2447,
	2448,
	2465,
	23,
	26,
	2454,
	2467,
	89,
	26,
	14,
	23,
	2468,
	2469,
	23,
	731,
	337,
	2470,
	23,
	89,
	26,
	26,
	731,
	23,
	89,
	31,
	105,
	2461,
	23,
	731,
	2471,
	89,
	26,
	27,
	28,
	26,
	23,
	26,
	2459,
	14,
	14,
	14,
	112,
	10,
	89,
	14,
	14,
	14,
	14,
	14,
	14,
	89,
	14,
	26,
	14,
	26,
	10,
	14,
	112,
	14,
	829,
	23,
	26,
	14,
	14,
	14,
	10,
	89,
	14,
	14,
	14,
	112,
	14,
	10,
	14,
	14,
	10,
	10,
	10,
	2472,
	14,
	2473,
	433,
	14,
	2320,
	26,
	26,
	2474,
	23,
	23,
	23,
	3,
	23,
	2475,
	2474,
	433,
	4,
	2320,
	26,
	2474,
	23,
	14,
	26,
	14,
	14,
	10,
	14,
	23,
	23,
	10,
	731,
	2454,
	10,
	14,
	89,
	26,
	26,
	89,
	26,
	26,
	26,
	26,
	23,
	14,
	2476,
	2451,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	49,
	859,
	14,
	26,
	26,
	130,
	27,
	107,
	2451,
	26,
	26,
	-1,
	2477,
	14,
	26,
	26,
	26,
	10,
	14,
	89,
	89,
	14,
	731,
	2460,
	2454,
	10,
	23,
	14,
	10,
	14,
	14,
	89,
	23,
	14,
	3,
	731,
	23,
	89,
	14,
	26,
	14,
	26,
	23,
	2454,
	9,
	14,
	23,
	23,
	23,
	731,
	814,
	130,
	2444,
	2478,
	2479,
	10,
	14,
	10,
	26,
	-1,
	26,
	-1,
	14,
	26,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[38] = 
{
	{ 0x02000002, { 0, 20 } },
	{ 0x02000003, { 20, 23 } },
	{ 0x02000005, { 43, 33 } },
	{ 0x02000006, { 76, 22 } },
	{ 0x0200000B, { 145, 7 } },
	{ 0x0200000D, { 152, 8 } },
	{ 0x02000016, { 160, 10 } },
	{ 0x02000024, { 170, 9 } },
	{ 0x02000025, { 179, 5 } },
	{ 0x02000055, { 197, 63 } },
	{ 0x02000056, { 260, 2 } },
	{ 0x02000057, { 262, 2 } },
	{ 0x02000058, { 264, 26 } },
	{ 0x0200005F, { 292, 15 } },
	{ 0x06000058, { 98, 2 } },
	{ 0x06000059, { 100, 2 } },
	{ 0x0600005E, { 102, 1 } },
	{ 0x06000063, { 103, 1 } },
	{ 0x06000064, { 104, 1 } },
	{ 0x06000065, { 105, 4 } },
	{ 0x06000069, { 109, 3 } },
	{ 0x0600006A, { 112, 3 } },
	{ 0x0600006D, { 115, 1 } },
	{ 0x0600006E, { 116, 9 } },
	{ 0x0600006F, { 125, 6 } },
	{ 0x06000070, { 131, 4 } },
	{ 0x06000071, { 135, 6 } },
	{ 0x06000072, { 141, 4 } },
	{ 0x060000FE, { 184, 1 } },
	{ 0x06000106, { 185, 2 } },
	{ 0x06000108, { 187, 2 } },
	{ 0x06000109, { 189, 2 } },
	{ 0x06000162, { 191, 4 } },
	{ 0x0600016B, { 195, 1 } },
	{ 0x06000170, { 196, 1 } },
	{ 0x0600029B, { 290, 2 } },
	{ 0x060002E0, { 307, 1 } },
	{ 0x060002E2, { 308, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[312] = 
{
	{ (Il2CppRGCTXDataType)3, 26351 },
	{ (Il2CppRGCTXDataType)3, 26352 },
	{ (Il2CppRGCTXDataType)2, 35035 },
	{ (Il2CppRGCTXDataType)3, 26353 },
	{ (Il2CppRGCTXDataType)3, 26354 },
	{ (Il2CppRGCTXDataType)3, 26355 },
	{ (Il2CppRGCTXDataType)3, 26356 },
	{ (Il2CppRGCTXDataType)3, 26357 },
	{ (Il2CppRGCTXDataType)3, 26358 },
	{ (Il2CppRGCTXDataType)3, 26359 },
	{ (Il2CppRGCTXDataType)3, 26360 },
	{ (Il2CppRGCTXDataType)3, 26361 },
	{ (Il2CppRGCTXDataType)3, 26362 },
	{ (Il2CppRGCTXDataType)2, 32533 },
	{ (Il2CppRGCTXDataType)3, 26363 },
	{ (Il2CppRGCTXDataType)3, 26364 },
	{ (Il2CppRGCTXDataType)2, 32537 },
	{ (Il2CppRGCTXDataType)3, 26365 },
	{ (Il2CppRGCTXDataType)2, 32538 },
	{ (Il2CppRGCTXDataType)3, 26366 },
	{ (Il2CppRGCTXDataType)2, 32544 },
	{ (Il2CppRGCTXDataType)3, 26367 },
	{ (Il2CppRGCTXDataType)3, 26368 },
	{ (Il2CppRGCTXDataType)3, 26369 },
	{ (Il2CppRGCTXDataType)3, 26370 },
	{ (Il2CppRGCTXDataType)3, 26371 },
	{ (Il2CppRGCTXDataType)3, 26372 },
	{ (Il2CppRGCTXDataType)3, 26373 },
	{ (Il2CppRGCTXDataType)2, 34529 },
	{ (Il2CppRGCTXDataType)3, 26374 },
	{ (Il2CppRGCTXDataType)3, 26375 },
	{ (Il2CppRGCTXDataType)3, 26376 },
	{ (Il2CppRGCTXDataType)3, 26377 },
	{ (Il2CppRGCTXDataType)2, 35036 },
	{ (Il2CppRGCTXDataType)3, 26378 },
	{ (Il2CppRGCTXDataType)3, 26379 },
	{ (Il2CppRGCTXDataType)3, 26380 },
	{ (Il2CppRGCTXDataType)3, 26381 },
	{ (Il2CppRGCTXDataType)3, 26382 },
	{ (Il2CppRGCTXDataType)3, 26383 },
	{ (Il2CppRGCTXDataType)3, 26384 },
	{ (Il2CppRGCTXDataType)2, 35037 },
	{ (Il2CppRGCTXDataType)3, 26385 },
	{ (Il2CppRGCTXDataType)3, 26386 },
	{ (Il2CppRGCTXDataType)2, 32557 },
	{ (Il2CppRGCTXDataType)3, 26387 },
	{ (Il2CppRGCTXDataType)2, 35038 },
	{ (Il2CppRGCTXDataType)3, 26388 },
	{ (Il2CppRGCTXDataType)1, 32558 },
	{ (Il2CppRGCTXDataType)1, 32560 },
	{ (Il2CppRGCTXDataType)3, 26389 },
	{ (Il2CppRGCTXDataType)3, 26390 },
	{ (Il2CppRGCTXDataType)3, 26391 },
	{ (Il2CppRGCTXDataType)2, 32559 },
	{ (Il2CppRGCTXDataType)3, 26392 },
	{ (Il2CppRGCTXDataType)3, 26393 },
	{ (Il2CppRGCTXDataType)3, 26394 },
	{ (Il2CppRGCTXDataType)3, 26395 },
	{ (Il2CppRGCTXDataType)3, 26396 },
	{ (Il2CppRGCTXDataType)3, 26397 },
	{ (Il2CppRGCTXDataType)3, 26398 },
	{ (Il2CppRGCTXDataType)3, 26399 },
	{ (Il2CppRGCTXDataType)3, 26400 },
	{ (Il2CppRGCTXDataType)3, 26401 },
	{ (Il2CppRGCTXDataType)3, 26402 },
	{ (Il2CppRGCTXDataType)3, 26403 },
	{ (Il2CppRGCTXDataType)3, 26404 },
	{ (Il2CppRGCTXDataType)3, 26405 },
	{ (Il2CppRGCTXDataType)3, 26406 },
	{ (Il2CppRGCTXDataType)3, 26407 },
	{ (Il2CppRGCTXDataType)3, 26408 },
	{ (Il2CppRGCTXDataType)3, 26409 },
	{ (Il2CppRGCTXDataType)3, 26410 },
	{ (Il2CppRGCTXDataType)3, 26411 },
	{ (Il2CppRGCTXDataType)3, 26412 },
	{ (Il2CppRGCTXDataType)3, 26413 },
	{ (Il2CppRGCTXDataType)3, 26414 },
	{ (Il2CppRGCTXDataType)2, 32570 },
	{ (Il2CppRGCTXDataType)3, 26415 },
	{ (Il2CppRGCTXDataType)2, 35039 },
	{ (Il2CppRGCTXDataType)3, 26416 },
	{ (Il2CppRGCTXDataType)1, 32571 },
	{ (Il2CppRGCTXDataType)3, 26417 },
	{ (Il2CppRGCTXDataType)3, 26418 },
	{ (Il2CppRGCTXDataType)3, 26419 },
	{ (Il2CppRGCTXDataType)3, 26420 },
	{ (Il2CppRGCTXDataType)3, 26421 },
	{ (Il2CppRGCTXDataType)3, 26422 },
	{ (Il2CppRGCTXDataType)3, 26423 },
	{ (Il2CppRGCTXDataType)3, 26424 },
	{ (Il2CppRGCTXDataType)3, 26425 },
	{ (Il2CppRGCTXDataType)3, 26426 },
	{ (Il2CppRGCTXDataType)3, 26427 },
	{ (Il2CppRGCTXDataType)3, 26428 },
	{ (Il2CppRGCTXDataType)3, 26429 },
	{ (Il2CppRGCTXDataType)3, 26430 },
	{ (Il2CppRGCTXDataType)3, 26431 },
	{ (Il2CppRGCTXDataType)3, 26432 },
	{ (Il2CppRGCTXDataType)1, 32584 },
	{ (Il2CppRGCTXDataType)3, 26433 },
	{ (Il2CppRGCTXDataType)3, 26434 },
	{ (Il2CppRGCTXDataType)3, 26435 },
	{ (Il2CppRGCTXDataType)2, 32588 },
	{ (Il2CppRGCTXDataType)3, 26436 },
	{ (Il2CppRGCTXDataType)3, 26437 },
	{ (Il2CppRGCTXDataType)1, 35040 },
	{ (Il2CppRGCTXDataType)3, 26438 },
	{ (Il2CppRGCTXDataType)3, 26439 },
	{ (Il2CppRGCTXDataType)3, 26440 },
	{ (Il2CppRGCTXDataType)3, 26441 },
	{ (Il2CppRGCTXDataType)3, 26442 },
	{ (Il2CppRGCTXDataType)2, 35042 },
	{ (Il2CppRGCTXDataType)3, 26443 },
	{ (Il2CppRGCTXDataType)3, 26444 },
	{ (Il2CppRGCTXDataType)2, 35044 },
	{ (Il2CppRGCTXDataType)3, 26445 },
	{ (Il2CppRGCTXDataType)2, 35045 },
	{ (Il2CppRGCTXDataType)3, 26446 },
	{ (Il2CppRGCTXDataType)3, 26447 },
	{ (Il2CppRGCTXDataType)3, 26448 },
	{ (Il2CppRGCTXDataType)1, 32601 },
	{ (Il2CppRGCTXDataType)3, 26449 },
	{ (Il2CppRGCTXDataType)2, 35046 },
	{ (Il2CppRGCTXDataType)3, 26450 },
	{ (Il2CppRGCTXDataType)3, 26451 },
	{ (Il2CppRGCTXDataType)1, 35047 },
	{ (Il2CppRGCTXDataType)3, 26452 },
	{ (Il2CppRGCTXDataType)3, 26453 },
	{ (Il2CppRGCTXDataType)3, 26454 },
	{ (Il2CppRGCTXDataType)2, 32605 },
	{ (Il2CppRGCTXDataType)3, 26455 },
	{ (Il2CppRGCTXDataType)2, 35048 },
	{ (Il2CppRGCTXDataType)3, 26456 },
	{ (Il2CppRGCTXDataType)3, 26457 },
	{ (Il2CppRGCTXDataType)3, 26458 },
	{ (Il2CppRGCTXDataType)1, 35049 },
	{ (Il2CppRGCTXDataType)3, 26459 },
	{ (Il2CppRGCTXDataType)3, 26460 },
	{ (Il2CppRGCTXDataType)3, 26461 },
	{ (Il2CppRGCTXDataType)2, 32613 },
	{ (Il2CppRGCTXDataType)3, 26462 },
	{ (Il2CppRGCTXDataType)2, 35050 },
	{ (Il2CppRGCTXDataType)3, 26463 },
	{ (Il2CppRGCTXDataType)3, 26464 },
	{ (Il2CppRGCTXDataType)3, 26465 },
	{ (Il2CppRGCTXDataType)3, 26466 },
	{ (Il2CppRGCTXDataType)2, 32650 },
	{ (Il2CppRGCTXDataType)3, 26467 },
	{ (Il2CppRGCTXDataType)3, 26468 },
	{ (Il2CppRGCTXDataType)3, 26469 },
	{ (Il2CppRGCTXDataType)3, 26470 },
	{ (Il2CppRGCTXDataType)3, 26471 },
	{ (Il2CppRGCTXDataType)2, 32661 },
	{ (Il2CppRGCTXDataType)3, 26472 },
	{ (Il2CppRGCTXDataType)2, 35051 },
	{ (Il2CppRGCTXDataType)3, 26473 },
	{ (Il2CppRGCTXDataType)3, 26474 },
	{ (Il2CppRGCTXDataType)3, 26475 },
	{ (Il2CppRGCTXDataType)3, 26476 },
	{ (Il2CppRGCTXDataType)3, 26477 },
	{ (Il2CppRGCTXDataType)2, 32693 },
	{ (Il2CppRGCTXDataType)2, 32691 },
	{ (Il2CppRGCTXDataType)3, 26478 },
	{ (Il2CppRGCTXDataType)3, 26479 },
	{ (Il2CppRGCTXDataType)3, 26480 },
	{ (Il2CppRGCTXDataType)1, 32691 },
	{ (Il2CppRGCTXDataType)3, 26481 },
	{ (Il2CppRGCTXDataType)3, 26482 },
	{ (Il2CppRGCTXDataType)3, 26483 },
	{ (Il2CppRGCTXDataType)3, 26484 },
	{ (Il2CppRGCTXDataType)3, 26485 },
	{ (Il2CppRGCTXDataType)3, 26486 },
	{ (Il2CppRGCTXDataType)3, 26487 },
	{ (Il2CppRGCTXDataType)2, 32727 },
	{ (Il2CppRGCTXDataType)3, 26488 },
	{ (Il2CppRGCTXDataType)2, 35052 },
	{ (Il2CppRGCTXDataType)3, 26489 },
	{ (Il2CppRGCTXDataType)3, 26490 },
	{ (Il2CppRGCTXDataType)3, 26491 },
	{ (Il2CppRGCTXDataType)2, 35053 },
	{ (Il2CppRGCTXDataType)2, 35054 },
	{ (Il2CppRGCTXDataType)3, 26492 },
	{ (Il2CppRGCTXDataType)3, 26493 },
	{ (Il2CppRGCTXDataType)3, 26494 },
	{ (Il2CppRGCTXDataType)2, 32736 },
	{ (Il2CppRGCTXDataType)1, 32740 },
	{ (Il2CppRGCTXDataType)2, 32740 },
	{ (Il2CppRGCTXDataType)1, 32741 },
	{ (Il2CppRGCTXDataType)2, 32741 },
	{ (Il2CppRGCTXDataType)1, 35055 },
	{ (Il2CppRGCTXDataType)1, 35056 },
	{ (Il2CppRGCTXDataType)3, 26495 },
	{ (Il2CppRGCTXDataType)3, 26496 },
	{ (Il2CppRGCTXDataType)3, 26497 },
	{ (Il2CppRGCTXDataType)3, 26498 },
	{ (Il2CppRGCTXDataType)3, 26499 },
	{ (Il2CppRGCTXDataType)3, 26500 },
	{ (Il2CppRGCTXDataType)3, 26501 },
	{ (Il2CppRGCTXDataType)3, 26502 },
	{ (Il2CppRGCTXDataType)3, 26503 },
	{ (Il2CppRGCTXDataType)3, 26504 },
	{ (Il2CppRGCTXDataType)2, 32862 },
	{ (Il2CppRGCTXDataType)3, 26505 },
	{ (Il2CppRGCTXDataType)3, 26506 },
	{ (Il2CppRGCTXDataType)2, 32862 },
	{ (Il2CppRGCTXDataType)3, 26507 },
	{ (Il2CppRGCTXDataType)3, 26508 },
	{ (Il2CppRGCTXDataType)2, 35057 },
	{ (Il2CppRGCTXDataType)3, 26509 },
	{ (Il2CppRGCTXDataType)3, 26510 },
	{ (Il2CppRGCTXDataType)3, 26511 },
	{ (Il2CppRGCTXDataType)3, 26512 },
	{ (Il2CppRGCTXDataType)3, 26513 },
	{ (Il2CppRGCTXDataType)3, 26514 },
	{ (Il2CppRGCTXDataType)2, 32859 },
	{ (Il2CppRGCTXDataType)3, 26515 },
	{ (Il2CppRGCTXDataType)2, 35058 },
	{ (Il2CppRGCTXDataType)3, 26516 },
	{ (Il2CppRGCTXDataType)3, 26517 },
	{ (Il2CppRGCTXDataType)3, 26518 },
	{ (Il2CppRGCTXDataType)2, 35059 },
	{ (Il2CppRGCTXDataType)3, 26519 },
	{ (Il2CppRGCTXDataType)3, 26520 },
	{ (Il2CppRGCTXDataType)2, 32861 },
	{ (Il2CppRGCTXDataType)3, 26521 },
	{ (Il2CppRGCTXDataType)3, 26522 },
	{ (Il2CppRGCTXDataType)2, 35060 },
	{ (Il2CppRGCTXDataType)3, 26523 },
	{ (Il2CppRGCTXDataType)3, 26524 },
	{ (Il2CppRGCTXDataType)3, 26525 },
	{ (Il2CppRGCTXDataType)3, 26526 },
	{ (Il2CppRGCTXDataType)3, 26527 },
	{ (Il2CppRGCTXDataType)3, 26528 },
	{ (Il2CppRGCTXDataType)3, 26529 },
	{ (Il2CppRGCTXDataType)3, 26530 },
	{ (Il2CppRGCTXDataType)3, 26531 },
	{ (Il2CppRGCTXDataType)3, 26532 },
	{ (Il2CppRGCTXDataType)3, 26533 },
	{ (Il2CppRGCTXDataType)3, 26534 },
	{ (Il2CppRGCTXDataType)3, 26535 },
	{ (Il2CppRGCTXDataType)3, 26536 },
	{ (Il2CppRGCTXDataType)3, 26537 },
	{ (Il2CppRGCTXDataType)3, 26538 },
	{ (Il2CppRGCTXDataType)3, 26539 },
	{ (Il2CppRGCTXDataType)3, 26540 },
	{ (Il2CppRGCTXDataType)3, 26541 },
	{ (Il2CppRGCTXDataType)3, 26542 },
	{ (Il2CppRGCTXDataType)3, 26543 },
	{ (Il2CppRGCTXDataType)3, 26544 },
	{ (Il2CppRGCTXDataType)3, 26545 },
	{ (Il2CppRGCTXDataType)3, 26546 },
	{ (Il2CppRGCTXDataType)3, 26547 },
	{ (Il2CppRGCTXDataType)3, 26548 },
	{ (Il2CppRGCTXDataType)3, 26549 },
	{ (Il2CppRGCTXDataType)3, 26550 },
	{ (Il2CppRGCTXDataType)3, 26551 },
	{ (Il2CppRGCTXDataType)1, 32859 },
	{ (Il2CppRGCTXDataType)3, 26552 },
	{ (Il2CppRGCTXDataType)3, 26553 },
	{ (Il2CppRGCTXDataType)3, 26554 },
	{ (Il2CppRGCTXDataType)3, 26555 },
	{ (Il2CppRGCTXDataType)2, 32875 },
	{ (Il2CppRGCTXDataType)3, 26556 },
	{ (Il2CppRGCTXDataType)2, 32880 },
	{ (Il2CppRGCTXDataType)3, 26557 },
	{ (Il2CppRGCTXDataType)3, 26558 },
	{ (Il2CppRGCTXDataType)3, 26559 },
	{ (Il2CppRGCTXDataType)3, 26560 },
	{ (Il2CppRGCTXDataType)3, 26561 },
	{ (Il2CppRGCTXDataType)2, 32886 },
	{ (Il2CppRGCTXDataType)3, 26562 },
	{ (Il2CppRGCTXDataType)3, 26563 },
	{ (Il2CppRGCTXDataType)3, 26564 },
	{ (Il2CppRGCTXDataType)3, 26565 },
	{ (Il2CppRGCTXDataType)3, 26566 },
	{ (Il2CppRGCTXDataType)3, 26567 },
	{ (Il2CppRGCTXDataType)3, 26568 },
	{ (Il2CppRGCTXDataType)3, 26569 },
	{ (Il2CppRGCTXDataType)3, 26570 },
	{ (Il2CppRGCTXDataType)3, 26571 },
	{ (Il2CppRGCTXDataType)3, 26572 },
	{ (Il2CppRGCTXDataType)3, 26573 },
	{ (Il2CppRGCTXDataType)3, 26574 },
	{ (Il2CppRGCTXDataType)3, 26575 },
	{ (Il2CppRGCTXDataType)3, 26576 },
	{ (Il2CppRGCTXDataType)3, 26577 },
	{ (Il2CppRGCTXDataType)3, 26578 },
	{ (Il2CppRGCTXDataType)3, 26579 },
	{ (Il2CppRGCTXDataType)3, 26580 },
	{ (Il2CppRGCTXDataType)2, 32885 },
	{ (Il2CppRGCTXDataType)2, 32892 },
	{ (Il2CppRGCTXDataType)3, 26581 },
	{ (Il2CppRGCTXDataType)3, 26582 },
	{ (Il2CppRGCTXDataType)3, 26583 },
	{ (Il2CppRGCTXDataType)3, 26584 },
	{ (Il2CppRGCTXDataType)3, 26585 },
	{ (Il2CppRGCTXDataType)2, 32908 },
	{ (Il2CppRGCTXDataType)1, 32909 },
	{ (Il2CppRGCTXDataType)3, 26586 },
	{ (Il2CppRGCTXDataType)2, 32909 },
	{ (Il2CppRGCTXDataType)3, 26587 },
	{ (Il2CppRGCTXDataType)3, 26588 },
	{ (Il2CppRGCTXDataType)3, 26589 },
	{ (Il2CppRGCTXDataType)3, 26590 },
	{ (Il2CppRGCTXDataType)3, 26591 },
	{ (Il2CppRGCTXDataType)3, 26592 },
	{ (Il2CppRGCTXDataType)3, 26593 },
	{ (Il2CppRGCTXDataType)2, 32910 },
	{ (Il2CppRGCTXDataType)2, 35061 },
	{ (Il2CppRGCTXDataType)3, 26594 },
	{ (Il2CppRGCTXDataType)2, 32911 },
	{ (Il2CppRGCTXDataType)1, 32911 },
};
extern const Il2CppCodeGenModule g_Unity_ResourceManagerCodeGenModule;
const Il2CppCodeGenModule g_Unity_ResourceManagerCodeGenModule = 
{
	"Unity.ResourceManager.dll",
	744,
	s_methodPointers,
	88,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	38,
	s_rgctxIndices,
	312,
	s_rgctxValues,
	NULL,
};
