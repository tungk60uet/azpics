﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Sirenix.Serialization.CustomLogger::.ctor(System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.Exception>)
extern void CustomLogger__ctor_m11132C8664248CABA7C74A46B1C667FC52E8824E (void);
// 0x00000002 System.Void Sirenix.Serialization.CustomLogger::LogWarning(System.String)
extern void CustomLogger_LogWarning_m21AC3378D4B326E66A9ED47E1E631EE7A7DC4B91 (void);
// 0x00000003 System.Void Sirenix.Serialization.CustomLogger::LogError(System.String)
extern void CustomLogger_LogError_mF61A72C224F94FFB2EFA1B6778D155124C929EEA (void);
// 0x00000004 System.Void Sirenix.Serialization.CustomLogger::LogException(System.Exception)
extern void CustomLogger_LogException_m85361A858858BBFA176945313B74A1628B6FE3D8 (void);
// 0x00000005 Sirenix.Serialization.ILogger Sirenix.Serialization.DefaultLoggers::get_DefaultLogger()
extern void DefaultLoggers_get_DefaultLogger_mD4758D541F9D0CDA080CAF6CE1EE85925E2DA8D7 (void);
// 0x00000006 Sirenix.Serialization.ILogger Sirenix.Serialization.DefaultLoggers::get_UnityLogger()
extern void DefaultLoggers_get_UnityLogger_mCA9BD734E49EC8F7E550296F95775266E30B7E54 (void);
// 0x00000007 System.Void Sirenix.Serialization.DefaultLoggers::.cctor()
extern void DefaultLoggers__cctor_mA6A418D5344FF049E9D86CA1EED93AAD6DE972A5 (void);
// 0x00000008 Sirenix.Serialization.ILogger Sirenix.Serialization.GlobalSerializationConfig::get_Logger()
extern void GlobalSerializationConfig_get_Logger_mFCA72EE658A49D49C76A6B1C73FA77B59324C0AA (void);
// 0x00000009 Sirenix.Serialization.DataFormat Sirenix.Serialization.GlobalSerializationConfig::get_EditorSerializationFormat()
extern void GlobalSerializationConfig_get_EditorSerializationFormat_mC71727CE5CB4000C3AE1DE2B2B1379038EBA98F8 (void);
// 0x0000000A System.Void Sirenix.Serialization.GlobalSerializationConfig::set_EditorSerializationFormat(Sirenix.Serialization.DataFormat)
extern void GlobalSerializationConfig_set_EditorSerializationFormat_m441B2EFD78DC7405991EEAFEDA16669F7BF89CA8 (void);
// 0x0000000B Sirenix.Serialization.DataFormat Sirenix.Serialization.GlobalSerializationConfig::get_BuildSerializationFormat()
extern void GlobalSerializationConfig_get_BuildSerializationFormat_m0909DB05BF5BDF62EAE16994508ABB9B7673FF63 (void);
// 0x0000000C System.Void Sirenix.Serialization.GlobalSerializationConfig::set_BuildSerializationFormat(Sirenix.Serialization.DataFormat)
extern void GlobalSerializationConfig_set_BuildSerializationFormat_m0377C22837371D99ECE9478A73AB588D9D6195F8 (void);
// 0x0000000D Sirenix.Serialization.LoggingPolicy Sirenix.Serialization.GlobalSerializationConfig::get_LoggingPolicy()
extern void GlobalSerializationConfig_get_LoggingPolicy_m38E2468D6277E9DBC35EF21DD8C8C868A4E0971B (void);
// 0x0000000E System.Void Sirenix.Serialization.GlobalSerializationConfig::set_LoggingPolicy(Sirenix.Serialization.LoggingPolicy)
extern void GlobalSerializationConfig_set_LoggingPolicy_m2F2431D9E0FF5E345C160D74225E9175998E376D (void);
// 0x0000000F Sirenix.Serialization.ErrorHandlingPolicy Sirenix.Serialization.GlobalSerializationConfig::get_ErrorHandlingPolicy()
extern void GlobalSerializationConfig_get_ErrorHandlingPolicy_m889BDAB73BA877BCED9F00FF69B4B811B72C9826 (void);
// 0x00000010 System.Void Sirenix.Serialization.GlobalSerializationConfig::set_ErrorHandlingPolicy(Sirenix.Serialization.ErrorHandlingPolicy)
extern void GlobalSerializationConfig_set_ErrorHandlingPolicy_mAAB08DF6D93A23D09742D1322DD557E7CE923E62 (void);
// 0x00000011 System.Void Sirenix.Serialization.GlobalSerializationConfig::OnInspectorGUI()
extern void GlobalSerializationConfig_OnInspectorGUI_m0285988A2D83C308DC54F12FF917B605A9B69A4B (void);
// 0x00000012 System.Void Sirenix.Serialization.GlobalSerializationConfig::.ctor()
extern void GlobalSerializationConfig__ctor_mBA162FB69E619433C668D7A0CF1A7AD432C98CD6 (void);
// 0x00000013 System.Void Sirenix.Serialization.GlobalSerializationConfig::.cctor()
extern void GlobalSerializationConfig__cctor_m6467F090607731C78C12F091913730BF44F50786 (void);
// 0x00000014 System.Void Sirenix.Serialization.ILogger::LogWarning(System.String)
// 0x00000015 System.Void Sirenix.Serialization.ILogger::LogError(System.String)
// 0x00000016 System.Void Sirenix.Serialization.ILogger::LogException(System.Exception)
static Il2CppMethodPointer s_methodPointers[22] = 
{
	CustomLogger__ctor_m11132C8664248CABA7C74A46B1C667FC52E8824E,
	CustomLogger_LogWarning_m21AC3378D4B326E66A9ED47E1E631EE7A7DC4B91,
	CustomLogger_LogError_mF61A72C224F94FFB2EFA1B6778D155124C929EEA,
	CustomLogger_LogException_m85361A858858BBFA176945313B74A1628B6FE3D8,
	DefaultLoggers_get_DefaultLogger_mD4758D541F9D0CDA080CAF6CE1EE85925E2DA8D7,
	DefaultLoggers_get_UnityLogger_mCA9BD734E49EC8F7E550296F95775266E30B7E54,
	DefaultLoggers__cctor_mA6A418D5344FF049E9D86CA1EED93AAD6DE972A5,
	GlobalSerializationConfig_get_Logger_mFCA72EE658A49D49C76A6B1C73FA77B59324C0AA,
	GlobalSerializationConfig_get_EditorSerializationFormat_mC71727CE5CB4000C3AE1DE2B2B1379038EBA98F8,
	GlobalSerializationConfig_set_EditorSerializationFormat_m441B2EFD78DC7405991EEAFEDA16669F7BF89CA8,
	GlobalSerializationConfig_get_BuildSerializationFormat_m0909DB05BF5BDF62EAE16994508ABB9B7673FF63,
	GlobalSerializationConfig_set_BuildSerializationFormat_m0377C22837371D99ECE9478A73AB588D9D6195F8,
	GlobalSerializationConfig_get_LoggingPolicy_m38E2468D6277E9DBC35EF21DD8C8C868A4E0971B,
	GlobalSerializationConfig_set_LoggingPolicy_m2F2431D9E0FF5E345C160D74225E9175998E376D,
	GlobalSerializationConfig_get_ErrorHandlingPolicy_m889BDAB73BA877BCED9F00FF69B4B811B72C9826,
	GlobalSerializationConfig_set_ErrorHandlingPolicy_mAAB08DF6D93A23D09742D1322DD557E7CE923E62,
	GlobalSerializationConfig_OnInspectorGUI_m0285988A2D83C308DC54F12FF917B605A9B69A4B,
	GlobalSerializationConfig__ctor_mBA162FB69E619433C668D7A0CF1A7AD432C98CD6,
	GlobalSerializationConfig__cctor_m6467F090607731C78C12F091913730BF44F50786,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[22] = 
{
	197,
	26,
	26,
	26,
	4,
	4,
	3,
	14,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	23,
	23,
	3,
	26,
	26,
	26,
};
extern const Il2CppCodeGenModule g_Sirenix_Serialization_ConfigCodeGenModule;
const Il2CppCodeGenModule g_Sirenix_Serialization_ConfigCodeGenModule = 
{
	"Sirenix.Serialization.Config.dll",
	22,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
