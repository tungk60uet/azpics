﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
extern void TextGenerationSettings_CompareColors_m41313F2A332F5780C5BD6F8134EBB14473CC5C66 (void);
// 0x00000002 System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern void TextGenerationSettings_CompareVector2_m27AE82F513B8E6D4A529A02B1A3806A85E710F1C (void);
// 0x00000003 System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern void TextGenerationSettings_Equals_m39912D195B0384AADC5C274659324EC8720C4F7D (void);
// 0x00000004 System.Void UnityEngine.TextGenerator::.ctor()
extern void TextGenerator__ctor_mD3956FF7D10DC470522A6363E7D6EC243415098A (void);
// 0x00000005 System.Void UnityEngine.TextGenerator::.ctor(System.Int32)
extern void TextGenerator__ctor_m86E01E7BB9DC1B28F04961E482ED4D7065062BCE (void);
// 0x00000006 System.Void UnityEngine.TextGenerator::Finalize()
extern void TextGenerator_Finalize_m6E9076F61F7B4DD5E56207F39E8F5FD85F188D8A (void);
// 0x00000007 System.Void UnityEngine.TextGenerator::System.IDisposable.Dispose()
extern void TextGenerator_System_IDisposable_Dispose_m9D3291DC086282AF57A115B39D3C17BD0074FA3D (void);
// 0x00000008 System.Int32 UnityEngine.TextGenerator::get_characterCountVisible()
extern void TextGenerator_get_characterCountVisible_mD0E9AA8120947F5AED58F512C0978C2E82ED1182 (void);
// 0x00000009 UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::ValidatedSettings(UnityEngine.TextGenerationSettings)
extern void TextGenerator_ValidatedSettings_m167131680BB6CD53B929EF189520F9FCF71FB1D3 (void);
// 0x0000000A System.Void UnityEngine.TextGenerator::Invalidate()
extern void TextGenerator_Invalidate_m5C360AB470CB728BAA03B34BE33C75CBB55B673E (void);
// 0x0000000B System.Void UnityEngine.TextGenerator::GetCharacters(System.Collections.Generic.List`1<UnityEngine.UICharInfo>)
extern void TextGenerator_GetCharacters_mBB7980F2FE8BE65A906A39B5559EC54B1CEF4131 (void);
// 0x0000000C System.Void UnityEngine.TextGenerator::GetLines(System.Collections.Generic.List`1<UnityEngine.UILineInfo>)
extern void TextGenerator_GetLines_mC31F7918A9159908EA914D01B2E32644B046E2B5 (void);
// 0x0000000D System.Void UnityEngine.TextGenerator::GetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern void TextGenerator_GetVertices_m6FA34586541514ED7396990542BDAC536C10A4F2 (void);
// 0x0000000E System.Single UnityEngine.TextGenerator::GetPreferredWidth(System.String,UnityEngine.TextGenerationSettings)
extern void TextGenerator_GetPreferredWidth_mBF228094564195BBB66669F4ECC6EE1B0B05BAAA (void);
// 0x0000000F System.Single UnityEngine.TextGenerator::GetPreferredHeight(System.String,UnityEngine.TextGenerationSettings)
extern void TextGenerator_GetPreferredHeight_mC2F191D9E9CF2365545D0A3F1EBD0F105DB27963 (void);
// 0x00000010 System.Boolean UnityEngine.TextGenerator::PopulateWithErrors(System.String,UnityEngine.TextGenerationSettings,UnityEngine.GameObject)
extern void TextGenerator_PopulateWithErrors_m1F1851B3C2B2EBEFD81C83DC124FB376C926B933 (void);
// 0x00000011 System.Boolean UnityEngine.TextGenerator::Populate(System.String,UnityEngine.TextGenerationSettings)
extern void TextGenerator_Populate_m15553808C8FA017AA1AC23D2818C30DAFD654A04 (void);
// 0x00000012 UnityEngine.TextGenerationError UnityEngine.TextGenerator::PopulateWithError(System.String,UnityEngine.TextGenerationSettings)
extern void TextGenerator_PopulateWithError_m24D1DA75F0563582E228C6F4982D0913C58E1D7D (void);
// 0x00000013 UnityEngine.TextGenerationError UnityEngine.TextGenerator::PopulateAlways(System.String,UnityEngine.TextGenerationSettings)
extern void TextGenerator_PopulateAlways_m8DCF389A51877975F29FAB9B6E800DFDC1E0B8DF (void);
// 0x00000014 System.Collections.Generic.IList`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::get_verts()
extern void TextGenerator_get_verts_mD0B3D877BE872CDE4BE3791685B8B5EF0AAC6120 (void);
// 0x00000015 System.Collections.Generic.IList`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::get_characters()
extern void TextGenerator_get_characters_m716FE1EF0738A1E6B3FBF4A1DBC46244B9594C7B (void);
// 0x00000016 System.Collections.Generic.IList`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::get_lines()
extern void TextGenerator_get_lines_m40303E6BF9508DD46E04A21B5F5510F0FB9437CD (void);
// 0x00000017 UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
extern void TextGenerator_get_rectExtents_m55F6A6727406C54BEFB7628751555B7C58BEC9B1 (void);
// 0x00000018 System.Int32 UnityEngine.TextGenerator::get_characterCount()
extern void TextGenerator_get_characterCount_m2A8F9764A7BD2AD1287D3721638FB6114D6BDDC7 (void);
// 0x00000019 System.Int32 UnityEngine.TextGenerator::get_lineCount()
extern void TextGenerator_get_lineCount_m7A3CC9D67099CDC4723A683716BE5FBC623EE9C4 (void);
// 0x0000001A System.IntPtr UnityEngine.TextGenerator::Internal_Create()
extern void TextGenerator_Internal_Create_m127DBEDE47D3028812950FD9184A18C9F3A5E994 (void);
// 0x0000001B System.Void UnityEngine.TextGenerator::Internal_Destroy(System.IntPtr)
extern void TextGenerator_Internal_Destroy_mB7FE56C2FAAE16938DE8BC7256EB44643E1845A5 (void);
// 0x0000001C System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,System.UInt32&)
extern void TextGenerator_Populate_Internal_mCA54081A0855AED6EC6345265603409FE330985C (void);
// 0x0000001D System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.VerticalWrapMode,UnityEngine.HorizontalWrapMode,System.Boolean,UnityEngine.TextAnchor,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean,System.Boolean,UnityEngine.TextGenerationError&)
extern void TextGenerator_Populate_Internal_m42F7FED165D62BFD9C006D19A0FCE5B70C1EF92B (void);
// 0x0000001E System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
extern void TextGenerator_GetVerticesInternal_mB794B94982BA35D2CDB8F3AA77880B33AEB42B9A (void);
// 0x0000001F System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
extern void TextGenerator_GetCharactersInternal_m4383B9A162CF10430636BFD248DDBDDB4D64E967 (void);
// 0x00000020 System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
extern void TextGenerator_GetLinesInternal_mA54A05D512EE1CED958F73E0024FB913E648EDAD (void);
// 0x00000021 System.Void UnityEngine.TextGenerator::get_rectExtents_Injected(UnityEngine.Rect&)
extern void TextGenerator_get_rectExtents_Injected_mD8FC9E47642590C7AC78DA83B583E5F4271842D0 (void);
// 0x00000022 System.Boolean UnityEngine.TextGenerator::Populate_Internal_Injected(System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,System.UInt32&)
extern void TextGenerator_Populate_Internal_Injected_mC1D6A0A0A9E0BFDB146EA921DA459D83FF33DEDE (void);
// 0x00000023 System.Void UnityEngine.UIVertex::.cctor()
extern void UIVertex__cctor_m86F60F5BB996D3C59B19B80C4BFB5770802BFB30 (void);
// 0x00000024 System.Void UnityEngine.Font::add_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern void Font_add_textureRebuilt_m031EFCD3B164273920B133A8689C18ED87C9B18F (void);
// 0x00000025 System.Void UnityEngine.Font::remove_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern void Font_remove_textureRebuilt_mBEF163DAE27CA126D400646E850AAEE4AE8DAAB4 (void);
// 0x00000026 System.Void UnityEngine.Font::add_m_FontTextureRebuildCallback(UnityEngine.Font/FontTextureRebuildCallback)
extern void Font_add_m_FontTextureRebuildCallback_mACE2BEBD62FAB82849A6A7C0F96CE23B50DA3658 (void);
// 0x00000027 System.Void UnityEngine.Font::remove_m_FontTextureRebuildCallback(UnityEngine.Font/FontTextureRebuildCallback)
extern void Font_remove_m_FontTextureRebuildCallback_m1B2D5405219E93F383DE52DC3F88AC9ECAD9D5F3 (void);
// 0x00000028 UnityEngine.Material UnityEngine.Font::get_material()
extern void Font_get_material_m92A995029540A5FACAEA3A2FE792FFDAC294827D (void);
// 0x00000029 System.Void UnityEngine.Font::set_material(UnityEngine.Material)
extern void Font_set_material_m51BC12DBE8B71718F09771B8A9D97C661651D284 (void);
// 0x0000002A System.String[] UnityEngine.Font::get_fontNames()
extern void Font_get_fontNames_m7F78D60ADD0E58D9C3BA99477FFE48CE229FB6A3 (void);
// 0x0000002B System.Void UnityEngine.Font::set_fontNames(System.String[])
extern void Font_set_fontNames_m425AE177BC31F62149659E4F6A473A1DF6CA033F (void);
// 0x0000002C System.Boolean UnityEngine.Font::get_dynamic()
extern void Font_get_dynamic_m14C7E59606E317C5952A69F05CC44BF399CFFE2E (void);
// 0x0000002D System.Int32 UnityEngine.Font::get_ascent()
extern void Font_get_ascent_m341529493C8D8C2B3498F4EE418731555E839C03 (void);
// 0x0000002E System.Int32 UnityEngine.Font::get_fontSize()
extern void Font_get_fontSize_m75A71EFC3D6483AD1A8C6F38133648BDFF1618A5 (void);
// 0x0000002F UnityEngine.CharacterInfo[] UnityEngine.Font::get_characterInfo()
extern void Font_get_characterInfo_mBE8CEA19DF0EC612614D613AAAD15EFEF3D32537 (void);
// 0x00000030 System.Void UnityEngine.Font::set_characterInfo(UnityEngine.CharacterInfo[])
extern void Font_set_characterInfo_m5DFFA2B9F95847B007475EEBD39BF61DDAA3EDD5 (void);
// 0x00000031 System.Int32 UnityEngine.Font::get_lineHeight()
extern void Font_get_lineHeight_mC05340801C5AB468A0EB2E4BDB2E32011DE253F6 (void);
// 0x00000032 UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::get_textureRebuildCallback()
extern void Font_get_textureRebuildCallback_mB68A155C4B91E6F6A68C249890BF565C8B3AFC4E (void);
// 0x00000033 System.Void UnityEngine.Font::set_textureRebuildCallback(UnityEngine.Font/FontTextureRebuildCallback)
extern void Font_set_textureRebuildCallback_mAF132BA51BB29431DAF0771395C9B85087C3A6BA (void);
// 0x00000034 System.Void UnityEngine.Font::.ctor()
extern void Font__ctor_m0EB492A9B2082EEE21587ED01866DE1ED4C1E628 (void);
// 0x00000035 System.Void UnityEngine.Font::.ctor(System.String)
extern void Font__ctor_mB9A264E997B5F752F49A571B1B4772F8E098DDB1 (void);
// 0x00000036 System.Void UnityEngine.Font::.ctor(System.String[],System.Int32)
extern void Font__ctor_m0C14FA34DD683B6F2CEBDF41BB5DB46A98C0AED4 (void);
// 0x00000037 UnityEngine.Font UnityEngine.Font::CreateDynamicFontFromOSFont(System.String,System.Int32)
extern void Font_CreateDynamicFontFromOSFont_m45A9E67DED6075958AB768A70765A2C222C1E8C2 (void);
// 0x00000038 UnityEngine.Font UnityEngine.Font::CreateDynamicFontFromOSFont(System.String[],System.Int32)
extern void Font_CreateDynamicFontFromOSFont_mC67BE05157E03E78B07E241DE3F76B705FA84602 (void);
// 0x00000039 System.Void UnityEngine.Font::InvokeTextureRebuilt_Internal(UnityEngine.Font)
extern void Font_InvokeTextureRebuilt_Internal_m2D4C9D99B6137EF380A19EC72D6EE8CBFF7B4062 (void);
// 0x0000003A System.Int32 UnityEngine.Font::GetMaxVertsForString(System.String)
extern void Font_GetMaxVertsForString_m22C893DA2582D3D72003C2BA4891E747D2F47BC4 (void);
// 0x0000003B UnityEngine.Font UnityEngine.Font::GetDefault()
extern void Font_GetDefault_m736F5D0824A03205D497D02A26F72AE7EB70A6DB (void);
// 0x0000003C System.Boolean UnityEngine.Font::HasCharacter(System.Char)
extern void Font_HasCharacter_m23CC7E1E37BCA115DC130B841CF3207212E2802E (void);
// 0x0000003D System.Boolean UnityEngine.Font::HasCharacter(System.Int32)
extern void Font_HasCharacter_m59FF574F1E4A2F9807CCF0C5D56C29E68D514D51 (void);
// 0x0000003E System.String[] UnityEngine.Font::GetOSInstalledFontNames()
extern void Font_GetOSInstalledFontNames_mEEAE2C831EE1BCCC4ACAA0E7F43DCB7FE7A643CA (void);
// 0x0000003F System.String[] UnityEngine.Font::GetPathsToOSFonts()
extern void Font_GetPathsToOSFonts_mF4274F8FE743413C1E6EC2B04285747B5E961C22 (void);
// 0x00000040 System.Void UnityEngine.Font::Internal_CreateFont(UnityEngine.Font,System.String)
extern void Font_Internal_CreateFont_m1B4B34CFCE6782196D19DB5020CB4C4CEFFFC05E (void);
// 0x00000041 System.Void UnityEngine.Font::Internal_CreateFontFromPath(UnityEngine.Font,System.String)
extern void Font_Internal_CreateFontFromPath_mF641D8864EE8AE3A7940F6310EF954F592A9165B (void);
// 0x00000042 System.Void UnityEngine.Font::Internal_CreateDynamicFont(UnityEngine.Font,System.String[],System.Int32)
extern void Font_Internal_CreateDynamicFont_mD89887C819EFA46156C8402BAFE1BA7C43DE6941 (void);
// 0x00000043 System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32,UnityEngine.FontStyle)
extern void Font_GetCharacterInfo_mFC0350FC06315C632781B0BAF05F9C4F0F0B7E12 (void);
// 0x00000044 System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32)
extern void Font_GetCharacterInfo_m094F1389E0395E9A458E4F6035208C71FE9DE07B (void);
// 0x00000045 System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&)
extern void Font_GetCharacterInfo_mEC133A16F066F559E3111395DDD8F17E849A3960 (void);
// 0x00000046 System.Void UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32,UnityEngine.FontStyle)
extern void Font_RequestCharactersInTexture_mDD60F1C27486774C51F157FD9043D022EAE5F5BA (void);
// 0x00000047 System.Void UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32)
extern void Font_RequestCharactersInTexture_m58A8AC5E6E7CB7FD61EF759F0B80806FEE59C44A (void);
// 0x00000048 System.Void UnityEngine.Font::RequestCharactersInTexture(System.String)
extern void Font_RequestCharactersInTexture_mD2DAF6E48D04555140620DB363A1B3FBFB88130F (void);
// 0x00000049 System.Void UnityEngine.Font/FontTextureRebuildCallback::.ctor(System.Object,System.IntPtr)
extern void FontTextureRebuildCallback__ctor_m83BD4ACFF1FDA3D203ABA140B0CA2B4B0064A3A3 (void);
// 0x0000004A System.Void UnityEngine.Font/FontTextureRebuildCallback::Invoke()
extern void FontTextureRebuildCallback_Invoke_m4E6CFDE11932BA7F129C9A2C4CAE294562B07480 (void);
// 0x0000004B System.IAsyncResult UnityEngine.Font/FontTextureRebuildCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void FontTextureRebuildCallback_BeginInvoke_m53EF837EFEA71B83AEA6706E2EB8F83062E43880 (void);
// 0x0000004C System.Void UnityEngine.Font/FontTextureRebuildCallback::EndInvoke(System.IAsyncResult)
extern void FontTextureRebuildCallback_EndInvoke_m8EEDB9652F6D2358523057E1164740820D2AE93C (void);
static Il2CppMethodPointer s_methodPointers[76] = 
{
	TextGenerationSettings_CompareColors_m41313F2A332F5780C5BD6F8134EBB14473CC5C66,
	TextGenerationSettings_CompareVector2_m27AE82F513B8E6D4A529A02B1A3806A85E710F1C,
	TextGenerationSettings_Equals_m39912D195B0384AADC5C274659324EC8720C4F7D,
	TextGenerator__ctor_mD3956FF7D10DC470522A6363E7D6EC243415098A,
	TextGenerator__ctor_m86E01E7BB9DC1B28F04961E482ED4D7065062BCE,
	TextGenerator_Finalize_m6E9076F61F7B4DD5E56207F39E8F5FD85F188D8A,
	TextGenerator_System_IDisposable_Dispose_m9D3291DC086282AF57A115B39D3C17BD0074FA3D,
	TextGenerator_get_characterCountVisible_mD0E9AA8120947F5AED58F512C0978C2E82ED1182,
	TextGenerator_ValidatedSettings_m167131680BB6CD53B929EF189520F9FCF71FB1D3,
	TextGenerator_Invalidate_m5C360AB470CB728BAA03B34BE33C75CBB55B673E,
	TextGenerator_GetCharacters_mBB7980F2FE8BE65A906A39B5559EC54B1CEF4131,
	TextGenerator_GetLines_mC31F7918A9159908EA914D01B2E32644B046E2B5,
	TextGenerator_GetVertices_m6FA34586541514ED7396990542BDAC536C10A4F2,
	TextGenerator_GetPreferredWidth_mBF228094564195BBB66669F4ECC6EE1B0B05BAAA,
	TextGenerator_GetPreferredHeight_mC2F191D9E9CF2365545D0A3F1EBD0F105DB27963,
	TextGenerator_PopulateWithErrors_m1F1851B3C2B2EBEFD81C83DC124FB376C926B933,
	TextGenerator_Populate_m15553808C8FA017AA1AC23D2818C30DAFD654A04,
	TextGenerator_PopulateWithError_m24D1DA75F0563582E228C6F4982D0913C58E1D7D,
	TextGenerator_PopulateAlways_m8DCF389A51877975F29FAB9B6E800DFDC1E0B8DF,
	TextGenerator_get_verts_mD0B3D877BE872CDE4BE3791685B8B5EF0AAC6120,
	TextGenerator_get_characters_m716FE1EF0738A1E6B3FBF4A1DBC46244B9594C7B,
	TextGenerator_get_lines_m40303E6BF9508DD46E04A21B5F5510F0FB9437CD,
	TextGenerator_get_rectExtents_m55F6A6727406C54BEFB7628751555B7C58BEC9B1,
	TextGenerator_get_characterCount_m2A8F9764A7BD2AD1287D3721638FB6114D6BDDC7,
	TextGenerator_get_lineCount_m7A3CC9D67099CDC4723A683716BE5FBC623EE9C4,
	TextGenerator_Internal_Create_m127DBEDE47D3028812950FD9184A18C9F3A5E994,
	TextGenerator_Internal_Destroy_mB7FE56C2FAAE16938DE8BC7256EB44643E1845A5,
	TextGenerator_Populate_Internal_mCA54081A0855AED6EC6345265603409FE330985C,
	TextGenerator_Populate_Internal_m42F7FED165D62BFD9C006D19A0FCE5B70C1EF92B,
	TextGenerator_GetVerticesInternal_mB794B94982BA35D2CDB8F3AA77880B33AEB42B9A,
	TextGenerator_GetCharactersInternal_m4383B9A162CF10430636BFD248DDBDDB4D64E967,
	TextGenerator_GetLinesInternal_mA54A05D512EE1CED958F73E0024FB913E648EDAD,
	TextGenerator_get_rectExtents_Injected_mD8FC9E47642590C7AC78DA83B583E5F4271842D0,
	TextGenerator_Populate_Internal_Injected_mC1D6A0A0A9E0BFDB146EA921DA459D83FF33DEDE,
	UIVertex__cctor_m86F60F5BB996D3C59B19B80C4BFB5770802BFB30,
	Font_add_textureRebuilt_m031EFCD3B164273920B133A8689C18ED87C9B18F,
	Font_remove_textureRebuilt_mBEF163DAE27CA126D400646E850AAEE4AE8DAAB4,
	Font_add_m_FontTextureRebuildCallback_mACE2BEBD62FAB82849A6A7C0F96CE23B50DA3658,
	Font_remove_m_FontTextureRebuildCallback_m1B2D5405219E93F383DE52DC3F88AC9ECAD9D5F3,
	Font_get_material_m92A995029540A5FACAEA3A2FE792FFDAC294827D,
	Font_set_material_m51BC12DBE8B71718F09771B8A9D97C661651D284,
	Font_get_fontNames_m7F78D60ADD0E58D9C3BA99477FFE48CE229FB6A3,
	Font_set_fontNames_m425AE177BC31F62149659E4F6A473A1DF6CA033F,
	Font_get_dynamic_m14C7E59606E317C5952A69F05CC44BF399CFFE2E,
	Font_get_ascent_m341529493C8D8C2B3498F4EE418731555E839C03,
	Font_get_fontSize_m75A71EFC3D6483AD1A8C6F38133648BDFF1618A5,
	Font_get_characterInfo_mBE8CEA19DF0EC612614D613AAAD15EFEF3D32537,
	Font_set_characterInfo_m5DFFA2B9F95847B007475EEBD39BF61DDAA3EDD5,
	Font_get_lineHeight_mC05340801C5AB468A0EB2E4BDB2E32011DE253F6,
	Font_get_textureRebuildCallback_mB68A155C4B91E6F6A68C249890BF565C8B3AFC4E,
	Font_set_textureRebuildCallback_mAF132BA51BB29431DAF0771395C9B85087C3A6BA,
	Font__ctor_m0EB492A9B2082EEE21587ED01866DE1ED4C1E628,
	Font__ctor_mB9A264E997B5F752F49A571B1B4772F8E098DDB1,
	Font__ctor_m0C14FA34DD683B6F2CEBDF41BB5DB46A98C0AED4,
	Font_CreateDynamicFontFromOSFont_m45A9E67DED6075958AB768A70765A2C222C1E8C2,
	Font_CreateDynamicFontFromOSFont_mC67BE05157E03E78B07E241DE3F76B705FA84602,
	Font_InvokeTextureRebuilt_Internal_m2D4C9D99B6137EF380A19EC72D6EE8CBFF7B4062,
	Font_GetMaxVertsForString_m22C893DA2582D3D72003C2BA4891E747D2F47BC4,
	Font_GetDefault_m736F5D0824A03205D497D02A26F72AE7EB70A6DB,
	Font_HasCharacter_m23CC7E1E37BCA115DC130B841CF3207212E2802E,
	Font_HasCharacter_m59FF574F1E4A2F9807CCF0C5D56C29E68D514D51,
	Font_GetOSInstalledFontNames_mEEAE2C831EE1BCCC4ACAA0E7F43DCB7FE7A643CA,
	Font_GetPathsToOSFonts_mF4274F8FE743413C1E6EC2B04285747B5E961C22,
	Font_Internal_CreateFont_m1B4B34CFCE6782196D19DB5020CB4C4CEFFFC05E,
	Font_Internal_CreateFontFromPath_mF641D8864EE8AE3A7940F6310EF954F592A9165B,
	Font_Internal_CreateDynamicFont_mD89887C819EFA46156C8402BAFE1BA7C43DE6941,
	Font_GetCharacterInfo_mFC0350FC06315C632781B0BAF05F9C4F0F0B7E12,
	Font_GetCharacterInfo_m094F1389E0395E9A458E4F6035208C71FE9DE07B,
	Font_GetCharacterInfo_mEC133A16F066F559E3111395DDD8F17E849A3960,
	Font_RequestCharactersInTexture_mDD60F1C27486774C51F157FD9043D022EAE5F5BA,
	Font_RequestCharactersInTexture_m58A8AC5E6E7CB7FD61EF759F0B80806FEE59C44A,
	Font_RequestCharactersInTexture_mD2DAF6E48D04555140620DB363A1B3FBFB88130F,
	FontTextureRebuildCallback__ctor_m83BD4ACFF1FDA3D203ABA140B0CA2B4B0064A3A3,
	FontTextureRebuildCallback_Invoke_m4E6CFDE11932BA7F129C9A2C4CAE294562B07480,
	FontTextureRebuildCallback_BeginInvoke_m53EF837EFEA71B83AEA6706E2EB8F83062E43880,
	FontTextureRebuildCallback_EndInvoke_m8EEDB9652F6D2358523057E1164740820D2AE93C,
};
extern void TextGenerationSettings_CompareColors_m41313F2A332F5780C5BD6F8134EBB14473CC5C66_AdjustorThunk (void);
extern void TextGenerationSettings_CompareVector2_m27AE82F513B8E6D4A529A02B1A3806A85E710F1C_AdjustorThunk (void);
extern void TextGenerationSettings_Equals_m39912D195B0384AADC5C274659324EC8720C4F7D_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[3] = 
{
	{ 0x06000001, TextGenerationSettings_CompareColors_m41313F2A332F5780C5BD6F8134EBB14473CC5C66_AdjustorThunk },
	{ 0x06000002, TextGenerationSettings_CompareVector2_m27AE82F513B8E6D4A529A02B1A3806A85E710F1C_AdjustorThunk },
	{ 0x06000003, TextGenerationSettings_Equals_m39912D195B0384AADC5C274659324EC8720C4F7D_AdjustorThunk },
};
static const int32_t s_InvokerIndices[76] = 
{
	1714,
	1715,
	1716,
	23,
	32,
	23,
	23,
	10,
	1717,
	23,
	26,
	26,
	26,
	1718,
	1718,
	1719,
	1720,
	1721,
	1721,
	14,
	14,
	14,
	1419,
	10,
	10,
	769,
	25,
	1722,
	1723,
	26,
	26,
	26,
	6,
	1724,
	3,
	154,
	154,
	26,
	26,
	14,
	26,
	14,
	26,
	89,
	10,
	10,
	14,
	26,
	10,
	14,
	26,
	23,
	26,
	130,
	119,
	119,
	154,
	94,
	4,
	231,
	30,
	4,
	4,
	137,
	137,
	195,
	1725,
	1726,
	669,
	35,
	130,
	26,
	124,
	23,
	105,
	26,
};
extern const Il2CppCodeGenModule g_UnityEngine_TextRenderingModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_TextRenderingModuleCodeGenModule = 
{
	"UnityEngine.TextRenderingModule.dll",
	76,
	s_methodPointers,
	3,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
