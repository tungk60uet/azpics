﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 <Assembly>j__TPar <>f__AnonymousType0`2::get_Assembly()
// 0x00000002 <Attribute>j__TPar <>f__AnonymousType0`2::get_Attribute()
// 0x00000003 System.Void <>f__AnonymousType0`2::.ctor(<Assembly>j__TPar,<Attribute>j__TPar)
// 0x00000004 System.Boolean <>f__AnonymousType0`2::Equals(System.Object)
// 0x00000005 System.Int32 <>f__AnonymousType0`2::GetHashCode()
// 0x00000006 System.String <>f__AnonymousType0`2::ToString()
// 0x00000007 Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedBehaviour::Sirenix.Serialization.ISupportsPrefabSerialization.get_SerializationData()
extern void SerializedBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_get_SerializationData_m1D7244104C84437AB66D01345D705AE1B96511D7 (void);
// 0x00000008 System.Void Sirenix.OdinInspector.SerializedBehaviour::Sirenix.Serialization.ISupportsPrefabSerialization.set_SerializationData(Sirenix.Serialization.SerializationData)
extern void SerializedBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_set_SerializationData_m7A7A175ABF183A68762981C05023875D1D16BBA3 (void);
// 0x00000009 System.Void Sirenix.OdinInspector.SerializedBehaviour::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SerializedBehaviour_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m231CBCF7B99F8737D0CE669C49C665B6ECCFCB1B (void);
// 0x0000000A System.Void Sirenix.OdinInspector.SerializedBehaviour::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SerializedBehaviour_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mEA27F0E4ED5E2BA44206EFF47C83C64F1CC66D95 (void);
// 0x0000000B System.Void Sirenix.OdinInspector.SerializedBehaviour::OnAfterDeserialize()
extern void SerializedBehaviour_OnAfterDeserialize_mDDFBF15521127B1605100D6426A426965EA53853 (void);
// 0x0000000C System.Void Sirenix.OdinInspector.SerializedBehaviour::OnBeforeSerialize()
extern void SerializedBehaviour_OnBeforeSerialize_mA3E56786A77E2CA5B0B3C8E4C922D38047DE2737 (void);
// 0x0000000D System.Void Sirenix.OdinInspector.SerializedBehaviour::.ctor()
extern void SerializedBehaviour__ctor_m7369A41C49C4DE613EC444D2C0C83ACC40718FBA (void);
// 0x0000000E Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedComponent::Sirenix.Serialization.ISupportsPrefabSerialization.get_SerializationData()
extern void SerializedComponent_Sirenix_Serialization_ISupportsPrefabSerialization_get_SerializationData_m9A415AF7EF2DF7BC3705A6561F62CEB25EECB2E2 (void);
// 0x0000000F System.Void Sirenix.OdinInspector.SerializedComponent::Sirenix.Serialization.ISupportsPrefabSerialization.set_SerializationData(Sirenix.Serialization.SerializationData)
extern void SerializedComponent_Sirenix_Serialization_ISupportsPrefabSerialization_set_SerializationData_m6EE6F9548064E29392CF5B973C7B1D4A63EC2F4F (void);
// 0x00000010 System.Void Sirenix.OdinInspector.SerializedComponent::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SerializedComponent_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m8E56BC26148CC8C8CB49EE4ED85587C9F8611C27 (void);
// 0x00000011 System.Void Sirenix.OdinInspector.SerializedComponent::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SerializedComponent_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m4A44AEED4D9068A7AD8BBD6499B936A48A3344BA (void);
// 0x00000012 System.Void Sirenix.OdinInspector.SerializedComponent::OnAfterDeserialize()
extern void SerializedComponent_OnAfterDeserialize_m898646C63DB9713C695AD0ACAB54D22EFBF257AC (void);
// 0x00000013 System.Void Sirenix.OdinInspector.SerializedComponent::OnBeforeSerialize()
extern void SerializedComponent_OnBeforeSerialize_m3F270DE1F0C50E929A18365248AD13A9F9E4720F (void);
// 0x00000014 System.Void Sirenix.OdinInspector.SerializedComponent::.ctor()
extern void SerializedComponent__ctor_mE9C85C50ADC8523FB1628A7BD2197C7BBAC33E89 (void);
// 0x00000015 Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedMonoBehaviour::Sirenix.Serialization.ISupportsPrefabSerialization.get_SerializationData()
extern void SerializedMonoBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_get_SerializationData_m2CE517A444D2A5E343188B81887401EB1712DFEC (void);
// 0x00000016 System.Void Sirenix.OdinInspector.SerializedMonoBehaviour::Sirenix.Serialization.ISupportsPrefabSerialization.set_SerializationData(Sirenix.Serialization.SerializationData)
extern void SerializedMonoBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_set_SerializationData_m64849DC2B9FBD4A1A60334A5E1868455E3692DB4 (void);
// 0x00000017 System.Void Sirenix.OdinInspector.SerializedMonoBehaviour::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SerializedMonoBehaviour_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mDB88DE9FB9302908036197B837842C66C484174B (void);
// 0x00000018 System.Void Sirenix.OdinInspector.SerializedMonoBehaviour::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SerializedMonoBehaviour_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m66535E272745D58C02FFC7DAE3013F5FCC48B49E (void);
// 0x00000019 System.Void Sirenix.OdinInspector.SerializedMonoBehaviour::OnAfterDeserialize()
extern void SerializedMonoBehaviour_OnAfterDeserialize_mEF706AB166EBC3022CF1C5D02E79DE385B6421D3 (void);
// 0x0000001A System.Void Sirenix.OdinInspector.SerializedMonoBehaviour::OnBeforeSerialize()
extern void SerializedMonoBehaviour_OnBeforeSerialize_mA43CF7F184927A667A3C7EAAD33F6353407F12AB (void);
// 0x0000001B System.Void Sirenix.OdinInspector.SerializedMonoBehaviour::.ctor()
extern void SerializedMonoBehaviour__ctor_m9966C7AAE31D79BB430868A86F4DD29AC3F30908 (void);
// 0x0000001C System.Void Sirenix.OdinInspector.SerializedScriptableObject::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SerializedScriptableObject_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3F1BBB06B5021D852E4BE29A4DC6D146CA9E4E23 (void);
// 0x0000001D System.Void Sirenix.OdinInspector.SerializedScriptableObject::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SerializedScriptableObject_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m162364613764E9E11D0AEE9A6E60D66F27DE3674 (void);
// 0x0000001E System.Void Sirenix.OdinInspector.SerializedScriptableObject::OnAfterDeserialize()
extern void SerializedScriptableObject_OnAfterDeserialize_m6A86D3F278A2AC4EDEA8F6990A4D3F6F5DC13157 (void);
// 0x0000001F System.Void Sirenix.OdinInspector.SerializedScriptableObject::OnBeforeSerialize()
extern void SerializedScriptableObject_OnBeforeSerialize_mDD54A16365E18651AD2F0A82FB972731CE354662 (void);
// 0x00000020 System.Void Sirenix.OdinInspector.SerializedScriptableObject::.ctor()
extern void SerializedScriptableObject__ctor_mF905DC04A8D07C7B5B56497A35B45A2EED638734 (void);
// 0x00000021 System.Void Sirenix.OdinInspector.SerializedStateMachineBehaviour::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SerializedStateMachineBehaviour_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3EDE134FF5B19BBB256109303BE96D1E974CF8E8 (void);
// 0x00000022 System.Void Sirenix.OdinInspector.SerializedStateMachineBehaviour::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SerializedStateMachineBehaviour_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m855E0B7644915E350746D14D2D8759CD79785128 (void);
// 0x00000023 System.Void Sirenix.OdinInspector.SerializedStateMachineBehaviour::OnAfterDeserialize()
extern void SerializedStateMachineBehaviour_OnAfterDeserialize_m3CC0D86B70A2D0538C86A13077EDF652103E05CF (void);
// 0x00000024 System.Void Sirenix.OdinInspector.SerializedStateMachineBehaviour::OnBeforeSerialize()
extern void SerializedStateMachineBehaviour_OnBeforeSerialize_mD20BBD8926B24CDFC5796FFC4C18C4C70DDB780B (void);
// 0x00000025 System.Void Sirenix.OdinInspector.SerializedStateMachineBehaviour::.ctor()
extern void SerializedStateMachineBehaviour__ctor_m8336D23805186A2E36683EE65F4DF8C814A5C5B1 (void);
// 0x00000026 System.Void Sirenix.OdinInspector.SerializedUnityObject::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SerializedUnityObject_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m660E2FA4FF687A60EBD0298AF737976BB4D5E857 (void);
// 0x00000027 System.Void Sirenix.OdinInspector.SerializedUnityObject::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SerializedUnityObject_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m63D7A3F8D675B353EC9A14768391A72A4C2BB8F5 (void);
// 0x00000028 System.Void Sirenix.OdinInspector.SerializedUnityObject::OnAfterDeserialize()
extern void SerializedUnityObject_OnAfterDeserialize_m8BEF8012872F172C655E29087159B21656206F10 (void);
// 0x00000029 System.Void Sirenix.OdinInspector.SerializedUnityObject::OnBeforeSerialize()
extern void SerializedUnityObject_OnBeforeSerialize_mB204CD9DECF9865DAEE8BCDA5A69CA10692D2B24 (void);
// 0x0000002A System.Void Sirenix.OdinInspector.SerializedUnityObject::.ctor()
extern void SerializedUnityObject__ctor_mF4574AA82B4DC0A43CE83FCEFA7C236C7F9584E2 (void);
// 0x0000002B System.Void Sirenix.Serialization.MethodInfoFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x0000002C System.Void Sirenix.Serialization.MethodInfoFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x0000002D T Sirenix.Serialization.MethodInfoFormatter`1::GetUninitializedObject()
// 0x0000002E System.Void Sirenix.Serialization.MethodInfoFormatter`1::.ctor()
// 0x0000002F System.Void Sirenix.Serialization.MethodInfoFormatter`1::.cctor()
// 0x00000030 System.Void Sirenix.Serialization.MethodInfoFormatter`1/<>c::.cctor()
// 0x00000031 System.Void Sirenix.Serialization.MethodInfoFormatter`1/<>c::.ctor()
// 0x00000032 System.String Sirenix.Serialization.MethodInfoFormatter`1/<>c::<DeserializeImplementation>b__3_0(System.Type)
// 0x00000033 System.String Sirenix.Serialization.MethodInfoFormatter`1/<>c::<DeserializeImplementation>b__3_1(System.Type)
// 0x00000034 System.Void Sirenix.Serialization.QueueFormatter`2::.cctor()
// 0x00000035 System.Void Sirenix.Serialization.QueueFormatter`2::.ctor()
// 0x00000036 TQueue Sirenix.Serialization.QueueFormatter`2::GetUninitializedObject()
// 0x00000037 System.Void Sirenix.Serialization.QueueFormatter`2::DeserializeImplementation(TQueue&,Sirenix.Serialization.IDataReader)
// 0x00000038 System.Void Sirenix.Serialization.QueueFormatter`2::SerializeImplementation(TQueue&,Sirenix.Serialization.IDataWriter)
// 0x00000039 System.Void Sirenix.Serialization.ReflectionOrEmittedBaseFormatter`1::.ctor()
// 0x0000003A System.Void Sirenix.Serialization.StackFormatter`2::.cctor()
// 0x0000003B System.Void Sirenix.Serialization.StackFormatter`2::.ctor()
// 0x0000003C TStack Sirenix.Serialization.StackFormatter`2::GetUninitializedObject()
// 0x0000003D System.Void Sirenix.Serialization.StackFormatter`2::DeserializeImplementation(TStack&,Sirenix.Serialization.IDataReader)
// 0x0000003E System.Void Sirenix.Serialization.StackFormatter`2::SerializeImplementation(TStack&,Sirenix.Serialization.IDataWriter)
// 0x0000003F System.Version Sirenix.Serialization.VersionFormatter::GetUninitializedObject()
extern void VersionFormatter_GetUninitializedObject_m81695DF17E9575E8B0764FDC8709CE26ABC14775 (void);
// 0x00000040 System.Void Sirenix.Serialization.VersionFormatter::Read(System.Version&,Sirenix.Serialization.IDataReader)
extern void VersionFormatter_Read_m301C155F9E9DF8DFE8B0104F22FAAAF1BD41D76D (void);
// 0x00000041 System.Void Sirenix.Serialization.VersionFormatter::Write(System.Version&,Sirenix.Serialization.IDataWriter)
extern void VersionFormatter_Write_mDA6955EEA60F8BAF1FCB686A396C9C9669DCE3E1 (void);
// 0x00000042 System.Void Sirenix.Serialization.VersionFormatter::.ctor()
extern void VersionFormatter__ctor_mC04B97DEAB78B25304642251FD42ECD4D925344E (void);
// 0x00000043 System.Void Sirenix.Serialization.AllowDeserializeInvalidDataAttribute::.ctor()
extern void AllowDeserializeInvalidDataAttribute__ctor_mE97361871FB9A3D929C3453AFF38435CEB754499 (void);
// 0x00000044 System.Void Sirenix.Serialization.ArchitectureInfo::.cctor()
extern void ArchitectureInfo__cctor_mC07B991E1DC115F3CB0C8529053556FEBB9DAF99 (void);
// 0x00000045 System.Void Sirenix.Serialization.ArchitectureInfo::SetRuntimePlatform(UnityEngine.RuntimePlatform)
extern void ArchitectureInfo_SetRuntimePlatform_mF3058044930F2BF44BD2AFE959B2626A4E53A30E (void);
// 0x00000046 Sirenix.Serialization.ISerializationPolicy Sirenix.Serialization.IOverridesSerializationPolicy::get_SerializationPolicy()
// 0x00000047 System.Boolean Sirenix.Serialization.IOverridesSerializationPolicy::get_OdinSerializesUnityFields()
// 0x00000048 System.Boolean Sirenix.Serialization.ArrayFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void ArrayFormatterLocator_TryGetFormatter_mA0968E64554BF2F7A081920FC4C17E08D799D168 (void);
// 0x00000049 System.Void Sirenix.Serialization.ArrayFormatterLocator::.ctor()
extern void ArrayFormatterLocator__ctor_mA9446684941516250931B101B8F0F44C6EDA770A (void);
// 0x0000004A System.Boolean Sirenix.Serialization.DelegateFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void DelegateFormatterLocator_TryGetFormatter_m725610512E6F6C11BA0653AE5152047AA5BC2A8E (void);
// 0x0000004B System.Void Sirenix.Serialization.DelegateFormatterLocator::.ctor()
extern void DelegateFormatterLocator__ctor_mEB23F4E90F9274E1E15278B437ACA5D2AA6AFCCC (void);
// 0x0000004C System.Boolean Sirenix.Serialization.IAskIfCanFormatTypes::CanFormatType(System.Type)
// 0x0000004D System.Boolean Sirenix.Serialization.IFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
// 0x0000004E System.Boolean Sirenix.Serialization.GenericCollectionFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void GenericCollectionFormatterLocator_TryGetFormatter_m3BCA951CF7CABE22A3567619600E60722E6A0601 (void);
// 0x0000004F System.Void Sirenix.Serialization.GenericCollectionFormatterLocator::.ctor()
extern void GenericCollectionFormatterLocator__ctor_mBEC7A66ACBC662630C23CC9534F6AED56EBB9649 (void);
// 0x00000050 System.Boolean Sirenix.Serialization.ISerializableFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void ISerializableFormatterLocator_TryGetFormatter_m2F812924A81DA804DEEB5304139443356D1B45D8 (void);
// 0x00000051 System.Void Sirenix.Serialization.ISerializableFormatterLocator::.ctor()
extern void ISerializableFormatterLocator__ctor_mE84E24B0D52003D3B4AEDFD5C87388D8243B7256 (void);
// 0x00000052 System.Type Sirenix.Serialization.RegisterFormatterAttribute::get_FormatterType()
extern void RegisterFormatterAttribute_get_FormatterType_mF956959C4A65FFD052162F474414F26947CB434D (void);
// 0x00000053 System.Void Sirenix.Serialization.RegisterFormatterAttribute::set_FormatterType(System.Type)
extern void RegisterFormatterAttribute_set_FormatterType_mA35337AE66D36BD75BDC4637934CB9485BD7AAC5 (void);
// 0x00000054 System.Int32 Sirenix.Serialization.RegisterFormatterAttribute::get_Priority()
extern void RegisterFormatterAttribute_get_Priority_m6C50BACE64D393E48F14EFFD26B0154FD768CDC0 (void);
// 0x00000055 System.Void Sirenix.Serialization.RegisterFormatterAttribute::set_Priority(System.Int32)
extern void RegisterFormatterAttribute_set_Priority_m12DBB04E52AEED65CB20ACAA6B8030B8F321DA1E (void);
// 0x00000056 System.Void Sirenix.Serialization.RegisterFormatterAttribute::.ctor(System.Type,System.Int32)
extern void RegisterFormatterAttribute__ctor_mB1B819795911CCB0A8AD1A23B9DF3E939A5EAE2D (void);
// 0x00000057 System.Type Sirenix.Serialization.RegisterFormatterLocatorAttribute::get_FormatterLocatorType()
extern void RegisterFormatterLocatorAttribute_get_FormatterLocatorType_m7FE9AF51A8F373582193FD38F6434BD4A760BB43 (void);
// 0x00000058 System.Void Sirenix.Serialization.RegisterFormatterLocatorAttribute::set_FormatterLocatorType(System.Type)
extern void RegisterFormatterLocatorAttribute_set_FormatterLocatorType_m0923C1539EFD4A00CBA3A41E744488986B474448 (void);
// 0x00000059 System.Int32 Sirenix.Serialization.RegisterFormatterLocatorAttribute::get_Priority()
extern void RegisterFormatterLocatorAttribute_get_Priority_m400AA137158D24DE437E972EF73FA030FA127985 (void);
// 0x0000005A System.Void Sirenix.Serialization.RegisterFormatterLocatorAttribute::set_Priority(System.Int32)
extern void RegisterFormatterLocatorAttribute_set_Priority_mBF03765D5F697C9ADB3EE0F854FD861CC59CA581 (void);
// 0x0000005B System.Void Sirenix.Serialization.RegisterFormatterLocatorAttribute::.ctor(System.Type,System.Int32)
extern void RegisterFormatterLocatorAttribute__ctor_m19C6CED811535C351A71A1FEDA17935DF6CE34BC (void);
// 0x0000005C System.Boolean Sirenix.Serialization.SelfFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void SelfFormatterLocator_TryGetFormatter_mA6508574A4FBE8184FAE951FA52D6EB62A3CBDA8 (void);
// 0x0000005D System.Void Sirenix.Serialization.SelfFormatterLocator::.ctor()
extern void SelfFormatterLocator__ctor_m9D1547C4ECFA91AEAFFB418B149D9A52CBFFA2E9 (void);
// 0x0000005E System.Boolean Sirenix.Serialization.TypeFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void TypeFormatterLocator_TryGetFormatter_m74916A084834502DB5BC344378DEA2BC5837A1A3 (void);
// 0x0000005F System.Void Sirenix.Serialization.TypeFormatterLocator::.ctor()
extern void TypeFormatterLocator__ctor_m5196C596A4EB5C3188B3021FFF1F64895EF8A6E9 (void);
// 0x00000060 System.Void Sirenix.Serialization.BaseDataReader::.ctor(System.IO.Stream,Sirenix.Serialization.DeserializationContext)
extern void BaseDataReader__ctor_m19AEF27F1CD0371D17D2E2F80C5DC4C0272CF81F (void);
// 0x00000061 System.Int32 Sirenix.Serialization.BaseDataReader::get_CurrentNodeId()
extern void BaseDataReader_get_CurrentNodeId_mD1627D85AFE075AB440B9F5BCE33E751381C49AC (void);
// 0x00000062 System.Int32 Sirenix.Serialization.BaseDataReader::get_CurrentNodeDepth()
extern void BaseDataReader_get_CurrentNodeDepth_mFA481A654CBE2756053B330CC2E01443C2A57F4C (void);
// 0x00000063 System.String Sirenix.Serialization.BaseDataReader::get_CurrentNodeName()
extern void BaseDataReader_get_CurrentNodeName_m4EF2D0E8107CD7D785A5CE10E13EE5FAC760CC91 (void);
// 0x00000064 System.IO.Stream Sirenix.Serialization.BaseDataReader::get_Stream()
extern void BaseDataReader_get_Stream_mD8FEF305D07F4121682AD07A32E534492C99898B (void);
// 0x00000065 System.Void Sirenix.Serialization.BaseDataReader::set_Stream(System.IO.Stream)
extern void BaseDataReader_set_Stream_m84189ADBAC883C106E2868A9CEC9537FE6D8C4F7 (void);
// 0x00000066 Sirenix.Serialization.DeserializationContext Sirenix.Serialization.BaseDataReader::get_Context()
extern void BaseDataReader_get_Context_m812E2B3857D46B577468CA7BCF7B4D7634A52A3E (void);
// 0x00000067 System.Void Sirenix.Serialization.BaseDataReader::set_Context(Sirenix.Serialization.DeserializationContext)
extern void BaseDataReader_set_Context_mB5E0A8B454B9FBDCD10BFBC6EE45EC9AC88F3C8C (void);
// 0x00000068 System.Boolean Sirenix.Serialization.BaseDataReader::EnterNode(System.Type&)
// 0x00000069 System.Boolean Sirenix.Serialization.BaseDataReader::ExitNode()
// 0x0000006A System.Boolean Sirenix.Serialization.BaseDataReader::EnterArray(System.Int64&)
// 0x0000006B System.Boolean Sirenix.Serialization.BaseDataReader::ExitArray()
// 0x0000006C System.Boolean Sirenix.Serialization.BaseDataReader::ReadPrimitiveArray(T[]&)
// 0x0000006D Sirenix.Serialization.EntryType Sirenix.Serialization.BaseDataReader::PeekEntry(System.String&)
// 0x0000006E System.Boolean Sirenix.Serialization.BaseDataReader::ReadInternalReference(System.Int32&)
// 0x0000006F System.Boolean Sirenix.Serialization.BaseDataReader::ReadExternalReference(System.Int32&)
// 0x00000070 System.Boolean Sirenix.Serialization.BaseDataReader::ReadExternalReference(System.Guid&)
// 0x00000071 System.Boolean Sirenix.Serialization.BaseDataReader::ReadExternalReference(System.String&)
// 0x00000072 System.Boolean Sirenix.Serialization.BaseDataReader::ReadChar(System.Char&)
// 0x00000073 System.Boolean Sirenix.Serialization.BaseDataReader::ReadString(System.String&)
// 0x00000074 System.Boolean Sirenix.Serialization.BaseDataReader::ReadGuid(System.Guid&)
// 0x00000075 System.Boolean Sirenix.Serialization.BaseDataReader::ReadSByte(System.SByte&)
// 0x00000076 System.Boolean Sirenix.Serialization.BaseDataReader::ReadInt16(System.Int16&)
// 0x00000077 System.Boolean Sirenix.Serialization.BaseDataReader::ReadInt32(System.Int32&)
// 0x00000078 System.Boolean Sirenix.Serialization.BaseDataReader::ReadInt64(System.Int64&)
// 0x00000079 System.Boolean Sirenix.Serialization.BaseDataReader::ReadByte(System.Byte&)
// 0x0000007A System.Boolean Sirenix.Serialization.BaseDataReader::ReadUInt16(System.UInt16&)
// 0x0000007B System.Boolean Sirenix.Serialization.BaseDataReader::ReadUInt32(System.UInt32&)
// 0x0000007C System.Boolean Sirenix.Serialization.BaseDataReader::ReadUInt64(System.UInt64&)
// 0x0000007D System.Boolean Sirenix.Serialization.BaseDataReader::ReadDecimal(System.Decimal&)
// 0x0000007E System.Boolean Sirenix.Serialization.BaseDataReader::ReadSingle(System.Single&)
// 0x0000007F System.Boolean Sirenix.Serialization.BaseDataReader::ReadDouble(System.Double&)
// 0x00000080 System.Boolean Sirenix.Serialization.BaseDataReader::ReadBoolean(System.Boolean&)
// 0x00000081 System.Boolean Sirenix.Serialization.BaseDataReader::ReadNull()
// 0x00000082 System.Void Sirenix.Serialization.BaseDataReader::SkipEntry()
extern void BaseDataReader_SkipEntry_m0F94BF50A3A9AB4B983B8A588016B7623AEC227E (void);
// 0x00000083 System.Void Sirenix.Serialization.BaseDataReader::Dispose()
// 0x00000084 System.Void Sirenix.Serialization.BaseDataReader::PrepareNewSerializationSession()
extern void BaseDataReader_PrepareNewSerializationSession_mCD9BDE456C3CCA7D40942A8EFFE4B47AF6A1FB87 (void);
// 0x00000085 System.String Sirenix.Serialization.BaseDataReader::GetDataDump()
// 0x00000086 Sirenix.Serialization.EntryType Sirenix.Serialization.BaseDataReader::PeekEntry()
// 0x00000087 Sirenix.Serialization.EntryType Sirenix.Serialization.BaseDataReader::ReadToNextEntry()
// 0x00000088 Sirenix.Serialization.TwoWaySerializationBinder Sirenix.Serialization.BaseDataReaderWriter::get_Binder()
extern void BaseDataReaderWriter_get_Binder_m34107DEFF80EAE2073B216E1FA0CFE8E1D84DCB5 (void);
// 0x00000089 System.Void Sirenix.Serialization.BaseDataReaderWriter::set_Binder(Sirenix.Serialization.TwoWaySerializationBinder)
extern void BaseDataReaderWriter_set_Binder_mEEF34D7EF09034E3FC945981387820C716947433 (void);
// 0x0000008A System.Boolean Sirenix.Serialization.BaseDataReaderWriter::get_IsInArrayNode()
extern void BaseDataReaderWriter_get_IsInArrayNode_m0058B73D7DDE539FC7C6C73FF03889B083B75E5E (void);
// 0x0000008B System.Int32 Sirenix.Serialization.BaseDataReaderWriter::get_NodeDepth()
extern void BaseDataReaderWriter_get_NodeDepth_m207767DCCE22E2C09B0F28CCB06DD618378C3922 (void);
// 0x0000008C Sirenix.Serialization.NodeInfo Sirenix.Serialization.BaseDataReaderWriter::get_CurrentNode()
extern void BaseDataReaderWriter_get_CurrentNode_mC8ECBA7738A96FF8C391ED8016AF0C0D23FCA61C (void);
// 0x0000008D System.Void Sirenix.Serialization.BaseDataReaderWriter::PushNode(Sirenix.Serialization.NodeInfo)
extern void BaseDataReaderWriter_PushNode_mCB39A9AE0E0D73AA9DA3E07E77C20B06D28A469D (void);
// 0x0000008E System.Void Sirenix.Serialization.BaseDataReaderWriter::PushNode(System.String,System.Int32,System.Type)
extern void BaseDataReaderWriter_PushNode_m5CBF3EE5F829E1B3EDAE8734758B4C7E075AC656 (void);
// 0x0000008F System.Void Sirenix.Serialization.BaseDataReaderWriter::PushArray()
extern void BaseDataReaderWriter_PushArray_mD5838EF19AB9FBE69622603994433A886D74BB96 (void);
// 0x00000090 System.Void Sirenix.Serialization.BaseDataReaderWriter::ExpandNodes()
extern void BaseDataReaderWriter_ExpandNodes_m0869773D575D9C2C4063674CECB7B5F1664CADCC (void);
// 0x00000091 System.Void Sirenix.Serialization.BaseDataReaderWriter::PopNode(System.String)
extern void BaseDataReaderWriter_PopNode_m696F0C285F9EB6F2EDFC8172D6119C1E00B6AE09 (void);
// 0x00000092 System.Void Sirenix.Serialization.BaseDataReaderWriter::PopArray()
extern void BaseDataReaderWriter_PopArray_mCBDF73852451F1891D697A047F1024BDFCED877B (void);
// 0x00000093 System.Void Sirenix.Serialization.BaseDataReaderWriter::ClearNodes()
extern void BaseDataReaderWriter_ClearNodes_m1BFA05AC36057AF4DF2C37EDCCCDE6AD891112D6 (void);
// 0x00000094 System.Void Sirenix.Serialization.BaseDataReaderWriter::.ctor()
extern void BaseDataReaderWriter__ctor_m0399C0F2609B9F0818966E7E61DAE72CFD2351C0 (void);
// 0x00000095 System.Void Sirenix.Serialization.BaseDataWriter::.ctor(System.IO.Stream,Sirenix.Serialization.SerializationContext)
extern void BaseDataWriter__ctor_mF1F5BE8D818E59FB2A4E5CEEEF68E06F9400295D (void);
// 0x00000096 System.IO.Stream Sirenix.Serialization.BaseDataWriter::get_Stream()
extern void BaseDataWriter_get_Stream_m06D61522D6ACB8E1F16F85A3327BE76938A40C2E (void);
// 0x00000097 System.Void Sirenix.Serialization.BaseDataWriter::set_Stream(System.IO.Stream)
extern void BaseDataWriter_set_Stream_mF993EB71758532CF62A06C7A56F92D54C13543D3 (void);
// 0x00000098 Sirenix.Serialization.SerializationContext Sirenix.Serialization.BaseDataWriter::get_Context()
extern void BaseDataWriter_get_Context_mA8C8E3F53809E142F7F457B50EFF332018F896A9 (void);
// 0x00000099 System.Void Sirenix.Serialization.BaseDataWriter::set_Context(Sirenix.Serialization.SerializationContext)
extern void BaseDataWriter_set_Context_mE4830D3043D4540063AB0F4F1CDCCB9805220FE6 (void);
// 0x0000009A System.Void Sirenix.Serialization.BaseDataWriter::FlushToStream()
extern void BaseDataWriter_FlushToStream_m7F0C1632A809E7AE23DFE0D253B2B37C42023E67 (void);
// 0x0000009B System.Void Sirenix.Serialization.BaseDataWriter::BeginReferenceNode(System.String,System.Type,System.Int32)
// 0x0000009C System.Void Sirenix.Serialization.BaseDataWriter::BeginStructNode(System.String,System.Type)
// 0x0000009D System.Void Sirenix.Serialization.BaseDataWriter::EndNode(System.String)
// 0x0000009E System.Void Sirenix.Serialization.BaseDataWriter::BeginArrayNode(System.Int64)
// 0x0000009F System.Void Sirenix.Serialization.BaseDataWriter::EndArrayNode()
// 0x000000A0 System.Void Sirenix.Serialization.BaseDataWriter::WritePrimitiveArray(T[])
// 0x000000A1 System.Void Sirenix.Serialization.BaseDataWriter::WriteNull(System.String)
// 0x000000A2 System.Void Sirenix.Serialization.BaseDataWriter::WriteInternalReference(System.String,System.Int32)
// 0x000000A3 System.Void Sirenix.Serialization.BaseDataWriter::WriteExternalReference(System.String,System.Int32)
// 0x000000A4 System.Void Sirenix.Serialization.BaseDataWriter::WriteExternalReference(System.String,System.Guid)
// 0x000000A5 System.Void Sirenix.Serialization.BaseDataWriter::WriteExternalReference(System.String,System.String)
// 0x000000A6 System.Void Sirenix.Serialization.BaseDataWriter::WriteChar(System.String,System.Char)
// 0x000000A7 System.Void Sirenix.Serialization.BaseDataWriter::WriteString(System.String,System.String)
// 0x000000A8 System.Void Sirenix.Serialization.BaseDataWriter::WriteGuid(System.String,System.Guid)
// 0x000000A9 System.Void Sirenix.Serialization.BaseDataWriter::WriteSByte(System.String,System.SByte)
// 0x000000AA System.Void Sirenix.Serialization.BaseDataWriter::WriteInt16(System.String,System.Int16)
// 0x000000AB System.Void Sirenix.Serialization.BaseDataWriter::WriteInt32(System.String,System.Int32)
// 0x000000AC System.Void Sirenix.Serialization.BaseDataWriter::WriteInt64(System.String,System.Int64)
// 0x000000AD System.Void Sirenix.Serialization.BaseDataWriter::WriteByte(System.String,System.Byte)
// 0x000000AE System.Void Sirenix.Serialization.BaseDataWriter::WriteUInt16(System.String,System.UInt16)
// 0x000000AF System.Void Sirenix.Serialization.BaseDataWriter::WriteUInt32(System.String,System.UInt32)
// 0x000000B0 System.Void Sirenix.Serialization.BaseDataWriter::WriteUInt64(System.String,System.UInt64)
// 0x000000B1 System.Void Sirenix.Serialization.BaseDataWriter::WriteDecimal(System.String,System.Decimal)
// 0x000000B2 System.Void Sirenix.Serialization.BaseDataWriter::WriteSingle(System.String,System.Single)
// 0x000000B3 System.Void Sirenix.Serialization.BaseDataWriter::WriteDouble(System.String,System.Double)
// 0x000000B4 System.Void Sirenix.Serialization.BaseDataWriter::WriteBoolean(System.String,System.Boolean)
// 0x000000B5 System.Void Sirenix.Serialization.BaseDataWriter::Dispose()
// 0x000000B6 System.Void Sirenix.Serialization.BaseDataWriter::PrepareNewSerializationSession()
extern void BaseDataWriter_PrepareNewSerializationSession_m4A4300B80406DA36225BB573EB5DAA5B8ACFF5AF (void);
// 0x000000B7 System.String Sirenix.Serialization.BaseDataWriter::GetDataDump()
// 0x000000B8 System.Void Sirenix.Serialization.BinaryDataReader::.ctor()
extern void BinaryDataReader__ctor_m88BDA3685AC0E1DD04743FCE6124CEF860907DC4 (void);
// 0x000000B9 System.Void Sirenix.Serialization.BinaryDataReader::.ctor(System.IO.Stream,Sirenix.Serialization.DeserializationContext)
extern void BinaryDataReader__ctor_mA54276115362DE2094F87655555F2C16B6D700ED (void);
// 0x000000BA System.Void Sirenix.Serialization.BinaryDataReader::Dispose()
extern void BinaryDataReader_Dispose_mF69B994CA38DAB15683F0F100FF3259D619394AF (void);
// 0x000000BB Sirenix.Serialization.EntryType Sirenix.Serialization.BinaryDataReader::PeekEntry(System.String&)
extern void BinaryDataReader_PeekEntry_mC5862ECD01767106319CFFBE6DB586312F4B0506 (void);
// 0x000000BC System.Boolean Sirenix.Serialization.BinaryDataReader::EnterArray(System.Int64&)
extern void BinaryDataReader_EnterArray_m48587C17EB40B23608F4948A91F48DD2482DC045 (void);
// 0x000000BD System.Boolean Sirenix.Serialization.BinaryDataReader::EnterNode(System.Type&)
extern void BinaryDataReader_EnterNode_mA48929208D4033E4D6C0CE6E2CB3E4FB552B931A (void);
// 0x000000BE System.Boolean Sirenix.Serialization.BinaryDataReader::ExitArray()
extern void BinaryDataReader_ExitArray_m855C533A72BAEC120B75061961C877424CC47799 (void);
// 0x000000BF System.Boolean Sirenix.Serialization.BinaryDataReader::ExitNode()
extern void BinaryDataReader_ExitNode_m30BB0D2249C04057D5B4A104E02124ABBB7C8CEF (void);
// 0x000000C0 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadPrimitiveArray(T[]&)
// 0x000000C1 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadBoolean(System.Boolean&)
extern void BinaryDataReader_ReadBoolean_m378DD919E3EC7DA4A737AFCFC4C7115E01B82754 (void);
// 0x000000C2 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadSByte(System.SByte&)
extern void BinaryDataReader_ReadSByte_m8CA3AC056171D787B05E6F566DE23F2492BECF6A (void);
// 0x000000C3 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadByte(System.Byte&)
extern void BinaryDataReader_ReadByte_m717140B880C6F20954E9BAF4A14446B8EB86C8D1 (void);
// 0x000000C4 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadInt16(System.Int16&)
extern void BinaryDataReader_ReadInt16_mD731B8F7C26F2B8EB0BAC580B3B505EA480C1267 (void);
// 0x000000C5 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadUInt16(System.UInt16&)
extern void BinaryDataReader_ReadUInt16_mB0BEC5B34F9455C89DB0A011D8A7E0D423C04E7E (void);
// 0x000000C6 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadInt32(System.Int32&)
extern void BinaryDataReader_ReadInt32_m9D58C783DE8979DE5BDC67DBD4DC06FAA35D6CCB (void);
// 0x000000C7 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadUInt32(System.UInt32&)
extern void BinaryDataReader_ReadUInt32_mBB7C1EA245934C81694607D961F06141405236B5 (void);
// 0x000000C8 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadInt64(System.Int64&)
extern void BinaryDataReader_ReadInt64_mFF654267D550E14904877F1BBEA34E9F7DE10989 (void);
// 0x000000C9 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadUInt64(System.UInt64&)
extern void BinaryDataReader_ReadUInt64_m81C63D46C961A0EDE4D52EC2CFD61DE9ED86501F (void);
// 0x000000CA System.Boolean Sirenix.Serialization.BinaryDataReader::ReadChar(System.Char&)
extern void BinaryDataReader_ReadChar_m2F4EA4ED76C3F064EFD96843C28EB6A59DEE541A (void);
// 0x000000CB System.Boolean Sirenix.Serialization.BinaryDataReader::ReadSingle(System.Single&)
extern void BinaryDataReader_ReadSingle_m5999034B728C4904CB5C8AB5CE52A294C3A6CAA6 (void);
// 0x000000CC System.Boolean Sirenix.Serialization.BinaryDataReader::ReadDouble(System.Double&)
extern void BinaryDataReader_ReadDouble_m8C36ADEF111EFFD7957364890E53704747AD3BF7 (void);
// 0x000000CD System.Boolean Sirenix.Serialization.BinaryDataReader::ReadDecimal(System.Decimal&)
extern void BinaryDataReader_ReadDecimal_m10D99EC0CDAF58960FA91EC8F1DBEB82964F5447 (void);
// 0x000000CE System.Boolean Sirenix.Serialization.BinaryDataReader::ReadExternalReference(System.Guid&)
extern void BinaryDataReader_ReadExternalReference_m7F3777D3B3B6670E0B712180BB255F2CDCD48159 (void);
// 0x000000CF System.Boolean Sirenix.Serialization.BinaryDataReader::ReadGuid(System.Guid&)
extern void BinaryDataReader_ReadGuid_m8890FE37F616CDAFE09DE989833A9EE72B7A497B (void);
// 0x000000D0 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadExternalReference(System.Int32&)
extern void BinaryDataReader_ReadExternalReference_m95E0EED00EB14609FDAF9D4DF9C4BB19B09F357F (void);
// 0x000000D1 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadExternalReference(System.String&)
extern void BinaryDataReader_ReadExternalReference_mAE6D5EFEDF45424916199809C711A715B26B359C (void);
// 0x000000D2 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadNull()
extern void BinaryDataReader_ReadNull_m4B12D1FAA47604EE85BC27F6AF3121AC117EA320 (void);
// 0x000000D3 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadInternalReference(System.Int32&)
extern void BinaryDataReader_ReadInternalReference_m56A9489CE8F1914CBA36300D7E3077AB22B29763 (void);
// 0x000000D4 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadString(System.String&)
extern void BinaryDataReader_ReadString_m04C5E6B35AA8ABB787F0E2E60B96D709BE8E3E83 (void);
// 0x000000D5 System.Void Sirenix.Serialization.BinaryDataReader::PrepareNewSerializationSession()
extern void BinaryDataReader_PrepareNewSerializationSession_m04C7EE2EDA448E61111A065FB7AE2D86CD9BDC2C (void);
// 0x000000D6 System.String Sirenix.Serialization.BinaryDataReader::GetDataDump()
extern void BinaryDataReader_GetDataDump_m53AF7D5E535DC17312B4CE9F37039980271981C4 (void);
// 0x000000D7 System.String Sirenix.Serialization.BinaryDataReader::ReadStringValue()
extern void BinaryDataReader_ReadStringValue_m0A03F2E87E2780551B54AB77442C6BCF3C66BE93 (void);
// 0x000000D8 System.Void Sirenix.Serialization.BinaryDataReader::SkipStringValue()
extern void BinaryDataReader_SkipStringValue_m0F019C755739EB16E03C6537CCF854ED6645F631 (void);
// 0x000000D9 System.Void Sirenix.Serialization.BinaryDataReader::SkipPeekedEntryContent()
extern void BinaryDataReader_SkipPeekedEntryContent_mFAB5708C3E9E82D2230F77839F3AA6C1E330EC88 (void);
// 0x000000DA System.Boolean Sirenix.Serialization.BinaryDataReader::SkipBuffer(System.Int32)
extern void BinaryDataReader_SkipBuffer_m2A54992EC4C254F45B4D9ABD3E13BA433021F4D2 (void);
// 0x000000DB System.Type Sirenix.Serialization.BinaryDataReader::ReadTypeEntry()
extern void BinaryDataReader_ReadTypeEntry_m21824269F79E19F784D1127D633AD608D96BD458 (void);
// 0x000000DC System.Void Sirenix.Serialization.BinaryDataReader::MarkEntryContentConsumed()
extern void BinaryDataReader_MarkEntryContentConsumed_mA52A41349AAB73BF7D8FBA14E9CD01F3FA9641C4 (void);
// 0x000000DD Sirenix.Serialization.EntryType Sirenix.Serialization.BinaryDataReader::PeekEntry()
extern void BinaryDataReader_PeekEntry_m19C08500083F4A090AD0A9228EA5EB4BD4565B7F (void);
// 0x000000DE Sirenix.Serialization.EntryType Sirenix.Serialization.BinaryDataReader::ReadToNextEntry()
extern void BinaryDataReader_ReadToNextEntry_mB84D8F3F979328BDBE8FB313A7BA8958CB89A51C (void);
// 0x000000DF System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_1_Byte(System.Byte&)
extern void BinaryDataReader_UNSAFE_Read_1_Byte_m6F2977F3D5EC300F18455C8DFA9E142395194E20 (void);
// 0x000000E0 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_1_SByte(System.SByte&)
extern void BinaryDataReader_UNSAFE_Read_1_SByte_m1025D18AB76FF1B908ABCC6840B7446E8FA5C869 (void);
// 0x000000E1 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_2_Int16(System.Int16&)
extern void BinaryDataReader_UNSAFE_Read_2_Int16_m8852115FD75E07626CB464B3334498C42B64B768 (void);
// 0x000000E2 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_2_UInt16(System.UInt16&)
extern void BinaryDataReader_UNSAFE_Read_2_UInt16_m8A30CE13CE56F91677C9965106BE0C5BDFFED9CC (void);
// 0x000000E3 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_2_Char(System.Char&)
extern void BinaryDataReader_UNSAFE_Read_2_Char_m1879706F390A93F5C3D993A395234BC088A4923F (void);
// 0x000000E4 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_4_Int32(System.Int32&)
extern void BinaryDataReader_UNSAFE_Read_4_Int32_mC955D7CF98B415A80F7C10A79BF13C9A29385FE5 (void);
// 0x000000E5 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_4_UInt32(System.UInt32&)
extern void BinaryDataReader_UNSAFE_Read_4_UInt32_mE81BE7436BAE1B93F9044473C7691ED0EDFF15CF (void);
// 0x000000E6 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_4_Float32(System.Single&)
extern void BinaryDataReader_UNSAFE_Read_4_Float32_mE61F5BF60D34409BD360DDFA16EF8BCAC3FB1AD6 (void);
// 0x000000E7 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_8_Int64(System.Int64&)
extern void BinaryDataReader_UNSAFE_Read_8_Int64_m5BA01533C4C06169BDFF70B676FC1F7BAE70E61A (void);
// 0x000000E8 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_8_UInt64(System.UInt64&)
extern void BinaryDataReader_UNSAFE_Read_8_UInt64_m68DD28FB4A0661326C754BA1837AE65028E30465 (void);
// 0x000000E9 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_8_Float64(System.Double&)
extern void BinaryDataReader_UNSAFE_Read_8_Float64_m53A76610530D9A77CD4A62AC8BCE3F38EA28104E (void);
// 0x000000EA System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_16_Decimal(System.Decimal&)
extern void BinaryDataReader_UNSAFE_Read_16_Decimal_m3C459886DF09B26A00A014CFF2A4B12533F99155 (void);
// 0x000000EB System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_16_Guid(System.Guid&)
extern void BinaryDataReader_UNSAFE_Read_16_Guid_mA6D4CDD78406EE2677B9C51CF06D3501EDFC01E3 (void);
// 0x000000EC System.Boolean Sirenix.Serialization.BinaryDataReader::HasBufferData(System.Int32)
extern void BinaryDataReader_HasBufferData_mF2D7D3FE10A13C08E0DAE8CB54C0038CE5F45460 (void);
// 0x000000ED System.Void Sirenix.Serialization.BinaryDataReader::ReadEntireStreamToBuffer()
extern void BinaryDataReader_ReadEntireStreamToBuffer_mBB7516FFB0EA9375B63884D67E29E999FB84B6B6 (void);
// 0x000000EE System.Void Sirenix.Serialization.BinaryDataReader::.cctor()
extern void BinaryDataReader__cctor_m28D6C7017F48C534C1D9D62F78AB2F8DF84DE915 (void);
// 0x000000EF System.Void Sirenix.Serialization.BinaryDataReader/<>c::.cctor()
extern void U3CU3Ec__cctor_m050BFA0A1B61314139EAB2B60E18F0D0AD50EE21 (void);
// 0x000000F0 System.Void Sirenix.Serialization.BinaryDataReader/<>c::.ctor()
extern void U3CU3Ec__ctor_m9A50009B93D606DDE91E49A7B7914F70E04060B0 (void);
// 0x000000F1 System.Char Sirenix.Serialization.BinaryDataReader/<>c::<.cctor>b__64_0(System.Byte[],System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__64_0_mDBB1666D139D83D5D2A8EF76E9F9F98AB9432361 (void);
// 0x000000F2 System.Byte Sirenix.Serialization.BinaryDataReader/<>c::<.cctor>b__64_1(System.Byte[],System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__64_1_m060DE417178FA176BA354EF44DD3FFC5C6A4B3F3 (void);
// 0x000000F3 System.SByte Sirenix.Serialization.BinaryDataReader/<>c::<.cctor>b__64_2(System.Byte[],System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__64_2_mA65FEF70769F8328F81E6D5B7E686FD52D22B7FB (void);
// 0x000000F4 System.Boolean Sirenix.Serialization.BinaryDataReader/<>c::<.cctor>b__64_3(System.Byte[],System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__64_3_mED4EBA4FAF918D29FFD28DD53CE4E6CB61DF360E (void);
// 0x000000F5 System.Void Sirenix.Serialization.BinaryDataWriter::.ctor()
extern void BinaryDataWriter__ctor_mE8C0E6AC4D94E20B6FC4620B59957B92C109D160 (void);
// 0x000000F6 System.Void Sirenix.Serialization.BinaryDataWriter::.ctor(System.IO.Stream,Sirenix.Serialization.SerializationContext)
extern void BinaryDataWriter__ctor_mB403169F6A5ED63B1F9F06BFBEDC68265171EF7D (void);
// 0x000000F7 System.Void Sirenix.Serialization.BinaryDataWriter::BeginArrayNode(System.Int64)
extern void BinaryDataWriter_BeginArrayNode_m23869E8F7D602BB54675E3567EF4D1E8963BE2E1 (void);
// 0x000000F8 System.Void Sirenix.Serialization.BinaryDataWriter::BeginReferenceNode(System.String,System.Type,System.Int32)
extern void BinaryDataWriter_BeginReferenceNode_m15A62F582613361E92CE828E9D1B396D2AB3FD7F (void);
// 0x000000F9 System.Void Sirenix.Serialization.BinaryDataWriter::BeginStructNode(System.String,System.Type)
extern void BinaryDataWriter_BeginStructNode_m744D3776D2AAE2862DA7D9A7C5A0138F600EFA34 (void);
// 0x000000FA System.Void Sirenix.Serialization.BinaryDataWriter::Dispose()
extern void BinaryDataWriter_Dispose_mFBC9CB175CB2505509760EB1664C37015B0C275E (void);
// 0x000000FB System.Void Sirenix.Serialization.BinaryDataWriter::EndArrayNode()
extern void BinaryDataWriter_EndArrayNode_mFA754130106E5188BD185A77E9501D273662CA9D (void);
// 0x000000FC System.Void Sirenix.Serialization.BinaryDataWriter::EndNode(System.String)
extern void BinaryDataWriter_EndNode_m7B30D2172A2355A13999394A99537725E7A8B98C (void);
// 0x000000FD System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_byte(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_byte_mFDCC11AFD0D132EFD5788EF0890DA862666A1A19 (void);
// 0x000000FE System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_sbyte(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_sbyte_m906CAE2C80F702A4718C4016EFD7B9AF04C5F6F0 (void);
// 0x000000FF System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_bool(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_bool_mD59501EBFC7A430EC31F667793F3D10DE8D2A28C (void);
// 0x00000100 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_char(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_char_m36B885F62D556EB9626C0346B31677261064281E (void);
// 0x00000101 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_short(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_short_m50012F6BCB8F48FE5DA7A0ED47BB98D86FB98FBE (void);
// 0x00000102 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_int(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_int_mD5DEAF8CD69CDE1A8785199525A2120C59606990 (void);
// 0x00000103 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_long(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_long_m2307A336E4AFC94668E461C7A6765A01CE58377A (void);
// 0x00000104 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_ushort(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_ushort_m757100432861A9E6CCB587B9F8FAECC6BAE72950 (void);
// 0x00000105 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_uint(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_uint_mDC712382B05E25F10E9AC6D18472ED59E8C57D85 (void);
// 0x00000106 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_ulong(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_ulong_m71B2C1D8FC07B70BB8FEFB59FD7F30C4CCFAF7F7 (void);
// 0x00000107 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_decimal(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_decimal_mA31C45A78D35C185DA0DDAB136DD1CB1F124E8D1 (void);
// 0x00000108 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_float(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_float_m0F539C302A6A4DAE91E23322787B82CB2EDB6BBB (void);
// 0x00000109 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_double(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_double_m7CAD7CC652517C73A535D295A7DCBA9F84B61A7A (void);
// 0x0000010A System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_Guid(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_Guid_m602B4528DEFE6A93200FE5080755DCCEA90F093B (void);
// 0x0000010B System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray(T[])
// 0x0000010C System.Void Sirenix.Serialization.BinaryDataWriter::WriteBoolean(System.String,System.Boolean)
extern void BinaryDataWriter_WriteBoolean_mE4F47CADF47BB4C9BC9CDA17777E49760C26EA25 (void);
// 0x0000010D System.Void Sirenix.Serialization.BinaryDataWriter::WriteByte(System.String,System.Byte)
extern void BinaryDataWriter_WriteByte_m28B86E020EA166D63EB9DD397E30B576E4F389F2 (void);
// 0x0000010E System.Void Sirenix.Serialization.BinaryDataWriter::WriteChar(System.String,System.Char)
extern void BinaryDataWriter_WriteChar_mBBDA7DC85DC51BDACC02320E3E6FCD676456605E (void);
// 0x0000010F System.Void Sirenix.Serialization.BinaryDataWriter::WriteDecimal(System.String,System.Decimal)
extern void BinaryDataWriter_WriteDecimal_m10C48A79429750F0AA8F39E19BACF438ED6B019F (void);
// 0x00000110 System.Void Sirenix.Serialization.BinaryDataWriter::WriteDouble(System.String,System.Double)
extern void BinaryDataWriter_WriteDouble_m0F4842E0AF1644B5A5850683414C206471975B12 (void);
// 0x00000111 System.Void Sirenix.Serialization.BinaryDataWriter::WriteGuid(System.String,System.Guid)
extern void BinaryDataWriter_WriteGuid_m3EFCA164A29E0C047375EECFF54529F4756FF273 (void);
// 0x00000112 System.Void Sirenix.Serialization.BinaryDataWriter::WriteExternalReference(System.String,System.Guid)
extern void BinaryDataWriter_WriteExternalReference_mD8C5A65AC1400A468C5ADF4F547C3FBFCA8862AA (void);
// 0x00000113 System.Void Sirenix.Serialization.BinaryDataWriter::WriteExternalReference(System.String,System.Int32)
extern void BinaryDataWriter_WriteExternalReference_m73C00B1DA58F822A2865CDD1D81D0160A0E64269 (void);
// 0x00000114 System.Void Sirenix.Serialization.BinaryDataWriter::WriteExternalReference(System.String,System.String)
extern void BinaryDataWriter_WriteExternalReference_m1ED2E8E514238BB55086D8E2D81A2245820F02AC (void);
// 0x00000115 System.Void Sirenix.Serialization.BinaryDataWriter::WriteInt32(System.String,System.Int32)
extern void BinaryDataWriter_WriteInt32_m0DBB8C151A9A1907BA367A8DC5499BA58AF45BAD (void);
// 0x00000116 System.Void Sirenix.Serialization.BinaryDataWriter::WriteInt64(System.String,System.Int64)
extern void BinaryDataWriter_WriteInt64_m199CC8179ECAE4A86DE3931DAD5E1B1DC22FF023 (void);
// 0x00000117 System.Void Sirenix.Serialization.BinaryDataWriter::WriteNull(System.String)
extern void BinaryDataWriter_WriteNull_m11E857323D675AF78742D373320DD9F44655FD85 (void);
// 0x00000118 System.Void Sirenix.Serialization.BinaryDataWriter::WriteInternalReference(System.String,System.Int32)
extern void BinaryDataWriter_WriteInternalReference_m5A0F17550F718686D7A579F837931555C8732FBC (void);
// 0x00000119 System.Void Sirenix.Serialization.BinaryDataWriter::WriteSByte(System.String,System.SByte)
extern void BinaryDataWriter_WriteSByte_m02865034A074CBBB9AD551EF755BEA10AC7DAE3D (void);
// 0x0000011A System.Void Sirenix.Serialization.BinaryDataWriter::WriteInt16(System.String,System.Int16)
extern void BinaryDataWriter_WriteInt16_m28B49BE97E8ED08A61659A0FAB38E56F834EF893 (void);
// 0x0000011B System.Void Sirenix.Serialization.BinaryDataWriter::WriteSingle(System.String,System.Single)
extern void BinaryDataWriter_WriteSingle_m5E082D79A06AF25D663E91A344820B96FE9AE027 (void);
// 0x0000011C System.Void Sirenix.Serialization.BinaryDataWriter::WriteString(System.String,System.String)
extern void BinaryDataWriter_WriteString_mBFB41B7E7EC332AF6FB46176755511BD26361B12 (void);
// 0x0000011D System.Void Sirenix.Serialization.BinaryDataWriter::WriteUInt32(System.String,System.UInt32)
extern void BinaryDataWriter_WriteUInt32_m04322A37668C5763F647232FFDD8C95231E26B90 (void);
// 0x0000011E System.Void Sirenix.Serialization.BinaryDataWriter::WriteUInt64(System.String,System.UInt64)
extern void BinaryDataWriter_WriteUInt64_m74D99C92CA49B22C712D7FB21F3BB2141F9DB626 (void);
// 0x0000011F System.Void Sirenix.Serialization.BinaryDataWriter::WriteUInt16(System.String,System.UInt16)
extern void BinaryDataWriter_WriteUInt16_m455076A8351E7B47DA0B36C2D947B73BBC378A8D (void);
// 0x00000120 System.Void Sirenix.Serialization.BinaryDataWriter::PrepareNewSerializationSession()
extern void BinaryDataWriter_PrepareNewSerializationSession_mFD048092F817B76F249BBD87081874EB9A7CC46A (void);
// 0x00000121 System.String Sirenix.Serialization.BinaryDataWriter::GetDataDump()
extern void BinaryDataWriter_GetDataDump_m3C0B816BC246B9A4FCA5B826B6274068F2281E8D (void);
// 0x00000122 System.Void Sirenix.Serialization.BinaryDataWriter::WriteType(System.Type)
extern void BinaryDataWriter_WriteType_mFB9BB191A388161EA65B990AF79F9975BACC6284 (void);
// 0x00000123 System.Void Sirenix.Serialization.BinaryDataWriter::WriteStringFast(System.String)
extern void BinaryDataWriter_WriteStringFast_mA0E5556D7EA59D494AD4967DA6D65E95364E15B7 (void);
// 0x00000124 System.Void Sirenix.Serialization.BinaryDataWriter::FlushToStream()
extern void BinaryDataWriter_FlushToStream_m704AF9F94D9F9678372A72A8A2A242CA67EB30FE (void);
// 0x00000125 System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_2_Char(System.Char)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_2_Char_mB8C8663F2F41F25AF90A86D6CEFDD499387C2816 (void);
// 0x00000126 System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_2_Int16(System.Int16)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_2_Int16_mC5A1DFDB026AEE62256DF07DF4B7DC2CBC0B9C40 (void);
// 0x00000127 System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_2_UInt16(System.UInt16)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_2_UInt16_m60F01963AE907E760341B2402CACD32F9B0FAA9D (void);
// 0x00000128 System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_4_Int32(System.Int32)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_4_Int32_m751BF6A1EB0E01201FBBA7A585075A0576715ADA (void);
// 0x00000129 System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_4_UInt32(System.UInt32)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_4_UInt32_m554F134FD72A23FF99350E55E54241A41A60C106 (void);
// 0x0000012A System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_4_Float32(System.Single)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_4_Float32_mEFCAB799A534ED25F8E7F581435321EC80E9ECE4 (void);
// 0x0000012B System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_8_Int64(System.Int64)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_8_Int64_m37B06348CFE8B65DE77182168B88BBEA1A2C59FE (void);
// 0x0000012C System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_8_UInt64(System.UInt64)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_8_UInt64_mFB413BC7BEC0EF0BBBD2E8A9BB0341598D1CA10F (void);
// 0x0000012D System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_8_Float64(System.Double)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_8_Float64_m75DDAEE21A9BEC51333499CBD925866734990FC0 (void);
// 0x0000012E System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_16_Decimal(System.Decimal)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_16_Decimal_mE782B0B183F85E133D345F118C6ABE808E5E7288 (void);
// 0x0000012F System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_16_Guid(System.Guid)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_16_Guid_m4DE7671E9608F3809511664F7C3F88F4772977D2 (void);
// 0x00000130 System.Void Sirenix.Serialization.BinaryDataWriter::EnsureBufferSpace(System.Int32)
extern void BinaryDataWriter_EnsureBufferSpace_m04C68A2D4BC64BFBAA12F526B048A728D36C1D2E (void);
// 0x00000131 System.Boolean Sirenix.Serialization.BinaryDataWriter::TryEnsureBufferSpace(System.Int32)
extern void BinaryDataWriter_TryEnsureBufferSpace_mAE324967BD3EB8D7BCD831F77EBEC81E755259E8 (void);
// 0x00000132 System.Void Sirenix.Serialization.BinaryDataWriter::.cctor()
extern void BinaryDataWriter__cctor_mFCA240934A8DB0FA28B16F2FE405151F6ABACA51 (void);
// 0x00000133 System.Void Sirenix.Serialization.BinaryDataWriter/<>c::.cctor()
extern void U3CU3Ec__cctor_m10BA3D1F6C8CE172D519D38FE640A7019E6BA949 (void);
// 0x00000134 System.Void Sirenix.Serialization.BinaryDataWriter/<>c::.ctor()
extern void U3CU3Ec__ctor_mE6CAD2B866ECCCD71ABEFEAF9824EE51516F1CFD (void);
// 0x00000135 System.Void Sirenix.Serialization.BinaryDataWriter/<>c::<.cctor>b__70_0(System.Byte[],System.Int32,System.Char)
extern void U3CU3Ec_U3C_cctorU3Eb__70_0_m9394A9921C45735E1774EDEE178E280DB947EF5D (void);
// 0x00000136 System.Void Sirenix.Serialization.BinaryDataWriter/<>c::<.cctor>b__70_1(System.Byte[],System.Int32,System.Byte)
extern void U3CU3Ec_U3C_cctorU3Eb__70_1_mE4F40DD3FE93F0AC5DB34762C79186001CB40351 (void);
// 0x00000137 System.Void Sirenix.Serialization.BinaryDataWriter/<>c::<.cctor>b__70_2(System.Byte[],System.Int32,System.SByte)
extern void U3CU3Ec_U3C_cctorU3Eb__70_2_mDA53FB238C9092FF6070C148C39DEAD8BF4CAF20 (void);
// 0x00000138 System.Void Sirenix.Serialization.BinaryDataWriter/<>c::<.cctor>b__70_3(System.Byte[],System.Int32,System.Boolean)
extern void U3CU3Ec_U3C_cctorU3Eb__70_3_m218080691DA514E35F7E9F6F421B0757FF7793E1 (void);
// 0x00000139 Sirenix.Serialization.TwoWaySerializationBinder Sirenix.Serialization.IDataReader::get_Binder()
// 0x0000013A System.Void Sirenix.Serialization.IDataReader::set_Binder(Sirenix.Serialization.TwoWaySerializationBinder)
// 0x0000013B System.IO.Stream Sirenix.Serialization.IDataReader::get_Stream()
// 0x0000013C System.Void Sirenix.Serialization.IDataReader::set_Stream(System.IO.Stream)
// 0x0000013D System.Boolean Sirenix.Serialization.IDataReader::get_IsInArrayNode()
// 0x0000013E System.String Sirenix.Serialization.IDataReader::get_CurrentNodeName()
// 0x0000013F System.Int32 Sirenix.Serialization.IDataReader::get_CurrentNodeId()
// 0x00000140 System.Int32 Sirenix.Serialization.IDataReader::get_CurrentNodeDepth()
// 0x00000141 Sirenix.Serialization.DeserializationContext Sirenix.Serialization.IDataReader::get_Context()
// 0x00000142 System.Void Sirenix.Serialization.IDataReader::set_Context(Sirenix.Serialization.DeserializationContext)
// 0x00000143 System.String Sirenix.Serialization.IDataReader::GetDataDump()
// 0x00000144 System.Boolean Sirenix.Serialization.IDataReader::EnterNode(System.Type&)
// 0x00000145 System.Boolean Sirenix.Serialization.IDataReader::ExitNode()
// 0x00000146 System.Boolean Sirenix.Serialization.IDataReader::EnterArray(System.Int64&)
// 0x00000147 System.Boolean Sirenix.Serialization.IDataReader::ExitArray()
// 0x00000148 System.Boolean Sirenix.Serialization.IDataReader::ReadPrimitiveArray(T[]&)
// 0x00000149 Sirenix.Serialization.EntryType Sirenix.Serialization.IDataReader::PeekEntry(System.String&)
// 0x0000014A System.Boolean Sirenix.Serialization.IDataReader::ReadInternalReference(System.Int32&)
// 0x0000014B System.Boolean Sirenix.Serialization.IDataReader::ReadExternalReference(System.Int32&)
// 0x0000014C System.Boolean Sirenix.Serialization.IDataReader::ReadExternalReference(System.Guid&)
// 0x0000014D System.Boolean Sirenix.Serialization.IDataReader::ReadExternalReference(System.String&)
// 0x0000014E System.Boolean Sirenix.Serialization.IDataReader::ReadChar(System.Char&)
// 0x0000014F System.Boolean Sirenix.Serialization.IDataReader::ReadString(System.String&)
// 0x00000150 System.Boolean Sirenix.Serialization.IDataReader::ReadGuid(System.Guid&)
// 0x00000151 System.Boolean Sirenix.Serialization.IDataReader::ReadSByte(System.SByte&)
// 0x00000152 System.Boolean Sirenix.Serialization.IDataReader::ReadInt16(System.Int16&)
// 0x00000153 System.Boolean Sirenix.Serialization.IDataReader::ReadInt32(System.Int32&)
// 0x00000154 System.Boolean Sirenix.Serialization.IDataReader::ReadInt64(System.Int64&)
// 0x00000155 System.Boolean Sirenix.Serialization.IDataReader::ReadByte(System.Byte&)
// 0x00000156 System.Boolean Sirenix.Serialization.IDataReader::ReadUInt16(System.UInt16&)
// 0x00000157 System.Boolean Sirenix.Serialization.IDataReader::ReadUInt32(System.UInt32&)
// 0x00000158 System.Boolean Sirenix.Serialization.IDataReader::ReadUInt64(System.UInt64&)
// 0x00000159 System.Boolean Sirenix.Serialization.IDataReader::ReadDecimal(System.Decimal&)
// 0x0000015A System.Boolean Sirenix.Serialization.IDataReader::ReadSingle(System.Single&)
// 0x0000015B System.Boolean Sirenix.Serialization.IDataReader::ReadDouble(System.Double&)
// 0x0000015C System.Boolean Sirenix.Serialization.IDataReader::ReadBoolean(System.Boolean&)
// 0x0000015D System.Boolean Sirenix.Serialization.IDataReader::ReadNull()
// 0x0000015E System.Void Sirenix.Serialization.IDataReader::SkipEntry()
// 0x0000015F System.Void Sirenix.Serialization.IDataReader::PrepareNewSerializationSession()
// 0x00000160 Sirenix.Serialization.TwoWaySerializationBinder Sirenix.Serialization.IDataWriter::get_Binder()
// 0x00000161 System.Void Sirenix.Serialization.IDataWriter::set_Binder(Sirenix.Serialization.TwoWaySerializationBinder)
// 0x00000162 System.IO.Stream Sirenix.Serialization.IDataWriter::get_Stream()
// 0x00000163 System.Void Sirenix.Serialization.IDataWriter::set_Stream(System.IO.Stream)
// 0x00000164 System.Boolean Sirenix.Serialization.IDataWriter::get_IsInArrayNode()
// 0x00000165 Sirenix.Serialization.SerializationContext Sirenix.Serialization.IDataWriter::get_Context()
// 0x00000166 System.Void Sirenix.Serialization.IDataWriter::set_Context(Sirenix.Serialization.SerializationContext)
// 0x00000167 System.String Sirenix.Serialization.IDataWriter::GetDataDump()
// 0x00000168 System.Void Sirenix.Serialization.IDataWriter::FlushToStream()
// 0x00000169 System.Void Sirenix.Serialization.IDataWriter::BeginReferenceNode(System.String,System.Type,System.Int32)
// 0x0000016A System.Void Sirenix.Serialization.IDataWriter::BeginStructNode(System.String,System.Type)
// 0x0000016B System.Void Sirenix.Serialization.IDataWriter::EndNode(System.String)
// 0x0000016C System.Void Sirenix.Serialization.IDataWriter::BeginArrayNode(System.Int64)
// 0x0000016D System.Void Sirenix.Serialization.IDataWriter::EndArrayNode()
// 0x0000016E System.Void Sirenix.Serialization.IDataWriter::WritePrimitiveArray(T[])
// 0x0000016F System.Void Sirenix.Serialization.IDataWriter::WriteNull(System.String)
// 0x00000170 System.Void Sirenix.Serialization.IDataWriter::WriteInternalReference(System.String,System.Int32)
// 0x00000171 System.Void Sirenix.Serialization.IDataWriter::WriteExternalReference(System.String,System.Int32)
// 0x00000172 System.Void Sirenix.Serialization.IDataWriter::WriteExternalReference(System.String,System.Guid)
// 0x00000173 System.Void Sirenix.Serialization.IDataWriter::WriteExternalReference(System.String,System.String)
// 0x00000174 System.Void Sirenix.Serialization.IDataWriter::WriteChar(System.String,System.Char)
// 0x00000175 System.Void Sirenix.Serialization.IDataWriter::WriteString(System.String,System.String)
// 0x00000176 System.Void Sirenix.Serialization.IDataWriter::WriteGuid(System.String,System.Guid)
// 0x00000177 System.Void Sirenix.Serialization.IDataWriter::WriteSByte(System.String,System.SByte)
// 0x00000178 System.Void Sirenix.Serialization.IDataWriter::WriteInt16(System.String,System.Int16)
// 0x00000179 System.Void Sirenix.Serialization.IDataWriter::WriteInt32(System.String,System.Int32)
// 0x0000017A System.Void Sirenix.Serialization.IDataWriter::WriteInt64(System.String,System.Int64)
// 0x0000017B System.Void Sirenix.Serialization.IDataWriter::WriteByte(System.String,System.Byte)
// 0x0000017C System.Void Sirenix.Serialization.IDataWriter::WriteUInt16(System.String,System.UInt16)
// 0x0000017D System.Void Sirenix.Serialization.IDataWriter::WriteUInt32(System.String,System.UInt32)
// 0x0000017E System.Void Sirenix.Serialization.IDataWriter::WriteUInt64(System.String,System.UInt64)
// 0x0000017F System.Void Sirenix.Serialization.IDataWriter::WriteDecimal(System.String,System.Decimal)
// 0x00000180 System.Void Sirenix.Serialization.IDataWriter::WriteSingle(System.String,System.Single)
// 0x00000181 System.Void Sirenix.Serialization.IDataWriter::WriteDouble(System.String,System.Double)
// 0x00000182 System.Void Sirenix.Serialization.IDataWriter::WriteBoolean(System.String,System.Boolean)
// 0x00000183 System.Void Sirenix.Serialization.IDataWriter::PrepareNewSerializationSession()
// 0x00000184 System.Void Sirenix.Serialization.JsonDataReader::.ctor()
extern void JsonDataReader__ctor_m51DA9B5CE4761257B673E9FC1B8F659BADEFEFE2 (void);
// 0x00000185 System.Void Sirenix.Serialization.JsonDataReader::.ctor(System.IO.Stream,Sirenix.Serialization.DeserializationContext)
extern void JsonDataReader__ctor_m3D341A1F756D00056804E6BBF16A23BC8D0FF5D9 (void);
// 0x00000186 System.IO.Stream Sirenix.Serialization.JsonDataReader::get_Stream()
extern void JsonDataReader_get_Stream_m1FD8B0A211CA90F1EB5ECA314ACB964FD203046B (void);
// 0x00000187 System.Void Sirenix.Serialization.JsonDataReader::set_Stream(System.IO.Stream)
extern void JsonDataReader_set_Stream_m104956178A2E2D80870D1DD651A736E7A6B671D1 (void);
// 0x00000188 System.Void Sirenix.Serialization.JsonDataReader::Dispose()
extern void JsonDataReader_Dispose_m121B0C2D97AC16D65131D2E634259304AF8B9F4F (void);
// 0x00000189 Sirenix.Serialization.EntryType Sirenix.Serialization.JsonDataReader::PeekEntry(System.String&)
extern void JsonDataReader_PeekEntry_mFFA0E512EC9D3BECBD1FEDC1E73D943B85403925 (void);
// 0x0000018A System.Boolean Sirenix.Serialization.JsonDataReader::EnterNode(System.Type&)
extern void JsonDataReader_EnterNode_m89CE444FCBB254BAB383DBF7DF481E3B7E772C76 (void);
// 0x0000018B System.Boolean Sirenix.Serialization.JsonDataReader::ExitNode()
extern void JsonDataReader_ExitNode_mF41AA3DDC0B39E898A4C491512220D142FEED8E5 (void);
// 0x0000018C System.Boolean Sirenix.Serialization.JsonDataReader::EnterArray(System.Int64&)
extern void JsonDataReader_EnterArray_m3E5DCDF3F3CB27CEED140ECA54CC23FF9929249C (void);
// 0x0000018D System.Boolean Sirenix.Serialization.JsonDataReader::ExitArray()
extern void JsonDataReader_ExitArray_m63576697912B4C4A5E2C4D4CCFB41FDEE14E39F0 (void);
// 0x0000018E System.Boolean Sirenix.Serialization.JsonDataReader::ReadPrimitiveArray(T[]&)
// 0x0000018F System.Boolean Sirenix.Serialization.JsonDataReader::ReadBoolean(System.Boolean&)
extern void JsonDataReader_ReadBoolean_mB3DA30F8AED83AE4F50217AE0B015BEB44BC262F (void);
// 0x00000190 System.Boolean Sirenix.Serialization.JsonDataReader::ReadInternalReference(System.Int32&)
extern void JsonDataReader_ReadInternalReference_mDCAB35DD2C45F3B250F2CD406E5DAC29F3D943A2 (void);
// 0x00000191 System.Boolean Sirenix.Serialization.JsonDataReader::ReadExternalReference(System.Int32&)
extern void JsonDataReader_ReadExternalReference_mBD9A29ACEB825419A3DAA641713FF5E6759AEB13 (void);
// 0x00000192 System.Boolean Sirenix.Serialization.JsonDataReader::ReadExternalReference(System.Guid&)
extern void JsonDataReader_ReadExternalReference_mB0CFF43B82D984854F29334F1A7803CFC6AE3E7A (void);
// 0x00000193 System.Boolean Sirenix.Serialization.JsonDataReader::ReadExternalReference(System.String&)
extern void JsonDataReader_ReadExternalReference_m6EC3A12FE0884F5B1A2686AC8857E547881015B5 (void);
// 0x00000194 System.Boolean Sirenix.Serialization.JsonDataReader::ReadChar(System.Char&)
extern void JsonDataReader_ReadChar_m11B84F63F7E4EBA53E5ECE1887793354B8E0451B (void);
// 0x00000195 System.Boolean Sirenix.Serialization.JsonDataReader::ReadString(System.String&)
extern void JsonDataReader_ReadString_m5FD0D89F41B40922B2E49F6E6DBFAC0664633294 (void);
// 0x00000196 System.Boolean Sirenix.Serialization.JsonDataReader::ReadGuid(System.Guid&)
extern void JsonDataReader_ReadGuid_m1D28006EFC04FE50EC4E2C385CF900B4E755044A (void);
// 0x00000197 System.Boolean Sirenix.Serialization.JsonDataReader::ReadSByte(System.SByte&)
extern void JsonDataReader_ReadSByte_mF66D7B7C8A70D23D5BFCF4B26DA26C8A7C0EF295 (void);
// 0x00000198 System.Boolean Sirenix.Serialization.JsonDataReader::ReadInt16(System.Int16&)
extern void JsonDataReader_ReadInt16_m6D7C4FDD4B499D3C80DB0B164CCC3AEAB3CDC9C0 (void);
// 0x00000199 System.Boolean Sirenix.Serialization.JsonDataReader::ReadInt32(System.Int32&)
extern void JsonDataReader_ReadInt32_mA49B7DE2BC792066AC3F4E2EC48E5F6DE7CB92D1 (void);
// 0x0000019A System.Boolean Sirenix.Serialization.JsonDataReader::ReadInt64(System.Int64&)
extern void JsonDataReader_ReadInt64_m2777FD3B0F3A2D076949D65992730BDEB6C1B50B (void);
// 0x0000019B System.Boolean Sirenix.Serialization.JsonDataReader::ReadByte(System.Byte&)
extern void JsonDataReader_ReadByte_mA95BACF8EF6611FF60F729DF4B39119C0EDBD475 (void);
// 0x0000019C System.Boolean Sirenix.Serialization.JsonDataReader::ReadUInt16(System.UInt16&)
extern void JsonDataReader_ReadUInt16_m55DE613E4E22907162B9CF33A656EB8DA366F45F (void);
// 0x0000019D System.Boolean Sirenix.Serialization.JsonDataReader::ReadUInt32(System.UInt32&)
extern void JsonDataReader_ReadUInt32_mCCC149ABBF223832FDD83E4C2467E4625F7227F5 (void);
// 0x0000019E System.Boolean Sirenix.Serialization.JsonDataReader::ReadUInt64(System.UInt64&)
extern void JsonDataReader_ReadUInt64_m2B938DEFF221996467398B71CBD12ED1855F5CEF (void);
// 0x0000019F System.Boolean Sirenix.Serialization.JsonDataReader::ReadDecimal(System.Decimal&)
extern void JsonDataReader_ReadDecimal_mA3993D315AB19A9DEFC5B5F9D4A35846A4E4DDCA (void);
// 0x000001A0 System.Boolean Sirenix.Serialization.JsonDataReader::ReadSingle(System.Single&)
extern void JsonDataReader_ReadSingle_m5D278CD30FC408EFA44E0BECDDDB9BFA82E71493 (void);
// 0x000001A1 System.Boolean Sirenix.Serialization.JsonDataReader::ReadDouble(System.Double&)
extern void JsonDataReader_ReadDouble_m21BB0DF645B3A475F21B2DD80AA610FE87E58EDE (void);
// 0x000001A2 System.Boolean Sirenix.Serialization.JsonDataReader::ReadNull()
extern void JsonDataReader_ReadNull_mB05985D273EEFBEFE244A878763D8BA155B067B6 (void);
// 0x000001A3 System.Void Sirenix.Serialization.JsonDataReader::PrepareNewSerializationSession()
extern void JsonDataReader_PrepareNewSerializationSession_mCE2442C90D330F1BF1F58266F16048AA27421887 (void);
// 0x000001A4 System.String Sirenix.Serialization.JsonDataReader::GetDataDump()
extern void JsonDataReader_GetDataDump_mA3ABCAE101DA2237202785B0FD46E0848B9A57A7 (void);
// 0x000001A5 Sirenix.Serialization.EntryType Sirenix.Serialization.JsonDataReader::PeekEntry()
extern void JsonDataReader_PeekEntry_m3A7C036B300CBFE7F8FD0A3813C5587BD1B46427 (void);
// 0x000001A6 Sirenix.Serialization.EntryType Sirenix.Serialization.JsonDataReader::ReadToNextEntry()
extern void JsonDataReader_ReadToNextEntry_mB13D45332629214020D7DE0878BF929CAA8E95D4 (void);
// 0x000001A7 System.Void Sirenix.Serialization.JsonDataReader::MarkEntryConsumed()
extern void JsonDataReader_MarkEntryConsumed_mB16539F226824B0FB118E90DEDA9085D3660567E (void);
// 0x000001A8 System.Boolean Sirenix.Serialization.JsonDataReader::ReadAnyIntReference(System.Int32&)
extern void JsonDataReader_ReadAnyIntReference_m10B56D83CAB7F45067FEE094696374A95C12E726 (void);
// 0x000001A9 System.Char Sirenix.Serialization.JsonDataReader::<.ctor>b__7_0()
extern void JsonDataReader_U3C_ctorU3Eb__7_0_mE112E5F9DFDA44F2606308B54A5E04218F59A737 (void);
// 0x000001AA System.SByte Sirenix.Serialization.JsonDataReader::<.ctor>b__7_1()
extern void JsonDataReader_U3C_ctorU3Eb__7_1_m5609CA50E804A1EF18853730CB82E802076F3A7C (void);
// 0x000001AB System.Int16 Sirenix.Serialization.JsonDataReader::<.ctor>b__7_2()
extern void JsonDataReader_U3C_ctorU3Eb__7_2_mC4AEA8E022EF968F7930688A9DE52F82AAE9BB97 (void);
// 0x000001AC System.Int32 Sirenix.Serialization.JsonDataReader::<.ctor>b__7_3()
extern void JsonDataReader_U3C_ctorU3Eb__7_3_mC8957C5C49E97F87110FFFA9BCFBF1F74C666531 (void);
// 0x000001AD System.Int64 Sirenix.Serialization.JsonDataReader::<.ctor>b__7_4()
extern void JsonDataReader_U3C_ctorU3Eb__7_4_m464C8FBA48324195572556364DC4F3BC4BBE125C (void);
// 0x000001AE System.Byte Sirenix.Serialization.JsonDataReader::<.ctor>b__7_5()
extern void JsonDataReader_U3C_ctorU3Eb__7_5_m479A5766373A101476E74F395E39373CA7B0A6A5 (void);
// 0x000001AF System.UInt16 Sirenix.Serialization.JsonDataReader::<.ctor>b__7_6()
extern void JsonDataReader_U3C_ctorU3Eb__7_6_m9A5E12F6A64B6B3D2B32EEF86F71929EF8A94216 (void);
// 0x000001B0 System.UInt32 Sirenix.Serialization.JsonDataReader::<.ctor>b__7_7()
extern void JsonDataReader_U3C_ctorU3Eb__7_7_mFB69ECDA3876AA464B186644A0F7EF33331F7E8C (void);
// 0x000001B1 System.UInt64 Sirenix.Serialization.JsonDataReader::<.ctor>b__7_8()
extern void JsonDataReader_U3C_ctorU3Eb__7_8_m75C1B2B2333056CAB76CAE090BE5874D5CC800EB (void);
// 0x000001B2 System.Decimal Sirenix.Serialization.JsonDataReader::<.ctor>b__7_9()
extern void JsonDataReader_U3C_ctorU3Eb__7_9_m9DBD6DDCA3741C2E2BA5B37ADB3176063CB1D719 (void);
// 0x000001B3 System.Boolean Sirenix.Serialization.JsonDataReader::<.ctor>b__7_10()
extern void JsonDataReader_U3C_ctorU3Eb__7_10_m0B170AEDEB2281A9CBDE3C2F6BD1039EE9563425 (void);
// 0x000001B4 System.Single Sirenix.Serialization.JsonDataReader::<.ctor>b__7_11()
extern void JsonDataReader_U3C_ctorU3Eb__7_11_mC6888AE4E54E61E13C806FEF2DF4D668E1D32762 (void);
// 0x000001B5 System.Double Sirenix.Serialization.JsonDataReader::<.ctor>b__7_12()
extern void JsonDataReader_U3C_ctorU3Eb__7_12_mC55185DCF8A9B00CD50747FB19363CCF9F254BAB (void);
// 0x000001B6 System.Guid Sirenix.Serialization.JsonDataReader::<.ctor>b__7_13()
extern void JsonDataReader_U3C_ctorU3Eb__7_13_mFBE6997F4A7B1FD533492AC6C83BC604B8717761 (void);
// 0x000001B7 System.Void Sirenix.Serialization.JsonDataWriter::.ctor()
extern void JsonDataWriter__ctor_m851DEFCEF694742BA30B7673E9C62AB7DE4A3469 (void);
// 0x000001B8 System.Void Sirenix.Serialization.JsonDataWriter::.ctor(System.IO.Stream,Sirenix.Serialization.SerializationContext,System.Boolean)
extern void JsonDataWriter__ctor_m711A85A2B2C05C48E3C5E8663582F1BA62736240 (void);
// 0x000001B9 System.Void Sirenix.Serialization.JsonDataWriter::MarkJustStarted()
extern void JsonDataWriter_MarkJustStarted_m531868F82600199DD5621C42D780572CC3CC390A (void);
// 0x000001BA System.Void Sirenix.Serialization.JsonDataWriter::FlushToStream()
extern void JsonDataWriter_FlushToStream_mB650CA1012ECE6DF8B9CCF9EC411C608EA82987F (void);
// 0x000001BB System.Void Sirenix.Serialization.JsonDataWriter::BeginReferenceNode(System.String,System.Type,System.Int32)
extern void JsonDataWriter_BeginReferenceNode_m2978F64A7BB49FC84EA7D753E9751B39EF853723 (void);
// 0x000001BC System.Void Sirenix.Serialization.JsonDataWriter::BeginStructNode(System.String,System.Type)
extern void JsonDataWriter_BeginStructNode_m8EDF08362CE624456C31511FC4757A9601FCFFCA (void);
// 0x000001BD System.Void Sirenix.Serialization.JsonDataWriter::EndNode(System.String)
extern void JsonDataWriter_EndNode_mD67CF83285D57EF02660BCEA35E14C1AD2499F09 (void);
// 0x000001BE System.Void Sirenix.Serialization.JsonDataWriter::BeginArrayNode(System.Int64)
extern void JsonDataWriter_BeginArrayNode_m5D9A7C3556356B14D925D0B1FB05111488BAB280 (void);
// 0x000001BF System.Void Sirenix.Serialization.JsonDataWriter::EndArrayNode()
extern void JsonDataWriter_EndArrayNode_m5E3E91B46CC2FD8149587AC150EE7E76E9ED1CC7 (void);
// 0x000001C0 System.Void Sirenix.Serialization.JsonDataWriter::WritePrimitiveArray(T[])
// 0x000001C1 System.Void Sirenix.Serialization.JsonDataWriter::WriteBoolean(System.String,System.Boolean)
extern void JsonDataWriter_WriteBoolean_m6D0CFC4989D4CE8FCF0DD49286AB2F52D69C4970 (void);
// 0x000001C2 System.Void Sirenix.Serialization.JsonDataWriter::WriteByte(System.String,System.Byte)
extern void JsonDataWriter_WriteByte_mC499889A0DF48127921FEF283196D840E9D64432 (void);
// 0x000001C3 System.Void Sirenix.Serialization.JsonDataWriter::WriteChar(System.String,System.Char)
extern void JsonDataWriter_WriteChar_m9A89C6BDBB07A9DDD45CA38CF53DDEC0F6375A71 (void);
// 0x000001C4 System.Void Sirenix.Serialization.JsonDataWriter::WriteDecimal(System.String,System.Decimal)
extern void JsonDataWriter_WriteDecimal_m1AF3F0F600A8A1E695ABB536FA9A7CFEB41D56A6 (void);
// 0x000001C5 System.Void Sirenix.Serialization.JsonDataWriter::WriteDouble(System.String,System.Double)
extern void JsonDataWriter_WriteDouble_mA31D3E3A26D77BAC4B6F922660F186C615EC018E (void);
// 0x000001C6 System.Void Sirenix.Serialization.JsonDataWriter::WriteInt32(System.String,System.Int32)
extern void JsonDataWriter_WriteInt32_mC48B12895C77667C50C31372F38159F7FBA09D48 (void);
// 0x000001C7 System.Void Sirenix.Serialization.JsonDataWriter::WriteInt64(System.String,System.Int64)
extern void JsonDataWriter_WriteInt64_mB8C27BC09B68E46B3A434BC71476BBB9483199B2 (void);
// 0x000001C8 System.Void Sirenix.Serialization.JsonDataWriter::WriteNull(System.String)
extern void JsonDataWriter_WriteNull_m35404288EBAB7D5A7E8DCE4E6FBC8DE4D55149D4 (void);
// 0x000001C9 System.Void Sirenix.Serialization.JsonDataWriter::WriteInternalReference(System.String,System.Int32)
extern void JsonDataWriter_WriteInternalReference_mF45FEF6998295E6E5855971E0AF6D9D28D3BB5DD (void);
// 0x000001CA System.Void Sirenix.Serialization.JsonDataWriter::WriteSByte(System.String,System.SByte)
extern void JsonDataWriter_WriteSByte_m67C864251205A6CA76AEC024EDB214C43EA329B4 (void);
// 0x000001CB System.Void Sirenix.Serialization.JsonDataWriter::WriteInt16(System.String,System.Int16)
extern void JsonDataWriter_WriteInt16_mD3F9C7F63522165DABD06F6FA79C84685438A306 (void);
// 0x000001CC System.Void Sirenix.Serialization.JsonDataWriter::WriteSingle(System.String,System.Single)
extern void JsonDataWriter_WriteSingle_m8FB6BD96049EECF56E9D0B320BAE9B33466BD848 (void);
// 0x000001CD System.Void Sirenix.Serialization.JsonDataWriter::WriteString(System.String,System.String)
extern void JsonDataWriter_WriteString_mF82E495FB1E42F3A42CE3FD166DCDD19AF033510 (void);
// 0x000001CE System.Void Sirenix.Serialization.JsonDataWriter::WriteGuid(System.String,System.Guid)
extern void JsonDataWriter_WriteGuid_m32F9E13793B8525EBDED213B387B0D76AF346CF9 (void);
// 0x000001CF System.Void Sirenix.Serialization.JsonDataWriter::WriteUInt32(System.String,System.UInt32)
extern void JsonDataWriter_WriteUInt32_m78712AFC4332BABA2D0A621097C3DB3B2713DC13 (void);
// 0x000001D0 System.Void Sirenix.Serialization.JsonDataWriter::WriteUInt64(System.String,System.UInt64)
extern void JsonDataWriter_WriteUInt64_mB21547576308E8A1C44B6BB8C4AE94EA88580268 (void);
// 0x000001D1 System.Void Sirenix.Serialization.JsonDataWriter::WriteExternalReference(System.String,System.Int32)
extern void JsonDataWriter_WriteExternalReference_mBF8DB8F6A5A0B1626D8D51125B760F7A5AD33746 (void);
// 0x000001D2 System.Void Sirenix.Serialization.JsonDataWriter::WriteExternalReference(System.String,System.Guid)
extern void JsonDataWriter_WriteExternalReference_m72C6B98B22B694370598C62BDFF40C13DF424F9A (void);
// 0x000001D3 System.Void Sirenix.Serialization.JsonDataWriter::WriteExternalReference(System.String,System.String)
extern void JsonDataWriter_WriteExternalReference_m45430BFA1F2F59ED2E05DD5F44C24D5F8D8B0230 (void);
// 0x000001D4 System.Void Sirenix.Serialization.JsonDataWriter::WriteUInt16(System.String,System.UInt16)
extern void JsonDataWriter_WriteUInt16_m781B5D8E4454BD8870FFB6ECED811DB45B1069CE (void);
// 0x000001D5 System.Void Sirenix.Serialization.JsonDataWriter::Dispose()
extern void JsonDataWriter_Dispose_m6EDECE39054BE9219187614C76E4532A604AF3D3 (void);
// 0x000001D6 System.Void Sirenix.Serialization.JsonDataWriter::PrepareNewSerializationSession()
extern void JsonDataWriter_PrepareNewSerializationSession_m2B13D9AE64EBD408215A2F00B2BB1588CA9F3EEB (void);
// 0x000001D7 System.String Sirenix.Serialization.JsonDataWriter::GetDataDump()
extern void JsonDataWriter_GetDataDump_mBD5CC997829AF86D5530E429685B2A6DB16520F8 (void);
// 0x000001D8 System.Void Sirenix.Serialization.JsonDataWriter::WriteEntry(System.String,System.String)
extern void JsonDataWriter_WriteEntry_m49EF3A6E9F7BA03F4B8609EDE9B48A2777B86F2E (void);
// 0x000001D9 System.Void Sirenix.Serialization.JsonDataWriter::WriteEntry(System.String,System.String,System.Char)
extern void JsonDataWriter_WriteEntry_m777296708D807EBC2ADEE4D25BA67FF31DF0A710 (void);
// 0x000001DA System.Void Sirenix.Serialization.JsonDataWriter::WriteTypeEntry(System.Type)
extern void JsonDataWriter_WriteTypeEntry_m8AAC5D013E600361B59CAAE8925AAD18FDF3FF77 (void);
// 0x000001DB System.Void Sirenix.Serialization.JsonDataWriter::StartNewLine(System.Boolean)
extern void JsonDataWriter_StartNewLine_m6B7DF7C8E8DDCCC0E11D9556C37AF65B9D34599D (void);
// 0x000001DC System.Void Sirenix.Serialization.JsonDataWriter::EnsureBufferSpace(System.Int32)
extern void JsonDataWriter_EnsureBufferSpace_m9145E0B7D97EC0E232061FFBD4D2BC5DD3942377 (void);
// 0x000001DD System.Void Sirenix.Serialization.JsonDataWriter::Buffer_WriteString_WithEscape(System.String)
extern void JsonDataWriter_Buffer_WriteString_WithEscape_mFEB0D9173C7A864412B7320B82196FBE4910D45F (void);
// 0x000001DE System.UInt32[] Sirenix.Serialization.JsonDataWriter::CreateByteToHexLookup()
extern void JsonDataWriter_CreateByteToHexLookup_m68509F4F8D258B3D2C955888A79804ABD9CF624A (void);
// 0x000001DF System.Void Sirenix.Serialization.JsonDataWriter::.cctor()
extern void JsonDataWriter__cctor_m01EFB632820150DA81DEBF25CD6A38CCDA8DC1D7 (void);
// 0x000001E0 Sirenix.Serialization.DeserializationContext Sirenix.Serialization.JsonTextReader::get_Context()
extern void JsonTextReader_get_Context_m85449D5D0A92D23879113C52E9AE319A041C8F1E (void);
// 0x000001E1 System.Void Sirenix.Serialization.JsonTextReader::set_Context(Sirenix.Serialization.DeserializationContext)
extern void JsonTextReader_set_Context_m1E96CDF1C8EB6B390CAC08457B7D3A3CFAE0CF1E (void);
// 0x000001E2 System.Void Sirenix.Serialization.JsonTextReader::.ctor(System.IO.Stream,Sirenix.Serialization.DeserializationContext)
extern void JsonTextReader__ctor_mAF3DBEFA5D85C6D579B9F5129ADB7FC3F97AF6AD (void);
// 0x000001E3 System.Void Sirenix.Serialization.JsonTextReader::Reset()
extern void JsonTextReader_Reset_mCB8A3696E0962DEEB8EB1D6AEBF13D05E0918A84 (void);
// 0x000001E4 System.Void Sirenix.Serialization.JsonTextReader::Dispose()
extern void JsonTextReader_Dispose_m9D330A26E52DEA3CD69278701B2F052C6312BF57 (void);
// 0x000001E5 System.Void Sirenix.Serialization.JsonTextReader::ReadToNextEntry(System.String&,System.String&,Sirenix.Serialization.EntryType&)
extern void JsonTextReader_ReadToNextEntry_m4057BF364DEF32361FCD6A62A5515ED729E15F4F (void);
// 0x000001E6 System.Void Sirenix.Serialization.JsonTextReader::ParseEntryFromBuffer(System.String&,System.String&,Sirenix.Serialization.EntryType&,System.Int32,System.Nullable`1<Sirenix.Serialization.EntryType>)
extern void JsonTextReader_ParseEntryFromBuffer_m9F2CFFC87160FF602E76C7164452723E8887E34F (void);
// 0x000001E7 System.Boolean Sirenix.Serialization.JsonTextReader::IsHex(System.Char)
extern void JsonTextReader_IsHex_m62B6807A96E386E797AEA198B169269290FB0F4D (void);
// 0x000001E8 System.UInt32 Sirenix.Serialization.JsonTextReader::ParseSingleChar(System.Char,System.UInt32)
extern void JsonTextReader_ParseSingleChar_mD4B9F33674147AFD2BE18CC5FAD0D2D69ACA6DD4 (void);
// 0x000001E9 System.Char Sirenix.Serialization.JsonTextReader::ParseHexChar(System.Char,System.Char,System.Char,System.Char)
extern void JsonTextReader_ParseHexChar_m00E5CBECE891BB46C31165C021D002A7B0142EE6 (void);
// 0x000001EA System.Char Sirenix.Serialization.JsonTextReader::ReadCharIntoBuffer()
extern void JsonTextReader_ReadCharIntoBuffer_m99B4E7BC5701345C959263230EE74C866E27CA7E (void);
// 0x000001EB System.Nullable`1<Sirenix.Serialization.EntryType> Sirenix.Serialization.JsonTextReader::GuessPrimitiveType(System.String)
extern void JsonTextReader_GuessPrimitiveType_m811107AD6D53F3FF73D802FFFE2A05D4AEF12E2B (void);
// 0x000001EC System.Char Sirenix.Serialization.JsonTextReader::PeekChar()
extern void JsonTextReader_PeekChar_m178748E2D58685EC74F9DA7DC84D3DE35AA9F22E (void);
// 0x000001ED System.Void Sirenix.Serialization.JsonTextReader::SkipChar()
extern void JsonTextReader_SkipChar_m4A7D5C8E3C97615E4E3CAABC1953BA3D0175B360 (void);
// 0x000001EE System.Char Sirenix.Serialization.JsonTextReader::ConsumeChar()
extern void JsonTextReader_ConsumeChar_m139BBF47B171C950216BFFC84B7EAAB9AE1ABAF6 (void);
// 0x000001EF System.Void Sirenix.Serialization.JsonTextReader::.cctor()
extern void JsonTextReader__cctor_m3BBCC5BF5EFCDBD99C6D648DBB7964E2C0E56CEE (void);
// 0x000001F0 System.Void Sirenix.Serialization.SerializationNodeDataReader::.ctor(Sirenix.Serialization.DeserializationContext)
extern void SerializationNodeDataReader__ctor_m976E320236651917C2442F91C73EB277620F96BF (void);
// 0x000001F1 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::get_IndexIsValid()
extern void SerializationNodeDataReader_get_IndexIsValid_mDD227070003AB1FEA659C40A50FB8C10089BEF5E (void);
// 0x000001F2 System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode> Sirenix.Serialization.SerializationNodeDataReader::get_Nodes()
extern void SerializationNodeDataReader_get_Nodes_m36C8F1179E69BA520B00F46B4D58CB5B76F5DEB7 (void);
// 0x000001F3 System.Void Sirenix.Serialization.SerializationNodeDataReader::set_Nodes(System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode>)
extern void SerializationNodeDataReader_set_Nodes_mD18A6F4C40C3A0DAFEA50E4D75F60C678D897782 (void);
// 0x000001F4 System.IO.Stream Sirenix.Serialization.SerializationNodeDataReader::get_Stream()
extern void SerializationNodeDataReader_get_Stream_mEF9060551BCB9762C20C4845661403477BC5BBE9 (void);
// 0x000001F5 System.Void Sirenix.Serialization.SerializationNodeDataReader::set_Stream(System.IO.Stream)
extern void SerializationNodeDataReader_set_Stream_m122526BCB2DA890CA057B2F06177922164608C34 (void);
// 0x000001F6 System.Void Sirenix.Serialization.SerializationNodeDataReader::Dispose()
extern void SerializationNodeDataReader_Dispose_mF6C4B011A479ED47F802FB78D4B06BFED5049023 (void);
// 0x000001F7 System.Void Sirenix.Serialization.SerializationNodeDataReader::PrepareNewSerializationSession()
extern void SerializationNodeDataReader_PrepareNewSerializationSession_m44A0A929785EDAA12734E01AEC775DB062C205CB (void);
// 0x000001F8 Sirenix.Serialization.EntryType Sirenix.Serialization.SerializationNodeDataReader::PeekEntry(System.String&)
extern void SerializationNodeDataReader_PeekEntry_m68F9F44526B9B723BB78679374E4F8D6C546F633 (void);
// 0x000001F9 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::EnterArray(System.Int64&)
extern void SerializationNodeDataReader_EnterArray_mB9AE0DBE4F116BF09ECBD614025A825EC31654D8 (void);
// 0x000001FA System.Boolean Sirenix.Serialization.SerializationNodeDataReader::EnterNode(System.Type&)
extern void SerializationNodeDataReader_EnterNode_m7DC9C962190B096693E8A40EF755C07981B86C43 (void);
// 0x000001FB System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ExitArray()
extern void SerializationNodeDataReader_ExitArray_m26DA1C1CE9ACE7E39FD327EC27AC2F89300F8ACC (void);
// 0x000001FC System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ExitNode()
extern void SerializationNodeDataReader_ExitNode_mC35E49FC3D8382B8A7FFC7DB5457C30B16F5CA12 (void);
// 0x000001FD System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadBoolean(System.Boolean&)
extern void SerializationNodeDataReader_ReadBoolean_mFC5E84A6364091296F6CCA59A1A13D9363A26AA2 (void);
// 0x000001FE System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadByte(System.Byte&)
extern void SerializationNodeDataReader_ReadByte_m61786C88BF6170044C3F58E1BB3C003F8F249B88 (void);
// 0x000001FF System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadChar(System.Char&)
extern void SerializationNodeDataReader_ReadChar_mD36FBC747836E56C8792267EB939616EA093BE5B (void);
// 0x00000200 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadDecimal(System.Decimal&)
extern void SerializationNodeDataReader_ReadDecimal_m6DF389A32D61DF851B9A912AFCE1A65D929E6F61 (void);
// 0x00000201 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadDouble(System.Double&)
extern void SerializationNodeDataReader_ReadDouble_mB3EE58D57B48DD309C3F9E43D501487A4056DE28 (void);
// 0x00000202 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadExternalReference(System.Guid&)
extern void SerializationNodeDataReader_ReadExternalReference_mE0B613FF2E56563074B56E1D8E7D51B93E6405A7 (void);
// 0x00000203 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadExternalReference(System.String&)
extern void SerializationNodeDataReader_ReadExternalReference_m04E8A8BE8CD751D6A48679A2347D35AC5DD15014 (void);
// 0x00000204 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadExternalReference(System.Int32&)
extern void SerializationNodeDataReader_ReadExternalReference_m8B4165A0585145045494B7C79DAEE2307264A75C (void);
// 0x00000205 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadGuid(System.Guid&)
extern void SerializationNodeDataReader_ReadGuid_mB5062678B1D6574258F44BBFF3BF680489D2BEFF (void);
// 0x00000206 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadInt16(System.Int16&)
extern void SerializationNodeDataReader_ReadInt16_mD57065D39B67F874F9EE79EC6E0DDEBFCD211833 (void);
// 0x00000207 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadInt32(System.Int32&)
extern void SerializationNodeDataReader_ReadInt32_m2197161BD17FBE39ABB3F7E806A64BEE8F26F8DC (void);
// 0x00000208 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadInt64(System.Int64&)
extern void SerializationNodeDataReader_ReadInt64_m5A9A718C2B00FDA772A6D97202CD5B0C41827EEA (void);
// 0x00000209 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadInternalReference(System.Int32&)
extern void SerializationNodeDataReader_ReadInternalReference_m6ECF4FFBDF05D08E0D4608F1F2B8569D36F32CB1 (void);
// 0x0000020A System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadNull()
extern void SerializationNodeDataReader_ReadNull_m6CBFF360F2E9822E477F736041E3AAC8BFCD66EA (void);
// 0x0000020B System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadPrimitiveArray(T[]&)
// 0x0000020C System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadSByte(System.SByte&)
extern void SerializationNodeDataReader_ReadSByte_m3C5376F6005997C3F181BC513A4E4D040EF958B4 (void);
// 0x0000020D System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadSingle(System.Single&)
extern void SerializationNodeDataReader_ReadSingle_m1A2799BCFECF85FBA97D961107D1F5CEDEDD5663 (void);
// 0x0000020E System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadString(System.String&)
extern void SerializationNodeDataReader_ReadString_m8BB41C0D2F6C35E22085AA7E1395B922619EDD80 (void);
// 0x0000020F System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadUInt16(System.UInt16&)
extern void SerializationNodeDataReader_ReadUInt16_mD3BEE085176842B38CBCB3DF9D76DFBF8C127F9F (void);
// 0x00000210 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadUInt32(System.UInt32&)
extern void SerializationNodeDataReader_ReadUInt32_m061081D18E3B14BECC40CAA70C60EB343EB0BE0F (void);
// 0x00000211 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadUInt64(System.UInt64&)
extern void SerializationNodeDataReader_ReadUInt64_mB4C90286086084D8B9180956E1ABDC9452ED2582 (void);
// 0x00000212 System.String Sirenix.Serialization.SerializationNodeDataReader::GetDataDump()
extern void SerializationNodeDataReader_GetDataDump_m6A08FB2120CDD1DA7693EB4968607B9E427D2BF0 (void);
// 0x00000213 System.Void Sirenix.Serialization.SerializationNodeDataReader::ConsumeCurrentEntry()
extern void SerializationNodeDataReader_ConsumeCurrentEntry_mD1B3BAE2DA7C8A2049B9107C649E442C69025A7A (void);
// 0x00000214 Sirenix.Serialization.EntryType Sirenix.Serialization.SerializationNodeDataReader::PeekEntry()
extern void SerializationNodeDataReader_PeekEntry_mC3CA21618EAD0A4C620B8972F1FE0C4787CA99EB (void);
// 0x00000215 Sirenix.Serialization.EntryType Sirenix.Serialization.SerializationNodeDataReader::ReadToNextEntry()
extern void SerializationNodeDataReader_ReadToNextEntry_m077B8C3A1757378ECCD5ECEABB9352CB0823CCB4 (void);
// 0x00000216 System.Char Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_0()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_0_mE4786C23BEA3BE4BDBA3BFBBCAD62EB2AF48F80D (void);
// 0x00000217 System.SByte Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_1()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_1_m9614490D64B32D4C2C7B34D3F5122C4C46C8041E (void);
// 0x00000218 System.Int16 Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_2()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_2_m725219854A80EFD61A1FA55A8243FC00FE7ECE7D (void);
// 0x00000219 System.Int32 Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_3()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_3_m553562A6D41179FCA3B9893BD7FDDB20B0979CEA (void);
// 0x0000021A System.Int64 Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_4()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_4_mA397B03CC5FF51044EEC1DC1477AD5DD2B916F5C (void);
// 0x0000021B System.Byte Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_5()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_5_mDD9F9F15739E3E869AA1EC8AAFB7E62F741F9247 (void);
// 0x0000021C System.UInt16 Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_6()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_6_m9A8AEE2C7F6C1C6215BBBC409C262CF8A8FCE210 (void);
// 0x0000021D System.UInt32 Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_7()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_7_m2C3DEDE8BD7C704B1A927DA281F1DCFD2ECE2E7C (void);
// 0x0000021E System.UInt64 Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_8()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_8_mDB3195374DA2876A9941FAA9E1EE72996C62506E (void);
// 0x0000021F System.Decimal Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_9()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_9_m86984641BF2059AE06B1BF464078BEAB8347F353 (void);
// 0x00000220 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_10()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_10_mAB88D550A4561B0CD7E4E28114A3049BA16A1BE3 (void);
// 0x00000221 System.Single Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_11()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_11_m0016EC67B3BEFB616554ADB1C1D69F0EC7E0592C (void);
// 0x00000222 System.Double Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_12()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_12_mDC36F9EF4481344B8CED2008599AB65B40E53AC1 (void);
// 0x00000223 System.Guid Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_13()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_13_mB1E9872F357E0C93A300E29625BCBC745E7EE8C9 (void);
// 0x00000224 System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode> Sirenix.Serialization.SerializationNodeDataWriter::get_Nodes()
extern void SerializationNodeDataWriter_get_Nodes_m2F595A8DAB26FA65D31681F315C55176A8008F79 (void);
// 0x00000225 System.Void Sirenix.Serialization.SerializationNodeDataWriter::set_Nodes(System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode>)
extern void SerializationNodeDataWriter_set_Nodes_m94A735E4146A17A5D53AB873EA06F51A7F0EC57C (void);
// 0x00000226 System.Void Sirenix.Serialization.SerializationNodeDataWriter::.ctor(Sirenix.Serialization.SerializationContext)
extern void SerializationNodeDataWriter__ctor_m5BF16B54AFAE4A7D16FFEA8896E1FC704B5682C5 (void);
// 0x00000227 System.IO.Stream Sirenix.Serialization.SerializationNodeDataWriter::get_Stream()
extern void SerializationNodeDataWriter_get_Stream_m31FEE403884B9A1DE3BDFC72C9D5AC5471D37EAB (void);
// 0x00000228 System.Void Sirenix.Serialization.SerializationNodeDataWriter::set_Stream(System.IO.Stream)
extern void SerializationNodeDataWriter_set_Stream_mCE92CD9A575D912D347127F9F0A38055AC6485A3 (void);
// 0x00000229 System.Void Sirenix.Serialization.SerializationNodeDataWriter::BeginArrayNode(System.Int64)
extern void SerializationNodeDataWriter_BeginArrayNode_m15ACB11CE6D36CD77A8A07B87DDE9ECDD33E3A03 (void);
// 0x0000022A System.Void Sirenix.Serialization.SerializationNodeDataWriter::BeginReferenceNode(System.String,System.Type,System.Int32)
extern void SerializationNodeDataWriter_BeginReferenceNode_m8E26D0D1B3EA8EACD1F5C50F489CBBB9B8A8C9A6 (void);
// 0x0000022B System.Void Sirenix.Serialization.SerializationNodeDataWriter::BeginStructNode(System.String,System.Type)
extern void SerializationNodeDataWriter_BeginStructNode_m538277BB76CC0BB894F2830570FF730FD76ACDA5 (void);
// 0x0000022C System.Void Sirenix.Serialization.SerializationNodeDataWriter::Dispose()
extern void SerializationNodeDataWriter_Dispose_mA68AEA3B7D9BA3C986136BD222A1E7A797A05E77 (void);
// 0x0000022D System.Void Sirenix.Serialization.SerializationNodeDataWriter::EndArrayNode()
extern void SerializationNodeDataWriter_EndArrayNode_m0A678F402E1BAF2147893E66BAA1F8098512DAB6 (void);
// 0x0000022E System.Void Sirenix.Serialization.SerializationNodeDataWriter::EndNode(System.String)
extern void SerializationNodeDataWriter_EndNode_m29CDFF09537E779AACEE2C9EFD76394450119C84 (void);
// 0x0000022F System.Void Sirenix.Serialization.SerializationNodeDataWriter::PrepareNewSerializationSession()
extern void SerializationNodeDataWriter_PrepareNewSerializationSession_m4583EB6CED541EF60331D43D16F5F82664EDDC50 (void);
// 0x00000230 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteBoolean(System.String,System.Boolean)
extern void SerializationNodeDataWriter_WriteBoolean_m9C9A0C2AFE02057EBFEAE932EE7F02DD14D7F116 (void);
// 0x00000231 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteByte(System.String,System.Byte)
extern void SerializationNodeDataWriter_WriteByte_mC256DDC404C92E3ED3B8270E14BB19DFA85D8761 (void);
// 0x00000232 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteChar(System.String,System.Char)
extern void SerializationNodeDataWriter_WriteChar_m5D0D25AC02A5C7DC600EF380D0B50AD1132AD350 (void);
// 0x00000233 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteDecimal(System.String,System.Decimal)
extern void SerializationNodeDataWriter_WriteDecimal_mB72E84911B4165EA941C0F704640C0F7E78E1046 (void);
// 0x00000234 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteSingle(System.String,System.Single)
extern void SerializationNodeDataWriter_WriteSingle_m3876574D5FBBE2FECC1A8778B6570370053EFA97 (void);
// 0x00000235 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteDouble(System.String,System.Double)
extern void SerializationNodeDataWriter_WriteDouble_m72CD65562A4C0EDAB0F6D3DCBDD1DDCC46ED0E5A (void);
// 0x00000236 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteExternalReference(System.String,System.Guid)
extern void SerializationNodeDataWriter_WriteExternalReference_m8D02BC0796F6FD231BA3AD9F82953C04900B3BFF (void);
// 0x00000237 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteExternalReference(System.String,System.String)
extern void SerializationNodeDataWriter_WriteExternalReference_m2FB93C1993BC4623A3C30BC26938F8AB9F8C0D8C (void);
// 0x00000238 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteExternalReference(System.String,System.Int32)
extern void SerializationNodeDataWriter_WriteExternalReference_m925C30CA4279A0B7650077D97B7682F1522BDCB8 (void);
// 0x00000239 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteGuid(System.String,System.Guid)
extern void SerializationNodeDataWriter_WriteGuid_mC5FB42F68F3614F653A98ED5F13EEF573E280027 (void);
// 0x0000023A System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteInt16(System.String,System.Int16)
extern void SerializationNodeDataWriter_WriteInt16_m04E73581ED5CB25A7D1BD4AAF40E16A6231E1DCC (void);
// 0x0000023B System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteInt32(System.String,System.Int32)
extern void SerializationNodeDataWriter_WriteInt32_m24A8DD1F2BEE457A2CADEC2F16ED8E4A04BE9027 (void);
// 0x0000023C System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteInt64(System.String,System.Int64)
extern void SerializationNodeDataWriter_WriteInt64_m410EECDB21C110A925BF00274223DC90CE78E7E1 (void);
// 0x0000023D System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteInternalReference(System.String,System.Int32)
extern void SerializationNodeDataWriter_WriteInternalReference_m9D1646ECE3E2461FFE9A23A1163CC151D71C6C37 (void);
// 0x0000023E System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteNull(System.String)
extern void SerializationNodeDataWriter_WriteNull_m0F56438C0AA4E0F8AD204BD710EF3916889A74AF (void);
// 0x0000023F System.Void Sirenix.Serialization.SerializationNodeDataWriter::WritePrimitiveArray(T[])
// 0x00000240 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteSByte(System.String,System.SByte)
extern void SerializationNodeDataWriter_WriteSByte_m7DC5DDDD04B85AAE54FCD23E21120FA74867F6C2 (void);
// 0x00000241 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteString(System.String,System.String)
extern void SerializationNodeDataWriter_WriteString_mCC5A7CFF35A49490A3080EAC4852FF04B8CF9516 (void);
// 0x00000242 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteUInt16(System.String,System.UInt16)
extern void SerializationNodeDataWriter_WriteUInt16_mD1EC282993194412D65D94471C0DDE3AB1BDD7A0 (void);
// 0x00000243 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteUInt32(System.String,System.UInt32)
extern void SerializationNodeDataWriter_WriteUInt32_mA62961CCB923DA980A4A31AA48484AE735967D47 (void);
// 0x00000244 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteUInt64(System.String,System.UInt64)
extern void SerializationNodeDataWriter_WriteUInt64_mE2CAA588C079618AEB70B5EA1CBA3F28167F4E92 (void);
// 0x00000245 System.Void Sirenix.Serialization.SerializationNodeDataWriter::FlushToStream()
extern void SerializationNodeDataWriter_FlushToStream_mD1DF9FA3EF5A01361A40FC2B201EC1DE243662EE (void);
// 0x00000246 System.String Sirenix.Serialization.SerializationNodeDataWriter::GetDataDump()
extern void SerializationNodeDataWriter_GetDataDump_m4C3792A22D7877C0A4B9D1D8FD2881310FD19612 (void);
// 0x00000247 T[] Sirenix.Serialization.ArrayFormatter`1::GetUninitializedObject()
// 0x00000248 System.Void Sirenix.Serialization.ArrayFormatter`1::DeserializeImplementation(T[]&,Sirenix.Serialization.IDataReader)
// 0x00000249 System.Void Sirenix.Serialization.ArrayFormatter`1::SerializeImplementation(T[]&,Sirenix.Serialization.IDataWriter)
// 0x0000024A System.Void Sirenix.Serialization.ArrayFormatter`1::.ctor()
// 0x0000024B System.Void Sirenix.Serialization.ArrayFormatter`1::.cctor()
// 0x0000024C System.Collections.ArrayList Sirenix.Serialization.ArrayListFormatter::GetUninitializedObject()
extern void ArrayListFormatter_GetUninitializedObject_m04973C36E971A3DB1E189192E6684406960A20C1 (void);
// 0x0000024D System.Void Sirenix.Serialization.ArrayListFormatter::DeserializeImplementation(System.Collections.ArrayList&,Sirenix.Serialization.IDataReader)
extern void ArrayListFormatter_DeserializeImplementation_m8DE5475CC951B9C7B575473993A34E0E245CD962 (void);
// 0x0000024E System.Void Sirenix.Serialization.ArrayListFormatter::SerializeImplementation(System.Collections.ArrayList&,Sirenix.Serialization.IDataWriter)
extern void ArrayListFormatter_SerializeImplementation_mA8D8712B515D2E326C466D04E74B1F7DCC934133 (void);
// 0x0000024F System.Void Sirenix.Serialization.ArrayListFormatter::.ctor()
extern void ArrayListFormatter__ctor_mA635D89A7A48DE51AFC221F381D749BDE8200657 (void);
// 0x00000250 System.Void Sirenix.Serialization.ArrayListFormatter::.cctor()
extern void ArrayListFormatter__cctor_m28360BCF306AC2594252927FA3835F780D1F67CF (void);
// 0x00000251 System.Void Sirenix.Serialization.BaseFormatter`1::.cctor()
// 0x00000252 Sirenix.Serialization.BaseFormatter`1/SerializationCallback<T>[] Sirenix.Serialization.BaseFormatter`1::GetCallbacks(System.Reflection.MethodInfo[],System.Type,System.Collections.Generic.List`1<Sirenix.Serialization.BaseFormatter`1/SerializationCallback<T>>&)
// 0x00000253 Sirenix.Serialization.BaseFormatter`1/SerializationCallback<T> Sirenix.Serialization.BaseFormatter`1::CreateCallback(System.Reflection.MethodInfo)
// 0x00000254 System.Type Sirenix.Serialization.BaseFormatter`1::get_SerializedType()
// 0x00000255 System.Void Sirenix.Serialization.BaseFormatter`1::Sirenix.Serialization.IFormatter.Serialize(System.Object,Sirenix.Serialization.IDataWriter)
// 0x00000256 System.Object Sirenix.Serialization.BaseFormatter`1::Sirenix.Serialization.IFormatter.Deserialize(Sirenix.Serialization.IDataReader)
// 0x00000257 T Sirenix.Serialization.BaseFormatter`1::Deserialize(Sirenix.Serialization.IDataReader)
// 0x00000258 System.Void Sirenix.Serialization.BaseFormatter`1::Serialize(T,Sirenix.Serialization.IDataWriter)
// 0x00000259 T Sirenix.Serialization.BaseFormatter`1::GetUninitializedObject()
// 0x0000025A System.Void Sirenix.Serialization.BaseFormatter`1::RegisterReferenceID(T,Sirenix.Serialization.IDataReader)
// 0x0000025B System.Void Sirenix.Serialization.BaseFormatter`1::InvokeOnDeserializingCallbacks(T,Sirenix.Serialization.DeserializationContext)
// 0x0000025C System.Void Sirenix.Serialization.BaseFormatter`1::InvokeOnDeserializingCallbacks(T&,Sirenix.Serialization.DeserializationContext)
// 0x0000025D System.Void Sirenix.Serialization.BaseFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x0000025E System.Void Sirenix.Serialization.BaseFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x0000025F System.Void Sirenix.Serialization.BaseFormatter`1::.ctor()
// 0x00000260 System.Void Sirenix.Serialization.BaseFormatter`1/SerializationCallback::.ctor(System.Object,System.IntPtr)
// 0x00000261 System.Void Sirenix.Serialization.BaseFormatter`1/SerializationCallback::Invoke(T&,System.Runtime.Serialization.StreamingContext)
// 0x00000262 System.IAsyncResult Sirenix.Serialization.BaseFormatter`1/SerializationCallback::BeginInvoke(T&,System.Runtime.Serialization.StreamingContext,System.AsyncCallback,System.Object)
// 0x00000263 System.Void Sirenix.Serialization.BaseFormatter`1/SerializationCallback::EndInvoke(T&,System.IAsyncResult)
// 0x00000264 System.Void Sirenix.Serialization.BaseFormatter`1/<>c__DisplayClass11_0::.ctor()
// 0x00000265 System.Void Sirenix.Serialization.BaseFormatter`1/<>c__DisplayClass11_0::<CreateCallback>b__0(T&,System.Runtime.Serialization.StreamingContext)
// 0x00000266 System.Void Sirenix.Serialization.BaseFormatter`1/<>c__DisplayClass11_0::<CreateCallback>b__1(T&,System.Runtime.Serialization.StreamingContext)
// 0x00000267 System.Void Sirenix.Serialization.DateTimeFormatter::Read(System.DateTime&,Sirenix.Serialization.IDataReader)
extern void DateTimeFormatter_Read_m6CF40B021E9E325ECA8BBCD6A10198CCD92ED2BC (void);
// 0x00000268 System.Void Sirenix.Serialization.DateTimeFormatter::Write(System.DateTime&,Sirenix.Serialization.IDataWriter)
extern void DateTimeFormatter_Write_mF6286F52612305C1EE2A75568F6592C38061B8D9 (void);
// 0x00000269 System.Void Sirenix.Serialization.DateTimeFormatter::.ctor()
extern void DateTimeFormatter__ctor_m439621F8BEA99A2EC6D0318CD4BF24D01A86C4B6 (void);
// 0x0000026A System.Void Sirenix.Serialization.DateTimeOffsetFormatter::Read(System.DateTimeOffset&,Sirenix.Serialization.IDataReader)
extern void DateTimeOffsetFormatter_Read_mC88476F648CB87FF47A357297E4DB88C22B3F7EC (void);
// 0x0000026B System.Void Sirenix.Serialization.DateTimeOffsetFormatter::Write(System.DateTimeOffset&,Sirenix.Serialization.IDataWriter)
extern void DateTimeOffsetFormatter_Write_m9353636387D2D0987B30B71F25DFEF9BDB311B6A (void);
// 0x0000026C System.Void Sirenix.Serialization.DateTimeOffsetFormatter::.ctor()
extern void DateTimeOffsetFormatter__ctor_m6BADBC035BFCA8469842F64E44C660ED70FE2491 (void);
// 0x0000026D System.Void Sirenix.Serialization.DelegateFormatter`1::.cctor()
// 0x0000026E System.Void Sirenix.Serialization.DelegateFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x0000026F System.Void Sirenix.Serialization.DelegateFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x00000270 T Sirenix.Serialization.DelegateFormatter`1::GetUninitializedObject()
// 0x00000271 System.Void Sirenix.Serialization.DelegateFormatter`1::.ctor()
// 0x00000272 System.Void Sirenix.Serialization.DelegateFormatter`1/<>c::.cctor()
// 0x00000273 System.Void Sirenix.Serialization.DelegateFormatter`1/<>c::.ctor()
// 0x00000274 System.String Sirenix.Serialization.DelegateFormatter`1/<>c::<DeserializeImplementation>b__6_0(System.Type)
// 0x00000275 System.String Sirenix.Serialization.DelegateFormatter`1/<>c::<DeserializeImplementation>b__6_1(System.Type)
// 0x00000276 System.Void Sirenix.Serialization.DerivedDictionaryFormatter`3::.cctor()
// 0x00000277 System.Void Sirenix.Serialization.DerivedDictionaryFormatter`3::.ctor()
// 0x00000278 TDictionary Sirenix.Serialization.DerivedDictionaryFormatter`3::GetUninitializedObject()
// 0x00000279 System.Void Sirenix.Serialization.DerivedDictionaryFormatter`3::DeserializeImplementation(TDictionary&,Sirenix.Serialization.IDataReader)
// 0x0000027A System.Void Sirenix.Serialization.DerivedDictionaryFormatter`3::SerializeImplementation(TDictionary&,Sirenix.Serialization.IDataWriter)
// 0x0000027B System.Void Sirenix.Serialization.DictionaryFormatter`2::.cctor()
// 0x0000027C System.Void Sirenix.Serialization.DictionaryFormatter`2::.ctor()
// 0x0000027D System.Collections.Generic.Dictionary`2<TKey,TValue> Sirenix.Serialization.DictionaryFormatter`2::GetUninitializedObject()
// 0x0000027E System.Void Sirenix.Serialization.DictionaryFormatter`2::DeserializeImplementation(System.Collections.Generic.Dictionary`2<TKey,TValue>&,Sirenix.Serialization.IDataReader)
// 0x0000027F System.Void Sirenix.Serialization.DictionaryFormatter`2::SerializeImplementation(System.Collections.Generic.Dictionary`2<TKey,TValue>&,Sirenix.Serialization.IDataWriter)
// 0x00000280 System.Void Sirenix.Serialization.DoubleLookupDictionaryFormatter`3::.cctor()
// 0x00000281 System.Void Sirenix.Serialization.DoubleLookupDictionaryFormatter`3::.ctor()
// 0x00000282 Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<TPrimary,TSecondary,TValue> Sirenix.Serialization.DoubleLookupDictionaryFormatter`3::GetUninitializedObject()
// 0x00000283 System.Void Sirenix.Serialization.DoubleLookupDictionaryFormatter`3::SerializeImplementation(Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<TPrimary,TSecondary,TValue>&,Sirenix.Serialization.IDataWriter)
// 0x00000284 System.Void Sirenix.Serialization.DoubleLookupDictionaryFormatter`3::DeserializeImplementation(Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<TPrimary,TSecondary,TValue>&,Sirenix.Serialization.IDataReader)
// 0x00000285 System.Void Sirenix.Serialization.EasyBaseFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x00000286 System.Void Sirenix.Serialization.EasyBaseFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x00000287 System.Void Sirenix.Serialization.EasyBaseFormatter`1::ReadDataEntry(T&,System.String,Sirenix.Serialization.EntryType,Sirenix.Serialization.IDataReader)
// 0x00000288 System.Void Sirenix.Serialization.EasyBaseFormatter`1::WriteDataEntries(T&,Sirenix.Serialization.IDataWriter)
// 0x00000289 System.Void Sirenix.Serialization.EasyBaseFormatter`1::.ctor()
// 0x0000028A System.Void Sirenix.Serialization.EmittedFormatterAttribute::.ctor()
extern void EmittedFormatterAttribute__ctor_m5F7732353D2CBA57280CDEDCE9FCA2947E8ED7AB (void);
// 0x0000028B System.Void Sirenix.Serialization.EmptyTypeFormatter`1::ReadDataEntry(T&,System.String,Sirenix.Serialization.EntryType,Sirenix.Serialization.IDataReader)
// 0x0000028C System.Void Sirenix.Serialization.EmptyTypeFormatter`1::WriteDataEntries(T&,Sirenix.Serialization.IDataWriter)
// 0x0000028D System.Void Sirenix.Serialization.EmptyTypeFormatter`1::.ctor()
// 0x0000028E Sirenix.Serialization.IFormatter Sirenix.Serialization.FormatterEmitter::GetEmittedFormatter(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterEmitter_GetEmittedFormatter_mC29E5001FE34E89CC77C42FD191F2E14C9AFBDB2 (void);
// 0x0000028F System.Void Sirenix.Serialization.FormatterEmitter/AOTEmittedFormatter`1::.ctor()
// 0x00000290 System.Void Sirenix.Serialization.FormatterEmitter/EmptyAOTEmittedFormatter`1::ReadDataEntry(T&,System.String,Sirenix.Serialization.EntryType,Sirenix.Serialization.IDataReader)
// 0x00000291 System.Void Sirenix.Serialization.FormatterEmitter/EmptyAOTEmittedFormatter`1::WriteDataEntries(T&,Sirenix.Serialization.IDataWriter)
// 0x00000292 System.Void Sirenix.Serialization.FormatterEmitter/EmptyAOTEmittedFormatter`1::.ctor()
// 0x00000293 System.Boolean Sirenix.Serialization.GenericCollectionFormatter::CanFormat(System.Type,System.Type&)
extern void GenericCollectionFormatter_CanFormat_mF1F0F65ADA7A21400F8599584FE524FB19651FB6 (void);
// 0x00000294 System.Void Sirenix.Serialization.GenericCollectionFormatter`2::.cctor()
// 0x00000295 System.Void Sirenix.Serialization.GenericCollectionFormatter`2::.ctor()
// 0x00000296 TCollection Sirenix.Serialization.GenericCollectionFormatter`2::GetUninitializedObject()
// 0x00000297 System.Void Sirenix.Serialization.GenericCollectionFormatter`2::DeserializeImplementation(TCollection&,Sirenix.Serialization.IDataReader)
// 0x00000298 System.Void Sirenix.Serialization.GenericCollectionFormatter`2::SerializeImplementation(TCollection&,Sirenix.Serialization.IDataWriter)
// 0x00000299 System.Void Sirenix.Serialization.HashSetFormatter`1::.cctor()
// 0x0000029A System.Void Sirenix.Serialization.HashSetFormatter`1::.ctor()
// 0x0000029B System.Collections.Generic.HashSet`1<T> Sirenix.Serialization.HashSetFormatter`1::GetUninitializedObject()
// 0x0000029C System.Void Sirenix.Serialization.HashSetFormatter`1::DeserializeImplementation(System.Collections.Generic.HashSet`1<T>&,Sirenix.Serialization.IDataReader)
// 0x0000029D System.Void Sirenix.Serialization.HashSetFormatter`1::SerializeImplementation(System.Collections.Generic.HashSet`1<T>&,Sirenix.Serialization.IDataWriter)
// 0x0000029E System.Type Sirenix.Serialization.IFormatter::get_SerializedType()
// 0x0000029F System.Void Sirenix.Serialization.IFormatter::Serialize(System.Object,Sirenix.Serialization.IDataWriter)
// 0x000002A0 System.Object Sirenix.Serialization.IFormatter::Deserialize(Sirenix.Serialization.IDataReader)
// 0x000002A1 System.Void Sirenix.Serialization.IFormatter`1::Serialize(T,Sirenix.Serialization.IDataWriter)
// 0x000002A2 T Sirenix.Serialization.IFormatter`1::Deserialize(Sirenix.Serialization.IDataReader)
// 0x000002A3 System.Void Sirenix.Serialization.KeyValuePairFormatter`2::SerializeImplementation(System.Collections.Generic.KeyValuePair`2<TKey,TValue>&,Sirenix.Serialization.IDataWriter)
// 0x000002A4 System.Void Sirenix.Serialization.KeyValuePairFormatter`2::DeserializeImplementation(System.Collections.Generic.KeyValuePair`2<TKey,TValue>&,Sirenix.Serialization.IDataReader)
// 0x000002A5 System.Void Sirenix.Serialization.KeyValuePairFormatter`2::.ctor()
// 0x000002A6 System.Void Sirenix.Serialization.KeyValuePairFormatter`2::.cctor()
// 0x000002A7 System.Void Sirenix.Serialization.ListFormatter`1::.cctor()
// 0x000002A8 System.Void Sirenix.Serialization.ListFormatter`1::.ctor()
// 0x000002A9 System.Collections.Generic.List`1<T> Sirenix.Serialization.ListFormatter`1::GetUninitializedObject()
// 0x000002AA System.Void Sirenix.Serialization.ListFormatter`1::DeserializeImplementation(System.Collections.Generic.List`1<T>&,Sirenix.Serialization.IDataReader)
// 0x000002AB System.Void Sirenix.Serialization.ListFormatter`1::SerializeImplementation(System.Collections.Generic.List`1<T>&,Sirenix.Serialization.IDataWriter)
// 0x000002AC System.Type Sirenix.Serialization.MinimalBaseFormatter`1::get_SerializedType()
// 0x000002AD T Sirenix.Serialization.MinimalBaseFormatter`1::Deserialize(Sirenix.Serialization.IDataReader)
// 0x000002AE System.Void Sirenix.Serialization.MinimalBaseFormatter`1::Serialize(T,Sirenix.Serialization.IDataWriter)
// 0x000002AF System.Void Sirenix.Serialization.MinimalBaseFormatter`1::Sirenix.Serialization.IFormatter.Serialize(System.Object,Sirenix.Serialization.IDataWriter)
// 0x000002B0 System.Object Sirenix.Serialization.MinimalBaseFormatter`1::Sirenix.Serialization.IFormatter.Deserialize(Sirenix.Serialization.IDataReader)
// 0x000002B1 T Sirenix.Serialization.MinimalBaseFormatter`1::GetUninitializedObject()
// 0x000002B2 System.Void Sirenix.Serialization.MinimalBaseFormatter`1::Read(T&,Sirenix.Serialization.IDataReader)
// 0x000002B3 System.Void Sirenix.Serialization.MinimalBaseFormatter`1::Write(T&,Sirenix.Serialization.IDataWriter)
// 0x000002B4 System.Void Sirenix.Serialization.MinimalBaseFormatter`1::RegisterReferenceID(T,Sirenix.Serialization.IDataReader)
// 0x000002B5 System.Void Sirenix.Serialization.MinimalBaseFormatter`1::.ctor()
// 0x000002B6 System.Void Sirenix.Serialization.MinimalBaseFormatter`1::.cctor()
// 0x000002B7 System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::.cctor()
// 0x000002B8 TArray Sirenix.Serialization.MultiDimensionalArrayFormatter`2::GetUninitializedObject()
// 0x000002B9 System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::DeserializeImplementation(TArray&,Sirenix.Serialization.IDataReader)
// 0x000002BA System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::SerializeImplementation(TArray&,Sirenix.Serialization.IDataWriter)
// 0x000002BB System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::IterateArrayWrite(System.Array,System.Func`1<TElement>)
// 0x000002BC System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::IterateArrayWrite(System.Array,System.Int32,System.Int32[],System.Func`1<TElement>)
// 0x000002BD System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::IterateArrayRead(System.Array,System.Action`1<TElement>)
// 0x000002BE System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::IterateArrayRead(System.Array,System.Int32,System.Int32[],System.Action`1<TElement>)
// 0x000002BF System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::.ctor()
// 0x000002C0 System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2/<>c__DisplayClass6_0::.ctor()
// 0x000002C1 System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2/<>c__DisplayClass6_1::.ctor()
// 0x000002C2 TElement Sirenix.Serialization.MultiDimensionalArrayFormatter`2/<>c__DisplayClass6_1::<DeserializeImplementation>b__0()
// 0x000002C3 System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2/<>c__DisplayClass7_0::.ctor()
// 0x000002C4 System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2/<>c__DisplayClass7_0::<SerializeImplementation>b__0(TElement)
// 0x000002C5 System.Void Sirenix.Serialization.NullableFormatter`1::.cctor()
// 0x000002C6 System.Void Sirenix.Serialization.NullableFormatter`1::.ctor()
// 0x000002C7 System.Void Sirenix.Serialization.NullableFormatter`1::DeserializeImplementation(System.Nullable`1<T>&,Sirenix.Serialization.IDataReader)
// 0x000002C8 System.Void Sirenix.Serialization.NullableFormatter`1::SerializeImplementation(System.Nullable`1<T>&,Sirenix.Serialization.IDataWriter)
// 0x000002C9 T[] Sirenix.Serialization.PrimitiveArrayFormatter`1::GetUninitializedObject()
// 0x000002CA System.Void Sirenix.Serialization.PrimitiveArrayFormatter`1::Read(T[]&,Sirenix.Serialization.IDataReader)
// 0x000002CB System.Void Sirenix.Serialization.PrimitiveArrayFormatter`1::Write(T[]&,Sirenix.Serialization.IDataWriter)
// 0x000002CC System.Void Sirenix.Serialization.PrimitiveArrayFormatter`1::.ctor()
// 0x000002CD System.Void Sirenix.Serialization.ReflectionFormatter`1::.ctor()
// 0x000002CE System.Void Sirenix.Serialization.ReflectionFormatter`1::.ctor(Sirenix.Serialization.ISerializationPolicy)
// 0x000002CF Sirenix.Serialization.ISerializationPolicy Sirenix.Serialization.ReflectionFormatter`1::get_OverridePolicy()
// 0x000002D0 System.Void Sirenix.Serialization.ReflectionFormatter`1::set_OverridePolicy(Sirenix.Serialization.ISerializationPolicy)
// 0x000002D1 System.Void Sirenix.Serialization.ReflectionFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x000002D2 System.Void Sirenix.Serialization.ReflectionFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x000002D3 System.Void Sirenix.Serialization.SelfFormatterFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x000002D4 System.Void Sirenix.Serialization.SelfFormatterFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x000002D5 System.Void Sirenix.Serialization.SelfFormatterFormatter`1::.ctor()
// 0x000002D6 System.Void Sirenix.Serialization.SerializableFormatter`1::.cctor()
// 0x000002D7 T Sirenix.Serialization.SerializableFormatter`1::GetUninitializedObject()
// 0x000002D8 System.Void Sirenix.Serialization.SerializableFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x000002D9 System.Void Sirenix.Serialization.SerializableFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x000002DA System.Runtime.Serialization.SerializationInfo Sirenix.Serialization.SerializableFormatter`1::ReadSerializationInfo(Sirenix.Serialization.IDataReader)
// 0x000002DB System.Void Sirenix.Serialization.SerializableFormatter`1::WriteSerializationInfo(System.Runtime.Serialization.SerializationInfo,Sirenix.Serialization.IDataWriter)
// 0x000002DC System.Void Sirenix.Serialization.SerializableFormatter`1::.ctor()
// 0x000002DD System.Void Sirenix.Serialization.SerializableFormatter`1/<>c__DisplayClass2_0::.ctor()
// 0x000002DE T Sirenix.Serialization.SerializableFormatter`1/<>c__DisplayClass2_0::<.cctor>b__0(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000002DF System.Void Sirenix.Serialization.TimeSpanFormatter::Read(System.TimeSpan&,Sirenix.Serialization.IDataReader)
extern void TimeSpanFormatter_Read_m53B0D1FE0F62588C8A68861DEDACC74D69EA7526 (void);
// 0x000002E0 System.Void Sirenix.Serialization.TimeSpanFormatter::Write(System.TimeSpan&,Sirenix.Serialization.IDataWriter)
extern void TimeSpanFormatter_Write_m94304EDED26C10825038E8D17836F05E14DA4346 (void);
// 0x000002E1 System.Void Sirenix.Serialization.TimeSpanFormatter::.ctor()
extern void TimeSpanFormatter__ctor_m68C98C23B9613C27EB5E39EE0BC4D059515DD3C2 (void);
// 0x000002E2 System.Void Sirenix.Serialization.TypeFormatter::Read(System.Type&,Sirenix.Serialization.IDataReader)
extern void TypeFormatter_Read_mA4D03F1AFAF413F0EF38BF4386FFDEDE91F51FDA (void);
// 0x000002E3 System.Void Sirenix.Serialization.TypeFormatter::Write(System.Type&,Sirenix.Serialization.IDataWriter)
extern void TypeFormatter_Write_m3E72764ED302CF03A5B7684C8130A90442D5C4D3 (void);
// 0x000002E4 System.Type Sirenix.Serialization.TypeFormatter::GetUninitializedObject()
extern void TypeFormatter_GetUninitializedObject_m7668EA7607A2A819DFCD62797812C5F74B4813AB (void);
// 0x000002E5 System.Void Sirenix.Serialization.TypeFormatter::.ctor()
extern void TypeFormatter__ctor_m6A3965C47CCF9DCC76DD94E9860EBEBB4F5B0CB3 (void);
// 0x000002E6 System.Void Sirenix.Serialization.CustomSerializationPolicy::.ctor(System.String,System.Boolean,System.Func`2<System.Reflection.MemberInfo,System.Boolean>)
extern void CustomSerializationPolicy__ctor_mF0E9B48B7CA3F0F2BCAC8937D7867F6BAA1E0863 (void);
// 0x000002E7 System.String Sirenix.Serialization.CustomSerializationPolicy::get_ID()
extern void CustomSerializationPolicy_get_ID_mD91FBD9D8A69EE3FAEA6D9D83C168E4F90999CC8 (void);
// 0x000002E8 System.Boolean Sirenix.Serialization.CustomSerializationPolicy::get_AllowNonSerializableTypes()
extern void CustomSerializationPolicy_get_AllowNonSerializableTypes_m7CCBE44B1480F9CE9120D61A88404D7A8B90CF33 (void);
// 0x000002E9 System.Boolean Sirenix.Serialization.CustomSerializationPolicy::ShouldSerializeMember(System.Reflection.MemberInfo)
extern void CustomSerializationPolicy_ShouldSerializeMember_mA10AB4E26282F9E85005770B3427AFB5026D8568 (void);
// 0x000002EA System.Void Sirenix.Serialization.AlwaysFormatsSelfAttribute::.ctor()
extern void AlwaysFormatsSelfAttribute__ctor_m01671ABC744F345DE0650CF9BFB66965CE918A6F (void);
// 0x000002EB System.Void Sirenix.Serialization.CustomFormatterAttribute::.ctor()
extern void CustomFormatterAttribute__ctor_mF4344679DBB6AFBE8848D6F15134181BEB721D7A (void);
// 0x000002EC System.Void Sirenix.Serialization.CustomFormatterAttribute::.ctor(System.Int32)
extern void CustomFormatterAttribute__ctor_m10C204AA0E45D48ECF6E1B8DA75B437EE0B4F9C5 (void);
// 0x000002ED System.Void Sirenix.Serialization.CustomGenericFormatterAttribute::.ctor(System.Type,System.Int32)
extern void CustomGenericFormatterAttribute__ctor_m67FCB3E66F68E2A28591F2E22D9F2D4478E36AFE (void);
// 0x000002EE System.Void Sirenix.Serialization.BindTypeNameToTypeAttribute::.ctor(System.String,System.Type)
extern void BindTypeNameToTypeAttribute__ctor_m881CE0D2924F38FE487A307B0D0481654FD32AD3 (void);
// 0x000002EF System.Void Sirenix.Serialization.DefaultSerializationBinder::.cctor()
extern void DefaultSerializationBinder__cctor_m2BA09ADCD6A5C21FB5CBD178709813A404545BBC (void);
// 0x000002F0 System.Void Sirenix.Serialization.DefaultSerializationBinder::RegisterAllQueuedAssembliesRepeating()
extern void DefaultSerializationBinder_RegisterAllQueuedAssembliesRepeating_mA644CF00BDF642617102E9D52E30E4D3DA6EEA3F (void);
// 0x000002F1 System.Boolean Sirenix.Serialization.DefaultSerializationBinder::RegisterQueuedAssemblies()
extern void DefaultSerializationBinder_RegisterQueuedAssemblies_mBC5FA26D023E679B2BB5BA975E6B8067EED52C46 (void);
// 0x000002F2 System.Boolean Sirenix.Serialization.DefaultSerializationBinder::RegisterQueuedAssemblyLoadEvents()
extern void DefaultSerializationBinder_RegisterQueuedAssemblyLoadEvents_m6FA3398A3D26D3975E6FE7F841DA7779F598E360 (void);
// 0x000002F3 System.Void Sirenix.Serialization.DefaultSerializationBinder::RegisterAssembly(System.Reflection.Assembly)
extern void DefaultSerializationBinder_RegisterAssembly_m47B9087EAEED274AB85765B0F97F17CDCCAF8631 (void);
// 0x000002F4 System.String Sirenix.Serialization.DefaultSerializationBinder::BindToName(System.Type,Sirenix.Serialization.DebugContext)
extern void DefaultSerializationBinder_BindToName_mC1A6799C4958E6C8311103BC0E26DC8E009DEB7A (void);
// 0x000002F5 System.Boolean Sirenix.Serialization.DefaultSerializationBinder::ContainsType(System.String)
extern void DefaultSerializationBinder_ContainsType_m0097D27B7BD36234F6CE558F2BBA2BCF30027A85 (void);
// 0x000002F6 System.Type Sirenix.Serialization.DefaultSerializationBinder::BindToType(System.String,Sirenix.Serialization.DebugContext)
extern void DefaultSerializationBinder_BindToType_m9B60651682307A6440742211189F8A4E92059FAB (void);
// 0x000002F7 System.Type Sirenix.Serialization.DefaultSerializationBinder::ParseTypeName(System.String,Sirenix.Serialization.DebugContext)
extern void DefaultSerializationBinder_ParseTypeName_m60799F5F3FEB97BFFEC254E7A000B992690F31DA (void);
// 0x000002F8 System.Void Sirenix.Serialization.DefaultSerializationBinder::ParseName(System.String,System.String&,System.String&)
extern void DefaultSerializationBinder_ParseName_mEE81027F8492E0061686C5701F0FDAF52171AAE9 (void);
// 0x000002F9 System.Type Sirenix.Serialization.DefaultSerializationBinder::ParseGenericAndOrArrayType(System.String,Sirenix.Serialization.DebugContext)
extern void DefaultSerializationBinder_ParseGenericAndOrArrayType_mF1FB286A39E3489BFB69F9AFD26D9C80AC4558DA (void);
// 0x000002FA System.Boolean Sirenix.Serialization.DefaultSerializationBinder::TryParseGenericAndOrArrayTypeName(System.String,System.String&,System.Boolean&,System.Collections.Generic.List`1<System.String>&,System.Boolean&,System.Int32&)
extern void DefaultSerializationBinder_TryParseGenericAndOrArrayTypeName_m63A65860D8573320D5DB9C8CC8F358D9FB1F4E03 (void);
// 0x000002FB System.Char Sirenix.Serialization.DefaultSerializationBinder::Peek(System.String,System.Int32,System.Int32)
extern void DefaultSerializationBinder_Peek_m57DF109450B8150FCB405E9C9758DE9EC4E50995 (void);
// 0x000002FC System.Boolean Sirenix.Serialization.DefaultSerializationBinder::ReadGenericArg(System.String,System.Int32&,System.String&)
extern void DefaultSerializationBinder_ReadGenericArg_m850F2FCB8ED0AF502377961C0EFD631DA9CF9685 (void);
// 0x000002FD System.Void Sirenix.Serialization.DefaultSerializationBinder::.ctor()
extern void DefaultSerializationBinder__ctor_mBD4FD9D22DA227762E230B0B96F54C0B51216161 (void);
// 0x000002FE System.Void Sirenix.Serialization.DefaultSerializationBinder/<>c::.cctor()
extern void U3CU3Ec__cctor_m420F65567DCDE63419AA582AF9C6D3317ED3F8E8 (void);
// 0x000002FF System.Void Sirenix.Serialization.DefaultSerializationBinder/<>c::.ctor()
extern void U3CU3Ec__ctor_mE7E82CAEA1C3D4F27E4F0F974D5151EC0166E907 (void);
// 0x00000300 System.Void Sirenix.Serialization.DefaultSerializationBinder/<>c::<.cctor>b__12_0(System.Object,System.AssemblyLoadEventArgs)
extern void U3CU3Ec_U3C_cctorU3Eb__12_0_m48E725AE2582F51909BF5BAFE94BBCC828BB7E1C (void);
// 0x00000301 System.Void Sirenix.Serialization.DeserializationContext::.ctor()
extern void DeserializationContext__ctor_m79D54C0E4356B35D7388C2D524F7F3CD53AF8698 (void);
// 0x00000302 System.Void Sirenix.Serialization.DeserializationContext::.ctor(System.Runtime.Serialization.StreamingContext)
extern void DeserializationContext__ctor_m95122F1819EA3773F68FDD54225A97417FBF6DBB (void);
// 0x00000303 System.Void Sirenix.Serialization.DeserializationContext::.ctor(System.Runtime.Serialization.FormatterConverter)
extern void DeserializationContext__ctor_m099CEE002CA35BC58B5376D29A8196F453A8593D (void);
// 0x00000304 System.Void Sirenix.Serialization.DeserializationContext::.ctor(System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.FormatterConverter)
extern void DeserializationContext__ctor_m9BED40C786E9A2148142BB290DD03B9415CB7DDC (void);
// 0x00000305 Sirenix.Serialization.TwoWaySerializationBinder Sirenix.Serialization.DeserializationContext::get_Binder()
extern void DeserializationContext_get_Binder_m0F7483ED7970C6864CC4AF19367B176D077754D8 (void);
// 0x00000306 System.Void Sirenix.Serialization.DeserializationContext::set_Binder(Sirenix.Serialization.TwoWaySerializationBinder)
extern void DeserializationContext_set_Binder_m6CD316DC0C08EA4983186B84EA3631AD513652AB (void);
// 0x00000307 Sirenix.Serialization.IExternalStringReferenceResolver Sirenix.Serialization.DeserializationContext::get_StringReferenceResolver()
extern void DeserializationContext_get_StringReferenceResolver_m7EC7BB3F21C03E172E695D6A0915EFF28168CD38 (void);
// 0x00000308 System.Void Sirenix.Serialization.DeserializationContext::set_StringReferenceResolver(Sirenix.Serialization.IExternalStringReferenceResolver)
extern void DeserializationContext_set_StringReferenceResolver_mAF3F1131432D3AC47708724561C1FDA47EB4856F (void);
// 0x00000309 Sirenix.Serialization.IExternalGuidReferenceResolver Sirenix.Serialization.DeserializationContext::get_GuidReferenceResolver()
extern void DeserializationContext_get_GuidReferenceResolver_m3FE8878B4E53FFC7653259668E6CB95A24813557 (void);
// 0x0000030A System.Void Sirenix.Serialization.DeserializationContext::set_GuidReferenceResolver(Sirenix.Serialization.IExternalGuidReferenceResolver)
extern void DeserializationContext_set_GuidReferenceResolver_m08ED3C754D5057DD5612B1EC68D211077EA7944A (void);
// 0x0000030B Sirenix.Serialization.IExternalIndexReferenceResolver Sirenix.Serialization.DeserializationContext::get_IndexReferenceResolver()
extern void DeserializationContext_get_IndexReferenceResolver_mA4280CD20359B1BC999EE911C429B29E45B5D155 (void);
// 0x0000030C System.Void Sirenix.Serialization.DeserializationContext::set_IndexReferenceResolver(Sirenix.Serialization.IExternalIndexReferenceResolver)
extern void DeserializationContext_set_IndexReferenceResolver_m514050C386067920670659B3F0C19DEF1F6F07FE (void);
// 0x0000030D System.Runtime.Serialization.StreamingContext Sirenix.Serialization.DeserializationContext::get_StreamingContext()
extern void DeserializationContext_get_StreamingContext_m76EF6F12E9E400F9E26F041E0E676D17B7C6857F (void);
// 0x0000030E System.Runtime.Serialization.IFormatterConverter Sirenix.Serialization.DeserializationContext::get_FormatterConverter()
extern void DeserializationContext_get_FormatterConverter_mEE5C9D74DFA980032F2141501E31B03BC56B37E6 (void);
// 0x0000030F Sirenix.Serialization.SerializationConfig Sirenix.Serialization.DeserializationContext::get_Config()
extern void DeserializationContext_get_Config_m7AA43C53F73F7BB081EE8594F43DCFCF654CD61B (void);
// 0x00000310 System.Void Sirenix.Serialization.DeserializationContext::set_Config(Sirenix.Serialization.SerializationConfig)
extern void DeserializationContext_set_Config_mB4D81343426C20E7ACEF01DA2B107774330921B9 (void);
// 0x00000311 System.Void Sirenix.Serialization.DeserializationContext::RegisterInternalReference(System.Int32,System.Object)
extern void DeserializationContext_RegisterInternalReference_m97967CCED634A7BC6B81094E7136A6FA540FDC89 (void);
// 0x00000312 System.Object Sirenix.Serialization.DeserializationContext::GetInternalReference(System.Int32)
extern void DeserializationContext_GetInternalReference_mBFF38146C554EA6725D9C99AAC8EA9F80E67A5B2 (void);
// 0x00000313 System.Object Sirenix.Serialization.DeserializationContext::GetExternalObject(System.Int32)
extern void DeserializationContext_GetExternalObject_m456A066739D167013EEA411007A7B120B462671F (void);
// 0x00000314 System.Object Sirenix.Serialization.DeserializationContext::GetExternalObject(System.Guid)
extern void DeserializationContext_GetExternalObject_m26A1BD8D927081962282FF1CBFC993309898685C (void);
// 0x00000315 System.Object Sirenix.Serialization.DeserializationContext::GetExternalObject(System.String)
extern void DeserializationContext_GetExternalObject_mBD215122F85C838A4B6BEAF0E8CD76CBFA774453 (void);
// 0x00000316 System.Void Sirenix.Serialization.DeserializationContext::Reset()
extern void DeserializationContext_Reset_mAF73339904ADA59E93E0C72698840540AA0D8B5B (void);
// 0x00000317 System.Void Sirenix.Serialization.DeserializationContext::Sirenix.Serialization.Utilities.ICacheNotificationReceiver.OnFreed()
extern void DeserializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnFreed_m4932D59DD37B39F55D072266A0452037C54B56A6 (void);
// 0x00000318 System.Void Sirenix.Serialization.DeserializationContext::Sirenix.Serialization.Utilities.ICacheNotificationReceiver.OnClaimed()
extern void DeserializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnClaimed_mE4EC7ED370A57EEDE03D39DB668A6DCFF1933FE6 (void);
// 0x00000319 System.Void Sirenix.Serialization.ExcludeDataFromInspectorAttribute::.ctor()
extern void ExcludeDataFromInspectorAttribute__ctor_m7A8B44B57C2485D75808F50E6FA5B5C5AA029A5B (void);
// 0x0000031A Sirenix.Serialization.IExternalGuidReferenceResolver Sirenix.Serialization.IExternalGuidReferenceResolver::get_NextResolver()
// 0x0000031B System.Void Sirenix.Serialization.IExternalGuidReferenceResolver::set_NextResolver(Sirenix.Serialization.IExternalGuidReferenceResolver)
// 0x0000031C System.Boolean Sirenix.Serialization.IExternalGuidReferenceResolver::TryResolveReference(System.Guid,System.Object&)
// 0x0000031D System.Boolean Sirenix.Serialization.IExternalGuidReferenceResolver::CanReference(System.Object,System.Guid&)
// 0x0000031E System.Boolean Sirenix.Serialization.IExternalIndexReferenceResolver::TryResolveReference(System.Int32,System.Object&)
// 0x0000031F System.Boolean Sirenix.Serialization.IExternalIndexReferenceResolver::CanReference(System.Object,System.Int32&)
// 0x00000320 Sirenix.Serialization.IExternalStringReferenceResolver Sirenix.Serialization.IExternalStringReferenceResolver::get_NextResolver()
// 0x00000321 System.Void Sirenix.Serialization.IExternalStringReferenceResolver::set_NextResolver(Sirenix.Serialization.IExternalStringReferenceResolver)
// 0x00000322 System.Boolean Sirenix.Serialization.IExternalStringReferenceResolver::TryResolveReference(System.String,System.Object&)
// 0x00000323 System.Boolean Sirenix.Serialization.IExternalStringReferenceResolver::CanReference(System.Object,System.String&)
// 0x00000324 System.Void Sirenix.Serialization.ISelfFormatter::Serialize(Sirenix.Serialization.IDataWriter)
// 0x00000325 System.Void Sirenix.Serialization.ISelfFormatter::Deserialize(Sirenix.Serialization.IDataReader)
// 0x00000326 System.String Sirenix.Serialization.ISerializationPolicy::get_ID()
// 0x00000327 System.Boolean Sirenix.Serialization.ISerializationPolicy::get_AllowNonSerializableTypes()
// 0x00000328 System.Boolean Sirenix.Serialization.ISerializationPolicy::ShouldSerializeMember(System.Reflection.MemberInfo)
// 0x00000329 System.Void Sirenix.Serialization.NodeInfo::.ctor(System.String,System.Int32,System.Type,System.Boolean)
extern void NodeInfo__ctor_m386DC2E5B9CA08AC916FB9E905B8F8E6C4B0414D (void);
// 0x0000032A System.Void Sirenix.Serialization.NodeInfo::.ctor(System.Boolean)
extern void NodeInfo__ctor_m0F7216E0579077DCB8FFEC72B74FBEF04C95A4BF (void);
// 0x0000032B System.Boolean Sirenix.Serialization.NodeInfo::op_Equality(Sirenix.Serialization.NodeInfo,Sirenix.Serialization.NodeInfo)
extern void NodeInfo_op_Equality_m382CD7C5A80AEFB90958B6912ACEC88B5EC273E6 (void);
// 0x0000032C System.Boolean Sirenix.Serialization.NodeInfo::op_Inequality(Sirenix.Serialization.NodeInfo,Sirenix.Serialization.NodeInfo)
extern void NodeInfo_op_Inequality_mF36F9D098733E0281D8137D061AA92800DCAFC67 (void);
// 0x0000032D System.Boolean Sirenix.Serialization.NodeInfo::Equals(System.Object)
extern void NodeInfo_Equals_m1B4D737F1DF0003BA4319E5899E8410B46EFB0D3 (void);
// 0x0000032E System.Int32 Sirenix.Serialization.NodeInfo::GetHashCode()
extern void NodeInfo_GetHashCode_mB34C81EAD31FC0CBF8FDE38F4B7B132ADACF72BA (void);
// 0x0000032F System.Void Sirenix.Serialization.NodeInfo::.cctor()
extern void NodeInfo__cctor_m56D181C999D2B94FEDFC1AEDB35F892F1980E299 (void);
// 0x00000330 System.String Sirenix.Serialization.PreviouslySerializedAsAttribute::get_Name()
extern void PreviouslySerializedAsAttribute_get_Name_m263CA2EFA9F5FF153F46444AB2F1764C8B5AEF06 (void);
// 0x00000331 System.Void Sirenix.Serialization.PreviouslySerializedAsAttribute::set_Name(System.String)
extern void PreviouslySerializedAsAttribute_set_Name_m9F8AAC3B45CE7004CE1EBB2F89D0AD7549F8BC79 (void);
// 0x00000332 System.Void Sirenix.Serialization.PreviouslySerializedAsAttribute::.ctor(System.String)
extern void PreviouslySerializedAsAttribute__ctor_mD3CD2C7AF3AA5CA89F18D8A0D141FAC2C52020C7 (void);
// 0x00000333 System.Void Sirenix.Serialization.SerializationAbortException::.ctor(System.String)
extern void SerializationAbortException__ctor_m0A554B480F0DA7439C460F7FF1E147148E90087B (void);
// 0x00000334 System.Void Sirenix.Serialization.SerializationAbortException::.ctor(System.String,System.Exception)
extern void SerializationAbortException__ctor_m04E5BD4C6547BF155AF6D8202E4F74DE9046A1CE (void);
// 0x00000335 System.Void Sirenix.Serialization.SerializationConfig::.ctor()
extern void SerializationConfig__ctor_mAD8DBBEDCEE62EF4A1DB3C0DFBA20C987C752D36 (void);
// 0x00000336 Sirenix.Serialization.ISerializationPolicy Sirenix.Serialization.SerializationConfig::get_SerializationPolicy()
extern void SerializationConfig_get_SerializationPolicy_mF5A346026244681F0BE0BF811FC742D029329C99 (void);
// 0x00000337 System.Void Sirenix.Serialization.SerializationConfig::set_SerializationPolicy(Sirenix.Serialization.ISerializationPolicy)
extern void SerializationConfig_set_SerializationPolicy_mF3B26A684D5231A1FE1B4E5228A6F28F981FB29E (void);
// 0x00000338 Sirenix.Serialization.DebugContext Sirenix.Serialization.SerializationConfig::get_DebugContext()
extern void SerializationConfig_get_DebugContext_m97D75096A9AFDE935F9029FA3028B64082E93BEC (void);
// 0x00000339 System.Void Sirenix.Serialization.SerializationConfig::set_DebugContext(Sirenix.Serialization.DebugContext)
extern void SerializationConfig_set_DebugContext_mE32D277747876FEAF10C107ABEB246C94656D58F (void);
// 0x0000033A System.Void Sirenix.Serialization.SerializationConfig::ResetToDefault()
extern void SerializationConfig_ResetToDefault_mF52B8EF79E6101B0B381BE43F6907860FBADCF8A (void);
// 0x0000033B Sirenix.Serialization.ILogger Sirenix.Serialization.DebugContext::get_Logger()
extern void DebugContext_get_Logger_m48999B60566E0CA1F4BE2FBF1FFC4AA400403861 (void);
// 0x0000033C System.Void Sirenix.Serialization.DebugContext::set_Logger(Sirenix.Serialization.ILogger)
extern void DebugContext_set_Logger_m323E707822A94C5B246E49BF83B54A08F7D3A1E3 (void);
// 0x0000033D Sirenix.Serialization.LoggingPolicy Sirenix.Serialization.DebugContext::get_LoggingPolicy()
extern void DebugContext_get_LoggingPolicy_m9C0F1780DA9D0F18397F88B487D2B6261D43E3EB (void);
// 0x0000033E System.Void Sirenix.Serialization.DebugContext::set_LoggingPolicy(Sirenix.Serialization.LoggingPolicy)
extern void DebugContext_set_LoggingPolicy_mBE96DC6AF7DE44E5C6DB8DAFD07C7F3AA87C3CA8 (void);
// 0x0000033F Sirenix.Serialization.ErrorHandlingPolicy Sirenix.Serialization.DebugContext::get_ErrorHandlingPolicy()
extern void DebugContext_get_ErrorHandlingPolicy_mF069F8E11DB73283D51BC497AF7C5C00F4006318 (void);
// 0x00000340 System.Void Sirenix.Serialization.DebugContext::set_ErrorHandlingPolicy(Sirenix.Serialization.ErrorHandlingPolicy)
extern void DebugContext_set_ErrorHandlingPolicy_mA11018C68636D5DB0BB8B316DA42CBE0765E3054 (void);
// 0x00000341 System.Void Sirenix.Serialization.DebugContext::LogWarning(System.String)
extern void DebugContext_LogWarning_m0A9E235C81573239C62B3417BBA20A6773320657 (void);
// 0x00000342 System.Void Sirenix.Serialization.DebugContext::LogError(System.String)
extern void DebugContext_LogError_mB7964B4F0CB8C00D8B9CCC91037527925CBB3362 (void);
// 0x00000343 System.Void Sirenix.Serialization.DebugContext::LogException(System.Exception)
extern void DebugContext_LogException_m05D07140DCF15AC718B731085DF5B968770B5E0F (void);
// 0x00000344 System.Void Sirenix.Serialization.DebugContext::ResetToDefault()
extern void DebugContext_ResetToDefault_m2C706BAD23AC009A673BB3BEC3621644E6D85DBA (void);
// 0x00000345 System.Void Sirenix.Serialization.DebugContext::.ctor()
extern void DebugContext__ctor_m78606858472F4B49FFD979C5B54ADD088A63F930 (void);
// 0x00000346 System.Void Sirenix.Serialization.SerializationContext::.ctor()
extern void SerializationContext__ctor_m465542BACCDD5D5ED7ADEE78D5C01FFDB22E4203 (void);
// 0x00000347 System.Void Sirenix.Serialization.SerializationContext::.ctor(System.Runtime.Serialization.StreamingContext)
extern void SerializationContext__ctor_mDF9626225DF127D4FDEA3420BC9CBF09BCED0A88 (void);
// 0x00000348 System.Void Sirenix.Serialization.SerializationContext::.ctor(System.Runtime.Serialization.FormatterConverter)
extern void SerializationContext__ctor_m9915F037800ED44484225B8AC6F316F7DBE8BDE3 (void);
// 0x00000349 System.Void Sirenix.Serialization.SerializationContext::.ctor(System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.FormatterConverter)
extern void SerializationContext__ctor_mC30D75D5C6279F2655F233D455050A081D046EC3 (void);
// 0x0000034A Sirenix.Serialization.TwoWaySerializationBinder Sirenix.Serialization.SerializationContext::get_Binder()
extern void SerializationContext_get_Binder_m117D9C9DCBC078B3749583A30C4FBC19512CA08D (void);
// 0x0000034B System.Void Sirenix.Serialization.SerializationContext::set_Binder(Sirenix.Serialization.TwoWaySerializationBinder)
extern void SerializationContext_set_Binder_mB35866E4C6AA31FC78A0305C677ACE97BBC4EDC8 (void);
// 0x0000034C System.Runtime.Serialization.StreamingContext Sirenix.Serialization.SerializationContext::get_StreamingContext()
extern void SerializationContext_get_StreamingContext_m2B673D425AA0BDA4A4704CDAC540A1765AA922EA (void);
// 0x0000034D System.Runtime.Serialization.IFormatterConverter Sirenix.Serialization.SerializationContext::get_FormatterConverter()
extern void SerializationContext_get_FormatterConverter_mB53B251AD78B4CE1D6B5A2A884ABC1A52102D79E (void);
// 0x0000034E Sirenix.Serialization.IExternalIndexReferenceResolver Sirenix.Serialization.SerializationContext::get_IndexReferenceResolver()
extern void SerializationContext_get_IndexReferenceResolver_mB03F8888875822A25310186B3162C1D54F507DDF (void);
// 0x0000034F System.Void Sirenix.Serialization.SerializationContext::set_IndexReferenceResolver(Sirenix.Serialization.IExternalIndexReferenceResolver)
extern void SerializationContext_set_IndexReferenceResolver_m302A313D8B5940631CE1080D63B031BB72A9F37B (void);
// 0x00000350 Sirenix.Serialization.IExternalStringReferenceResolver Sirenix.Serialization.SerializationContext::get_StringReferenceResolver()
extern void SerializationContext_get_StringReferenceResolver_m117290E2FC39BBBDC598C71595B9DEE75E23734B (void);
// 0x00000351 System.Void Sirenix.Serialization.SerializationContext::set_StringReferenceResolver(Sirenix.Serialization.IExternalStringReferenceResolver)
extern void SerializationContext_set_StringReferenceResolver_mE6623E2962600E1B37CC96F15013099103AC441D (void);
// 0x00000352 Sirenix.Serialization.IExternalGuidReferenceResolver Sirenix.Serialization.SerializationContext::get_GuidReferenceResolver()
extern void SerializationContext_get_GuidReferenceResolver_m623454ADCBFCD5198104AB855E0AD913427AA714 (void);
// 0x00000353 System.Void Sirenix.Serialization.SerializationContext::set_GuidReferenceResolver(Sirenix.Serialization.IExternalGuidReferenceResolver)
extern void SerializationContext_set_GuidReferenceResolver_m1B9940D0DF827220AAA92094670429882C752BE1 (void);
// 0x00000354 Sirenix.Serialization.SerializationConfig Sirenix.Serialization.SerializationContext::get_Config()
extern void SerializationContext_get_Config_m4A193016B5537E6E7B84CA0E5D034C17545C9A6A (void);
// 0x00000355 System.Void Sirenix.Serialization.SerializationContext::set_Config(Sirenix.Serialization.SerializationConfig)
extern void SerializationContext_set_Config_mD3AE305F6F6D5A2202718D5850A54A3D110894D8 (void);
// 0x00000356 System.Boolean Sirenix.Serialization.SerializationContext::TryGetInternalReferenceId(System.Object,System.Int32&)
extern void SerializationContext_TryGetInternalReferenceId_mAE5155ED3C0CA798929299038DEA6AEE6303DCA9 (void);
// 0x00000357 System.Boolean Sirenix.Serialization.SerializationContext::TryRegisterInternalReference(System.Object,System.Int32&)
extern void SerializationContext_TryRegisterInternalReference_mBF8ADD20EB3D72AC54B1A66A1ED8A1B454BC8BFC (void);
// 0x00000358 System.Boolean Sirenix.Serialization.SerializationContext::TryRegisterExternalReference(System.Object,System.Int32&)
extern void SerializationContext_TryRegisterExternalReference_mD6E5EA399D9A9D42E82D7103111F21C34BDFF82C (void);
// 0x00000359 System.Boolean Sirenix.Serialization.SerializationContext::TryRegisterExternalReference(System.Object,System.Guid&)
extern void SerializationContext_TryRegisterExternalReference_m756F497F8EBB10CF732F65439002F5C72B278860 (void);
// 0x0000035A System.Boolean Sirenix.Serialization.SerializationContext::TryRegisterExternalReference(System.Object,System.String&)
extern void SerializationContext_TryRegisterExternalReference_m9BCE1D1E2C394CD50C4302F18007A169890C01B3 (void);
// 0x0000035B System.Void Sirenix.Serialization.SerializationContext::ResetInternalReferences()
extern void SerializationContext_ResetInternalReferences_mF3482C4F865E151C2D52ACE70D50B91DC8AB1C2C (void);
// 0x0000035C System.Void Sirenix.Serialization.SerializationContext::ResetToDefault()
extern void SerializationContext_ResetToDefault_mE5FEC8F28794227A89ED1F1058BD02E83DFE8E7C (void);
// 0x0000035D System.Void Sirenix.Serialization.SerializationContext::Sirenix.Serialization.Utilities.ICacheNotificationReceiver.OnFreed()
extern void SerializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnFreed_m0A789691C06A7855FC6EB40E379B02A3B89F68E7 (void);
// 0x0000035E System.Void Sirenix.Serialization.SerializationContext::Sirenix.Serialization.Utilities.ICacheNotificationReceiver.OnClaimed()
extern void SerializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnClaimed_m65B3AAABCC8B96DFB7D3BF92083CAB9C22BF6385 (void);
// 0x0000035F System.Void Sirenix.Serialization.OdinSerializeAttribute::.ctor()
extern void OdinSerializeAttribute__ctor_mFCE6089EC2D42338251875B38D732C44152030CC (void);
// 0x00000360 System.String Sirenix.Serialization.TwoWaySerializationBinder::BindToName(System.Type,Sirenix.Serialization.DebugContext)
// 0x00000361 System.Type Sirenix.Serialization.TwoWaySerializationBinder::BindToType(System.String,Sirenix.Serialization.DebugContext)
// 0x00000362 System.Boolean Sirenix.Serialization.TwoWaySerializationBinder::ContainsType(System.String)
// 0x00000363 System.Void Sirenix.Serialization.TwoWaySerializationBinder::.ctor()
extern void TwoWaySerializationBinder__ctor_m5F3BB749B2C88E49B8EC404704031865AA9CB105 (void);
// 0x00000364 System.Void Sirenix.Serialization.TwoWaySerializationBinder::.cctor()
extern void TwoWaySerializationBinder__cctor_m7E071F538C58C102EC5B3F6D28A321781D836D2B (void);
// 0x00000365 System.Boolean Sirenix.Serialization.SerializationPolicies::TryGetByID(System.String,Sirenix.Serialization.ISerializationPolicy&)
extern void SerializationPolicies_TryGetByID_m632039DD424C95823E8B9F8B0FA19617898B2E0A (void);
// 0x00000366 Sirenix.Serialization.ISerializationPolicy Sirenix.Serialization.SerializationPolicies::get_Everything()
extern void SerializationPolicies_get_Everything_m6E1F040F2A8B465590FB01AED0975790DB0A1B10 (void);
// 0x00000367 Sirenix.Serialization.ISerializationPolicy Sirenix.Serialization.SerializationPolicies::get_Unity()
extern void SerializationPolicies_get_Unity_m6CDEBDE6A0B109C6EFD586B741021A95CF9B69C8 (void);
// 0x00000368 Sirenix.Serialization.ISerializationPolicy Sirenix.Serialization.SerializationPolicies::get_Strict()
extern void SerializationPolicies_get_Strict_mAF701C8A7568E68DBB0CED90A70BE1800599699F (void);
// 0x00000369 System.Void Sirenix.Serialization.SerializationPolicies::.cctor()
extern void SerializationPolicies__cctor_mB76CCDFC84B5F5D7FA15CC082055AD5E39EC1BCC (void);
// 0x0000036A System.Void Sirenix.Serialization.SerializationPolicies/<>c::.cctor()
extern void U3CU3Ec__cctor_mEEC330DB547EF30AE7047211A80788EF42E7265C (void);
// 0x0000036B System.Void Sirenix.Serialization.SerializationPolicies/<>c::.ctor()
extern void U3CU3Ec__ctor_m81E0C5F4C07ED8E07D4CDFEFA1EE20935DDEF3F0 (void);
// 0x0000036C System.Boolean Sirenix.Serialization.SerializationPolicies/<>c::<get_Everything>b__6_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3Cget_EverythingU3Eb__6_0_mE75E4096BA031B44F19CC2B40A7FC57A133FC3D7 (void);
// 0x0000036D System.Boolean Sirenix.Serialization.SerializationPolicies/<>c::<get_Strict>b__10_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3Cget_StrictU3Eb__10_0_m773E6057F688D26A88CFD3D82BE6EDD224699BC0 (void);
// 0x0000036E System.Void Sirenix.Serialization.SerializationPolicies/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mA6CD2F2904650AED08FD9E18ED43BD24B758340C (void);
// 0x0000036F System.Boolean Sirenix.Serialization.SerializationPolicies/<>c__DisplayClass8_0::<get_Unity>b__0(System.Reflection.MemberInfo)
extern void U3CU3Ec__DisplayClass8_0_U3Cget_UnityU3Eb__0_m675C89EF4B9CB12CCE23F043D944CAD00349439D (void);
// 0x00000370 System.Boolean Sirenix.Serialization.BooleanSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void BooleanSerializer_ReadValue_mCFF038F984F2DA0BFA61AB6EC40B0C574F9AAA2E (void);
// 0x00000371 System.Void Sirenix.Serialization.BooleanSerializer::WriteValue(System.String,System.Boolean,Sirenix.Serialization.IDataWriter)
extern void BooleanSerializer_WriteValue_mE4A25E48982846248623480F4DCBBAE91FD0E42B (void);
// 0x00000372 System.Void Sirenix.Serialization.BooleanSerializer::.ctor()
extern void BooleanSerializer__ctor_m59A6F830DCEAD036D84C0118DFF1FD1FEDC067FB (void);
// 0x00000373 System.Byte Sirenix.Serialization.ByteSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void ByteSerializer_ReadValue_m8BF8AF832346C45A52CEC5A307A929EC66736476 (void);
// 0x00000374 System.Void Sirenix.Serialization.ByteSerializer::WriteValue(System.String,System.Byte,Sirenix.Serialization.IDataWriter)
extern void ByteSerializer_WriteValue_m0832A20094BDEF69D5A1661A4EA432417ABEEFF9 (void);
// 0x00000375 System.Void Sirenix.Serialization.ByteSerializer::.ctor()
extern void ByteSerializer__ctor_m216630A46A05498B8D4ACA1E97C3A46E804B622B (void);
// 0x00000376 System.Char Sirenix.Serialization.CharSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void CharSerializer_ReadValue_mF4BC0E710952A97A439D2D1AAD8047ADF75BF26C (void);
// 0x00000377 System.Void Sirenix.Serialization.CharSerializer::WriteValue(System.String,System.Char,Sirenix.Serialization.IDataWriter)
extern void CharSerializer_WriteValue_m6C8FED855EDFE7D629934FEE43BCC1AAAC793554 (void);
// 0x00000378 System.Void Sirenix.Serialization.CharSerializer::.ctor()
extern void CharSerializer__ctor_m64DC6FEA2E7069E240173F2C21D6AA37E5DF491B (void);
// 0x00000379 T Sirenix.Serialization.ComplexTypeSerializer`1::ReadValue(Sirenix.Serialization.IDataReader)
// 0x0000037A Sirenix.Serialization.IFormatter`1<T> Sirenix.Serialization.ComplexTypeSerializer`1::GetBaseFormatter(Sirenix.Serialization.ISerializationPolicy)
// 0x0000037B System.Void Sirenix.Serialization.ComplexTypeSerializer`1::WriteValue(System.String,T,Sirenix.Serialization.IDataWriter)
// 0x0000037C System.Void Sirenix.Serialization.ComplexTypeSerializer`1::.ctor()
// 0x0000037D System.Void Sirenix.Serialization.ComplexTypeSerializer`1::.cctor()
// 0x0000037E System.Decimal Sirenix.Serialization.DecimalSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void DecimalSerializer_ReadValue_m6322CC7A05AD6B27D3FEC2A13D435E02658BDE16 (void);
// 0x0000037F System.Void Sirenix.Serialization.DecimalSerializer::WriteValue(System.String,System.Decimal,Sirenix.Serialization.IDataWriter)
extern void DecimalSerializer_WriteValue_mCB26C51161922165F9A4629D9DD6A5884C091144 (void);
// 0x00000380 System.Void Sirenix.Serialization.DecimalSerializer::.ctor()
extern void DecimalSerializer__ctor_mA6C875075A23C54B7513EC59EDBEF419D44ABAFA (void);
// 0x00000381 System.Double Sirenix.Serialization.DoubleSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void DoubleSerializer_ReadValue_mDD11B4B7BE7A9581FE67605168A01EBFBDC85B07 (void);
// 0x00000382 System.Void Sirenix.Serialization.DoubleSerializer::WriteValue(System.String,System.Double,Sirenix.Serialization.IDataWriter)
extern void DoubleSerializer_WriteValue_mB3290324D2E572C9FCEDD08AD2B6377DC9E89B60 (void);
// 0x00000383 System.Void Sirenix.Serialization.DoubleSerializer::.ctor()
extern void DoubleSerializer__ctor_m3D0501BF4026EA2513AA219C2604D9E962FD5193 (void);
// 0x00000384 System.Void Sirenix.Serialization.EnumSerializer`1::.cctor()
// 0x00000385 T Sirenix.Serialization.EnumSerializer`1::ReadValue(Sirenix.Serialization.IDataReader)
// 0x00000386 System.Void Sirenix.Serialization.EnumSerializer`1::WriteValue(System.String,T,Sirenix.Serialization.IDataWriter)
// 0x00000387 System.Void Sirenix.Serialization.EnumSerializer`1::.ctor()
// 0x00000388 System.Guid Sirenix.Serialization.GuidSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void GuidSerializer_ReadValue_m3203D1C6AF1AC55EAF62012C95BA91988AA4557E (void);
// 0x00000389 System.Void Sirenix.Serialization.GuidSerializer::WriteValue(System.String,System.Guid,Sirenix.Serialization.IDataWriter)
extern void GuidSerializer_WriteValue_mD36492F48494FE195A213187027A7FD8574E6B09 (void);
// 0x0000038A System.Void Sirenix.Serialization.GuidSerializer::.ctor()
extern void GuidSerializer__ctor_mC3750BC7A9DD4484F47DDFBB2F143A833E543DE0 (void);
// 0x0000038B System.Int16 Sirenix.Serialization.Int16Serializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void Int16Serializer_ReadValue_mCA6DACE37FEFC3AA23C07157A7FC0D59CF72C183 (void);
// 0x0000038C System.Void Sirenix.Serialization.Int16Serializer::WriteValue(System.String,System.Int16,Sirenix.Serialization.IDataWriter)
extern void Int16Serializer_WriteValue_m6B3C3AB34E379EE6DFA296B6446109B6AFAB7447 (void);
// 0x0000038D System.Void Sirenix.Serialization.Int16Serializer::.ctor()
extern void Int16Serializer__ctor_m429F28A5BD505D9ACCB85A608B620C3A71CA1E38 (void);
// 0x0000038E System.Int32 Sirenix.Serialization.Int32Serializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void Int32Serializer_ReadValue_mCB2D4CF9E5807E08785C66B947C2E106A2B0E452 (void);
// 0x0000038F System.Void Sirenix.Serialization.Int32Serializer::WriteValue(System.String,System.Int32,Sirenix.Serialization.IDataWriter)
extern void Int32Serializer_WriteValue_m37D76212ACE61D70E10B49CDF7E0AEA78958644B (void);
// 0x00000390 System.Void Sirenix.Serialization.Int32Serializer::.ctor()
extern void Int32Serializer__ctor_m9568EBF076A93D4AE5F5B340BDA99D0B47A3F2AF (void);
// 0x00000391 System.Int64 Sirenix.Serialization.Int64Serializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void Int64Serializer_ReadValue_m7BF2C0C7F98EEBB6507FA7724B0A9A048025096F (void);
// 0x00000392 System.Void Sirenix.Serialization.Int64Serializer::WriteValue(System.String,System.Int64,Sirenix.Serialization.IDataWriter)
extern void Int64Serializer_WriteValue_m54B6142045EBE1D1A21635714BC61B007DBB5F4D (void);
// 0x00000393 System.Void Sirenix.Serialization.Int64Serializer::.ctor()
extern void Int64Serializer__ctor_mC4B0EF87AE736BFAB7B30239F1B679B3A2E7EFA4 (void);
// 0x00000394 System.IntPtr Sirenix.Serialization.IntPtrSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void IntPtrSerializer_ReadValue_mE850893CD4346CD043ACE6679AF70A4B7A78A895 (void);
// 0x00000395 System.Void Sirenix.Serialization.IntPtrSerializer::WriteValue(System.String,System.IntPtr,Sirenix.Serialization.IDataWriter)
extern void IntPtrSerializer_WriteValue_m51BBA05BF5BF7C3933AF019C42C1DE4D8FA64462 (void);
// 0x00000396 System.Void Sirenix.Serialization.IntPtrSerializer::.ctor()
extern void IntPtrSerializer__ctor_mBDC0B2692B942809C48B6D0474458A7E190459CA (void);
// 0x00000397 System.SByte Sirenix.Serialization.SByteSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void SByteSerializer_ReadValue_m623DC10D88CA711ACB231BB27D9F8914C73C5032 (void);
// 0x00000398 System.Void Sirenix.Serialization.SByteSerializer::WriteValue(System.String,System.SByte,Sirenix.Serialization.IDataWriter)
extern void SByteSerializer_WriteValue_m0E41081A69450250EF53C3E243486595D7BA7CA3 (void);
// 0x00000399 System.Void Sirenix.Serialization.SByteSerializer::.ctor()
extern void SByteSerializer__ctor_m542885A6614D6AE624E53FC1F9D8ED5244E76522 (void);
// 0x0000039A System.Void Sirenix.Serialization.Serializer::FireOnSerializedType(System.Type)
extern void Serializer_FireOnSerializedType_mF5F9E16719D18A6713CFFF4B21E70A85A6DA1FD8 (void);
// 0x0000039B Sirenix.Serialization.Serializer Sirenix.Serialization.Serializer::GetForValue(System.Object)
extern void Serializer_GetForValue_mEDEC8D49F84F770AC85808176CBADE6E3C7B7233 (void);
// 0x0000039C Sirenix.Serialization.Serializer`1<T> Sirenix.Serialization.Serializer::Get()
// 0x0000039D Sirenix.Serialization.Serializer Sirenix.Serialization.Serializer::Get(System.Type)
extern void Serializer_Get_m5984DDA5927847C360B93AF249FA4DC87F33B4AA (void);
// 0x0000039E System.Object Sirenix.Serialization.Serializer::ReadValueWeak(Sirenix.Serialization.IDataReader)
// 0x0000039F System.Void Sirenix.Serialization.Serializer::WriteValueWeak(System.Object,Sirenix.Serialization.IDataWriter)
extern void Serializer_WriteValueWeak_m8105FF46594C2DBDB86701AC111CBCA9BD14F452 (void);
// 0x000003A0 System.Void Sirenix.Serialization.Serializer::WriteValueWeak(System.String,System.Object,Sirenix.Serialization.IDataWriter)
// 0x000003A1 Sirenix.Serialization.Serializer Sirenix.Serialization.Serializer::Create(System.Type)
extern void Serializer_Create_m6F9FF6F90E9C6B4F4B2A50F1D90624E6576F2277 (void);
// 0x000003A2 System.Void Sirenix.Serialization.Serializer::LogAOTError(System.Type,System.ExecutionEngineException)
extern void Serializer_LogAOTError_mD1F886DA6099115E1E4A3803DB921FC37DFC6326 (void);
// 0x000003A3 System.Void Sirenix.Serialization.Serializer::.ctor()
extern void Serializer__ctor_m210B2F9B0595AE842C2F8D4871D8558FB803DE2A (void);
// 0x000003A4 System.Void Sirenix.Serialization.Serializer::.cctor()
extern void Serializer__cctor_mF52E0CDF54F46B6AA4F7BE8027D053142A04CB01 (void);
// 0x000003A5 System.Object Sirenix.Serialization.Serializer`1::ReadValueWeak(Sirenix.Serialization.IDataReader)
// 0x000003A6 System.Void Sirenix.Serialization.Serializer`1::WriteValueWeak(System.String,System.Object,Sirenix.Serialization.IDataWriter)
// 0x000003A7 T Sirenix.Serialization.Serializer`1::ReadValue(Sirenix.Serialization.IDataReader)
// 0x000003A8 System.Void Sirenix.Serialization.Serializer`1::WriteValue(T,Sirenix.Serialization.IDataWriter)
// 0x000003A9 System.Void Sirenix.Serialization.Serializer`1::WriteValue(System.String,T,Sirenix.Serialization.IDataWriter)
// 0x000003AA System.Void Sirenix.Serialization.Serializer`1::FireOnSerializedType()
// 0x000003AB System.Void Sirenix.Serialization.Serializer`1::.ctor()
// 0x000003AC System.Single Sirenix.Serialization.SingleSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void SingleSerializer_ReadValue_mEB4F684B5C5754DF8EB8E4F193E96B82130EE398 (void);
// 0x000003AD System.Void Sirenix.Serialization.SingleSerializer::WriteValue(System.String,System.Single,Sirenix.Serialization.IDataWriter)
extern void SingleSerializer_WriteValue_m291730C965CE8DB4400AA12DAE6A153543E9AE33 (void);
// 0x000003AE System.Void Sirenix.Serialization.SingleSerializer::.ctor()
extern void SingleSerializer__ctor_mB208CC340715FBE7B7E972EA8AF587B2E97302D3 (void);
// 0x000003AF System.String Sirenix.Serialization.StringSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void StringSerializer_ReadValue_m2EAFEBBE510BD8324E9D6566BC5376FDD5C4E857 (void);
// 0x000003B0 System.Void Sirenix.Serialization.StringSerializer::WriteValue(System.String,System.String,Sirenix.Serialization.IDataWriter)
extern void StringSerializer_WriteValue_m88A667FCD7DAD1562ADE2F05C1092D999BBF7FB5 (void);
// 0x000003B1 System.Void Sirenix.Serialization.StringSerializer::.ctor()
extern void StringSerializer__ctor_mCDF166D8A2EF2F2D01411C935A6436213BC1B1EC (void);
// 0x000003B2 System.UInt16 Sirenix.Serialization.UInt16Serializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void UInt16Serializer_ReadValue_m492C1F769B663D2CE09FEC1A52F5782265448D74 (void);
// 0x000003B3 System.Void Sirenix.Serialization.UInt16Serializer::WriteValue(System.String,System.UInt16,Sirenix.Serialization.IDataWriter)
extern void UInt16Serializer_WriteValue_mF8B4E7D4C00444E80772859ED4D8EDF46F2C878B (void);
// 0x000003B4 System.Void Sirenix.Serialization.UInt16Serializer::.ctor()
extern void UInt16Serializer__ctor_m6C5BFC0EBFA2D30A86DCBED81B8575A1AB3074BD (void);
// 0x000003B5 System.UInt32 Sirenix.Serialization.UInt32Serializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void UInt32Serializer_ReadValue_m47949AF83F5AFFF1650FB7A1F8C8EFA465BF225E (void);
// 0x000003B6 System.Void Sirenix.Serialization.UInt32Serializer::WriteValue(System.String,System.UInt32,Sirenix.Serialization.IDataWriter)
extern void UInt32Serializer_WriteValue_m91DB476BBA0831C56E44A3AFB4C1F09E9F93DB1C (void);
// 0x000003B7 System.Void Sirenix.Serialization.UInt32Serializer::.ctor()
extern void UInt32Serializer__ctor_mCA7FBF5E0B9D93A322D6691AE93016642642A70C (void);
// 0x000003B8 System.UInt64 Sirenix.Serialization.UInt64Serializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void UInt64Serializer_ReadValue_mD42155640F7F4B7300FFDF4F03987D6B1861E584 (void);
// 0x000003B9 System.Void Sirenix.Serialization.UInt64Serializer::WriteValue(System.String,System.UInt64,Sirenix.Serialization.IDataWriter)
extern void UInt64Serializer_WriteValue_m360649935A4959B67FFB58280EC8595C7E931853 (void);
// 0x000003BA System.Void Sirenix.Serialization.UInt64Serializer::.ctor()
extern void UInt64Serializer__ctor_mB3C7FDD0CDA72F5F61F24F67D029DE8E58C69BC1 (void);
// 0x000003BB System.UIntPtr Sirenix.Serialization.UIntPtrSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void UIntPtrSerializer_ReadValue_m2DA629D86C771D01D15500D863857B4FE21FF779 (void);
// 0x000003BC System.Void Sirenix.Serialization.UIntPtrSerializer::WriteValue(System.String,System.UIntPtr,Sirenix.Serialization.IDataWriter)
extern void UIntPtrSerializer_WriteValue_mADDA4920D3CEF328438A0CF4B91D68AD8264C5E5 (void);
// 0x000003BD System.Void Sirenix.Serialization.UIntPtrSerializer::.ctor()
extern void UIntPtrSerializer__ctor_m48B82496D8ADBE88AFC2D0FE4E84C91FB0D30C64 (void);
// 0x000003BE UnityEngine.AnimationCurve Sirenix.Serialization.AnimationCurveFormatter::GetUninitializedObject()
extern void AnimationCurveFormatter_GetUninitializedObject_m525E9854432B7F629A8B73E92CA1E399ABE61FBD (void);
// 0x000003BF System.Void Sirenix.Serialization.AnimationCurveFormatter::Read(UnityEngine.AnimationCurve&,Sirenix.Serialization.IDataReader)
extern void AnimationCurveFormatter_Read_m9741BE4BD3D04B8A51F82E5D8CF1FCF561FCBFBA (void);
// 0x000003C0 System.Void Sirenix.Serialization.AnimationCurveFormatter::Write(UnityEngine.AnimationCurve&,Sirenix.Serialization.IDataWriter)
extern void AnimationCurveFormatter_Write_mD1393081E16F96134CFB31EC47EE28302831D5CC (void);
// 0x000003C1 System.Void Sirenix.Serialization.AnimationCurveFormatter::.ctor()
extern void AnimationCurveFormatter__ctor_m5D07E6D2D92E3A6DF2065CBE415936652DAE0034 (void);
// 0x000003C2 System.Void Sirenix.Serialization.AnimationCurveFormatter::.cctor()
extern void AnimationCurveFormatter__cctor_m194E27568177533AD85BEB4544447F3377B04FB3 (void);
// 0x000003C3 System.Void Sirenix.Serialization.BoundsFormatter::Read(UnityEngine.Bounds&,Sirenix.Serialization.IDataReader)
extern void BoundsFormatter_Read_m00C3AFDF6A3C511F7A1D88B1173FBC15D409743E (void);
// 0x000003C4 System.Void Sirenix.Serialization.BoundsFormatter::Write(UnityEngine.Bounds&,Sirenix.Serialization.IDataWriter)
extern void BoundsFormatter_Write_mAB7D87A96809CFFEEC6E2F81E71C06778D7678D4 (void);
// 0x000003C5 System.Void Sirenix.Serialization.BoundsFormatter::.ctor()
extern void BoundsFormatter__ctor_m1AB5454445CA1C9460B92D0073838330657C465F (void);
// 0x000003C6 System.Void Sirenix.Serialization.BoundsFormatter::.cctor()
extern void BoundsFormatter__cctor_mE153A7E906EA57AD90FA6D387EF57FD055B414BC (void);
// 0x000003C7 System.Void Sirenix.Serialization.Color32Formatter::Read(UnityEngine.Color32&,Sirenix.Serialization.IDataReader)
extern void Color32Formatter_Read_m7E64AA98FB71F629E21381C20B5330553C2D43BB (void);
// 0x000003C8 System.Void Sirenix.Serialization.Color32Formatter::Write(UnityEngine.Color32&,Sirenix.Serialization.IDataWriter)
extern void Color32Formatter_Write_m8B5C6F94F8A2EA3FC3F33FF3F09415169E42B752 (void);
// 0x000003C9 System.Void Sirenix.Serialization.Color32Formatter::.ctor()
extern void Color32Formatter__ctor_m9E611EC8BF4FEB8D20BF29D102BA2CAF452A1919 (void);
// 0x000003CA System.Void Sirenix.Serialization.Color32Formatter::.cctor()
extern void Color32Formatter__cctor_m4E805C92E26D402C7D8EE8333897EA0031311F1D (void);
// 0x000003CB System.Boolean Sirenix.Serialization.ColorBlockFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void ColorBlockFormatterLocator_TryGetFormatter_m48935BCC8283E815DD4159FD3C1F7CAAA11B3D6F (void);
// 0x000003CC System.Void Sirenix.Serialization.ColorBlockFormatterLocator::.ctor()
extern void ColorBlockFormatterLocator__ctor_m4E729FB68563E13EE8F7C826F12C5EFD4E799F68 (void);
// 0x000003CD System.Void Sirenix.Serialization.ColorBlockFormatter`1::Read(T&,Sirenix.Serialization.IDataReader)
// 0x000003CE System.Void Sirenix.Serialization.ColorBlockFormatter`1::Write(T&,Sirenix.Serialization.IDataWriter)
// 0x000003CF System.Void Sirenix.Serialization.ColorBlockFormatter`1::.ctor()
// 0x000003D0 System.Void Sirenix.Serialization.ColorBlockFormatter`1::.cctor()
// 0x000003D1 System.Void Sirenix.Serialization.ColorFormatter::Read(UnityEngine.Color&,Sirenix.Serialization.IDataReader)
extern void ColorFormatter_Read_mAEC4258330E1B0A418586756F317E0FF267C352C (void);
// 0x000003D2 System.Void Sirenix.Serialization.ColorFormatter::Write(UnityEngine.Color&,Sirenix.Serialization.IDataWriter)
extern void ColorFormatter_Write_m19EE0C800609AE937AB6DAC7622145E36C1CC937 (void);
// 0x000003D3 System.Void Sirenix.Serialization.ColorFormatter::.ctor()
extern void ColorFormatter__ctor_m897F856F58996B3C26C8A7FF771EC29C57AA6EFA (void);
// 0x000003D4 System.Void Sirenix.Serialization.ColorFormatter::.cctor()
extern void ColorFormatter__cctor_m3F5BBFDC17997DB5F77232F8D941F52A62A252F8 (void);
// 0x000003D5 System.Type Sirenix.Serialization.CoroutineFormatter::get_SerializedType()
extern void CoroutineFormatter_get_SerializedType_m9640E1AB3F5E14F17CC4BED92D3FE111AEE92452 (void);
// 0x000003D6 System.Object Sirenix.Serialization.CoroutineFormatter::Sirenix.Serialization.IFormatter.Deserialize(Sirenix.Serialization.IDataReader)
extern void CoroutineFormatter_Sirenix_Serialization_IFormatter_Deserialize_mEDCE95E9AA0402FFE15DD98A826C7E217E54FB48 (void);
// 0x000003D7 UnityEngine.Coroutine Sirenix.Serialization.CoroutineFormatter::Deserialize(Sirenix.Serialization.IDataReader)
extern void CoroutineFormatter_Deserialize_m60F15A2AAC63249F8C5ECCDCACF3DB9F8C3FA740 (void);
// 0x000003D8 System.Void Sirenix.Serialization.CoroutineFormatter::Serialize(System.Object,Sirenix.Serialization.IDataWriter)
extern void CoroutineFormatter_Serialize_mA93ADDCA4BBBC9D6A7C03BB1F795159DB96A918E (void);
// 0x000003D9 System.Void Sirenix.Serialization.CoroutineFormatter::Serialize(UnityEngine.Coroutine,Sirenix.Serialization.IDataWriter)
extern void CoroutineFormatter_Serialize_m9E5F320088348A6F472F06B6DB69C9E77BEE6FE8 (void);
// 0x000003DA System.Void Sirenix.Serialization.CoroutineFormatter::.ctor()
extern void CoroutineFormatter__ctor_m119FC79D807011FC1712E58CA9904800A684B6E1 (void);
// 0x000003DB System.String Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::get_ProviderID()
// 0x000003DC T Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::GetKeyFromPathString(System.String)
// 0x000003DD System.String Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::GetPathStringFromKey(T)
// 0x000003DE System.Int32 Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::Compare(T,T)
// 0x000003DF System.Int32 Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::Sirenix.Serialization.IDictionaryKeyPathProvider.Compare(System.Object,System.Object)
// 0x000003E0 System.Object Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::Sirenix.Serialization.IDictionaryKeyPathProvider.GetKeyFromPathString(System.String)
// 0x000003E1 System.String Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::Sirenix.Serialization.IDictionaryKeyPathProvider.GetPathStringFromKey(System.Object)
// 0x000003E2 System.Void Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::.ctor()
// 0x000003E3 System.String Sirenix.Serialization.IDictionaryKeyPathProvider::get_ProviderID()
// 0x000003E4 System.String Sirenix.Serialization.IDictionaryKeyPathProvider::GetPathStringFromKey(System.Object)
// 0x000003E5 System.Object Sirenix.Serialization.IDictionaryKeyPathProvider::GetKeyFromPathString(System.String)
// 0x000003E6 System.Int32 Sirenix.Serialization.IDictionaryKeyPathProvider::Compare(System.Object,System.Object)
// 0x000003E7 System.String Sirenix.Serialization.IDictionaryKeyPathProvider`1::GetPathStringFromKey(T)
// 0x000003E8 T Sirenix.Serialization.IDictionaryKeyPathProvider`1::GetKeyFromPathString(System.String)
// 0x000003E9 System.Int32 Sirenix.Serialization.IDictionaryKeyPathProvider`1::Compare(T,T)
// 0x000003EA System.Void Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute::.ctor(System.Type)
extern void RegisterDictionaryKeyPathProviderAttribute__ctor_m9305B768AC5434D4CA204CFD90516F302CC003AF (void);
// 0x000003EB System.String Sirenix.Serialization.Vector2DictionaryKeyPathProvider::get_ProviderID()
extern void Vector2DictionaryKeyPathProvider_get_ProviderID_m3DC672A8F1F038D86ED800C7BA15C846939C30A7 (void);
// 0x000003EC System.Int32 Sirenix.Serialization.Vector2DictionaryKeyPathProvider::Compare(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2DictionaryKeyPathProvider_Compare_mABF5636048241686EA08A582B561BBFAE1DA63F3 (void);
// 0x000003ED UnityEngine.Vector2 Sirenix.Serialization.Vector2DictionaryKeyPathProvider::GetKeyFromPathString(System.String)
extern void Vector2DictionaryKeyPathProvider_GetKeyFromPathString_mEAD126AFE524CB8FC290B8B2B141C38E1F08D3FE (void);
// 0x000003EE System.String Sirenix.Serialization.Vector2DictionaryKeyPathProvider::GetPathStringFromKey(UnityEngine.Vector2)
extern void Vector2DictionaryKeyPathProvider_GetPathStringFromKey_mF25D8EF5D0168CFDFD1C72752EB9BB2F47EBDBCD (void);
// 0x000003EF System.Void Sirenix.Serialization.Vector2DictionaryKeyPathProvider::.ctor()
extern void Vector2DictionaryKeyPathProvider__ctor_m5A82420688BC8CCDEEF0E85130517DD184C313F8 (void);
// 0x000003F0 System.String Sirenix.Serialization.Vector3DictionaryKeyPathProvider::get_ProviderID()
extern void Vector3DictionaryKeyPathProvider_get_ProviderID_m0DB6CFF1A7989883880D50D9B2E20DA49E8EA7BB (void);
// 0x000003F1 System.Int32 Sirenix.Serialization.Vector3DictionaryKeyPathProvider::Compare(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3DictionaryKeyPathProvider_Compare_mA021862132B657769FD96AF1C7E485D618F2111B (void);
// 0x000003F2 UnityEngine.Vector3 Sirenix.Serialization.Vector3DictionaryKeyPathProvider::GetKeyFromPathString(System.String)
extern void Vector3DictionaryKeyPathProvider_GetKeyFromPathString_mA6286F2F6629B1DB3EDC4027FDF8E4F0300A80B5 (void);
// 0x000003F3 System.String Sirenix.Serialization.Vector3DictionaryKeyPathProvider::GetPathStringFromKey(UnityEngine.Vector3)
extern void Vector3DictionaryKeyPathProvider_GetPathStringFromKey_m2F22A88959E151F5400C9A714C7C427C60A88C9D (void);
// 0x000003F4 System.Void Sirenix.Serialization.Vector3DictionaryKeyPathProvider::.ctor()
extern void Vector3DictionaryKeyPathProvider__ctor_mAA8CFCA3922C075F2FD7571A7B0FF538A04EC629 (void);
// 0x000003F5 System.String Sirenix.Serialization.Vector4DictionaryKeyPathProvider::get_ProviderID()
extern void Vector4DictionaryKeyPathProvider_get_ProviderID_m4C79BF90A145122E3D2CC4463AC9130576F8B4FF (void);
// 0x000003F6 System.Int32 Sirenix.Serialization.Vector4DictionaryKeyPathProvider::Compare(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Vector4DictionaryKeyPathProvider_Compare_mB4E56CB8FCA9F13FB94FF712A24B0ACB1ADBE896 (void);
// 0x000003F7 UnityEngine.Vector4 Sirenix.Serialization.Vector4DictionaryKeyPathProvider::GetKeyFromPathString(System.String)
extern void Vector4DictionaryKeyPathProvider_GetKeyFromPathString_m004F9E8409351C12CF6158EF04D7A35CD4538A5D (void);
// 0x000003F8 System.String Sirenix.Serialization.Vector4DictionaryKeyPathProvider::GetPathStringFromKey(UnityEngine.Vector4)
extern void Vector4DictionaryKeyPathProvider_GetPathStringFromKey_mD38148EED635D375A9225EEDEAF18F2549D8B2C4 (void);
// 0x000003F9 System.Void Sirenix.Serialization.Vector4DictionaryKeyPathProvider::.ctor()
extern void Vector4DictionaryKeyPathProvider__ctor_m6D4AD54447176F9E8240E410E54FC355F7AF8C4A (void);
// 0x000003FA System.Void Sirenix.Serialization.GradientAlphaKeyFormatter::Read(UnityEngine.GradientAlphaKey&,Sirenix.Serialization.IDataReader)
extern void GradientAlphaKeyFormatter_Read_mD0BF936362EE76FC6B34C93ED1C5EC59A22A841B (void);
// 0x000003FB System.Void Sirenix.Serialization.GradientAlphaKeyFormatter::Write(UnityEngine.GradientAlphaKey&,Sirenix.Serialization.IDataWriter)
extern void GradientAlphaKeyFormatter_Write_mEBC82F0D219DF553411B19F291BDCE704E69E6AF (void);
// 0x000003FC System.Void Sirenix.Serialization.GradientAlphaKeyFormatter::.ctor()
extern void GradientAlphaKeyFormatter__ctor_mA4EEB011E5E52EFB8C6665B23E69EA8FA5CA55E0 (void);
// 0x000003FD System.Void Sirenix.Serialization.GradientAlphaKeyFormatter::.cctor()
extern void GradientAlphaKeyFormatter__cctor_mE2363DE32D5374D7B2DD4C5BD71260D411E77524 (void);
// 0x000003FE System.Void Sirenix.Serialization.GradientColorKeyFormatter::Read(UnityEngine.GradientColorKey&,Sirenix.Serialization.IDataReader)
extern void GradientColorKeyFormatter_Read_m0AB0AC316EA565E9C0F33E49D5A6F6C4A6B72EEA (void);
// 0x000003FF System.Void Sirenix.Serialization.GradientColorKeyFormatter::Write(UnityEngine.GradientColorKey&,Sirenix.Serialization.IDataWriter)
extern void GradientColorKeyFormatter_Write_m8053036BFB36FE9A919DA7C9F416294D7061B168 (void);
// 0x00000400 System.Void Sirenix.Serialization.GradientColorKeyFormatter::.ctor()
extern void GradientColorKeyFormatter__ctor_m87C0E95508392754C2266906F7437EEAF502F9EF (void);
// 0x00000401 System.Void Sirenix.Serialization.GradientColorKeyFormatter::.cctor()
extern void GradientColorKeyFormatter__cctor_m09283737ACE3DD2188ECBF2FF3385DE82E9D2790 (void);
// 0x00000402 UnityEngine.Gradient Sirenix.Serialization.GradientFormatter::GetUninitializedObject()
extern void GradientFormatter_GetUninitializedObject_mF856CADFEE72AA1B10477636CA72C9D5D181813F (void);
// 0x00000403 System.Void Sirenix.Serialization.GradientFormatter::Read(UnityEngine.Gradient&,Sirenix.Serialization.IDataReader)
extern void GradientFormatter_Read_m144D89D31C70591DE64B0300E8871C20CE948918 (void);
// 0x00000404 System.Void Sirenix.Serialization.GradientFormatter::Write(UnityEngine.Gradient&,Sirenix.Serialization.IDataWriter)
extern void GradientFormatter_Write_m3E23FDE8ADBFE1425D9E7107072FB0CD5DB01856 (void);
// 0x00000405 System.Void Sirenix.Serialization.GradientFormatter::.ctor()
extern void GradientFormatter__ctor_mA041A71B226104F39B36869CB8BDB93985DC2F8E (void);
// 0x00000406 System.Void Sirenix.Serialization.GradientFormatter::.cctor()
extern void GradientFormatter__cctor_m8F1012BE696598C966F96F2815E2B4BDC547615B (void);
// 0x00000407 Sirenix.Serialization.DataFormat Sirenix.Serialization.IOverridesSerializationFormat::GetFormatToSerializeAs(System.Boolean)
// 0x00000408 Sirenix.Serialization.SerializationData Sirenix.Serialization.ISupportsPrefabSerialization::get_SerializationData()
// 0x00000409 System.Void Sirenix.Serialization.ISupportsPrefabSerialization::set_SerializationData(Sirenix.Serialization.SerializationData)
// 0x0000040A System.Void Sirenix.Serialization.KeyframeFormatter::.cctor()
extern void KeyframeFormatter__cctor_mAE06A892FF00E28D0B3B7753C12B8E58CE0995E1 (void);
// 0x0000040B System.Void Sirenix.Serialization.KeyframeFormatter::Read(UnityEngine.Keyframe&,Sirenix.Serialization.IDataReader)
extern void KeyframeFormatter_Read_mEBD133564B2C4005CE4699F2AE7D7DAEA1105B41 (void);
// 0x0000040C System.Void Sirenix.Serialization.KeyframeFormatter::Write(UnityEngine.Keyframe&,Sirenix.Serialization.IDataWriter)
extern void KeyframeFormatter_Write_mACD1B61C7E79D76CC0783123813C3B13EF48EB81 (void);
// 0x0000040D System.Void Sirenix.Serialization.KeyframeFormatter::.ctor()
extern void KeyframeFormatter__ctor_m042E7CB0FBBADBB5C62B6E58A76EEDC32FC1E4DC (void);
// 0x0000040E System.Void Sirenix.Serialization.LayerMaskFormatter::Read(UnityEngine.LayerMask&,Sirenix.Serialization.IDataReader)
extern void LayerMaskFormatter_Read_mD24F0F8941C06F7DDE9A09874798E24BCEA2AD0C (void);
// 0x0000040F System.Void Sirenix.Serialization.LayerMaskFormatter::Write(UnityEngine.LayerMask&,Sirenix.Serialization.IDataWriter)
extern void LayerMaskFormatter_Write_mD9E7036A1530FB684A4D4E21B7AFFEBA06F5B19D (void);
// 0x00000410 System.Void Sirenix.Serialization.LayerMaskFormatter::.ctor()
extern void LayerMaskFormatter__ctor_mF07A9013D7CCC05A966AA1607D3C33AE8320AD50 (void);
// 0x00000411 System.Void Sirenix.Serialization.LayerMaskFormatter::.cctor()
extern void LayerMaskFormatter__cctor_mA29E198988AB9B7CFF00FB70359F6B371543AD68 (void);
// 0x00000412 System.Void Sirenix.Serialization.QuaternionFormatter::Read(UnityEngine.Quaternion&,Sirenix.Serialization.IDataReader)
extern void QuaternionFormatter_Read_mC801847AB0F33AD9135CCC2E8AC98B61F6668AE1 (void);
// 0x00000413 System.Void Sirenix.Serialization.QuaternionFormatter::Write(UnityEngine.Quaternion&,Sirenix.Serialization.IDataWriter)
extern void QuaternionFormatter_Write_m35671ADEE30F278B545FA0636F03E867083C8C70 (void);
// 0x00000414 System.Void Sirenix.Serialization.QuaternionFormatter::.ctor()
extern void QuaternionFormatter__ctor_m2526DD0118146137E793C08E253124BF835D1496 (void);
// 0x00000415 System.Void Sirenix.Serialization.QuaternionFormatter::.cctor()
extern void QuaternionFormatter__cctor_m768912C59B54558682D1855D881E882110011D1F (void);
// 0x00000416 System.Void Sirenix.Serialization.RectFormatter::Read(UnityEngine.Rect&,Sirenix.Serialization.IDataReader)
extern void RectFormatter_Read_mEC03C8E1722F260536C5F15E92F5E4A3FD2C3D73 (void);
// 0x00000417 System.Void Sirenix.Serialization.RectFormatter::Write(UnityEngine.Rect&,Sirenix.Serialization.IDataWriter)
extern void RectFormatter_Write_m84E93283D29A6F0A99DF0C1DE0216002EDBB5989 (void);
// 0x00000418 System.Void Sirenix.Serialization.RectFormatter::.ctor()
extern void RectFormatter__ctor_mFB71D3DB3513AF1A460B1C8C4E70A1A2465D9285 (void);
// 0x00000419 System.Void Sirenix.Serialization.RectFormatter::.cctor()
extern void RectFormatter__cctor_mCF1D4C81036A527C3203208CD077F6E36DE5D003 (void);
// 0x0000041A System.Boolean Sirenix.Serialization.SerializationData::get_HasEditorData()
extern void SerializationData_get_HasEditorData_m3537300781FF61083E5118C85AD08EF75BCBD4A6 (void);
// 0x0000041B System.Boolean Sirenix.Serialization.SerializationData::get_ContainsData()
extern void SerializationData_get_ContainsData_mD670EBA3B480BEE760C62ACBB6DF66AFC6BF5A43 (void);
// 0x0000041C System.Void Sirenix.Serialization.SerializationData::Reset()
extern void SerializationData_Reset_m1EB15EF81D0E03A10D51227A18311DEAEDCB66AE (void);
// 0x0000041D T Sirenix.Serialization.UnityEventFormatter`1::GetUninitializedObject()
// 0x0000041E System.Void Sirenix.Serialization.UnityEventFormatter`1::.ctor()
// 0x0000041F System.Void Sirenix.Serialization.UnityReferenceResolver::.ctor()
extern void UnityReferenceResolver__ctor_m96CFF7618DA71E09842A1582861B310B6AF55BC5 (void);
// 0x00000420 System.Void Sirenix.Serialization.UnityReferenceResolver::.ctor(System.Collections.Generic.List`1<UnityEngine.Object>)
extern void UnityReferenceResolver__ctor_mE922D04EDFCAA20967BD224DA9E92D4A4D8FC2B2 (void);
// 0x00000421 System.Collections.Generic.List`1<UnityEngine.Object> Sirenix.Serialization.UnityReferenceResolver::GetReferencedUnityObjects()
extern void UnityReferenceResolver_GetReferencedUnityObjects_mFBA23F7D35BA2509457627F529172BB784DDE8A1 (void);
// 0x00000422 System.Void Sirenix.Serialization.UnityReferenceResolver::SetReferencedUnityObjects(System.Collections.Generic.List`1<UnityEngine.Object>)
extern void UnityReferenceResolver_SetReferencedUnityObjects_mD37E1166E1E3262C75083D20B889E2157E87DD08 (void);
// 0x00000423 System.Boolean Sirenix.Serialization.UnityReferenceResolver::CanReference(System.Object,System.Int32&)
extern void UnityReferenceResolver_CanReference_m8811ADBD0D79EC84E4781E44465A495662F2393C (void);
// 0x00000424 System.Boolean Sirenix.Serialization.UnityReferenceResolver::TryResolveReference(System.Int32,System.Object&)
extern void UnityReferenceResolver_TryResolveReference_m9A914C9A8533FD1A2EA0F2DB393521023E769032 (void);
// 0x00000425 System.Void Sirenix.Serialization.UnityReferenceResolver::Reset()
extern void UnityReferenceResolver_Reset_m84A632E381742A94635D9D5D81FC2D421A7658DA (void);
// 0x00000426 System.Void Sirenix.Serialization.UnityReferenceResolver::Sirenix.Serialization.Utilities.ICacheNotificationReceiver.OnFreed()
extern void UnityReferenceResolver_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnFreed_mD2689226BD768A1ADE1B303F2DEC8297D80D5867 (void);
// 0x00000427 System.Void Sirenix.Serialization.UnityReferenceResolver::Sirenix.Serialization.Utilities.ICacheNotificationReceiver.OnClaimed()
extern void UnityReferenceResolver_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnClaimed_m282C5CEB61573A4341AA8F8E7DE6BB37392A5F49 (void);
// 0x00000428 System.Boolean Sirenix.Serialization.UnitySerializationInitializer::get_Initialized()
extern void UnitySerializationInitializer_get_Initialized_mC6FC28109C4DC7C120179A706CC15D151F3FD0DC (void);
// 0x00000429 UnityEngine.RuntimePlatform Sirenix.Serialization.UnitySerializationInitializer::get_CurrentPlatform()
extern void UnitySerializationInitializer_get_CurrentPlatform_m91D11F98FCACAE94CDD0E37B79111BBC9AD912C4 (void);
// 0x0000042A System.Void Sirenix.Serialization.UnitySerializationInitializer::set_CurrentPlatform(UnityEngine.RuntimePlatform)
extern void UnitySerializationInitializer_set_CurrentPlatform_m504A8B8DD2DB51664E6A814D5A6031E6FA218CCA (void);
// 0x0000042B System.Void Sirenix.Serialization.UnitySerializationInitializer::Initialize()
extern void UnitySerializationInitializer_Initialize_m4FA3E148819EA9096A88D7316ADAF373813B0FF9 (void);
// 0x0000042C System.Void Sirenix.Serialization.UnitySerializationInitializer::InitializeRuntime()
extern void UnitySerializationInitializer_InitializeRuntime_mDF40113E7472FEBF6A8C547DFBA347E9359288AD (void);
// 0x0000042D System.Void Sirenix.Serialization.UnitySerializationInitializer::.cctor()
extern void UnitySerializationInitializer__cctor_mEF9BC9DFD965E89343198A9F0441592E33E36EA6 (void);
// 0x0000042E System.Void Sirenix.Serialization.Vector2Formatter::Read(UnityEngine.Vector2&,Sirenix.Serialization.IDataReader)
extern void Vector2Formatter_Read_mDBBA275FF6E33ECFF001EEB8EE52C98C0648B662 (void);
// 0x0000042F System.Void Sirenix.Serialization.Vector2Formatter::Write(UnityEngine.Vector2&,Sirenix.Serialization.IDataWriter)
extern void Vector2Formatter_Write_mC00A7FA6A8761C6A5234CA456B264C26F1E36FE3 (void);
// 0x00000430 System.Void Sirenix.Serialization.Vector2Formatter::.ctor()
extern void Vector2Formatter__ctor_mE3203FB4E145DBE4A03882A546C901C7677F3E1E (void);
// 0x00000431 System.Void Sirenix.Serialization.Vector2Formatter::.cctor()
extern void Vector2Formatter__cctor_mB93E4AEB8030A1CF49223A260ADD4B5AF17AF23B (void);
// 0x00000432 System.Void Sirenix.Serialization.Vector3Formatter::Read(UnityEngine.Vector3&,Sirenix.Serialization.IDataReader)
extern void Vector3Formatter_Read_m5DFDF50DF7761B81E3FD617B76DB521249C296A4 (void);
// 0x00000433 System.Void Sirenix.Serialization.Vector3Formatter::Write(UnityEngine.Vector3&,Sirenix.Serialization.IDataWriter)
extern void Vector3Formatter_Write_m859D7F8E5AA3B7F5EBFF88DB03F2796323BDA6CB (void);
// 0x00000434 System.Void Sirenix.Serialization.Vector3Formatter::.ctor()
extern void Vector3Formatter__ctor_mA665AC99E4BAE4F4FD4DE9FD122121E930F02446 (void);
// 0x00000435 System.Void Sirenix.Serialization.Vector3Formatter::.cctor()
extern void Vector3Formatter__cctor_mE3D1FA2B4D9E3E0D3E1A377BEB61D52441F706FE (void);
// 0x00000436 System.Void Sirenix.Serialization.Vector4Formatter::Read(UnityEngine.Vector4&,Sirenix.Serialization.IDataReader)
extern void Vector4Formatter_Read_m456AEB1375418934EA36143D72D1F8D32B54E48B (void);
// 0x00000437 System.Void Sirenix.Serialization.Vector4Formatter::Write(UnityEngine.Vector4&,Sirenix.Serialization.IDataWriter)
extern void Vector4Formatter_Write_m21F42BF11DC4AA32ED78C712B1536B71DF11AE5E (void);
// 0x00000438 System.Void Sirenix.Serialization.Vector4Formatter::.ctor()
extern void Vector4Formatter__ctor_m20DC716B3DD67474A2D4D9D3A22473F5C24178C7 (void);
// 0x00000439 System.Void Sirenix.Serialization.Vector4Formatter::.cctor()
extern void Vector4Formatter__cctor_m619F0CD14DE7D94A09F6F9C058ACA5B910F34E50 (void);
// 0x0000043A System.Void Sirenix.Serialization.Buffer`1::.ctor(System.Int32)
// 0x0000043B System.Int32 Sirenix.Serialization.Buffer`1::get_Count()
// 0x0000043C T[] Sirenix.Serialization.Buffer`1::get_Array()
// 0x0000043D System.Boolean Sirenix.Serialization.Buffer`1::get_IsFree()
// 0x0000043E Sirenix.Serialization.Buffer`1<T> Sirenix.Serialization.Buffer`1::Claim(System.Int32)
// 0x0000043F System.Void Sirenix.Serialization.Buffer`1::Free(Sirenix.Serialization.Buffer`1<T>)
// 0x00000440 System.Void Sirenix.Serialization.Buffer`1::Free()
// 0x00000441 System.Void Sirenix.Serialization.Buffer`1::Dispose()
// 0x00000442 System.Int32 Sirenix.Serialization.Buffer`1::NextPowerOfTwo(System.Int32)
// 0x00000443 System.Void Sirenix.Serialization.Buffer`1::.cctor()
// 0x00000444 System.IO.MemoryStream Sirenix.Serialization.CachedMemoryStream::get_MemoryStream()
extern void CachedMemoryStream_get_MemoryStream_mEEB005B5C8369AAEBDB9041B31DE4733AD00289D (void);
// 0x00000445 System.Void Sirenix.Serialization.CachedMemoryStream::.ctor()
extern void CachedMemoryStream__ctor_m3A386C9B5A19BB230C763FEADB57772B1A25BAB2 (void);
// 0x00000446 System.Void Sirenix.Serialization.CachedMemoryStream::OnFreed()
extern void CachedMemoryStream_OnFreed_mC2D8FE5DF691E4FBD9A60C0DC70097A26E03CECE (void);
// 0x00000447 System.Void Sirenix.Serialization.CachedMemoryStream::OnClaimed()
extern void CachedMemoryStream_OnClaimed_m4305B99A8642890D6B8191B2081BED877CC38B67 (void);
// 0x00000448 Sirenix.Serialization.Utilities.Cache`1<Sirenix.Serialization.CachedMemoryStream> Sirenix.Serialization.CachedMemoryStream::Claim(System.Int32)
extern void CachedMemoryStream_Claim_m47F6BCBDDE71CBDC749ABF3566F6126E1110A14C (void);
// 0x00000449 Sirenix.Serialization.Utilities.Cache`1<Sirenix.Serialization.CachedMemoryStream> Sirenix.Serialization.CachedMemoryStream::Claim(System.Byte[])
extern void CachedMemoryStream_Claim_mBD3BCFAADC96B0F511D4C0F49389007F56F78ADB (void);
// 0x0000044A System.Void Sirenix.Serialization.CachedMemoryStream::.cctor()
extern void CachedMemoryStream__cctor_m767654013D7B50DE6AD9C49C4DE243510EBCCF8F (void);
// 0x0000044B System.Void Sirenix.Serialization.EmittedAssemblyAttribute::.ctor()
extern void EmittedAssemblyAttribute__ctor_mE3785C486914F648A23405A5D5DE7357D4D33365 (void);
// 0x0000044C System.Void Sirenix.Serialization.FormatterLocator::.cctor()
extern void FormatterLocator__cctor_m02DEA4CA2F8483F4C403BD0CA28DFDC4BBBA6080 (void);
// 0x0000044D System.Void Sirenix.Serialization.FormatterLocator::add_FormatterResolve(System.Func`2<System.Type,Sirenix.Serialization.IFormatter>)
extern void FormatterLocator_add_FormatterResolve_m414D138B3CC56647CBA361259E35A50C33F13349 (void);
// 0x0000044E System.Void Sirenix.Serialization.FormatterLocator::remove_FormatterResolve(System.Func`2<System.Type,Sirenix.Serialization.IFormatter>)
extern void FormatterLocator_remove_FormatterResolve_m18543B478624615802C52C884ECA7A7A42C05295 (void);
// 0x0000044F Sirenix.Serialization.IFormatter`1<T> Sirenix.Serialization.FormatterLocator::GetFormatter(Sirenix.Serialization.ISerializationPolicy)
// 0x00000450 Sirenix.Serialization.IFormatter Sirenix.Serialization.FormatterLocator::GetFormatter(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterLocator_GetFormatter_m338A613BEB091D64128B42AA43E8D62137E1F140 (void);
// 0x00000451 System.Void Sirenix.Serialization.FormatterLocator::LogAOTError(System.Type,System.Exception)
extern void FormatterLocator_LogAOTError_m0A39E0C32E148ABFD22FA5B73CED7A06F79B9121 (void);
// 0x00000452 System.Collections.Generic.IEnumerable`1<System.String> Sirenix.Serialization.FormatterLocator::GetAllPossibleMissingAOTTypes(System.Type)
extern void FormatterLocator_GetAllPossibleMissingAOTTypes_m733EE9D38B64CC4E5961D1D70AEDB26024EC846D (void);
// 0x00000453 System.Collections.Generic.List`1<Sirenix.Serialization.IFormatter> Sirenix.Serialization.FormatterLocator::GetAllCompatiblePredefinedFormatters(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterLocator_GetAllCompatiblePredefinedFormatters_m369AF54255747ABB8CB0D7CDFDEA872BB604840C (void);
// 0x00000454 Sirenix.Serialization.IFormatter Sirenix.Serialization.FormatterLocator::CreateFormatter(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterLocator_CreateFormatter_m1576975374699853FCE3DDEEFE7EE5BFFE1C3EDA (void);
// 0x00000455 Sirenix.Serialization.IFormatter Sirenix.Serialization.FormatterLocator::GetFormatterInstance(System.Type)
extern void FormatterLocator_GetFormatterInstance_mDDD157573ECFDB5CD836743BDDF7BDE7AD945498 (void);
// 0x00000456 System.Void Sirenix.Serialization.FormatterLocator/<>c::.cctor()
extern void U3CU3Ec__cctor_m9331773D9CDB9978BF0DAB6597C12EE6ED7A30A7 (void);
// 0x00000457 System.Void Sirenix.Serialization.FormatterLocator/<>c::.ctor()
extern void U3CU3Ec__ctor_m6802C7C0E5FF87D2EA392BECEEE73A494E028202 (void);
// 0x00000458 System.Int32 Sirenix.Serialization.FormatterLocator/<>c::<.cctor>b__7_0(Sirenix.Serialization.FormatterLocator/FormatterInfo,Sirenix.Serialization.FormatterLocator/FormatterInfo)
extern void U3CU3Ec_U3C_cctorU3Eb__7_0_mBD0643739CB95DBB42A7475D4AD68A21B088C574 (void);
// 0x00000459 System.Int32 Sirenix.Serialization.FormatterLocator/<>c::<.cctor>b__7_1(Sirenix.Serialization.FormatterLocator/FormatterLocatorInfo,Sirenix.Serialization.FormatterLocator/FormatterLocatorInfo)
extern void U3CU3Ec_U3C_cctorU3Eb__7_1_m003D06BB02F32859899264FA45EA45DD759B1B76 (void);
// 0x0000045A System.Void Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::.ctor(System.Int32)
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14__ctor_m6807DCF6D5F514E393A3506E51F9F6D6BF2B5743 (void);
// 0x0000045B System.Void Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::System.IDisposable.Dispose()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_IDisposable_Dispose_m1CC5D5B263630965C2576ECB8BFEC3C078D61C3E (void);
// 0x0000045C System.Boolean Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::MoveNext()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_MoveNext_m5FD817C61E20F5B925B0D8411E4FAC10593F08DF (void);
// 0x0000045D System.Void Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::<>m__Finally1()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_U3CU3Em__Finally1_mF8F20E11D7C04E385F96B9FC218627524F833985 (void);
// 0x0000045E System.String Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mC70BDEC8AED72FE57AD10B247740100D053B5B6F (void);
// 0x0000045F System.Void Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::System.Collections.IEnumerator.Reset()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerator_Reset_m3A92AC8AF65A4E57A5249432202ED103D8133C3E (void);
// 0x00000460 System.Object Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerator_get_Current_mD42319F2D838D833B0F339C5FB86F5AB4CA21748 (void);
// 0x00000461 System.Collections.Generic.IEnumerator`1<System.String> Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mD6D432ABC0F655548A70757DFA066F3319F95841 (void);
// 0x00000462 System.Collections.IEnumerator Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerable_GetEnumerator_mC2A965A639C688AE03EC3A9A6F10751A18DD5536 (void);
// 0x00000463 System.Void Sirenix.Serialization.FormatterUtilities::.cctor()
extern void FormatterUtilities__cctor_mDB12F43EC3FE888256B720456223801D95046C54 (void);
// 0x00000464 System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo> Sirenix.Serialization.FormatterUtilities::GetSerializableMembersMap(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterUtilities_GetSerializableMembersMap_m1E696AEF935F270792C93AF083EAB38CE57D77C5 (void);
// 0x00000465 System.Reflection.MemberInfo[] Sirenix.Serialization.FormatterUtilities::GetSerializableMembers(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterUtilities_GetSerializableMembers_m40BB77B1C95F5E69E3FB90BD2756BBEE41AF01B7 (void);
// 0x00000466 UnityEngine.Object Sirenix.Serialization.FormatterUtilities::CreateUnityNull(System.Type,System.Type)
extern void FormatterUtilities_CreateUnityNull_m9F29E00B1EA9E567C8522AA1CBAEBE7BB933804E (void);
// 0x00000467 System.Boolean Sirenix.Serialization.FormatterUtilities::IsPrimitiveType(System.Type)
extern void FormatterUtilities_IsPrimitiveType_m4FA0D9043862BF0E6950C5DE9D00EF52C4E4081B (void);
// 0x00000468 System.Boolean Sirenix.Serialization.FormatterUtilities::IsPrimitiveArrayType(System.Type)
extern void FormatterUtilities_IsPrimitiveArrayType_m9021194370421E39B818583E6A0511428A9078BD (void);
// 0x00000469 System.Type Sirenix.Serialization.FormatterUtilities::GetContainedType(System.Reflection.MemberInfo)
extern void FormatterUtilities_GetContainedType_m7EA67A5D85713F168735A082F6566804ABACF1B9 (void);
// 0x0000046A System.Object Sirenix.Serialization.FormatterUtilities::GetMemberValue(System.Reflection.MemberInfo,System.Object)
extern void FormatterUtilities_GetMemberValue_mA90CA20908489F1BFDFDCD5D5ED63E972EA1A613 (void);
// 0x0000046B System.Void Sirenix.Serialization.FormatterUtilities::SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)
extern void FormatterUtilities_SetMemberValue_mCD5FF6CA0CBEBFF70B61D928DB9A63F484573B28 (void);
// 0x0000046C System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo> Sirenix.Serialization.FormatterUtilities::FindSerializableMembersMap(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterUtilities_FindSerializableMembersMap_mEDFF45926C9BBFB049B0E795CEB527656CE7B260 (void);
// 0x0000046D System.Void Sirenix.Serialization.FormatterUtilities::FindSerializableMembers(System.Type,System.Collections.Generic.List`1<System.Reflection.MemberInfo>,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterUtilities_FindSerializableMembers_m143780126B793AF62B3181B42EE7B6F7C63C9BFE (void);
// 0x0000046E System.Reflection.MemberInfo Sirenix.Serialization.FormatterUtilities::GetPrivateMemberAlias(System.Reflection.MemberInfo,System.String,System.String)
extern void FormatterUtilities_GetPrivateMemberAlias_mDB92E479FB82F6C4719AC9DDBA426A36F6820AC3 (void);
// 0x0000046F System.Boolean Sirenix.Serialization.FormatterUtilities::MemberIsPrivate(System.Reflection.MemberInfo)
extern void FormatterUtilities_MemberIsPrivate_mC306E58947FF2CC8F21CD9D282AA32374B9654BD (void);
// 0x00000470 System.Void Sirenix.Serialization.FormatterUtilities/<>c::.cctor()
extern void U3CU3Ec__cctor_m61F914E8F3DB991A8E2F216DAEA33663E412A1E4 (void);
// 0x00000471 System.Void Sirenix.Serialization.FormatterUtilities/<>c::.ctor()
extern void U3CU3Ec__ctor_mD865791C90A7779DF2AAD941336DF98AC0518A78 (void);
// 0x00000472 System.String Sirenix.Serialization.FormatterUtilities/<>c::<FindSerializableMembersMap>b__15_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CFindSerializableMembersMapU3Eb__15_0_m33DDB023F41E24FC50DFE11F115C3B89FD97E286 (void);
// 0x00000473 System.Reflection.MemberInfo Sirenix.Serialization.FormatterUtilities/<>c::<FindSerializableMembersMap>b__15_1(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CFindSerializableMembersMapU3Eb__15_1_mC523774053B664DDA27322F52EC128CB26209D3B (void);
// 0x00000474 System.Boolean Sirenix.Serialization.FormatterUtilities/<>c::<FindSerializableMembers>b__16_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CFindSerializableMembersU3Eb__16_0_mBCC04EEC55914DD51232DF297E11BE6588CBDB12 (void);
// 0x00000475 System.Void Sirenix.Serialization.FormatterUtilities/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mAB757C57286F8CFAFD96545A19A1186DEE5691D4 (void);
// 0x00000476 System.Boolean Sirenix.Serialization.FormatterUtilities/<>c__DisplayClass16_0::<FindSerializableMembers>b__1(System.Reflection.MemberInfo)
extern void U3CU3Ec__DisplayClass16_0_U3CFindSerializableMembersU3Eb__1_m51DC274C5C43DC45E9C2182E55489FC9775A4550 (void);
// 0x00000477 System.Void Sirenix.Serialization.DictionaryKeyUtility::.cctor()
extern void DictionaryKeyUtility__cctor_mA35EA90D57472B02449954604FC49A3A80D6EC2A (void);
// 0x00000478 System.Void Sirenix.Serialization.DictionaryKeyUtility::LogInvalidKeyPathProvider(System.Type,System.Reflection.Assembly,System.String)
extern void DictionaryKeyUtility_LogInvalidKeyPathProvider_mC277D3439F7EBBEC355FFCC588DA6C68CE7A17D2 (void);
// 0x00000479 System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Serialization.DictionaryKeyUtility::GetPersistentPathKeyTypes()
extern void DictionaryKeyUtility_GetPersistentPathKeyTypes_m1949DEFD97F131ADC78687A89FDA99D68E22245F (void);
// 0x0000047A System.Boolean Sirenix.Serialization.DictionaryKeyUtility::KeyTypeSupportsPersistentPaths(System.Type)
extern void DictionaryKeyUtility_KeyTypeSupportsPersistentPaths_mF56254ABC57F592365B594C8F055E4C11896EB93 (void);
// 0x0000047B System.Boolean Sirenix.Serialization.DictionaryKeyUtility::PrivateIsSupportedDictionaryKeyType(System.Type)
extern void DictionaryKeyUtility_PrivateIsSupportedDictionaryKeyType_mDB414E1CD23DF020247E1A6FFC5526C96BFCE916 (void);
// 0x0000047C System.String Sirenix.Serialization.DictionaryKeyUtility::GetDictionaryKeyString(System.Object)
extern void DictionaryKeyUtility_GetDictionaryKeyString_m14828B8BBB98514B4226DC387E233653CA3CF69B (void);
// 0x0000047D System.Object Sirenix.Serialization.DictionaryKeyUtility::GetDictionaryKeyValue(System.String,System.Type)
extern void DictionaryKeyUtility_GetDictionaryKeyValue_m45ED2B7124DDACA89029053757F9614ED32E5520 (void);
// 0x0000047E System.String Sirenix.Serialization.DictionaryKeyUtility::FromTo(System.String,System.Int32,System.Int32)
extern void DictionaryKeyUtility_FromTo_m8989BC8F41FD5496ECD27DFE6E4DB9C1A5AF940F (void);
// 0x0000047F System.Int32 Sirenix.Serialization.DictionaryKeyUtility/UnityObjectKeyComparer`1::Compare(T,T)
// 0x00000480 System.Void Sirenix.Serialization.DictionaryKeyUtility/UnityObjectKeyComparer`1::.ctor()
// 0x00000481 System.Int32 Sirenix.Serialization.DictionaryKeyUtility/FallbackKeyComparer`1::Compare(T,T)
// 0x00000482 System.Void Sirenix.Serialization.DictionaryKeyUtility/FallbackKeyComparer`1::.ctor()
// 0x00000483 System.Void Sirenix.Serialization.DictionaryKeyUtility/KeyComparer`1::.ctor()
// 0x00000484 System.Int32 Sirenix.Serialization.DictionaryKeyUtility/KeyComparer`1::Compare(T,T)
// 0x00000485 System.Void Sirenix.Serialization.DictionaryKeyUtility/KeyComparer`1::.cctor()
// 0x00000486 System.Void Sirenix.Serialization.DictionaryKeyUtility/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m8266A84F31C76A24BB6571F023C392842CE83AFB (void);
// 0x00000487 <>f__AnonymousType0`2<System.Reflection.Assembly,Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute> Sirenix.Serialization.DictionaryKeyUtility/<>c__DisplayClass12_0::<.cctor>b__1(System.Object)
extern void U3CU3Ec__DisplayClass12_0_U3C_cctorU3Eb__1_mC6A75E1F2346E6C4603D4BE10A63AC1A6B9FC9EB (void);
// 0x00000488 System.Void Sirenix.Serialization.DictionaryKeyUtility/<>c::.cctor()
extern void U3CU3Ec__cctor_m64D0412C8A1A4CE8EE4EE89E34D62765D0E174FE (void);
// 0x00000489 System.Void Sirenix.Serialization.DictionaryKeyUtility/<>c::.ctor()
extern void U3CU3Ec__ctor_m65FC08EA6EB8495388811B0E6FF94259225C4F64 (void);
// 0x0000048A System.Collections.Generic.IEnumerable`1<<>f__AnonymousType0`2<System.Reflection.Assembly,Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute>> Sirenix.Serialization.DictionaryKeyUtility/<>c::<.cctor>b__12_0(System.Reflection.Assembly)
extern void U3CU3Ec_U3C_cctorU3Eb__12_0_mA5351559363892F7896914FD8E729AEE043EDBC0 (void);
// 0x0000048B System.Boolean Sirenix.Serialization.DictionaryKeyUtility/<>c::<.cctor>b__12_2(<>f__AnonymousType0`2<System.Reflection.Assembly,Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute>)
extern void U3CU3Ec_U3C_cctorU3Eb__12_2_m9BDC90D57B42081F01F1E35D23663D3EC7547785 (void);
// 0x0000048C System.Void Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::.ctor(System.Int32)
extern void U3CGetPersistentPathKeyTypesU3Ed__14__ctor_m36A88212D2E381607C9DAF12BA928B22E12A0276 (void);
// 0x0000048D System.Void Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::System.IDisposable.Dispose()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_System_IDisposable_Dispose_m3823A29402B6AFD21BF4006684F0546FFEBC5685 (void);
// 0x0000048E System.Boolean Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::MoveNext()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_MoveNext_m2635EAB28F5967C981D7E8DC267179603ACCBBF0 (void);
// 0x0000048F System.Void Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::<>m__Finally1()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_U3CU3Em__Finally1_m9F6BB8ADDE2BA7CF1F3BFFB56E764DE5753BF21C (void);
// 0x00000490 System.Void Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::<>m__Finally2()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_U3CU3Em__Finally2_m71E6CAE2E07311D401168666DE8082BF8D449AD5 (void);
// 0x00000491 System.Type Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m0CC16A93FD09854E6C5CEE628CE8F2EA20154AD6 (void);
// 0x00000492 System.Void Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::System.Collections.IEnumerator.Reset()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerator_Reset_mB933F49AAB164AF50F5B689943724D5F72B9E3A2 (void);
// 0x00000493 System.Object Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerator_get_Current_m1B6827FE5C3E31BF5E1D6D857FA0F9C08190BB5A (void);
// 0x00000494 System.Collections.Generic.IEnumerator`1<System.Type> Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m9A8C7C8E62BDF4CB19FDE73627722736FCE49703 (void);
// 0x00000495 System.Collections.IEnumerator Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m8B8245CD9F99E186DDA436899290EAFE588501E7 (void);
// 0x00000496 System.Void Sirenix.Serialization.PrefabModification::Apply(UnityEngine.Object)
extern void PrefabModification_Apply_m67930A3515963FB738E9F50B43745519B411E193 (void);
// 0x00000497 System.Void Sirenix.Serialization.PrefabModification::ApplyValue(UnityEngine.Object)
extern void PrefabModification_ApplyValue_m2C1787B6D7B4850463149784AD56765D2AAF9409 (void);
// 0x00000498 System.Void Sirenix.Serialization.PrefabModification::ApplyListLength(UnityEngine.Object)
extern void PrefabModification_ApplyListLength_m975A69A837E43386FB0DBBF684F9A06963CC1866 (void);
// 0x00000499 System.Void Sirenix.Serialization.PrefabModification::ApplyDictionaryModifications(UnityEngine.Object)
extern void PrefabModification_ApplyDictionaryModifications_m322227F7D93FFEDA04554E547164C2E16FEC25D6 (void);
// 0x0000049A System.Void Sirenix.Serialization.PrefabModification::ReplaceAllReferencesInGraph(System.Object,System.Object,System.Object,System.Collections.Generic.HashSet`1<System.Object>)
extern void PrefabModification_ReplaceAllReferencesInGraph_m3D598C659793B83D7DA10B51BD048E61601622C2 (void);
// 0x0000049B System.Object Sirenix.Serialization.PrefabModification::GetInstanceFromPath(System.String,System.Object)
extern void PrefabModification_GetInstanceFromPath_mBBF07DDDEF9B2857ECB54213DAAF300D1E8E3272 (void);
// 0x0000049C System.Object Sirenix.Serialization.PrefabModification::GetInstanceOfStep(System.String,System.Object)
extern void PrefabModification_GetInstanceOfStep_m90982733BF040D73488C7E4FD096E72BF8E13015 (void);
// 0x0000049D System.Void Sirenix.Serialization.PrefabModification::SetInstanceToPath(System.String,System.Object,System.Object)
extern void PrefabModification_SetInstanceToPath_m8BAAFB4A2ACBB9E235CD595177BF3DEDDEB8349D (void);
// 0x0000049E System.Void Sirenix.Serialization.PrefabModification::SetInstanceToPath(System.String,System.String[],System.Int32,System.Object,System.Object,System.Boolean&)
extern void PrefabModification_SetInstanceToPath_m98B1BFF34F54DC105CF772516D3DDBE2E23552F5 (void);
// 0x0000049F System.Boolean Sirenix.Serialization.PrefabModification::TrySetInstanceOfStep(System.String,System.Object,System.Object,System.Boolean&)
extern void PrefabModification_TrySetInstanceOfStep_m76280F198FA78FC8DE0F1041ABC80EF18AEC3908 (void);
// 0x000004A0 System.Void Sirenix.Serialization.PrefabModification::.ctor()
extern void PrefabModification__ctor_m79DAB9ACAF0C4163917E18139D23803DB8A9DF09 (void);
// 0x000004A1 System.Void Sirenix.Serialization.PrefabModification/<>c::.cctor()
extern void U3CU3Ec__cctor_m354A95D6D8FFB77ADDCA97A163D68D56D5E1D69A (void);
// 0x000004A2 System.Void Sirenix.Serialization.PrefabModification/<>c::.ctor()
extern void U3CU3Ec__ctor_m37883E9759EBADFADC27B0D9546D5149C4E25D37 (void);
// 0x000004A3 System.Boolean Sirenix.Serialization.PrefabModification/<>c::<GetInstanceOfStep>b__13_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetInstanceOfStepU3Eb__13_0_mF537EA24FE5824031718B1E18FEE6651ABFDD8F3 (void);
// 0x000004A4 System.Boolean Sirenix.Serialization.PrefabModification/<>c::<TrySetInstanceOfStep>b__16_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CTrySetInstanceOfStepU3Eb__16_0_mE43342852FEF829FB9E7EDD397F4F960E54480C3 (void);
// 0x000004A5 System.UInt32[] Sirenix.Serialization.ProperBitConverter::CreateByteToHexLookup(System.Boolean)
extern void ProperBitConverter_CreateByteToHexLookup_m414CCD800CE54B801F4A0E1B400B5A2095FFA31B (void);
// 0x000004A6 System.String Sirenix.Serialization.ProperBitConverter::BytesToHexString(System.Byte[],System.Boolean)
extern void ProperBitConverter_BytesToHexString_mB7B0C30A61D3EF5E317BBC2A6C3D2A658F3A6302 (void);
// 0x000004A7 System.Byte[] Sirenix.Serialization.ProperBitConverter::HexStringToBytes(System.String)
extern void ProperBitConverter_HexStringToBytes_mE48E14EC3C6068D80A903FF7FAE79D8F0E049AED (void);
// 0x000004A8 System.Int16 Sirenix.Serialization.ProperBitConverter::ToInt16(System.Byte[],System.Int32)
extern void ProperBitConverter_ToInt16_m74C69878863F5561B43FCA9A6051C6A3F5B04D71 (void);
// 0x000004A9 System.UInt16 Sirenix.Serialization.ProperBitConverter::ToUInt16(System.Byte[],System.Int32)
extern void ProperBitConverter_ToUInt16_mAA5F544ECEE8BA577729309719737227505A5BA8 (void);
// 0x000004AA System.Int32 Sirenix.Serialization.ProperBitConverter::ToInt32(System.Byte[],System.Int32)
extern void ProperBitConverter_ToInt32_m321C96A3E7D15DC43FF9AAAFD8D704D818D06645 (void);
// 0x000004AB System.UInt32 Sirenix.Serialization.ProperBitConverter::ToUInt32(System.Byte[],System.Int32)
extern void ProperBitConverter_ToUInt32_m6D91CF4AA9AC8190264BC49402E36279454A21AA (void);
// 0x000004AC System.Int64 Sirenix.Serialization.ProperBitConverter::ToInt64(System.Byte[],System.Int32)
extern void ProperBitConverter_ToInt64_m757A85166D887059C89D2D3E7B373A96C012A620 (void);
// 0x000004AD System.UInt64 Sirenix.Serialization.ProperBitConverter::ToUInt64(System.Byte[],System.Int32)
extern void ProperBitConverter_ToUInt64_m379C28960F68204086D71DDF18E9AE6202104134 (void);
// 0x000004AE System.Single Sirenix.Serialization.ProperBitConverter::ToSingle(System.Byte[],System.Int32)
extern void ProperBitConverter_ToSingle_mADFCA0C670800E6C70A3C4C708E7EB792930C5E6 (void);
// 0x000004AF System.Double Sirenix.Serialization.ProperBitConverter::ToDouble(System.Byte[],System.Int32)
extern void ProperBitConverter_ToDouble_mC96B07516E93BBADAC260B531DC5F6AF60F2E9D1 (void);
// 0x000004B0 System.Decimal Sirenix.Serialization.ProperBitConverter::ToDecimal(System.Byte[],System.Int32)
extern void ProperBitConverter_ToDecimal_m533993F2DE9AC84867BA9C856AE13D7F19A98DF3 (void);
// 0x000004B1 System.Guid Sirenix.Serialization.ProperBitConverter::ToGuid(System.Byte[],System.Int32)
extern void ProperBitConverter_ToGuid_m636E872E3100CCF26A92A2EAB6EF82E123178AF7 (void);
// 0x000004B2 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Int16)
extern void ProperBitConverter_GetBytes_m2140B571B96FBC227AAA36297CB5D4E81B7582B4 (void);
// 0x000004B3 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.UInt16)
extern void ProperBitConverter_GetBytes_mC2BB412AA9804081FFED7A1CBB93A0EC0F393B1C (void);
// 0x000004B4 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Int32)
extern void ProperBitConverter_GetBytes_m3C74377A2FA7EC2D14474D235407C7038FADDCF6 (void);
// 0x000004B5 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.UInt32)
extern void ProperBitConverter_GetBytes_m4CCD7BFCC98DC4E95314828C1BCC36C3FBF6320A (void);
// 0x000004B6 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Int64)
extern void ProperBitConverter_GetBytes_m6CCB53D54E3FB5A2AAE9ACB4D596E841DBCC17C1 (void);
// 0x000004B7 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.UInt64)
extern void ProperBitConverter_GetBytes_mD3F9A4E496E67B014CCAD7F4A7D091083493B175 (void);
// 0x000004B8 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Single)
extern void ProperBitConverter_GetBytes_mCD50D385DB302DEAD21AC084FFD4008E6374A261 (void);
// 0x000004B9 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Double)
extern void ProperBitConverter_GetBytes_mF0B7BE416F3D35B9BF0EAA9B0EB6A49FDB43B75D (void);
// 0x000004BA System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Decimal)
extern void ProperBitConverter_GetBytes_m1F2855E87476C1D4304B5AC1590DDA5DD14EBEF5 (void);
// 0x000004BB System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Guid)
extern void ProperBitConverter_GetBytes_m318EC6D9C8155E1306F14C4F1E11DD05D8FFD2F3 (void);
// 0x000004BC System.Void Sirenix.Serialization.ProperBitConverter::.cctor()
extern void ProperBitConverter__cctor_mF05F0DEEEA105549206120C109A3C8B35E874CD0 (void);
// 0x000004BD Sirenix.Serialization.IDataWriter Sirenix.Serialization.SerializationUtility::CreateWriter(System.IO.Stream,Sirenix.Serialization.SerializationContext,Sirenix.Serialization.DataFormat)
extern void SerializationUtility_CreateWriter_m02E9DBDA55D4D54CF44FE731032106E92ED982C0 (void);
// 0x000004BE Sirenix.Serialization.IDataReader Sirenix.Serialization.SerializationUtility::CreateReader(System.IO.Stream,Sirenix.Serialization.DeserializationContext,Sirenix.Serialization.DataFormat)
extern void SerializationUtility_CreateReader_m4461B6525F1F47656377F900531544BFA4B9DC6F (void);
// 0x000004BF Sirenix.Serialization.IDataWriter Sirenix.Serialization.SerializationUtility::GetCachedWriter(System.IDisposable&,Sirenix.Serialization.DataFormat,System.IO.Stream,Sirenix.Serialization.SerializationContext)
extern void SerializationUtility_GetCachedWriter_m99185D13CA8B42F0EDEA7FAA57A5109161632971 (void);
// 0x000004C0 Sirenix.Serialization.IDataReader Sirenix.Serialization.SerializationUtility::GetCachedReader(System.IDisposable&,Sirenix.Serialization.DataFormat,System.IO.Stream,Sirenix.Serialization.DeserializationContext)
extern void SerializationUtility_GetCachedReader_m6A1F7B68F51637F42C5F2AF1D218413FD057A2EA (void);
// 0x000004C1 System.Void Sirenix.Serialization.SerializationUtility::SerializeValueWeak(System.Object,Sirenix.Serialization.IDataWriter)
extern void SerializationUtility_SerializeValueWeak_mF02C288CF93016F141D308687C503E6E1BFC5AEA (void);
// 0x000004C2 System.Void Sirenix.Serialization.SerializationUtility::SerializeValueWeak(System.Object,Sirenix.Serialization.IDataWriter,System.Collections.Generic.List`1<UnityEngine.Object>&)
extern void SerializationUtility_SerializeValueWeak_m87595E3087D81EF94E2CDCEBC663D93B04F0CFBA (void);
// 0x000004C3 System.Void Sirenix.Serialization.SerializationUtility::SerializeValue(T,Sirenix.Serialization.IDataWriter)
// 0x000004C4 System.Void Sirenix.Serialization.SerializationUtility::SerializeValue(T,Sirenix.Serialization.IDataWriter,System.Collections.Generic.List`1<UnityEngine.Object>&)
// 0x000004C5 System.Void Sirenix.Serialization.SerializationUtility::SerializeValueWeak(System.Object,System.IO.Stream,Sirenix.Serialization.DataFormat,Sirenix.Serialization.SerializationContext)
extern void SerializationUtility_SerializeValueWeak_m15C5C6DD7B0C7F75B91131551B3ADF997EDE69C0 (void);
// 0x000004C6 System.Void Sirenix.Serialization.SerializationUtility::SerializeValueWeak(System.Object,System.IO.Stream,Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.SerializationContext)
extern void SerializationUtility_SerializeValueWeak_mD8D00A51EE11993198668D3745C6D23C928830FE (void);
// 0x000004C7 System.Void Sirenix.Serialization.SerializationUtility::SerializeValue(T,System.IO.Stream,Sirenix.Serialization.DataFormat,Sirenix.Serialization.SerializationContext)
// 0x000004C8 System.Void Sirenix.Serialization.SerializationUtility::SerializeValue(T,System.IO.Stream,Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.SerializationContext)
// 0x000004C9 System.Byte[] Sirenix.Serialization.SerializationUtility::SerializeValueWeak(System.Object,Sirenix.Serialization.DataFormat,Sirenix.Serialization.SerializationContext)
extern void SerializationUtility_SerializeValueWeak_m0ED19276C9409DD002E261891DA23A3B1308CD5E (void);
// 0x000004CA System.Byte[] Sirenix.Serialization.SerializationUtility::SerializeValueWeak(System.Object,Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>&)
extern void SerializationUtility_SerializeValueWeak_mAC921280E17D11713CBBF195635E28011526B9B4 (void);
// 0x000004CB System.Byte[] Sirenix.Serialization.SerializationUtility::SerializeValue(T,Sirenix.Serialization.DataFormat,Sirenix.Serialization.SerializationContext)
// 0x000004CC System.Byte[] Sirenix.Serialization.SerializationUtility::SerializeValue(T,Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.SerializationContext)
// 0x000004CD System.Object Sirenix.Serialization.SerializationUtility::DeserializeValueWeak(Sirenix.Serialization.IDataReader)
extern void SerializationUtility_DeserializeValueWeak_mA53CD489DF03F94AF43739B7196C7062EE20D8E0 (void);
// 0x000004CE System.Object Sirenix.Serialization.SerializationUtility::DeserializeValueWeak(Sirenix.Serialization.IDataReader,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void SerializationUtility_DeserializeValueWeak_mA3DD666CB0E4F0164D278B729996FD46697C190E (void);
// 0x000004CF T Sirenix.Serialization.SerializationUtility::DeserializeValue(Sirenix.Serialization.IDataReader)
// 0x000004D0 T Sirenix.Serialization.SerializationUtility::DeserializeValue(Sirenix.Serialization.IDataReader,System.Collections.Generic.List`1<UnityEngine.Object>)
// 0x000004D1 System.Object Sirenix.Serialization.SerializationUtility::DeserializeValueWeak(System.IO.Stream,Sirenix.Serialization.DataFormat,Sirenix.Serialization.DeserializationContext)
extern void SerializationUtility_DeserializeValueWeak_m65B4D231C6C0A5DF9DCF5C38AB4B112C6BD6F7FF (void);
// 0x000004D2 System.Object Sirenix.Serialization.SerializationUtility::DeserializeValueWeak(System.IO.Stream,Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>,Sirenix.Serialization.DeserializationContext)
extern void SerializationUtility_DeserializeValueWeak_m8B6EFF2B51D5D4E7EC944135D241B5E89D534637 (void);
// 0x000004D3 T Sirenix.Serialization.SerializationUtility::DeserializeValue(System.IO.Stream,Sirenix.Serialization.DataFormat,Sirenix.Serialization.DeserializationContext)
// 0x000004D4 T Sirenix.Serialization.SerializationUtility::DeserializeValue(System.IO.Stream,Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>,Sirenix.Serialization.DeserializationContext)
// 0x000004D5 System.Object Sirenix.Serialization.SerializationUtility::DeserializeValueWeak(System.Byte[],Sirenix.Serialization.DataFormat,Sirenix.Serialization.DeserializationContext)
extern void SerializationUtility_DeserializeValueWeak_mC446ABEDB28C13124A93700D8F842C8FC27CBC40 (void);
// 0x000004D6 System.Object Sirenix.Serialization.SerializationUtility::DeserializeValueWeak(System.Byte[],Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void SerializationUtility_DeserializeValueWeak_m54CC756C9343A42C6AB60FDD756B5484620617FD (void);
// 0x000004D7 T Sirenix.Serialization.SerializationUtility::DeserializeValue(System.Byte[],Sirenix.Serialization.DataFormat,Sirenix.Serialization.DeserializationContext)
// 0x000004D8 T Sirenix.Serialization.SerializationUtility::DeserializeValue(System.Byte[],Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>,Sirenix.Serialization.DeserializationContext)
// 0x000004D9 System.Object Sirenix.Serialization.SerializationUtility::CreateCopy(System.Object)
extern void SerializationUtility_CreateCopy_mBDA87EA78EC963F1A2D28560AC9DD0E09E23D653 (void);
// 0x000004DA System.Boolean Sirenix.Serialization.UnitySerializationUtility::OdinWillSerialize(System.Reflection.MemberInfo,System.Boolean,Sirenix.Serialization.ISerializationPolicy)
extern void UnitySerializationUtility_OdinWillSerialize_m4A038ED0300BE52C49C4254D74E8EC46D4B16007 (void);
// 0x000004DB System.Boolean Sirenix.Serialization.UnitySerializationUtility::CalculateOdinWillSerialize(System.Reflection.MemberInfo,System.Boolean,Sirenix.Serialization.ISerializationPolicy)
extern void UnitySerializationUtility_CalculateOdinWillSerialize_mDB549CCFCDCC8E71879C8B73256BFDB454439506 (void);
// 0x000004DC System.Boolean Sirenix.Serialization.UnitySerializationUtility::GuessIfUnityWillSerialize(System.Reflection.MemberInfo)
extern void UnitySerializationUtility_GuessIfUnityWillSerialize_m46AD7B18F1835DB02AE64CF7C85E4E1C2C3E7FE6 (void);
// 0x000004DD System.Boolean Sirenix.Serialization.UnitySerializationUtility::GuessIfUnityWillSerializePrivate(System.Reflection.MemberInfo)
extern void UnitySerializationUtility_GuessIfUnityWillSerializePrivate_mDA886C136BEB13AF5BF526C66BD397DDE1D33458 (void);
// 0x000004DE System.Boolean Sirenix.Serialization.UnitySerializationUtility::GuessIfUnityWillSerialize(System.Type)
extern void UnitySerializationUtility_GuessIfUnityWillSerialize_mB5FCC469C4C54A79438E8127D6A69F86EAE1D2C0 (void);
// 0x000004DF System.Boolean Sirenix.Serialization.UnitySerializationUtility::GuessIfUnityWillSerializePrivate(System.Type)
extern void UnitySerializationUtility_GuessIfUnityWillSerializePrivate_m86AC799B7E3A0AB4AD836721F225079932BF45F2 (void);
// 0x000004E0 System.Void Sirenix.Serialization.UnitySerializationUtility::SerializeUnityObject(UnityEngine.Object,Sirenix.Serialization.SerializationData&,System.Boolean,Sirenix.Serialization.SerializationContext)
extern void UnitySerializationUtility_SerializeUnityObject_m9C6AF6F004397642A019B2BBB602609C720D33CF (void);
// 0x000004E1 System.Void Sirenix.Serialization.UnitySerializationUtility::SerializeUnityObject(UnityEngine.Object,System.String&,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.DataFormat,System.Boolean,Sirenix.Serialization.SerializationContext)
extern void UnitySerializationUtility_SerializeUnityObject_m49DF881F3AC881AFCA536828C3AE05FFAC3C4FC1 (void);
// 0x000004E2 System.Void Sirenix.Serialization.UnitySerializationUtility::SerializeUnityObject(UnityEngine.Object,System.Byte[]&,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.DataFormat,System.Boolean,Sirenix.Serialization.SerializationContext)
extern void UnitySerializationUtility_SerializeUnityObject_m83356366B48DC0D9FEFD45CE574CDBB3C944A7E6 (void);
// 0x000004E3 System.Void Sirenix.Serialization.UnitySerializationUtility::SerializeUnityObject(UnityEngine.Object,Sirenix.Serialization.IDataWriter,System.Boolean)
extern void UnitySerializationUtility_SerializeUnityObject_mD6FF50D900D7892B3824FA907B6DD7768796681D (void);
// 0x000004E4 System.Void Sirenix.Serialization.UnitySerializationUtility::DeserializeUnityObject(UnityEngine.Object,Sirenix.Serialization.SerializationData&,Sirenix.Serialization.DeserializationContext)
extern void UnitySerializationUtility_DeserializeUnityObject_m3ACBB8BF2373E5BBCAD6573C5DC78A2AE4D58DCD (void);
// 0x000004E5 System.Void Sirenix.Serialization.UnitySerializationUtility::DeserializeUnityObject(UnityEngine.Object,Sirenix.Serialization.SerializationData&,Sirenix.Serialization.DeserializationContext,System.Boolean,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void UnitySerializationUtility_DeserializeUnityObject_m2A1249FD954C11A4E9FF6016F493147B73EEE7EC (void);
// 0x000004E6 System.Void Sirenix.Serialization.UnitySerializationUtility::DeserializeUnityObject(UnityEngine.Object,System.String&,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.DataFormat,Sirenix.Serialization.DeserializationContext)
extern void UnitySerializationUtility_DeserializeUnityObject_m0C402CCBC6C46AFE969621CA8C1F9CF27E53AB14 (void);
// 0x000004E7 System.Void Sirenix.Serialization.UnitySerializationUtility::DeserializeUnityObject(UnityEngine.Object,System.Byte[]&,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.DataFormat,Sirenix.Serialization.DeserializationContext)
extern void UnitySerializationUtility_DeserializeUnityObject_mDE3EA623012D7C068E5081F66FF9B2122B01E170 (void);
// 0x000004E8 System.Void Sirenix.Serialization.UnitySerializationUtility::DeserializeUnityObject(UnityEngine.Object,Sirenix.Serialization.IDataReader)
extern void UnitySerializationUtility_DeserializeUnityObject_m1B0EB4E3A774362CB0B19201CC535D7BD47FF450 (void);
// 0x000004E9 System.Collections.Generic.List`1<System.String> Sirenix.Serialization.UnitySerializationUtility::SerializePrefabModifications(System.Collections.Generic.List`1<Sirenix.Serialization.PrefabModification>,System.Collections.Generic.List`1<UnityEngine.Object>&)
extern void UnitySerializationUtility_SerializePrefabModifications_m1A538BA7C479B86505D30DD35627E4C6002242D5 (void);
// 0x000004EA System.String Sirenix.Serialization.UnitySerializationUtility::GetStringFromStreamAndReset(System.IO.Stream)
extern void UnitySerializationUtility_GetStringFromStreamAndReset_mCEB869F3E6138858841D8005E2BA3DD81BEDD3FE (void);
// 0x000004EB System.Collections.Generic.List`1<Sirenix.Serialization.PrefabModification> Sirenix.Serialization.UnitySerializationUtility::DeserializePrefabModifications(System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void UnitySerializationUtility_DeserializePrefabModifications_m9A7AB7D00B9C0800377CDB6B7AA3539FCD207DBE (void);
// 0x000004EC System.Object Sirenix.Serialization.UnitySerializationUtility::CreateDefaultUnityInitializedObject(System.Type)
extern void UnitySerializationUtility_CreateDefaultUnityInitializedObject_m262FC947FCEFBD63C6BF5B036C39439885DD9977 (void);
// 0x000004ED System.Object Sirenix.Serialization.UnitySerializationUtility::CreateDefaultUnityInitializedObject(System.Type,System.Int32)
extern void UnitySerializationUtility_CreateDefaultUnityInitializedObject_m47CE3AF7A3AAB732C6B38DDA8A01A80E6BC39769 (void);
// 0x000004EE System.Void Sirenix.Serialization.UnitySerializationUtility::ApplyPrefabModifications(UnityEngine.Object,System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void UnitySerializationUtility_ApplyPrefabModifications_m30545B67AC7DDA5859C1929CA1BA6857C8932065 (void);
// 0x000004EF Sirenix.Serialization.Utilities.WeakValueGetter Sirenix.Serialization.UnitySerializationUtility::GetCachedUnityMemberGetter(System.Reflection.MemberInfo)
extern void UnitySerializationUtility_GetCachedUnityMemberGetter_m4173D15DCD31A2494C881FCA53B0C11B0481AD5C (void);
// 0x000004F0 Sirenix.Serialization.Utilities.WeakValueSetter Sirenix.Serialization.UnitySerializationUtility::GetCachedUnityMemberSetter(System.Reflection.MemberInfo)
extern void UnitySerializationUtility_GetCachedUnityMemberSetter_mE06FFD6B82AC1EEDB2AFDDDA56101C71D28E2D19 (void);
// 0x000004F1 Sirenix.Serialization.Utilities.ICache Sirenix.Serialization.UnitySerializationUtility::GetCachedUnityWriter(Sirenix.Serialization.DataFormat,System.IO.Stream,Sirenix.Serialization.SerializationContext)
extern void UnitySerializationUtility_GetCachedUnityWriter_m3CDF3E157B25F699B5BB9896DF02AD28ADC7DE2E (void);
// 0x000004F2 Sirenix.Serialization.Utilities.ICache Sirenix.Serialization.UnitySerializationUtility::GetCachedUnityReader(Sirenix.Serialization.DataFormat,System.IO.Stream,Sirenix.Serialization.DeserializationContext)
extern void UnitySerializationUtility_GetCachedUnityReader_m103B83E012DECCDB6CAC3BD232AC576A0F2F686C (void);
// 0x000004F3 System.Void Sirenix.Serialization.UnitySerializationUtility::.cctor()
extern void UnitySerializationUtility__cctor_mDB051BCFD1F4D9D479518AF2B14698444E1A2F57 (void);
// 0x000004F4 System.Void Sirenix.Serialization.UnitySerializationUtility/<>c::.cctor()
extern void U3CU3Ec__cctor_m473EE8642574288A98B20E012EDD063513EBCA28 (void);
// 0x000004F5 System.Void Sirenix.Serialization.UnitySerializationUtility/<>c::.ctor()
extern void U3CU3Ec__ctor_m4332BC882242005B731F8159A9EB53832F250A43 (void);
// 0x000004F6 System.Int32 Sirenix.Serialization.UnitySerializationUtility/<>c::<SerializePrefabModifications>b__32_0(Sirenix.Serialization.PrefabModification,Sirenix.Serialization.PrefabModification)
extern void U3CU3Ec_U3CSerializePrefabModificationsU3Eb__32_0_m8FFB91C25A6CC17B806EBF10ADE7472649DF4816 (void);
// 0x000004F7 System.Void Sirenix.Serialization.UnitySerializationUtility/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m49AFF3A3A2BCCB0F88CF8A8371BF9A70304ACD4A (void);
// 0x000004F8 System.Object Sirenix.Serialization.UnitySerializationUtility/<>c__DisplayClass38_0::<GetCachedUnityMemberGetter>b__0(System.Object&)
extern void U3CU3Ec__DisplayClass38_0_U3CGetCachedUnityMemberGetterU3Eb__0_m8EFE2B2DC02F54952344C4B7099BA819AB4490FB (void);
// 0x000004F9 System.Void Sirenix.Serialization.UnitySerializationUtility/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m6FECBB79E042E8C6A1DB5CCD5821981BADF9D4E3 (void);
// 0x000004FA System.Void Sirenix.Serialization.UnitySerializationUtility/<>c__DisplayClass39_0::<GetCachedUnityMemberSetter>b__0(System.Object&,System.Object)
extern void U3CU3Ec__DisplayClass39_0_U3CGetCachedUnityMemberSetterU3Eb__0_m5F7874E79F1AB894016EE8EFEECE00C427BF3F0B (void);
// 0x000004FB System.Boolean Sirenix.Serialization.Utilities.FieldInfoExtensions::IsAliasField(System.Reflection.FieldInfo)
extern void FieldInfoExtensions_IsAliasField_m398CAF3DB1F9E890A1FFA30A3FE5BAB20935A071 (void);
// 0x000004FC System.Reflection.FieldInfo Sirenix.Serialization.Utilities.FieldInfoExtensions::DeAliasField(System.Reflection.FieldInfo,System.Boolean)
extern void FieldInfoExtensions_DeAliasField_m7CA6F3403FD5D2639556A9691DA441B81B41F639 (void);
// 0x000004FD Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1<T> Sirenix.Serialization.Utilities.GarbageFreeIterators::GFIterator(System.Collections.Generic.List`1<T>)
// 0x000004FE Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2<T1,T2> Sirenix.Serialization.Utilities.GarbageFreeIterators::GFIterator(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x000004FF Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2<T1,T2> Sirenix.Serialization.Utilities.GarbageFreeIterators::GFValueIterator(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x00000500 Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1<T> Sirenix.Serialization.Utilities.GarbageFreeIterators::GFIterator(System.Collections.Generic.HashSet`1<T>)
// 0x00000501 System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1::.ctor(System.Collections.Generic.List`1<T>)
// 0x00000502 Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1<T> Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1::GetEnumerator()
// 0x00000503 T Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1::get_Current()
// 0x00000504 System.Boolean Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1::MoveNext()
// 0x00000505 System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1::Dispose()
// 0x00000506 System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000507 Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1<T> Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1::GetEnumerator()
// 0x00000508 T Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1::get_Current()
// 0x00000509 System.Boolean Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1::MoveNext()
// 0x0000050A System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1::Dispose()
// 0x0000050B System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2::.ctor(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x0000050C Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2<T1,T2> Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2::GetEnumerator()
// 0x0000050D System.Collections.Generic.KeyValuePair`2<T1,T2> Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2::get_Current()
// 0x0000050E System.Boolean Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2::MoveNext()
// 0x0000050F System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2::Dispose()
// 0x00000510 System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::.ctor(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x00000511 Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2<T1,T2> Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::GetEnumerator()
// 0x00000512 T2 Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::get_Current()
// 0x00000513 System.Boolean Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::MoveNext()
// 0x00000514 System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::Dispose()
// 0x00000515 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.LinqExtensions::ForEach(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
// 0x00000516 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.LinqExtensions::ForEach(System.Collections.Generic.IEnumerable`1<T>,System.Action`2<T,System.Int32>)
// 0x00000517 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.LinqExtensions::Append(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000518 System.Void Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::.ctor(System.Int32)
// 0x00000519 System.Void Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::System.IDisposable.Dispose()
// 0x0000051A System.Boolean Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::MoveNext()
// 0x0000051B System.Void Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::<>m__Finally1()
// 0x0000051C System.Void Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::<>m__Finally2()
// 0x0000051D T Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000051E System.Void Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::System.Collections.IEnumerator.Reset()
// 0x0000051F System.Object Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::System.Collections.IEnumerator.get_Current()
// 0x00000520 System.Collections.Generic.IEnumerator`1<T> Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000521 System.Collections.IEnumerator Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000522 System.Boolean Sirenix.Serialization.Utilities.MemberInfoExtensions::IsDefined(System.Reflection.ICustomAttributeProvider,System.Boolean)
// 0x00000523 System.Boolean Sirenix.Serialization.Utilities.MemberInfoExtensions::IsDefined(System.Reflection.ICustomAttributeProvider)
// 0x00000524 T Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttribute(System.Reflection.ICustomAttributeProvider,System.Boolean)
// 0x00000525 T Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttribute(System.Reflection.ICustomAttributeProvider)
// 0x00000526 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider)
// 0x00000527 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider,System.Boolean)
// 0x00000528 System.Attribute[] Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider)
extern void MemberInfoExtensions_GetAttributes_mD782A90C748F653B02C2BCC8C7F8F8ADB6B65D70 (void);
// 0x00000529 System.Attribute[] Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider,System.Boolean)
extern void MemberInfoExtensions_GetAttributes_mA2399E26C93BD5E3D7A4F9D3955F20732203D62A (void);
// 0x0000052A System.String Sirenix.Serialization.Utilities.MemberInfoExtensions::GetNiceName(System.Reflection.MemberInfo)
extern void MemberInfoExtensions_GetNiceName_m5A9A8D891272D074286741039D4B149EEBCE63F3 (void);
// 0x0000052B System.Boolean Sirenix.Serialization.Utilities.MemberInfoExtensions::IsStatic(System.Reflection.MemberInfo)
extern void MemberInfoExtensions_IsStatic_m92C8266D7E99D8FCFD5B2D130B6D612F6D746A44 (void);
// 0x0000052C System.Boolean Sirenix.Serialization.Utilities.MemberInfoExtensions::IsAlias(System.Reflection.MemberInfo)
extern void MemberInfoExtensions_IsAlias_m1EE81196457E6519345454055B6AB352C8B13C97 (void);
// 0x0000052D System.Reflection.MemberInfo Sirenix.Serialization.Utilities.MemberInfoExtensions::DeAlias(System.Reflection.MemberInfo,System.Boolean)
extern void MemberInfoExtensions_DeAlias_mB33E9231B63A85EDB7FB78767DC9241CA3C33853 (void);
// 0x0000052E System.String Sirenix.Serialization.Utilities.MethodInfoExtensions::GetFullName(System.Reflection.MethodBase,System.String)
extern void MethodInfoExtensions_GetFullName_m4D4D2E1FF1C1D1F0C6FFEE3AF6F4B648129AEEE1 (void);
// 0x0000052F System.String Sirenix.Serialization.Utilities.MethodInfoExtensions::GetParamsNames(System.Reflection.MethodBase)
extern void MethodInfoExtensions_GetParamsNames_mC382256243DD039BF9AE133EE40A39ACF7524CA5 (void);
// 0x00000530 System.String Sirenix.Serialization.Utilities.MethodInfoExtensions::GetFullName(System.Reflection.MethodBase)
extern void MethodInfoExtensions_GetFullName_mA2252143DBADBCD585950C1C2E62F948D2674CDC (void);
// 0x00000531 System.Boolean Sirenix.Serialization.Utilities.MethodInfoExtensions::IsExtensionMethod(System.Reflection.MethodBase)
extern void MethodInfoExtensions_IsExtensionMethod_m3FACB2B5F24DCE49515887532D8232A4886B618D (void);
// 0x00000532 System.Boolean Sirenix.Serialization.Utilities.MethodInfoExtensions::IsAliasMethod(System.Reflection.MethodInfo)
extern void MethodInfoExtensions_IsAliasMethod_m20B3034326AA3F95D9793F423E2825434E8AD94F (void);
// 0x00000533 System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MethodInfoExtensions::DeAliasMethod(System.Reflection.MethodInfo,System.Boolean)
extern void MethodInfoExtensions_DeAliasMethod_m7B9DCEACE52AD2D8E8E3BD0D166D6CD253707811 (void);
// 0x00000534 System.Boolean Sirenix.Serialization.Utilities.PathUtilities::HasSubDirectory(System.IO.DirectoryInfo,System.IO.DirectoryInfo)
extern void PathUtilities_HasSubDirectory_mBE6C0DDF57CEB2E0D21BEAE9BCA72BCB07DC8C2B (void);
// 0x00000535 System.Boolean Sirenix.Serialization.Utilities.PropertyInfoExtensions::IsAutoProperty(System.Reflection.PropertyInfo,System.Boolean)
extern void PropertyInfoExtensions_IsAutoProperty_mA7690BC6F8BFB91FB4D32626F9B7244ED6B9DBB9 (void);
// 0x00000536 System.Boolean Sirenix.Serialization.Utilities.PropertyInfoExtensions::IsAliasProperty(System.Reflection.PropertyInfo)
extern void PropertyInfoExtensions_IsAliasProperty_m9B5A46F946052C275D8BFFC0637B94116B0A16E6 (void);
// 0x00000537 System.Reflection.PropertyInfo Sirenix.Serialization.Utilities.PropertyInfoExtensions::DeAliasProperty(System.Reflection.PropertyInfo,System.Boolean)
extern void PropertyInfoExtensions_DeAliasProperty_mF26930F3FDFA4897D50A07B9B55B3021FABA23C7 (void);
// 0x00000538 System.String Sirenix.Serialization.Utilities.StringExtensions::ToTitleCase(System.String)
extern void StringExtensions_ToTitleCase_m20933A3645B0BA1CBE7261E1B3C43C04A79FE02E (void);
// 0x00000539 System.Boolean Sirenix.Serialization.Utilities.StringExtensions::IsNullOrWhitespace(System.String)
extern void StringExtensions_IsNullOrWhitespace_m2CF893DEBA74662B72373457906014BB8926FE58 (void);
// 0x0000053A System.String Sirenix.Serialization.Utilities.TypeExtensions::GetCachedNiceName(System.Type)
extern void TypeExtensions_GetCachedNiceName_m82A235935C043929775365C519625DDFC988F2CC (void);
// 0x0000053B System.String Sirenix.Serialization.Utilities.TypeExtensions::CreateNiceName(System.Type)
extern void TypeExtensions_CreateNiceName_m9AC8F0E50037A12D5BBD4E26DEFCA2D215A1DD6B (void);
// 0x0000053C System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasCastDefined(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_HasCastDefined_m9290DC6C1ECD292809874ECB8EA128441BAA2E32 (void);
// 0x0000053D System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsValidIdentifier(System.String)
extern void TypeExtensions_IsValidIdentifier_mDF93ACD96CFCC5F971CE59E8A79CDCD20655496E (void);
// 0x0000053E System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsValidIdentifierStartCharacter(System.Char)
extern void TypeExtensions_IsValidIdentifierStartCharacter_mF722890817B7B4F4446651E0BF5965077AE650F0 (void);
// 0x0000053F System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsValidIdentifierPartCharacter(System.Char)
extern void TypeExtensions_IsValidIdentifierPartCharacter_mCA07EF328158AB02B2E2680A67F4AD0C0150B2BE (void);
// 0x00000540 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsCastableTo(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_IsCastableTo_m578BBAD72A62999057382CD59FDD1F72C1643C7C (void);
// 0x00000541 System.Func`2<System.Object,System.Object> Sirenix.Serialization.Utilities.TypeExtensions::GetCastMethodDelegate(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_GetCastMethodDelegate_m9AF21C776CA8046D583719E67039416332715A17 (void);
// 0x00000542 System.Func`2<TFrom,TTo> Sirenix.Serialization.Utilities.TypeExtensions::GetCastMethodDelegate(System.Boolean)
// 0x00000543 System.Reflection.MethodInfo Sirenix.Serialization.Utilities.TypeExtensions::GetCastMethod(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_GetCastMethod_m46043887A5FDC4603156BA596FF58E33B861649F (void);
// 0x00000544 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::FloatEqualityComparer(System.Single,System.Single)
extern void TypeExtensions_FloatEqualityComparer_m45336611D9A63B1F8BF6BB9126A799DE7D610E8D (void);
// 0x00000545 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::DoubleEqualityComparer(System.Double,System.Double)
extern void TypeExtensions_DoubleEqualityComparer_mA261793CDB4CF0F66BD11B3F8086C94366EB37AC (void);
// 0x00000546 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::QuaternionEqualityComparer(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void TypeExtensions_QuaternionEqualityComparer_mCF439E2E37EE43EFFB34A52908224A546DBEA1F4 (void);
// 0x00000547 System.Func`3<T,T,System.Boolean> Sirenix.Serialization.Utilities.TypeExtensions::GetEqualityComparerDelegate()
// 0x00000548 T Sirenix.Serialization.Utilities.TypeExtensions::GetAttribute(System.Type,System.Boolean)
// 0x00000549 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOrInherits(System.Type,System.Type)
extern void TypeExtensions_ImplementsOrInherits_mA27D78BEA6AF536B9C208AFA20B871F4487A5605 (void);
// 0x0000054A System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOpenGenericType(System.Type,System.Type)
extern void TypeExtensions_ImplementsOpenGenericType_m89D32D68B8220791B5BB9411CFF4E024DDB72A59 (void);
// 0x0000054B System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOpenGenericInterface(System.Type,System.Type)
extern void TypeExtensions_ImplementsOpenGenericInterface_m93141BD0F34B0489021F6CACBB99D3D706242C87 (void);
// 0x0000054C System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOpenGenericClass(System.Type,System.Type)
extern void TypeExtensions_ImplementsOpenGenericClass_mD1FE91E2B27B1F206F8D809DE537CC6843396D9C (void);
// 0x0000054D System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericType(System.Type,System.Type)
extern void TypeExtensions_GetArgumentsOfInheritedOpenGenericType_mA79B7D3DBE1C400FEB480AAC05363A64DE161D17 (void);
// 0x0000054E System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericClass(System.Type,System.Type)
extern void TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m8D1A976AF561C4FF081B0EE6EBF16D1C9BDDB8E2 (void);
// 0x0000054F System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericInterface(System.Type,System.Type)
extern void TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_mCCD3EA914DC8AF10F2DED841DB3415B52636E4DA (void);
// 0x00000550 System.Reflection.MethodInfo Sirenix.Serialization.Utilities.TypeExtensions::GetOperatorMethod(System.Type,Sirenix.Serialization.Utilities.Operator,System.Type,System.Type)
extern void TypeExtensions_GetOperatorMethod_mFDDE0C5788E9BA3152B4D7E5BACF5FB736D22D33 (void);
// 0x00000551 System.Reflection.MethodInfo Sirenix.Serialization.Utilities.TypeExtensions::GetOperatorMethod(System.Type,Sirenix.Serialization.Utilities.Operator)
extern void TypeExtensions_GetOperatorMethod_m2771538F157766F9B357B49289BA061EF5589CB1 (void);
// 0x00000552 System.Reflection.MethodInfo[] Sirenix.Serialization.Utilities.TypeExtensions::GetOperatorMethods(System.Type,Sirenix.Serialization.Utilities.Operator)
extern void TypeExtensions_GetOperatorMethods_m3D42793AD55F198485B224B736D29C878E8E4A83 (void);
// 0x00000553 System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions::GetAllMembers(System.Type,System.Reflection.BindingFlags)
extern void TypeExtensions_GetAllMembers_mEAD8CCA6713BC546C11E6E2383D61EADCF71FD6A (void);
// 0x00000554 System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions::GetAllMembers(System.Type,System.String,System.Reflection.BindingFlags)
extern void TypeExtensions_GetAllMembers_m4FA62093A39775CC258CB5C15B01D837C5E9098B (void);
// 0x00000555 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.TypeExtensions::GetAllMembers(System.Type,System.Reflection.BindingFlags)
// 0x00000556 System.Type Sirenix.Serialization.Utilities.TypeExtensions::GetGenericBaseType(System.Type,System.Type)
extern void TypeExtensions_GetGenericBaseType_m73E3AC661E81FE4361756D30AE9E328BBF785A5E (void);
// 0x00000557 System.Type Sirenix.Serialization.Utilities.TypeExtensions::GetGenericBaseType(System.Type,System.Type,System.Int32&)
extern void TypeExtensions_GetGenericBaseType_m92108E5F1AD7932F658A825AF8FD1879B5EDFCC3 (void);
// 0x00000558 System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GetBaseTypes(System.Type,System.Boolean)
extern void TypeExtensions_GetBaseTypes_mCA60FDCB5B2C0300B38E5EA00FEA4113494C3EE0 (void);
// 0x00000559 System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GetBaseClasses(System.Type,System.Boolean)
extern void TypeExtensions_GetBaseClasses_m52D077E43D5871509D852118A0618042B0948FF5 (void);
// 0x0000055A System.String Sirenix.Serialization.Utilities.TypeExtensions::TypeNameGauntlet(System.Type)
extern void TypeExtensions_TypeNameGauntlet_mC9DE40F790CCF1B330A29F7447611826F403084F (void);
// 0x0000055B System.String Sirenix.Serialization.Utilities.TypeExtensions::GetNiceName(System.Type)
extern void TypeExtensions_GetNiceName_m0F37B9149A1B7694D3A62BDDF7D0635F8F67C043 (void);
// 0x0000055C System.String Sirenix.Serialization.Utilities.TypeExtensions::GetNiceFullName(System.Type)
extern void TypeExtensions_GetNiceFullName_mB795756486D1EE049BEA95070A0852377C3A4408 (void);
// 0x0000055D System.String Sirenix.Serialization.Utilities.TypeExtensions::GetCompilableNiceName(System.Type)
extern void TypeExtensions_GetCompilableNiceName_m6837178F74C38B1D0D32061D16BC35EE83E2F372 (void);
// 0x0000055E System.String Sirenix.Serialization.Utilities.TypeExtensions::GetCompilableNiceFullName(System.Type)
extern void TypeExtensions_GetCompilableNiceFullName_m58AE95F6613A819AA9C78DFE0853F3196A8BE297 (void);
// 0x0000055F T Sirenix.Serialization.Utilities.TypeExtensions::GetCustomAttribute(System.Type,System.Boolean)
// 0x00000560 T Sirenix.Serialization.Utilities.TypeExtensions::GetCustomAttribute(System.Type)
// 0x00000561 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.TypeExtensions::GetCustomAttributes(System.Type)
// 0x00000562 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.TypeExtensions::GetCustomAttributes(System.Type,System.Boolean)
// 0x00000563 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsDefined(System.Type)
// 0x00000564 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsDefined(System.Type,System.Boolean)
// 0x00000565 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::InheritsFrom(System.Type)
// 0x00000566 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::InheritsFrom(System.Type,System.Type)
extern void TypeExtensions_InheritsFrom_mD57A7F3D0E3302A4656C0EE35E6F6756B16EE642 (void);
// 0x00000567 System.Int32 Sirenix.Serialization.Utilities.TypeExtensions::GetInheritanceDistance(System.Type,System.Type)
extern void TypeExtensions_GetInheritanceDistance_mB824EE440D2D786FCC73EF32CD8243330C51C6D0 (void);
// 0x00000568 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasParamaters(System.Reflection.MethodInfo,System.Collections.Generic.IList`1<System.Type>,System.Boolean)
extern void TypeExtensions_HasParamaters_mA5311BE6CD51941C75856CCA8DC4612366A22764 (void);
// 0x00000569 System.Type Sirenix.Serialization.Utilities.TypeExtensions::GetReturnType(System.Reflection.MemberInfo)
extern void TypeExtensions_GetReturnType_mBE8E92DD2B796A26EF66672BACF12360DF2F8F54 (void);
// 0x0000056A System.Object Sirenix.Serialization.Utilities.TypeExtensions::GetMemberValue(System.Reflection.MemberInfo,System.Object)
extern void TypeExtensions_GetMemberValue_mD898651002D3A08963505D71EE607A2070D63304 (void);
// 0x0000056B System.Void Sirenix.Serialization.Utilities.TypeExtensions::SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)
extern void TypeExtensions_SetMemberValue_m5AF88E6BEF682F16962EE82D8E475A9890FDA6EB (void);
// 0x0000056C System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::TryInferGenericParameters(System.Type,System.Type[]&,System.Type[])
extern void TypeExtensions_TryInferGenericParameters_mBF16A731F12A3E15EE42C62834FAD8DB78002289 (void);
// 0x0000056D System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Type,System.Type[])
extern void TypeExtensions_AreGenericConstraintsSatisfiedBy_m895118386C70D74B54F52077842FB62C94A636FD (void);
// 0x0000056E System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Reflection.MethodBase,System.Type[])
extern void TypeExtensions_AreGenericConstraintsSatisfiedBy_m2E3250146F9DD571BBD78C647FAC3F6F80627A8A (void);
// 0x0000056F System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Type[],System.Type[])
extern void TypeExtensions_AreGenericConstraintsSatisfiedBy_mC5A3F144D69B782E27CE8770DBB3F253C11C4E6B (void);
// 0x00000570 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::GenericParameterIsFulfilledBy(System.Type,System.Type)
extern void TypeExtensions_GenericParameterIsFulfilledBy_m1269590C68DE54AEC1CA8D4916A63A179EB1CA72 (void);
// 0x00000571 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::GenericParameterIsFulfilledBy(System.Type,System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Type>,System.Collections.Generic.HashSet`1<System.Type>)
extern void TypeExtensions_GenericParameterIsFulfilledBy_mC3CF07D7A2677E6500F03BD9FDB6F0E0EB3076C7 (void);
// 0x00000572 System.String Sirenix.Serialization.Utilities.TypeExtensions::GetGenericConstraintsString(System.Type,System.Boolean)
extern void TypeExtensions_GetGenericConstraintsString_mDB86975B4C78AB81853D4059387155CD13F73BB8 (void);
// 0x00000573 System.String Sirenix.Serialization.Utilities.TypeExtensions::GetGenericParameterConstraintsString(System.Type,System.Boolean)
extern void TypeExtensions_GetGenericParameterConstraintsString_mDA43C53D8EC51F87FD7FDC72DBFA00B9023C3A77 (void);
// 0x00000574 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::GenericArgumentsContainsTypes(System.Type,System.Type[])
extern void TypeExtensions_GenericArgumentsContainsTypes_m511CA2AF8361F772B728F3387E50A1FC93920CA8 (void);
// 0x00000575 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsFullyConstructedGenericType(System.Type)
extern void TypeExtensions_IsFullyConstructedGenericType_mA4E15B1EC4449FE12E465946483056996224C395 (void);
// 0x00000576 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsNullableType(System.Type)
extern void TypeExtensions_IsNullableType_m6E53FF543018A73E0A7797301B6D6F411EDFF52B (void);
// 0x00000577 System.UInt64 Sirenix.Serialization.Utilities.TypeExtensions::GetEnumBitmask(System.Object,System.Type)
extern void TypeExtensions_GetEnumBitmask_mC90AC3CB163AB1C4BE88CD1F0E26CE6F3BFB63ED (void);
// 0x00000578 System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::SafeGetTypes(System.Reflection.Assembly)
extern void TypeExtensions_SafeGetTypes_m0110261E78C023188AC3BF63911A87F5B5FC2EAE (void);
// 0x00000579 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::SafeIsDefined(System.Reflection.Assembly,System.Type,System.Boolean)
extern void TypeExtensions_SafeIsDefined_m03307A35EFB78B55E14F3267F6FE43E17EDFBD91 (void);
// 0x0000057A System.Object[] Sirenix.Serialization.Utilities.TypeExtensions::SafeGetCustomAttributes(System.Reflection.Assembly,System.Type,System.Boolean)
extern void TypeExtensions_SafeGetCustomAttributes_m311A2826BB7E8A98F02058C6768DDD12E177B8C5 (void);
// 0x0000057B System.Void Sirenix.Serialization.Utilities.TypeExtensions::.cctor()
extern void TypeExtensions__cctor_m0406A5B6145F5C7E1E6C90F8D19FE02B549E973D (void);
// 0x0000057C System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m5725234EB17E7A1B0F7FEA0BD4C9A8561DCC80C4 (void);
// 0x0000057D System.Object Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass29_0::<GetCastMethodDelegate>b__0(System.Object)
extern void U3CU3Ec__DisplayClass29_0_U3CGetCastMethodDelegateU3Eb__0_m50BB07BE75B22C4AF6CDDB3E5C9A89C6CA95C17A (void);
// 0x0000057E System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__35`1::.cctor()
// 0x0000057F System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__35`1::.ctor()
// 0x00000580 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__35`1::<GetEqualityComparerDelegate>b__35_0(T,T)
// 0x00000581 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__35`1::<GetEqualityComparerDelegate>b__35_1(T,T)
// 0x00000582 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_mA4E51B9D41F12F9010B94C276629B47E9A1C00FC (void);
// 0x00000583 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass45_0::<GetOperatorMethod>b__0(System.Reflection.MethodInfo)
extern void U3CU3Ec__DisplayClass45_0_U3CGetOperatorMethodU3Eb__0_m8CD094F2EBD9AE3244478840554A7B7B9F107F7D (void);
// 0x00000584 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_mB08963945B06193B1452AAC860BBDCBD0C3A61C5 (void);
// 0x00000585 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass46_0::<GetOperatorMethods>b__0(System.Reflection.MethodInfo)
extern void U3CU3Ec__DisplayClass46_0_U3CGetOperatorMethodsU3Eb__0_m58F8B033B7596D63ED4ADFEFE229ADF6F8D899E8 (void);
// 0x00000586 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__47::.ctor(System.Int32)
extern void U3CGetAllMembersU3Ed__47__ctor_m6531D2B00F68C79AA5B748966E5EC7289167E288 (void);
// 0x00000587 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__47::System.IDisposable.Dispose()
extern void U3CGetAllMembersU3Ed__47_System_IDisposable_Dispose_m84F1AE700946CF7DCE27BB1DE2E20508A428BE39 (void);
// 0x00000588 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__47::MoveNext()
extern void U3CGetAllMembersU3Ed__47_MoveNext_m12FD74881ACB9997B63F23692F5B0E464AA2D056 (void);
// 0x00000589 System.Reflection.MemberInfo Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__47::System.Collections.Generic.IEnumerator<System.Reflection.MemberInfo>.get_Current()
extern void U3CGetAllMembersU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mE1F39B24C11A8333C232F485FA128DADE03A5A67 (void);
// 0x0000058A System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__47::System.Collections.IEnumerator.Reset()
extern void U3CGetAllMembersU3Ed__47_System_Collections_IEnumerator_Reset_mF10576626E8566133DC612EDD7C3063AB993D436 (void);
// 0x0000058B System.Object Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__47::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllMembersU3Ed__47_System_Collections_IEnumerator_get_Current_m0618AFC2F349BA109D0D2432FDEA0424F4A71FCF (void);
// 0x0000058C System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__47::System.Collections.Generic.IEnumerable<System.Reflection.MemberInfo>.GetEnumerator()
extern void U3CGetAllMembersU3Ed__47_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m80CFB19D9D24F07401075AFD664276D4ED4449CA (void);
// 0x0000058D System.Collections.IEnumerator Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__47::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllMembersU3Ed__47_System_Collections_IEnumerable_GetEnumerator_mDEF3A2F8A3C862A2CB7313470ACC3F44634D697B (void);
// 0x0000058E System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__48::.ctor(System.Int32)
extern void U3CGetAllMembersU3Ed__48__ctor_m422B5C64AF72E942DD56389F2DAF729CC420E47A (void);
// 0x0000058F System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__48::System.IDisposable.Dispose()
extern void U3CGetAllMembersU3Ed__48_System_IDisposable_Dispose_m702840A883EFCBA609C1B1E5BDB86479F602F45E (void);
// 0x00000590 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__48::MoveNext()
extern void U3CGetAllMembersU3Ed__48_MoveNext_m8CE8854D948FD04CEDB1F17C7A48AEE64A066D2C (void);
// 0x00000591 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__48::<>m__Finally1()
extern void U3CGetAllMembersU3Ed__48_U3CU3Em__Finally1_m4F8ECB7E0E5714796F99857A857AD87D0EBE3E24 (void);
// 0x00000592 System.Reflection.MemberInfo Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__48::System.Collections.Generic.IEnumerator<System.Reflection.MemberInfo>.get_Current()
extern void U3CGetAllMembersU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_m3E73B1020BAF18C3D76ECCC12AF3B863E9917F0E (void);
// 0x00000593 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__48::System.Collections.IEnumerator.Reset()
extern void U3CGetAllMembersU3Ed__48_System_Collections_IEnumerator_Reset_m476DA9DD313D5C435E3721F4927E17C7056CBCC6 (void);
// 0x00000594 System.Object Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllMembersU3Ed__48_System_Collections_IEnumerator_get_Current_m2042F1683D4BB87B88A0F443FE5012246CFC01D2 (void);
// 0x00000595 System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__48::System.Collections.Generic.IEnumerable<System.Reflection.MemberInfo>.GetEnumerator()
extern void U3CGetAllMembersU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mBB79179C4C2A31932B3D9890A6190718FC72C66B (void);
// 0x00000596 System.Collections.IEnumerator Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__48::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllMembersU3Ed__48_System_Collections_IEnumerable_GetEnumerator_m36EC0B235F02798C10CADFA0956D30F62127126A (void);
// 0x00000597 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__49`1::.ctor(System.Int32)
// 0x00000598 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__49`1::System.IDisposable.Dispose()
// 0x00000599 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__49`1::MoveNext()
// 0x0000059A T Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__49`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000059B System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__49`1::System.Collections.IEnumerator.Reset()
// 0x0000059C System.Object Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__49`1::System.Collections.IEnumerator.get_Current()
// 0x0000059D System.Collections.Generic.IEnumerator`1<T> Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__49`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000059E System.Collections.IEnumerator Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__49`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000059F System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__53::.ctor(System.Int32)
extern void U3CGetBaseClassesU3Ed__53__ctor_m211FCC3DF22F2FF83E815B2CBB79F0478CE4732E (void);
// 0x000005A0 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__53::System.IDisposable.Dispose()
extern void U3CGetBaseClassesU3Ed__53_System_IDisposable_Dispose_m388B361D0B4D9AB71B2146615E3DA09C35E4A555 (void);
// 0x000005A1 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__53::MoveNext()
extern void U3CGetBaseClassesU3Ed__53_MoveNext_m479481B59F4C34E99FF8EB9C2BCD2491BB6D6086 (void);
// 0x000005A2 System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__53::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern void U3CGetBaseClassesU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m3890F0F6E07922C7EB5BA1ACC451FFB4AA7F79C4 (void);
// 0x000005A3 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__53::System.Collections.IEnumerator.Reset()
extern void U3CGetBaseClassesU3Ed__53_System_Collections_IEnumerator_Reset_mC0351B85DF82DCFD1A2A1CEA5954E779AF0875A3 (void);
// 0x000005A4 System.Object Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__53::System.Collections.IEnumerator.get_Current()
extern void U3CGetBaseClassesU3Ed__53_System_Collections_IEnumerator_get_Current_mD6019B72E79537D9ED00C3AABFA490742F97A5F0 (void);
// 0x000005A5 System.Collections.Generic.IEnumerator`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__53::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern void U3CGetBaseClassesU3Ed__53_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m22890AC370D6A36EDFA6E91FB83E849B8BC14D32 (void);
// 0x000005A6 System.Collections.IEnumerator Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__53::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetBaseClassesU3Ed__53_System_Collections_IEnumerable_GetEnumerator_m7D793D39D20CCD6D3EA70D634B479C7235F1FFC7 (void);
// 0x000005A7 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::.ctor(System.Int32)
// 0x000005A8 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::System.IDisposable.Dispose()
// 0x000005A9 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::MoveNext()
// 0x000005AA T Sirenix.Serialization.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000005AB System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::System.Collections.IEnumerator.Reset()
// 0x000005AC System.Object Sirenix.Serialization.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::System.Collections.IEnumerator.get_Current()
// 0x000005AD System.Collections.Generic.IEnumerator`1<T> Sirenix.Serialization.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000005AE System.Collections.IEnumerator Sirenix.Serialization.Utilities.TypeExtensions/<GetCustomAttributes>d__62`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000005AF System.Void Sirenix.Serialization.Utilities.UnityExtensions::.cctor()
extern void UnityExtensions__cctor_mD3734DA22993FCD933A760EA358613C2052DA334 (void);
// 0x000005B0 System.Boolean Sirenix.Serialization.Utilities.UnityExtensions::SafeIsUnityNull(UnityEngine.Object)
extern void UnityExtensions_SafeIsUnityNull_m4B3E5197918704AD095118BD47D4B3A469F36206 (void);
// 0x000005B1 System.Object Sirenix.Serialization.Utilities.ICache::get_Value()
// 0x000005B2 System.Int32 Sirenix.Serialization.Utilities.Cache`1::get_MaxCacheSize()
// 0x000005B3 System.Void Sirenix.Serialization.Utilities.Cache`1::set_MaxCacheSize(System.Int32)
// 0x000005B4 System.Void Sirenix.Serialization.Utilities.Cache`1::.ctor()
// 0x000005B5 System.Boolean Sirenix.Serialization.Utilities.Cache`1::get_IsFree()
// 0x000005B6 System.Object Sirenix.Serialization.Utilities.Cache`1::Sirenix.Serialization.Utilities.ICache.get_Value()
// 0x000005B7 Sirenix.Serialization.Utilities.Cache`1<T> Sirenix.Serialization.Utilities.Cache`1::Claim()
// 0x000005B8 System.Void Sirenix.Serialization.Utilities.Cache`1::Release(Sirenix.Serialization.Utilities.Cache`1<T>)
// 0x000005B9 T Sirenix.Serialization.Utilities.Cache`1::op_Implicit(Sirenix.Serialization.Utilities.Cache`1<T>)
// 0x000005BA System.Void Sirenix.Serialization.Utilities.Cache`1::Release()
// 0x000005BB System.Void Sirenix.Serialization.Utilities.Cache`1::System.IDisposable.Dispose()
// 0x000005BC System.Void Sirenix.Serialization.Utilities.Cache`1::.cctor()
// 0x000005BD System.Void Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::.ctor()
// 0x000005BE System.Void Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirstKey>,System.Collections.Generic.IEqualityComparer`1<TSecondKey>)
// 0x000005BF System.Collections.Generic.Dictionary`2<TSecondKey,TValue> Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::get_Item(TFirstKey)
// 0x000005C0 System.Int32 Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::InnerCount(TFirstKey)
// 0x000005C1 System.Int32 Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::TotalInnerCount()
// 0x000005C2 System.Boolean Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::ContainsKeys(TFirstKey,TSecondKey)
// 0x000005C3 System.Boolean Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::TryGetInnerValue(TFirstKey,TSecondKey,TValue&)
// 0x000005C4 TValue Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::AddInner(TFirstKey,TSecondKey,TValue)
// 0x000005C5 System.Boolean Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::RemoveInner(TFirstKey,TSecondKey)
// 0x000005C6 System.Void Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::RemoveWhere(System.Func`2<TValue,System.Boolean>)
// 0x000005C7 System.Void Sirenix.Serialization.Utilities.WeakValueGetter::.ctor(System.Object,System.IntPtr)
extern void WeakValueGetter__ctor_mED35E1C9DA471A28679F1DD95CCBEB769A539E84 (void);
// 0x000005C8 System.Object Sirenix.Serialization.Utilities.WeakValueGetter::Invoke(System.Object&)
extern void WeakValueGetter_Invoke_m8342B7DF2EE6049432E1A9199DC3CEB33320FA83 (void);
// 0x000005C9 System.IAsyncResult Sirenix.Serialization.Utilities.WeakValueGetter::BeginInvoke(System.Object&,System.AsyncCallback,System.Object)
extern void WeakValueGetter_BeginInvoke_m41BCE16EE21ECFA0E28251F081254795DD324A9A (void);
// 0x000005CA System.Object Sirenix.Serialization.Utilities.WeakValueGetter::EndInvoke(System.Object&,System.IAsyncResult)
extern void WeakValueGetter_EndInvoke_m91011C4C6096043412A75B127178EBFC0959D1E3 (void);
// 0x000005CB System.Void Sirenix.Serialization.Utilities.WeakValueSetter::.ctor(System.Object,System.IntPtr)
extern void WeakValueSetter__ctor_m96E4D0BD6AAE23BD5D52C8D76006D78012E8F695 (void);
// 0x000005CC System.Void Sirenix.Serialization.Utilities.WeakValueSetter::Invoke(System.Object&,System.Object)
extern void WeakValueSetter_Invoke_m68AAA32E6793D8F1F66D87C22689403D855D301B (void);
// 0x000005CD System.IAsyncResult Sirenix.Serialization.Utilities.WeakValueSetter::BeginInvoke(System.Object&,System.Object,System.AsyncCallback,System.Object)
extern void WeakValueSetter_BeginInvoke_mBC6F95756ECDB9E3A55B27C2500B498C1EF0FD1A (void);
// 0x000005CE System.Void Sirenix.Serialization.Utilities.WeakValueSetter::EndInvoke(System.Object&,System.IAsyncResult)
extern void WeakValueSetter_EndInvoke_m7E534F1E2FDA67F99F05E10A50834A157CED6945 (void);
// 0x000005CF System.Void Sirenix.Serialization.Utilities.WeakValueGetter`1::.ctor(System.Object,System.IntPtr)
// 0x000005D0 FieldType Sirenix.Serialization.Utilities.WeakValueGetter`1::Invoke(System.Object&)
// 0x000005D1 System.IAsyncResult Sirenix.Serialization.Utilities.WeakValueGetter`1::BeginInvoke(System.Object&,System.AsyncCallback,System.Object)
// 0x000005D2 FieldType Sirenix.Serialization.Utilities.WeakValueGetter`1::EndInvoke(System.Object&,System.IAsyncResult)
// 0x000005D3 System.Void Sirenix.Serialization.Utilities.WeakValueSetter`1::.ctor(System.Object,System.IntPtr)
// 0x000005D4 System.Void Sirenix.Serialization.Utilities.WeakValueSetter`1::Invoke(System.Object&,FieldType)
// 0x000005D5 System.IAsyncResult Sirenix.Serialization.Utilities.WeakValueSetter`1::BeginInvoke(System.Object&,FieldType,System.AsyncCallback,System.Object)
// 0x000005D6 System.Void Sirenix.Serialization.Utilities.WeakValueSetter`1::EndInvoke(System.Object&,System.IAsyncResult)
// 0x000005D7 System.Void Sirenix.Serialization.Utilities.ValueGetter`2::.ctor(System.Object,System.IntPtr)
// 0x000005D8 FieldType Sirenix.Serialization.Utilities.ValueGetter`2::Invoke(InstanceType&)
// 0x000005D9 System.IAsyncResult Sirenix.Serialization.Utilities.ValueGetter`2::BeginInvoke(InstanceType&,System.AsyncCallback,System.Object)
// 0x000005DA FieldType Sirenix.Serialization.Utilities.ValueGetter`2::EndInvoke(InstanceType&,System.IAsyncResult)
// 0x000005DB System.Void Sirenix.Serialization.Utilities.ValueSetter`2::.ctor(System.Object,System.IntPtr)
// 0x000005DC System.Void Sirenix.Serialization.Utilities.ValueSetter`2::Invoke(InstanceType&,FieldType)
// 0x000005DD System.IAsyncResult Sirenix.Serialization.Utilities.ValueSetter`2::BeginInvoke(InstanceType&,FieldType,System.AsyncCallback,System.Object)
// 0x000005DE System.Void Sirenix.Serialization.Utilities.ValueSetter`2::EndInvoke(InstanceType&,System.IAsyncResult)
// 0x000005DF System.Boolean Sirenix.Serialization.Utilities.EmitUtilities::get_CanEmit()
extern void EmitUtilities_get_CanEmit_mE5E88AB47D606D83E60D2C439D17C567E7B8D7C1 (void);
// 0x000005E0 System.Func`1<FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateStaticFieldGetter(System.Reflection.FieldInfo)
// 0x000005E1 System.Func`1<System.Object> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakStaticFieldGetter(System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakStaticFieldGetter_m1FBC3793F5B352A17190E83BBA77364B09DFFBB5 (void);
// 0x000005E2 System.Action`1<FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateStaticFieldSetter(System.Reflection.FieldInfo)
// 0x000005E3 System.Action`1<System.Object> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakStaticFieldSetter(System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakStaticFieldSetter_mA2979E00C807511E10693A7FB9673F98E4159C32 (void);
// 0x000005E4 Sirenix.Serialization.Utilities.ValueGetter`2<InstanceType,FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceFieldGetter(System.Reflection.FieldInfo)
// 0x000005E5 Sirenix.Serialization.Utilities.WeakValueGetter`1<FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceFieldGetter(System.Type,System.Reflection.FieldInfo)
// 0x000005E6 Sirenix.Serialization.Utilities.WeakValueGetter Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceFieldGetter(System.Type,System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakInstanceFieldGetter_m8491CE845300E6F6263978A3DB7DD32BA7AC4EC3 (void);
// 0x000005E7 Sirenix.Serialization.Utilities.ValueSetter`2<InstanceType,FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceFieldSetter(System.Reflection.FieldInfo)
// 0x000005E8 Sirenix.Serialization.Utilities.WeakValueSetter`1<FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceFieldSetter(System.Type,System.Reflection.FieldInfo)
// 0x000005E9 Sirenix.Serialization.Utilities.WeakValueSetter Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceFieldSetter(System.Type,System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakInstanceFieldSetter_mB647498773F4B6ED1E8367C95D3159D2B9C377FB (void);
// 0x000005EA Sirenix.Serialization.Utilities.WeakValueGetter Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstancePropertyGetter(System.Type,System.Reflection.PropertyInfo)
extern void EmitUtilities_CreateWeakInstancePropertyGetter_mD368463A4F8F02E3F9D3BDF03D40E2833FEF54D0 (void);
// 0x000005EB Sirenix.Serialization.Utilities.WeakValueSetter Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstancePropertySetter(System.Type,System.Reflection.PropertyInfo)
extern void EmitUtilities_CreateWeakInstancePropertySetter_m2BAE1F7C9DA5ED3950E1CD6D85A097948C745320 (void);
// 0x000005EC System.Action`1<PropType> Sirenix.Serialization.Utilities.EmitUtilities::CreateStaticPropertySetter(System.Reflection.PropertyInfo)
// 0x000005ED System.Func`1<PropType> Sirenix.Serialization.Utilities.EmitUtilities::CreateStaticPropertyGetter(System.Reflection.PropertyInfo)
// 0x000005EE Sirenix.Serialization.Utilities.ValueSetter`2<InstanceType,PropType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstancePropertySetter(System.Reflection.PropertyInfo)
// 0x000005EF Sirenix.Serialization.Utilities.ValueGetter`2<InstanceType,PropType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstancePropertyGetter(System.Reflection.PropertyInfo)
// 0x000005F0 System.Func`2<InstanceType,ReturnType> Sirenix.Serialization.Utilities.EmitUtilities::CreateMethodReturner(System.Reflection.MethodInfo)
// 0x000005F1 System.Action Sirenix.Serialization.Utilities.EmitUtilities::CreateStaticMethodCaller(System.Reflection.MethodInfo)
extern void EmitUtilities_CreateStaticMethodCaller_mC73CAF5DC3615D06925F6FB210FD699A2E8B4B8E (void);
// 0x000005F2 System.Action`2<System.Object,TArg1> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x000005F3 System.Action`1<System.Object> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceMethodCaller(System.Reflection.MethodInfo)
extern void EmitUtilities_CreateWeakInstanceMethodCaller_m4AF052239514128313E0F222E265CA0DF3F099A8 (void);
// 0x000005F4 System.Func`3<System.Object,TArg1,TResult> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x000005F5 System.Func`2<System.Object,TResult> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceMethodCallerFunc(System.Reflection.MethodInfo)
// 0x000005F6 System.Func`3<System.Object,TArg,TResult> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceMethodCallerFunc(System.Reflection.MethodInfo)
// 0x000005F7 System.Action`1<InstanceType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x000005F8 System.Action`2<InstanceType,Arg1> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x000005F9 Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`1<InstanceType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceRefMethodCaller(System.Reflection.MethodInfo)
// 0x000005FA Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`2<InstanceType,Arg1> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceRefMethodCaller(System.Reflection.MethodInfo)
// 0x000005FB System.Void Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`1::.ctor(System.Object,System.IntPtr)
// 0x000005FC System.Void Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`1::Invoke(InstanceType&)
// 0x000005FD System.IAsyncResult Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`1::BeginInvoke(InstanceType&,System.AsyncCallback,System.Object)
// 0x000005FE System.Void Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`1::EndInvoke(InstanceType&,System.IAsyncResult)
// 0x000005FF System.Void Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`2::.ctor(System.Object,System.IntPtr)
// 0x00000600 System.Void Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`2::Invoke(InstanceType&,TArg1)
// 0x00000601 System.IAsyncResult Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`2::BeginInvoke(InstanceType&,TArg1,System.AsyncCallback,System.Object)
// 0x00000602 System.Void Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`2::EndInvoke(InstanceType&,System.IAsyncResult)
// 0x00000603 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass2_0`1::.ctor()
// 0x00000604 FieldType Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass2_0`1::<CreateStaticFieldGetter>b__0()
// 0x00000605 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass2_1`1::.ctor()
// 0x00000606 FieldType Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass2_1`1::<CreateStaticFieldGetter>b__1()
// 0x00000607 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m838783AF688ECD45977D51F2A8AF7AE1F9E69749 (void);
// 0x00000608 System.Object Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass3_0::<CreateWeakStaticFieldGetter>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CCreateWeakStaticFieldGetterU3Eb__0_m06EECB344D795A9CC1D47516B78E3F25A89ACFA5 (void);
// 0x00000609 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass4_0`1::.ctor()
// 0x0000060A System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass4_0`1::<CreateStaticFieldSetter>b__0(FieldType)
// 0x0000060B System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m6A880D2C40F1C7AF5FB0F78BB801433176990121 (void);
// 0x0000060C System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass5_0::<CreateWeakStaticFieldSetter>b__0(System.Object)
extern void U3CU3Ec__DisplayClass5_0_U3CCreateWeakStaticFieldSetterU3Eb__0_m2DF8BEED8539DB13B11019F5EBD645AB748B8C31 (void);
// 0x0000060D System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass6_0`2::.ctor()
// 0x0000060E FieldType Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass6_0`2::<CreateInstanceFieldGetter>b__0(InstanceType&)
// 0x0000060F System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass7_0`1::.ctor()
// 0x00000610 FieldType Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass7_0`1::<CreateWeakInstanceFieldGetter>b__0(System.Object&)
// 0x00000611 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m8A2AF66B262B31F06E2E8FD45838304BD1B54E86 (void);
// 0x00000612 System.Object Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass8_0::<CreateWeakInstanceFieldGetter>b__0(System.Object&)
extern void U3CU3Ec__DisplayClass8_0_U3CCreateWeakInstanceFieldGetterU3Eb__0_m882280ED790345234F95E084D09F9298007FF1E8 (void);
// 0x00000613 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass9_0`2::.ctor()
// 0x00000614 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass9_0`2::<CreateInstanceFieldSetter>b__0(InstanceType&,FieldType)
// 0x00000615 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass10_0`1::.ctor()
// 0x00000616 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass10_0`1::<CreateWeakInstanceFieldSetter>b__0(System.Object&,FieldType)
// 0x00000617 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m8A77D7B4789EA674FAA55E356ADA0C34EE7B1739 (void);
// 0x00000618 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass11_0::<CreateWeakInstanceFieldSetter>b__0(System.Object&,System.Object)
extern void U3CU3Ec__DisplayClass11_0_U3CCreateWeakInstanceFieldSetterU3Eb__0_mA6FD749D5382A43C6F1A377DD199B516785CC023 (void);
// 0x00000619 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m8E4F174ACF247B56894E7FD8EF1153CAD8C8E084 (void);
// 0x0000061A System.Object Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass12_0::<CreateWeakInstancePropertyGetter>b__0(System.Object&)
extern void U3CU3Ec__DisplayClass12_0_U3CCreateWeakInstancePropertyGetterU3Eb__0_mE0C751CCDC79A84F96BE3092040CD1A3B169710B (void);
// 0x0000061B System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mD1019D8F880D140DA98560808B96A1059717BD64 (void);
// 0x0000061C System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass13_0::<CreateWeakInstancePropertySetter>b__0(System.Object&,System.Object)
extern void U3CU3Ec__DisplayClass13_0_U3CCreateWeakInstancePropertySetterU3Eb__0_m1C5F1429DAAC1E5C4C4EC1BC3B010F91872240DF (void);
// 0x0000061D System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass14_0`1::.ctor()
// 0x0000061E System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass14_0`1::<CreateStaticPropertySetter>b__0(PropType)
// 0x0000061F System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass15_0`1::.ctor()
// 0x00000620 PropType Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass15_0`1::<CreateStaticPropertyGetter>b__0()
// 0x00000621 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass16_0`2::.ctor()
// 0x00000622 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass16_0`2::<CreateInstancePropertySetter>b__0(InstanceType&,PropType)
// 0x00000623 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass17_0`2::.ctor()
// 0x00000624 PropType Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass17_0`2::<CreateInstancePropertyGetter>b__0(InstanceType&)
// 0x00000625 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass20_0`1::.ctor()
// 0x00000626 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass20_0`1::<CreateWeakInstanceMethodCaller>b__0(System.Object,TArg1)
// 0x00000627 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mB7C029ADE1E2EF3692B45C131209DC117B967E9C (void);
// 0x00000628 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass21_0::<CreateWeakInstanceMethodCaller>b__0(System.Object)
extern void U3CU3Ec__DisplayClass21_0_U3CCreateWeakInstanceMethodCallerU3Eb__0_mBBF2E5E245115CB827C5CCB1A37FB04F78D908ED (void);
// 0x00000629 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass22_0`2::.ctor()
// 0x0000062A TResult Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass22_0`2::<CreateWeakInstanceMethodCaller>b__0(System.Object,TArg1)
// 0x0000062B System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass23_0`1::.ctor()
// 0x0000062C TResult Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass23_0`1::<CreateWeakInstanceMethodCallerFunc>b__0(System.Object)
// 0x0000062D System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass24_0`2::.ctor()
// 0x0000062E TResult Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass24_0`2::<CreateWeakInstanceMethodCallerFunc>b__0(System.Object,TArg)
// 0x0000062F System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass29_0`1::.ctor()
// 0x00000630 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass29_0`1::<CreateInstanceRefMethodCaller>b__0(InstanceType&)
// 0x00000631 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass30_0`2::.ctor()
// 0x00000632 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass30_0`2::<CreateInstanceRefMethodCaller>b__0(InstanceType&,Arg1)
// 0x00000633 System.Boolean Sirenix.Serialization.Utilities.FastTypeComparer::Equals(System.Type,System.Type)
extern void FastTypeComparer_Equals_m903ED7253E35BA0FB495EAB1FC06A1E708E82F81 (void);
// 0x00000634 System.Int32 Sirenix.Serialization.Utilities.FastTypeComparer::GetHashCode(System.Type)
extern void FastTypeComparer_GetHashCode_m540435210864F6134EB0F4B185727F715BC51ED3 (void);
// 0x00000635 System.Void Sirenix.Serialization.Utilities.FastTypeComparer::.ctor()
extern void FastTypeComparer__ctor_m34563B95C215274B6DEBC65F031A380611FB0FC3 (void);
// 0x00000636 System.Void Sirenix.Serialization.Utilities.FastTypeComparer::.cctor()
extern void FastTypeComparer__cctor_m86C79E0B8C3D40DF34CA193099A07BE44C538C75 (void);
// 0x00000637 System.Void Sirenix.Serialization.Utilities.ICacheNotificationReceiver::OnFreed()
// 0x00000638 System.Void Sirenix.Serialization.Utilities.ICacheNotificationReceiver::OnClaimed()
// 0x00000639 T Sirenix.Serialization.Utilities.IImmutableList`1::get_Item(System.Int32)
// 0x0000063A System.Void Sirenix.Serialization.Utilities.ImmutableList::.ctor(System.Collections.IList)
extern void ImmutableList__ctor_mD743802510BCE9DDDE33FD9F8FC5AD2746514197 (void);
// 0x0000063B System.Int32 Sirenix.Serialization.Utilities.ImmutableList::get_Count()
extern void ImmutableList_get_Count_m628D07411950AAEE6E6D5583F1EE551B5F1D17BB (void);
// 0x0000063C System.Boolean Sirenix.Serialization.Utilities.ImmutableList::get_IsFixedSize()
extern void ImmutableList_get_IsFixedSize_m9F61791966C93B3C65544EC34A4CBB2EE44914F9 (void);
// 0x0000063D System.Boolean Sirenix.Serialization.Utilities.ImmutableList::get_IsReadOnly()
extern void ImmutableList_get_IsReadOnly_m20BBA1E91AEAE1A802191F5619E03E82226AF3AD (void);
// 0x0000063E System.Boolean Sirenix.Serialization.Utilities.ImmutableList::get_IsSynchronized()
extern void ImmutableList_get_IsSynchronized_m834E1D21975A74A181CBE3B1A5E72EC5BEF3ACD3 (void);
// 0x0000063F System.Object Sirenix.Serialization.Utilities.ImmutableList::get_SyncRoot()
extern void ImmutableList_get_SyncRoot_m0AD546D5E0EC7F0EE3F5EB6584F7C4FB775136F2 (void);
// 0x00000640 System.Object Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.get_Item(System.Int32)
extern void ImmutableList_System_Collections_IList_get_Item_m6B45FB5E56FB5EE52A5C000CA84C29BEF0E3306C (void);
// 0x00000641 System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.set_Item(System.Int32,System.Object)
extern void ImmutableList_System_Collections_IList_set_Item_m76A42F04BB486468E6361AB80AF376608D742B54 (void);
// 0x00000642 System.Object Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.get_Item(System.Int32)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_get_Item_m19D0F17A2C441DB8BBF88CC2F1B2BBD8802A3855 (void);
// 0x00000643 System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.set_Item(System.Int32,System.Object)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_set_Item_m8765375F85ED7EB140EC684FEF2416FAD9FBFE32 (void);
// 0x00000644 System.Object Sirenix.Serialization.Utilities.ImmutableList::get_Item(System.Int32)
extern void ImmutableList_get_Item_mDCF098FC89391B8B732397CD520C78E616A034F4 (void);
// 0x00000645 System.Boolean Sirenix.Serialization.Utilities.ImmutableList::Contains(System.Object)
extern void ImmutableList_Contains_mDA1B5D35309466CAD069A21E33C318AD02ACA95C (void);
// 0x00000646 System.Void Sirenix.Serialization.Utilities.ImmutableList::CopyTo(System.Object[],System.Int32)
extern void ImmutableList_CopyTo_mC50AE4852E45353290EA2A81AF93A50CAE24E449 (void);
// 0x00000647 System.Void Sirenix.Serialization.Utilities.ImmutableList::CopyTo(System.Array,System.Int32)
extern void ImmutableList_CopyTo_m4CA02FE0FB99548A8A0F08A6B9436D67E6E2E951 (void);
// 0x00000648 System.Collections.IEnumerator Sirenix.Serialization.Utilities.ImmutableList::GetEnumerator()
extern void ImmutableList_GetEnumerator_mD3E794DF87616577C86A240500E6B132FA4D1D7E (void);
// 0x00000649 System.Collections.IEnumerator Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IEnumerable.GetEnumerator()
extern void ImmutableList_System_Collections_IEnumerable_GetEnumerator_m662DB29FC593218F94C1EAA90ACDCF032528CA51 (void);
// 0x0000064A System.Collections.Generic.IEnumerator`1<System.Object> Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
extern void ImmutableList_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m0EFC67A88649A75A6B2D870D05E47701D84FC7AB (void);
// 0x0000064B System.Int32 Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.Add(System.Object)
extern void ImmutableList_System_Collections_IList_Add_m8D03593974E2E3A79983D80963CE14E8F3AE2825 (void);
// 0x0000064C System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.Clear()
extern void ImmutableList_System_Collections_IList_Clear_m5BB8383C7EC2592839449E9D6CE301590870EE8A (void);
// 0x0000064D System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.Insert(System.Int32,System.Object)
extern void ImmutableList_System_Collections_IList_Insert_mE4A6045B55566525088F5982712C4F3B64B1C6F0 (void);
// 0x0000064E System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.Remove(System.Object)
extern void ImmutableList_System_Collections_IList_Remove_mCC6A1A58626571F745A1147F94270CF447F58472 (void);
// 0x0000064F System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.RemoveAt(System.Int32)
extern void ImmutableList_System_Collections_IList_RemoveAt_mBEED1926DB081297577919E4F444239DDCC70451 (void);
// 0x00000650 System.Int32 Sirenix.Serialization.Utilities.ImmutableList::IndexOf(System.Object)
extern void ImmutableList_IndexOf_m2562AC49A314908BFEBC64867E939C3A2A2FAAF4 (void);
// 0x00000651 System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.RemoveAt(System.Int32)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_RemoveAt_m9E092CF694E15DF8265003C47B0617D447D1F270 (void);
// 0x00000652 System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.Insert(System.Int32,System.Object)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_Insert_mDC057078D75874CC9282B3D5FFD2243D5D81D6A8 (void);
// 0x00000653 System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.ICollection<System.Object>.Add(System.Object)
extern void ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Add_m86DA80C1CC4E832E24EACA5BBFB0CB8C60762AC2 (void);
// 0x00000654 System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.ICollection<System.Object>.Clear()
extern void ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Clear_m5E4C0AF7DFC93FD954E0552F115D05CE8BA92F69 (void);
// 0x00000655 System.Boolean Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.ICollection<System.Object>.Remove(System.Object)
extern void ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Remove_mDCC9028D918A6E9B1A58F29E920BC650A7D2E896 (void);
// 0x00000656 System.Void Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::.ctor(System.Int32)
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_m7A4AAD7637598AD3A1B243FA716BBA0F9D833000 (void);
// 0x00000657 System.Void Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.IDisposable.Dispose()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m758A80F54C1DC156B36BE7EE8CB2292F355D992B (void);
// 0x00000658 System.Boolean Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::MoveNext()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_MoveNext_mDF6DFD9E579394357A93A8AD15A4008F2E93F812 (void);
// 0x00000659 System.Void Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>m__Finally1()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_m43FDB85F5EE6720959BBB6800664B76C7C6214D2 (void);
// 0x0000065A System.Object Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9DDA4BE1D2C4B4FC45E2C282DEE63BF2CF2C726B (void);
// 0x0000065B System.Void Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.Collections.IEnumerator.Reset()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m9704C6223E770A6C552F271FCAB0B4DC727EAEA7 (void);
// 0x0000065C System.Object Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_mBBF0434A75B43F6DA549B64020BBEE78875DAF82 (void);
// 0x0000065D System.Void Sirenix.Serialization.Utilities.ImmutableList`1::.ctor(System.Collections.Generic.IList`1<T>)
// 0x0000065E System.Int32 Sirenix.Serialization.Utilities.ImmutableList`1::get_Count()
// 0x0000065F System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.ICollection.get_IsSynchronized()
// 0x00000660 System.Object Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.ICollection.get_SyncRoot()
// 0x00000661 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.get_IsFixedSize()
// 0x00000662 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.get_IsReadOnly()
// 0x00000663 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::get_IsReadOnly()
// 0x00000664 System.Object Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.get_Item(System.Int32)
// 0x00000665 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.set_Item(System.Int32,System.Object)
// 0x00000666 T Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.get_Item(System.Int32)
// 0x00000667 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
// 0x00000668 T Sirenix.Serialization.Utilities.ImmutableList`1::get_Item(System.Int32)
// 0x00000669 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::Contains(T)
// 0x0000066A System.Void Sirenix.Serialization.Utilities.ImmutableList`1::CopyTo(T[],System.Int32)
// 0x0000066B System.Collections.Generic.IEnumerator`1<T> Sirenix.Serialization.Utilities.ImmutableList`1::GetEnumerator()
// 0x0000066C System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x0000066D System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000066E System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.ICollection<T>.Clear()
// 0x0000066F System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.ICollection<T>.Remove(T)
// 0x00000670 System.Collections.IEnumerator Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000671 System.Int32 Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.Add(System.Object)
// 0x00000672 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.Clear()
// 0x00000673 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.Contains(System.Object)
// 0x00000674 System.Int32 Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.IndexOf(System.Object)
// 0x00000675 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.Insert(System.Int32,System.Object)
// 0x00000676 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.Remove(System.Object)
// 0x00000677 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
// 0x00000678 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.RemoveAt(System.Int32)
// 0x00000679 System.Int32 Sirenix.Serialization.Utilities.ImmutableList`1::IndexOf(T)
// 0x0000067A System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
// 0x0000067B System.Void Sirenix.Serialization.Utilities.ImmutableList`2::.ctor(TList)
// 0x0000067C System.Int32 Sirenix.Serialization.Utilities.ImmutableList`2::get_Count()
// 0x0000067D System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.ICollection.get_IsSynchronized()
// 0x0000067E System.Object Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.ICollection.get_SyncRoot()
// 0x0000067F System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.get_IsFixedSize()
// 0x00000680 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.get_IsReadOnly()
// 0x00000681 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::get_IsReadOnly()
// 0x00000682 System.Object Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.get_Item(System.Int32)
// 0x00000683 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.set_Item(System.Int32,System.Object)
// 0x00000684 TElement Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x00000685 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x00000686 TElement Sirenix.Serialization.Utilities.ImmutableList`2::get_Item(System.Int32)
// 0x00000687 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::Contains(TElement)
// 0x00000688 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::CopyTo(TElement[],System.Int32)
// 0x00000689 System.Collections.Generic.IEnumerator`1<TElement> Sirenix.Serialization.Utilities.ImmutableList`2::GetEnumerator()
// 0x0000068A System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x0000068B System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x0000068C System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x0000068D System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x0000068E System.Collections.IEnumerator Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IEnumerable.GetEnumerator()
// 0x0000068F System.Int32 Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.Add(System.Object)
// 0x00000690 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.Clear()
// 0x00000691 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.Contains(System.Object)
// 0x00000692 System.Int32 Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.IndexOf(System.Object)
// 0x00000693 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.Insert(System.Int32,System.Object)
// 0x00000694 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.Remove(System.Object)
// 0x00000695 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x00000696 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.RemoveAt(System.Int32)
// 0x00000697 System.Int32 Sirenix.Serialization.Utilities.ImmutableList`2::IndexOf(TElement)
// 0x00000698 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x00000699 System.Void Sirenix.Serialization.Utilities.MemberAliasFieldInfo::.ctor(System.Reflection.FieldInfo,System.String)
extern void MemberAliasFieldInfo__ctor_m6291D72D7647EED0ADEBDF495DC8FCF10F5149CE (void);
// 0x0000069A System.Void Sirenix.Serialization.Utilities.MemberAliasFieldInfo::.ctor(System.Reflection.FieldInfo,System.String,System.String)
extern void MemberAliasFieldInfo__ctor_m2E88616801E0CFDB50FAE4671D02C9EE3F8ADF84 (void);
// 0x0000069B System.Reflection.FieldInfo Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_AliasedField()
extern void MemberAliasFieldInfo_get_AliasedField_mA4772D6489C7ACEE68E37BDB67352ECEC64A79B3 (void);
// 0x0000069C System.Reflection.Module Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_Module()
extern void MemberAliasFieldInfo_get_Module_mF5355FCEDAC45D4C859E4DE24FA7D61CC861195F (void);
// 0x0000069D System.Int32 Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_MetadataToken()
extern void MemberAliasFieldInfo_get_MetadataToken_m4000D9C28F98FA1742DA0A83935FDF80957EEEAC (void);
// 0x0000069E System.String Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_Name()
extern void MemberAliasFieldInfo_get_Name_m856D029505EAFB628BC3D587E27356DAF0CA6EC5 (void);
// 0x0000069F System.Type Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_DeclaringType()
extern void MemberAliasFieldInfo_get_DeclaringType_m47AC48A006C6701D4887970E1B6C1A5F079CB9C7 (void);
// 0x000006A0 System.Type Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_ReflectedType()
extern void MemberAliasFieldInfo_get_ReflectedType_m26A376E35082CB80B6538D5C8E1B0A3CD2485B9B (void);
// 0x000006A1 System.Type Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_FieldType()
extern void MemberAliasFieldInfo_get_FieldType_m34F3717578C06FFCD1C3C52F344C70CCB0F0C2A4 (void);
// 0x000006A2 System.RuntimeFieldHandle Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_FieldHandle()
extern void MemberAliasFieldInfo_get_FieldHandle_mACCABC3661C6B1D7F28ABA5E1B654F456F19250B (void);
// 0x000006A3 System.Reflection.FieldAttributes Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_Attributes()
extern void MemberAliasFieldInfo_get_Attributes_mFCE2983AFC616969D9C83858876AEEEBEE113B04 (void);
// 0x000006A4 System.Object[] Sirenix.Serialization.Utilities.MemberAliasFieldInfo::GetCustomAttributes(System.Boolean)
extern void MemberAliasFieldInfo_GetCustomAttributes_mDD8E2D433080D2951DB69C3C138E2D79B137DB09 (void);
// 0x000006A5 System.Object[] Sirenix.Serialization.Utilities.MemberAliasFieldInfo::GetCustomAttributes(System.Type,System.Boolean)
extern void MemberAliasFieldInfo_GetCustomAttributes_m74D7995F8C8F7B3616FE65CE7C7F54E275DC8CF5 (void);
// 0x000006A6 System.Boolean Sirenix.Serialization.Utilities.MemberAliasFieldInfo::IsDefined(System.Type,System.Boolean)
extern void MemberAliasFieldInfo_IsDefined_m34BAC252FACDC00E201648E635806AEB4B44790E (void);
// 0x000006A7 System.Object Sirenix.Serialization.Utilities.MemberAliasFieldInfo::GetValue(System.Object)
extern void MemberAliasFieldInfo_GetValue_mB59CD2CAAD3B4C951D76C69278B2330DDDF74562 (void);
// 0x000006A8 System.Void Sirenix.Serialization.Utilities.MemberAliasFieldInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo)
extern void MemberAliasFieldInfo_SetValue_m9A7B6B9766A8DB33EE9A0BD1D22191692E7546DF (void);
// 0x000006A9 System.Void Sirenix.Serialization.Utilities.MemberAliasMethodInfo::.ctor(System.Reflection.MethodInfo,System.String)
extern void MemberAliasMethodInfo__ctor_m8ED4DD15C3E7636AE81D47F50D5DDC0BE2813510 (void);
// 0x000006AA System.Void Sirenix.Serialization.Utilities.MemberAliasMethodInfo::.ctor(System.Reflection.MethodInfo,System.String,System.String)
extern void MemberAliasMethodInfo__ctor_m48690BF27EFD4B4114402B79E66168744AE72811 (void);
// 0x000006AB System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_AliasedMethod()
extern void MemberAliasMethodInfo_get_AliasedMethod_m61AC69C4DE8D031731A6D18269488E4DD4A91282 (void);
// 0x000006AC System.Reflection.ICustomAttributeProvider Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_ReturnTypeCustomAttributes()
extern void MemberAliasMethodInfo_get_ReturnTypeCustomAttributes_m2C5420F7CE569F879A46867378C9E804685A9A75 (void);
// 0x000006AD System.RuntimeMethodHandle Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_MethodHandle()
extern void MemberAliasMethodInfo_get_MethodHandle_m68D2C7B066906FE212DF42ABC676AA5CA93D527F (void);
// 0x000006AE System.Reflection.MethodAttributes Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_Attributes()
extern void MemberAliasMethodInfo_get_Attributes_mE1A827BB0615C29D87ABF61CA56E1AC4337C05D9 (void);
// 0x000006AF System.Type Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_ReturnType()
extern void MemberAliasMethodInfo_get_ReturnType_mF824DA1143979223DCD70FCF0C2A4D0330359CCE (void);
// 0x000006B0 System.Type Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_DeclaringType()
extern void MemberAliasMethodInfo_get_DeclaringType_m314935605E5A3F319B914B5153CDBC0215F1DFAA (void);
// 0x000006B1 System.String Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_Name()
extern void MemberAliasMethodInfo_get_Name_mB6C139E754EA64D21C15F8022988B87ADC633260 (void);
// 0x000006B2 System.Type Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_ReflectedType()
extern void MemberAliasMethodInfo_get_ReflectedType_m7AFD00991526666FA4817FB2AC658399F85A72FB (void);
// 0x000006B3 System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetBaseDefinition()
extern void MemberAliasMethodInfo_GetBaseDefinition_m24BC92DF9CEF1FC90DD504B5666D6632DA65EC44 (void);
// 0x000006B4 System.Object[] Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetCustomAttributes(System.Boolean)
extern void MemberAliasMethodInfo_GetCustomAttributes_m392499E8466489F2BFBFD712F18E1F0023C49F0B (void);
// 0x000006B5 System.Object[] Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetCustomAttributes(System.Type,System.Boolean)
extern void MemberAliasMethodInfo_GetCustomAttributes_m07F3B84B3820495AF8C819CFE3A091994850B641 (void);
// 0x000006B6 System.Reflection.MethodImplAttributes Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetMethodImplementationFlags()
extern void MemberAliasMethodInfo_GetMethodImplementationFlags_mDD12F1FB5B23852E38494BDDF606C24DA9B34AC3 (void);
// 0x000006B7 System.Reflection.ParameterInfo[] Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetParameters()
extern void MemberAliasMethodInfo_GetParameters_m247B3302C93AE5332F015F7967DFE16ADC3B3829 (void);
// 0x000006B8 System.Object Sirenix.Serialization.Utilities.MemberAliasMethodInfo::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern void MemberAliasMethodInfo_Invoke_mB09584B2C8F0CF2DF0C26352A5F591EDE2376FC9 (void);
// 0x000006B9 System.Boolean Sirenix.Serialization.Utilities.MemberAliasMethodInfo::IsDefined(System.Type,System.Boolean)
extern void MemberAliasMethodInfo_IsDefined_m003D74B56BFBD7BB654794CE4C8CCA12FB427FDB (void);
// 0x000006BA System.Void Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::.ctor(System.Reflection.PropertyInfo,System.String)
extern void MemberAliasPropertyInfo__ctor_m31F7966B854F8C787A87DF11EB3B88E74ED01E50 (void);
// 0x000006BB System.Void Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::.ctor(System.Reflection.PropertyInfo,System.String,System.String)
extern void MemberAliasPropertyInfo__ctor_mE869B7B6F5441B1CD67E25DBEFDFCD91EF9360C9 (void);
// 0x000006BC System.Reflection.PropertyInfo Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_AliasedProperty()
extern void MemberAliasPropertyInfo_get_AliasedProperty_mA16048158E811314BCE84BE878B0881E9335DE2C (void);
// 0x000006BD System.Reflection.Module Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_Module()
extern void MemberAliasPropertyInfo_get_Module_m65699C23616E927727983B3C654CDEBF21EE57DF (void);
// 0x000006BE System.Int32 Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_MetadataToken()
extern void MemberAliasPropertyInfo_get_MetadataToken_mF28C1911F14B940F2FE9EDDE54FCBBDE612EDD83 (void);
// 0x000006BF System.String Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_Name()
extern void MemberAliasPropertyInfo_get_Name_m7AA295B5671299B0B86E60A33DF856E1487A152F (void);
// 0x000006C0 System.Type Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_DeclaringType()
extern void MemberAliasPropertyInfo_get_DeclaringType_m9A438253C638140709BC19EA27DF0DDC45687E1A (void);
// 0x000006C1 System.Type Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_ReflectedType()
extern void MemberAliasPropertyInfo_get_ReflectedType_m29D9DCA59AA68782D8BD2558A4042FF44406DFC8 (void);
// 0x000006C2 System.Type Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_PropertyType()
extern void MemberAliasPropertyInfo_get_PropertyType_mAA4FDB53F44E1A4E6E7BC4092CF04249F50AFDDB (void);
// 0x000006C3 System.Reflection.PropertyAttributes Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_Attributes()
extern void MemberAliasPropertyInfo_get_Attributes_m7C8C0BC96BBF67562C446ADF141B9D99B46B8375 (void);
// 0x000006C4 System.Boolean Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_CanRead()
extern void MemberAliasPropertyInfo_get_CanRead_mBC85D29928740B528F42C19654571BFCBBD391C8 (void);
// 0x000006C5 System.Boolean Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_CanWrite()
extern void MemberAliasPropertyInfo_get_CanWrite_mA892B8CE11A2D5CC56DE94BBE70F08F52947B5D0 (void);
// 0x000006C6 System.Object[] Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetCustomAttributes(System.Boolean)
extern void MemberAliasPropertyInfo_GetCustomAttributes_m131D5FEA7052D5E2B1C3F2757D9372A5E5AA4335 (void);
// 0x000006C7 System.Object[] Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetCustomAttributes(System.Type,System.Boolean)
extern void MemberAliasPropertyInfo_GetCustomAttributes_m9409895E831141A080800D9297680F930CD0A138 (void);
// 0x000006C8 System.Boolean Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::IsDefined(System.Type,System.Boolean)
extern void MemberAliasPropertyInfo_IsDefined_mCA5B764FF246A0A3F7BBD76CB7B232E61898534A (void);
// 0x000006C9 System.Reflection.MethodInfo[] Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetAccessors(System.Boolean)
extern void MemberAliasPropertyInfo_GetAccessors_mA4361B6D8EB19E3F46DE8C7D15F8321CA72D3716 (void);
// 0x000006CA System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetGetMethod(System.Boolean)
extern void MemberAliasPropertyInfo_GetGetMethod_m904015C20FE5E8597DF0E9AC42DC8448E0E1FE0F (void);
// 0x000006CB System.Reflection.ParameterInfo[] Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetIndexParameters()
extern void MemberAliasPropertyInfo_GetIndexParameters_m1084B3A296CFAEE36FAC8E185C82F75641483AFC (void);
// 0x000006CC System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetSetMethod(System.Boolean)
extern void MemberAliasPropertyInfo_GetSetMethod_mA3617D52753CD0CF3487E209575161CF776D7AC1 (void);
// 0x000006CD System.Object Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern void MemberAliasPropertyInfo_GetValue_m713D28D16C23B68024157B7E78662462AC8C3037 (void);
// 0x000006CE System.Void Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern void MemberAliasPropertyInfo_SetValue_m7B44101A30899165EB67CEDD485765DE58B1ACEB (void);
// 0x000006CF System.Boolean Sirenix.Serialization.Utilities.ReferenceEqualityComparer`1::Equals(T,T)
// 0x000006D0 System.Int32 Sirenix.Serialization.Utilities.ReferenceEqualityComparer`1::GetHashCode(T)
// 0x000006D1 System.Void Sirenix.Serialization.Utilities.ReferenceEqualityComparer`1::.ctor()
// 0x000006D2 System.Void Sirenix.Serialization.Utilities.ReferenceEqualityComparer`1::.cctor()
// 0x000006D3 System.Void Sirenix.Serialization.Utilities.UnityVersion::.cctor()
extern void UnityVersion__cctor_mE278A791E550A2727E733D3EC80E28214004876D (void);
// 0x000006D4 System.Void Sirenix.Serialization.Utilities.UnityVersion::EnsureLoaded()
extern void UnityVersion_EnsureLoaded_mB8D7004347114DB5DFD4089FAE669C5522592C0F (void);
// 0x000006D5 System.Boolean Sirenix.Serialization.Utilities.UnityVersion::IsVersionOrGreater(System.Int32,System.Int32)
extern void UnityVersion_IsVersionOrGreater_mB00A922D8EFDD10E6ECE391C04C89B697F6A2284 (void);
// 0x000006D6 T[] Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StructArrayFromBytes(System.Byte[],System.Int32)
// 0x000006D7 T[] Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StructArrayFromBytes(System.Byte[],System.Int32,System.Int32)
// 0x000006D8 System.Byte[] Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StructArrayToBytes(T[])
// 0x000006D9 System.Byte[] Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StructArrayToBytes(T[],System.Byte[]&,System.Int32)
// 0x000006DA System.String Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StringFromBytes(System.Byte[],System.Int32,System.Boolean)
extern void UnsafeUtilities_StringFromBytes_m913419982B445F5A2683517541E34030A1361CD3 (void);
// 0x000006DB System.Int32 Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StringToBytes(System.Byte[],System.String,System.Boolean)
extern void UnsafeUtilities_StringToBytes_m21C9542DB3983BAE84ECD914B20738E95DD964D4 (void);
// 0x000006DC System.Void Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::MemoryCopy(System.Void*,System.Void*,System.Int32)
extern void UnsafeUtilities_MemoryCopy_m5694365F803140B4CB897EAE26789568396CB6A4 (void);
// 0x000006DD System.Void Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::MemoryCopy(System.Object,System.Object,System.Int32,System.Int32,System.Int32)
extern void UnsafeUtilities_MemoryCopy_m4ECC505D44D7510CA01DE22DF3065126E2CC1FE3 (void);
// 0x000006DE System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m272C79CFF409153DFC504A695283A06E8F16FDC8 (void);
static Il2CppMethodPointer s_methodPointers[1758] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SerializedBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_get_SerializationData_m1D7244104C84437AB66D01345D705AE1B96511D7,
	SerializedBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_set_SerializationData_m7A7A175ABF183A68762981C05023875D1D16BBA3,
	SerializedBehaviour_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m231CBCF7B99F8737D0CE669C49C665B6ECCFCB1B,
	SerializedBehaviour_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mEA27F0E4ED5E2BA44206EFF47C83C64F1CC66D95,
	SerializedBehaviour_OnAfterDeserialize_mDDFBF15521127B1605100D6426A426965EA53853,
	SerializedBehaviour_OnBeforeSerialize_mA3E56786A77E2CA5B0B3C8E4C922D38047DE2737,
	SerializedBehaviour__ctor_m7369A41C49C4DE613EC444D2C0C83ACC40718FBA,
	SerializedComponent_Sirenix_Serialization_ISupportsPrefabSerialization_get_SerializationData_m9A415AF7EF2DF7BC3705A6561F62CEB25EECB2E2,
	SerializedComponent_Sirenix_Serialization_ISupportsPrefabSerialization_set_SerializationData_m6EE6F9548064E29392CF5B973C7B1D4A63EC2F4F,
	SerializedComponent_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m8E56BC26148CC8C8CB49EE4ED85587C9F8611C27,
	SerializedComponent_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m4A44AEED4D9068A7AD8BBD6499B936A48A3344BA,
	SerializedComponent_OnAfterDeserialize_m898646C63DB9713C695AD0ACAB54D22EFBF257AC,
	SerializedComponent_OnBeforeSerialize_m3F270DE1F0C50E929A18365248AD13A9F9E4720F,
	SerializedComponent__ctor_mE9C85C50ADC8523FB1628A7BD2197C7BBAC33E89,
	SerializedMonoBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_get_SerializationData_m2CE517A444D2A5E343188B81887401EB1712DFEC,
	SerializedMonoBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_set_SerializationData_m64849DC2B9FBD4A1A60334A5E1868455E3692DB4,
	SerializedMonoBehaviour_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mDB88DE9FB9302908036197B837842C66C484174B,
	SerializedMonoBehaviour_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m66535E272745D58C02FFC7DAE3013F5FCC48B49E,
	SerializedMonoBehaviour_OnAfterDeserialize_mEF706AB166EBC3022CF1C5D02E79DE385B6421D3,
	SerializedMonoBehaviour_OnBeforeSerialize_mA43CF7F184927A667A3C7EAAD33F6353407F12AB,
	SerializedMonoBehaviour__ctor_m9966C7AAE31D79BB430868A86F4DD29AC3F30908,
	SerializedScriptableObject_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3F1BBB06B5021D852E4BE29A4DC6D146CA9E4E23,
	SerializedScriptableObject_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m162364613764E9E11D0AEE9A6E60D66F27DE3674,
	SerializedScriptableObject_OnAfterDeserialize_m6A86D3F278A2AC4EDEA8F6990A4D3F6F5DC13157,
	SerializedScriptableObject_OnBeforeSerialize_mDD54A16365E18651AD2F0A82FB972731CE354662,
	SerializedScriptableObject__ctor_mF905DC04A8D07C7B5B56497A35B45A2EED638734,
	SerializedStateMachineBehaviour_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3EDE134FF5B19BBB256109303BE96D1E974CF8E8,
	SerializedStateMachineBehaviour_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m855E0B7644915E350746D14D2D8759CD79785128,
	SerializedStateMachineBehaviour_OnAfterDeserialize_m3CC0D86B70A2D0538C86A13077EDF652103E05CF,
	SerializedStateMachineBehaviour_OnBeforeSerialize_mD20BBD8926B24CDFC5796FFC4C18C4C70DDB780B,
	SerializedStateMachineBehaviour__ctor_m8336D23805186A2E36683EE65F4DF8C814A5C5B1,
	SerializedUnityObject_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m660E2FA4FF687A60EBD0298AF737976BB4D5E857,
	SerializedUnityObject_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m63D7A3F8D675B353EC9A14768391A72A4C2BB8F5,
	SerializedUnityObject_OnAfterDeserialize_m8BEF8012872F172C655E29087159B21656206F10,
	SerializedUnityObject_OnBeforeSerialize_mB204CD9DECF9865DAEE8BCDA5A69CA10692D2B24,
	SerializedUnityObject__ctor_mF4574AA82B4DC0A43CE83FCEFA7C236C7F9584E2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	VersionFormatter_GetUninitializedObject_m81695DF17E9575E8B0764FDC8709CE26ABC14775,
	VersionFormatter_Read_m301C155F9E9DF8DFE8B0104F22FAAAF1BD41D76D,
	VersionFormatter_Write_mDA6955EEA60F8BAF1FCB686A396C9C9669DCE3E1,
	VersionFormatter__ctor_mC04B97DEAB78B25304642251FD42ECD4D925344E,
	AllowDeserializeInvalidDataAttribute__ctor_mE97361871FB9A3D929C3453AFF38435CEB754499,
	ArchitectureInfo__cctor_mC07B991E1DC115F3CB0C8529053556FEBB9DAF99,
	ArchitectureInfo_SetRuntimePlatform_mF3058044930F2BF44BD2AFE959B2626A4E53A30E,
	NULL,
	NULL,
	ArrayFormatterLocator_TryGetFormatter_mA0968E64554BF2F7A081920FC4C17E08D799D168,
	ArrayFormatterLocator__ctor_mA9446684941516250931B101B8F0F44C6EDA770A,
	DelegateFormatterLocator_TryGetFormatter_m725610512E6F6C11BA0653AE5152047AA5BC2A8E,
	DelegateFormatterLocator__ctor_mEB23F4E90F9274E1E15278B437ACA5D2AA6AFCCC,
	NULL,
	NULL,
	GenericCollectionFormatterLocator_TryGetFormatter_m3BCA951CF7CABE22A3567619600E60722E6A0601,
	GenericCollectionFormatterLocator__ctor_mBEC7A66ACBC662630C23CC9534F6AED56EBB9649,
	ISerializableFormatterLocator_TryGetFormatter_m2F812924A81DA804DEEB5304139443356D1B45D8,
	ISerializableFormatterLocator__ctor_mE84E24B0D52003D3B4AEDFD5C87388D8243B7256,
	RegisterFormatterAttribute_get_FormatterType_mF956959C4A65FFD052162F474414F26947CB434D,
	RegisterFormatterAttribute_set_FormatterType_mA35337AE66D36BD75BDC4637934CB9485BD7AAC5,
	RegisterFormatterAttribute_get_Priority_m6C50BACE64D393E48F14EFFD26B0154FD768CDC0,
	RegisterFormatterAttribute_set_Priority_m12DBB04E52AEED65CB20ACAA6B8030B8F321DA1E,
	RegisterFormatterAttribute__ctor_mB1B819795911CCB0A8AD1A23B9DF3E939A5EAE2D,
	RegisterFormatterLocatorAttribute_get_FormatterLocatorType_m7FE9AF51A8F373582193FD38F6434BD4A760BB43,
	RegisterFormatterLocatorAttribute_set_FormatterLocatorType_m0923C1539EFD4A00CBA3A41E744488986B474448,
	RegisterFormatterLocatorAttribute_get_Priority_m400AA137158D24DE437E972EF73FA030FA127985,
	RegisterFormatterLocatorAttribute_set_Priority_mBF03765D5F697C9ADB3EE0F854FD861CC59CA581,
	RegisterFormatterLocatorAttribute__ctor_m19C6CED811535C351A71A1FEDA17935DF6CE34BC,
	SelfFormatterLocator_TryGetFormatter_mA6508574A4FBE8184FAE951FA52D6EB62A3CBDA8,
	SelfFormatterLocator__ctor_m9D1547C4ECFA91AEAFFB418B149D9A52CBFFA2E9,
	TypeFormatterLocator_TryGetFormatter_m74916A084834502DB5BC344378DEA2BC5837A1A3,
	TypeFormatterLocator__ctor_m5196C596A4EB5C3188B3021FFF1F64895EF8A6E9,
	BaseDataReader__ctor_m19AEF27F1CD0371D17D2E2F80C5DC4C0272CF81F,
	BaseDataReader_get_CurrentNodeId_mD1627D85AFE075AB440B9F5BCE33E751381C49AC,
	BaseDataReader_get_CurrentNodeDepth_mFA481A654CBE2756053B330CC2E01443C2A57F4C,
	BaseDataReader_get_CurrentNodeName_m4EF2D0E8107CD7D785A5CE10E13EE5FAC760CC91,
	BaseDataReader_get_Stream_mD8FEF305D07F4121682AD07A32E534492C99898B,
	BaseDataReader_set_Stream_m84189ADBAC883C106E2868A9CEC9537FE6D8C4F7,
	BaseDataReader_get_Context_m812E2B3857D46B577468CA7BCF7B4D7634A52A3E,
	BaseDataReader_set_Context_mB5E0A8B454B9FBDCD10BFBC6EE45EC9AC88F3C8C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseDataReader_SkipEntry_m0F94BF50A3A9AB4B983B8A588016B7623AEC227E,
	NULL,
	BaseDataReader_PrepareNewSerializationSession_mCD9BDE456C3CCA7D40942A8EFFE4B47AF6A1FB87,
	NULL,
	NULL,
	NULL,
	BaseDataReaderWriter_get_Binder_m34107DEFF80EAE2073B216E1FA0CFE8E1D84DCB5,
	BaseDataReaderWriter_set_Binder_mEEF34D7EF09034E3FC945981387820C716947433,
	BaseDataReaderWriter_get_IsInArrayNode_m0058B73D7DDE539FC7C6C73FF03889B083B75E5E,
	BaseDataReaderWriter_get_NodeDepth_m207767DCCE22E2C09B0F28CCB06DD618378C3922,
	BaseDataReaderWriter_get_CurrentNode_mC8ECBA7738A96FF8C391ED8016AF0C0D23FCA61C,
	BaseDataReaderWriter_PushNode_mCB39A9AE0E0D73AA9DA3E07E77C20B06D28A469D,
	BaseDataReaderWriter_PushNode_m5CBF3EE5F829E1B3EDAE8734758B4C7E075AC656,
	BaseDataReaderWriter_PushArray_mD5838EF19AB9FBE69622603994433A886D74BB96,
	BaseDataReaderWriter_ExpandNodes_m0869773D575D9C2C4063674CECB7B5F1664CADCC,
	BaseDataReaderWriter_PopNode_m696F0C285F9EB6F2EDFC8172D6119C1E00B6AE09,
	BaseDataReaderWriter_PopArray_mCBDF73852451F1891D697A047F1024BDFCED877B,
	BaseDataReaderWriter_ClearNodes_m1BFA05AC36057AF4DF2C37EDCCCDE6AD891112D6,
	BaseDataReaderWriter__ctor_m0399C0F2609B9F0818966E7E61DAE72CFD2351C0,
	BaseDataWriter__ctor_mF1F5BE8D818E59FB2A4E5CEEEF68E06F9400295D,
	BaseDataWriter_get_Stream_m06D61522D6ACB8E1F16F85A3327BE76938A40C2E,
	BaseDataWriter_set_Stream_mF993EB71758532CF62A06C7A56F92D54C13543D3,
	BaseDataWriter_get_Context_mA8C8E3F53809E142F7F457B50EFF332018F896A9,
	BaseDataWriter_set_Context_mE4830D3043D4540063AB0F4F1CDCCB9805220FE6,
	BaseDataWriter_FlushToStream_m7F0C1632A809E7AE23DFE0D253B2B37C42023E67,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseDataWriter_PrepareNewSerializationSession_m4A4300B80406DA36225BB573EB5DAA5B8ACFF5AF,
	NULL,
	BinaryDataReader__ctor_m88BDA3685AC0E1DD04743FCE6124CEF860907DC4,
	BinaryDataReader__ctor_mA54276115362DE2094F87655555F2C16B6D700ED,
	BinaryDataReader_Dispose_mF69B994CA38DAB15683F0F100FF3259D619394AF,
	BinaryDataReader_PeekEntry_mC5862ECD01767106319CFFBE6DB586312F4B0506,
	BinaryDataReader_EnterArray_m48587C17EB40B23608F4948A91F48DD2482DC045,
	BinaryDataReader_EnterNode_mA48929208D4033E4D6C0CE6E2CB3E4FB552B931A,
	BinaryDataReader_ExitArray_m855C533A72BAEC120B75061961C877424CC47799,
	BinaryDataReader_ExitNode_m30BB0D2249C04057D5B4A104E02124ABBB7C8CEF,
	NULL,
	BinaryDataReader_ReadBoolean_m378DD919E3EC7DA4A737AFCFC4C7115E01B82754,
	BinaryDataReader_ReadSByte_m8CA3AC056171D787B05E6F566DE23F2492BECF6A,
	BinaryDataReader_ReadByte_m717140B880C6F20954E9BAF4A14446B8EB86C8D1,
	BinaryDataReader_ReadInt16_mD731B8F7C26F2B8EB0BAC580B3B505EA480C1267,
	BinaryDataReader_ReadUInt16_mB0BEC5B34F9455C89DB0A011D8A7E0D423C04E7E,
	BinaryDataReader_ReadInt32_m9D58C783DE8979DE5BDC67DBD4DC06FAA35D6CCB,
	BinaryDataReader_ReadUInt32_mBB7C1EA245934C81694607D961F06141405236B5,
	BinaryDataReader_ReadInt64_mFF654267D550E14904877F1BBEA34E9F7DE10989,
	BinaryDataReader_ReadUInt64_m81C63D46C961A0EDE4D52EC2CFD61DE9ED86501F,
	BinaryDataReader_ReadChar_m2F4EA4ED76C3F064EFD96843C28EB6A59DEE541A,
	BinaryDataReader_ReadSingle_m5999034B728C4904CB5C8AB5CE52A294C3A6CAA6,
	BinaryDataReader_ReadDouble_m8C36ADEF111EFFD7957364890E53704747AD3BF7,
	BinaryDataReader_ReadDecimal_m10D99EC0CDAF58960FA91EC8F1DBEB82964F5447,
	BinaryDataReader_ReadExternalReference_m7F3777D3B3B6670E0B712180BB255F2CDCD48159,
	BinaryDataReader_ReadGuid_m8890FE37F616CDAFE09DE989833A9EE72B7A497B,
	BinaryDataReader_ReadExternalReference_m95E0EED00EB14609FDAF9D4DF9C4BB19B09F357F,
	BinaryDataReader_ReadExternalReference_mAE6D5EFEDF45424916199809C711A715B26B359C,
	BinaryDataReader_ReadNull_m4B12D1FAA47604EE85BC27F6AF3121AC117EA320,
	BinaryDataReader_ReadInternalReference_m56A9489CE8F1914CBA36300D7E3077AB22B29763,
	BinaryDataReader_ReadString_m04C5E6B35AA8ABB787F0E2E60B96D709BE8E3E83,
	BinaryDataReader_PrepareNewSerializationSession_m04C7EE2EDA448E61111A065FB7AE2D86CD9BDC2C,
	BinaryDataReader_GetDataDump_m53AF7D5E535DC17312B4CE9F37039980271981C4,
	BinaryDataReader_ReadStringValue_m0A03F2E87E2780551B54AB77442C6BCF3C66BE93,
	BinaryDataReader_SkipStringValue_m0F019C755739EB16E03C6537CCF854ED6645F631,
	BinaryDataReader_SkipPeekedEntryContent_mFAB5708C3E9E82D2230F77839F3AA6C1E330EC88,
	BinaryDataReader_SkipBuffer_m2A54992EC4C254F45B4D9ABD3E13BA433021F4D2,
	BinaryDataReader_ReadTypeEntry_m21824269F79E19F784D1127D633AD608D96BD458,
	BinaryDataReader_MarkEntryContentConsumed_mA52A41349AAB73BF7D8FBA14E9CD01F3FA9641C4,
	BinaryDataReader_PeekEntry_m19C08500083F4A090AD0A9228EA5EB4BD4565B7F,
	BinaryDataReader_ReadToNextEntry_mB84D8F3F979328BDBE8FB313A7BA8958CB89A51C,
	BinaryDataReader_UNSAFE_Read_1_Byte_m6F2977F3D5EC300F18455C8DFA9E142395194E20,
	BinaryDataReader_UNSAFE_Read_1_SByte_m1025D18AB76FF1B908ABCC6840B7446E8FA5C869,
	BinaryDataReader_UNSAFE_Read_2_Int16_m8852115FD75E07626CB464B3334498C42B64B768,
	BinaryDataReader_UNSAFE_Read_2_UInt16_m8A30CE13CE56F91677C9965106BE0C5BDFFED9CC,
	BinaryDataReader_UNSAFE_Read_2_Char_m1879706F390A93F5C3D993A395234BC088A4923F,
	BinaryDataReader_UNSAFE_Read_4_Int32_mC955D7CF98B415A80F7C10A79BF13C9A29385FE5,
	BinaryDataReader_UNSAFE_Read_4_UInt32_mE81BE7436BAE1B93F9044473C7691ED0EDFF15CF,
	BinaryDataReader_UNSAFE_Read_4_Float32_mE61F5BF60D34409BD360DDFA16EF8BCAC3FB1AD6,
	BinaryDataReader_UNSAFE_Read_8_Int64_m5BA01533C4C06169BDFF70B676FC1F7BAE70E61A,
	BinaryDataReader_UNSAFE_Read_8_UInt64_m68DD28FB4A0661326C754BA1837AE65028E30465,
	BinaryDataReader_UNSAFE_Read_8_Float64_m53A76610530D9A77CD4A62AC8BCE3F38EA28104E,
	BinaryDataReader_UNSAFE_Read_16_Decimal_m3C459886DF09B26A00A014CFF2A4B12533F99155,
	BinaryDataReader_UNSAFE_Read_16_Guid_mA6D4CDD78406EE2677B9C51CF06D3501EDFC01E3,
	BinaryDataReader_HasBufferData_mF2D7D3FE10A13C08E0DAE8CB54C0038CE5F45460,
	BinaryDataReader_ReadEntireStreamToBuffer_mBB7516FFB0EA9375B63884D67E29E999FB84B6B6,
	BinaryDataReader__cctor_m28D6C7017F48C534C1D9D62F78AB2F8DF84DE915,
	U3CU3Ec__cctor_m050BFA0A1B61314139EAB2B60E18F0D0AD50EE21,
	U3CU3Ec__ctor_m9A50009B93D606DDE91E49A7B7914F70E04060B0,
	U3CU3Ec_U3C_cctorU3Eb__64_0_mDBB1666D139D83D5D2A8EF76E9F9F98AB9432361,
	U3CU3Ec_U3C_cctorU3Eb__64_1_m060DE417178FA176BA354EF44DD3FFC5C6A4B3F3,
	U3CU3Ec_U3C_cctorU3Eb__64_2_mA65FEF70769F8328F81E6D5B7E686FD52D22B7FB,
	U3CU3Ec_U3C_cctorU3Eb__64_3_mED4EBA4FAF918D29FFD28DD53CE4E6CB61DF360E,
	BinaryDataWriter__ctor_mE8C0E6AC4D94E20B6FC4620B59957B92C109D160,
	BinaryDataWriter__ctor_mB403169F6A5ED63B1F9F06BFBEDC68265171EF7D,
	BinaryDataWriter_BeginArrayNode_m23869E8F7D602BB54675E3567EF4D1E8963BE2E1,
	BinaryDataWriter_BeginReferenceNode_m15A62F582613361E92CE828E9D1B396D2AB3FD7F,
	BinaryDataWriter_BeginStructNode_m744D3776D2AAE2862DA7D9A7C5A0138F600EFA34,
	BinaryDataWriter_Dispose_mFBC9CB175CB2505509760EB1664C37015B0C275E,
	BinaryDataWriter_EndArrayNode_mFA754130106E5188BD185A77E9501D273662CA9D,
	BinaryDataWriter_EndNode_m7B30D2172A2355A13999394A99537725E7A8B98C,
	BinaryDataWriter_WritePrimitiveArray_byte_mFDCC11AFD0D132EFD5788EF0890DA862666A1A19,
	BinaryDataWriter_WritePrimitiveArray_sbyte_m906CAE2C80F702A4718C4016EFD7B9AF04C5F6F0,
	BinaryDataWriter_WritePrimitiveArray_bool_mD59501EBFC7A430EC31F667793F3D10DE8D2A28C,
	BinaryDataWriter_WritePrimitiveArray_char_m36B885F62D556EB9626C0346B31677261064281E,
	BinaryDataWriter_WritePrimitiveArray_short_m50012F6BCB8F48FE5DA7A0ED47BB98D86FB98FBE,
	BinaryDataWriter_WritePrimitiveArray_int_mD5DEAF8CD69CDE1A8785199525A2120C59606990,
	BinaryDataWriter_WritePrimitiveArray_long_m2307A336E4AFC94668E461C7A6765A01CE58377A,
	BinaryDataWriter_WritePrimitiveArray_ushort_m757100432861A9E6CCB587B9F8FAECC6BAE72950,
	BinaryDataWriter_WritePrimitiveArray_uint_mDC712382B05E25F10E9AC6D18472ED59E8C57D85,
	BinaryDataWriter_WritePrimitiveArray_ulong_m71B2C1D8FC07B70BB8FEFB59FD7F30C4CCFAF7F7,
	BinaryDataWriter_WritePrimitiveArray_decimal_mA31C45A78D35C185DA0DDAB136DD1CB1F124E8D1,
	BinaryDataWriter_WritePrimitiveArray_float_m0F539C302A6A4DAE91E23322787B82CB2EDB6BBB,
	BinaryDataWriter_WritePrimitiveArray_double_m7CAD7CC652517C73A535D295A7DCBA9F84B61A7A,
	BinaryDataWriter_WritePrimitiveArray_Guid_m602B4528DEFE6A93200FE5080755DCCEA90F093B,
	NULL,
	BinaryDataWriter_WriteBoolean_mE4F47CADF47BB4C9BC9CDA17777E49760C26EA25,
	BinaryDataWriter_WriteByte_m28B86E020EA166D63EB9DD397E30B576E4F389F2,
	BinaryDataWriter_WriteChar_mBBDA7DC85DC51BDACC02320E3E6FCD676456605E,
	BinaryDataWriter_WriteDecimal_m10C48A79429750F0AA8F39E19BACF438ED6B019F,
	BinaryDataWriter_WriteDouble_m0F4842E0AF1644B5A5850683414C206471975B12,
	BinaryDataWriter_WriteGuid_m3EFCA164A29E0C047375EECFF54529F4756FF273,
	BinaryDataWriter_WriteExternalReference_mD8C5A65AC1400A468C5ADF4F547C3FBFCA8862AA,
	BinaryDataWriter_WriteExternalReference_m73C00B1DA58F822A2865CDD1D81D0160A0E64269,
	BinaryDataWriter_WriteExternalReference_m1ED2E8E514238BB55086D8E2D81A2245820F02AC,
	BinaryDataWriter_WriteInt32_m0DBB8C151A9A1907BA367A8DC5499BA58AF45BAD,
	BinaryDataWriter_WriteInt64_m199CC8179ECAE4A86DE3931DAD5E1B1DC22FF023,
	BinaryDataWriter_WriteNull_m11E857323D675AF78742D373320DD9F44655FD85,
	BinaryDataWriter_WriteInternalReference_m5A0F17550F718686D7A579F837931555C8732FBC,
	BinaryDataWriter_WriteSByte_m02865034A074CBBB9AD551EF755BEA10AC7DAE3D,
	BinaryDataWriter_WriteInt16_m28B49BE97E8ED08A61659A0FAB38E56F834EF893,
	BinaryDataWriter_WriteSingle_m5E082D79A06AF25D663E91A344820B96FE9AE027,
	BinaryDataWriter_WriteString_mBFB41B7E7EC332AF6FB46176755511BD26361B12,
	BinaryDataWriter_WriteUInt32_m04322A37668C5763F647232FFDD8C95231E26B90,
	BinaryDataWriter_WriteUInt64_m74D99C92CA49B22C712D7FB21F3BB2141F9DB626,
	BinaryDataWriter_WriteUInt16_m455076A8351E7B47DA0B36C2D947B73BBC378A8D,
	BinaryDataWriter_PrepareNewSerializationSession_mFD048092F817B76F249BBD87081874EB9A7CC46A,
	BinaryDataWriter_GetDataDump_m3C0B816BC246B9A4FCA5B826B6274068F2281E8D,
	BinaryDataWriter_WriteType_mFB9BB191A388161EA65B990AF79F9975BACC6284,
	BinaryDataWriter_WriteStringFast_mA0E5556D7EA59D494AD4967DA6D65E95364E15B7,
	BinaryDataWriter_FlushToStream_m704AF9F94D9F9678372A72A8A2A242CA67EB30FE,
	BinaryDataWriter_UNSAFE_WriteToBuffer_2_Char_mB8C8663F2F41F25AF90A86D6CEFDD499387C2816,
	BinaryDataWriter_UNSAFE_WriteToBuffer_2_Int16_mC5A1DFDB026AEE62256DF07DF4B7DC2CBC0B9C40,
	BinaryDataWriter_UNSAFE_WriteToBuffer_2_UInt16_m60F01963AE907E760341B2402CACD32F9B0FAA9D,
	BinaryDataWriter_UNSAFE_WriteToBuffer_4_Int32_m751BF6A1EB0E01201FBBA7A585075A0576715ADA,
	BinaryDataWriter_UNSAFE_WriteToBuffer_4_UInt32_m554F134FD72A23FF99350E55E54241A41A60C106,
	BinaryDataWriter_UNSAFE_WriteToBuffer_4_Float32_mEFCAB799A534ED25F8E7F581435321EC80E9ECE4,
	BinaryDataWriter_UNSAFE_WriteToBuffer_8_Int64_m37B06348CFE8B65DE77182168B88BBEA1A2C59FE,
	BinaryDataWriter_UNSAFE_WriteToBuffer_8_UInt64_mFB413BC7BEC0EF0BBBD2E8A9BB0341598D1CA10F,
	BinaryDataWriter_UNSAFE_WriteToBuffer_8_Float64_m75DDAEE21A9BEC51333499CBD925866734990FC0,
	BinaryDataWriter_UNSAFE_WriteToBuffer_16_Decimal_mE782B0B183F85E133D345F118C6ABE808E5E7288,
	BinaryDataWriter_UNSAFE_WriteToBuffer_16_Guid_m4DE7671E9608F3809511664F7C3F88F4772977D2,
	BinaryDataWriter_EnsureBufferSpace_m04C68A2D4BC64BFBAA12F526B048A728D36C1D2E,
	BinaryDataWriter_TryEnsureBufferSpace_mAE324967BD3EB8D7BCD831F77EBEC81E755259E8,
	BinaryDataWriter__cctor_mFCA240934A8DB0FA28B16F2FE405151F6ABACA51,
	U3CU3Ec__cctor_m10BA3D1F6C8CE172D519D38FE640A7019E6BA949,
	U3CU3Ec__ctor_mE6CAD2B866ECCCD71ABEFEAF9824EE51516F1CFD,
	U3CU3Ec_U3C_cctorU3Eb__70_0_m9394A9921C45735E1774EDEE178E280DB947EF5D,
	U3CU3Ec_U3C_cctorU3Eb__70_1_mE4F40DD3FE93F0AC5DB34762C79186001CB40351,
	U3CU3Ec_U3C_cctorU3Eb__70_2_mDA53FB238C9092FF6070C148C39DEAD8BF4CAF20,
	U3CU3Ec_U3C_cctorU3Eb__70_3_m218080691DA514E35F7E9F6F421B0757FF7793E1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonDataReader__ctor_m51DA9B5CE4761257B673E9FC1B8F659BADEFEFE2,
	JsonDataReader__ctor_m3D341A1F756D00056804E6BBF16A23BC8D0FF5D9,
	JsonDataReader_get_Stream_m1FD8B0A211CA90F1EB5ECA314ACB964FD203046B,
	JsonDataReader_set_Stream_m104956178A2E2D80870D1DD651A736E7A6B671D1,
	JsonDataReader_Dispose_m121B0C2D97AC16D65131D2E634259304AF8B9F4F,
	JsonDataReader_PeekEntry_mFFA0E512EC9D3BECBD1FEDC1E73D943B85403925,
	JsonDataReader_EnterNode_m89CE444FCBB254BAB383DBF7DF481E3B7E772C76,
	JsonDataReader_ExitNode_mF41AA3DDC0B39E898A4C491512220D142FEED8E5,
	JsonDataReader_EnterArray_m3E5DCDF3F3CB27CEED140ECA54CC23FF9929249C,
	JsonDataReader_ExitArray_m63576697912B4C4A5E2C4D4CCFB41FDEE14E39F0,
	NULL,
	JsonDataReader_ReadBoolean_mB3DA30F8AED83AE4F50217AE0B015BEB44BC262F,
	JsonDataReader_ReadInternalReference_mDCAB35DD2C45F3B250F2CD406E5DAC29F3D943A2,
	JsonDataReader_ReadExternalReference_mBD9A29ACEB825419A3DAA641713FF5E6759AEB13,
	JsonDataReader_ReadExternalReference_mB0CFF43B82D984854F29334F1A7803CFC6AE3E7A,
	JsonDataReader_ReadExternalReference_m6EC3A12FE0884F5B1A2686AC8857E547881015B5,
	JsonDataReader_ReadChar_m11B84F63F7E4EBA53E5ECE1887793354B8E0451B,
	JsonDataReader_ReadString_m5FD0D89F41B40922B2E49F6E6DBFAC0664633294,
	JsonDataReader_ReadGuid_m1D28006EFC04FE50EC4E2C385CF900B4E755044A,
	JsonDataReader_ReadSByte_mF66D7B7C8A70D23D5BFCF4B26DA26C8A7C0EF295,
	JsonDataReader_ReadInt16_m6D7C4FDD4B499D3C80DB0B164CCC3AEAB3CDC9C0,
	JsonDataReader_ReadInt32_mA49B7DE2BC792066AC3F4E2EC48E5F6DE7CB92D1,
	JsonDataReader_ReadInt64_m2777FD3B0F3A2D076949D65992730BDEB6C1B50B,
	JsonDataReader_ReadByte_mA95BACF8EF6611FF60F729DF4B39119C0EDBD475,
	JsonDataReader_ReadUInt16_m55DE613E4E22907162B9CF33A656EB8DA366F45F,
	JsonDataReader_ReadUInt32_mCCC149ABBF223832FDD83E4C2467E4625F7227F5,
	JsonDataReader_ReadUInt64_m2B938DEFF221996467398B71CBD12ED1855F5CEF,
	JsonDataReader_ReadDecimal_mA3993D315AB19A9DEFC5B5F9D4A35846A4E4DDCA,
	JsonDataReader_ReadSingle_m5D278CD30FC408EFA44E0BECDDDB9BFA82E71493,
	JsonDataReader_ReadDouble_m21BB0DF645B3A475F21B2DD80AA610FE87E58EDE,
	JsonDataReader_ReadNull_mB05985D273EEFBEFE244A878763D8BA155B067B6,
	JsonDataReader_PrepareNewSerializationSession_mCE2442C90D330F1BF1F58266F16048AA27421887,
	JsonDataReader_GetDataDump_mA3ABCAE101DA2237202785B0FD46E0848B9A57A7,
	JsonDataReader_PeekEntry_m3A7C036B300CBFE7F8FD0A3813C5587BD1B46427,
	JsonDataReader_ReadToNextEntry_mB13D45332629214020D7DE0878BF929CAA8E95D4,
	JsonDataReader_MarkEntryConsumed_mB16539F226824B0FB118E90DEDA9085D3660567E,
	JsonDataReader_ReadAnyIntReference_m10B56D83CAB7F45067FEE094696374A95C12E726,
	JsonDataReader_U3C_ctorU3Eb__7_0_mE112E5F9DFDA44F2606308B54A5E04218F59A737,
	JsonDataReader_U3C_ctorU3Eb__7_1_m5609CA50E804A1EF18853730CB82E802076F3A7C,
	JsonDataReader_U3C_ctorU3Eb__7_2_mC4AEA8E022EF968F7930688A9DE52F82AAE9BB97,
	JsonDataReader_U3C_ctorU3Eb__7_3_mC8957C5C49E97F87110FFFA9BCFBF1F74C666531,
	JsonDataReader_U3C_ctorU3Eb__7_4_m464C8FBA48324195572556364DC4F3BC4BBE125C,
	JsonDataReader_U3C_ctorU3Eb__7_5_m479A5766373A101476E74F395E39373CA7B0A6A5,
	JsonDataReader_U3C_ctorU3Eb__7_6_m9A5E12F6A64B6B3D2B32EEF86F71929EF8A94216,
	JsonDataReader_U3C_ctorU3Eb__7_7_mFB69ECDA3876AA464B186644A0F7EF33331F7E8C,
	JsonDataReader_U3C_ctorU3Eb__7_8_m75C1B2B2333056CAB76CAE090BE5874D5CC800EB,
	JsonDataReader_U3C_ctorU3Eb__7_9_m9DBD6DDCA3741C2E2BA5B37ADB3176063CB1D719,
	JsonDataReader_U3C_ctorU3Eb__7_10_m0B170AEDEB2281A9CBDE3C2F6BD1039EE9563425,
	JsonDataReader_U3C_ctorU3Eb__7_11_mC6888AE4E54E61E13C806FEF2DF4D668E1D32762,
	JsonDataReader_U3C_ctorU3Eb__7_12_mC55185DCF8A9B00CD50747FB19363CCF9F254BAB,
	JsonDataReader_U3C_ctorU3Eb__7_13_mFBE6997F4A7B1FD533492AC6C83BC604B8717761,
	JsonDataWriter__ctor_m851DEFCEF694742BA30B7673E9C62AB7DE4A3469,
	JsonDataWriter__ctor_m711A85A2B2C05C48E3C5E8663582F1BA62736240,
	JsonDataWriter_MarkJustStarted_m531868F82600199DD5621C42D780572CC3CC390A,
	JsonDataWriter_FlushToStream_mB650CA1012ECE6DF8B9CCF9EC411C608EA82987F,
	JsonDataWriter_BeginReferenceNode_m2978F64A7BB49FC84EA7D753E9751B39EF853723,
	JsonDataWriter_BeginStructNode_m8EDF08362CE624456C31511FC4757A9601FCFFCA,
	JsonDataWriter_EndNode_mD67CF83285D57EF02660BCEA35E14C1AD2499F09,
	JsonDataWriter_BeginArrayNode_m5D9A7C3556356B14D925D0B1FB05111488BAB280,
	JsonDataWriter_EndArrayNode_m5E3E91B46CC2FD8149587AC150EE7E76E9ED1CC7,
	NULL,
	JsonDataWriter_WriteBoolean_m6D0CFC4989D4CE8FCF0DD49286AB2F52D69C4970,
	JsonDataWriter_WriteByte_mC499889A0DF48127921FEF283196D840E9D64432,
	JsonDataWriter_WriteChar_m9A89C6BDBB07A9DDD45CA38CF53DDEC0F6375A71,
	JsonDataWriter_WriteDecimal_m1AF3F0F600A8A1E695ABB536FA9A7CFEB41D56A6,
	JsonDataWriter_WriteDouble_mA31D3E3A26D77BAC4B6F922660F186C615EC018E,
	JsonDataWriter_WriteInt32_mC48B12895C77667C50C31372F38159F7FBA09D48,
	JsonDataWriter_WriteInt64_mB8C27BC09B68E46B3A434BC71476BBB9483199B2,
	JsonDataWriter_WriteNull_m35404288EBAB7D5A7E8DCE4E6FBC8DE4D55149D4,
	JsonDataWriter_WriteInternalReference_mF45FEF6998295E6E5855971E0AF6D9D28D3BB5DD,
	JsonDataWriter_WriteSByte_m67C864251205A6CA76AEC024EDB214C43EA329B4,
	JsonDataWriter_WriteInt16_mD3F9C7F63522165DABD06F6FA79C84685438A306,
	JsonDataWriter_WriteSingle_m8FB6BD96049EECF56E9D0B320BAE9B33466BD848,
	JsonDataWriter_WriteString_mF82E495FB1E42F3A42CE3FD166DCDD19AF033510,
	JsonDataWriter_WriteGuid_m32F9E13793B8525EBDED213B387B0D76AF346CF9,
	JsonDataWriter_WriteUInt32_m78712AFC4332BABA2D0A621097C3DB3B2713DC13,
	JsonDataWriter_WriteUInt64_mB21547576308E8A1C44B6BB8C4AE94EA88580268,
	JsonDataWriter_WriteExternalReference_mBF8DB8F6A5A0B1626D8D51125B760F7A5AD33746,
	JsonDataWriter_WriteExternalReference_m72C6B98B22B694370598C62BDFF40C13DF424F9A,
	JsonDataWriter_WriteExternalReference_m45430BFA1F2F59ED2E05DD5F44C24D5F8D8B0230,
	JsonDataWriter_WriteUInt16_m781B5D8E4454BD8870FFB6ECED811DB45B1069CE,
	JsonDataWriter_Dispose_m6EDECE39054BE9219187614C76E4532A604AF3D3,
	JsonDataWriter_PrepareNewSerializationSession_m2B13D9AE64EBD408215A2F00B2BB1588CA9F3EEB,
	JsonDataWriter_GetDataDump_mBD5CC997829AF86D5530E429685B2A6DB16520F8,
	JsonDataWriter_WriteEntry_m49EF3A6E9F7BA03F4B8609EDE9B48A2777B86F2E,
	JsonDataWriter_WriteEntry_m777296708D807EBC2ADEE4D25BA67FF31DF0A710,
	JsonDataWriter_WriteTypeEntry_m8AAC5D013E600361B59CAAE8925AAD18FDF3FF77,
	JsonDataWriter_StartNewLine_m6B7DF7C8E8DDCCC0E11D9556C37AF65B9D34599D,
	JsonDataWriter_EnsureBufferSpace_m9145E0B7D97EC0E232061FFBD4D2BC5DD3942377,
	JsonDataWriter_Buffer_WriteString_WithEscape_mFEB0D9173C7A864412B7320B82196FBE4910D45F,
	JsonDataWriter_CreateByteToHexLookup_m68509F4F8D258B3D2C955888A79804ABD9CF624A,
	JsonDataWriter__cctor_m01EFB632820150DA81DEBF25CD6A38CCDA8DC1D7,
	JsonTextReader_get_Context_m85449D5D0A92D23879113C52E9AE319A041C8F1E,
	JsonTextReader_set_Context_m1E96CDF1C8EB6B390CAC08457B7D3A3CFAE0CF1E,
	JsonTextReader__ctor_mAF3DBEFA5D85C6D579B9F5129ADB7FC3F97AF6AD,
	JsonTextReader_Reset_mCB8A3696E0962DEEB8EB1D6AEBF13D05E0918A84,
	JsonTextReader_Dispose_m9D330A26E52DEA3CD69278701B2F052C6312BF57,
	JsonTextReader_ReadToNextEntry_m4057BF364DEF32361FCD6A62A5515ED729E15F4F,
	JsonTextReader_ParseEntryFromBuffer_m9F2CFFC87160FF602E76C7164452723E8887E34F,
	JsonTextReader_IsHex_m62B6807A96E386E797AEA198B169269290FB0F4D,
	JsonTextReader_ParseSingleChar_mD4B9F33674147AFD2BE18CC5FAD0D2D69ACA6DD4,
	JsonTextReader_ParseHexChar_m00E5CBECE891BB46C31165C021D002A7B0142EE6,
	JsonTextReader_ReadCharIntoBuffer_m99B4E7BC5701345C959263230EE74C866E27CA7E,
	JsonTextReader_GuessPrimitiveType_m811107AD6D53F3FF73D802FFFE2A05D4AEF12E2B,
	JsonTextReader_PeekChar_m178748E2D58685EC74F9DA7DC84D3DE35AA9F22E,
	JsonTextReader_SkipChar_m4A7D5C8E3C97615E4E3CAABC1953BA3D0175B360,
	JsonTextReader_ConsumeChar_m139BBF47B171C950216BFFC84B7EAAB9AE1ABAF6,
	JsonTextReader__cctor_m3BBCC5BF5EFCDBD99C6D648DBB7964E2C0E56CEE,
	SerializationNodeDataReader__ctor_m976E320236651917C2442F91C73EB277620F96BF,
	SerializationNodeDataReader_get_IndexIsValid_mDD227070003AB1FEA659C40A50FB8C10089BEF5E,
	SerializationNodeDataReader_get_Nodes_m36C8F1179E69BA520B00F46B4D58CB5B76F5DEB7,
	SerializationNodeDataReader_set_Nodes_mD18A6F4C40C3A0DAFEA50E4D75F60C678D897782,
	SerializationNodeDataReader_get_Stream_mEF9060551BCB9762C20C4845661403477BC5BBE9,
	SerializationNodeDataReader_set_Stream_m122526BCB2DA890CA057B2F06177922164608C34,
	SerializationNodeDataReader_Dispose_mF6C4B011A479ED47F802FB78D4B06BFED5049023,
	SerializationNodeDataReader_PrepareNewSerializationSession_m44A0A929785EDAA12734E01AEC775DB062C205CB,
	SerializationNodeDataReader_PeekEntry_m68F9F44526B9B723BB78679374E4F8D6C546F633,
	SerializationNodeDataReader_EnterArray_mB9AE0DBE4F116BF09ECBD614025A825EC31654D8,
	SerializationNodeDataReader_EnterNode_m7DC9C962190B096693E8A40EF755C07981B86C43,
	SerializationNodeDataReader_ExitArray_m26DA1C1CE9ACE7E39FD327EC27AC2F89300F8ACC,
	SerializationNodeDataReader_ExitNode_mC35E49FC3D8382B8A7FFC7DB5457C30B16F5CA12,
	SerializationNodeDataReader_ReadBoolean_mFC5E84A6364091296F6CCA59A1A13D9363A26AA2,
	SerializationNodeDataReader_ReadByte_m61786C88BF6170044C3F58E1BB3C003F8F249B88,
	SerializationNodeDataReader_ReadChar_mD36FBC747836E56C8792267EB939616EA093BE5B,
	SerializationNodeDataReader_ReadDecimal_m6DF389A32D61DF851B9A912AFCE1A65D929E6F61,
	SerializationNodeDataReader_ReadDouble_mB3EE58D57B48DD309C3F9E43D501487A4056DE28,
	SerializationNodeDataReader_ReadExternalReference_mE0B613FF2E56563074B56E1D8E7D51B93E6405A7,
	SerializationNodeDataReader_ReadExternalReference_m04E8A8BE8CD751D6A48679A2347D35AC5DD15014,
	SerializationNodeDataReader_ReadExternalReference_m8B4165A0585145045494B7C79DAEE2307264A75C,
	SerializationNodeDataReader_ReadGuid_mB5062678B1D6574258F44BBFF3BF680489D2BEFF,
	SerializationNodeDataReader_ReadInt16_mD57065D39B67F874F9EE79EC6E0DDEBFCD211833,
	SerializationNodeDataReader_ReadInt32_m2197161BD17FBE39ABB3F7E806A64BEE8F26F8DC,
	SerializationNodeDataReader_ReadInt64_m5A9A718C2B00FDA772A6D97202CD5B0C41827EEA,
	SerializationNodeDataReader_ReadInternalReference_m6ECF4FFBDF05D08E0D4608F1F2B8569D36F32CB1,
	SerializationNodeDataReader_ReadNull_m6CBFF360F2E9822E477F736041E3AAC8BFCD66EA,
	NULL,
	SerializationNodeDataReader_ReadSByte_m3C5376F6005997C3F181BC513A4E4D040EF958B4,
	SerializationNodeDataReader_ReadSingle_m1A2799BCFECF85FBA97D961107D1F5CEDEDD5663,
	SerializationNodeDataReader_ReadString_m8BB41C0D2F6C35E22085AA7E1395B922619EDD80,
	SerializationNodeDataReader_ReadUInt16_mD3BEE085176842B38CBCB3DF9D76DFBF8C127F9F,
	SerializationNodeDataReader_ReadUInt32_m061081D18E3B14BECC40CAA70C60EB343EB0BE0F,
	SerializationNodeDataReader_ReadUInt64_mB4C90286086084D8B9180956E1ABDC9452ED2582,
	SerializationNodeDataReader_GetDataDump_m6A08FB2120CDD1DA7693EB4968607B9E427D2BF0,
	SerializationNodeDataReader_ConsumeCurrentEntry_mD1B3BAE2DA7C8A2049B9107C649E442C69025A7A,
	SerializationNodeDataReader_PeekEntry_mC3CA21618EAD0A4C620B8972F1FE0C4787CA99EB,
	SerializationNodeDataReader_ReadToNextEntry_m077B8C3A1757378ECCD5ECEABB9352CB0823CCB4,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_0_mE4786C23BEA3BE4BDBA3BFBBCAD62EB2AF48F80D,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_1_m9614490D64B32D4C2C7B34D3F5122C4C46C8041E,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_2_m725219854A80EFD61A1FA55A8243FC00FE7ECE7D,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_3_m553562A6D41179FCA3B9893BD7FDDB20B0979CEA,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_4_mA397B03CC5FF51044EEC1DC1477AD5DD2B916F5C,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_5_mDD9F9F15739E3E869AA1EC8AAFB7E62F741F9247,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_6_m9A8AEE2C7F6C1C6215BBBC409C262CF8A8FCE210,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_7_m2C3DEDE8BD7C704B1A927DA281F1DCFD2ECE2E7C,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_8_mDB3195374DA2876A9941FAA9E1EE72996C62506E,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_9_m86984641BF2059AE06B1BF464078BEAB8347F353,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_10_mAB88D550A4561B0CD7E4E28114A3049BA16A1BE3,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_11_m0016EC67B3BEFB616554ADB1C1D69F0EC7E0592C,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_12_mDC36F9EF4481344B8CED2008599AB65B40E53AC1,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_13_mB1E9872F357E0C93A300E29625BCBC745E7EE8C9,
	SerializationNodeDataWriter_get_Nodes_m2F595A8DAB26FA65D31681F315C55176A8008F79,
	SerializationNodeDataWriter_set_Nodes_m94A735E4146A17A5D53AB873EA06F51A7F0EC57C,
	SerializationNodeDataWriter__ctor_m5BF16B54AFAE4A7D16FFEA8896E1FC704B5682C5,
	SerializationNodeDataWriter_get_Stream_m31FEE403884B9A1DE3BDFC72C9D5AC5471D37EAB,
	SerializationNodeDataWriter_set_Stream_mCE92CD9A575D912D347127F9F0A38055AC6485A3,
	SerializationNodeDataWriter_BeginArrayNode_m15ACB11CE6D36CD77A8A07B87DDE9ECDD33E3A03,
	SerializationNodeDataWriter_BeginReferenceNode_m8E26D0D1B3EA8EACD1F5C50F489CBBB9B8A8C9A6,
	SerializationNodeDataWriter_BeginStructNode_m538277BB76CC0BB894F2830570FF730FD76ACDA5,
	SerializationNodeDataWriter_Dispose_mA68AEA3B7D9BA3C986136BD222A1E7A797A05E77,
	SerializationNodeDataWriter_EndArrayNode_m0A678F402E1BAF2147893E66BAA1F8098512DAB6,
	SerializationNodeDataWriter_EndNode_m29CDFF09537E779AACEE2C9EFD76394450119C84,
	SerializationNodeDataWriter_PrepareNewSerializationSession_m4583EB6CED541EF60331D43D16F5F82664EDDC50,
	SerializationNodeDataWriter_WriteBoolean_m9C9A0C2AFE02057EBFEAE932EE7F02DD14D7F116,
	SerializationNodeDataWriter_WriteByte_mC256DDC404C92E3ED3B8270E14BB19DFA85D8761,
	SerializationNodeDataWriter_WriteChar_m5D0D25AC02A5C7DC600EF380D0B50AD1132AD350,
	SerializationNodeDataWriter_WriteDecimal_mB72E84911B4165EA941C0F704640C0F7E78E1046,
	SerializationNodeDataWriter_WriteSingle_m3876574D5FBBE2FECC1A8778B6570370053EFA97,
	SerializationNodeDataWriter_WriteDouble_m72CD65562A4C0EDAB0F6D3DCBDD1DDCC46ED0E5A,
	SerializationNodeDataWriter_WriteExternalReference_m8D02BC0796F6FD231BA3AD9F82953C04900B3BFF,
	SerializationNodeDataWriter_WriteExternalReference_m2FB93C1993BC4623A3C30BC26938F8AB9F8C0D8C,
	SerializationNodeDataWriter_WriteExternalReference_m925C30CA4279A0B7650077D97B7682F1522BDCB8,
	SerializationNodeDataWriter_WriteGuid_mC5FB42F68F3614F653A98ED5F13EEF573E280027,
	SerializationNodeDataWriter_WriteInt16_m04E73581ED5CB25A7D1BD4AAF40E16A6231E1DCC,
	SerializationNodeDataWriter_WriteInt32_m24A8DD1F2BEE457A2CADEC2F16ED8E4A04BE9027,
	SerializationNodeDataWriter_WriteInt64_m410EECDB21C110A925BF00274223DC90CE78E7E1,
	SerializationNodeDataWriter_WriteInternalReference_m9D1646ECE3E2461FFE9A23A1163CC151D71C6C37,
	SerializationNodeDataWriter_WriteNull_m0F56438C0AA4E0F8AD204BD710EF3916889A74AF,
	NULL,
	SerializationNodeDataWriter_WriteSByte_m7DC5DDDD04B85AAE54FCD23E21120FA74867F6C2,
	SerializationNodeDataWriter_WriteString_mCC5A7CFF35A49490A3080EAC4852FF04B8CF9516,
	SerializationNodeDataWriter_WriteUInt16_mD1EC282993194412D65D94471C0DDE3AB1BDD7A0,
	SerializationNodeDataWriter_WriteUInt32_mA62961CCB923DA980A4A31AA48484AE735967D47,
	SerializationNodeDataWriter_WriteUInt64_mE2CAA588C079618AEB70B5EA1CBA3F28167F4E92,
	SerializationNodeDataWriter_FlushToStream_mD1DF9FA3EF5A01361A40FC2B201EC1DE243662EE,
	SerializationNodeDataWriter_GetDataDump_m4C3792A22D7877C0A4B9D1D8FD2881310FD19612,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ArrayListFormatter_GetUninitializedObject_m04973C36E971A3DB1E189192E6684406960A20C1,
	ArrayListFormatter_DeserializeImplementation_m8DE5475CC951B9C7B575473993A34E0E245CD962,
	ArrayListFormatter_SerializeImplementation_mA8D8712B515D2E326C466D04E74B1F7DCC934133,
	ArrayListFormatter__ctor_mA635D89A7A48DE51AFC221F381D749BDE8200657,
	ArrayListFormatter__cctor_m28360BCF306AC2594252927FA3835F780D1F67CF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DateTimeFormatter_Read_m6CF40B021E9E325ECA8BBCD6A10198CCD92ED2BC,
	DateTimeFormatter_Write_mF6286F52612305C1EE2A75568F6592C38061B8D9,
	DateTimeFormatter__ctor_m439621F8BEA99A2EC6D0318CD4BF24D01A86C4B6,
	DateTimeOffsetFormatter_Read_mC88476F648CB87FF47A357297E4DB88C22B3F7EC,
	DateTimeOffsetFormatter_Write_m9353636387D2D0987B30B71F25DFEF9BDB311B6A,
	DateTimeOffsetFormatter__ctor_m6BADBC035BFCA8469842F64E44C660ED70FE2491,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmittedFormatterAttribute__ctor_m5F7732353D2CBA57280CDEDCE9FCA2947E8ED7AB,
	NULL,
	NULL,
	NULL,
	FormatterEmitter_GetEmittedFormatter_mC29E5001FE34E89CC77C42FD191F2E14C9AFBDB2,
	NULL,
	NULL,
	NULL,
	NULL,
	GenericCollectionFormatter_CanFormat_mF1F0F65ADA7A21400F8599584FE524FB19651FB6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TimeSpanFormatter_Read_m53B0D1FE0F62588C8A68861DEDACC74D69EA7526,
	TimeSpanFormatter_Write_m94304EDED26C10825038E8D17836F05E14DA4346,
	TimeSpanFormatter__ctor_m68C98C23B9613C27EB5E39EE0BC4D059515DD3C2,
	TypeFormatter_Read_mA4D03F1AFAF413F0EF38BF4386FFDEDE91F51FDA,
	TypeFormatter_Write_m3E72764ED302CF03A5B7684C8130A90442D5C4D3,
	TypeFormatter_GetUninitializedObject_m7668EA7607A2A819DFCD62797812C5F74B4813AB,
	TypeFormatter__ctor_m6A3965C47CCF9DCC76DD94E9860EBEBB4F5B0CB3,
	CustomSerializationPolicy__ctor_mF0E9B48B7CA3F0F2BCAC8937D7867F6BAA1E0863,
	CustomSerializationPolicy_get_ID_mD91FBD9D8A69EE3FAEA6D9D83C168E4F90999CC8,
	CustomSerializationPolicy_get_AllowNonSerializableTypes_m7CCBE44B1480F9CE9120D61A88404D7A8B90CF33,
	CustomSerializationPolicy_ShouldSerializeMember_mA10AB4E26282F9E85005770B3427AFB5026D8568,
	AlwaysFormatsSelfAttribute__ctor_m01671ABC744F345DE0650CF9BFB66965CE918A6F,
	CustomFormatterAttribute__ctor_mF4344679DBB6AFBE8848D6F15134181BEB721D7A,
	CustomFormatterAttribute__ctor_m10C204AA0E45D48ECF6E1B8DA75B437EE0B4F9C5,
	CustomGenericFormatterAttribute__ctor_m67FCB3E66F68E2A28591F2E22D9F2D4478E36AFE,
	BindTypeNameToTypeAttribute__ctor_m881CE0D2924F38FE487A307B0D0481654FD32AD3,
	DefaultSerializationBinder__cctor_m2BA09ADCD6A5C21FB5CBD178709813A404545BBC,
	DefaultSerializationBinder_RegisterAllQueuedAssembliesRepeating_mA644CF00BDF642617102E9D52E30E4D3DA6EEA3F,
	DefaultSerializationBinder_RegisterQueuedAssemblies_mBC5FA26D023E679B2BB5BA975E6B8067EED52C46,
	DefaultSerializationBinder_RegisterQueuedAssemblyLoadEvents_m6FA3398A3D26D3975E6FE7F841DA7779F598E360,
	DefaultSerializationBinder_RegisterAssembly_m47B9087EAEED274AB85765B0F97F17CDCCAF8631,
	DefaultSerializationBinder_BindToName_mC1A6799C4958E6C8311103BC0E26DC8E009DEB7A,
	DefaultSerializationBinder_ContainsType_m0097D27B7BD36234F6CE558F2BBA2BCF30027A85,
	DefaultSerializationBinder_BindToType_m9B60651682307A6440742211189F8A4E92059FAB,
	DefaultSerializationBinder_ParseTypeName_m60799F5F3FEB97BFFEC254E7A000B992690F31DA,
	DefaultSerializationBinder_ParseName_mEE81027F8492E0061686C5701F0FDAF52171AAE9,
	DefaultSerializationBinder_ParseGenericAndOrArrayType_mF1FB286A39E3489BFB69F9AFD26D9C80AC4558DA,
	DefaultSerializationBinder_TryParseGenericAndOrArrayTypeName_m63A65860D8573320D5DB9C8CC8F358D9FB1F4E03,
	DefaultSerializationBinder_Peek_m57DF109450B8150FCB405E9C9758DE9EC4E50995,
	DefaultSerializationBinder_ReadGenericArg_m850F2FCB8ED0AF502377961C0EFD631DA9CF9685,
	DefaultSerializationBinder__ctor_mBD4FD9D22DA227762E230B0B96F54C0B51216161,
	U3CU3Ec__cctor_m420F65567DCDE63419AA582AF9C6D3317ED3F8E8,
	U3CU3Ec__ctor_mE7E82CAEA1C3D4F27E4F0F974D5151EC0166E907,
	U3CU3Ec_U3C_cctorU3Eb__12_0_m48E725AE2582F51909BF5BAFE94BBCC828BB7E1C,
	DeserializationContext__ctor_m79D54C0E4356B35D7388C2D524F7F3CD53AF8698,
	DeserializationContext__ctor_m95122F1819EA3773F68FDD54225A97417FBF6DBB,
	DeserializationContext__ctor_m099CEE002CA35BC58B5376D29A8196F453A8593D,
	DeserializationContext__ctor_m9BED40C786E9A2148142BB290DD03B9415CB7DDC,
	DeserializationContext_get_Binder_m0F7483ED7970C6864CC4AF19367B176D077754D8,
	DeserializationContext_set_Binder_m6CD316DC0C08EA4983186B84EA3631AD513652AB,
	DeserializationContext_get_StringReferenceResolver_m7EC7BB3F21C03E172E695D6A0915EFF28168CD38,
	DeserializationContext_set_StringReferenceResolver_mAF3F1131432D3AC47708724561C1FDA47EB4856F,
	DeserializationContext_get_GuidReferenceResolver_m3FE8878B4E53FFC7653259668E6CB95A24813557,
	DeserializationContext_set_GuidReferenceResolver_m08ED3C754D5057DD5612B1EC68D211077EA7944A,
	DeserializationContext_get_IndexReferenceResolver_mA4280CD20359B1BC999EE911C429B29E45B5D155,
	DeserializationContext_set_IndexReferenceResolver_m514050C386067920670659B3F0C19DEF1F6F07FE,
	DeserializationContext_get_StreamingContext_m76EF6F12E9E400F9E26F041E0E676D17B7C6857F,
	DeserializationContext_get_FormatterConverter_mEE5C9D74DFA980032F2141501E31B03BC56B37E6,
	DeserializationContext_get_Config_m7AA43C53F73F7BB081EE8594F43DCFCF654CD61B,
	DeserializationContext_set_Config_mB4D81343426C20E7ACEF01DA2B107774330921B9,
	DeserializationContext_RegisterInternalReference_m97967CCED634A7BC6B81094E7136A6FA540FDC89,
	DeserializationContext_GetInternalReference_mBFF38146C554EA6725D9C99AAC8EA9F80E67A5B2,
	DeserializationContext_GetExternalObject_m456A066739D167013EEA411007A7B120B462671F,
	DeserializationContext_GetExternalObject_m26A1BD8D927081962282FF1CBFC993309898685C,
	DeserializationContext_GetExternalObject_mBD215122F85C838A4B6BEAF0E8CD76CBFA774453,
	DeserializationContext_Reset_mAF73339904ADA59E93E0C72698840540AA0D8B5B,
	DeserializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnFreed_m4932D59DD37B39F55D072266A0452037C54B56A6,
	DeserializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnClaimed_mE4EC7ED370A57EEDE03D39DB668A6DCFF1933FE6,
	ExcludeDataFromInspectorAttribute__ctor_m7A8B44B57C2485D75808F50E6FA5B5C5AA029A5B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NodeInfo__ctor_m386DC2E5B9CA08AC916FB9E905B8F8E6C4B0414D,
	NodeInfo__ctor_m0F7216E0579077DCB8FFEC72B74FBEF04C95A4BF,
	NodeInfo_op_Equality_m382CD7C5A80AEFB90958B6912ACEC88B5EC273E6,
	NodeInfo_op_Inequality_mF36F9D098733E0281D8137D061AA92800DCAFC67,
	NodeInfo_Equals_m1B4D737F1DF0003BA4319E5899E8410B46EFB0D3,
	NodeInfo_GetHashCode_mB34C81EAD31FC0CBF8FDE38F4B7B132ADACF72BA,
	NodeInfo__cctor_m56D181C999D2B94FEDFC1AEDB35F892F1980E299,
	PreviouslySerializedAsAttribute_get_Name_m263CA2EFA9F5FF153F46444AB2F1764C8B5AEF06,
	PreviouslySerializedAsAttribute_set_Name_m9F8AAC3B45CE7004CE1EBB2F89D0AD7549F8BC79,
	PreviouslySerializedAsAttribute__ctor_mD3CD2C7AF3AA5CA89F18D8A0D141FAC2C52020C7,
	SerializationAbortException__ctor_m0A554B480F0DA7439C460F7FF1E147148E90087B,
	SerializationAbortException__ctor_m04E5BD4C6547BF155AF6D8202E4F74DE9046A1CE,
	SerializationConfig__ctor_mAD8DBBEDCEE62EF4A1DB3C0DFBA20C987C752D36,
	SerializationConfig_get_SerializationPolicy_mF5A346026244681F0BE0BF811FC742D029329C99,
	SerializationConfig_set_SerializationPolicy_mF3B26A684D5231A1FE1B4E5228A6F28F981FB29E,
	SerializationConfig_get_DebugContext_m97D75096A9AFDE935F9029FA3028B64082E93BEC,
	SerializationConfig_set_DebugContext_mE32D277747876FEAF10C107ABEB246C94656D58F,
	SerializationConfig_ResetToDefault_mF52B8EF79E6101B0B381BE43F6907860FBADCF8A,
	DebugContext_get_Logger_m48999B60566E0CA1F4BE2FBF1FFC4AA400403861,
	DebugContext_set_Logger_m323E707822A94C5B246E49BF83B54A08F7D3A1E3,
	DebugContext_get_LoggingPolicy_m9C0F1780DA9D0F18397F88B487D2B6261D43E3EB,
	DebugContext_set_LoggingPolicy_mBE96DC6AF7DE44E5C6DB8DAFD07C7F3AA87C3CA8,
	DebugContext_get_ErrorHandlingPolicy_mF069F8E11DB73283D51BC497AF7C5C00F4006318,
	DebugContext_set_ErrorHandlingPolicy_mA11018C68636D5DB0BB8B316DA42CBE0765E3054,
	DebugContext_LogWarning_m0A9E235C81573239C62B3417BBA20A6773320657,
	DebugContext_LogError_mB7964B4F0CB8C00D8B9CCC91037527925CBB3362,
	DebugContext_LogException_m05D07140DCF15AC718B731085DF5B968770B5E0F,
	DebugContext_ResetToDefault_m2C706BAD23AC009A673BB3BEC3621644E6D85DBA,
	DebugContext__ctor_m78606858472F4B49FFD979C5B54ADD088A63F930,
	SerializationContext__ctor_m465542BACCDD5D5ED7ADEE78D5C01FFDB22E4203,
	SerializationContext__ctor_mDF9626225DF127D4FDEA3420BC9CBF09BCED0A88,
	SerializationContext__ctor_m9915F037800ED44484225B8AC6F316F7DBE8BDE3,
	SerializationContext__ctor_mC30D75D5C6279F2655F233D455050A081D046EC3,
	SerializationContext_get_Binder_m117D9C9DCBC078B3749583A30C4FBC19512CA08D,
	SerializationContext_set_Binder_mB35866E4C6AA31FC78A0305C677ACE97BBC4EDC8,
	SerializationContext_get_StreamingContext_m2B673D425AA0BDA4A4704CDAC540A1765AA922EA,
	SerializationContext_get_FormatterConverter_mB53B251AD78B4CE1D6B5A2A884ABC1A52102D79E,
	SerializationContext_get_IndexReferenceResolver_mB03F8888875822A25310186B3162C1D54F507DDF,
	SerializationContext_set_IndexReferenceResolver_m302A313D8B5940631CE1080D63B031BB72A9F37B,
	SerializationContext_get_StringReferenceResolver_m117290E2FC39BBBDC598C71595B9DEE75E23734B,
	SerializationContext_set_StringReferenceResolver_mE6623E2962600E1B37CC96F15013099103AC441D,
	SerializationContext_get_GuidReferenceResolver_m623454ADCBFCD5198104AB855E0AD913427AA714,
	SerializationContext_set_GuidReferenceResolver_m1B9940D0DF827220AAA92094670429882C752BE1,
	SerializationContext_get_Config_m4A193016B5537E6E7B84CA0E5D034C17545C9A6A,
	SerializationContext_set_Config_mD3AE305F6F6D5A2202718D5850A54A3D110894D8,
	SerializationContext_TryGetInternalReferenceId_mAE5155ED3C0CA798929299038DEA6AEE6303DCA9,
	SerializationContext_TryRegisterInternalReference_mBF8ADD20EB3D72AC54B1A66A1ED8A1B454BC8BFC,
	SerializationContext_TryRegisterExternalReference_mD6E5EA399D9A9D42E82D7103111F21C34BDFF82C,
	SerializationContext_TryRegisterExternalReference_m756F497F8EBB10CF732F65439002F5C72B278860,
	SerializationContext_TryRegisterExternalReference_m9BCE1D1E2C394CD50C4302F18007A169890C01B3,
	SerializationContext_ResetInternalReferences_mF3482C4F865E151C2D52ACE70D50B91DC8AB1C2C,
	SerializationContext_ResetToDefault_mE5FEC8F28794227A89ED1F1058BD02E83DFE8E7C,
	SerializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnFreed_m0A789691C06A7855FC6EB40E379B02A3B89F68E7,
	SerializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnClaimed_m65B3AAABCC8B96DFB7D3BF92083CAB9C22BF6385,
	OdinSerializeAttribute__ctor_mFCE6089EC2D42338251875B38D732C44152030CC,
	NULL,
	NULL,
	NULL,
	TwoWaySerializationBinder__ctor_m5F3BB749B2C88E49B8EC404704031865AA9CB105,
	TwoWaySerializationBinder__cctor_m7E071F538C58C102EC5B3F6D28A321781D836D2B,
	SerializationPolicies_TryGetByID_m632039DD424C95823E8B9F8B0FA19617898B2E0A,
	SerializationPolicies_get_Everything_m6E1F040F2A8B465590FB01AED0975790DB0A1B10,
	SerializationPolicies_get_Unity_m6CDEBDE6A0B109C6EFD586B741021A95CF9B69C8,
	SerializationPolicies_get_Strict_mAF701C8A7568E68DBB0CED90A70BE1800599699F,
	SerializationPolicies__cctor_mB76CCDFC84B5F5D7FA15CC082055AD5E39EC1BCC,
	U3CU3Ec__cctor_mEEC330DB547EF30AE7047211A80788EF42E7265C,
	U3CU3Ec__ctor_m81E0C5F4C07ED8E07D4CDFEFA1EE20935DDEF3F0,
	U3CU3Ec_U3Cget_EverythingU3Eb__6_0_mE75E4096BA031B44F19CC2B40A7FC57A133FC3D7,
	U3CU3Ec_U3Cget_StrictU3Eb__10_0_m773E6057F688D26A88CFD3D82BE6EDD224699BC0,
	U3CU3Ec__DisplayClass8_0__ctor_mA6CD2F2904650AED08FD9E18ED43BD24B758340C,
	U3CU3Ec__DisplayClass8_0_U3Cget_UnityU3Eb__0_m675C89EF4B9CB12CCE23F043D944CAD00349439D,
	BooleanSerializer_ReadValue_mCFF038F984F2DA0BFA61AB6EC40B0C574F9AAA2E,
	BooleanSerializer_WriteValue_mE4A25E48982846248623480F4DCBBAE91FD0E42B,
	BooleanSerializer__ctor_m59A6F830DCEAD036D84C0118DFF1FD1FEDC067FB,
	ByteSerializer_ReadValue_m8BF8AF832346C45A52CEC5A307A929EC66736476,
	ByteSerializer_WriteValue_m0832A20094BDEF69D5A1661A4EA432417ABEEFF9,
	ByteSerializer__ctor_m216630A46A05498B8D4ACA1E97C3A46E804B622B,
	CharSerializer_ReadValue_mF4BC0E710952A97A439D2D1AAD8047ADF75BF26C,
	CharSerializer_WriteValue_m6C8FED855EDFE7D629934FEE43BCC1AAAC793554,
	CharSerializer__ctor_m64DC6FEA2E7069E240173F2C21D6AA37E5DF491B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DecimalSerializer_ReadValue_m6322CC7A05AD6B27D3FEC2A13D435E02658BDE16,
	DecimalSerializer_WriteValue_mCB26C51161922165F9A4629D9DD6A5884C091144,
	DecimalSerializer__ctor_mA6C875075A23C54B7513EC59EDBEF419D44ABAFA,
	DoubleSerializer_ReadValue_mDD11B4B7BE7A9581FE67605168A01EBFBDC85B07,
	DoubleSerializer_WriteValue_mB3290324D2E572C9FCEDD08AD2B6377DC9E89B60,
	DoubleSerializer__ctor_m3D0501BF4026EA2513AA219C2604D9E962FD5193,
	NULL,
	NULL,
	NULL,
	NULL,
	GuidSerializer_ReadValue_m3203D1C6AF1AC55EAF62012C95BA91988AA4557E,
	GuidSerializer_WriteValue_mD36492F48494FE195A213187027A7FD8574E6B09,
	GuidSerializer__ctor_mC3750BC7A9DD4484F47DDFBB2F143A833E543DE0,
	Int16Serializer_ReadValue_mCA6DACE37FEFC3AA23C07157A7FC0D59CF72C183,
	Int16Serializer_WriteValue_m6B3C3AB34E379EE6DFA296B6446109B6AFAB7447,
	Int16Serializer__ctor_m429F28A5BD505D9ACCB85A608B620C3A71CA1E38,
	Int32Serializer_ReadValue_mCB2D4CF9E5807E08785C66B947C2E106A2B0E452,
	Int32Serializer_WriteValue_m37D76212ACE61D70E10B49CDF7E0AEA78958644B,
	Int32Serializer__ctor_m9568EBF076A93D4AE5F5B340BDA99D0B47A3F2AF,
	Int64Serializer_ReadValue_m7BF2C0C7F98EEBB6507FA7724B0A9A048025096F,
	Int64Serializer_WriteValue_m54B6142045EBE1D1A21635714BC61B007DBB5F4D,
	Int64Serializer__ctor_mC4B0EF87AE736BFAB7B30239F1B679B3A2E7EFA4,
	IntPtrSerializer_ReadValue_mE850893CD4346CD043ACE6679AF70A4B7A78A895,
	IntPtrSerializer_WriteValue_m51BBA05BF5BF7C3933AF019C42C1DE4D8FA64462,
	IntPtrSerializer__ctor_mBDC0B2692B942809C48B6D0474458A7E190459CA,
	SByteSerializer_ReadValue_m623DC10D88CA711ACB231BB27D9F8914C73C5032,
	SByteSerializer_WriteValue_m0E41081A69450250EF53C3E243486595D7BA7CA3,
	SByteSerializer__ctor_m542885A6614D6AE624E53FC1F9D8ED5244E76522,
	Serializer_FireOnSerializedType_mF5F9E16719D18A6713CFFF4B21E70A85A6DA1FD8,
	Serializer_GetForValue_mEDEC8D49F84F770AC85808176CBADE6E3C7B7233,
	NULL,
	Serializer_Get_m5984DDA5927847C360B93AF249FA4DC87F33B4AA,
	NULL,
	Serializer_WriteValueWeak_m8105FF46594C2DBDB86701AC111CBCA9BD14F452,
	NULL,
	Serializer_Create_m6F9FF6F90E9C6B4F4B2A50F1D90624E6576F2277,
	Serializer_LogAOTError_mD1F886DA6099115E1E4A3803DB921FC37DFC6326,
	Serializer__ctor_m210B2F9B0595AE842C2F8D4871D8558FB803DE2A,
	Serializer__cctor_mF52E0CDF54F46B6AA4F7BE8027D053142A04CB01,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SingleSerializer_ReadValue_mEB4F684B5C5754DF8EB8E4F193E96B82130EE398,
	SingleSerializer_WriteValue_m291730C965CE8DB4400AA12DAE6A153543E9AE33,
	SingleSerializer__ctor_mB208CC340715FBE7B7E972EA8AF587B2E97302D3,
	StringSerializer_ReadValue_m2EAFEBBE510BD8324E9D6566BC5376FDD5C4E857,
	StringSerializer_WriteValue_m88A667FCD7DAD1562ADE2F05C1092D999BBF7FB5,
	StringSerializer__ctor_mCDF166D8A2EF2F2D01411C935A6436213BC1B1EC,
	UInt16Serializer_ReadValue_m492C1F769B663D2CE09FEC1A52F5782265448D74,
	UInt16Serializer_WriteValue_mF8B4E7D4C00444E80772859ED4D8EDF46F2C878B,
	UInt16Serializer__ctor_m6C5BFC0EBFA2D30A86DCBED81B8575A1AB3074BD,
	UInt32Serializer_ReadValue_m47949AF83F5AFFF1650FB7A1F8C8EFA465BF225E,
	UInt32Serializer_WriteValue_m91DB476BBA0831C56E44A3AFB4C1F09E9F93DB1C,
	UInt32Serializer__ctor_mCA7FBF5E0B9D93A322D6691AE93016642642A70C,
	UInt64Serializer_ReadValue_mD42155640F7F4B7300FFDF4F03987D6B1861E584,
	UInt64Serializer_WriteValue_m360649935A4959B67FFB58280EC8595C7E931853,
	UInt64Serializer__ctor_mB3C7FDD0CDA72F5F61F24F67D029DE8E58C69BC1,
	UIntPtrSerializer_ReadValue_m2DA629D86C771D01D15500D863857B4FE21FF779,
	UIntPtrSerializer_WriteValue_mADDA4920D3CEF328438A0CF4B91D68AD8264C5E5,
	UIntPtrSerializer__ctor_m48B82496D8ADBE88AFC2D0FE4E84C91FB0D30C64,
	AnimationCurveFormatter_GetUninitializedObject_m525E9854432B7F629A8B73E92CA1E399ABE61FBD,
	AnimationCurveFormatter_Read_m9741BE4BD3D04B8A51F82E5D8CF1FCF561FCBFBA,
	AnimationCurveFormatter_Write_mD1393081E16F96134CFB31EC47EE28302831D5CC,
	AnimationCurveFormatter__ctor_m5D07E6D2D92E3A6DF2065CBE415936652DAE0034,
	AnimationCurveFormatter__cctor_m194E27568177533AD85BEB4544447F3377B04FB3,
	BoundsFormatter_Read_m00C3AFDF6A3C511F7A1D88B1173FBC15D409743E,
	BoundsFormatter_Write_mAB7D87A96809CFFEEC6E2F81E71C06778D7678D4,
	BoundsFormatter__ctor_m1AB5454445CA1C9460B92D0073838330657C465F,
	BoundsFormatter__cctor_mE153A7E906EA57AD90FA6D387EF57FD055B414BC,
	Color32Formatter_Read_m7E64AA98FB71F629E21381C20B5330553C2D43BB,
	Color32Formatter_Write_m8B5C6F94F8A2EA3FC3F33FF3F09415169E42B752,
	Color32Formatter__ctor_m9E611EC8BF4FEB8D20BF29D102BA2CAF452A1919,
	Color32Formatter__cctor_m4E805C92E26D402C7D8EE8333897EA0031311F1D,
	ColorBlockFormatterLocator_TryGetFormatter_m48935BCC8283E815DD4159FD3C1F7CAAA11B3D6F,
	ColorBlockFormatterLocator__ctor_m4E729FB68563E13EE8F7C826F12C5EFD4E799F68,
	NULL,
	NULL,
	NULL,
	NULL,
	ColorFormatter_Read_mAEC4258330E1B0A418586756F317E0FF267C352C,
	ColorFormatter_Write_m19EE0C800609AE937AB6DAC7622145E36C1CC937,
	ColorFormatter__ctor_m897F856F58996B3C26C8A7FF771EC29C57AA6EFA,
	ColorFormatter__cctor_m3F5BBFDC17997DB5F77232F8D941F52A62A252F8,
	CoroutineFormatter_get_SerializedType_m9640E1AB3F5E14F17CC4BED92D3FE111AEE92452,
	CoroutineFormatter_Sirenix_Serialization_IFormatter_Deserialize_mEDCE95E9AA0402FFE15DD98A826C7E217E54FB48,
	CoroutineFormatter_Deserialize_m60F15A2AAC63249F8C5ECCDCACF3DB9F8C3FA740,
	CoroutineFormatter_Serialize_mA93ADDCA4BBBC9D6A7C03BB1F795159DB96A918E,
	CoroutineFormatter_Serialize_m9E5F320088348A6F472F06B6DB69C9E77BEE6FE8,
	CoroutineFormatter__ctor_m119FC79D807011FC1712E58CA9904800A684B6E1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RegisterDictionaryKeyPathProviderAttribute__ctor_m9305B768AC5434D4CA204CFD90516F302CC003AF,
	Vector2DictionaryKeyPathProvider_get_ProviderID_m3DC672A8F1F038D86ED800C7BA15C846939C30A7,
	Vector2DictionaryKeyPathProvider_Compare_mABF5636048241686EA08A582B561BBFAE1DA63F3,
	Vector2DictionaryKeyPathProvider_GetKeyFromPathString_mEAD126AFE524CB8FC290B8B2B141C38E1F08D3FE,
	Vector2DictionaryKeyPathProvider_GetPathStringFromKey_mF25D8EF5D0168CFDFD1C72752EB9BB2F47EBDBCD,
	Vector2DictionaryKeyPathProvider__ctor_m5A82420688BC8CCDEEF0E85130517DD184C313F8,
	Vector3DictionaryKeyPathProvider_get_ProviderID_m0DB6CFF1A7989883880D50D9B2E20DA49E8EA7BB,
	Vector3DictionaryKeyPathProvider_Compare_mA021862132B657769FD96AF1C7E485D618F2111B,
	Vector3DictionaryKeyPathProvider_GetKeyFromPathString_mA6286F2F6629B1DB3EDC4027FDF8E4F0300A80B5,
	Vector3DictionaryKeyPathProvider_GetPathStringFromKey_m2F22A88959E151F5400C9A714C7C427C60A88C9D,
	Vector3DictionaryKeyPathProvider__ctor_mAA8CFCA3922C075F2FD7571A7B0FF538A04EC629,
	Vector4DictionaryKeyPathProvider_get_ProviderID_m4C79BF90A145122E3D2CC4463AC9130576F8B4FF,
	Vector4DictionaryKeyPathProvider_Compare_mB4E56CB8FCA9F13FB94FF712A24B0ACB1ADBE896,
	Vector4DictionaryKeyPathProvider_GetKeyFromPathString_m004F9E8409351C12CF6158EF04D7A35CD4538A5D,
	Vector4DictionaryKeyPathProvider_GetPathStringFromKey_mD38148EED635D375A9225EEDEAF18F2549D8B2C4,
	Vector4DictionaryKeyPathProvider__ctor_m6D4AD54447176F9E8240E410E54FC355F7AF8C4A,
	GradientAlphaKeyFormatter_Read_mD0BF936362EE76FC6B34C93ED1C5EC59A22A841B,
	GradientAlphaKeyFormatter_Write_mEBC82F0D219DF553411B19F291BDCE704E69E6AF,
	GradientAlphaKeyFormatter__ctor_mA4EEB011E5E52EFB8C6665B23E69EA8FA5CA55E0,
	GradientAlphaKeyFormatter__cctor_mE2363DE32D5374D7B2DD4C5BD71260D411E77524,
	GradientColorKeyFormatter_Read_m0AB0AC316EA565E9C0F33E49D5A6F6C4A6B72EEA,
	GradientColorKeyFormatter_Write_m8053036BFB36FE9A919DA7C9F416294D7061B168,
	GradientColorKeyFormatter__ctor_m87C0E95508392754C2266906F7437EEAF502F9EF,
	GradientColorKeyFormatter__cctor_m09283737ACE3DD2188ECBF2FF3385DE82E9D2790,
	GradientFormatter_GetUninitializedObject_mF856CADFEE72AA1B10477636CA72C9D5D181813F,
	GradientFormatter_Read_m144D89D31C70591DE64B0300E8871C20CE948918,
	GradientFormatter_Write_m3E23FDE8ADBFE1425D9E7107072FB0CD5DB01856,
	GradientFormatter__ctor_mA041A71B226104F39B36869CB8BDB93985DC2F8E,
	GradientFormatter__cctor_m8F1012BE696598C966F96F2815E2B4BDC547615B,
	NULL,
	NULL,
	NULL,
	KeyframeFormatter__cctor_mAE06A892FF00E28D0B3B7753C12B8E58CE0995E1,
	KeyframeFormatter_Read_mEBD133564B2C4005CE4699F2AE7D7DAEA1105B41,
	KeyframeFormatter_Write_mACD1B61C7E79D76CC0783123813C3B13EF48EB81,
	KeyframeFormatter__ctor_m042E7CB0FBBADBB5C62B6E58A76EEDC32FC1E4DC,
	LayerMaskFormatter_Read_mD24F0F8941C06F7DDE9A09874798E24BCEA2AD0C,
	LayerMaskFormatter_Write_mD9E7036A1530FB684A4D4E21B7AFFEBA06F5B19D,
	LayerMaskFormatter__ctor_mF07A9013D7CCC05A966AA1607D3C33AE8320AD50,
	LayerMaskFormatter__cctor_mA29E198988AB9B7CFF00FB70359F6B371543AD68,
	QuaternionFormatter_Read_mC801847AB0F33AD9135CCC2E8AC98B61F6668AE1,
	QuaternionFormatter_Write_m35671ADEE30F278B545FA0636F03E867083C8C70,
	QuaternionFormatter__ctor_m2526DD0118146137E793C08E253124BF835D1496,
	QuaternionFormatter__cctor_m768912C59B54558682D1855D881E882110011D1F,
	RectFormatter_Read_mEC03C8E1722F260536C5F15E92F5E4A3FD2C3D73,
	RectFormatter_Write_m84E93283D29A6F0A99DF0C1DE0216002EDBB5989,
	RectFormatter__ctor_mFB71D3DB3513AF1A460B1C8C4E70A1A2465D9285,
	RectFormatter__cctor_mCF1D4C81036A527C3203208CD077F6E36DE5D003,
	SerializationData_get_HasEditorData_m3537300781FF61083E5118C85AD08EF75BCBD4A6,
	SerializationData_get_ContainsData_mD670EBA3B480BEE760C62ACBB6DF66AFC6BF5A43,
	SerializationData_Reset_m1EB15EF81D0E03A10D51227A18311DEAEDCB66AE,
	NULL,
	NULL,
	UnityReferenceResolver__ctor_m96CFF7618DA71E09842A1582861B310B6AF55BC5,
	UnityReferenceResolver__ctor_mE922D04EDFCAA20967BD224DA9E92D4A4D8FC2B2,
	UnityReferenceResolver_GetReferencedUnityObjects_mFBA23F7D35BA2509457627F529172BB784DDE8A1,
	UnityReferenceResolver_SetReferencedUnityObjects_mD37E1166E1E3262C75083D20B889E2157E87DD08,
	UnityReferenceResolver_CanReference_m8811ADBD0D79EC84E4781E44465A495662F2393C,
	UnityReferenceResolver_TryResolveReference_m9A914C9A8533FD1A2EA0F2DB393521023E769032,
	UnityReferenceResolver_Reset_m84A632E381742A94635D9D5D81FC2D421A7658DA,
	UnityReferenceResolver_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnFreed_mD2689226BD768A1ADE1B303F2DEC8297D80D5867,
	UnityReferenceResolver_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnClaimed_m282C5CEB61573A4341AA8F8E7DE6BB37392A5F49,
	UnitySerializationInitializer_get_Initialized_mC6FC28109C4DC7C120179A706CC15D151F3FD0DC,
	UnitySerializationInitializer_get_CurrentPlatform_m91D11F98FCACAE94CDD0E37B79111BBC9AD912C4,
	UnitySerializationInitializer_set_CurrentPlatform_m504A8B8DD2DB51664E6A814D5A6031E6FA218CCA,
	UnitySerializationInitializer_Initialize_m4FA3E148819EA9096A88D7316ADAF373813B0FF9,
	UnitySerializationInitializer_InitializeRuntime_mDF40113E7472FEBF6A8C547DFBA347E9359288AD,
	UnitySerializationInitializer__cctor_mEF9BC9DFD965E89343198A9F0441592E33E36EA6,
	Vector2Formatter_Read_mDBBA275FF6E33ECFF001EEB8EE52C98C0648B662,
	Vector2Formatter_Write_mC00A7FA6A8761C6A5234CA456B264C26F1E36FE3,
	Vector2Formatter__ctor_mE3203FB4E145DBE4A03882A546C901C7677F3E1E,
	Vector2Formatter__cctor_mB93E4AEB8030A1CF49223A260ADD4B5AF17AF23B,
	Vector3Formatter_Read_m5DFDF50DF7761B81E3FD617B76DB521249C296A4,
	Vector3Formatter_Write_m859D7F8E5AA3B7F5EBFF88DB03F2796323BDA6CB,
	Vector3Formatter__ctor_mA665AC99E4BAE4F4FD4DE9FD122121E930F02446,
	Vector3Formatter__cctor_mE3D1FA2B4D9E3E0D3E1A377BEB61D52441F706FE,
	Vector4Formatter_Read_m456AEB1375418934EA36143D72D1F8D32B54E48B,
	Vector4Formatter_Write_m21F42BF11DC4AA32ED78C712B1536B71DF11AE5E,
	Vector4Formatter__ctor_m20DC716B3DD67474A2D4D9D3A22473F5C24178C7,
	Vector4Formatter__cctor_m619F0CD14DE7D94A09F6F9C058ACA5B910F34E50,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CachedMemoryStream_get_MemoryStream_mEEB005B5C8369AAEBDB9041B31DE4733AD00289D,
	CachedMemoryStream__ctor_m3A386C9B5A19BB230C763FEADB57772B1A25BAB2,
	CachedMemoryStream_OnFreed_mC2D8FE5DF691E4FBD9A60C0DC70097A26E03CECE,
	CachedMemoryStream_OnClaimed_m4305B99A8642890D6B8191B2081BED877CC38B67,
	CachedMemoryStream_Claim_m47F6BCBDDE71CBDC749ABF3566F6126E1110A14C,
	CachedMemoryStream_Claim_mBD3BCFAADC96B0F511D4C0F49389007F56F78ADB,
	CachedMemoryStream__cctor_m767654013D7B50DE6AD9C49C4DE243510EBCCF8F,
	EmittedAssemblyAttribute__ctor_mE3785C486914F648A23405A5D5DE7357D4D33365,
	FormatterLocator__cctor_m02DEA4CA2F8483F4C403BD0CA28DFDC4BBBA6080,
	FormatterLocator_add_FormatterResolve_m414D138B3CC56647CBA361259E35A50C33F13349,
	FormatterLocator_remove_FormatterResolve_m18543B478624615802C52C884ECA7A7A42C05295,
	NULL,
	FormatterLocator_GetFormatter_m338A613BEB091D64128B42AA43E8D62137E1F140,
	FormatterLocator_LogAOTError_m0A39E0C32E148ABFD22FA5B73CED7A06F79B9121,
	FormatterLocator_GetAllPossibleMissingAOTTypes_m733EE9D38B64CC4E5961D1D70AEDB26024EC846D,
	FormatterLocator_GetAllCompatiblePredefinedFormatters_m369AF54255747ABB8CB0D7CDFDEA872BB604840C,
	FormatterLocator_CreateFormatter_m1576975374699853FCE3DDEEFE7EE5BFFE1C3EDA,
	FormatterLocator_GetFormatterInstance_mDDD157573ECFDB5CD836743BDDF7BDE7AD945498,
	U3CU3Ec__cctor_m9331773D9CDB9978BF0DAB6597C12EE6ED7A30A7,
	U3CU3Ec__ctor_m6802C7C0E5FF87D2EA392BECEEE73A494E028202,
	U3CU3Ec_U3C_cctorU3Eb__7_0_mBD0643739CB95DBB42A7475D4AD68A21B088C574,
	U3CU3Ec_U3C_cctorU3Eb__7_1_m003D06BB02F32859899264FA45EA45DD759B1B76,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14__ctor_m6807DCF6D5F514E393A3506E51F9F6D6BF2B5743,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_IDisposable_Dispose_m1CC5D5B263630965C2576ECB8BFEC3C078D61C3E,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_MoveNext_m5FD817C61E20F5B925B0D8411E4FAC10593F08DF,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_U3CU3Em__Finally1_mF8F20E11D7C04E385F96B9FC218627524F833985,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mC70BDEC8AED72FE57AD10B247740100D053B5B6F,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerator_Reset_m3A92AC8AF65A4E57A5249432202ED103D8133C3E,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerator_get_Current_mD42319F2D838D833B0F339C5FB86F5AB4CA21748,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mD6D432ABC0F655548A70757DFA066F3319F95841,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerable_GetEnumerator_mC2A965A639C688AE03EC3A9A6F10751A18DD5536,
	FormatterUtilities__cctor_mDB12F43EC3FE888256B720456223801D95046C54,
	FormatterUtilities_GetSerializableMembersMap_m1E696AEF935F270792C93AF083EAB38CE57D77C5,
	FormatterUtilities_GetSerializableMembers_m40BB77B1C95F5E69E3FB90BD2756BBEE41AF01B7,
	FormatterUtilities_CreateUnityNull_m9F29E00B1EA9E567C8522AA1CBAEBE7BB933804E,
	FormatterUtilities_IsPrimitiveType_m4FA0D9043862BF0E6950C5DE9D00EF52C4E4081B,
	FormatterUtilities_IsPrimitiveArrayType_m9021194370421E39B818583E6A0511428A9078BD,
	FormatterUtilities_GetContainedType_m7EA67A5D85713F168735A082F6566804ABACF1B9,
	FormatterUtilities_GetMemberValue_mA90CA20908489F1BFDFDCD5D5ED63E972EA1A613,
	FormatterUtilities_SetMemberValue_mCD5FF6CA0CBEBFF70B61D928DB9A63F484573B28,
	FormatterUtilities_FindSerializableMembersMap_mEDFF45926C9BBFB049B0E795CEB527656CE7B260,
	FormatterUtilities_FindSerializableMembers_m143780126B793AF62B3181B42EE7B6F7C63C9BFE,
	FormatterUtilities_GetPrivateMemberAlias_mDB92E479FB82F6C4719AC9DDBA426A36F6820AC3,
	FormatterUtilities_MemberIsPrivate_mC306E58947FF2CC8F21CD9D282AA32374B9654BD,
	U3CU3Ec__cctor_m61F914E8F3DB991A8E2F216DAEA33663E412A1E4,
	U3CU3Ec__ctor_mD865791C90A7779DF2AAD941336DF98AC0518A78,
	U3CU3Ec_U3CFindSerializableMembersMapU3Eb__15_0_m33DDB023F41E24FC50DFE11F115C3B89FD97E286,
	U3CU3Ec_U3CFindSerializableMembersMapU3Eb__15_1_mC523774053B664DDA27322F52EC128CB26209D3B,
	U3CU3Ec_U3CFindSerializableMembersU3Eb__16_0_mBCC04EEC55914DD51232DF297E11BE6588CBDB12,
	U3CU3Ec__DisplayClass16_0__ctor_mAB757C57286F8CFAFD96545A19A1186DEE5691D4,
	U3CU3Ec__DisplayClass16_0_U3CFindSerializableMembersU3Eb__1_m51DC274C5C43DC45E9C2182E55489FC9775A4550,
	DictionaryKeyUtility__cctor_mA35EA90D57472B02449954604FC49A3A80D6EC2A,
	DictionaryKeyUtility_LogInvalidKeyPathProvider_mC277D3439F7EBBEC355FFCC588DA6C68CE7A17D2,
	DictionaryKeyUtility_GetPersistentPathKeyTypes_m1949DEFD97F131ADC78687A89FDA99D68E22245F,
	DictionaryKeyUtility_KeyTypeSupportsPersistentPaths_mF56254ABC57F592365B594C8F055E4C11896EB93,
	DictionaryKeyUtility_PrivateIsSupportedDictionaryKeyType_mDB414E1CD23DF020247E1A6FFC5526C96BFCE916,
	DictionaryKeyUtility_GetDictionaryKeyString_m14828B8BBB98514B4226DC387E233653CA3CF69B,
	DictionaryKeyUtility_GetDictionaryKeyValue_m45ED2B7124DDACA89029053757F9614ED32E5520,
	DictionaryKeyUtility_FromTo_m8989BC8F41FD5496ECD27DFE6E4DB9C1A5AF940F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass12_0__ctor_m8266A84F31C76A24BB6571F023C392842CE83AFB,
	U3CU3Ec__DisplayClass12_0_U3C_cctorU3Eb__1_mC6A75E1F2346E6C4603D4BE10A63AC1A6B9FC9EB,
	U3CU3Ec__cctor_m64D0412C8A1A4CE8EE4EE89E34D62765D0E174FE,
	U3CU3Ec__ctor_m65FC08EA6EB8495388811B0E6FF94259225C4F64,
	U3CU3Ec_U3C_cctorU3Eb__12_0_mA5351559363892F7896914FD8E729AEE043EDBC0,
	U3CU3Ec_U3C_cctorU3Eb__12_2_m9BDC90D57B42081F01F1E35D23663D3EC7547785,
	U3CGetPersistentPathKeyTypesU3Ed__14__ctor_m36A88212D2E381607C9DAF12BA928B22E12A0276,
	U3CGetPersistentPathKeyTypesU3Ed__14_System_IDisposable_Dispose_m3823A29402B6AFD21BF4006684F0546FFEBC5685,
	U3CGetPersistentPathKeyTypesU3Ed__14_MoveNext_m2635EAB28F5967C981D7E8DC267179603ACCBBF0,
	U3CGetPersistentPathKeyTypesU3Ed__14_U3CU3Em__Finally1_m9F6BB8ADDE2BA7CF1F3BFFB56E764DE5753BF21C,
	U3CGetPersistentPathKeyTypesU3Ed__14_U3CU3Em__Finally2_m71E6CAE2E07311D401168666DE8082BF8D449AD5,
	U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m0CC16A93FD09854E6C5CEE628CE8F2EA20154AD6,
	U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerator_Reset_mB933F49AAB164AF50F5B689943724D5F72B9E3A2,
	U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerator_get_Current_m1B6827FE5C3E31BF5E1D6D857FA0F9C08190BB5A,
	U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m9A8C7C8E62BDF4CB19FDE73627722736FCE49703,
	U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m8B8245CD9F99E186DDA436899290EAFE588501E7,
	PrefabModification_Apply_m67930A3515963FB738E9F50B43745519B411E193,
	PrefabModification_ApplyValue_m2C1787B6D7B4850463149784AD56765D2AAF9409,
	PrefabModification_ApplyListLength_m975A69A837E43386FB0DBBF684F9A06963CC1866,
	PrefabModification_ApplyDictionaryModifications_m322227F7D93FFEDA04554E547164C2E16FEC25D6,
	PrefabModification_ReplaceAllReferencesInGraph_m3D598C659793B83D7DA10B51BD048E61601622C2,
	PrefabModification_GetInstanceFromPath_mBBF07DDDEF9B2857ECB54213DAAF300D1E8E3272,
	PrefabModification_GetInstanceOfStep_m90982733BF040D73488C7E4FD096E72BF8E13015,
	PrefabModification_SetInstanceToPath_m8BAAFB4A2ACBB9E235CD595177BF3DEDDEB8349D,
	PrefabModification_SetInstanceToPath_m98B1BFF34F54DC105CF772516D3DDBE2E23552F5,
	PrefabModification_TrySetInstanceOfStep_m76280F198FA78FC8DE0F1041ABC80EF18AEC3908,
	PrefabModification__ctor_m79DAB9ACAF0C4163917E18139D23803DB8A9DF09,
	U3CU3Ec__cctor_m354A95D6D8FFB77ADDCA97A163D68D56D5E1D69A,
	U3CU3Ec__ctor_m37883E9759EBADFADC27B0D9546D5149C4E25D37,
	U3CU3Ec_U3CGetInstanceOfStepU3Eb__13_0_mF537EA24FE5824031718B1E18FEE6651ABFDD8F3,
	U3CU3Ec_U3CTrySetInstanceOfStepU3Eb__16_0_mE43342852FEF829FB9E7EDD397F4F960E54480C3,
	ProperBitConverter_CreateByteToHexLookup_m414CCD800CE54B801F4A0E1B400B5A2095FFA31B,
	ProperBitConverter_BytesToHexString_mB7B0C30A61D3EF5E317BBC2A6C3D2A658F3A6302,
	ProperBitConverter_HexStringToBytes_mE48E14EC3C6068D80A903FF7FAE79D8F0E049AED,
	ProperBitConverter_ToInt16_m74C69878863F5561B43FCA9A6051C6A3F5B04D71,
	ProperBitConverter_ToUInt16_mAA5F544ECEE8BA577729309719737227505A5BA8,
	ProperBitConverter_ToInt32_m321C96A3E7D15DC43FF9AAAFD8D704D818D06645,
	ProperBitConverter_ToUInt32_m6D91CF4AA9AC8190264BC49402E36279454A21AA,
	ProperBitConverter_ToInt64_m757A85166D887059C89D2D3E7B373A96C012A620,
	ProperBitConverter_ToUInt64_m379C28960F68204086D71DDF18E9AE6202104134,
	ProperBitConverter_ToSingle_mADFCA0C670800E6C70A3C4C708E7EB792930C5E6,
	ProperBitConverter_ToDouble_mC96B07516E93BBADAC260B531DC5F6AF60F2E9D1,
	ProperBitConverter_ToDecimal_m533993F2DE9AC84867BA9C856AE13D7F19A98DF3,
	ProperBitConverter_ToGuid_m636E872E3100CCF26A92A2EAB6EF82E123178AF7,
	ProperBitConverter_GetBytes_m2140B571B96FBC227AAA36297CB5D4E81B7582B4,
	ProperBitConverter_GetBytes_mC2BB412AA9804081FFED7A1CBB93A0EC0F393B1C,
	ProperBitConverter_GetBytes_m3C74377A2FA7EC2D14474D235407C7038FADDCF6,
	ProperBitConverter_GetBytes_m4CCD7BFCC98DC4E95314828C1BCC36C3FBF6320A,
	ProperBitConverter_GetBytes_m6CCB53D54E3FB5A2AAE9ACB4D596E841DBCC17C1,
	ProperBitConverter_GetBytes_mD3F9A4E496E67B014CCAD7F4A7D091083493B175,
	ProperBitConverter_GetBytes_mCD50D385DB302DEAD21AC084FFD4008E6374A261,
	ProperBitConverter_GetBytes_mF0B7BE416F3D35B9BF0EAA9B0EB6A49FDB43B75D,
	ProperBitConverter_GetBytes_m1F2855E87476C1D4304B5AC1590DDA5DD14EBEF5,
	ProperBitConverter_GetBytes_m318EC6D9C8155E1306F14C4F1E11DD05D8FFD2F3,
	ProperBitConverter__cctor_mF05F0DEEEA105549206120C109A3C8B35E874CD0,
	SerializationUtility_CreateWriter_m02E9DBDA55D4D54CF44FE731032106E92ED982C0,
	SerializationUtility_CreateReader_m4461B6525F1F47656377F900531544BFA4B9DC6F,
	SerializationUtility_GetCachedWriter_m99185D13CA8B42F0EDEA7FAA57A5109161632971,
	SerializationUtility_GetCachedReader_m6A1F7B68F51637F42C5F2AF1D218413FD057A2EA,
	SerializationUtility_SerializeValueWeak_mF02C288CF93016F141D308687C503E6E1BFC5AEA,
	SerializationUtility_SerializeValueWeak_m87595E3087D81EF94E2CDCEBC663D93B04F0CFBA,
	NULL,
	NULL,
	SerializationUtility_SerializeValueWeak_m15C5C6DD7B0C7F75B91131551B3ADF997EDE69C0,
	SerializationUtility_SerializeValueWeak_mD8D00A51EE11993198668D3745C6D23C928830FE,
	NULL,
	NULL,
	SerializationUtility_SerializeValueWeak_m0ED19276C9409DD002E261891DA23A3B1308CD5E,
	SerializationUtility_SerializeValueWeak_mAC921280E17D11713CBBF195635E28011526B9B4,
	NULL,
	NULL,
	SerializationUtility_DeserializeValueWeak_mA53CD489DF03F94AF43739B7196C7062EE20D8E0,
	SerializationUtility_DeserializeValueWeak_mA3DD666CB0E4F0164D278B729996FD46697C190E,
	NULL,
	NULL,
	SerializationUtility_DeserializeValueWeak_m65B4D231C6C0A5DF9DCF5C38AB4B112C6BD6F7FF,
	SerializationUtility_DeserializeValueWeak_m8B6EFF2B51D5D4E7EC944135D241B5E89D534637,
	NULL,
	NULL,
	SerializationUtility_DeserializeValueWeak_mC446ABEDB28C13124A93700D8F842C8FC27CBC40,
	SerializationUtility_DeserializeValueWeak_m54CC756C9343A42C6AB60FDD756B5484620617FD,
	NULL,
	NULL,
	SerializationUtility_CreateCopy_mBDA87EA78EC963F1A2D28560AC9DD0E09E23D653,
	UnitySerializationUtility_OdinWillSerialize_m4A038ED0300BE52C49C4254D74E8EC46D4B16007,
	UnitySerializationUtility_CalculateOdinWillSerialize_mDB549CCFCDCC8E71879C8B73256BFDB454439506,
	UnitySerializationUtility_GuessIfUnityWillSerialize_m46AD7B18F1835DB02AE64CF7C85E4E1C2C3E7FE6,
	UnitySerializationUtility_GuessIfUnityWillSerializePrivate_mDA886C136BEB13AF5BF526C66BD397DDE1D33458,
	UnitySerializationUtility_GuessIfUnityWillSerialize_mB5FCC469C4C54A79438E8127D6A69F86EAE1D2C0,
	UnitySerializationUtility_GuessIfUnityWillSerializePrivate_m86AC799B7E3A0AB4AD836721F225079932BF45F2,
	UnitySerializationUtility_SerializeUnityObject_m9C6AF6F004397642A019B2BBB602609C720D33CF,
	UnitySerializationUtility_SerializeUnityObject_m49DF881F3AC881AFCA536828C3AE05FFAC3C4FC1,
	UnitySerializationUtility_SerializeUnityObject_m83356366B48DC0D9FEFD45CE574CDBB3C944A7E6,
	UnitySerializationUtility_SerializeUnityObject_mD6FF50D900D7892B3824FA907B6DD7768796681D,
	UnitySerializationUtility_DeserializeUnityObject_m3ACBB8BF2373E5BBCAD6573C5DC78A2AE4D58DCD,
	UnitySerializationUtility_DeserializeUnityObject_m2A1249FD954C11A4E9FF6016F493147B73EEE7EC,
	UnitySerializationUtility_DeserializeUnityObject_m0C402CCBC6C46AFE969621CA8C1F9CF27E53AB14,
	UnitySerializationUtility_DeserializeUnityObject_mDE3EA623012D7C068E5081F66FF9B2122B01E170,
	UnitySerializationUtility_DeserializeUnityObject_m1B0EB4E3A774362CB0B19201CC535D7BD47FF450,
	UnitySerializationUtility_SerializePrefabModifications_m1A538BA7C479B86505D30DD35627E4C6002242D5,
	UnitySerializationUtility_GetStringFromStreamAndReset_mCEB869F3E6138858841D8005E2BA3DD81BEDD3FE,
	UnitySerializationUtility_DeserializePrefabModifications_m9A7AB7D00B9C0800377CDB6B7AA3539FCD207DBE,
	UnitySerializationUtility_CreateDefaultUnityInitializedObject_m262FC947FCEFBD63C6BF5B036C39439885DD9977,
	UnitySerializationUtility_CreateDefaultUnityInitializedObject_m47CE3AF7A3AAB732C6B38DDA8A01A80E6BC39769,
	UnitySerializationUtility_ApplyPrefabModifications_m30545B67AC7DDA5859C1929CA1BA6857C8932065,
	UnitySerializationUtility_GetCachedUnityMemberGetter_m4173D15DCD31A2494C881FCA53B0C11B0481AD5C,
	UnitySerializationUtility_GetCachedUnityMemberSetter_mE06FFD6B82AC1EEDB2AFDDDA56101C71D28E2D19,
	UnitySerializationUtility_GetCachedUnityWriter_m3CDF3E157B25F699B5BB9896DF02AD28ADC7DE2E,
	UnitySerializationUtility_GetCachedUnityReader_m103B83E012DECCDB6CAC3BD232AC576A0F2F686C,
	UnitySerializationUtility__cctor_mDB051BCFD1F4D9D479518AF2B14698444E1A2F57,
	U3CU3Ec__cctor_m473EE8642574288A98B20E012EDD063513EBCA28,
	U3CU3Ec__ctor_m4332BC882242005B731F8159A9EB53832F250A43,
	U3CU3Ec_U3CSerializePrefabModificationsU3Eb__32_0_m8FFB91C25A6CC17B806EBF10ADE7472649DF4816,
	U3CU3Ec__DisplayClass38_0__ctor_m49AFF3A3A2BCCB0F88CF8A8371BF9A70304ACD4A,
	U3CU3Ec__DisplayClass38_0_U3CGetCachedUnityMemberGetterU3Eb__0_m8EFE2B2DC02F54952344C4B7099BA819AB4490FB,
	U3CU3Ec__DisplayClass39_0__ctor_m6FECBB79E042E8C6A1DB5CCD5821981BADF9D4E3,
	U3CU3Ec__DisplayClass39_0_U3CGetCachedUnityMemberSetterU3Eb__0_m5F7874E79F1AB894016EE8EFEECE00C427BF3F0B,
	FieldInfoExtensions_IsAliasField_m398CAF3DB1F9E890A1FFA30A3FE5BAB20935A071,
	FieldInfoExtensions_DeAliasField_m7CA6F3403FD5D2639556A9691DA441B81B41F639,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MemberInfoExtensions_GetAttributes_mD782A90C748F653B02C2BCC8C7F8F8ADB6B65D70,
	MemberInfoExtensions_GetAttributes_mA2399E26C93BD5E3D7A4F9D3955F20732203D62A,
	MemberInfoExtensions_GetNiceName_m5A9A8D891272D074286741039D4B149EEBCE63F3,
	MemberInfoExtensions_IsStatic_m92C8266D7E99D8FCFD5B2D130B6D612F6D746A44,
	MemberInfoExtensions_IsAlias_m1EE81196457E6519345454055B6AB352C8B13C97,
	MemberInfoExtensions_DeAlias_mB33E9231B63A85EDB7FB78767DC9241CA3C33853,
	MethodInfoExtensions_GetFullName_m4D4D2E1FF1C1D1F0C6FFEE3AF6F4B648129AEEE1,
	MethodInfoExtensions_GetParamsNames_mC382256243DD039BF9AE133EE40A39ACF7524CA5,
	MethodInfoExtensions_GetFullName_mA2252143DBADBCD585950C1C2E62F948D2674CDC,
	MethodInfoExtensions_IsExtensionMethod_m3FACB2B5F24DCE49515887532D8232A4886B618D,
	MethodInfoExtensions_IsAliasMethod_m20B3034326AA3F95D9793F423E2825434E8AD94F,
	MethodInfoExtensions_DeAliasMethod_m7B9DCEACE52AD2D8E8E3BD0D166D6CD253707811,
	PathUtilities_HasSubDirectory_mBE6C0DDF57CEB2E0D21BEAE9BCA72BCB07DC8C2B,
	PropertyInfoExtensions_IsAutoProperty_mA7690BC6F8BFB91FB4D32626F9B7244ED6B9DBB9,
	PropertyInfoExtensions_IsAliasProperty_m9B5A46F946052C275D8BFFC0637B94116B0A16E6,
	PropertyInfoExtensions_DeAliasProperty_mF26930F3FDFA4897D50A07B9B55B3021FABA23C7,
	StringExtensions_ToTitleCase_m20933A3645B0BA1CBE7261E1B3C43C04A79FE02E,
	StringExtensions_IsNullOrWhitespace_m2CF893DEBA74662B72373457906014BB8926FE58,
	TypeExtensions_GetCachedNiceName_m82A235935C043929775365C519625DDFC988F2CC,
	TypeExtensions_CreateNiceName_m9AC8F0E50037A12D5BBD4E26DEFCA2D215A1DD6B,
	TypeExtensions_HasCastDefined_m9290DC6C1ECD292809874ECB8EA128441BAA2E32,
	TypeExtensions_IsValidIdentifier_mDF93ACD96CFCC5F971CE59E8A79CDCD20655496E,
	TypeExtensions_IsValidIdentifierStartCharacter_mF722890817B7B4F4446651E0BF5965077AE650F0,
	TypeExtensions_IsValidIdentifierPartCharacter_mCA07EF328158AB02B2E2680A67F4AD0C0150B2BE,
	TypeExtensions_IsCastableTo_m578BBAD72A62999057382CD59FDD1F72C1643C7C,
	TypeExtensions_GetCastMethodDelegate_m9AF21C776CA8046D583719E67039416332715A17,
	NULL,
	TypeExtensions_GetCastMethod_m46043887A5FDC4603156BA596FF58E33B861649F,
	TypeExtensions_FloatEqualityComparer_m45336611D9A63B1F8BF6BB9126A799DE7D610E8D,
	TypeExtensions_DoubleEqualityComparer_mA261793CDB4CF0F66BD11B3F8086C94366EB37AC,
	TypeExtensions_QuaternionEqualityComparer_mCF439E2E37EE43EFFB34A52908224A546DBEA1F4,
	NULL,
	NULL,
	TypeExtensions_ImplementsOrInherits_mA27D78BEA6AF536B9C208AFA20B871F4487A5605,
	TypeExtensions_ImplementsOpenGenericType_m89D32D68B8220791B5BB9411CFF4E024DDB72A59,
	TypeExtensions_ImplementsOpenGenericInterface_m93141BD0F34B0489021F6CACBB99D3D706242C87,
	TypeExtensions_ImplementsOpenGenericClass_mD1FE91E2B27B1F206F8D809DE537CC6843396D9C,
	TypeExtensions_GetArgumentsOfInheritedOpenGenericType_mA79B7D3DBE1C400FEB480AAC05363A64DE161D17,
	TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m8D1A976AF561C4FF081B0EE6EBF16D1C9BDDB8E2,
	TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_mCCD3EA914DC8AF10F2DED841DB3415B52636E4DA,
	TypeExtensions_GetOperatorMethod_mFDDE0C5788E9BA3152B4D7E5BACF5FB736D22D33,
	TypeExtensions_GetOperatorMethod_m2771538F157766F9B357B49289BA061EF5589CB1,
	TypeExtensions_GetOperatorMethods_m3D42793AD55F198485B224B736D29C878E8E4A83,
	TypeExtensions_GetAllMembers_mEAD8CCA6713BC546C11E6E2383D61EADCF71FD6A,
	TypeExtensions_GetAllMembers_m4FA62093A39775CC258CB5C15B01D837C5E9098B,
	NULL,
	TypeExtensions_GetGenericBaseType_m73E3AC661E81FE4361756D30AE9E328BBF785A5E,
	TypeExtensions_GetGenericBaseType_m92108E5F1AD7932F658A825AF8FD1879B5EDFCC3,
	TypeExtensions_GetBaseTypes_mCA60FDCB5B2C0300B38E5EA00FEA4113494C3EE0,
	TypeExtensions_GetBaseClasses_m52D077E43D5871509D852118A0618042B0948FF5,
	TypeExtensions_TypeNameGauntlet_mC9DE40F790CCF1B330A29F7447611826F403084F,
	TypeExtensions_GetNiceName_m0F37B9149A1B7694D3A62BDDF7D0635F8F67C043,
	TypeExtensions_GetNiceFullName_mB795756486D1EE049BEA95070A0852377C3A4408,
	TypeExtensions_GetCompilableNiceName_m6837178F74C38B1D0D32061D16BC35EE83E2F372,
	TypeExtensions_GetCompilableNiceFullName_m58AE95F6613A819AA9C78DFE0853F3196A8BE297,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TypeExtensions_InheritsFrom_mD57A7F3D0E3302A4656C0EE35E6F6756B16EE642,
	TypeExtensions_GetInheritanceDistance_mB824EE440D2D786FCC73EF32CD8243330C51C6D0,
	TypeExtensions_HasParamaters_mA5311BE6CD51941C75856CCA8DC4612366A22764,
	TypeExtensions_GetReturnType_mBE8E92DD2B796A26EF66672BACF12360DF2F8F54,
	TypeExtensions_GetMemberValue_mD898651002D3A08963505D71EE607A2070D63304,
	TypeExtensions_SetMemberValue_m5AF88E6BEF682F16962EE82D8E475A9890FDA6EB,
	TypeExtensions_TryInferGenericParameters_mBF16A731F12A3E15EE42C62834FAD8DB78002289,
	TypeExtensions_AreGenericConstraintsSatisfiedBy_m895118386C70D74B54F52077842FB62C94A636FD,
	TypeExtensions_AreGenericConstraintsSatisfiedBy_m2E3250146F9DD571BBD78C647FAC3F6F80627A8A,
	TypeExtensions_AreGenericConstraintsSatisfiedBy_mC5A3F144D69B782E27CE8770DBB3F253C11C4E6B,
	TypeExtensions_GenericParameterIsFulfilledBy_m1269590C68DE54AEC1CA8D4916A63A179EB1CA72,
	TypeExtensions_GenericParameterIsFulfilledBy_mC3CF07D7A2677E6500F03BD9FDB6F0E0EB3076C7,
	TypeExtensions_GetGenericConstraintsString_mDB86975B4C78AB81853D4059387155CD13F73BB8,
	TypeExtensions_GetGenericParameterConstraintsString_mDA43C53D8EC51F87FD7FDC72DBFA00B9023C3A77,
	TypeExtensions_GenericArgumentsContainsTypes_m511CA2AF8361F772B728F3387E50A1FC93920CA8,
	TypeExtensions_IsFullyConstructedGenericType_mA4E15B1EC4449FE12E465946483056996224C395,
	TypeExtensions_IsNullableType_m6E53FF543018A73E0A7797301B6D6F411EDFF52B,
	TypeExtensions_GetEnumBitmask_mC90AC3CB163AB1C4BE88CD1F0E26CE6F3BFB63ED,
	TypeExtensions_SafeGetTypes_m0110261E78C023188AC3BF63911A87F5B5FC2EAE,
	TypeExtensions_SafeIsDefined_m03307A35EFB78B55E14F3267F6FE43E17EDFBD91,
	TypeExtensions_SafeGetCustomAttributes_m311A2826BB7E8A98F02058C6768DDD12E177B8C5,
	TypeExtensions__cctor_m0406A5B6145F5C7E1E6C90F8D19FE02B549E973D,
	U3CU3Ec__DisplayClass29_0__ctor_m5725234EB17E7A1B0F7FEA0BD4C9A8561DCC80C4,
	U3CU3Ec__DisplayClass29_0_U3CGetCastMethodDelegateU3Eb__0_m50BB07BE75B22C4AF6CDDB3E5C9A89C6CA95C17A,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass45_0__ctor_mA4E51B9D41F12F9010B94C276629B47E9A1C00FC,
	U3CU3Ec__DisplayClass45_0_U3CGetOperatorMethodU3Eb__0_m8CD094F2EBD9AE3244478840554A7B7B9F107F7D,
	U3CU3Ec__DisplayClass46_0__ctor_mB08963945B06193B1452AAC860BBDCBD0C3A61C5,
	U3CU3Ec__DisplayClass46_0_U3CGetOperatorMethodsU3Eb__0_m58F8B033B7596D63ED4ADFEFE229ADF6F8D899E8,
	U3CGetAllMembersU3Ed__47__ctor_m6531D2B00F68C79AA5B748966E5EC7289167E288,
	U3CGetAllMembersU3Ed__47_System_IDisposable_Dispose_m84F1AE700946CF7DCE27BB1DE2E20508A428BE39,
	U3CGetAllMembersU3Ed__47_MoveNext_m12FD74881ACB9997B63F23692F5B0E464AA2D056,
	U3CGetAllMembersU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mE1F39B24C11A8333C232F485FA128DADE03A5A67,
	U3CGetAllMembersU3Ed__47_System_Collections_IEnumerator_Reset_mF10576626E8566133DC612EDD7C3063AB993D436,
	U3CGetAllMembersU3Ed__47_System_Collections_IEnumerator_get_Current_m0618AFC2F349BA109D0D2432FDEA0424F4A71FCF,
	U3CGetAllMembersU3Ed__47_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m80CFB19D9D24F07401075AFD664276D4ED4449CA,
	U3CGetAllMembersU3Ed__47_System_Collections_IEnumerable_GetEnumerator_mDEF3A2F8A3C862A2CB7313470ACC3F44634D697B,
	U3CGetAllMembersU3Ed__48__ctor_m422B5C64AF72E942DD56389F2DAF729CC420E47A,
	U3CGetAllMembersU3Ed__48_System_IDisposable_Dispose_m702840A883EFCBA609C1B1E5BDB86479F602F45E,
	U3CGetAllMembersU3Ed__48_MoveNext_m8CE8854D948FD04CEDB1F17C7A48AEE64A066D2C,
	U3CGetAllMembersU3Ed__48_U3CU3Em__Finally1_m4F8ECB7E0E5714796F99857A857AD87D0EBE3E24,
	U3CGetAllMembersU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_m3E73B1020BAF18C3D76ECCC12AF3B863E9917F0E,
	U3CGetAllMembersU3Ed__48_System_Collections_IEnumerator_Reset_m476DA9DD313D5C435E3721F4927E17C7056CBCC6,
	U3CGetAllMembersU3Ed__48_System_Collections_IEnumerator_get_Current_m2042F1683D4BB87B88A0F443FE5012246CFC01D2,
	U3CGetAllMembersU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mBB79179C4C2A31932B3D9890A6190718FC72C66B,
	U3CGetAllMembersU3Ed__48_System_Collections_IEnumerable_GetEnumerator_m36EC0B235F02798C10CADFA0956D30F62127126A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CGetBaseClassesU3Ed__53__ctor_m211FCC3DF22F2FF83E815B2CBB79F0478CE4732E,
	U3CGetBaseClassesU3Ed__53_System_IDisposable_Dispose_m388B361D0B4D9AB71B2146615E3DA09C35E4A555,
	U3CGetBaseClassesU3Ed__53_MoveNext_m479481B59F4C34E99FF8EB9C2BCD2491BB6D6086,
	U3CGetBaseClassesU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m3890F0F6E07922C7EB5BA1ACC451FFB4AA7F79C4,
	U3CGetBaseClassesU3Ed__53_System_Collections_IEnumerator_Reset_mC0351B85DF82DCFD1A2A1CEA5954E779AF0875A3,
	U3CGetBaseClassesU3Ed__53_System_Collections_IEnumerator_get_Current_mD6019B72E79537D9ED00C3AABFA490742F97A5F0,
	U3CGetBaseClassesU3Ed__53_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m22890AC370D6A36EDFA6E91FB83E849B8BC14D32,
	U3CGetBaseClassesU3Ed__53_System_Collections_IEnumerable_GetEnumerator_m7D793D39D20CCD6D3EA70D634B479C7235F1FFC7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UnityExtensions__cctor_mD3734DA22993FCD933A760EA358613C2052DA334,
	UnityExtensions_SafeIsUnityNull_m4B3E5197918704AD095118BD47D4B3A469F36206,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WeakValueGetter__ctor_mED35E1C9DA471A28679F1DD95CCBEB769A539E84,
	WeakValueGetter_Invoke_m8342B7DF2EE6049432E1A9199DC3CEB33320FA83,
	WeakValueGetter_BeginInvoke_m41BCE16EE21ECFA0E28251F081254795DD324A9A,
	WeakValueGetter_EndInvoke_m91011C4C6096043412A75B127178EBFC0959D1E3,
	WeakValueSetter__ctor_m96E4D0BD6AAE23BD5D52C8D76006D78012E8F695,
	WeakValueSetter_Invoke_m68AAA32E6793D8F1F66D87C22689403D855D301B,
	WeakValueSetter_BeginInvoke_mBC6F95756ECDB9E3A55B27C2500B498C1EF0FD1A,
	WeakValueSetter_EndInvoke_m7E534F1E2FDA67F99F05E10A50834A157CED6945,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmitUtilities_get_CanEmit_mE5E88AB47D606D83E60D2C439D17C567E7B8D7C1,
	NULL,
	EmitUtilities_CreateWeakStaticFieldGetter_m1FBC3793F5B352A17190E83BBA77364B09DFFBB5,
	NULL,
	EmitUtilities_CreateWeakStaticFieldSetter_mA2979E00C807511E10693A7FB9673F98E4159C32,
	NULL,
	NULL,
	EmitUtilities_CreateWeakInstanceFieldGetter_m8491CE845300E6F6263978A3DB7DD32BA7AC4EC3,
	NULL,
	NULL,
	EmitUtilities_CreateWeakInstanceFieldSetter_mB647498773F4B6ED1E8367C95D3159D2B9C377FB,
	EmitUtilities_CreateWeakInstancePropertyGetter_mD368463A4F8F02E3F9D3BDF03D40E2833FEF54D0,
	EmitUtilities_CreateWeakInstancePropertySetter_m2BAE1F7C9DA5ED3950E1CD6D85A097948C745320,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmitUtilities_CreateStaticMethodCaller_mC73CAF5DC3615D06925F6FB210FD699A2E8B4B8E,
	NULL,
	EmitUtilities_CreateWeakInstanceMethodCaller_m4AF052239514128313E0F222E265CA0DF3F099A8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass3_0__ctor_m838783AF688ECD45977D51F2A8AF7AE1F9E69749,
	U3CU3Ec__DisplayClass3_0_U3CCreateWeakStaticFieldGetterU3Eb__0_m06EECB344D795A9CC1D47516B78E3F25A89ACFA5,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass5_0__ctor_m6A880D2C40F1C7AF5FB0F78BB801433176990121,
	U3CU3Ec__DisplayClass5_0_U3CCreateWeakStaticFieldSetterU3Eb__0_m2DF8BEED8539DB13B11019F5EBD645AB748B8C31,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass8_0__ctor_m8A2AF66B262B31F06E2E8FD45838304BD1B54E86,
	U3CU3Ec__DisplayClass8_0_U3CCreateWeakInstanceFieldGetterU3Eb__0_m882280ED790345234F95E084D09F9298007FF1E8,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass11_0__ctor_m8A77D7B4789EA674FAA55E356ADA0C34EE7B1739,
	U3CU3Ec__DisplayClass11_0_U3CCreateWeakInstanceFieldSetterU3Eb__0_mA6FD749D5382A43C6F1A377DD199B516785CC023,
	U3CU3Ec__DisplayClass12_0__ctor_m8E4F174ACF247B56894E7FD8EF1153CAD8C8E084,
	U3CU3Ec__DisplayClass12_0_U3CCreateWeakInstancePropertyGetterU3Eb__0_mE0C751CCDC79A84F96BE3092040CD1A3B169710B,
	U3CU3Ec__DisplayClass13_0__ctor_mD1019D8F880D140DA98560808B96A1059717BD64,
	U3CU3Ec__DisplayClass13_0_U3CCreateWeakInstancePropertySetterU3Eb__0_m1C5F1429DAAC1E5C4C4EC1BC3B010F91872240DF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass21_0__ctor_mB7C029ADE1E2EF3692B45C131209DC117B967E9C,
	U3CU3Ec__DisplayClass21_0_U3CCreateWeakInstanceMethodCallerU3Eb__0_mBBF2E5E245115CB827C5CCB1A37FB04F78D908ED,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FastTypeComparer_Equals_m903ED7253E35BA0FB495EAB1FC06A1E708E82F81,
	FastTypeComparer_GetHashCode_m540435210864F6134EB0F4B185727F715BC51ED3,
	FastTypeComparer__ctor_m34563B95C215274B6DEBC65F031A380611FB0FC3,
	FastTypeComparer__cctor_m86C79E0B8C3D40DF34CA193099A07BE44C538C75,
	NULL,
	NULL,
	NULL,
	ImmutableList__ctor_mD743802510BCE9DDDE33FD9F8FC5AD2746514197,
	ImmutableList_get_Count_m628D07411950AAEE6E6D5583F1EE551B5F1D17BB,
	ImmutableList_get_IsFixedSize_m9F61791966C93B3C65544EC34A4CBB2EE44914F9,
	ImmutableList_get_IsReadOnly_m20BBA1E91AEAE1A802191F5619E03E82226AF3AD,
	ImmutableList_get_IsSynchronized_m834E1D21975A74A181CBE3B1A5E72EC5BEF3ACD3,
	ImmutableList_get_SyncRoot_m0AD546D5E0EC7F0EE3F5EB6584F7C4FB775136F2,
	ImmutableList_System_Collections_IList_get_Item_m6B45FB5E56FB5EE52A5C000CA84C29BEF0E3306C,
	ImmutableList_System_Collections_IList_set_Item_m76A42F04BB486468E6361AB80AF376608D742B54,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_get_Item_m19D0F17A2C441DB8BBF88CC2F1B2BBD8802A3855,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_set_Item_m8765375F85ED7EB140EC684FEF2416FAD9FBFE32,
	ImmutableList_get_Item_mDCF098FC89391B8B732397CD520C78E616A034F4,
	ImmutableList_Contains_mDA1B5D35309466CAD069A21E33C318AD02ACA95C,
	ImmutableList_CopyTo_mC50AE4852E45353290EA2A81AF93A50CAE24E449,
	ImmutableList_CopyTo_m4CA02FE0FB99548A8A0F08A6B9436D67E6E2E951,
	ImmutableList_GetEnumerator_mD3E794DF87616577C86A240500E6B132FA4D1D7E,
	ImmutableList_System_Collections_IEnumerable_GetEnumerator_m662DB29FC593218F94C1EAA90ACDCF032528CA51,
	ImmutableList_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m0EFC67A88649A75A6B2D870D05E47701D84FC7AB,
	ImmutableList_System_Collections_IList_Add_m8D03593974E2E3A79983D80963CE14E8F3AE2825,
	ImmutableList_System_Collections_IList_Clear_m5BB8383C7EC2592839449E9D6CE301590870EE8A,
	ImmutableList_System_Collections_IList_Insert_mE4A6045B55566525088F5982712C4F3B64B1C6F0,
	ImmutableList_System_Collections_IList_Remove_mCC6A1A58626571F745A1147F94270CF447F58472,
	ImmutableList_System_Collections_IList_RemoveAt_mBEED1926DB081297577919E4F444239DDCC70451,
	ImmutableList_IndexOf_m2562AC49A314908BFEBC64867E939C3A2A2FAAF4,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_RemoveAt_m9E092CF694E15DF8265003C47B0617D447D1F270,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_Insert_mDC057078D75874CC9282B3D5FFD2243D5D81D6A8,
	ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Add_m86DA80C1CC4E832E24EACA5BBFB0CB8C60762AC2,
	ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Clear_m5E4C0AF7DFC93FD954E0552F115D05CE8BA92F69,
	ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Remove_mDCC9028D918A6E9B1A58F29E920BC650A7D2E896,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_m7A4AAD7637598AD3A1B243FA716BBA0F9D833000,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m758A80F54C1DC156B36BE7EE8CB2292F355D992B,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_MoveNext_mDF6DFD9E579394357A93A8AD15A4008F2E93F812,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_m43FDB85F5EE6720959BBB6800664B76C7C6214D2,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9DDA4BE1D2C4B4FC45E2C282DEE63BF2CF2C726B,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m9704C6223E770A6C552F271FCAB0B4DC727EAEA7,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_mBBF0434A75B43F6DA549B64020BBEE78875DAF82,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MemberAliasFieldInfo__ctor_m6291D72D7647EED0ADEBDF495DC8FCF10F5149CE,
	MemberAliasFieldInfo__ctor_m2E88616801E0CFDB50FAE4671D02C9EE3F8ADF84,
	MemberAliasFieldInfo_get_AliasedField_mA4772D6489C7ACEE68E37BDB67352ECEC64A79B3,
	MemberAliasFieldInfo_get_Module_mF5355FCEDAC45D4C859E4DE24FA7D61CC861195F,
	MemberAliasFieldInfo_get_MetadataToken_m4000D9C28F98FA1742DA0A83935FDF80957EEEAC,
	MemberAliasFieldInfo_get_Name_m856D029505EAFB628BC3D587E27356DAF0CA6EC5,
	MemberAliasFieldInfo_get_DeclaringType_m47AC48A006C6701D4887970E1B6C1A5F079CB9C7,
	MemberAliasFieldInfo_get_ReflectedType_m26A376E35082CB80B6538D5C8E1B0A3CD2485B9B,
	MemberAliasFieldInfo_get_FieldType_m34F3717578C06FFCD1C3C52F344C70CCB0F0C2A4,
	MemberAliasFieldInfo_get_FieldHandle_mACCABC3661C6B1D7F28ABA5E1B654F456F19250B,
	MemberAliasFieldInfo_get_Attributes_mFCE2983AFC616969D9C83858876AEEEBEE113B04,
	MemberAliasFieldInfo_GetCustomAttributes_mDD8E2D433080D2951DB69C3C138E2D79B137DB09,
	MemberAliasFieldInfo_GetCustomAttributes_m74D7995F8C8F7B3616FE65CE7C7F54E275DC8CF5,
	MemberAliasFieldInfo_IsDefined_m34BAC252FACDC00E201648E635806AEB4B44790E,
	MemberAliasFieldInfo_GetValue_mB59CD2CAAD3B4C951D76C69278B2330DDDF74562,
	MemberAliasFieldInfo_SetValue_m9A7B6B9766A8DB33EE9A0BD1D22191692E7546DF,
	MemberAliasMethodInfo__ctor_m8ED4DD15C3E7636AE81D47F50D5DDC0BE2813510,
	MemberAliasMethodInfo__ctor_m48690BF27EFD4B4114402B79E66168744AE72811,
	MemberAliasMethodInfo_get_AliasedMethod_m61AC69C4DE8D031731A6D18269488E4DD4A91282,
	MemberAliasMethodInfo_get_ReturnTypeCustomAttributes_m2C5420F7CE569F879A46867378C9E804685A9A75,
	MemberAliasMethodInfo_get_MethodHandle_m68D2C7B066906FE212DF42ABC676AA5CA93D527F,
	MemberAliasMethodInfo_get_Attributes_mE1A827BB0615C29D87ABF61CA56E1AC4337C05D9,
	MemberAliasMethodInfo_get_ReturnType_mF824DA1143979223DCD70FCF0C2A4D0330359CCE,
	MemberAliasMethodInfo_get_DeclaringType_m314935605E5A3F319B914B5153CDBC0215F1DFAA,
	MemberAliasMethodInfo_get_Name_mB6C139E754EA64D21C15F8022988B87ADC633260,
	MemberAliasMethodInfo_get_ReflectedType_m7AFD00991526666FA4817FB2AC658399F85A72FB,
	MemberAliasMethodInfo_GetBaseDefinition_m24BC92DF9CEF1FC90DD504B5666D6632DA65EC44,
	MemberAliasMethodInfo_GetCustomAttributes_m392499E8466489F2BFBFD712F18E1F0023C49F0B,
	MemberAliasMethodInfo_GetCustomAttributes_m07F3B84B3820495AF8C819CFE3A091994850B641,
	MemberAliasMethodInfo_GetMethodImplementationFlags_mDD12F1FB5B23852E38494BDDF606C24DA9B34AC3,
	MemberAliasMethodInfo_GetParameters_m247B3302C93AE5332F015F7967DFE16ADC3B3829,
	MemberAliasMethodInfo_Invoke_mB09584B2C8F0CF2DF0C26352A5F591EDE2376FC9,
	MemberAliasMethodInfo_IsDefined_m003D74B56BFBD7BB654794CE4C8CCA12FB427FDB,
	MemberAliasPropertyInfo__ctor_m31F7966B854F8C787A87DF11EB3B88E74ED01E50,
	MemberAliasPropertyInfo__ctor_mE869B7B6F5441B1CD67E25DBEFDFCD91EF9360C9,
	MemberAliasPropertyInfo_get_AliasedProperty_mA16048158E811314BCE84BE878B0881E9335DE2C,
	MemberAliasPropertyInfo_get_Module_m65699C23616E927727983B3C654CDEBF21EE57DF,
	MemberAliasPropertyInfo_get_MetadataToken_mF28C1911F14B940F2FE9EDDE54FCBBDE612EDD83,
	MemberAliasPropertyInfo_get_Name_m7AA295B5671299B0B86E60A33DF856E1487A152F,
	MemberAliasPropertyInfo_get_DeclaringType_m9A438253C638140709BC19EA27DF0DDC45687E1A,
	MemberAliasPropertyInfo_get_ReflectedType_m29D9DCA59AA68782D8BD2558A4042FF44406DFC8,
	MemberAliasPropertyInfo_get_PropertyType_mAA4FDB53F44E1A4E6E7BC4092CF04249F50AFDDB,
	MemberAliasPropertyInfo_get_Attributes_m7C8C0BC96BBF67562C446ADF141B9D99B46B8375,
	MemberAliasPropertyInfo_get_CanRead_mBC85D29928740B528F42C19654571BFCBBD391C8,
	MemberAliasPropertyInfo_get_CanWrite_mA892B8CE11A2D5CC56DE94BBE70F08F52947B5D0,
	MemberAliasPropertyInfo_GetCustomAttributes_m131D5FEA7052D5E2B1C3F2757D9372A5E5AA4335,
	MemberAliasPropertyInfo_GetCustomAttributes_m9409895E831141A080800D9297680F930CD0A138,
	MemberAliasPropertyInfo_IsDefined_mCA5B764FF246A0A3F7BBD76CB7B232E61898534A,
	MemberAliasPropertyInfo_GetAccessors_mA4361B6D8EB19E3F46DE8C7D15F8321CA72D3716,
	MemberAliasPropertyInfo_GetGetMethod_m904015C20FE5E8597DF0E9AC42DC8448E0E1FE0F,
	MemberAliasPropertyInfo_GetIndexParameters_m1084B3A296CFAEE36FAC8E185C82F75641483AFC,
	MemberAliasPropertyInfo_GetSetMethod_mA3617D52753CD0CF3487E209575161CF776D7AC1,
	MemberAliasPropertyInfo_GetValue_m713D28D16C23B68024157B7E78662462AC8C3037,
	MemberAliasPropertyInfo_SetValue_m7B44101A30899165EB67CEDD485765DE58B1ACEB,
	NULL,
	NULL,
	NULL,
	NULL,
	UnityVersion__cctor_mE278A791E550A2727E733D3EC80E28214004876D,
	UnityVersion_EnsureLoaded_mB8D7004347114DB5DFD4089FAE669C5522592C0F,
	UnityVersion_IsVersionOrGreater_mB00A922D8EFDD10E6ECE391C04C89B697F6A2284,
	NULL,
	NULL,
	NULL,
	NULL,
	UnsafeUtilities_StringFromBytes_m913419982B445F5A2683517541E34030A1361CD3,
	UnsafeUtilities_StringToBytes_m21C9542DB3983BAE84ECD914B20738E95DD964D4,
	UnsafeUtilities_MemoryCopy_m5694365F803140B4CB897EAE26789568396CB6A4,
	UnsafeUtilities_MemoryCopy_m4ECC505D44D7510CA01DE22DF3065126E2CC1FE3,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m272C79CFF409153DFC504A695283A06E8F16FDC8,
};
extern void NodeInfo__ctor_m386DC2E5B9CA08AC916FB9E905B8F8E6C4B0414D_AdjustorThunk (void);
extern void NodeInfo__ctor_m0F7216E0579077DCB8FFEC72B74FBEF04C95A4BF_AdjustorThunk (void);
extern void NodeInfo_Equals_m1B4D737F1DF0003BA4319E5899E8410B46EFB0D3_AdjustorThunk (void);
extern void NodeInfo_GetHashCode_mB34C81EAD31FC0CBF8FDE38F4B7B132ADACF72BA_AdjustorThunk (void);
extern void SerializationData_get_HasEditorData_m3537300781FF61083E5118C85AD08EF75BCBD4A6_AdjustorThunk (void);
extern void SerializationData_get_ContainsData_mD670EBA3B480BEE760C62ACBB6DF66AFC6BF5A43_AdjustorThunk (void);
extern void SerializationData_Reset_m1EB15EF81D0E03A10D51227A18311DEAEDCB66AE_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[7] = 
{
	{ 0x06000329, NodeInfo__ctor_m386DC2E5B9CA08AC916FB9E905B8F8E6C4B0414D_AdjustorThunk },
	{ 0x0600032A, NodeInfo__ctor_m0F7216E0579077DCB8FFEC72B74FBEF04C95A4BF_AdjustorThunk },
	{ 0x0600032D, NodeInfo_Equals_m1B4D737F1DF0003BA4319E5899E8410B46EFB0D3_AdjustorThunk },
	{ 0x0600032E, NodeInfo_GetHashCode_mB34C81EAD31FC0CBF8FDE38F4B7B132ADACF72BA_AdjustorThunk },
	{ 0x0600041A, SerializationData_get_HasEditorData_m3537300781FF61083E5118C85AD08EF75BCBD4A6_AdjustorThunk },
	{ 0x0600041B, SerializationData_get_ContainsData_mD670EBA3B480BEE760C62ACBB6DF66AFC6BF5A43_AdjustorThunk },
	{ 0x0600041C, SerializationData_Reset_m1EB15EF81D0E03A10D51227A18311DEAEDCB66AE_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1758] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2385,
	2386,
	23,
	23,
	23,
	23,
	23,
	2385,
	2386,
	23,
	23,
	23,
	23,
	23,
	2385,
	2386,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	358,
	358,
	23,
	23,
	3,
	164,
	14,
	89,
	2387,
	23,
	2387,
	23,
	9,
	2387,
	2387,
	23,
	2387,
	23,
	14,
	26,
	10,
	32,
	130,
	14,
	26,
	10,
	32,
	130,
	2387,
	23,
	2387,
	23,
	27,
	10,
	10,
	14,
	14,
	26,
	14,
	26,
	852,
	89,
	852,
	89,
	-1,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	89,
	23,
	23,
	23,
	14,
	89,
	89,
	14,
	26,
	89,
	10,
	2388,
	2389,
	107,
	23,
	23,
	26,
	23,
	23,
	23,
	27,
	14,
	26,
	14,
	26,
	23,
	118,
	27,
	26,
	200,
	23,
	-1,
	26,
	130,
	130,
	2390,
	27,
	942,
	27,
	2390,
	459,
	942,
	130,
	171,
	459,
	942,
	130,
	171,
	616,
	943,
	1125,
	459,
	23,
	23,
	14,
	23,
	27,
	23,
	852,
	852,
	852,
	89,
	89,
	-1,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	89,
	852,
	852,
	23,
	14,
	14,
	23,
	23,
	30,
	14,
	23,
	89,
	89,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	30,
	23,
	3,
	3,
	23,
	645,
	141,
	141,
	141,
	23,
	27,
	200,
	118,
	27,
	23,
	23,
	26,
	137,
	137,
	137,
	137,
	137,
	137,
	137,
	137,
	137,
	137,
	137,
	137,
	137,
	137,
	-1,
	459,
	459,
	942,
	616,
	1125,
	2390,
	2390,
	130,
	27,
	130,
	171,
	26,
	130,
	459,
	942,
	943,
	27,
	130,
	171,
	942,
	23,
	14,
	26,
	26,
	23,
	617,
	617,
	617,
	32,
	32,
	337,
	200,
	200,
	338,
	960,
	1615,
	32,
	30,
	3,
	3,
	23,
	2391,
	362,
	362,
	362,
	14,
	26,
	14,
	26,
	89,
	14,
	10,
	10,
	14,
	26,
	14,
	852,
	89,
	852,
	89,
	-1,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	89,
	23,
	23,
	14,
	26,
	14,
	26,
	89,
	14,
	26,
	14,
	23,
	118,
	27,
	26,
	200,
	23,
	-1,
	26,
	130,
	130,
	2390,
	27,
	942,
	27,
	2390,
	459,
	942,
	130,
	171,
	459,
	942,
	130,
	171,
	616,
	943,
	1125,
	459,
	23,
	23,
	27,
	14,
	26,
	23,
	852,
	852,
	89,
	852,
	89,
	-1,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	89,
	23,
	14,
	89,
	89,
	23,
	852,
	238,
	89,
	238,
	10,
	172,
	89,
	238,
	10,
	172,
	732,
	89,
	731,
	463,
	487,
	23,
	147,
	23,
	23,
	118,
	27,
	26,
	200,
	23,
	-1,
	459,
	459,
	942,
	616,
	1125,
	130,
	171,
	26,
	130,
	459,
	942,
	943,
	27,
	2390,
	130,
	171,
	130,
	2390,
	27,
	942,
	23,
	23,
	14,
	27,
	2392,
	26,
	31,
	32,
	26,
	4,
	3,
	14,
	26,
	27,
	23,
	23,
	726,
	2393,
	231,
	511,
	2394,
	238,
	2395,
	238,
	23,
	238,
	3,
	26,
	89,
	14,
	26,
	14,
	26,
	23,
	23,
	852,
	852,
	852,
	89,
	89,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	852,
	89,
	-1,
	852,
	852,
	852,
	852,
	852,
	852,
	14,
	23,
	89,
	89,
	238,
	89,
	238,
	10,
	172,
	89,
	238,
	10,
	172,
	732,
	89,
	731,
	463,
	487,
	14,
	26,
	26,
	14,
	26,
	200,
	118,
	27,
	23,
	23,
	26,
	23,
	459,
	459,
	942,
	616,
	943,
	1125,
	2390,
	27,
	130,
	2390,
	942,
	130,
	171,
	130,
	26,
	-1,
	459,
	27,
	942,
	130,
	171,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	358,
	358,
	23,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	358,
	358,
	23,
	358,
	358,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	-1,
	-1,
	-1,
	1,
	-1,
	-1,
	-1,
	-1,
	216,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	27,
	28,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	358,
	358,
	23,
	358,
	358,
	14,
	23,
	126,
	14,
	89,
	9,
	23,
	23,
	32,
	130,
	27,
	3,
	3,
	49,
	49,
	154,
	105,
	9,
	105,
	105,
	464,
	105,
	2396,
	2397,
	366,
	23,
	3,
	23,
	27,
	23,
	340,
	26,
	2398,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	2258,
	14,
	14,
	26,
	62,
	34,
	34,
	1672,
	28,
	23,
	23,
	23,
	23,
	14,
	26,
	2399,
	803,
	1150,
	803,
	14,
	26,
	803,
	803,
	26,
	26,
	14,
	89,
	9,
	2400,
	31,
	2401,
	2401,
	9,
	10,
	3,
	14,
	26,
	26,
	26,
	27,
	23,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	10,
	32,
	10,
	32,
	26,
	26,
	26,
	23,
	23,
	23,
	340,
	26,
	2398,
	14,
	26,
	2258,
	14,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	803,
	803,
	803,
	803,
	803,
	23,
	23,
	23,
	23,
	23,
	105,
	105,
	9,
	23,
	3,
	216,
	4,
	4,
	4,
	3,
	3,
	23,
	9,
	9,
	23,
	9,
	9,
	126,
	23,
	9,
	126,
	23,
	217,
	2402,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	221,
	2403,
	23,
	220,
	1751,
	23,
	-1,
	-1,
	-1,
	-1,
	2404,
	2405,
	23,
	217,
	2402,
	23,
	112,
	107,
	23,
	218,
	2406,
	23,
	145,
	992,
	23,
	9,
	126,
	23,
	154,
	0,
	-1,
	0,
	28,
	27,
	197,
	0,
	137,
	23,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	219,
	1916,
	23,
	28,
	197,
	23,
	217,
	2402,
	23,
	112,
	107,
	23,
	218,
	2406,
	23,
	145,
	992,
	23,
	14,
	358,
	358,
	23,
	3,
	358,
	358,
	23,
	3,
	358,
	358,
	23,
	3,
	2387,
	23,
	-1,
	-1,
	-1,
	-1,
	358,
	358,
	23,
	3,
	14,
	28,
	28,
	27,
	27,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	28,
	28,
	41,
	-1,
	-1,
	-1,
	26,
	14,
	2407,
	1495,
	2408,
	23,
	14,
	2409,
	2410,
	2000,
	23,
	14,
	2411,
	1492,
	2412,
	23,
	358,
	358,
	23,
	3,
	358,
	358,
	23,
	3,
	14,
	358,
	358,
	23,
	3,
	215,
	2385,
	2386,
	3,
	358,
	358,
	23,
	358,
	358,
	23,
	3,
	358,
	358,
	23,
	3,
	358,
	358,
	23,
	3,
	89,
	89,
	23,
	-1,
	-1,
	23,
	26,
	14,
	26,
	803,
	1150,
	23,
	23,
	23,
	49,
	106,
	164,
	3,
	3,
	3,
	358,
	358,
	23,
	3,
	358,
	358,
	23,
	3,
	358,
	358,
	23,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	23,
	23,
	23,
	43,
	0,
	3,
	23,
	3,
	154,
	154,
	-1,
	1,
	137,
	0,
	1,
	1,
	0,
	3,
	23,
	2413,
	2414,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	14,
	14,
	3,
	1,
	1,
	1,
	114,
	114,
	0,
	1,
	186,
	1,
	186,
	2,
	114,
	3,
	23,
	28,
	28,
	9,
	23,
	9,
	3,
	186,
	4,
	114,
	114,
	0,
	1,
	193,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	28,
	3,
	23,
	28,
	9,
	32,
	23,
	89,
	23,
	23,
	14,
	23,
	14,
	14,
	14,
	26,
	26,
	26,
	26,
	1106,
	1,
	1,
	186,
	2415,
	2297,
	23,
	3,
	23,
	9,
	9,
	825,
	153,
	0,
	209,
	209,
	131,
	131,
	210,
	210,
	100,
	101,
	346,
	2416,
	2417,
	2417,
	182,
	182,
	2418,
	2418,
	2419,
	2420,
	2421,
	2422,
	3,
	563,
	563,
	2423,
	2423,
	137,
	977,
	-1,
	-1,
	2254,
	2424,
	-1,
	-1,
	241,
	1155,
	-1,
	-1,
	0,
	1,
	-1,
	-1,
	241,
	1930,
	-1,
	-1,
	241,
	241,
	-1,
	-1,
	0,
	2425,
	2425,
	114,
	114,
	114,
	114,
	2426,
	2427,
	2427,
	109,
	2428,
	2429,
	2430,
	2430,
	137,
	695,
	0,
	1,
	0,
	119,
	186,
	0,
	0,
	449,
	449,
	3,
	3,
	23,
	41,
	23,
	503,
	23,
	358,
	114,
	153,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	0,
	153,
	0,
	114,
	114,
	153,
	1,
	0,
	0,
	114,
	114,
	153,
	135,
	632,
	114,
	153,
	0,
	114,
	0,
	0,
	207,
	114,
	48,
	48,
	207,
	206,
	-1,
	206,
	1589,
	1929,
	1582,
	-1,
	-1,
	135,
	135,
	135,
	135,
	1,
	1,
	1,
	1930,
	119,
	119,
	119,
	563,
	-1,
	1,
	720,
	153,
	153,
	0,
	0,
	0,
	0,
	0,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	135,
	138,
	207,
	0,
	1,
	186,
	1931,
	135,
	135,
	135,
	135,
	1932,
	153,
	153,
	135,
	114,
	114,
	257,
	0,
	207,
	206,
	3,
	23,
	28,
	-1,
	-1,
	-1,
	-1,
	23,
	9,
	23,
	9,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3,
	114,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	124,
	503,
	1180,
	1111,
	124,
	358,
	1936,
	358,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	49,
	-1,
	0,
	-1,
	0,
	-1,
	-1,
	1,
	-1,
	-1,
	1,
	1,
	1,
	-1,
	-1,
	-1,
	-1,
	-1,
	0,
	-1,
	0,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	14,
	-1,
	-1,
	23,
	26,
	-1,
	-1,
	-1,
	-1,
	23,
	503,
	-1,
	-1,
	-1,
	-1,
	23,
	358,
	23,
	503,
	23,
	358,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	90,
	112,
	23,
	3,
	23,
	23,
	-1,
	26,
	10,
	89,
	89,
	89,
	14,
	34,
	62,
	34,
	62,
	34,
	9,
	130,
	130,
	14,
	14,
	14,
	112,
	23,
	62,
	26,
	32,
	112,
	32,
	62,
	26,
	23,
	9,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	27,
	197,
	14,
	14,
	10,
	14,
	14,
	14,
	14,
	715,
	10,
	123,
	148,
	481,
	28,
	716,
	27,
	197,
	14,
	14,
	703,
	10,
	14,
	14,
	14,
	14,
	14,
	123,
	148,
	10,
	14,
	581,
	481,
	27,
	197,
	14,
	14,
	10,
	14,
	14,
	14,
	14,
	10,
	89,
	89,
	123,
	148,
	481,
	123,
	123,
	14,
	123,
	581,
	728,
	-1,
	-1,
	-1,
	-1,
	3,
	3,
	53,
	-1,
	-1,
	-1,
	-1,
	704,
	506,
	227,
	1948,
	94,
};
static const Il2CppTokenRangePair s_rgctxIndices[138] = 
{
	{ 0x02000002, { 0, 11 } },
	{ 0x02000009, { 11, 10 } },
	{ 0x0200000A, { 21, 3 } },
	{ 0x0200000B, { 24, 19 } },
	{ 0x0200000C, { 43, 3 } },
	{ 0x0200000D, { 46, 25 } },
	{ 0x02000031, { 92, 8 } },
	{ 0x02000033, { 100, 24 } },
	{ 0x02000035, { 124, 1 } },
	{ 0x02000038, { 125, 10 } },
	{ 0x02000039, { 135, 3 } },
	{ 0x0200003A, { 138, 28 } },
	{ 0x0200003B, { 166, 27 } },
	{ 0x0200003C, { 193, 20 } },
	{ 0x0200003D, { 213, 4 } },
	{ 0x0200003F, { 217, 3 } },
	{ 0x02000041, { 220, 3 } },
	{ 0x02000042, { 223, 4 } },
	{ 0x02000044, { 227, 16 } },
	{ 0x02000045, { 243, 15 } },
	{ 0x02000048, { 258, 13 } },
	{ 0x02000049, { 271, 12 } },
	{ 0x0200004A, { 283, 9 } },
	{ 0x0200004B, { 292, 27 } },
	{ 0x0200004D, { 319, 3 } },
	{ 0x0200004E, { 322, 3 } },
	{ 0x0200004F, { 325, 10 } },
	{ 0x02000050, { 335, 5 } },
	{ 0x02000051, { 340, 6 } },
	{ 0x02000052, { 346, 3 } },
	{ 0x02000053, { 349, 19 } },
	{ 0x02000054, { 368, 2 } },
	{ 0x02000074, { 370, 12 } },
	{ 0x02000077, { 382, 4 } },
	{ 0x0200007F, { 388, 3 } },
	{ 0x0200008A, { 391, 5 } },
	{ 0x0200008D, { 396, 4 } },
	{ 0x0200009E, { 400, 4 } },
	{ 0x020000A4, { 404, 12 } },
	{ 0x020000B0, { 418, 1 } },
	{ 0x020000B1, { 419, 1 } },
	{ 0x020000B2, { 420, 12 } },
	{ 0x020000C6, { 456, 4 } },
	{ 0x020000C7, { 460, 4 } },
	{ 0x020000C8, { 464, 4 } },
	{ 0x020000C9, { 468, 5 } },
	{ 0x020000CB, { 481, 9 } },
	{ 0x020000D4, { 527, 5 } },
	{ 0x020000D9, { 532, 4 } },
	{ 0x020000DB, { 536, 5 } },
	{ 0x020000DE, { 541, 8 } },
	{ 0x020000DF, { 549, 48 } },
	{ 0x020000EA, { 695, 1 } },
	{ 0x020000EC, { 696, 1 } },
	{ 0x020000EE, { 697, 2 } },
	{ 0x020000EF, { 699, 1 } },
	{ 0x020000F1, { 700, 3 } },
	{ 0x020000F2, { 703, 1 } },
	{ 0x020000F6, { 704, 1 } },
	{ 0x020000F7, { 705, 1 } },
	{ 0x020000F8, { 706, 3 } },
	{ 0x020000F9, { 709, 2 } },
	{ 0x020000FA, { 711, 1 } },
	{ 0x020000FC, { 712, 2 } },
	{ 0x020000FD, { 714, 1 } },
	{ 0x020000FE, { 715, 2 } },
	{ 0x020000FF, { 717, 1 } },
	{ 0x02000100, { 718, 2 } },
	{ 0x02000108, { 720, 7 } },
	{ 0x02000109, { 727, 14 } },
	{ 0x0200010D, { 741, 4 } },
	{ 0x060000C0, { 71, 5 } },
	{ 0x0600010B, { 76, 1 } },
	{ 0x0600018E, { 77, 4 } },
	{ 0x060001C0, { 81, 3 } },
	{ 0x0600020B, { 84, 5 } },
	{ 0x0600023F, { 89, 3 } },
	{ 0x0600039C, { 386, 2 } },
	{ 0x0600044F, { 416, 2 } },
	{ 0x060004C3, { 432, 2 } },
	{ 0x060004C4, { 434, 2 } },
	{ 0x060004C7, { 436, 1 } },
	{ 0x060004C8, { 437, 1 } },
	{ 0x060004CB, { 438, 1 } },
	{ 0x060004CC, { 439, 1 } },
	{ 0x060004CF, { 440, 2 } },
	{ 0x060004D0, { 442, 2 } },
	{ 0x060004D3, { 444, 1 } },
	{ 0x060004D4, { 445, 1 } },
	{ 0x060004D7, { 446, 1 } },
	{ 0x060004D8, { 447, 1 } },
	{ 0x060004FD, { 448, 2 } },
	{ 0x060004FE, { 450, 2 } },
	{ 0x060004FF, { 452, 2 } },
	{ 0x06000500, { 454, 2 } },
	{ 0x06000515, { 473, 3 } },
	{ 0x06000516, { 476, 3 } },
	{ 0x06000517, { 479, 2 } },
	{ 0x06000522, { 490, 1 } },
	{ 0x06000523, { 491, 1 } },
	{ 0x06000524, { 492, 2 } },
	{ 0x06000525, { 494, 1 } },
	{ 0x06000526, { 495, 1 } },
	{ 0x06000527, { 496, 3 } },
	{ 0x06000542, { 499, 4 } },
	{ 0x06000547, { 503, 11 } },
	{ 0x06000548, { 514, 2 } },
	{ 0x06000555, { 516, 2 } },
	{ 0x0600055F, { 518, 2 } },
	{ 0x06000560, { 520, 1 } },
	{ 0x06000561, { 521, 1 } },
	{ 0x06000562, { 522, 2 } },
	{ 0x06000563, { 524, 1 } },
	{ 0x06000564, { 525, 1 } },
	{ 0x06000565, { 526, 1 } },
	{ 0x060005E0, { 597, 9 } },
	{ 0x060005E2, { 606, 5 } },
	{ 0x060005E4, { 611, 5 } },
	{ 0x060005E5, { 616, 5 } },
	{ 0x060005E7, { 621, 5 } },
	{ 0x060005E8, { 626, 5 } },
	{ 0x060005EC, { 631, 5 } },
	{ 0x060005ED, { 636, 5 } },
	{ 0x060005EE, { 641, 5 } },
	{ 0x060005EF, { 646, 5 } },
	{ 0x060005F0, { 651, 2 } },
	{ 0x060005F2, { 653, 6 } },
	{ 0x060005F4, { 659, 7 } },
	{ 0x060005F5, { 666, 6 } },
	{ 0x060005F6, { 672, 7 } },
	{ 0x060005F7, { 679, 3 } },
	{ 0x060005F8, { 682, 3 } },
	{ 0x060005F9, { 685, 5 } },
	{ 0x060005FA, { 690, 5 } },
	{ 0x060006D6, { 745, 1 } },
	{ 0x060006D7, { 746, 2 } },
	{ 0x060006D8, { 748, 1 } },
	{ 0x060006D9, { 749, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[750] = 
{
	{ (Il2CppRGCTXDataType)2, 34924 },
	{ (Il2CppRGCTXDataType)3, 25909 },
	{ (Il2CppRGCTXDataType)2, 34925 },
	{ (Il2CppRGCTXDataType)3, 25910 },
	{ (Il2CppRGCTXDataType)3, 25911 },
	{ (Il2CppRGCTXDataType)2, 34926 },
	{ (Il2CppRGCTXDataType)3, 25912 },
	{ (Il2CppRGCTXDataType)3, 25913 },
	{ (Il2CppRGCTXDataType)3, 25914 },
	{ (Il2CppRGCTXDataType)2, 31062 },
	{ (Il2CppRGCTXDataType)2, 31063 },
	{ (Il2CppRGCTXDataType)1, 31083 },
	{ (Il2CppRGCTXDataType)2, 34487 },
	{ (Il2CppRGCTXDataType)2, 34927 },
	{ (Il2CppRGCTXDataType)2, 31082 },
	{ (Il2CppRGCTXDataType)2, 34928 },
	{ (Il2CppRGCTXDataType)3, 25915 },
	{ (Il2CppRGCTXDataType)3, 25916 },
	{ (Il2CppRGCTXDataType)2, 31083 },
	{ (Il2CppRGCTXDataType)3, 25917 },
	{ (Il2CppRGCTXDataType)3, 25918 },
	{ (Il2CppRGCTXDataType)2, 34929 },
	{ (Il2CppRGCTXDataType)3, 25919 },
	{ (Il2CppRGCTXDataType)2, 34929 },
	{ (Il2CppRGCTXDataType)3, 25920 },
	{ (Il2CppRGCTXDataType)2, 34930 },
	{ (Il2CppRGCTXDataType)2, 31097 },
	{ (Il2CppRGCTXDataType)1, 31098 },
	{ (Il2CppRGCTXDataType)1, 31102 },
	{ (Il2CppRGCTXDataType)3, 25921 },
	{ (Il2CppRGCTXDataType)2, 31102 },
	{ (Il2CppRGCTXDataType)3, 25922 },
	{ (Il2CppRGCTXDataType)2, 31098 },
	{ (Il2CppRGCTXDataType)3, 25923 },
	{ (Il2CppRGCTXDataType)3, 25924 },
	{ (Il2CppRGCTXDataType)3, 25925 },
	{ (Il2CppRGCTXDataType)3, 25926 },
	{ (Il2CppRGCTXDataType)3, 25927 },
	{ (Il2CppRGCTXDataType)3, 25928 },
	{ (Il2CppRGCTXDataType)3, 25929 },
	{ (Il2CppRGCTXDataType)3, 25930 },
	{ (Il2CppRGCTXDataType)3, 25931 },
	{ (Il2CppRGCTXDataType)2, 34931 },
	{ (Il2CppRGCTXDataType)3, 25932 },
	{ (Il2CppRGCTXDataType)2, 31105 },
	{ (Il2CppRGCTXDataType)2, 34932 },
	{ (Il2CppRGCTXDataType)3, 25933 },
	{ (Il2CppRGCTXDataType)2, 34933 },
	{ (Il2CppRGCTXDataType)2, 31109 },
	{ (Il2CppRGCTXDataType)2, 34934 },
	{ (Il2CppRGCTXDataType)3, 25934 },
	{ (Il2CppRGCTXDataType)1, 31110 },
	{ (Il2CppRGCTXDataType)1, 31115 },
	{ (Il2CppRGCTXDataType)3, 25935 },
	{ (Il2CppRGCTXDataType)2, 31115 },
	{ (Il2CppRGCTXDataType)3, 25936 },
	{ (Il2CppRGCTXDataType)2, 31110 },
	{ (Il2CppRGCTXDataType)3, 25937 },
	{ (Il2CppRGCTXDataType)3, 25938 },
	{ (Il2CppRGCTXDataType)3, 25939 },
	{ (Il2CppRGCTXDataType)3, 25940 },
	{ (Il2CppRGCTXDataType)3, 25941 },
	{ (Il2CppRGCTXDataType)3, 25942 },
	{ (Il2CppRGCTXDataType)3, 25943 },
	{ (Il2CppRGCTXDataType)3, 25944 },
	{ (Il2CppRGCTXDataType)3, 25945 },
	{ (Il2CppRGCTXDataType)3, 25946 },
	{ (Il2CppRGCTXDataType)2, 34935 },
	{ (Il2CppRGCTXDataType)3, 25947 },
	{ (Il2CppRGCTXDataType)3, 25948 },
	{ (Il2CppRGCTXDataType)3, 25949 },
	{ (Il2CppRGCTXDataType)1, 31161 },
	{ (Il2CppRGCTXDataType)2, 31160 },
	{ (Il2CppRGCTXDataType)2, 31160 },
	{ (Il2CppRGCTXDataType)2, 34936 },
	{ (Il2CppRGCTXDataType)3, 25950 },
	{ (Il2CppRGCTXDataType)1, 31174 },
	{ (Il2CppRGCTXDataType)1, 31198 },
	{ (Il2CppRGCTXDataType)2, 34937 },
	{ (Il2CppRGCTXDataType)2, 31197 },
	{ (Il2CppRGCTXDataType)3, 25951 },
	{ (Il2CppRGCTXDataType)1, 31203 },
	{ (Il2CppRGCTXDataType)2, 34938 },
	{ (Il2CppRGCTXDataType)3, 25952 },
	{ (Il2CppRGCTXDataType)1, 31219 },
	{ (Il2CppRGCTXDataType)2, 31218 },
	{ (Il2CppRGCTXDataType)2, 31218 },
	{ (Il2CppRGCTXDataType)2, 34939 },
	{ (Il2CppRGCTXDataType)3, 25953 },
	{ (Il2CppRGCTXDataType)1, 31226 },
	{ (Il2CppRGCTXDataType)2, 34940 },
	{ (Il2CppRGCTXDataType)3, 25954 },
	{ (Il2CppRGCTXDataType)2, 31229 },
	{ (Il2CppRGCTXDataType)3, 25955 },
	{ (Il2CppRGCTXDataType)2, 34941 },
	{ (Il2CppRGCTXDataType)2, 31228 },
	{ (Il2CppRGCTXDataType)3, 25956 },
	{ (Il2CppRGCTXDataType)3, 25957 },
	{ (Il2CppRGCTXDataType)3, 25958 },
	{ (Il2CppRGCTXDataType)3, 25959 },
	{ (Il2CppRGCTXDataType)1, 31241 },
	{ (Il2CppRGCTXDataType)2, 34942 },
	{ (Il2CppRGCTXDataType)2, 31239 },
	{ (Il2CppRGCTXDataType)3, 25960 },
	{ (Il2CppRGCTXDataType)3, 25961 },
	{ (Il2CppRGCTXDataType)3, 25962 },
	{ (Il2CppRGCTXDataType)3, 25963 },
	{ (Il2CppRGCTXDataType)3, 25964 },
	{ (Il2CppRGCTXDataType)3, 25965 },
	{ (Il2CppRGCTXDataType)2, 34943 },
	{ (Il2CppRGCTXDataType)3, 25966 },
	{ (Il2CppRGCTXDataType)3, 25967 },
	{ (Il2CppRGCTXDataType)2, 31240 },
	{ (Il2CppRGCTXDataType)3, 25968 },
	{ (Il2CppRGCTXDataType)3, 25969 },
	{ (Il2CppRGCTXDataType)2, 31241 },
	{ (Il2CppRGCTXDataType)3, 25970 },
	{ (Il2CppRGCTXDataType)3, 25971 },
	{ (Il2CppRGCTXDataType)3, 25972 },
	{ (Il2CppRGCTXDataType)3, 25973 },
	{ (Il2CppRGCTXDataType)3, 25974 },
	{ (Il2CppRGCTXDataType)3, 25975 },
	{ (Il2CppRGCTXDataType)3, 25976 },
	{ (Il2CppRGCTXDataType)3, 25977 },
	{ (Il2CppRGCTXDataType)2, 31253 },
	{ (Il2CppRGCTXDataType)2, 34944 },
	{ (Il2CppRGCTXDataType)2, 31261 },
	{ (Il2CppRGCTXDataType)1, 31262 },
	{ (Il2CppRGCTXDataType)2, 31262 },
	{ (Il2CppRGCTXDataType)2, 34945 },
	{ (Il2CppRGCTXDataType)3, 25978 },
	{ (Il2CppRGCTXDataType)3, 25979 },
	{ (Il2CppRGCTXDataType)3, 25980 },
	{ (Il2CppRGCTXDataType)3, 25981 },
	{ (Il2CppRGCTXDataType)3, 25982 },
	{ (Il2CppRGCTXDataType)2, 34946 },
	{ (Il2CppRGCTXDataType)3, 25983 },
	{ (Il2CppRGCTXDataType)2, 34946 },
	{ (Il2CppRGCTXDataType)1, 31276 },
	{ (Il2CppRGCTXDataType)2, 34947 },
	{ (Il2CppRGCTXDataType)2, 31271 },
	{ (Il2CppRGCTXDataType)3, 25984 },
	{ (Il2CppRGCTXDataType)3, 25985 },
	{ (Il2CppRGCTXDataType)3, 25986 },
	{ (Il2CppRGCTXDataType)1, 31272 },
	{ (Il2CppRGCTXDataType)1, 31275 },
	{ (Il2CppRGCTXDataType)3, 25987 },
	{ (Il2CppRGCTXDataType)3, 25988 },
	{ (Il2CppRGCTXDataType)2, 31272 },
	{ (Il2CppRGCTXDataType)3, 25989 },
	{ (Il2CppRGCTXDataType)3, 25990 },
	{ (Il2CppRGCTXDataType)3, 25991 },
	{ (Il2CppRGCTXDataType)3, 25992 },
	{ (Il2CppRGCTXDataType)2, 31276 },
	{ (Il2CppRGCTXDataType)3, 25993 },
	{ (Il2CppRGCTXDataType)3, 25994 },
	{ (Il2CppRGCTXDataType)3, 25995 },
	{ (Il2CppRGCTXDataType)3, 25996 },
	{ (Il2CppRGCTXDataType)3, 25997 },
	{ (Il2CppRGCTXDataType)3, 25998 },
	{ (Il2CppRGCTXDataType)3, 25999 },
	{ (Il2CppRGCTXDataType)3, 26000 },
	{ (Il2CppRGCTXDataType)3, 26001 },
	{ (Il2CppRGCTXDataType)3, 26002 },
	{ (Il2CppRGCTXDataType)3, 26003 },
	{ (Il2CppRGCTXDataType)2, 34948 },
	{ (Il2CppRGCTXDataType)1, 31286 },
	{ (Il2CppRGCTXDataType)2, 34949 },
	{ (Il2CppRGCTXDataType)2, 31284 },
	{ (Il2CppRGCTXDataType)3, 26004 },
	{ (Il2CppRGCTXDataType)3, 26005 },
	{ (Il2CppRGCTXDataType)3, 26006 },
	{ (Il2CppRGCTXDataType)3, 26007 },
	{ (Il2CppRGCTXDataType)3, 26008 },
	{ (Il2CppRGCTXDataType)2, 31285 },
	{ (Il2CppRGCTXDataType)3, 26009 },
	{ (Il2CppRGCTXDataType)3, 26010 },
	{ (Il2CppRGCTXDataType)3, 26011 },
	{ (Il2CppRGCTXDataType)3, 26012 },
	{ (Il2CppRGCTXDataType)3, 26013 },
	{ (Il2CppRGCTXDataType)2, 31286 },
	{ (Il2CppRGCTXDataType)3, 26014 },
	{ (Il2CppRGCTXDataType)3, 26015 },
	{ (Il2CppRGCTXDataType)3, 26016 },
	{ (Il2CppRGCTXDataType)3, 26017 },
	{ (Il2CppRGCTXDataType)3, 26018 },
	{ (Il2CppRGCTXDataType)3, 26019 },
	{ (Il2CppRGCTXDataType)3, 26020 },
	{ (Il2CppRGCTXDataType)3, 26021 },
	{ (Il2CppRGCTXDataType)3, 26022 },
	{ (Il2CppRGCTXDataType)3, 26023 },
	{ (Il2CppRGCTXDataType)3, 26024 },
	{ (Il2CppRGCTXDataType)2, 34950 },
	{ (Il2CppRGCTXDataType)3, 26025 },
	{ (Il2CppRGCTXDataType)2, 34951 },
	{ (Il2CppRGCTXDataType)2, 31295 },
	{ (Il2CppRGCTXDataType)3, 26026 },
	{ (Il2CppRGCTXDataType)3, 26027 },
	{ (Il2CppRGCTXDataType)3, 26028 },
	{ (Il2CppRGCTXDataType)3, 26029 },
	{ (Il2CppRGCTXDataType)3, 26030 },
	{ (Il2CppRGCTXDataType)3, 26031 },
	{ (Il2CppRGCTXDataType)3, 26032 },
	{ (Il2CppRGCTXDataType)3, 26033 },
	{ (Il2CppRGCTXDataType)3, 26034 },
	{ (Il2CppRGCTXDataType)3, 26035 },
	{ (Il2CppRGCTXDataType)2, 34952 },
	{ (Il2CppRGCTXDataType)2, 31296 },
	{ (Il2CppRGCTXDataType)3, 26036 },
	{ (Il2CppRGCTXDataType)3, 26037 },
	{ (Il2CppRGCTXDataType)3, 26038 },
	{ (Il2CppRGCTXDataType)3, 26039 },
	{ (Il2CppRGCTXDataType)3, 26040 },
	{ (Il2CppRGCTXDataType)3, 26041 },
	{ (Il2CppRGCTXDataType)3, 26042 },
	{ (Il2CppRGCTXDataType)3, 26043 },
	{ (Il2CppRGCTXDataType)2, 31306 },
	{ (Il2CppRGCTXDataType)3, 26044 },
	{ (Il2CppRGCTXDataType)2, 31313 },
	{ (Il2CppRGCTXDataType)2, 34953 },
	{ (Il2CppRGCTXDataType)3, 26045 },
	{ (Il2CppRGCTXDataType)2, 31320 },
	{ (Il2CppRGCTXDataType)2, 34954 },
	{ (Il2CppRGCTXDataType)3, 26046 },
	{ (Il2CppRGCTXDataType)2, 31324 },
	{ (Il2CppRGCTXDataType)2, 34955 },
	{ (Il2CppRGCTXDataType)2, 34956 },
	{ (Il2CppRGCTXDataType)3, 26047 },
	{ (Il2CppRGCTXDataType)2, 34957 },
	{ (Il2CppRGCTXDataType)2, 31330 },
	{ (Il2CppRGCTXDataType)1, 31331 },
	{ (Il2CppRGCTXDataType)1, 31334 },
	{ (Il2CppRGCTXDataType)3, 26048 },
	{ (Il2CppRGCTXDataType)3, 26049 },
	{ (Il2CppRGCTXDataType)3, 26050 },
	{ (Il2CppRGCTXDataType)2, 31331 },
	{ (Il2CppRGCTXDataType)2, 31335 },
	{ (Il2CppRGCTXDataType)3, 26051 },
	{ (Il2CppRGCTXDataType)3, 26052 },
	{ (Il2CppRGCTXDataType)2, 34958 },
	{ (Il2CppRGCTXDataType)3, 26053 },
	{ (Il2CppRGCTXDataType)2, 34959 },
	{ (Il2CppRGCTXDataType)3, 26054 },
	{ (Il2CppRGCTXDataType)3, 26055 },
	{ (Il2CppRGCTXDataType)2, 34960 },
	{ (Il2CppRGCTXDataType)2, 31338 },
	{ (Il2CppRGCTXDataType)3, 26056 },
	{ (Il2CppRGCTXDataType)2, 31339 },
	{ (Il2CppRGCTXDataType)3, 26057 },
	{ (Il2CppRGCTXDataType)3, 26058 },
	{ (Il2CppRGCTXDataType)3, 26059 },
	{ (Il2CppRGCTXDataType)3, 26060 },
	{ (Il2CppRGCTXDataType)3, 26061 },
	{ (Il2CppRGCTXDataType)3, 26062 },
	{ (Il2CppRGCTXDataType)3, 26063 },
	{ (Il2CppRGCTXDataType)3, 26064 },
	{ (Il2CppRGCTXDataType)3, 26065 },
	{ (Il2CppRGCTXDataType)2, 34961 },
	{ (Il2CppRGCTXDataType)2, 34962 },
	{ (Il2CppRGCTXDataType)2, 31348 },
	{ (Il2CppRGCTXDataType)3, 26066 },
	{ (Il2CppRGCTXDataType)3, 26067 },
	{ (Il2CppRGCTXDataType)3, 26068 },
	{ (Il2CppRGCTXDataType)3, 26069 },
	{ (Il2CppRGCTXDataType)3, 26070 },
	{ (Il2CppRGCTXDataType)3, 26071 },
	{ (Il2CppRGCTXDataType)2, 31349 },
	{ (Il2CppRGCTXDataType)3, 26072 },
	{ (Il2CppRGCTXDataType)3, 26073 },
	{ (Il2CppRGCTXDataType)3, 26074 },
	{ (Il2CppRGCTXDataType)3, 26075 },
	{ (Il2CppRGCTXDataType)3, 26076 },
	{ (Il2CppRGCTXDataType)2, 34963 },
	{ (Il2CppRGCTXDataType)2, 31357 },
	{ (Il2CppRGCTXDataType)3, 26077 },
	{ (Il2CppRGCTXDataType)2, 31358 },
	{ (Il2CppRGCTXDataType)3, 26078 },
	{ (Il2CppRGCTXDataType)3, 26079 },
	{ (Il2CppRGCTXDataType)3, 26080 },
	{ (Il2CppRGCTXDataType)3, 26081 },
	{ (Il2CppRGCTXDataType)3, 26082 },
	{ (Il2CppRGCTXDataType)3, 26083 },
	{ (Il2CppRGCTXDataType)3, 26084 },
	{ (Il2CppRGCTXDataType)1, 31364 },
	{ (Il2CppRGCTXDataType)3, 26085 },
	{ (Il2CppRGCTXDataType)2, 34964 },
	{ (Il2CppRGCTXDataType)2, 31364 },
	{ (Il2CppRGCTXDataType)3, 26086 },
	{ (Il2CppRGCTXDataType)3, 26087 },
	{ (Il2CppRGCTXDataType)3, 26088 },
	{ (Il2CppRGCTXDataType)3, 26089 },
	{ (Il2CppRGCTXDataType)3, 26090 },
	{ (Il2CppRGCTXDataType)3, 26091 },
	{ (Il2CppRGCTXDataType)2, 34965 },
	{ (Il2CppRGCTXDataType)2, 31368 },
	{ (Il2CppRGCTXDataType)1, 31369 },
	{ (Il2CppRGCTXDataType)1, 31372 },
	{ (Il2CppRGCTXDataType)2, 34966 },
	{ (Il2CppRGCTXDataType)3, 26092 },
	{ (Il2CppRGCTXDataType)2, 34967 },
	{ (Il2CppRGCTXDataType)3, 26093 },
	{ (Il2CppRGCTXDataType)2, 31369 },
	{ (Il2CppRGCTXDataType)3, 26094 },
	{ (Il2CppRGCTXDataType)3, 26095 },
	{ (Il2CppRGCTXDataType)2, 31371 },
	{ (Il2CppRGCTXDataType)3, 26096 },
	{ (Il2CppRGCTXDataType)3, 26097 },
	{ (Il2CppRGCTXDataType)2, 34968 },
	{ (Il2CppRGCTXDataType)3, 26098 },
	{ (Il2CppRGCTXDataType)3, 26099 },
	{ (Il2CppRGCTXDataType)2, 31373 },
	{ (Il2CppRGCTXDataType)3, 26100 },
	{ (Il2CppRGCTXDataType)3, 26101 },
	{ (Il2CppRGCTXDataType)3, 26102 },
	{ (Il2CppRGCTXDataType)3, 26103 },
	{ (Il2CppRGCTXDataType)2, 31372 },
	{ (Il2CppRGCTXDataType)3, 26104 },
	{ (Il2CppRGCTXDataType)3, 26105 },
	{ (Il2CppRGCTXDataType)3, 26106 },
	{ (Il2CppRGCTXDataType)2, 34969 },
	{ (Il2CppRGCTXDataType)2, 34970 },
	{ (Il2CppRGCTXDataType)3, 26107 },
	{ (Il2CppRGCTXDataType)2, 34971 },
	{ (Il2CppRGCTXDataType)2, 34973 },
	{ (Il2CppRGCTXDataType)3, 26108 },
	{ (Il2CppRGCTXDataType)3, 26109 },
	{ (Il2CppRGCTXDataType)2, 34974 },
	{ (Il2CppRGCTXDataType)2, 31390 },
	{ (Il2CppRGCTXDataType)3, 26110 },
	{ (Il2CppRGCTXDataType)3, 26111 },
	{ (Il2CppRGCTXDataType)2, 31391 },
	{ (Il2CppRGCTXDataType)3, 26112 },
	{ (Il2CppRGCTXDataType)3, 26113 },
	{ (Il2CppRGCTXDataType)3, 26114 },
	{ (Il2CppRGCTXDataType)3, 26115 },
	{ (Il2CppRGCTXDataType)3, 26116 },
	{ (Il2CppRGCTXDataType)3, 26117 },
	{ (Il2CppRGCTXDataType)3, 26118 },
	{ (Il2CppRGCTXDataType)3, 26119 },
	{ (Il2CppRGCTXDataType)2, 31396 },
	{ (Il2CppRGCTXDataType)3, 26120 },
	{ (Il2CppRGCTXDataType)2, 31401 },
	{ (Il2CppRGCTXDataType)3, 26121 },
	{ (Il2CppRGCTXDataType)2, 31402 },
	{ (Il2CppRGCTXDataType)1, 31402 },
	{ (Il2CppRGCTXDataType)3, 26122 },
	{ (Il2CppRGCTXDataType)2, 31407 },
	{ (Il2CppRGCTXDataType)3, 26123 },
	{ (Il2CppRGCTXDataType)2, 31406 },
	{ (Il2CppRGCTXDataType)2, 34975 },
	{ (Il2CppRGCTXDataType)3, 26124 },
	{ (Il2CppRGCTXDataType)1, 31411 },
	{ (Il2CppRGCTXDataType)3, 26125 },
	{ (Il2CppRGCTXDataType)2, 34976 },
	{ (Il2CppRGCTXDataType)3, 26126 },
	{ (Il2CppRGCTXDataType)2, 34977 },
	{ (Il2CppRGCTXDataType)2, 31410 },
	{ (Il2CppRGCTXDataType)2, 34978 },
	{ (Il2CppRGCTXDataType)3, 26127 },
	{ (Il2CppRGCTXDataType)3, 26128 },
	{ (Il2CppRGCTXDataType)3, 26129 },
	{ (Il2CppRGCTXDataType)3, 26130 },
	{ (Il2CppRGCTXDataType)3, 26131 },
	{ (Il2CppRGCTXDataType)3, 24230 },
	{ (Il2CppRGCTXDataType)2, 31411 },
	{ (Il2CppRGCTXDataType)3, 26132 },
	{ (Il2CppRGCTXDataType)3, 24229 },
	{ (Il2CppRGCTXDataType)3, 26133 },
	{ (Il2CppRGCTXDataType)1, 31417 },
	{ (Il2CppRGCTXDataType)2, 31417 },
	{ (Il2CppRGCTXDataType)2, 34979 },
	{ (Il2CppRGCTXDataType)2, 31493 },
	{ (Il2CppRGCTXDataType)2, 31494 },
	{ (Il2CppRGCTXDataType)3, 26134 },
	{ (Il2CppRGCTXDataType)2, 31495 },
	{ (Il2CppRGCTXDataType)3, 26135 },
	{ (Il2CppRGCTXDataType)3, 26136 },
	{ (Il2CppRGCTXDataType)3, 26137 },
	{ (Il2CppRGCTXDataType)3, 26138 },
	{ (Il2CppRGCTXDataType)1, 31494 },
	{ (Il2CppRGCTXDataType)2, 34980 },
	{ (Il2CppRGCTXDataType)3, 26139 },
	{ (Il2CppRGCTXDataType)1, 31503 },
	{ (Il2CppRGCTXDataType)2, 31503 },
	{ (Il2CppRGCTXDataType)3, 26140 },
	{ (Il2CppRGCTXDataType)2, 31502 },
	{ (Il2CppRGCTXDataType)1, 31512 },
	{ (Il2CppRGCTXDataType)2, 31511 },
	{ (Il2CppRGCTXDataType)3, 26141 },
	{ (Il2CppRGCTXDataType)2, 31516 },
	{ (Il2CppRGCTXDataType)3, 26142 },
	{ (Il2CppRGCTXDataType)2, 31537 },
	{ (Il2CppRGCTXDataType)2, 34981 },
	{ (Il2CppRGCTXDataType)2, 31536 },
	{ (Il2CppRGCTXDataType)3, 26143 },
	{ (Il2CppRGCTXDataType)1, 31537 },
	{ (Il2CppRGCTXDataType)2, 31548 },
	{ (Il2CppRGCTXDataType)3, 26144 },
	{ (Il2CppRGCTXDataType)3, 26145 },
	{ (Il2CppRGCTXDataType)3, 26146 },
	{ (Il2CppRGCTXDataType)3, 26147 },
	{ (Il2CppRGCTXDataType)3, 26148 },
	{ (Il2CppRGCTXDataType)2, 31590 },
	{ (Il2CppRGCTXDataType)2, 34982 },
	{ (Il2CppRGCTXDataType)2, 31606 },
	{ (Il2CppRGCTXDataType)2, 31608 },
	{ (Il2CppRGCTXDataType)3, 26149 },
	{ (Il2CppRGCTXDataType)3, 26150 },
	{ (Il2CppRGCTXDataType)3, 26151 },
	{ (Il2CppRGCTXDataType)3, 26152 },
	{ (Il2CppRGCTXDataType)2, 31608 },
	{ (Il2CppRGCTXDataType)3, 26153 },
	{ (Il2CppRGCTXDataType)3, 26154 },
	{ (Il2CppRGCTXDataType)3, 26155 },
	{ (Il2CppRGCTXDataType)2, 34983 },
	{ (Il2CppRGCTXDataType)3, 26156 },
	{ (Il2CppRGCTXDataType)1, 31617 },
	{ (Il2CppRGCTXDataType)2, 31616 },
	{ (Il2CppRGCTXDataType)2, 31654 },
	{ (Il2CppRGCTXDataType)2, 31658 },
	{ (Il2CppRGCTXDataType)1, 31662 },
	{ (Il2CppRGCTXDataType)2, 31665 },
	{ (Il2CppRGCTXDataType)1, 34984 },
	{ (Il2CppRGCTXDataType)3, 26157 },
	{ (Il2CppRGCTXDataType)2, 34985 },
	{ (Il2CppRGCTXDataType)2, 34986 },
	{ (Il2CppRGCTXDataType)3, 26158 },
	{ (Il2CppRGCTXDataType)2, 34987 },
	{ (Il2CppRGCTXDataType)3, 26159 },
	{ (Il2CppRGCTXDataType)2, 34988 },
	{ (Il2CppRGCTXDataType)3, 26160 },
	{ (Il2CppRGCTXDataType)2, 34988 },
	{ (Il2CppRGCTXDataType)3, 26161 },
	{ (Il2CppRGCTXDataType)3, 26162 },
	{ (Il2CppRGCTXDataType)3, 26163 },
	{ (Il2CppRGCTXDataType)3, 26164 },
	{ (Il2CppRGCTXDataType)3, 26165 },
	{ (Il2CppRGCTXDataType)3, 26166 },
	{ (Il2CppRGCTXDataType)3, 26167 },
	{ (Il2CppRGCTXDataType)3, 26168 },
	{ (Il2CppRGCTXDataType)3, 26169 },
	{ (Il2CppRGCTXDataType)3, 26170 },
	{ (Il2CppRGCTXDataType)3, 26171 },
	{ (Il2CppRGCTXDataType)3, 26172 },
	{ (Il2CppRGCTXDataType)3, 26173 },
	{ (Il2CppRGCTXDataType)3, 26174 },
	{ (Il2CppRGCTXDataType)3, 26175 },
	{ (Il2CppRGCTXDataType)3, 26176 },
	{ (Il2CppRGCTXDataType)2, 31741 },
	{ (Il2CppRGCTXDataType)3, 26177 },
	{ (Il2CppRGCTXDataType)2, 31745 },
	{ (Il2CppRGCTXDataType)3, 26178 },
	{ (Il2CppRGCTXDataType)2, 31749 },
	{ (Il2CppRGCTXDataType)3, 26179 },
	{ (Il2CppRGCTXDataType)2, 31752 },
	{ (Il2CppRGCTXDataType)3, 26180 },
	{ (Il2CppRGCTXDataType)3, 26181 },
	{ (Il2CppRGCTXDataType)3, 26182 },
	{ (Il2CppRGCTXDataType)3, 26183 },
	{ (Il2CppRGCTXDataType)3, 26184 },
	{ (Il2CppRGCTXDataType)3, 26185 },
	{ (Il2CppRGCTXDataType)3, 26186 },
	{ (Il2CppRGCTXDataType)3, 26187 },
	{ (Il2CppRGCTXDataType)3, 26188 },
	{ (Il2CppRGCTXDataType)3, 26189 },
	{ (Il2CppRGCTXDataType)3, 26190 },
	{ (Il2CppRGCTXDataType)3, 26191 },
	{ (Il2CppRGCTXDataType)3, 26192 },
	{ (Il2CppRGCTXDataType)3, 26193 },
	{ (Il2CppRGCTXDataType)3, 26194 },
	{ (Il2CppRGCTXDataType)3, 26195 },
	{ (Il2CppRGCTXDataType)3, 26196 },
	{ (Il2CppRGCTXDataType)3, 26197 },
	{ (Il2CppRGCTXDataType)2, 31786 },
	{ (Il2CppRGCTXDataType)2, 34989 },
	{ (Il2CppRGCTXDataType)3, 26198 },
	{ (Il2CppRGCTXDataType)2, 31789 },
	{ (Il2CppRGCTXDataType)2, 34990 },
	{ (Il2CppRGCTXDataType)3, 26199 },
	{ (Il2CppRGCTXDataType)2, 34991 },
	{ (Il2CppRGCTXDataType)3, 26200 },
	{ (Il2CppRGCTXDataType)3, 26201 },
	{ (Il2CppRGCTXDataType)3, 26202 },
	{ (Il2CppRGCTXDataType)2, 31802 },
	{ (Il2CppRGCTXDataType)2, 31797 },
	{ (Il2CppRGCTXDataType)3, 26203 },
	{ (Il2CppRGCTXDataType)2, 31796 },
	{ (Il2CppRGCTXDataType)2, 34992 },
	{ (Il2CppRGCTXDataType)3, 26204 },
	{ (Il2CppRGCTXDataType)3, 26205 },
	{ (Il2CppRGCTXDataType)1, 34993 },
	{ (Il2CppRGCTXDataType)3, 26206 },
	{ (Il2CppRGCTXDataType)3, 26207 },
	{ (Il2CppRGCTXDataType)3, 26208 },
	{ (Il2CppRGCTXDataType)3, 26209 },
	{ (Il2CppRGCTXDataType)3, 26210 },
	{ (Il2CppRGCTXDataType)1, 31811 },
	{ (Il2CppRGCTXDataType)3, 26211 },
	{ (Il2CppRGCTXDataType)2, 34995 },
	{ (Il2CppRGCTXDataType)1, 31826 },
	{ (Il2CppRGCTXDataType)1, 31827 },
	{ (Il2CppRGCTXDataType)1, 31825 },
	{ (Il2CppRGCTXDataType)2, 31825 },
	{ (Il2CppRGCTXDataType)1, 31829 },
	{ (Il2CppRGCTXDataType)2, 31828 },
	{ (Il2CppRGCTXDataType)1, 34996 },
	{ (Il2CppRGCTXDataType)2, 34997 },
	{ (Il2CppRGCTXDataType)3, 26212 },
	{ (Il2CppRGCTXDataType)3, 26213 },
	{ (Il2CppRGCTXDataType)3, 26214 },
	{ (Il2CppRGCTXDataType)1, 31828 },
	{ (Il2CppRGCTXDataType)3, 26215 },
	{ (Il2CppRGCTXDataType)2, 34998 },
	{ (Il2CppRGCTXDataType)3, 26216 },
	{ (Il2CppRGCTXDataType)1, 31830 },
	{ (Il2CppRGCTXDataType)2, 31830 },
	{ (Il2CppRGCTXDataType)2, 34999 },
	{ (Il2CppRGCTXDataType)3, 26217 },
	{ (Il2CppRGCTXDataType)1, 31833 },
	{ (Il2CppRGCTXDataType)2, 31833 },
	{ (Il2CppRGCTXDataType)3, 26218 },
	{ (Il2CppRGCTXDataType)3, 26219 },
	{ (Il2CppRGCTXDataType)2, 35000 },
	{ (Il2CppRGCTXDataType)3, 26220 },
	{ (Il2CppRGCTXDataType)1, 35001 },
	{ (Il2CppRGCTXDataType)1, 35002 },
	{ (Il2CppRGCTXDataType)1, 35003 },
	{ (Il2CppRGCTXDataType)2, 35004 },
	{ (Il2CppRGCTXDataType)3, 26221 },
	{ (Il2CppRGCTXDataType)2, 35004 },
	{ (Il2CppRGCTXDataType)2, 31845 },
	{ (Il2CppRGCTXDataType)2, 35005 },
	{ (Il2CppRGCTXDataType)2, 31858 },
	{ (Il2CppRGCTXDataType)2, 35006 },
	{ (Il2CppRGCTXDataType)3, 26222 },
	{ (Il2CppRGCTXDataType)3, 26223 },
	{ (Il2CppRGCTXDataType)1, 31867 },
	{ (Il2CppRGCTXDataType)2, 31867 },
	{ (Il2CppRGCTXDataType)2, 35007 },
	{ (Il2CppRGCTXDataType)3, 26224 },
	{ (Il2CppRGCTXDataType)3, 26225 },
	{ (Il2CppRGCTXDataType)2, 31878 },
	{ (Il2CppRGCTXDataType)3, 26226 },
	{ (Il2CppRGCTXDataType)2, 31879 },
	{ (Il2CppRGCTXDataType)2, 31878 },
	{ (Il2CppRGCTXDataType)3, 26227 },
	{ (Il2CppRGCTXDataType)3, 26228 },
	{ (Il2CppRGCTXDataType)3, 26229 },
	{ (Il2CppRGCTXDataType)1, 31879 },
	{ (Il2CppRGCTXDataType)3, 26230 },
	{ (Il2CppRGCTXDataType)2, 31883 },
	{ (Il2CppRGCTXDataType)3, 26231 },
	{ (Il2CppRGCTXDataType)2, 35008 },
	{ (Il2CppRGCTXDataType)3, 26232 },
	{ (Il2CppRGCTXDataType)3, 24274 },
	{ (Il2CppRGCTXDataType)2, 31885 },
	{ (Il2CppRGCTXDataType)3, 26233 },
	{ (Il2CppRGCTXDataType)3, 24272 },
	{ (Il2CppRGCTXDataType)3, 26234 },
	{ (Il2CppRGCTXDataType)3, 24275 },
	{ (Il2CppRGCTXDataType)3, 26235 },
	{ (Il2CppRGCTXDataType)3, 26236 },
	{ (Il2CppRGCTXDataType)3, 26237 },
	{ (Il2CppRGCTXDataType)3, 26238 },
	{ (Il2CppRGCTXDataType)2, 35009 },
	{ (Il2CppRGCTXDataType)3, 26239 },
	{ (Il2CppRGCTXDataType)3, 26240 },
	{ (Il2CppRGCTXDataType)3, 26241 },
	{ (Il2CppRGCTXDataType)3, 26242 },
	{ (Il2CppRGCTXDataType)3, 26243 },
	{ (Il2CppRGCTXDataType)3, 26244 },
	{ (Il2CppRGCTXDataType)3, 24273 },
	{ (Il2CppRGCTXDataType)2, 35010 },
	{ (Il2CppRGCTXDataType)3, 26245 },
	{ (Il2CppRGCTXDataType)2, 35011 },
	{ (Il2CppRGCTXDataType)3, 26246 },
	{ (Il2CppRGCTXDataType)3, 26247 },
	{ (Il2CppRGCTXDataType)3, 26248 },
	{ (Il2CppRGCTXDataType)3, 26249 },
	{ (Il2CppRGCTXDataType)3, 26250 },
	{ (Il2CppRGCTXDataType)3, 26251 },
	{ (Il2CppRGCTXDataType)3, 26252 },
	{ (Il2CppRGCTXDataType)3, 26253 },
	{ (Il2CppRGCTXDataType)3, 26254 },
	{ (Il2CppRGCTXDataType)3, 26255 },
	{ (Il2CppRGCTXDataType)3, 26256 },
	{ (Il2CppRGCTXDataType)3, 26257 },
	{ (Il2CppRGCTXDataType)3, 26258 },
	{ (Il2CppRGCTXDataType)3, 26259 },
	{ (Il2CppRGCTXDataType)3, 26260 },
	{ (Il2CppRGCTXDataType)2, 35012 },
	{ (Il2CppRGCTXDataType)3, 26261 },
	{ (Il2CppRGCTXDataType)2, 35013 },
	{ (Il2CppRGCTXDataType)3, 26262 },
	{ (Il2CppRGCTXDataType)3, 26263 },
	{ (Il2CppRGCTXDataType)3, 26264 },
	{ (Il2CppRGCTXDataType)3, 26265 },
	{ (Il2CppRGCTXDataType)2, 35014 },
	{ (Il2CppRGCTXDataType)3, 26266 },
	{ (Il2CppRGCTXDataType)2, 35015 },
	{ (Il2CppRGCTXDataType)3, 26267 },
	{ (Il2CppRGCTXDataType)2, 31914 },
	{ (Il2CppRGCTXDataType)3, 26268 },
	{ (Il2CppRGCTXDataType)2, 31913 },
	{ (Il2CppRGCTXDataType)3, 26269 },
	{ (Il2CppRGCTXDataType)3, 26270 },
	{ (Il2CppRGCTXDataType)2, 35016 },
	{ (Il2CppRGCTXDataType)3, 26271 },
	{ (Il2CppRGCTXDataType)3, 26272 },
	{ (Il2CppRGCTXDataType)2, 31915 },
	{ (Il2CppRGCTXDataType)3, 26273 },
	{ (Il2CppRGCTXDataType)2, 35017 },
	{ (Il2CppRGCTXDataType)3, 26274 },
	{ (Il2CppRGCTXDataType)3, 26275 },
	{ (Il2CppRGCTXDataType)2, 31917 },
	{ (Il2CppRGCTXDataType)3, 26276 },
	{ (Il2CppRGCTXDataType)2, 35018 },
	{ (Il2CppRGCTXDataType)3, 26277 },
	{ (Il2CppRGCTXDataType)3, 26278 },
	{ (Il2CppRGCTXDataType)2, 31920 },
	{ (Il2CppRGCTXDataType)3, 26279 },
	{ (Il2CppRGCTXDataType)2, 35019 },
	{ (Il2CppRGCTXDataType)3, 26280 },
	{ (Il2CppRGCTXDataType)3, 26281 },
	{ (Il2CppRGCTXDataType)2, 31922 },
	{ (Il2CppRGCTXDataType)3, 26282 },
	{ (Il2CppRGCTXDataType)2, 35020 },
	{ (Il2CppRGCTXDataType)3, 26283 },
	{ (Il2CppRGCTXDataType)3, 26284 },
	{ (Il2CppRGCTXDataType)2, 31925 },
	{ (Il2CppRGCTXDataType)3, 26285 },
	{ (Il2CppRGCTXDataType)2, 35021 },
	{ (Il2CppRGCTXDataType)3, 26286 },
	{ (Il2CppRGCTXDataType)3, 26287 },
	{ (Il2CppRGCTXDataType)2, 31927 },
	{ (Il2CppRGCTXDataType)3, 26288 },
	{ (Il2CppRGCTXDataType)2, 35022 },
	{ (Il2CppRGCTXDataType)3, 26289 },
	{ (Il2CppRGCTXDataType)3, 26290 },
	{ (Il2CppRGCTXDataType)2, 31929 },
	{ (Il2CppRGCTXDataType)3, 26291 },
	{ (Il2CppRGCTXDataType)2, 35023 },
	{ (Il2CppRGCTXDataType)3, 26292 },
	{ (Il2CppRGCTXDataType)3, 26293 },
	{ (Il2CppRGCTXDataType)2, 31931 },
	{ (Il2CppRGCTXDataType)3, 26294 },
	{ (Il2CppRGCTXDataType)2, 35024 },
	{ (Il2CppRGCTXDataType)3, 26295 },
	{ (Il2CppRGCTXDataType)3, 26296 },
	{ (Il2CppRGCTXDataType)2, 31934 },
	{ (Il2CppRGCTXDataType)3, 26297 },
	{ (Il2CppRGCTXDataType)1, 31937 },
	{ (Il2CppRGCTXDataType)2, 31937 },
	{ (Il2CppRGCTXDataType)2, 35025 },
	{ (Il2CppRGCTXDataType)3, 26298 },
	{ (Il2CppRGCTXDataType)1, 31941 },
	{ (Il2CppRGCTXDataType)3, 26299 },
	{ (Il2CppRGCTXDataType)2, 31940 },
	{ (Il2CppRGCTXDataType)3, 26300 },
	{ (Il2CppRGCTXDataType)2, 35026 },
	{ (Il2CppRGCTXDataType)3, 26301 },
	{ (Il2CppRGCTXDataType)1, 31944 },
	{ (Il2CppRGCTXDataType)1, 31943 },
	{ (Il2CppRGCTXDataType)3, 26302 },
	{ (Il2CppRGCTXDataType)2, 31942 },
	{ (Il2CppRGCTXDataType)3, 26303 },
	{ (Il2CppRGCTXDataType)2, 35027 },
	{ (Il2CppRGCTXDataType)3, 26304 },
	{ (Il2CppRGCTXDataType)1, 31946 },
	{ (Il2CppRGCTXDataType)3, 26305 },
	{ (Il2CppRGCTXDataType)2, 31945 },
	{ (Il2CppRGCTXDataType)3, 26306 },
	{ (Il2CppRGCTXDataType)2, 35028 },
	{ (Il2CppRGCTXDataType)3, 26307 },
	{ (Il2CppRGCTXDataType)1, 31949 },
	{ (Il2CppRGCTXDataType)1, 31948 },
	{ (Il2CppRGCTXDataType)3, 26308 },
	{ (Il2CppRGCTXDataType)2, 31947 },
	{ (Il2CppRGCTXDataType)3, 26309 },
	{ (Il2CppRGCTXDataType)1, 31951 },
	{ (Il2CppRGCTXDataType)1, 31950 },
	{ (Il2CppRGCTXDataType)2, 31950 },
	{ (Il2CppRGCTXDataType)1, 31953 },
	{ (Il2CppRGCTXDataType)1, 31952 },
	{ (Il2CppRGCTXDataType)2, 31952 },
	{ (Il2CppRGCTXDataType)2, 35029 },
	{ (Il2CppRGCTXDataType)3, 26310 },
	{ (Il2CppRGCTXDataType)3, 26311 },
	{ (Il2CppRGCTXDataType)2, 31955 },
	{ (Il2CppRGCTXDataType)3, 26312 },
	{ (Il2CppRGCTXDataType)2, 35030 },
	{ (Il2CppRGCTXDataType)3, 26313 },
	{ (Il2CppRGCTXDataType)3, 26314 },
	{ (Il2CppRGCTXDataType)2, 31957 },
	{ (Il2CppRGCTXDataType)3, 26315 },
	{ (Il2CppRGCTXDataType)2, 31975 },
	{ (Il2CppRGCTXDataType)2, 31980 },
	{ (Il2CppRGCTXDataType)2, 31986 },
	{ (Il2CppRGCTXDataType)2, 31987 },
	{ (Il2CppRGCTXDataType)2, 31990 },
	{ (Il2CppRGCTXDataType)1, 31996 },
	{ (Il2CppRGCTXDataType)2, 31996 },
	{ (Il2CppRGCTXDataType)2, 31997 },
	{ (Il2CppRGCTXDataType)2, 32000 },
	{ (Il2CppRGCTXDataType)2, 32009 },
	{ (Il2CppRGCTXDataType)2, 32012 },
	{ (Il2CppRGCTXDataType)1, 32016 },
	{ (Il2CppRGCTXDataType)2, 32016 },
	{ (Il2CppRGCTXDataType)2, 32017 },
	{ (Il2CppRGCTXDataType)2, 32021 },
	{ (Il2CppRGCTXDataType)2, 32022 },
	{ (Il2CppRGCTXDataType)2, 32025 },
	{ (Il2CppRGCTXDataType)2, 32030 },
	{ (Il2CppRGCTXDataType)2, 32031 },
	{ (Il2CppRGCTXDataType)2, 32034 },
	{ (Il2CppRGCTXDataType)2, 32037 },
	{ (Il2CppRGCTXDataType)2, 32038 },
	{ (Il2CppRGCTXDataType)2, 32042 },
	{ (Il2CppRGCTXDataType)2, 32046 },
	{ (Il2CppRGCTXDataType)2, 32047 },
	{ (Il2CppRGCTXDataType)2, 34524 },
	{ (Il2CppRGCTXDataType)3, 26316 },
	{ (Il2CppRGCTXDataType)2, 32071 },
	{ (Il2CppRGCTXDataType)2, 32070 },
	{ (Il2CppRGCTXDataType)2, 34525 },
	{ (Il2CppRGCTXDataType)2, 32072 },
	{ (Il2CppRGCTXDataType)3, 26317 },
	{ (Il2CppRGCTXDataType)2, 32081 },
	{ (Il2CppRGCTXDataType)2, 34526 },
	{ (Il2CppRGCTXDataType)3, 26318 },
	{ (Il2CppRGCTXDataType)3, 26319 },
	{ (Il2CppRGCTXDataType)2, 32082 },
	{ (Il2CppRGCTXDataType)2, 32090 },
	{ (Il2CppRGCTXDataType)3, 26320 },
	{ (Il2CppRGCTXDataType)3, 26321 },
	{ (Il2CppRGCTXDataType)3, 26322 },
	{ (Il2CppRGCTXDataType)2, 34527 },
	{ (Il2CppRGCTXDataType)3, 26323 },
	{ (Il2CppRGCTXDataType)2, 32083 },
	{ (Il2CppRGCTXDataType)3, 26324 },
	{ (Il2CppRGCTXDataType)3, 26325 },
	{ (Il2CppRGCTXDataType)2, 32099 },
	{ (Il2CppRGCTXDataType)2, 35031 },
	{ (Il2CppRGCTXDataType)3, 26326 },
	{ (Il2CppRGCTXDataType)2, 35031 },
	{ (Il2CppRGCTXDataType)3, 26327 },
	{ (Il2CppRGCTXDataType)1, 32109 },
	{ (Il2CppRGCTXDataType)2, 32108 },
	{ (Il2CppRGCTXDataType)3, 26328 },
	{ (Il2CppRGCTXDataType)1, 32113 },
};
extern const Il2CppCodeGenModule g_Sirenix_SerializationCodeGenModule;
const Il2CppCodeGenModule g_Sirenix_SerializationCodeGenModule = 
{
	"Sirenix.Serialization.dll",
	1758,
	s_methodPointers,
	7,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	138,
	s_rgctxIndices,
	750,
	s_rgctxValues,
	NULL,
};
