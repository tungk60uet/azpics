﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.Texture2D Lean.Common.LeanGuide::get_Icon()
extern void LeanGuide_get_Icon_mC342CBBC9F940DE4969117F8EDE3592FEC4E1322 (void);
// 0x00000002 System.String Lean.Common.LeanGuide::get_Version()
extern void LeanGuide_get_Version_m02E6AAB2788ED5F2855F772C2C0731C5294F0191 (void);
// 0x00000003 System.Void Lean.Common.LeanGuide::.ctor()
extern void LeanGuide__ctor_m812A17DD463D4BAAE0A5D27500854ABAAEBD2D0B (void);
// 0x00000004 System.Void Lean.Common.LeanDestroy::set_Execute(Lean.Common.LeanDestroy/ExecuteType)
extern void LeanDestroy_set_Execute_m166BB1894E785581AFFC0BA7576561BDB947881B (void);
// 0x00000005 Lean.Common.LeanDestroy/ExecuteType Lean.Common.LeanDestroy::get_Execute()
extern void LeanDestroy_get_Execute_mDF5B6C3010D28D94C421C661EB38BB55EC337610 (void);
// 0x00000006 System.Void Lean.Common.LeanDestroy::set_Target(UnityEngine.GameObject)
extern void LeanDestroy_set_Target_m9A67362AA7C590B37EC62EAFB35E3F7724B96BD2 (void);
// 0x00000007 UnityEngine.GameObject Lean.Common.LeanDestroy::get_Target()
extern void LeanDestroy_get_Target_m3D9BE220570285E3990EE27669F57358E592D20B (void);
// 0x00000008 System.Void Lean.Common.LeanDestroy::set_Seconds(System.Single)
extern void LeanDestroy_set_Seconds_m8B39597D27B1E69D1C4DB7A97589C0C07B7431F5 (void);
// 0x00000009 System.Single Lean.Common.LeanDestroy::get_Seconds()
extern void LeanDestroy_get_Seconds_m77450A6391D2261867108946DEEF490D81F4EE4F (void);
// 0x0000000A System.Void Lean.Common.LeanDestroy::Update()
extern void LeanDestroy_Update_m47E888DE4CA9C306FF1FD2EAC50FF91B51A6BFEA (void);
// 0x0000000B System.Void Lean.Common.LeanDestroy::DestroyNow()
extern void LeanDestroy_DestroyNow_m7E586E917761D888672AE1E9608FEB3F376C4FD9 (void);
// 0x0000000C System.Void Lean.Common.LeanDestroy::.ctor()
extern void LeanDestroy__ctor_mF329265FBF8E6C0C8BA74E311CC77C9D19541B97 (void);
// 0x0000000D System.Void Lean.Common.LeanFormatString::set_Format(System.String)
extern void LeanFormatString_set_Format_m1A8DF72D34D95207338BC9AB5FA93878390C19A8 (void);
// 0x0000000E System.String Lean.Common.LeanFormatString::get_Format()
extern void LeanFormatString_get_Format_mAC38BD817CAB8D75CAB68AD38A89E99B594F1125 (void);
// 0x0000000F Lean.Common.LeanFormatString/StringEvent Lean.Common.LeanFormatString::get_OnString()
extern void LeanFormatString_get_OnString_mC06F68BC48907A55B7CF7874345D46F3C888D74A (void);
// 0x00000010 System.Void Lean.Common.LeanFormatString::SetString(System.String)
extern void LeanFormatString_SetString_m45E1096AE743D8FA7102EA84C47E3DF5B48A0BD7 (void);
// 0x00000011 System.Void Lean.Common.LeanFormatString::SetString(System.String,System.String)
extern void LeanFormatString_SetString_m4ABDC36976ABBC4429628FB6FC76415D10634D4E (void);
// 0x00000012 System.Void Lean.Common.LeanFormatString::SetString(System.Int32)
extern void LeanFormatString_SetString_mD15B3F51A8C01B6ADCE687BD48B843CDBFAB20FF (void);
// 0x00000013 System.Void Lean.Common.LeanFormatString::SetString(System.Int32,System.Int32)
extern void LeanFormatString_SetString_m0463D960C5187144FD2EEA3CEBA161B873B6694A (void);
// 0x00000014 System.Void Lean.Common.LeanFormatString::SetString(System.Single)
extern void LeanFormatString_SetString_m60F9CE4BF05D9D64167D03E63B67EF04513220E2 (void);
// 0x00000015 System.Void Lean.Common.LeanFormatString::SetString(System.Single,System.Single)
extern void LeanFormatString_SetString_mCA14D1D089FD92896B0ECAC0EF05248076BA5D33 (void);
// 0x00000016 System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector2)
extern void LeanFormatString_SetString_m62289D8F2EC52CCDE686883C5A525299CCC44BF8 (void);
// 0x00000017 System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFormatString_SetString_m1CE75E61E22F90A3C43B678782060D432318BCD7 (void);
// 0x00000018 System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector3)
extern void LeanFormatString_SetString_m25CDE010B45BE09BF698B52F7918475CF04A2813 (void);
// 0x00000019 System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanFormatString_SetString_mAAD50D4158C11E62BA307735397279BD43D7E1DF (void);
// 0x0000001A System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector4)
extern void LeanFormatString_SetString_m16F58EF81DAC87AFE1C66EDB2CCFCD803BAF1413 (void);
// 0x0000001B System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector4,UnityEngine.Vector4)
extern void LeanFormatString_SetString_m48DC2444BE823EAC0443F9E8B977400FD27EAE02 (void);
// 0x0000001C System.Void Lean.Common.LeanFormatString::SetString(System.Single,System.Int32)
extern void LeanFormatString_SetString_mAEBE5C5139D6D13CB8D14CAF2E796E32406D574A (void);
// 0x0000001D System.Void Lean.Common.LeanFormatString::SetString(System.Int32,System.Single)
extern void LeanFormatString_SetString_mCDA901FA8B6E112066EFFCE75B6967AE43614C79 (void);
// 0x0000001E System.Void Lean.Common.LeanFormatString::SendString(System.Object)
extern void LeanFormatString_SendString_m6727C6A1A5FB981B278A50F4688AC5F839FE29BC (void);
// 0x0000001F System.Void Lean.Common.LeanFormatString::SendString(System.Object,System.Object)
extern void LeanFormatString_SendString_m78107E8A777A4D190284899C87C271E32C241465 (void);
// 0x00000020 System.Void Lean.Common.LeanFormatString::.ctor()
extern void LeanFormatString__ctor_mADCE8AF8902D2213194D2145A36B435DAB13BA33 (void);
// 0x00000021 System.Int32 Lean.Common.LeanPath::get_PointCount()
extern void LeanPath_get_PointCount_m791BDBACDBD2531ED87C29D19F2D3F23BA6B9F6E (void);
// 0x00000022 System.Int32 Lean.Common.LeanPath::GetPointCount(System.Int32)
extern void LeanPath_GetPointCount_mF59EF4AF02F472309A48259BF595EE5AB1AA4377 (void);
// 0x00000023 UnityEngine.Vector3 Lean.Common.LeanPath::GetSmoothedPoint(System.Single)
extern void LeanPath_GetSmoothedPoint_m1C037E2A1C0B3F80235E16928B717C0198B79991 (void);
// 0x00000024 UnityEngine.Vector3 Lean.Common.LeanPath::GetPoint(System.Int32,System.Int32)
extern void LeanPath_GetPoint_m02C8B39AD5A8C857ECFCA386CFC7E63AD913BADB (void);
// 0x00000025 UnityEngine.Vector3 Lean.Common.LeanPath::GetPointRaw(System.Int32,System.Int32)
extern void LeanPath_GetPointRaw_mBF4D849132319266EB84FF8AC3CF1028F8ABFF8F (void);
// 0x00000026 System.Void Lean.Common.LeanPath::SetLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_SetLine_mFC8AEABE182D201BB75B490E324129E338F26D7B (void);
// 0x00000027 System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
extern void LeanPath_TryGetClosest_mCB41AD274F16628F3A3E16184CE71708990CAA09 (void);
// 0x00000028 System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32)
extern void LeanPath_TryGetClosest_mF340113774CF9E1B7BCA506074FF1587854A1EFE (void);
// 0x00000029 System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
extern void LeanPath_TryGetClosest_m9AA350EB963B9DC45761F934FF0E28559CFEFEE0 (void);
// 0x0000002A System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32)
extern void LeanPath_TryGetClosest_m2E6F1BDA9491E4876333F779AA23BCBAB13F6D78 (void);
// 0x0000002B System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32,System.Single)
extern void LeanPath_TryGetClosest_m4B1869B258D46F984DBBA2193DC121C27D72B4DC (void);
// 0x0000002C UnityEngine.Vector3 Lean.Common.LeanPath::GetClosestPoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_GetClosestPoint_mEBEE16114D59404DF10CF75E5D121376887B2E48 (void);
// 0x0000002D UnityEngine.Vector3 Lean.Common.LeanPath::GetClosestPoint(UnityEngine.Ray,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_GetClosestPoint_m24B13DFD725ECDC0E2A9FB8657788218A32F4774 (void);
// 0x0000002E System.Single Lean.Common.LeanPath::GetClosestDistance(UnityEngine.Ray,UnityEngine.Vector3)
extern void LeanPath_GetClosestDistance_mDC5293908E288BB5D4CC6A2E40750AFDD44556B8 (void);
// 0x0000002F System.Int32 Lean.Common.LeanPath::Mod(System.Int32,System.Int32)
extern void LeanPath_Mod_mB84DC1E281CABBC97F349FBFB66D08C7AAB715A2 (void);
// 0x00000030 System.Single Lean.Common.LeanPath::CubicInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanPath_CubicInterpolate_m61DD0A87B85DFFE79BB4E7BB8AF056456F6EDD4B (void);
// 0x00000031 System.Void Lean.Common.LeanPath::UpdateVisual()
extern void LeanPath_UpdateVisual_m298B3611F78B78A6753EBC730374ACDA28DEB986 (void);
// 0x00000032 System.Void Lean.Common.LeanPath::Update()
extern void LeanPath_Update_mD5420A5DE1E834C45495183036D3662D09481821 (void);
// 0x00000033 System.Void Lean.Common.LeanPath::.ctor()
extern void LeanPath__ctor_mC1A4DB1E2526EDB5E65E47CF709C3049C9A0560A (void);
// 0x00000034 System.Void Lean.Common.LeanPath::.cctor()
extern void LeanPath__cctor_mEA3C136DB3CE56029B08B58D9C06D2FB22191D0B (void);
// 0x00000035 UnityEngine.Vector3 Lean.Common.LeanPlane::GetClosest(UnityEngine.Vector3,System.Single)
extern void LeanPlane_GetClosest_m3922292E2474E87C6D0BE0528BB50DC76923B28D (void);
// 0x00000036 System.Boolean Lean.Common.LeanPlane::TryRaycast(UnityEngine.Ray,UnityEngine.Vector3&,System.Single,System.Boolean)
extern void LeanPlane_TryRaycast_m547EA5F3CB30946C43E90C6B737E5225AA6B5CED (void);
// 0x00000037 UnityEngine.Vector3 Lean.Common.LeanPlane::GetClosest(UnityEngine.Ray,System.Single)
extern void LeanPlane_GetClosest_mEB4174B1E168745B01869061732CCCB114A45154 (void);
// 0x00000038 System.Boolean Lean.Common.LeanPlane::RayToPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Ray,System.Single&)
extern void LeanPlane_RayToPlane_m9A5ED668725827F0707403CEFF8B6C54851DB636 (void);
// 0x00000039 System.Void Lean.Common.LeanPlane::.ctor()
extern void LeanPlane__ctor_m3C89D5F3B03A8E1CBD3CB37A43A35AD35E1A2B77 (void);
// 0x0000003A System.Void Lean.Common.LeanRoll::set_Angle(System.Single)
extern void LeanRoll_set_Angle_m9CCEAECBE1A1D159402D0CE60D97ABB681B54654 (void);
// 0x0000003B System.Single Lean.Common.LeanRoll::get_Angle()
extern void LeanRoll_get_Angle_mEF25A957199E03C1BE10AFE3EF8DA0046864DCDD (void);
// 0x0000003C System.Void Lean.Common.LeanRoll::set_Clamp(System.Boolean)
extern void LeanRoll_set_Clamp_m35C56C528885DE03A6137F33521F5D2672A9306C (void);
// 0x0000003D System.Boolean Lean.Common.LeanRoll::get_Clamp()
extern void LeanRoll_get_Clamp_mBCACB1E0EB55AA6F4B4A8218E5708340A4519242 (void);
// 0x0000003E System.Void Lean.Common.LeanRoll::set_ClampMin(System.Single)
extern void LeanRoll_set_ClampMin_m7E8EF2576AF2E0897981CCF6F7C42B7F50C637CB (void);
// 0x0000003F System.Single Lean.Common.LeanRoll::get_ClampMin()
extern void LeanRoll_get_ClampMin_mD33270CB88752B1C062F6693EA872AC87775921C (void);
// 0x00000040 System.Void Lean.Common.LeanRoll::set_ClampMax(System.Single)
extern void LeanRoll_set_ClampMax_m22A62AF9FBE127F9790F4BCCFC2C98C9CE501995 (void);
// 0x00000041 System.Single Lean.Common.LeanRoll::get_ClampMax()
extern void LeanRoll_get_ClampMax_m0A9CA375F49385B07BB318344D209DFF8BB7DB08 (void);
// 0x00000042 System.Void Lean.Common.LeanRoll::set_Damping(System.Single)
extern void LeanRoll_set_Damping_m4BD7D5A8EEA96030355BC2CCA97DBAB7142EAA9B (void);
// 0x00000043 System.Single Lean.Common.LeanRoll::get_Damping()
extern void LeanRoll_get_Damping_m2243F120C54A7923A20F3590C62FAECA926A7C51 (void);
// 0x00000044 System.Void Lean.Common.LeanRoll::IncrementAngle(System.Single)
extern void LeanRoll_IncrementAngle_m59632C0B0CA6126DDAA1F1894246461108617428 (void);
// 0x00000045 System.Void Lean.Common.LeanRoll::DecrementAngle(System.Single)
extern void LeanRoll_DecrementAngle_m5C6918E2DC895EFD41E6F28971C399A274BDE2AD (void);
// 0x00000046 System.Void Lean.Common.LeanRoll::RotateToDelta(UnityEngine.Vector2)
extern void LeanRoll_RotateToDelta_mB29EAA00ACC86D28C0FDCDE244ADD9F230FCB563 (void);
// 0x00000047 System.Void Lean.Common.LeanRoll::SnapToTarget()
extern void LeanRoll_SnapToTarget_mBC7C23B4BEEE20D405A21F979C92CFC56FBD1639 (void);
// 0x00000048 System.Void Lean.Common.LeanRoll::Start()
extern void LeanRoll_Start_m568D1FAB9E92BC3E9842E91EEB20394867E5A3DD (void);
// 0x00000049 System.Void Lean.Common.LeanRoll::Update()
extern void LeanRoll_Update_m812350F2283BDFDC496278B524F08B89442A0D33 (void);
// 0x0000004A System.Void Lean.Common.LeanRoll::.ctor()
extern void LeanRoll__ctor_mC47CB7FA9F21BE46A39B015D0FC0281484AA44EE (void);
// 0x0000004B System.Void Lean.Common.LeanSelect::set_DeselectWithNothing(System.Boolean)
extern void LeanSelect_set_DeselectWithNothing_mFB401D6A3438FB45CB8EA2F973E74FDDB41A098F (void);
// 0x0000004C System.Boolean Lean.Common.LeanSelect::get_DeselectWithNothing()
extern void LeanSelect_get_DeselectWithNothing_m89E5744A57BA7E076219A915628F9688E3BF6D4B (void);
// 0x0000004D System.Void Lean.Common.LeanSelect::set_Limit(Lean.Common.LeanSelect/LimitType)
extern void LeanSelect_set_Limit_m4BF8CD6AD4DD5328E3DF61B1A596C645E7BE4315 (void);
// 0x0000004E Lean.Common.LeanSelect/LimitType Lean.Common.LeanSelect::get_Limit()
extern void LeanSelect_get_Limit_m5D80EF0695B89E4FD2CC8D12B61CFECF192D4EE5 (void);
// 0x0000004F System.Void Lean.Common.LeanSelect::set_MaxSelectables(System.Int32)
extern void LeanSelect_set_MaxSelectables_m8FC1FBF4CCC208ECE099FC62D70022839A486E07 (void);
// 0x00000050 System.Int32 Lean.Common.LeanSelect::get_MaxSelectables()
extern void LeanSelect_get_MaxSelectables_mAB025390799B6CA0E149E7EC85FAD8C508C40ECA (void);
// 0x00000051 System.Void Lean.Common.LeanSelect::set_Reselect(Lean.Common.LeanSelect/ReselectType)
extern void LeanSelect_set_Reselect_m074DE09F6455D6482A7B5365AFD68920E878A887 (void);
// 0x00000052 Lean.Common.LeanSelect/ReselectType Lean.Common.LeanSelect::get_Reselect()
extern void LeanSelect_get_Reselect_m29035A9BCA19C651E99A7405AD66F8CCDF976D83 (void);
// 0x00000053 System.Collections.Generic.List`1<Lean.Common.LeanSelectable> Lean.Common.LeanSelect::get_Selectables()
extern void LeanSelect_get_Selectables_m886E41F694C8E7F63E0F16E65B39A815C70EA531 (void);
// 0x00000054 Lean.Common.LeanSelect/LeanSelectableEvent Lean.Common.LeanSelect::get_OnSelected()
extern void LeanSelect_get_OnSelected_m10D4E6ADB4883F58BCD5647989DE526B8F6A4350 (void);
// 0x00000055 Lean.Common.LeanSelect/LeanSelectableEvent Lean.Common.LeanSelect::get_OnDeselected()
extern void LeanSelect_get_OnDeselected_m2FC13636EAE046B8F265784F8BD980E4A3C377CF (void);
// 0x00000056 UnityEngine.Events.UnityEvent Lean.Common.LeanSelect::get_OnNothing()
extern void LeanSelect_get_OnNothing_m96F976DCFE7628CB0C4E951A8568D66F6BB4FD68 (void);
// 0x00000057 System.Void Lean.Common.LeanSelect::add_OnAnySelected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelect_add_OnAnySelected_m3721278466D05C31AB52DB92D253710A0FF5BFFF (void);
// 0x00000058 System.Void Lean.Common.LeanSelect::remove_OnAnySelected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelect_remove_OnAnySelected_m55111EBC17E5401F0BFF09CEA81E36ECEE6C5441 (void);
// 0x00000059 System.Void Lean.Common.LeanSelect::add_OnAnyDeselected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelect_add_OnAnyDeselected_mC254A7B797EA017BFDD5F75D778129179D158AF5 (void);
// 0x0000005A System.Void Lean.Common.LeanSelect::remove_OnAnyDeselected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelect_remove_OnAnyDeselected_m4F1A0BB681081D3586CD7D2DCEF3E297B47BDF55 (void);
// 0x0000005B System.Boolean Lean.Common.LeanSelect::IsSelected(Lean.Common.LeanSelectable)
extern void LeanSelect_IsSelected_mFB16947AF24E5DA9489C8A29CB78953868CFC654 (void);
// 0x0000005C System.Void Lean.Common.LeanSelect::Select(Lean.Common.LeanSelectable)
extern void LeanSelect_Select_m5D0C01A61F8259EA1352B327A0E1D7110B274746 (void);
// 0x0000005D System.Void Lean.Common.LeanSelect::Deselect(Lean.Common.LeanSelectable)
extern void LeanSelect_Deselect_mFFACE713654CC54EE11EB863FBC9D65C81CEA2C7 (void);
// 0x0000005E System.Boolean Lean.Common.LeanSelect::TrySelect(Lean.Common.LeanSelectable)
extern void LeanSelect_TrySelect_m9E46D0E5E7D824DB3DA8C36880C2A1561523975F (void);
// 0x0000005F System.Boolean Lean.Common.LeanSelect::TryReselect(Lean.Common.LeanSelectable)
extern void LeanSelect_TryReselect_m831BC63CCCB2E3B2F26382756B495E1FA0CAF06F (void);
// 0x00000060 System.Boolean Lean.Common.LeanSelect::TryDeselect(Lean.Common.LeanSelectable)
extern void LeanSelect_TryDeselect_mD6DEE075EBBFB738D943BF97C4FB82D9D2011EA1 (void);
// 0x00000061 System.Boolean Lean.Common.LeanSelect::TryDeselect(System.Int32)
extern void LeanSelect_TryDeselect_m84B8A9AAD66075B94434DAC6F6786A42F345FDFA (void);
// 0x00000062 System.Void Lean.Common.LeanSelect::DeselectAll()
extern void LeanSelect_DeselectAll_mFE184416CE6E63B5A0D39E595C06148896350BF3 (void);
// 0x00000063 System.Void Lean.Common.LeanSelect::Cull(System.Int32)
extern void LeanSelect_Cull_mF4ED48BA46DBA9DDFE99D792EF1B996F94E5079E (void);
// 0x00000064 System.Void Lean.Common.LeanSelect::OnEnable()
extern void LeanSelect_OnEnable_mB3E53DBEA024A1D81098384C4E2AA34C363ABC6B (void);
// 0x00000065 System.Void Lean.Common.LeanSelect::OnDisable()
extern void LeanSelect_OnDisable_m3806A8D29811C699B87E676F1FDC41681E86D277 (void);
// 0x00000066 System.Void Lean.Common.LeanSelect::OnDestroy()
extern void LeanSelect_OnDestroy_m1D1C5E3485D9068A95F322F4D82879E88943E188 (void);
// 0x00000067 System.Void Lean.Common.LeanSelect::.ctor()
extern void LeanSelect__ctor_mAA1BDF7B0E302EA7CC43505DED75AA7CFD6736FD (void);
// 0x00000068 System.Void Lean.Common.LeanSelect::.cctor()
extern void LeanSelect__cctor_m0B9EBE0C86D35095D85146A2AB8A24B8B6A8293D (void);
// 0x00000069 System.Void Lean.Common.LeanSelectable::set_SelfSelected(System.Boolean)
extern void LeanSelectable_set_SelfSelected_m084F86106F53D2AAD073AA876197EC6336920549 (void);
// 0x0000006A System.Boolean Lean.Common.LeanSelectable::get_SelfSelected()
extern void LeanSelectable_get_SelfSelected_mE941DA0A15A40FCB21693F63EF613DFE1545A907 (void);
// 0x0000006B UnityEngine.Events.UnityEvent Lean.Common.LeanSelectable::get_OnSelected()
extern void LeanSelectable_get_OnSelected_m017D009660A677B55DEB2D6FF9864AE63C5844A4 (void);
// 0x0000006C UnityEngine.Events.UnityEvent Lean.Common.LeanSelectable::get_OnDeselected()
extern void LeanSelectable_get_OnDeselected_mA32DFC4FD8CCA385E5A46ADB7CB3D42155708132 (void);
// 0x0000006D System.Void Lean.Common.LeanSelectable::add_OnAnyEnabled(System.Action`1<Lean.Common.LeanSelectable>)
extern void LeanSelectable_add_OnAnyEnabled_mC4CDF0392B095B16F3B058B1FE997EE328603C1F (void);
// 0x0000006E System.Void Lean.Common.LeanSelectable::remove_OnAnyEnabled(System.Action`1<Lean.Common.LeanSelectable>)
extern void LeanSelectable_remove_OnAnyEnabled_mD6EB75344F7B1C45BA0BB4A0756AD44C8C96D49E (void);
// 0x0000006F System.Void Lean.Common.LeanSelectable::add_OnAnyDisabled(System.Action`1<Lean.Common.LeanSelectable>)
extern void LeanSelectable_add_OnAnyDisabled_mC385BA7B9D3A51D60FC6D03C64FA0571DDADF213 (void);
// 0x00000070 System.Void Lean.Common.LeanSelectable::remove_OnAnyDisabled(System.Action`1<Lean.Common.LeanSelectable>)
extern void LeanSelectable_remove_OnAnyDisabled_mBCF7A98ED552441C6C1761BA6DC21462BE9B7ED8 (void);
// 0x00000071 System.Void Lean.Common.LeanSelectable::add_OnAnySelected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelectable_add_OnAnySelected_mC73BD4ACC56FF10CD0FA79633EBEF053AA1DE278 (void);
// 0x00000072 System.Void Lean.Common.LeanSelectable::remove_OnAnySelected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelectable_remove_OnAnySelected_m2EB3510123B29A3FA225E739BC34AA58A44B04F4 (void);
// 0x00000073 System.Void Lean.Common.LeanSelectable::add_OnAnyDeselected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelectable_add_OnAnyDeselected_m64ED1525EE1489C21D04AEB203E7E464D09402A9 (void);
// 0x00000074 System.Void Lean.Common.LeanSelectable::remove_OnAnyDeselected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelectable_remove_OnAnyDeselected_mEDF24C0C5EDCE5119F8D18430926C7172481689B (void);
// 0x00000075 System.Int32 Lean.Common.LeanSelectable::get_SelectedCount()
extern void LeanSelectable_get_SelectedCount_mB857FEE53599D836886BDC77045B29BB2976CA7A (void);
// 0x00000076 System.Boolean Lean.Common.LeanSelectable::get_IsSelected()
extern void LeanSelectable_get_IsSelected_mD364318D1E43F650D1F1B9AD6041B2E60B1F9C91 (void);
// 0x00000077 System.Int32 Lean.Common.LeanSelectable::get_IsSelectedCount()
extern void LeanSelectable_get_IsSelectedCount_mA04A711123367591957F27B69418B2F0F6CB038C (void);
// 0x00000078 System.Void Lean.Common.LeanSelectable::Deselect()
extern void LeanSelectable_Deselect_mDB3147AA2FBBAFAC12B75E550166E48AA105F8FA (void);
// 0x00000079 System.Void Lean.Common.LeanSelectable::DeselectAll()
extern void LeanSelectable_DeselectAll_m49A1D27B4BAC28FF9FACF512A95B6DDAF61E0021 (void);
// 0x0000007A System.Void Lean.Common.LeanSelectable::InvokeOnSelected(Lean.Common.LeanSelect)
extern void LeanSelectable_InvokeOnSelected_m88EE956FB0B841E633E09C737E8EF706DA760AD7 (void);
// 0x0000007B System.Void Lean.Common.LeanSelectable::InvokeOnDeslected(Lean.Common.LeanSelect)
extern void LeanSelectable_InvokeOnDeslected_m1FD250CCD3F2AAD8193637EED125D642830B2BB6 (void);
// 0x0000007C System.Void Lean.Common.LeanSelectable::OnEnable()
extern void LeanSelectable_OnEnable_m4C276FE0ECC1304A8B981D597C6EC1521124B7B4 (void);
// 0x0000007D System.Void Lean.Common.LeanSelectable::OnDisable()
extern void LeanSelectable_OnDisable_m6ACD460A65DFC37ACE8A57AFD21BBC02934914F3 (void);
// 0x0000007E System.Void Lean.Common.LeanSelectable::OnDestroy()
extern void LeanSelectable_OnDestroy_mF4B7C34A580F8813E49E2E1452794E225FA04CB4 (void);
// 0x0000007F System.Void Lean.Common.LeanSelectable::.ctor()
extern void LeanSelectable__ctor_mF08DDB2433DD8402F17EDD86ED682F9E794B8B7C (void);
// 0x00000080 System.Void Lean.Common.LeanSelectable::.cctor()
extern void LeanSelectable__cctor_m4B56BA400F698D59D5DAD221339897C11353D3C3 (void);
// 0x00000081 Lean.Common.LeanSelectable Lean.Common.LeanSelectableBehaviour::get_Selectable()
extern void LeanSelectableBehaviour_get_Selectable_m3179061642491DB3E492363BFB7F87B10611CA72 (void);
// 0x00000082 System.Void Lean.Common.LeanSelectableBehaviour::Register()
extern void LeanSelectableBehaviour_Register_m818FEC227F1FD5C1641F4BADC3CA0078057CBB2B (void);
// 0x00000083 System.Void Lean.Common.LeanSelectableBehaviour::Register(Lean.Common.LeanSelectable)
extern void LeanSelectableBehaviour_Register_m4C01745C188A5CF757782AE9DD5AFF4AAE59483B (void);
// 0x00000084 System.Void Lean.Common.LeanSelectableBehaviour::Unregister()
extern void LeanSelectableBehaviour_Unregister_m49B03AF04BE330EB89D97A4E9C2931B86A7BAB25 (void);
// 0x00000085 System.Void Lean.Common.LeanSelectableBehaviour::OnEnable()
extern void LeanSelectableBehaviour_OnEnable_m9A662BE51025A7F546BAE11F66D2DE4126D39B74 (void);
// 0x00000086 System.Void Lean.Common.LeanSelectableBehaviour::Start()
extern void LeanSelectableBehaviour_Start_m802159E77734261F1C7F4B3E5E491653270EF64E (void);
// 0x00000087 System.Void Lean.Common.LeanSelectableBehaviour::OnDisable()
extern void LeanSelectableBehaviour_OnDisable_mF44F2079A3D58187031DBDED5169757BE64620D4 (void);
// 0x00000088 System.Void Lean.Common.LeanSelectableBehaviour::OnSelected()
extern void LeanSelectableBehaviour_OnSelected_mE69B0305323571F0D9032B38D3DB56FAEE2F263E (void);
// 0x00000089 System.Void Lean.Common.LeanSelectableBehaviour::OnDeselected()
extern void LeanSelectableBehaviour_OnDeselected_mD4FA6C7A266AEEDE1D666163709C3ED85C32E97A (void);
// 0x0000008A System.Void Lean.Common.LeanSelectableBehaviour::.ctor()
extern void LeanSelectableBehaviour__ctor_mCD83FC3DC435252184A5D5D4FBB450AEC430182C (void);
// 0x0000008B System.Void Lean.Common.LeanSelectableGraphicColor::set_DefaultColor(UnityEngine.Color)
extern void LeanSelectableGraphicColor_set_DefaultColor_mD8C560F5673F8977EC0EFE81CD63E605C7160B88 (void);
// 0x0000008C UnityEngine.Color Lean.Common.LeanSelectableGraphicColor::get_DefaultColor()
extern void LeanSelectableGraphicColor_get_DefaultColor_m28CD0531C3348B1764932DA273C10C8D09340A24 (void);
// 0x0000008D System.Void Lean.Common.LeanSelectableGraphicColor::set_SelectedColor(UnityEngine.Color)
extern void LeanSelectableGraphicColor_set_SelectedColor_mC9DC3E32E49528560B785F963EF218F39A54D776 (void);
// 0x0000008E UnityEngine.Color Lean.Common.LeanSelectableGraphicColor::get_SelectedColor()
extern void LeanSelectableGraphicColor_get_SelectedColor_mE5CEC40657F34A2BE117D7081B8F5C996D9A613E (void);
// 0x0000008F System.Void Lean.Common.LeanSelectableGraphicColor::OnSelected()
extern void LeanSelectableGraphicColor_OnSelected_mB25BAAE9806F9029BB4DE2EA70EC107E9F251C9B (void);
// 0x00000090 System.Void Lean.Common.LeanSelectableGraphicColor::OnDeselected()
extern void LeanSelectableGraphicColor_OnDeselected_mC3E5AF97FBA0B3C9446EEF97DA699BD6D9A464D9 (void);
// 0x00000091 System.Void Lean.Common.LeanSelectableGraphicColor::UpdateColor()
extern void LeanSelectableGraphicColor_UpdateColor_mA0CB907B13AD6D881FE5E08DD20A262AE518CCFF (void);
// 0x00000092 System.Void Lean.Common.LeanSelectableGraphicColor::.ctor()
extern void LeanSelectableGraphicColor__ctor_mAEE0AE64F610BD33E213AE16DE00854E6E75BE79 (void);
// 0x00000093 System.Void Lean.Common.LeanSelectableRendererColor::set_DefaultColor(UnityEngine.Color)
extern void LeanSelectableRendererColor_set_DefaultColor_m1A8757A2E983E8BA194FCF73755835C860B6000C (void);
// 0x00000094 UnityEngine.Color Lean.Common.LeanSelectableRendererColor::get_DefaultColor()
extern void LeanSelectableRendererColor_get_DefaultColor_m8F6F050B98594027A7702C1C485E0AFC9FA53D93 (void);
// 0x00000095 System.Void Lean.Common.LeanSelectableRendererColor::set_SelectedColor(UnityEngine.Color)
extern void LeanSelectableRendererColor_set_SelectedColor_m4D39C23D18AD4A0EC022238BDAEB30889CBB154F (void);
// 0x00000096 UnityEngine.Color Lean.Common.LeanSelectableRendererColor::get_SelectedColor()
extern void LeanSelectableRendererColor_get_SelectedColor_m6CA74321888A68BDE49E95558EE140BBD99C5065 (void);
// 0x00000097 System.Void Lean.Common.LeanSelectableRendererColor::OnSelected()
extern void LeanSelectableRendererColor_OnSelected_mEEC47ADA1EE163B63CA99EB37B5BBA845DBE4764 (void);
// 0x00000098 System.Void Lean.Common.LeanSelectableRendererColor::OnDeselected()
extern void LeanSelectableRendererColor_OnDeselected_m6CBE49594FC15E66CE30A7060E120A8632FB384D (void);
// 0x00000099 System.Void Lean.Common.LeanSelectableRendererColor::Start()
extern void LeanSelectableRendererColor_Start_m0F0CF59D5905F98036501068E13ECA4894010BAD (void);
// 0x0000009A System.Void Lean.Common.LeanSelectableRendererColor::UpdateColor()
extern void LeanSelectableRendererColor_UpdateColor_mFADF1583C8147B92BC61EADAF644454501DF2D9E (void);
// 0x0000009B System.Void Lean.Common.LeanSelectableRendererColor::.ctor()
extern void LeanSelectableRendererColor__ctor_mEF6E9E171D482CBF6D4ADC0D2FB502B0A640ED0D (void);
// 0x0000009C System.Void Lean.Common.LeanSelectableSpriteRendererColor::set_DefaultColor(UnityEngine.Color)
extern void LeanSelectableSpriteRendererColor_set_DefaultColor_m0DCEE98C568B7936A49040F3927D76651F31C71F (void);
// 0x0000009D UnityEngine.Color Lean.Common.LeanSelectableSpriteRendererColor::get_DefaultColor()
extern void LeanSelectableSpriteRendererColor_get_DefaultColor_m9D51D9458C20989A3342574A894B7A50AB566567 (void);
// 0x0000009E System.Void Lean.Common.LeanSelectableSpriteRendererColor::set_SelectedColor(UnityEngine.Color)
extern void LeanSelectableSpriteRendererColor_set_SelectedColor_m685C964E16CBE1F32678B261206007E4BC76F9B9 (void);
// 0x0000009F UnityEngine.Color Lean.Common.LeanSelectableSpriteRendererColor::get_SelectedColor()
extern void LeanSelectableSpriteRendererColor_get_SelectedColor_mC037F6C99C53AB642A0C23B44B299DEA46DF83C0 (void);
// 0x000000A0 System.Void Lean.Common.LeanSelectableSpriteRendererColor::OnSelected()
extern void LeanSelectableSpriteRendererColor_OnSelected_mDB246778B00B08B6245E0BFD385FBB8D7CCED978 (void);
// 0x000000A1 System.Void Lean.Common.LeanSelectableSpriteRendererColor::OnDeselected()
extern void LeanSelectableSpriteRendererColor_OnDeselected_m825546BA2614B9F90326B985A7667CCB999DA4D1 (void);
// 0x000000A2 System.Void Lean.Common.LeanSelectableSpriteRendererColor::UpdateColor()
extern void LeanSelectableSpriteRendererColor_UpdateColor_m28BA1CDBD342A0397AF7B1F9FDF61247B141D512 (void);
// 0x000000A3 System.Void Lean.Common.LeanSelectableSpriteRendererColor::.ctor()
extern void LeanSelectableSpriteRendererColor__ctor_mAE81A911541CFB0053E540720D38F9792A5011F8 (void);
// 0x000000A4 System.Void Lean.Common.LeanSpawn::Spawn()
extern void LeanSpawn_Spawn_m7ABA172ABC9E4E778FB5768C8007A6E62F0A7288 (void);
// 0x000000A5 System.Void Lean.Common.LeanSpawn::Spawn(UnityEngine.Vector3)
extern void LeanSpawn_Spawn_m59D0F7C4ABE3D8B52F139E5106EDE7EC5DD881BA (void);
// 0x000000A6 System.Void Lean.Common.LeanSpawn::.ctor()
extern void LeanSpawn__ctor_m5D0059D6CE5DA4477D18D0D1428673CF70B9076B (void);
// 0x000000A7 T Lean.Common.LeanHelper::CreateElement(UnityEngine.Transform)
// 0x000000A8 System.Single Lean.Common.LeanHelper::GetDampenFactor(System.Single,System.Single)
extern void LeanHelper_GetDampenFactor_m52703DB548BE869104268F407B36ECB1984E5871 (void);
// 0x000000A9 T Lean.Common.LeanHelper::Destroy(T)
// 0x000000AA UnityEngine.Camera Lean.Common.LeanHelper::GetCamera(UnityEngine.Camera,UnityEngine.GameObject)
extern void LeanHelper_GetCamera_m36B3A0AE813922D60EB7E218A95D9DEB7C87F130 (void);
// 0x000000AB UnityEngine.Vector2 Lean.Common.LeanHelper::Hermite(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanHelper_Hermite_mECF9A4F69E9372F446E088C5D549CE3BED865706 (void);
// 0x000000AC System.Single Lean.Common.LeanHelper::HermiteInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanHelper_HermiteInterpolate_m17517674980B6D010D3F82CDC1F456AD99FE69C0 (void);
// 0x000000AD System.Int32 Lean.Common.LeanInput::GetTouchCount()
extern void LeanInput_GetTouchCount_mF4BDF1C08B6A4DFF050EFDC379B9CDF24339B788 (void);
// 0x000000AE System.Void Lean.Common.LeanInput::GetTouch(System.Int32,System.Int32&,UnityEngine.Vector2&,System.Single&,System.Boolean&)
extern void LeanInput_GetTouch_m0522BED7297526125F98A2747EDB7D771AF8B891 (void);
// 0x000000AF UnityEngine.Vector2 Lean.Common.LeanInput::GetMousePosition()
extern void LeanInput_GetMousePosition_mF3EF9EF9D99349CA88667DF4BB9EACED4F34A2A1 (void);
// 0x000000B0 System.Boolean Lean.Common.LeanInput::GetDown(UnityEngine.KeyCode)
extern void LeanInput_GetDown_m068A56F031EFCB5FC2C69B2DFF945E4CBA7A3ED6 (void);
// 0x000000B1 System.Boolean Lean.Common.LeanInput::GetPressed(UnityEngine.KeyCode)
extern void LeanInput_GetPressed_m9A2C8797BC996C724279DAC7ABDEA27FAAFC680A (void);
// 0x000000B2 System.Boolean Lean.Common.LeanInput::GetUp(UnityEngine.KeyCode)
extern void LeanInput_GetUp_m6BA7F98C4ED11EEEFB96EDF963355FCE30BDD3CC (void);
// 0x000000B3 System.Boolean Lean.Common.LeanInput::GetMouseDown(System.Int32)
extern void LeanInput_GetMouseDown_mA84738702B884081C08EE61FCC587A5A58808AAD (void);
// 0x000000B4 System.Boolean Lean.Common.LeanInput::GetMousePressed(System.Int32)
extern void LeanInput_GetMousePressed_mB761F1322C0FF6E5D5BC58146BA042607CD7E484 (void);
// 0x000000B5 System.Boolean Lean.Common.LeanInput::GetMouseUp(System.Int32)
extern void LeanInput_GetMouseUp_m2D622B6BAA2656BE5B673A386E1992DDDFF76A25 (void);
// 0x000000B6 System.Single Lean.Common.LeanInput::GetMouseWheelDelta()
extern void LeanInput_GetMouseWheelDelta_mA9809ED18EDD283ACA45F39B0FCD840428CD4128 (void);
// 0x000000B7 System.Boolean Lean.Common.LeanInput::GetMouseExists()
extern void LeanInput_GetMouseExists_m8C8E0119A853CF8898A08F3E50311551F2F30B7C (void);
// 0x000000B8 System.Boolean Lean.Common.LeanInput::GetKeyboardExists()
extern void LeanInput_GetKeyboardExists_mC75B2AC3439D74F50904A8FB7821AEAA6AECB20E (void);
// 0x000000B9 System.Void Lean.Common.Examples.LeanDemo::set_UpgradeInputModule(System.Boolean)
extern void LeanDemo_set_UpgradeInputModule_m8E546D42219CC874326B2C64F82D13A37B146E07 (void);
// 0x000000BA System.Boolean Lean.Common.Examples.LeanDemo::get_UpgradeInputModule()
extern void LeanDemo_get_UpgradeInputModule_mC83E601E463E914C3BF6B3BEF7B9308BBF2EA411 (void);
// 0x000000BB System.Void Lean.Common.Examples.LeanDemo::set_SkyboxRoot(UnityEngine.Transform)
extern void LeanDemo_set_SkyboxRoot_m1487C18F30094ECD564EEF36F48E25C8CAA4DA54 (void);
// 0x000000BC UnityEngine.Transform Lean.Common.Examples.LeanDemo::get_SkyboxRoot()
extern void LeanDemo_get_SkyboxRoot_mE277FDE370CB69BFE43072BBA31951DB043C1A2D (void);
// 0x000000BD System.Void Lean.Common.Examples.LeanDemo::OnEnable()
extern void LeanDemo_OnEnable_m6AE6551FC8D65E4021B6544E8FF8D6D5ACBB5C2C (void);
// 0x000000BE System.Void Lean.Common.Examples.LeanDemo::LateUpdate()
extern void LeanDemo_LateUpdate_mBE3CD8A8922A50BF23C8C1ABECCB22304AA06CD8 (void);
// 0x000000BF System.Void Lean.Common.Examples.LeanDemo::TryUpgradeEventSystem()
extern void LeanDemo_TryUpgradeEventSystem_m6BB041EF209421315B707B5A2F8407E171C9AE01 (void);
// 0x000000C0 System.Void Lean.Common.Examples.LeanDemo::.ctor()
extern void LeanDemo__ctor_m6A5863F40341E12025685CB8D3759519496D063C (void);
// 0x000000C1 System.Void Lean.Common.Examples.LeanLinkTo::set_Link(Lean.Common.Examples.LeanLinkTo/LinkType)
extern void LeanLinkTo_set_Link_mEE546F70913CCF248121CBF017DD0A3C95C63346 (void);
// 0x000000C2 Lean.Common.Examples.LeanLinkTo/LinkType Lean.Common.Examples.LeanLinkTo::get_Link()
extern void LeanLinkTo_get_Link_m6C153A64C1F1B6B610477DC951F05E0BB473DAEE (void);
// 0x000000C3 System.Void Lean.Common.Examples.LeanLinkTo::Update()
extern void LeanLinkTo_Update_m93C4CBCA3FFAB3BD2F9F25F03A27DE82756252B5 (void);
// 0x000000C4 System.Void Lean.Common.Examples.LeanLinkTo::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void LeanLinkTo_OnPointerClick_m850F3ED2818F10DACC75FC41BCEC464211692225 (void);
// 0x000000C5 System.Int32 Lean.Common.Examples.LeanLinkTo::GetCurrentLevel()
extern void LeanLinkTo_GetCurrentLevel_m90D6D996F9A1FBBDEACC012122896E0BFE9433CC (void);
// 0x000000C6 System.Int32 Lean.Common.Examples.LeanLinkTo::GetLevelCount()
extern void LeanLinkTo_GetLevelCount_m5209995A5C000F0F8F5840A7710B361E38B0EBAD (void);
// 0x000000C7 System.Void Lean.Common.Examples.LeanLinkTo::LoadLevel(System.Int32)
extern void LeanLinkTo_LoadLevel_mE6F75753D87EC3A537C4A22F7AE12E7C29CB647E (void);
// 0x000000C8 System.Void Lean.Common.Examples.LeanLinkTo::.ctor()
extern void LeanLinkTo__ctor_m2C8243121364332048FA6C029D94EF14A7501490 (void);
// 0x000000C9 System.Void Lean.Common.LeanFormatString/StringEvent::.ctor()
extern void StringEvent__ctor_m77282D943902FF61011811BF65D28F643C2EE533 (void);
// 0x000000CA System.Void Lean.Common.LeanSelect/LeanSelectableEvent::.ctor()
extern void LeanSelectableEvent__ctor_mF5627EFF40B3A1732E557CBD53BFCE1636461D4E (void);
// 0x000000CB System.Void Lean.Common.LeanSelectable/LeanSelectEvent::.ctor()
extern void LeanSelectEvent__ctor_m40B651A5318119BC1E371D91D24F69854D16F1A5 (void);
static Il2CppMethodPointer s_methodPointers[203] = 
{
	LeanGuide_get_Icon_mC342CBBC9F940DE4969117F8EDE3592FEC4E1322,
	LeanGuide_get_Version_m02E6AAB2788ED5F2855F772C2C0731C5294F0191,
	LeanGuide__ctor_m812A17DD463D4BAAE0A5D27500854ABAAEBD2D0B,
	LeanDestroy_set_Execute_m166BB1894E785581AFFC0BA7576561BDB947881B,
	LeanDestroy_get_Execute_mDF5B6C3010D28D94C421C661EB38BB55EC337610,
	LeanDestroy_set_Target_m9A67362AA7C590B37EC62EAFB35E3F7724B96BD2,
	LeanDestroy_get_Target_m3D9BE220570285E3990EE27669F57358E592D20B,
	LeanDestroy_set_Seconds_m8B39597D27B1E69D1C4DB7A97589C0C07B7431F5,
	LeanDestroy_get_Seconds_m77450A6391D2261867108946DEEF490D81F4EE4F,
	LeanDestroy_Update_m47E888DE4CA9C306FF1FD2EAC50FF91B51A6BFEA,
	LeanDestroy_DestroyNow_m7E586E917761D888672AE1E9608FEB3F376C4FD9,
	LeanDestroy__ctor_mF329265FBF8E6C0C8BA74E311CC77C9D19541B97,
	LeanFormatString_set_Format_m1A8DF72D34D95207338BC9AB5FA93878390C19A8,
	LeanFormatString_get_Format_mAC38BD817CAB8D75CAB68AD38A89E99B594F1125,
	LeanFormatString_get_OnString_mC06F68BC48907A55B7CF7874345D46F3C888D74A,
	LeanFormatString_SetString_m45E1096AE743D8FA7102EA84C47E3DF5B48A0BD7,
	LeanFormatString_SetString_m4ABDC36976ABBC4429628FB6FC76415D10634D4E,
	LeanFormatString_SetString_mD15B3F51A8C01B6ADCE687BD48B843CDBFAB20FF,
	LeanFormatString_SetString_m0463D960C5187144FD2EEA3CEBA161B873B6694A,
	LeanFormatString_SetString_m60F9CE4BF05D9D64167D03E63B67EF04513220E2,
	LeanFormatString_SetString_mCA14D1D089FD92896B0ECAC0EF05248076BA5D33,
	LeanFormatString_SetString_m62289D8F2EC52CCDE686883C5A525299CCC44BF8,
	LeanFormatString_SetString_m1CE75E61E22F90A3C43B678782060D432318BCD7,
	LeanFormatString_SetString_m25CDE010B45BE09BF698B52F7918475CF04A2813,
	LeanFormatString_SetString_mAAD50D4158C11E62BA307735397279BD43D7E1DF,
	LeanFormatString_SetString_m16F58EF81DAC87AFE1C66EDB2CCFCD803BAF1413,
	LeanFormatString_SetString_m48DC2444BE823EAC0443F9E8B977400FD27EAE02,
	LeanFormatString_SetString_mAEBE5C5139D6D13CB8D14CAF2E796E32406D574A,
	LeanFormatString_SetString_mCDA901FA8B6E112066EFFCE75B6967AE43614C79,
	LeanFormatString_SendString_m6727C6A1A5FB981B278A50F4688AC5F839FE29BC,
	LeanFormatString_SendString_m78107E8A777A4D190284899C87C271E32C241465,
	LeanFormatString__ctor_mADCE8AF8902D2213194D2145A36B435DAB13BA33,
	LeanPath_get_PointCount_m791BDBACDBD2531ED87C29D19F2D3F23BA6B9F6E,
	LeanPath_GetPointCount_mF59EF4AF02F472309A48259BF595EE5AB1AA4377,
	LeanPath_GetSmoothedPoint_m1C037E2A1C0B3F80235E16928B717C0198B79991,
	LeanPath_GetPoint_m02C8B39AD5A8C857ECFCA386CFC7E63AD913BADB,
	LeanPath_GetPointRaw_mBF4D849132319266EB84FF8AC3CF1028F8ABFF8F,
	LeanPath_SetLine_mFC8AEABE182D201BB75B490E324129E338F26D7B,
	LeanPath_TryGetClosest_mCB41AD274F16628F3A3E16184CE71708990CAA09,
	LeanPath_TryGetClosest_mF340113774CF9E1B7BCA506074FF1587854A1EFE,
	LeanPath_TryGetClosest_m9AA350EB963B9DC45761F934FF0E28559CFEFEE0,
	LeanPath_TryGetClosest_m2E6F1BDA9491E4876333F779AA23BCBAB13F6D78,
	LeanPath_TryGetClosest_m4B1869B258D46F984DBBA2193DC121C27D72B4DC,
	LeanPath_GetClosestPoint_mEBEE16114D59404DF10CF75E5D121376887B2E48,
	LeanPath_GetClosestPoint_m24B13DFD725ECDC0E2A9FB8657788218A32F4774,
	LeanPath_GetClosestDistance_mDC5293908E288BB5D4CC6A2E40750AFDD44556B8,
	LeanPath_Mod_mB84DC1E281CABBC97F349FBFB66D08C7AAB715A2,
	LeanPath_CubicInterpolate_m61DD0A87B85DFFE79BB4E7BB8AF056456F6EDD4B,
	LeanPath_UpdateVisual_m298B3611F78B78A6753EBC730374ACDA28DEB986,
	LeanPath_Update_mD5420A5DE1E834C45495183036D3662D09481821,
	LeanPath__ctor_mC1A4DB1E2526EDB5E65E47CF709C3049C9A0560A,
	LeanPath__cctor_mEA3C136DB3CE56029B08B58D9C06D2FB22191D0B,
	LeanPlane_GetClosest_m3922292E2474E87C6D0BE0528BB50DC76923B28D,
	LeanPlane_TryRaycast_m547EA5F3CB30946C43E90C6B737E5225AA6B5CED,
	LeanPlane_GetClosest_mEB4174B1E168745B01869061732CCCB114A45154,
	LeanPlane_RayToPlane_m9A5ED668725827F0707403CEFF8B6C54851DB636,
	LeanPlane__ctor_m3C89D5F3B03A8E1CBD3CB37A43A35AD35E1A2B77,
	LeanRoll_set_Angle_m9CCEAECBE1A1D159402D0CE60D97ABB681B54654,
	LeanRoll_get_Angle_mEF25A957199E03C1BE10AFE3EF8DA0046864DCDD,
	LeanRoll_set_Clamp_m35C56C528885DE03A6137F33521F5D2672A9306C,
	LeanRoll_get_Clamp_mBCACB1E0EB55AA6F4B4A8218E5708340A4519242,
	LeanRoll_set_ClampMin_m7E8EF2576AF2E0897981CCF6F7C42B7F50C637CB,
	LeanRoll_get_ClampMin_mD33270CB88752B1C062F6693EA872AC87775921C,
	LeanRoll_set_ClampMax_m22A62AF9FBE127F9790F4BCCFC2C98C9CE501995,
	LeanRoll_get_ClampMax_m0A9CA375F49385B07BB318344D209DFF8BB7DB08,
	LeanRoll_set_Damping_m4BD7D5A8EEA96030355BC2CCA97DBAB7142EAA9B,
	LeanRoll_get_Damping_m2243F120C54A7923A20F3590C62FAECA926A7C51,
	LeanRoll_IncrementAngle_m59632C0B0CA6126DDAA1F1894246461108617428,
	LeanRoll_DecrementAngle_m5C6918E2DC895EFD41E6F28971C399A274BDE2AD,
	LeanRoll_RotateToDelta_mB29EAA00ACC86D28C0FDCDE244ADD9F230FCB563,
	LeanRoll_SnapToTarget_mBC7C23B4BEEE20D405A21F979C92CFC56FBD1639,
	LeanRoll_Start_m568D1FAB9E92BC3E9842E91EEB20394867E5A3DD,
	LeanRoll_Update_m812350F2283BDFDC496278B524F08B89442A0D33,
	LeanRoll__ctor_mC47CB7FA9F21BE46A39B015D0FC0281484AA44EE,
	LeanSelect_set_DeselectWithNothing_mFB401D6A3438FB45CB8EA2F973E74FDDB41A098F,
	LeanSelect_get_DeselectWithNothing_m89E5744A57BA7E076219A915628F9688E3BF6D4B,
	LeanSelect_set_Limit_m4BF8CD6AD4DD5328E3DF61B1A596C645E7BE4315,
	LeanSelect_get_Limit_m5D80EF0695B89E4FD2CC8D12B61CFECF192D4EE5,
	LeanSelect_set_MaxSelectables_m8FC1FBF4CCC208ECE099FC62D70022839A486E07,
	LeanSelect_get_MaxSelectables_mAB025390799B6CA0E149E7EC85FAD8C508C40ECA,
	LeanSelect_set_Reselect_m074DE09F6455D6482A7B5365AFD68920E878A887,
	LeanSelect_get_Reselect_m29035A9BCA19C651E99A7405AD66F8CCDF976D83,
	LeanSelect_get_Selectables_m886E41F694C8E7F63E0F16E65B39A815C70EA531,
	LeanSelect_get_OnSelected_m10D4E6ADB4883F58BCD5647989DE526B8F6A4350,
	LeanSelect_get_OnDeselected_m2FC13636EAE046B8F265784F8BD980E4A3C377CF,
	LeanSelect_get_OnNothing_m96F976DCFE7628CB0C4E951A8568D66F6BB4FD68,
	LeanSelect_add_OnAnySelected_m3721278466D05C31AB52DB92D253710A0FF5BFFF,
	LeanSelect_remove_OnAnySelected_m55111EBC17E5401F0BFF09CEA81E36ECEE6C5441,
	LeanSelect_add_OnAnyDeselected_mC254A7B797EA017BFDD5F75D778129179D158AF5,
	LeanSelect_remove_OnAnyDeselected_m4F1A0BB681081D3586CD7D2DCEF3E297B47BDF55,
	LeanSelect_IsSelected_mFB16947AF24E5DA9489C8A29CB78953868CFC654,
	LeanSelect_Select_m5D0C01A61F8259EA1352B327A0E1D7110B274746,
	LeanSelect_Deselect_mFFACE713654CC54EE11EB863FBC9D65C81CEA2C7,
	LeanSelect_TrySelect_m9E46D0E5E7D824DB3DA8C36880C2A1561523975F,
	LeanSelect_TryReselect_m831BC63CCCB2E3B2F26382756B495E1FA0CAF06F,
	LeanSelect_TryDeselect_mD6DEE075EBBFB738D943BF97C4FB82D9D2011EA1,
	LeanSelect_TryDeselect_m84B8A9AAD66075B94434DAC6F6786A42F345FDFA,
	LeanSelect_DeselectAll_mFE184416CE6E63B5A0D39E595C06148896350BF3,
	LeanSelect_Cull_mF4ED48BA46DBA9DDFE99D792EF1B996F94E5079E,
	LeanSelect_OnEnable_mB3E53DBEA024A1D81098384C4E2AA34C363ABC6B,
	LeanSelect_OnDisable_m3806A8D29811C699B87E676F1FDC41681E86D277,
	LeanSelect_OnDestroy_m1D1C5E3485D9068A95F322F4D82879E88943E188,
	LeanSelect__ctor_mAA1BDF7B0E302EA7CC43505DED75AA7CFD6736FD,
	LeanSelect__cctor_m0B9EBE0C86D35095D85146A2AB8A24B8B6A8293D,
	LeanSelectable_set_SelfSelected_m084F86106F53D2AAD073AA876197EC6336920549,
	LeanSelectable_get_SelfSelected_mE941DA0A15A40FCB21693F63EF613DFE1545A907,
	LeanSelectable_get_OnSelected_m017D009660A677B55DEB2D6FF9864AE63C5844A4,
	LeanSelectable_get_OnDeselected_mA32DFC4FD8CCA385E5A46ADB7CB3D42155708132,
	LeanSelectable_add_OnAnyEnabled_mC4CDF0392B095B16F3B058B1FE997EE328603C1F,
	LeanSelectable_remove_OnAnyEnabled_mD6EB75344F7B1C45BA0BB4A0756AD44C8C96D49E,
	LeanSelectable_add_OnAnyDisabled_mC385BA7B9D3A51D60FC6D03C64FA0571DDADF213,
	LeanSelectable_remove_OnAnyDisabled_mBCF7A98ED552441C6C1761BA6DC21462BE9B7ED8,
	LeanSelectable_add_OnAnySelected_mC73BD4ACC56FF10CD0FA79633EBEF053AA1DE278,
	LeanSelectable_remove_OnAnySelected_m2EB3510123B29A3FA225E739BC34AA58A44B04F4,
	LeanSelectable_add_OnAnyDeselected_m64ED1525EE1489C21D04AEB203E7E464D09402A9,
	LeanSelectable_remove_OnAnyDeselected_mEDF24C0C5EDCE5119F8D18430926C7172481689B,
	LeanSelectable_get_SelectedCount_mB857FEE53599D836886BDC77045B29BB2976CA7A,
	LeanSelectable_get_IsSelected_mD364318D1E43F650D1F1B9AD6041B2E60B1F9C91,
	LeanSelectable_get_IsSelectedCount_mA04A711123367591957F27B69418B2F0F6CB038C,
	LeanSelectable_Deselect_mDB3147AA2FBBAFAC12B75E550166E48AA105F8FA,
	LeanSelectable_DeselectAll_m49A1D27B4BAC28FF9FACF512A95B6DDAF61E0021,
	LeanSelectable_InvokeOnSelected_m88EE956FB0B841E633E09C737E8EF706DA760AD7,
	LeanSelectable_InvokeOnDeslected_m1FD250CCD3F2AAD8193637EED125D642830B2BB6,
	LeanSelectable_OnEnable_m4C276FE0ECC1304A8B981D597C6EC1521124B7B4,
	LeanSelectable_OnDisable_m6ACD460A65DFC37ACE8A57AFD21BBC02934914F3,
	LeanSelectable_OnDestroy_mF4B7C34A580F8813E49E2E1452794E225FA04CB4,
	LeanSelectable__ctor_mF08DDB2433DD8402F17EDD86ED682F9E794B8B7C,
	LeanSelectable__cctor_m4B56BA400F698D59D5DAD221339897C11353D3C3,
	LeanSelectableBehaviour_get_Selectable_m3179061642491DB3E492363BFB7F87B10611CA72,
	LeanSelectableBehaviour_Register_m818FEC227F1FD5C1641F4BADC3CA0078057CBB2B,
	LeanSelectableBehaviour_Register_m4C01745C188A5CF757782AE9DD5AFF4AAE59483B,
	LeanSelectableBehaviour_Unregister_m49B03AF04BE330EB89D97A4E9C2931B86A7BAB25,
	LeanSelectableBehaviour_OnEnable_m9A662BE51025A7F546BAE11F66D2DE4126D39B74,
	LeanSelectableBehaviour_Start_m802159E77734261F1C7F4B3E5E491653270EF64E,
	LeanSelectableBehaviour_OnDisable_mF44F2079A3D58187031DBDED5169757BE64620D4,
	LeanSelectableBehaviour_OnSelected_mE69B0305323571F0D9032B38D3DB56FAEE2F263E,
	LeanSelectableBehaviour_OnDeselected_mD4FA6C7A266AEEDE1D666163709C3ED85C32E97A,
	LeanSelectableBehaviour__ctor_mCD83FC3DC435252184A5D5D4FBB450AEC430182C,
	LeanSelectableGraphicColor_set_DefaultColor_mD8C560F5673F8977EC0EFE81CD63E605C7160B88,
	LeanSelectableGraphicColor_get_DefaultColor_m28CD0531C3348B1764932DA273C10C8D09340A24,
	LeanSelectableGraphicColor_set_SelectedColor_mC9DC3E32E49528560B785F963EF218F39A54D776,
	LeanSelectableGraphicColor_get_SelectedColor_mE5CEC40657F34A2BE117D7081B8F5C996D9A613E,
	LeanSelectableGraphicColor_OnSelected_mB25BAAE9806F9029BB4DE2EA70EC107E9F251C9B,
	LeanSelectableGraphicColor_OnDeselected_mC3E5AF97FBA0B3C9446EEF97DA699BD6D9A464D9,
	LeanSelectableGraphicColor_UpdateColor_mA0CB907B13AD6D881FE5E08DD20A262AE518CCFF,
	LeanSelectableGraphicColor__ctor_mAEE0AE64F610BD33E213AE16DE00854E6E75BE79,
	LeanSelectableRendererColor_set_DefaultColor_m1A8757A2E983E8BA194FCF73755835C860B6000C,
	LeanSelectableRendererColor_get_DefaultColor_m8F6F050B98594027A7702C1C485E0AFC9FA53D93,
	LeanSelectableRendererColor_set_SelectedColor_m4D39C23D18AD4A0EC022238BDAEB30889CBB154F,
	LeanSelectableRendererColor_get_SelectedColor_m6CA74321888A68BDE49E95558EE140BBD99C5065,
	LeanSelectableRendererColor_OnSelected_mEEC47ADA1EE163B63CA99EB37B5BBA845DBE4764,
	LeanSelectableRendererColor_OnDeselected_m6CBE49594FC15E66CE30A7060E120A8632FB384D,
	LeanSelectableRendererColor_Start_m0F0CF59D5905F98036501068E13ECA4894010BAD,
	LeanSelectableRendererColor_UpdateColor_mFADF1583C8147B92BC61EADAF644454501DF2D9E,
	LeanSelectableRendererColor__ctor_mEF6E9E171D482CBF6D4ADC0D2FB502B0A640ED0D,
	LeanSelectableSpriteRendererColor_set_DefaultColor_m0DCEE98C568B7936A49040F3927D76651F31C71F,
	LeanSelectableSpriteRendererColor_get_DefaultColor_m9D51D9458C20989A3342574A894B7A50AB566567,
	LeanSelectableSpriteRendererColor_set_SelectedColor_m685C964E16CBE1F32678B261206007E4BC76F9B9,
	LeanSelectableSpriteRendererColor_get_SelectedColor_mC037F6C99C53AB642A0C23B44B299DEA46DF83C0,
	LeanSelectableSpriteRendererColor_OnSelected_mDB246778B00B08B6245E0BFD385FBB8D7CCED978,
	LeanSelectableSpriteRendererColor_OnDeselected_m825546BA2614B9F90326B985A7667CCB999DA4D1,
	LeanSelectableSpriteRendererColor_UpdateColor_m28BA1CDBD342A0397AF7B1F9FDF61247B141D512,
	LeanSelectableSpriteRendererColor__ctor_mAE81A911541CFB0053E540720D38F9792A5011F8,
	LeanSpawn_Spawn_m7ABA172ABC9E4E778FB5768C8007A6E62F0A7288,
	LeanSpawn_Spawn_m59D0F7C4ABE3D8B52F139E5106EDE7EC5DD881BA,
	LeanSpawn__ctor_m5D0059D6CE5DA4477D18D0D1428673CF70B9076B,
	NULL,
	LeanHelper_GetDampenFactor_m52703DB548BE869104268F407B36ECB1984E5871,
	NULL,
	LeanHelper_GetCamera_m36B3A0AE813922D60EB7E218A95D9DEB7C87F130,
	LeanHelper_Hermite_mECF9A4F69E9372F446E088C5D549CE3BED865706,
	LeanHelper_HermiteInterpolate_m17517674980B6D010D3F82CDC1F456AD99FE69C0,
	LeanInput_GetTouchCount_mF4BDF1C08B6A4DFF050EFDC379B9CDF24339B788,
	LeanInput_GetTouch_m0522BED7297526125F98A2747EDB7D771AF8B891,
	LeanInput_GetMousePosition_mF3EF9EF9D99349CA88667DF4BB9EACED4F34A2A1,
	LeanInput_GetDown_m068A56F031EFCB5FC2C69B2DFF945E4CBA7A3ED6,
	LeanInput_GetPressed_m9A2C8797BC996C724279DAC7ABDEA27FAAFC680A,
	LeanInput_GetUp_m6BA7F98C4ED11EEEFB96EDF963355FCE30BDD3CC,
	LeanInput_GetMouseDown_mA84738702B884081C08EE61FCC587A5A58808AAD,
	LeanInput_GetMousePressed_mB761F1322C0FF6E5D5BC58146BA042607CD7E484,
	LeanInput_GetMouseUp_m2D622B6BAA2656BE5B673A386E1992DDDFF76A25,
	LeanInput_GetMouseWheelDelta_mA9809ED18EDD283ACA45F39B0FCD840428CD4128,
	LeanInput_GetMouseExists_m8C8E0119A853CF8898A08F3E50311551F2F30B7C,
	LeanInput_GetKeyboardExists_mC75B2AC3439D74F50904A8FB7821AEAA6AECB20E,
	LeanDemo_set_UpgradeInputModule_m8E546D42219CC874326B2C64F82D13A37B146E07,
	LeanDemo_get_UpgradeInputModule_mC83E601E463E914C3BF6B3BEF7B9308BBF2EA411,
	LeanDemo_set_SkyboxRoot_m1487C18F30094ECD564EEF36F48E25C8CAA4DA54,
	LeanDemo_get_SkyboxRoot_mE277FDE370CB69BFE43072BBA31951DB043C1A2D,
	LeanDemo_OnEnable_m6AE6551FC8D65E4021B6544E8FF8D6D5ACBB5C2C,
	LeanDemo_LateUpdate_mBE3CD8A8922A50BF23C8C1ABECCB22304AA06CD8,
	LeanDemo_TryUpgradeEventSystem_m6BB041EF209421315B707B5A2F8407E171C9AE01,
	LeanDemo__ctor_m6A5863F40341E12025685CB8D3759519496D063C,
	LeanLinkTo_set_Link_mEE546F70913CCF248121CBF017DD0A3C95C63346,
	LeanLinkTo_get_Link_m6C153A64C1F1B6B610477DC951F05E0BB473DAEE,
	LeanLinkTo_Update_m93C4CBCA3FFAB3BD2F9F25F03A27DE82756252B5,
	LeanLinkTo_OnPointerClick_m850F3ED2818F10DACC75FC41BCEC464211692225,
	LeanLinkTo_GetCurrentLevel_m90D6D996F9A1FBBDEACC012122896E0BFE9433CC,
	LeanLinkTo_GetLevelCount_m5209995A5C000F0F8F5840A7710B361E38B0EBAD,
	LeanLinkTo_LoadLevel_mE6F75753D87EC3A537C4A22F7AE12E7C29CB647E,
	LeanLinkTo__ctor_m2C8243121364332048FA6C029D94EF14A7501490,
	StringEvent__ctor_m77282D943902FF61011811BF65D28F643C2EE533,
	LeanSelectableEvent__ctor_mF5627EFF40B3A1732E557CBD53BFCE1636461D4E,
	LeanSelectEvent__ctor_m40B651A5318119BC1E371D91D24F69854D16F1A5,
};
static const int32_t s_InvokerIndices[203] = 
{
	14,
	14,
	23,
	32,
	10,
	26,
	14,
	337,
	731,
	23,
	23,
	23,
	26,
	14,
	14,
	26,
	27,
	32,
	129,
	337,
	1400,
	1442,
	1439,
	1434,
	1431,
	1985,
	2205,
	1900,
	1020,
	26,
	27,
	23,
	10,
	37,
	1437,
	2206,
	2206,
	1431,
	2207,
	2208,
	2209,
	2210,
	2211,
	2212,
	2213,
	2214,
	56,
	2215,
	23,
	23,
	23,
	3,
	2216,
	2217,
	2218,
	2219,
	23,
	337,
	731,
	31,
	89,
	337,
	731,
	337,
	731,
	337,
	731,
	337,
	337,
	1442,
	23,
	23,
	23,
	23,
	31,
	89,
	32,
	10,
	32,
	10,
	32,
	10,
	14,
	14,
	14,
	14,
	154,
	154,
	154,
	154,
	9,
	26,
	26,
	9,
	9,
	9,
	30,
	23,
	32,
	23,
	23,
	23,
	23,
	3,
	31,
	89,
	14,
	14,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	10,
	89,
	106,
	23,
	3,
	26,
	26,
	23,
	23,
	23,
	23,
	3,
	14,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1418,
	1417,
	1418,
	1417,
	23,
	23,
	23,
	23,
	1418,
	1417,
	1418,
	1417,
	23,
	23,
	23,
	23,
	23,
	1418,
	1417,
	1418,
	1417,
	23,
	23,
	23,
	23,
	23,
	1434,
	23,
	-1,
	445,
	-1,
	1,
	2220,
	2221,
	106,
	2222,
	1599,
	46,
	46,
	46,
	46,
	46,
	46,
	1456,
	49,
	49,
	31,
	89,
	26,
	14,
	23,
	23,
	23,
	23,
	32,
	10,
	23,
	26,
	106,
	106,
	164,
	23,
	23,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x060000A7, { 0, 3 } },
	{ 0x060000A9, { 3, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)1, 29091 },
	{ (Il2CppRGCTXDataType)3, 25740 },
	{ (Il2CppRGCTXDataType)2, 29091 },
	{ (Il2CppRGCTXDataType)2, 29092 },
};
extern const Il2CppCodeGenModule g_LeanCommonCodeGenModule;
const Il2CppCodeGenModule g_LeanCommonCodeGenModule = 
{
	"LeanCommon.dll",
	203,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
};
