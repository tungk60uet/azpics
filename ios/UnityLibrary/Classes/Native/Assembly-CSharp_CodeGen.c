﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 <id>j__TPar <>f__AnonymousType0`4::get_id()
// 0x00000002 <seq>j__TPar <>f__AnonymousType0`4::get_seq()
// 0x00000003 <name>j__TPar <>f__AnonymousType0`4::get_name()
// 0x00000004 <data>j__TPar <>f__AnonymousType0`4::get_data()
// 0x00000005 System.Void <>f__AnonymousType0`4::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
// 0x00000006 System.Boolean <>f__AnonymousType0`4::Equals(System.Object)
// 0x00000007 System.Int32 <>f__AnonymousType0`4::GetHashCode()
// 0x00000008 System.String <>f__AnonymousType0`4::ToString()
// 0x00000009 System.Void NativeAPI::OnUnityMessage(System.String)
extern void NativeAPI_OnUnityMessage_m37E97E313FD4FA3B821FC5CAB30F2CF89E42FE11 (void);
// 0x0000000A System.Void NativeAPI::OnUnitySceneLoaded(System.String,System.Int32,System.Boolean,System.Boolean)
extern void NativeAPI_OnUnitySceneLoaded_mACC814F3778DC1C7D58F7D5E544AC7041830C7F1 (void);
// 0x0000000B System.Void NativeAPI::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void NativeAPI_OnSceneLoaded_m648E2B7082BA98A576B212037EF0C65DD7E4D81C (void);
// 0x0000000C System.Void NativeAPI::SendMessageToFlutter(System.String)
extern void NativeAPI_SendMessageToFlutter_m64F5E97AD13823EBE038477A3904F5D36A4DD7F7 (void);
// 0x0000000D System.Void NativeAPI::ShowHostMainWindow()
extern void NativeAPI_ShowHostMainWindow_m5B4408679381319B7A871C0DEE7B3635DFCDA1C7 (void);
// 0x0000000E System.Void NativeAPI::UnloadMainWindow()
extern void NativeAPI_UnloadMainWindow_m550CFAC39B2D199C11186B9A7A36D25307EE241E (void);
// 0x0000000F System.Void NativeAPI::QuitUnityWindow()
extern void NativeAPI_QuitUnityWindow_mC7084563F0D750771CBADB5A68B4E46F3B1C8160 (void);
// 0x00000010 System.Void NativeAPI::.ctor()
extern void NativeAPI__ctor_m6AF8C586F2C4674EE98EA0EEC7E4DE11D26FD038 (void);
// 0x00000011 T SingletonMonoBehaviour`1::get_Instance()
// 0x00000012 T SingletonMonoBehaviour`1::CreateSingleton()
// 0x00000013 System.Void SingletonMonoBehaviour`1::.ctor()
// 0x00000014 System.Void SingletonMonoBehaviour`1::.cctor()
// 0x00000015 MessageHandler MessageHandler::Deserialize(System.String)
extern void MessageHandler_Deserialize_mE6CDB21EAEA2491B135D4DDC69091312F68ED426 (void);
// 0x00000016 T MessageHandler::getData()
// 0x00000017 System.Void MessageHandler::.ctor(System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JToken)
extern void MessageHandler__ctor_mEF0C5F957F8FAC2E9EA73C4749BFAF9B727547FF (void);
// 0x00000018 System.Void MessageHandler::send(System.Object)
extern void MessageHandler_send_m6E4C9DE3BD5C18B8DD958734711E4FA0803B895C (void);
// 0x00000019 System.Void UnityMessage::.ctor()
extern void UnityMessage__ctor_m418530AF3414A53A37F1433AEE0C8A83206D44A6 (void);
// 0x0000001A System.Int32 UnityMessageManager::generateId()
extern void UnityMessageManager_generateId_m105F25DED7848A21DD040716977EF8B8785C3846 (void);
// 0x0000001B System.Void UnityMessageManager::add_OnMessage(UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_add_OnMessage_m2725B086DA28C9E73814BE3D96E21BC3F3E3BAA4 (void);
// 0x0000001C System.Void UnityMessageManager::remove_OnMessage(UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_remove_OnMessage_m67B6DA617E39FEA4523E49A8D40590E6F6F19B07 (void);
// 0x0000001D System.Void UnityMessageManager::add_OnFlutterMessage(UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_add_OnFlutterMessage_m7FD992E772A26B34DD346B04C3D68A0B1F92805A (void);
// 0x0000001E System.Void UnityMessageManager::remove_OnFlutterMessage(UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_remove_OnFlutterMessage_m623F9C78BDD8E605C5EEE5AC1400C4A2F0FFF892 (void);
// 0x0000001F System.Void UnityMessageManager::Start()
extern void UnityMessageManager_Start_m8BEE1ACBB306B976AB4A5BEC9AF6F376AA4D2137 (void);
// 0x00000020 System.Void UnityMessageManager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void UnityMessageManager_OnSceneLoaded_mF79096186CFB8E736D4D3404176A3294583DB278 (void);
// 0x00000021 System.Void UnityMessageManager::ShowHostMainWindow()
extern void UnityMessageManager_ShowHostMainWindow_m2C68A5D1CE022ECCE2680E4A4E7583C15F0FA311 (void);
// 0x00000022 System.Void UnityMessageManager::UnloadMainWindow()
extern void UnityMessageManager_UnloadMainWindow_m0EAB6ABA36AE636F13B9CC0D873093EBA084885E (void);
// 0x00000023 System.Void UnityMessageManager::QuitUnityWindow()
extern void UnityMessageManager_QuitUnityWindow_m617D71DD8130842B8E365A8EF204F050246A4CBC (void);
// 0x00000024 System.Void UnityMessageManager::SendMessageToFlutter(System.String)
extern void UnityMessageManager_SendMessageToFlutter_mC8D4CB3F70B56F758CC34B3246EE018AF6E4805A (void);
// 0x00000025 System.Void UnityMessageManager::SendMessageToFlutter(UnityMessage)
extern void UnityMessageManager_SendMessageToFlutter_m32B9FFB546C6D09C86CF79E046F7D00BB5AECD6E (void);
// 0x00000026 System.Void UnityMessageManager::onMessage(System.String)
extern void UnityMessageManager_onMessage_m6E60A5C3A00B83B070276B32B727847F9CEEF413 (void);
// 0x00000027 System.Void UnityMessageManager::onFlutterMessage(System.String)
extern void UnityMessageManager_onFlutterMessage_m7DF88A89869A1FF528735694C4855BD8C0DD74F0 (void);
// 0x00000028 System.Void UnityMessageManager::.ctor()
extern void UnityMessageManager__ctor_mE89F6AEB4A75A1E52A8DF59747ACC13D3858C7C2 (void);
// 0x00000029 System.Void UnityMessageManager::.cctor()
extern void UnityMessageManager__cctor_mA16D91AB3FC363648DDA304555ECCFFF699591E5 (void);
// 0x0000002A SROptions SROptions::get_Current()
extern void SROptions_get_Current_m82BF8479C330727BB3C7012FFA818837D9B8A856 (void);
// 0x0000002B System.Void SROptions::OnStartup()
extern void SROptions_OnStartup_m9E3C90F00C547C697A3D9FAF50FEF5D54CDBCDC0 (void);
// 0x0000002C System.Void SROptions::add_PropertyChanged(SROptionsPropertyChanged)
extern void SROptions_add_PropertyChanged_mA0871B8690A19A5500B96D4A3AEC937C40B9C1C6 (void);
// 0x0000002D System.Void SROptions::remove_PropertyChanged(SROptionsPropertyChanged)
extern void SROptions_remove_PropertyChanged_mD4EE8133CEC4A2CA9BACBCBFB13A32C1CEBE367F (void);
// 0x0000002E System.Void SROptions::OnPropertyChanged(System.String)
extern void SROptions_OnPropertyChanged_m6672EED85D4ACBE958B96E759C0BA39290CB90D1 (void);
// 0x0000002F System.Void SROptions::add_InterfacePropertyChangedEventHandler(System.ComponentModel.PropertyChangedEventHandler)
extern void SROptions_add_InterfacePropertyChangedEventHandler_mD4C6B3CE455368C51F68C2AAE3FF2B8592034B24 (void);
// 0x00000030 System.Void SROptions::remove_InterfacePropertyChangedEventHandler(System.ComponentModel.PropertyChangedEventHandler)
extern void SROptions_remove_InterfacePropertyChangedEventHandler_m473DA4B90052E59207C19EB020DACB08A5453225 (void);
// 0x00000031 System.Void SROptions::System.ComponentModel.INotifyPropertyChanged.add_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void SROptions_System_ComponentModel_INotifyPropertyChanged_add_PropertyChanged_m74570EAFAFB8FC9CCF87D5CBD7CFDDA5DBBA7C29 (void);
// 0x00000032 System.Void SROptions::System.ComponentModel.INotifyPropertyChanged.remove_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void SROptions_System_ComponentModel_INotifyPropertyChanged_remove_PropertyChanged_m482FF52259193EF1549073FCA814052FBA6C0C03 (void);
// 0x00000033 System.Void SROptions::.ctor()
extern void SROptions__ctor_m2BCAE5898CD8DFF144134B689C779BD25CD9651E (void);
// 0x00000034 System.Void SROptions::.cctor()
extern void SROptions__cctor_m3E2D64545DADBAFC5987FD427D4F96B03270F8E4 (void);
// 0x00000035 System.Void SROptionsPropertyChanged::.ctor(System.Object,System.IntPtr)
extern void SROptionsPropertyChanged__ctor_m61B22632CE2B5113411E2A6EDD21298BDC460684 (void);
// 0x00000036 System.Void SROptionsPropertyChanged::Invoke(System.Object,System.String)
extern void SROptionsPropertyChanged_Invoke_m9E9CC44C4B9BA72583F4A154C180CED87D78BA60 (void);
// 0x00000037 System.IAsyncResult SROptionsPropertyChanged::BeginInvoke(System.Object,System.String,System.AsyncCallback,System.Object)
extern void SROptionsPropertyChanged_BeginInvoke_m3D3F30B086D91AC6C6C81BAF7E003E57F5F01B06 (void);
// 0x00000038 System.Void SROptionsPropertyChanged::EndInvoke(System.IAsyncResult)
extern void SROptionsPropertyChanged_EndInvoke_mA032503CA4E5AA7C2BF45C08EAE80451817D92D5 (void);
// 0x00000039 System.Void CanvasScreenShot::add_OnPictureTaken(CanvasScreenShot/takePictureHandler)
extern void CanvasScreenShot_add_OnPictureTaken_m4D26FFA7CE4F1D28A3B51AED5E902789C377AB91 (void);
// 0x0000003A System.Void CanvasScreenShot::remove_OnPictureTaken(CanvasScreenShot/takePictureHandler)
extern void CanvasScreenShot_remove_OnPictureTaken_mB6683D6A5BDAB7B3C71F4725259FFEFF1D794393 (void);
// 0x0000003B System.Void CanvasScreenShot::takeScreenShot(UnityEngine.Canvas,SCREENSHOT_TYPE,System.Boolean)
extern void CanvasScreenShot_takeScreenShot_m7BBB478C6AC0A6ED6941A53520C83F86B04DF569 (void);
// 0x0000003C System.Collections.IEnumerator CanvasScreenShot::_takeScreenShot(UnityEngine.Canvas,SCREENSHOT_TYPE,System.Boolean)
extern void CanvasScreenShot__takeScreenShot_m1355462C6755EC5299F9F5B708CEC1521FE348D2 (void);
// 0x0000003D UnityEngine.GameObject CanvasScreenShot::duplicateUI(UnityEngine.GameObject,System.String)
extern void CanvasScreenShot_duplicateUI_mD7D6CB0A15A6D1871FD48B7D2422C38699A26ECF (void);
// 0x0000003E UnityEngine.UI.Image[] CanvasScreenShot::getAllImagesFromCanvas(UnityEngine.GameObject,System.Boolean)
extern void CanvasScreenShot_getAllImagesFromCanvas_mCBE2EAA165FAA3EBDFB49151180C0F505367289F (void);
// 0x0000003F UnityEngine.UI.Text[] CanvasScreenShot::getAllTextsFromCanvas(UnityEngine.GameObject,System.Boolean)
extern void CanvasScreenShot_getAllTextsFromCanvas_m41B5B9914C519ECDC5855BE4133EDF91FC6F87A4 (void);
// 0x00000040 UnityEngine.Canvas[] CanvasScreenShot::getAllCanvasFromCanvas(UnityEngine.Canvas,System.Boolean)
extern void CanvasScreenShot_getAllCanvasFromCanvas_m26BD602102343DEF4D0F7E1CAEDE0CACBA7FB8BA (void);
// 0x00000041 UnityEngine.Canvas[] CanvasScreenShot::getAllCanvasInScene(System.Boolean)
extern void CanvasScreenShot_getAllCanvasInScene_mF1AFC51F811A428ECDFB9EEE3A21AF5E26B96776 (void);
// 0x00000042 System.Void CanvasScreenShot::showImages(UnityEngine.UI.Image[],System.Boolean)
extern void CanvasScreenShot_showImages_m3ACEACB828C6DC2BC2730E3C61DFCA4231BA7476 (void);
// 0x00000043 System.Void CanvasScreenShot::showTexts(UnityEngine.UI.Text[],System.Boolean)
extern void CanvasScreenShot_showTexts_m23A72E1DDE2D6CFEB46352E8AD5B5C1F33D28014 (void);
// 0x00000044 System.Void CanvasScreenShot::showCanvas(UnityEngine.Canvas[],System.Boolean)
extern void CanvasScreenShot_showCanvas_m6C850980A8CF85ED62F97F358E252D2C20D0094A (void);
// 0x00000045 System.Void CanvasScreenShot::showCanvas(UnityEngine.Canvas,System.Boolean)
extern void CanvasScreenShot_showCanvas_mF450C3EE37CAC7D2131B50BE8000C9EABC2C27DD (void);
// 0x00000046 System.Void CanvasScreenShot::showCanvasExcept(UnityEngine.Canvas[],UnityEngine.Canvas,System.Boolean)
extern void CanvasScreenShot_showCanvasExcept_m7A6B757D4E13FF6BBF06C7957BF9913CA38843FF (void);
// 0x00000047 System.Void CanvasScreenShot::showCanvasExcept(UnityEngine.Canvas[],UnityEngine.Canvas[],System.Boolean)
extern void CanvasScreenShot_showCanvasExcept_m0AECD0FF313C38257D7C84DC14BFEE8909B0CBE8 (void);
// 0x00000048 System.Void CanvasScreenShot::resetPosAndRot(UnityEngine.GameObject)
extern void CanvasScreenShot_resetPosAndRot_mF00E4D195379135E43FA325817561AB63736605C (void);
// 0x00000049 System.Void CanvasScreenShot::.ctor()
extern void CanvasScreenShot__ctor_mEBD4D4C3600BF6A34A40C4363816DD514CF53A6A (void);
// 0x0000004A System.Void ColorAdjust::Awake()
extern void ColorAdjust_Awake_m900CAA678518F28F7EAA2DA0E8D05D6CF9606016 (void);
// 0x0000004B System.Void ColorAdjust::Start()
extern void ColorAdjust_Start_m40BF2F361BB0B109D3F0609E37BBBC4963AE05B1 (void);
// 0x0000004C System.Void ColorAdjust::CallUpdate()
extern void ColorAdjust_CallUpdate_m7E6FF358335770ED3EDC20FFFD46C27F0FCA14CC (void);
// 0x0000004D System.Void ColorAdjust::Update()
extern void ColorAdjust_Update_m1E54EF5BE42A19C79314E2DBD499CB2EE56593A1 (void);
// 0x0000004E System.Void ColorAdjust::XUpdate()
extern void ColorAdjust_XUpdate_m24794D1D7152BC71DEE05A4538E7AD5EB732C3F1 (void);
// 0x0000004F System.Void ColorAdjust::OnDestroy()
extern void ColorAdjust_OnDestroy_m948B6A87FAC69D64558EAC76818F2188A8EF013E (void);
// 0x00000050 System.Void ColorAdjust::OnDisable()
extern void ColorAdjust_OnDisable_m322372F24255D0DE09E94B73BE596E0E4096CDD3 (void);
// 0x00000051 System.Void ColorAdjust::OnEnable()
extern void ColorAdjust_OnEnable_m0A815201281A85B62AB0902834E49FC454651C75 (void);
// 0x00000052 System.Void ColorAdjust::.ctor()
extern void ColorAdjust__ctor_mCAB92FC2E2B1FB2CCBEB9B641B9B71258BEFAABA (void);
// 0x00000053 System.Void ICommand::Execute()
// 0x00000054 System.Void ICommand::Undo()
// 0x00000055 System.Void CommandHandler::Start()
extern void CommandHandler_Start_mE70AE4FFCF2EDF710818EFA307EC7848555394BB (void);
// 0x00000056 System.Void CommandHandler::UpdateUI()
extern void CommandHandler_UpdateUI_mD8B2DC1480DBE38839DF45AEA0BB4E1F2F7A28DB (void);
// 0x00000057 System.Void CommandHandler::AddCommand(ICommand)
extern void CommandHandler_AddCommand_m8EC29B56202C4CC55A7BCD191C338992C4C754C6 (void);
// 0x00000058 System.Void CommandHandler::UndoCommand()
extern void CommandHandler_UndoCommand_m7D84861A533248CDC7AF0576552DF23C890BA4C5 (void);
// 0x00000059 System.Void CommandHandler::RedoCommand()
extern void CommandHandler_RedoCommand_m6174834550422EA1A42AEEFBB725A40AEE498E16 (void);
// 0x0000005A System.Void CommandHandler::.ctor()
extern void CommandHandler__ctor_m6A9FEAE4B964ED8C24CEC96110FC71EEF21610EF (void);
// 0x0000005B System.Void MoveCommand::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.RectTransform)
extern void MoveCommand__ctor_m21E8C0DD5B2F1EB200908F88C427D646E26F6952 (void);
// 0x0000005C System.Void MoveCommand::Execute()
extern void MoveCommand_Execute_mA8B048E56C8109811B9EC54DD403045D9F89E009 (void);
// 0x0000005D System.Void MoveCommand::Undo()
extern void MoveCommand_Undo_m7A5459289621642AA95D91722E2E80991E5DC181 (void);
// 0x0000005E System.Void CustomImage::Start()
extern void CustomImage_Start_m19B55FF89F25731A2C73D4C68FF514A0D1489C9B (void);
// 0x0000005F System.Boolean CustomImage::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern void CustomImage_IsRaycastLocationValid_m1D6119B97791515A1EDE1FE9354A4DDAC6F6E277 (void);
// 0x00000060 System.Void CustomImage::.ctor()
extern void CustomImage__ctor_mA59B58EA7B0B73A5B9735A5211106A3FB217A22A (void);
// 0x00000061 System.Void EditContainer::Start()
extern void EditContainer_Start_m9A80A0E61100FAE6CDF838C81AB8086A258C2672 (void);
// 0x00000062 System.Collections.IEnumerator EditContainer::LoadAssetBundle(System.String)
extern void EditContainer_LoadAssetBundle_m755F22CC949A46F08A32D6A9ED2E764286E9549D (void);
// 0x00000063 System.Void EditContainer::LateUpdate()
extern void EditContainer_LateUpdate_mAD5EF4CBFB94488F6661CD6882C9F60356284FB4 (void);
// 0x00000064 System.Void EditContainer::Export()
extern void EditContainer_Export_m5D2CEB6EC87E6432BA6204CA688E13DCC4C3CE47 (void);
// 0x00000065 System.Collections.IEnumerator EditContainer::takeScreenShot()
extern void EditContainer_takeScreenShot_m788AC417CB80F52FB72B12F9700D74951FDF4478 (void);
// 0x00000066 System.Void EditContainer::.ctor()
extern void EditContainer__ctor_mC05773A42475D685DAD6747BE7D96E5E1910B4D1 (void);
// 0x00000067 UnityEngine.RectTransform EditTransform::get_SelectedRect()
extern void EditTransform_get_SelectedRect_m7F119647DC0C32C1B348489E0DCE35EB340224A4 (void);
// 0x00000068 System.Void EditTransform::set_SelectedRect(UnityEngine.RectTransform)
extern void EditTransform_set_SelectedRect_m6E3620F94221577FA410F706FD3209D24F0C3D8B (void);
// 0x00000069 System.Void EditTransform::Awake()
extern void EditTransform_Awake_mB94268E99909F87AFF5DCD61EC7E298AB5A62779 (void);
// 0x0000006A System.Void EditTransform::OnBegin()
extern void EditTransform_OnBegin_m867EF3A0944650E27B065A7FA69AF0E58E4BFEC0 (void);
// 0x0000006B System.Void EditTransform::OnEnd()
extern void EditTransform_OnEnd_m3C5D552B727BFC8AABD0D42C197AB5C552FCDBA5 (void);
// 0x0000006C System.Void EditTransform::LateUpdate()
extern void EditTransform_LateUpdate_m156B8BBFC659D75A6E51D2975C50E2E918CEC396 (void);
// 0x0000006D System.Boolean EditTransform::UpdateRect(UnityEngine.RectTransform)
extern void EditTransform_UpdateRect_mCD2070CFBDE055E75A48606DC4CD09CD141A5F2B (void);
// 0x0000006E UnityEngine.Vector3 EditTransform::WorldToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3,System.Boolean)
extern void EditTransform_WorldToViewportPoint_mF7593C1FD5F0D4196FDD0BA5D09E4F681D3E6E05 (void);
// 0x0000006F System.Void EditTransform::.ctor()
extern void EditTransform__ctor_mE7069CC18464C1C8A599AAF15C668542B7E0A2A3 (void);
// 0x00000070 System.Void ImageProp::Awake()
extern void ImageProp_Awake_m41D9581CB5B8C307ADE0894363414AEEA57DA79E (void);
// 0x00000071 System.Void ImageProp::OnDestroy()
extern void ImageProp_OnDestroy_mC451746447E3914277053DE45111C2F12C9A101B (void);
// 0x00000072 System.Void ImageProp::OnOpen()
extern void ImageProp_OnOpen_m5062DA60E9BF388C4CEF74E11006A8C1E815400C (void);
// 0x00000073 System.Void ImageProp::PickImage(System.Boolean)
extern void ImageProp_PickImage_mBA702034A1664B1A1F6CA31A464AC2341C143A7B (void);
// 0x00000074 System.Collections.IEnumerator ImageProp::RemoveBG(System.String)
extern void ImageProp_RemoveBG_m998A6BDB0F23674C4F3EF98DD6C2A6643315719C (void);
// 0x00000075 System.Void ImageProp::LoadByteToTexture(System.Byte[])
extern void ImageProp_LoadByteToTexture_m09822E22A71C80CB80331DB99C322864AB0C6FD4 (void);
// 0x00000076 System.Void ImageProp::OnSatChange(System.Single)
extern void ImageProp_OnSatChange_m1928C4149693886EC9FBB695B03AE2F03B57B558 (void);
// 0x00000077 System.Void ImageProp::OnConChange(System.Single)
extern void ImageProp_OnConChange_m624719B705B55F6C85FCF830192B35AB428A21C7 (void);
// 0x00000078 System.Void ImageProp::OnBriChange(System.Single)
extern void ImageProp_OnBriChange_m58CA693C3ACF41D8410ED0F1C99F428D15E3F873 (void);
// 0x00000079 System.Void ImageProp::OnHueChange(System.Single)
extern void ImageProp_OnHueChange_m67FD4734A3E5703E86165400216912E74042C793 (void);
// 0x0000007A System.Void ImageProp::OnAlphaChange(System.Single)
extern void ImageProp_OnAlphaChange_mA4B1CF679035A2E2D318472A13DCA1891265A759 (void);
// 0x0000007B System.Void ImageProp::Flip(System.Boolean)
extern void ImageProp_Flip_m27AD08E43202B887DB2F64471CF78DA50B546691 (void);
// 0x0000007C System.Void ImageProp::UpdateLayer(System.Boolean)
extern void ImageProp_UpdateLayer_mF4AD7441D3641418B9E4F4562E968C591F97C582 (void);
// 0x0000007D System.Void ImageProp::.ctor()
extern void ImageProp__ctor_m4C9D4ED85E662EBE32A0A30026B3A317E373D267 (void);
// 0x0000007E System.Void TextProp::OnOpen()
extern void TextProp_OnOpen_mA1A67C59AB4AFD33BDA9A45120B6A02D9028400B (void);
// 0x0000007F System.Void TextProp::UpdateText(System.String)
extern void TextProp_UpdateText_mEA57F0159B1185C92DFAE1D22E576F966095DAAF (void);
// 0x00000080 System.Void TextProp::UpdateAlign(System.Int32)
extern void TextProp_UpdateAlign_mEEB8FDCF99FC587A27275C5D842093C4FB6BD1A6 (void);
// 0x00000081 System.Void TextProp::UpdateLayer(System.Boolean)
extern void TextProp_UpdateLayer_mA58DB76FB822EA8AA0C4BB02DFA6422FAFC43A5A (void);
// 0x00000082 System.Void TextProp::.ctor()
extern void TextProp__ctor_m937A709911F80676C443960DF91C8A5F84B145DF (void);
// 0x00000083 System.Boolean UIFlippable::get_horizontal()
extern void UIFlippable_get_horizontal_mB630C34B97FE69F9250008F231575E05D715B0EC (void);
// 0x00000084 System.Void UIFlippable::set_horizontal(System.Boolean)
extern void UIFlippable_set_horizontal_m4AF04B475EFEECE0FFA7D1AD3E44707E780B6D5D (void);
// 0x00000085 System.Boolean UIFlippable::get_vertical()
extern void UIFlippable_get_vertical_m2E216762ED5618E7D8A67C698FEF41742C81A1A1 (void);
// 0x00000086 System.Void UIFlippable::set_vertical(System.Boolean)
extern void UIFlippable_set_vertical_mC076407E6E44312F226723A90C71AAEB4E631C1F (void);
// 0x00000087 System.Void UIFlippable::ModifyMesh(UnityEngine.UI.VertexHelper)
extern void UIFlippable_ModifyMesh_m9B89FC767EDFD495D71845846CEC1CDE24942D37 (void);
// 0x00000088 System.Void UIFlippable::.ctor()
extern void UIFlippable__ctor_m3593D59D6C0E7F258B5C50C48663358FE5D3B07B (void);
// 0x00000089 System.Void Ultils::LoadImage(UnityEngine.UI.Image,System.String)
extern void Ultils_LoadImage_mDB3845CC81EDC0B64F3C6FA391527FC6600017CC (void);
// 0x0000008A System.Collections.IEnumerator Ultils::LoadImageCor(UnityEngine.UI.Image,System.String)
extern void Ultils_LoadImageCor_m3FFBC2521C421430947C01A5A985FE64A18EBA97 (void);
// 0x0000008B System.Void Ultils::SetSizeAndPos(UnityEngine.RectTransform,UnityEngine.Vector4)
extern void Ultils_SetSizeAndPos_m4693FBFD659F774CDF9F5E9AD784F5FC36F7EF7A (void);
// 0x0000008C UnityEngine.Vector4 Ultils::ToSizePos(UnityEngine.RectTransform)
extern void Ultils_ToSizePos_m055128823A20965D47F34DFABB27170B3D843054 (void);
// 0x0000008D UnityEngine.Vector2 DanielLochner.Assets.SimpleZoom.MultiTouchScrollRect::get_MultiTouchPosition()
extern void MultiTouchScrollRect_get_MultiTouchPosition_mA132F72E3FE982FDD30944012EA73A0CB471C405 (void);
// 0x0000008E System.Void DanielLochner.Assets.SimpleZoom.MultiTouchScrollRect::Update()
extern void MultiTouchScrollRect_Update_m01FB760AD4E0F3B0E047239AAA9E1AAA654AC279 (void);
// 0x0000008F System.Void DanielLochner.Assets.SimpleZoom.MultiTouchScrollRect::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void MultiTouchScrollRect_OnBeginDrag_m4B4605E853E512E73B607610D6247A86C060FF7E (void);
// 0x00000090 System.Void DanielLochner.Assets.SimpleZoom.MultiTouchScrollRect::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void MultiTouchScrollRect_OnDrag_m7D94703FFC72B99ECE8777809A1BB674E1E44F12 (void);
// 0x00000091 System.Void DanielLochner.Assets.SimpleZoom.MultiTouchScrollRect::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void MultiTouchScrollRect_OnEndDrag_mC1CCC5BB7BBAE5E9C83B5B3FC331524CD1F6AE84 (void);
// 0x00000092 System.Void DanielLochner.Assets.SimpleZoom.MultiTouchScrollRect::.ctor()
extern void MultiTouchScrollRect__ctor_mC7985D545CE4DF81C89975ED1A3237EA5105CB98 (void);
// 0x00000093 UnityEngine.RectTransform DanielLochner.Assets.SimpleZoom.SimpleZoom::get_Content()
extern void SimpleZoom_get_Content_mBCD8F5A2DF4A457C1EB4E54F8AA65DFF0793AF00 (void);
// 0x00000094 UnityEngine.RectTransform DanielLochner.Assets.SimpleZoom.SimpleZoom::get_Viewport()
extern void SimpleZoom_get_Viewport_m8505EA0E6A77196CF601FB174EF21E6CB95D5B6A (void);
// 0x00000095 System.Single DanielLochner.Assets.SimpleZoom.SimpleZoom::get_TargetZoom()
extern void SimpleZoom_get_TargetZoom_mF589E7939FA9147B5E9E5C5E1AC59F646F36C33D (void);
// 0x00000096 System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::set_TargetZoom(System.Single)
extern void SimpleZoom_set_TargetZoom_m532721634919DE5497E9F80448E525FC988EDF34 (void);
// 0x00000097 System.Single DanielLochner.Assets.SimpleZoom.SimpleZoom::get_CurrentZoom()
extern void SimpleZoom_get_CurrentZoom_mC8263F84762B1CD1FE4D4135D540A8852C47B6C9 (void);
// 0x00000098 System.Single DanielLochner.Assets.SimpleZoom.SimpleZoom::get_ZoomProgress()
extern void SimpleZoom_get_ZoomProgress_m3517281F97F2193634C47DE94160C5B28AEC0EE6 (void);
// 0x00000099 UnityEngine.Vector4 DanielLochner.Assets.SimpleZoom.SimpleZoom::get_ZoomMargin()
extern void SimpleZoom_get_ZoomMargin_m2F30C2B39B85A81CB4379AA869C06BAF5FC4732D (void);
// 0x0000009A System.Boolean DanielLochner.Assets.SimpleZoom.SimpleZoom::get_UsingUnityRemote()
extern void SimpleZoom_get_UsingUnityRemote_m49899D5B82010A5D10F67294265D55DC6ACBAF8A (void);
// 0x0000009B System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::Start()
extern void SimpleZoom_Start_m143BC264599FA2B77261A1F94BEE54B50EE09E12 (void);
// 0x0000009C System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::Update()
extern void SimpleZoom_Update_m1847F85AA1F11073F947FF6D9FFA1E49CC4E406D (void);
// 0x0000009D System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void SimpleZoom_OnPointerClick_m3DD5DF50ED65FF46DC28E522946C24EA99356444 (void);
// 0x0000009E System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::Initialize()
extern void SimpleZoom_Initialize_m4B27BA6FC6D3982DB407B5FD333983E706100E58 (void);
// 0x0000009F System.Boolean DanielLochner.Assets.SimpleZoom.SimpleZoom::Validate()
extern void SimpleZoom_Validate_mCFDDA0D11E8AD744F7F0685BCD9FAE14C6B10599 (void);
// 0x000000A0 System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::Setup()
extern void SimpleZoom_Setup_m29C47BC2F9C476E73AEC83882FC1BCBA329C3C49 (void);
// 0x000000A1 System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::OnPivotZoomUpdate()
extern void SimpleZoom_OnPivotZoomUpdate_m830BDF743A688F5AA451571577567CA2C70A9404 (void);
// 0x000000A2 System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::OnHandheldUpdate()
extern void SimpleZoom_OnHandheldUpdate_m3E34167D3A83B1FBBEA0243FF0DC6A1942250C6E (void);
// 0x000000A3 System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::OnDesktopUpdate()
extern void SimpleZoom_OnDesktopUpdate_mBDA10370DEF1D793ECD22A4235D76CA2FBBBE4AF (void);
// 0x000000A4 System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::OnDoubleTap()
extern void SimpleZoom_OnDoubleTap_mA660D78FA72C8B8D377DF110C8B37C9C184D3FC1 (void);
// 0x000000A5 System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::OnZoomSlider()
extern void SimpleZoom_OnZoomSlider_m12D22BBA3916A1E76AF9BDEC0A80D163D40DF9C7 (void);
// 0x000000A6 System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::OnZoomView()
extern void SimpleZoom_OnZoomView_mB177A85BD77EA8BF34FFCF5534DC305A5C87E673 (void);
// 0x000000A7 System.Single DanielLochner.Assets.SimpleZoom.SimpleZoom::ElasticClamp(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void SimpleZoom_ElasticClamp_mE9BAAA6946FCA4F69E0C81A594507A83C3A4C1C4 (void);
// 0x000000A8 System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::SetZoom(System.Single,System.Single)
extern void SimpleZoom_SetZoom_m42588E17A00D16BA32F336D50D2C002CBA1C0A1E (void);
// 0x000000A9 System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::SetPivot(UnityEngine.Vector2)
extern void SimpleZoom_SetPivot_m80F23B688EFF8862C0F52DF8BE9235482ABFC00D (void);
// 0x000000AA System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::ZoomIn(UnityEngine.Vector2,System.Single,System.Single)
extern void SimpleZoom_ZoomIn_m14371BB9C505A4CDD56EFB97794BC8A293E56A99 (void);
// 0x000000AB System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::ZoomOut(UnityEngine.Vector2,System.Single,System.Single)
extern void SimpleZoom_ZoomOut_m6CDE481D2E227FF663582BD4FFA9FA8E268ED3F9 (void);
// 0x000000AC System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::.ctor()
extern void SimpleZoom__ctor_mA54C9790EEA211AE6C84CD5F3CE2A9637AF7B898 (void);
// 0x000000AD System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::<Setup>b__62_0()
extern void SimpleZoom_U3CSetupU3Eb__62_0_mA6F82113DC0D8C5122E97AF180D1B7A6ACBFB4CB (void);
// 0x000000AE System.Void DanielLochner.Assets.SimpleZoom.SimpleZoom::<Setup>b__62_1()
extern void SimpleZoom_U3CSetupU3Eb__62_1_mFD481E0181AF8A5EAA1E15DC406F638D95B37BBF (void);
// 0x000000AF System.Void PluginMaster.PsLayerDemo::Awake()
extern void PsLayerDemo_Awake_mB6C32E494D6B77B2D2705AC77DE8EADF92CC8C88 (void);
// 0x000000B0 System.Void PluginMaster.PsLayerDemo::OnModeChange()
extern void PsLayerDemo_OnModeChange_mF8E33019A2033DD76AFC4B967275B16B47F225BF (void);
// 0x000000B1 System.Void PluginMaster.PsLayerDemo::OnOppacityChange()
extern void PsLayerDemo_OnOppacityChange_m3D08C0098E3CA3D4E7583E605F238DFDAB8FC554 (void);
// 0x000000B2 System.Void PluginMaster.PsLayerDemo::.ctor()
extern void PsLayerDemo__ctor_m9A073E661C35014426214382FF6933995A17EF78 (void);
// 0x000000B3 System.Void PluginMaster.ScaleWithScreen::Update()
extern void ScaleWithScreen_Update_mA4CA5F94B31B03B3F1FEC5F6174D9736629E1DE8 (void);
// 0x000000B4 System.Void PluginMaster.ScaleWithScreen::.ctor()
extern void ScaleWithScreen__ctor_m1C580B5BCCE9106346DCA75961037084A86F0C13 (void);
// 0x000000B5 System.Boolean PluginMaster.PsGroup::get_thereAreUiLayersInChildren()
extern void PsGroup_get_thereAreUiLayersInChildren_mDA5BC41A4968E313BCEA0C1BE53C3A34EFA922F5 (void);
// 0x000000B6 UnityEngine.Canvas PluginMaster.PsGroup::get_canvas()
extern void PsGroup_get_canvas_mA56AEED0D767F121D5CFE5971BB7270EABF5E708 (void);
// 0x000000B7 System.String PluginMaster.PsGroup::GetShaderTypeName(PluginMaster.PsGroup/BlendingShaderType)
extern void PsGroup_GetShaderTypeName_mA8B6ADB9A72726738AAC8BB7891121D69BC41D4F (void);
// 0x000000B8 PluginMaster.PsGroup/BlendingShaderType PluginMaster.PsGroup::get_BlendingShader()
extern void PsGroup_get_BlendingShader_mA1B39CD5CC197CD276FBA5EF054EF3913B933CD8 (void);
// 0x000000B9 System.Void PluginMaster.PsGroup::set_BlendingShader(PluginMaster.PsGroup/BlendingShaderType)
extern void PsGroup_set_BlendingShader_m64727CDC41EA8625CAC02F2B0B9E8BD34F4ED60B (void);
// 0x000000BA PluginMaster.PsdBlendModeType/BlendModeType PluginMaster.PsGroup::get_BlendModeTypeInHierarchy()
extern void PsGroup_get_BlendModeTypeInHierarchy_m63594B7F1061AD36BBA891A606F1857BE038DC8E (void);
// 0x000000BB System.Void PluginMaster.PsGroup::set_BlendModeTypeInHierarchy(PluginMaster.PsdBlendModeType/BlendModeType)
extern void PsGroup_set_BlendModeTypeInHierarchy_mC22B6660111599601DA50D6F9C88AD49D8E2DBF0 (void);
// 0x000000BC PluginMaster.PsdBlendModeType/BlendModeType PluginMaster.PsGroup::get_BlendModeType()
extern void PsGroup_get_BlendModeType_m276E6679489B9B0505EA3FEACD51BE351B4DA1F5 (void);
// 0x000000BD System.Void PluginMaster.PsGroup::set_BlendModeType(PluginMaster.PsdBlendModeType/BlendModeType)
extern void PsGroup_set_BlendModeType_mD6E7FC2FC6535138A74CF8F3769D6A4D2229E55A (void);
// 0x000000BE PluginMaster.PsdBlendModeType/BlendModeType PluginMaster.PsGroup::get_InheritedBlending()
extern void PsGroup_get_InheritedBlending_m05FF7D0B951686A4D782D09A541D421D9A4E2D87 (void);
// 0x000000BF System.Void PluginMaster.PsGroup::InitMaterial()
extern void PsGroup_InitMaterial_mBB6D94C9B5A2C23EB7A209B2E2E0AFCEB584E490 (void);
// 0x000000C0 System.Single PluginMaster.PsGroup::get_OpacityInHierarchy()
extern void PsGroup_get_OpacityInHierarchy_m03E77DF21039ADE501B580A7CF555E35796DEF4A (void);
// 0x000000C1 System.Void PluginMaster.PsGroup::set_OpacityInHierarchy(System.Single)
extern void PsGroup_set_OpacityInHierarchy_m317E7448495E1969780AB7AEDDE7753E8AE8D8D7 (void);
// 0x000000C2 System.Single PluginMaster.PsGroup::get_Opacity()
extern void PsGroup_get_Opacity_mFB94637DB52391F3F9C80B968F97C8CCB7E8427D (void);
// 0x000000C3 System.Void PluginMaster.PsGroup::set_Opacity(System.Single)
extern void PsGroup_set_Opacity_mA1EA5CBD44E8930B3CFF6C5F37AC8161BC72BACE (void);
// 0x000000C4 System.Boolean PluginMaster.PsGroup::get_VisibleInHierarchy()
extern void PsGroup_get_VisibleInHierarchy_m9A39CEB7024E2B0CD42A10171ED0F1C77DE0DC68 (void);
// 0x000000C5 System.Void PluginMaster.PsGroup::set_VisibleInHierarchy(System.Boolean)
extern void PsGroup_set_VisibleInHierarchy_m2E156B89CA2FABC240B8A06072F2D5F594C53CDE (void);
// 0x000000C6 System.Boolean PluginMaster.PsGroup::get_Visible()
extern void PsGroup_get_Visible_mECEFB43194DA83EFF4D9C222A9C3C49C0DAC2D83 (void);
// 0x000000C7 System.Void PluginMaster.PsGroup::set_Visible(System.Boolean)
extern void PsGroup_set_Visible_m2D7E8E832A91B73FA0934111037F4CCC43CAD6DE (void);
// 0x000000C8 System.Void PluginMaster.PsGroup::Initialize(PluginMaster.PsdBlendModeType/BlendModeType,System.Single,System.Boolean,System.Boolean,PluginMaster.PsGroup/BlendingShaderType)
extern void PsGroup_Initialize_m1A3CAD02BC39D72A72133B5D125A9E4118584286 (void);
// 0x000000C9 System.Void PluginMaster.PsGroup::UpdateUiLayersInChildren()
extern void PsGroup_UpdateUiLayersInChildren_mF282DB8CD0806A3982918291672CF28D0465B875 (void);
// 0x000000CA System.Void PluginMaster.PsGroup::Start()
extern void PsGroup_Start_m4593DDFFC8516337302975163355D2F89C6471D1 (void);
// 0x000000CB System.Void PluginMaster.PsGroup::OnDestroy()
extern void PsGroup_OnDestroy_m91ADFF4B206A6AAD4CCDB270BA5DF48FF4D413AE (void);
// 0x000000CC System.Void PluginMaster.PsGroup::OnEnable()
extern void PsGroup_OnEnable_m16C6FBB7FD8B02AECD275088F27AE28B59F1EF03 (void);
// 0x000000CD System.Void PluginMaster.PsGroup::OnDisable()
extern void PsGroup_OnDisable_m65229058D1F28536B46D56B11CC605B456B8807F (void);
// 0x000000CE System.Void PluginMaster.PsGroup::OnTransformParentChanged()
extern void PsGroup_OnTransformParentChanged_mEF0B765A3F2A4854DF2AB8510067ABFC4E41DB10 (void);
// 0x000000CF System.Void PluginMaster.PsGroup::.ctor()
extern void PsGroup__ctor_mB1EB07F35D1EA313457B341EFB6BD4191D67F4D5 (void);
// 0x000000D0 PluginMaster.PsGroup/BlendingShaderType PluginMaster.PsLayer::get_BlendingShader()
extern void PsLayer_get_BlendingShader_m0022324B3232BC6839CE9E979A59F21B31CF21EF (void);
// 0x000000D1 System.Void PluginMaster.PsLayer::set_BlendingShader(PluginMaster.PsGroup/BlendingShaderType)
extern void PsLayer_set_BlendingShader_m9147559B86EEA4117C9FF87B6E9570FBC4694F64 (void);
// 0x000000D2 PluginMaster.PsdBlendModeType/BlendModeType PluginMaster.PsLayer::get_BlendModeTypeInHierarchy()
extern void PsLayer_get_BlendModeTypeInHierarchy_mDD961EC6CD7D2BC774895E3AC50AD22FE369C04A (void);
// 0x000000D3 System.Void PluginMaster.PsLayer::set_BlendModeTypeInHierarchy(PluginMaster.PsdBlendModeType/BlendModeType)
extern void PsLayer_set_BlendModeTypeInHierarchy_mF918E59292F4C531F875E5D0C4F9B7E144E5AFC4 (void);
// 0x000000D4 System.Void PluginMaster.PsLayer::Awake()
extern void PsLayer_Awake_m26982DF17D4D2C4C8A795C63DEFF10CEF9B7E258 (void);
// 0x000000D5 System.Void PluginMaster.PsLayer::Initialize(PluginMaster.PsdBlendModeType/BlendModeType,System.Single,System.Boolean,System.Boolean,PluginMaster.PsGroup/BlendingShaderType)
extern void PsLayer_Initialize_mC0D7F0F3FBCC34BE49D7A30BA90BFED591BD9FCE (void);
// 0x000000D6 System.Void PluginMaster.PsLayer::SetBlendMode(PluginMaster.PsdBlendModeType)
extern void PsLayer_SetBlendMode_mF7088057363C944290ACBC14DD0730E6541B4413 (void);
// 0x000000D7 System.Void PluginMaster.PsLayer::SetBlendOperation1(UnityEngine.Rendering.BlendOp,UnityEngine.Rendering.BlendMode,UnityEngine.Rendering.BlendMode)
extern void PsLayer_SetBlendOperation1_m1CEEBB9CFB7B62662B0034AF7DBD65DEBC84B2AA (void);
// 0x000000D8 System.Void PluginMaster.PsLayer::SetBlendOperation2(UnityEngine.Rendering.BlendOp,UnityEngine.Rendering.BlendMode,UnityEngine.Rendering.BlendMode)
extern void PsLayer_SetBlendOperation2_mF5A1C6EDB06058B56E15B940A0C198512C77F037 (void);
// 0x000000D9 System.Void PluginMaster.PsLayer::Update()
extern void PsLayer_Update_m7EAE925C05138A7C2739B588882FCAF17E5E7D5C (void);
// 0x000000DA System.String PluginMaster.PsLayer::get_DefaultShaderName()
// 0x000000DB System.String PluginMaster.PsLayer::get_ShaderName()
extern void PsLayer_get_ShaderName_mD64438F4F72863DE5F75CB97C6B433A599C09149 (void);
// 0x000000DC PluginMaster.PsLayer/ImageComponentProxy PluginMaster.PsLayer::get_Proxy()
extern void PsLayer_get_Proxy_m85A0D759E4D03E8C4BE62CD689A2BD34558AD1BB (void);
// 0x000000DD System.Void PluginMaster.PsLayer::set_Proxy(PluginMaster.PsLayer/ImageComponentProxy)
extern void PsLayer_set_Proxy_mC7194057B8C794BC9282B419740D934AEDCFDA0A (void);
// 0x000000DE System.Single PluginMaster.PsLayer::get_OpacityInHierarchy()
extern void PsLayer_get_OpacityInHierarchy_m9FA17C0716F1E7E371B0EDDD7022B5A580820656 (void);
// 0x000000DF System.Void PluginMaster.PsLayer::set_OpacityInHierarchy(System.Single)
extern void PsLayer_set_OpacityInHierarchy_m08BE2B4F3B5306E7E2E93F0DD5F2D9100CD2DE9B (void);
// 0x000000E0 System.Boolean PluginMaster.PsLayer::get_VisibleInHierarchy()
extern void PsLayer_get_VisibleInHierarchy_m35AC94EC3C7B7133720D6F70F6898BCC09C12B70 (void);
// 0x000000E1 System.Void PluginMaster.PsLayer::set_VisibleInHierarchy(System.Boolean)
extern void PsLayer_set_VisibleInHierarchy_mD5CB9EDC6488C96E2A0A08148E8F4F3EC7A80D1E (void);
// 0x000000E2 System.Boolean PluginMaster.PsLayer::get_Visible()
extern void PsLayer_get_Visible_m43A3B2C13B337EE85AC52826B140B0DB6077551A (void);
// 0x000000E3 System.Void PluginMaster.PsLayer::set_Visible(System.Boolean)
extern void PsLayer_set_Visible_m145003AF17AA235C32E4576A862C11D1BD10EAC4 (void);
// 0x000000E4 System.Void PluginMaster.PsLayer::InitMaterial()
extern void PsLayer_InitMaterial_m6813EA6634E363699EC4913BEEB4A6051FF84696 (void);
// 0x000000E5 System.Void PluginMaster.PsLayer::.ctor()
extern void PsLayer__ctor_m8EF9BC39C58936552ACEE3D0AA9A45FDB8B0E6EA (void);
// 0x000000E6 System.Void PluginMaster.PsLayer::.cctor()
extern void PsLayer__cctor_m419FBD70539D10B926BE8F03ED425DAA1938D6E4 (void);
// 0x000000E7 System.String PluginMaster.PsLayerImage::get_DefaultShaderName()
extern void PsLayerImage_get_DefaultShaderName_m3FB2042D5676CA10D6B5FDDE5324E8C59A0AE482 (void);
// 0x000000E8 System.Void PluginMaster.PsLayerImage::InitMaterial()
extern void PsLayerImage_InitMaterial_m01FEA5644EDA481C9CF3383CE86EA16DEF4E552E (void);
// 0x000000E9 System.Void PluginMaster.PsLayerImage::.ctor()
extern void PsLayerImage__ctor_mBE63B9D33F0C5F5BBDFA50F2068FFF913FF8007F (void);
// 0x000000EA System.String PluginMaster.PsLayerSprite::get_DefaultShaderName()
extern void PsLayerSprite_get_DefaultShaderName_mA6872D32CB63FC336FE1B113D39DEF4A61850E05 (void);
// 0x000000EB System.Void PluginMaster.PsLayerSprite::InitMaterial()
extern void PsLayerSprite_InitMaterial_m53EE63DE710F01E8BE7CD47A04FF873589421E74 (void);
// 0x000000EC System.Void PluginMaster.PsLayerSprite::.ctor()
extern void PsLayerSprite__ctor_mD2D860296E9051E30192B44E5BF06A2DEB5CB126 (void);
// 0x000000ED PluginMaster.PsdBlendModeType/FastBlendModeType PluginMaster.PsdBlendModeType::GetFastBlendModeType(PluginMaster.PsdBlendModeType/BlendModeType)
extern void PsdBlendModeType_GetFastBlendModeType_m896378857F26D57952783F7B20FDEF33A4E64797 (void);
// 0x000000EE PluginMaster.PsdBlendModeType/BlendModeType PluginMaster.PsdBlendModeType::GetGroupBlendModeType(PluginMaster.PsdBlendModeType/FastBlendModeType)
extern void PsdBlendModeType_GetGroupBlendModeType_mABA46E7FB3E154C6293CB5EF7AD2E7FC15A83C32 (void);
// 0x000000EF System.Void PluginMaster.PsdBlendModeType::.ctor()
extern void PsdBlendModeType__ctor_m6C2447BFD323EC888CAD6EDF6F6DE7389F9C4693 (void);
// 0x000000F0 System.Void PluginMaster.PsdBlendModeType::.ctor(System.Int32,System.String,System.String,System.Boolean)
extern void PsdBlendModeType__ctor_m6C417BBF6AA97F44CD5C6C1FCA5339FCE758CC65 (void);
// 0x000000F1 System.String PluginMaster.PsdBlendModeType::op_Implicit(PluginMaster.PsdBlendModeType)
extern void PsdBlendModeType_op_Implicit_mA820AA78CBC94949BC4199BEE30C28801564A737 (void);
// 0x000000F2 System.Int32 PluginMaster.PsdBlendModeType::op_Implicit(PluginMaster.PsdBlendModeType)
extern void PsdBlendModeType_op_Implicit_mDA61F08083F06A7F02FF942CDB59AF7446F17B2B (void);
// 0x000000F3 PluginMaster.PsdBlendModeType/BlendModeType PluginMaster.PsdBlendModeType::op_Implicit(PluginMaster.PsdBlendModeType)
extern void PsdBlendModeType_op_Implicit_mA225EAD32EBF4AB92EBF3CDB436487C701515B7E (void);
// 0x000000F4 PluginMaster.PsdBlendModeType PluginMaster.PsdBlendModeType::op_Implicit(System.String)
extern void PsdBlendModeType_op_Implicit_mA2F6A2022B82787FA2CA07D63FA74DC02DAFB420 (void);
// 0x000000F5 PluginMaster.PsdBlendModeType PluginMaster.PsdBlendModeType::op_Implicit(System.Int32)
extern void PsdBlendModeType_op_Implicit_m97039F1036A92A91883B4B382870FEB28DC8550F (void);
// 0x000000F6 PluginMaster.PsdBlendModeType PluginMaster.PsdBlendModeType::op_Implicit(PluginMaster.PsdBlendModeType/BlendModeType)
extern void PsdBlendModeType_op_Implicit_m2FD2A54C36EF1086FEAB6A68F751CB60F6D3F4A9 (void);
// 0x000000F7 System.Boolean PluginMaster.PsdBlendModeType::IsSimple(PluginMaster.PsdBlendModeType/BlendModeType)
extern void PsdBlendModeType_IsSimple_m3BF5EEEDFA0822D0A10F5D738E23654B7E17C58F (void);
// 0x000000F8 System.Void PluginMaster.PsdBlendModeType::.cctor()
extern void PsdBlendModeType__cctor_m112D4DAA7C21F49BA25E819DF744606BC85A3EE1 (void);
// 0x000000F9 Baum2.UIRoot Baum2.BaumUI::Instantiate(UnityEngine.GameObject,UnityEngine.GameObject)
extern void BaumUI_Instantiate_mF2588C90827124DDDB7FA1B48D9CC4E687F9B77E (void);
// 0x000000FA System.Void Baum2.Cache::CreateCache(UnityEngine.Transform,System.Collections.Generic.List`1<System.String>)
extern void Cache_CreateCache_m1B1EE5A2DBC69FCC49FFF38076D72835A5DB94F1 (void);
// 0x000000FB System.Void Baum2.Cache::ClearCache()
extern void Cache_ClearCache_m9E0E1D25BF13B5F9FF409078F3B1998B472D6F0F (void);
// 0x000000FC UnityEngine.GameObject Baum2.Cache::Get(System.String)
extern void Cache_Get_m8156F01185F25AEF2400DCFB2FB498B0444A8A03 (void);
// 0x000000FD System.Void Baum2.Cache::.ctor()
extern void Cache__ctor_m86CD81683A0A29924C9ABCA82443DE7B12E4E527 (void);
// 0x000000FE System.Void Baum2.Cache::.cctor()
extern void Cache__cctor_m87962E808695EE6DA160707A18AAC7C1FDC28E10 (void);
// 0x000000FF UnityEngine.GameObject Baum2.List::get_Content()
extern void List_get_Content_m42FB0DF3A5E1F9A3C945705F4E709D7DC6B0A7CB (void);
// 0x00000100 UnityEngine.RectTransform Baum2.List::get_ContentRectTransform()
extern void List_get_ContentRectTransform_m2C8702727320BEE67AEAC3574B632242562D8072 (void);
// 0x00000101 UnityEngine.RectTransform Baum2.List::get_RectTransform()
extern void List_get_RectTransform_m86C9498A130D634ABA846EDADB817302C32D8731 (void);
// 0x00000102 UnityEngine.UI.ScrollRect Baum2.List::get_ScrollRect()
extern void List_get_ScrollRect_m5E8DD6F8DAA466538D393B3751C980278C4A0A6F (void);
// 0x00000103 System.Void Baum2.List::set_Scrollbar(UnityEngine.UI.Scrollbar)
extern void List_set_Scrollbar_m12F7B2A7C22E735072421B725D21A1F8C1306FE7 (void);
// 0x00000104 System.Int32 Baum2.List::get_Count()
extern void List_get_Count_m68B841D260B93C4A4B21C38D8CAA9F3F47B67F83 (void);
// 0x00000105 System.Func`2<System.Int32,System.String> Baum2.List::get_UISelector()
extern void List_get_UISelector_mFBBB6516CAAE91FED39561855D75788F828C560C (void);
// 0x00000106 System.Void Baum2.List::set_UISelector(System.Func`2<System.Int32,System.String>)
extern void List_set_UISelector_m56A593C58E116A040D06561935AAEF7DB3DCCBF9 (void);
// 0x00000107 System.Action`2<System.String,Baum2.UIRoot> Baum2.List::get_UIFactory()
extern void List_get_UIFactory_m792392FB27E412992632870496B188071AF68EAF (void);
// 0x00000108 System.Void Baum2.List::set_UIFactory(System.Action`2<System.String,Baum2.UIRoot>)
extern void List_set_UIFactory_mB4BA69F9EAEE05465F0D6CFC18CF6BF2DADA14F5 (void);
// 0x00000109 System.Action`2<Baum2.UIRoot,System.Int32> Baum2.List::get_UIUpdater()
extern void List_get_UIUpdater_m6635D509318C7F732F264C58D61BC6E6005008E8 (void);
// 0x0000010A System.Void Baum2.List::set_UIUpdater(System.Action`2<Baum2.UIRoot,System.Int32>)
extern void List_set_UIUpdater_m2F08E2CD235FFB7D0C538A0827858204BA3295B7 (void);
// 0x0000010B System.Void Baum2.List::Awake()
extern void List_Awake_m250C2DA4281FDCDEA7DEC0EF45E11576BD010995 (void);
// 0x0000010C Baum2.UIRoot Baum2.List::CreateObject(System.String)
extern void List_CreateObject_mB55EA3FF335811DAA9EBAC00208ECF681B8DA9F1 (void);
// 0x0000010D System.Void Baum2.List::AddItem(System.Int32)
extern void List_AddItem_m467A2B01D4BB2E8A7CE7850C19FD9B45A7955E66 (void);
// 0x0000010E System.Void Baum2.List::ReturnObjectsToPool()
extern void List_ReturnObjectsToPool_m58B4033D2D01706F6EA91488F22281C044FA68AC (void);
// 0x0000010F System.Void Baum2.List::ToPool(System.Int32)
extern void List_ToPool_m0E84F8D19EB10F49A8566C8AE679DAD203E41047 (void);
// 0x00000110 Baum2.UIRoot Baum2.List::AddItemDirect(System.String)
extern void List_AddItemDirect_m230F23A663838236343A9D4FF28B7E1D4E7E35D9 (void);
// 0x00000111 System.Void Baum2.List::Resize(System.Int32)
extern void List_Resize_mF029C2EA20F067F238437C6B6687FA2FC5F4E9F8 (void);
// 0x00000112 System.Void Baum2.List::LateUpdate()
extern void List_LateUpdate_mC90F3EF1FC95EA8ADD5BE51B3F3A9CFFBC60416D (void);
// 0x00000113 System.Boolean Baum2.List::TryCreate(System.Int32)
extern void List_TryCreate_m82A135305FB715341EB125DA61C9FBDD17BEAFA7 (void);
// 0x00000114 System.Boolean Baum2.List::TryDelete(System.Int32)
extern void List_TryDelete_m92848A90DF1F5F0747D801F4CC8814DA39DDFC12 (void);
// 0x00000115 System.Void Baum2.List::UpdateItem(System.Int32)
extern void List_UpdateItem_m7102AF2D416E79BE68FAFFEAE7643B447B2022CA (void);
// 0x00000116 System.Void Baum2.List::UpdateAll()
extern void List_UpdateAll_m377FC0DFDD97D014F28317FD5E157FD690CF2453 (void);
// 0x00000117 System.Void Baum2.List::ResetScroll()
extern void List_ResetScroll_mB225DFB5360263D1C0D291BE8CE3C8EFE2E4DE41 (void);
// 0x00000118 System.Void Baum2.List::.ctor()
extern void List__ctor_m07D57134BAC36F9028575E2156DA643FAFECAD67 (void);
// 0x00000119 UnityEngine.Vector2 Baum2.ListLayoutGroup::get_MaxElementSize()
extern void ListLayoutGroup_get_MaxElementSize_m11A0ABC230136F8CDABECFFA50604A681E103188 (void);
// 0x0000011A System.Void Baum2.ListLayoutGroup::set_MaxElementSize(UnityEngine.Vector2)
extern void ListLayoutGroup_set_MaxElementSize_m27177BEA11BECEA1FCCE309C7A51B6016051EC7B (void);
// 0x0000011B System.Collections.Generic.List`1<UnityEngine.Vector2> Baum2.ListLayoutGroup::get_ElementPositions()
extern void ListLayoutGroup_get_ElementPositions_m98CAB463C22948EBC6A1AE82A5F72BECD399B537 (void);
// 0x0000011C System.Void Baum2.ListLayoutGroup::set_ElementPositions(System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void ListLayoutGroup_set_ElementPositions_mEC72450A41D7064023B0B9CE61A74BDAF33FDF15 (void);
// 0x0000011D System.Void Baum2.ListLayoutGroup::Initialize(Baum2.List)
extern void ListLayoutGroup_Initialize_m25E914121212C774B720E3443A49697F2A856EDF (void);
// 0x0000011E UnityEngine.RectTransform Baum2.ListLayoutGroup::get_RectTransform()
extern void ListLayoutGroup_get_RectTransform_mC5401FE6669A55260195A0BCBE2B9F472F88290A (void);
// 0x0000011F System.Void Baum2.ListLayoutGroup::RequestUpdate()
extern void ListLayoutGroup_RequestUpdate_mCE71521B218F5BFC351D574221F7C1F5EBF79781 (void);
// 0x00000120 System.Void Baum2.ListLayoutGroup::RecalcSize()
extern void ListLayoutGroup_RecalcSize_m9EB9773343788BD8A073B1D9E1402A83D95BA11C (void);
// 0x00000121 System.Void Baum2.ListLayoutGroup::RecalcSizeInternal(System.Int32,System.Int32,System.Single,System.Single)
extern void ListLayoutGroup_RecalcSizeInternal_mE5C08C8BC80034D9165F304B4ED90BEBE9A063DC (void);
// 0x00000122 System.Void Baum2.ListLayoutGroup::ResetScroll()
extern void ListLayoutGroup_ResetScroll_m9A050EC7ECA4B742120E1B204812012616A58AAB (void);
// 0x00000123 System.Void Baum2.ListLayoutGroup::.ctor()
extern void ListLayoutGroup__ctor_mDDD65CFEA363F608A6776669308113FB710E4F47 (void);
// 0x00000124 System.Collections.Generic.Dictionary`2<System.String,System.Object> Baum2.RawData::get_Info()
extern void RawData_get_Info_m06AD87522772AF915B4CA149161EC8DEB5C0D300 (void);
// 0x00000125 System.Void Baum2.RawData::set_Info(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void RawData_set_Info_mF9033334F504DD6BBC442785093714C729AF1C9E (void);
// 0x00000126 System.Void Baum2.RawData::.ctor()
extern void RawData__ctor_m8F827B84F022B5B8030A4D04A2252E812D8B0D93 (void);
// 0x00000127 Baum2.Cache Baum2.UIRoot::get_Raw()
extern void UIRoot_get_Raw_m769955BB65F2EB60B8A3CBC3D5E531BC62202B56 (void);
// 0x00000128 System.Void Baum2.UIRoot::Awake()
extern void UIRoot_Awake_m922C245AD5C851615679BA2B7C0E76C066543CBA (void);
// 0x00000129 UnityEngine.GameObject Baum2.UIRoot::Get()
extern void UIRoot_Get_m132DD43E88B4233D10D356155887A2EECF6AF8F2 (void);
// 0x0000012A UnityEngine.GameObject Baum2.UIRoot::Get(System.String,System.Boolean)
extern void UIRoot_Get_mE5B7329F0096C027D465CBA89068AF2FACDA0BD9 (void);
// 0x0000012B T Baum2.UIRoot::Get(System.String)
// 0x0000012C T Baum2.UIRoot::Get(System.String,System.Boolean)
// 0x0000012D T Baum2.UIRoot::GetComponent(UnityEngine.GameObject)
// 0x0000012E Baum2.UIRoot Baum2.UIRoot::CreateWithCache(UnityEngine.GameObject)
extern void UIRoot_CreateWithCache_m3D64CBC28ADEC12B0C30FB7EE74901496F2DA77B (void);
// 0x0000012F System.Void Baum2.UIRoot::SetupCache(UnityEngine.GameObject)
extern void UIRoot_SetupCache_m595F80BD2519D6FFD8DD48CAA5623619812C56E9 (void);
// 0x00000130 Baum2.UIRoot Baum2.UIRoot::UpdateCache(UnityEngine.GameObject)
extern void UIRoot_UpdateCache_m36D573DA85C6984ECB658689CE2A06D96D5F5EFE (void);
// 0x00000131 Baum2.IMapper Baum2.UIRoot::GetChildMap(System.String)
extern void UIRoot_GetChildMap_mBE0E0E6CA340B561EC918545D2069FC4EEB838A4 (void);
// 0x00000132 System.Void Baum2.UIRoot::RecalculateBounds()
extern void UIRoot_RecalculateBounds_m451F46825F38E6AF32AE637693F08AD6AEE56801 (void);
// 0x00000133 System.Void Baum2.UIRoot::RecalculateBounds(UnityEngine.Vector2)
extern void UIRoot_RecalculateBounds_m2942583D192B04B94FC78C02900CA1E9445EBB58 (void);
// 0x00000134 System.Void Baum2.UIRoot::.ctor()
extern void UIRoot__ctor_m718CEDC4FD727C4E3F4B339D6DA22FB4F3BBF789 (void);
// 0x00000135 T Baum2.UIRoot::Baum2.IMapper.GetComponent()
// 0x00000136 UnityEngine.GameObject Baum2.IMapper::Get()
// 0x00000137 UnityEngine.GameObject Baum2.IMapper::Get(System.String,System.Boolean)
// 0x00000138 T Baum2.IMapper::Get(System.String)
// 0x00000139 T Baum2.IMapper::Get(System.String,System.Boolean)
// 0x0000013A Baum2.IMapper Baum2.IMapper::GetChildMap(System.String)
// 0x0000013B T Baum2.IMapper::GetComponent()
// 0x0000013C System.Void Baum2.UIMapper::.ctor(Baum2.UIRoot,System.String)
extern void UIMapper__ctor_m8CA5B3BF1BD4D9680788A71DD4FC83B5142C859E (void);
// 0x0000013D UnityEngine.GameObject Baum2.UIMapper::Get()
extern void UIMapper_Get_m3696884903AA76C5D183E0C06B5AA8CBB207A4F4 (void);
// 0x0000013E UnityEngine.GameObject Baum2.UIMapper::Get(System.String,System.Boolean)
extern void UIMapper_Get_mA9C50896AE3FC8E31AC469AC6609F16FED5E086B (void);
// 0x0000013F T Baum2.UIMapper::Get(System.String)
// 0x00000140 T Baum2.UIMapper::Get(System.String,System.Boolean)
// 0x00000141 Baum2.IMapper Baum2.UIMapper::GetChildMap(System.String)
extern void UIMapper_GetChildMap_mD943C2DAF325CB80F3C68BE8E5A2F76FB1B296D3 (void);
// 0x00000142 T Baum2.UIMapper::GetComponent()
// 0x00000143 System.Void Baum2.Sample.Sample::Start()
extern void Sample_Start_mE5ABECA6AEC5884D0892751C53C2685D07463649 (void);
// 0x00000144 System.Void Baum2.Sample.Sample::Update()
extern void Sample_Update_m4F7D39AF038C42E67A77576D7481F9A3A4F453E2 (void);
// 0x00000145 System.Void Baum2.Sample.Sample::ImageSample()
extern void Sample_ImageSample_mFE6FE5E412339F203B798B7A7FE9ADCE38E82F82 (void);
// 0x00000146 System.Void Baum2.Sample.Sample::ButtonSample()
extern void Sample_ButtonSample_mE5E7AA267C3A468FC10A978DA99C30F08D743EB7 (void);
// 0x00000147 System.Void Baum2.Sample.Sample::ListSample()
extern void Sample_ListSample_mDF88333CB32E73FE0043CD4FD38F1CA9A601F331 (void);
// 0x00000148 System.Void Baum2.Sample.Sample::SliderSample()
extern void Sample_SliderSample_m32ADAF04829BA5558D961F06F97D53FBCBBBBC6A (void);
// 0x00000149 System.Void Baum2.Sample.Sample::.ctor()
extern void Sample__ctor_mF85224DFFC1622B62BFD64B790D69C94821B92A7 (void);
// 0x0000014A System.String Baum2.Sample.Sample::<ListSample>b__7_0(System.Int32)
extern void Sample_U3CListSampleU3Eb__7_0_mBA9622AF8D4108DE9252CD55098C83B18C09A953 (void);
// 0x0000014B System.Void Baum2.Sample.Sample::<ListSample>b__7_1(Baum2.UIRoot,System.Int32)
extern void Sample_U3CListSampleU3Eb__7_1_mB700F85AE910FCC3C3A45AAA9F9AC72B61F9895C (void);
// 0x0000014C System.Void UnityMessageManager/MessageDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageDelegate__ctor_mE80DF7F22AC095655F222BA7B808FB750C059479 (void);
// 0x0000014D System.Void UnityMessageManager/MessageDelegate::Invoke(System.String)
extern void MessageDelegate_Invoke_m71D9C54B8D33C236D4145F5C171F5057C0A08B09 (void);
// 0x0000014E System.IAsyncResult UnityMessageManager/MessageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MessageDelegate_BeginInvoke_mAEE45C362BD9197D646B59B1414ABE19C53BF4C1 (void);
// 0x0000014F System.Void UnityMessageManager/MessageDelegate::EndInvoke(System.IAsyncResult)
extern void MessageDelegate_EndInvoke_m57B0BBB4D9EBF8DC1F6A7D8A6C449B7C1C75796F (void);
// 0x00000150 System.Void UnityMessageManager/MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageHandlerDelegate__ctor_mB09E60C8134DF32641562ED528E1E0E35B40184A (void);
// 0x00000151 System.Void UnityMessageManager/MessageHandlerDelegate::Invoke(MessageHandler)
extern void MessageHandlerDelegate_Invoke_m82C612B57B22FCF40994CAA70914DD9701E5CCCF (void);
// 0x00000152 System.IAsyncResult UnityMessageManager/MessageHandlerDelegate::BeginInvoke(MessageHandler,System.AsyncCallback,System.Object)
extern void MessageHandlerDelegate_BeginInvoke_m3E0807D2B4E1CB959FEEA14DFB15AD08299EE994 (void);
// 0x00000153 System.Void UnityMessageManager/MessageHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void MessageHandlerDelegate_EndInvoke_mE15D4204B18F5409F5B926454790F915652ED3B5 (void);
// 0x00000154 System.Void SROptions/DisplayNameAttribute::.ctor(System.String)
extern void DisplayNameAttribute__ctor_m8B7D935FAA9CF5CD055BDEA7856CEDECA0B00A3B (void);
// 0x00000155 System.Void SROptions/IncrementAttribute::.ctor(System.Double)
extern void IncrementAttribute__ctor_m0D82A5ED7B83C2C3E93B8F5469A4C1E0302B4D14 (void);
// 0x00000156 System.Void SROptions/NumberRangeAttribute::.ctor(System.Double,System.Double)
extern void NumberRangeAttribute__ctor_m0EC713F19D0C2DAD6320130466ECB59624102CE8 (void);
// 0x00000157 System.Void SROptions/SortAttribute::.ctor(System.Int32)
extern void SortAttribute__ctor_m2437D5A00ECCCFA06742718C5E6E153D1672339A (void);
// 0x00000158 System.Void CanvasScreenShot/takePictureHandler::.ctor(System.Object,System.IntPtr)
extern void takePictureHandler__ctor_mA5DE095B02C3C994487382748B6E8BB1B580B675 (void);
// 0x00000159 System.Void CanvasScreenShot/takePictureHandler::Invoke(System.Byte[])
extern void takePictureHandler_Invoke_m92267B6E686F08546F74EF54E450F0E724F5FD35 (void);
// 0x0000015A System.IAsyncResult CanvasScreenShot/takePictureHandler::BeginInvoke(System.Byte[],System.AsyncCallback,System.Object)
extern void takePictureHandler_BeginInvoke_m4995F7E6B499267082E7F990FB9DF1F06BFCA87A (void);
// 0x0000015B System.Void CanvasScreenShot/takePictureHandler::EndInvoke(System.IAsyncResult)
extern void takePictureHandler_EndInvoke_m1A6265C03A03F3536E7FF12CEAA7B7037EE38C71 (void);
// 0x0000015C System.Void CanvasScreenShot/<_takeScreenShot>d__9::.ctor(System.Int32)
extern void U3C_takeScreenShotU3Ed__9__ctor_m98D21DEB4F959143BE9A697117F9093CD3E66790 (void);
// 0x0000015D System.Void CanvasScreenShot/<_takeScreenShot>d__9::System.IDisposable.Dispose()
extern void U3C_takeScreenShotU3Ed__9_System_IDisposable_Dispose_mBAD312ADCFBE0F7FB1C17BD8523AA6D55D518FDF (void);
// 0x0000015E System.Boolean CanvasScreenShot/<_takeScreenShot>d__9::MoveNext()
extern void U3C_takeScreenShotU3Ed__9_MoveNext_m68574E71E009253B4E58A500575E3CBF10B0F319 (void);
// 0x0000015F System.Object CanvasScreenShot/<_takeScreenShot>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_takeScreenShotU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D1AA346300183E5496293449F8F1478CC772840 (void);
// 0x00000160 System.Void CanvasScreenShot/<_takeScreenShot>d__9::System.Collections.IEnumerator.Reset()
extern void U3C_takeScreenShotU3Ed__9_System_Collections_IEnumerator_Reset_mAA1066CF404ABF4C4DFF872B95BA570AD3C56E1E (void);
// 0x00000161 System.Object CanvasScreenShot/<_takeScreenShot>d__9::System.Collections.IEnumerator.get_Current()
extern void U3C_takeScreenShotU3Ed__9_System_Collections_IEnumerator_get_Current_m6F7ACEF03C951FD316561FBA3E939E10A90B3A62 (void);
// 0x00000162 System.Void EditContainer/<LoadAssetBundle>d__5::.ctor(System.Int32)
extern void U3CLoadAssetBundleU3Ed__5__ctor_m0D24CC471DFE4867573F1CB3CDCB9C33B53E92BB (void);
// 0x00000163 System.Void EditContainer/<LoadAssetBundle>d__5::System.IDisposable.Dispose()
extern void U3CLoadAssetBundleU3Ed__5_System_IDisposable_Dispose_m73DEA100541AB3B3592D440EA2E3675F63613B42 (void);
// 0x00000164 System.Boolean EditContainer/<LoadAssetBundle>d__5::MoveNext()
extern void U3CLoadAssetBundleU3Ed__5_MoveNext_m5019B4D24927E1AA7F9A471B83B867F24AB62317 (void);
// 0x00000165 System.Object EditContainer/<LoadAssetBundle>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadAssetBundleU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86666B1EBB5C37F1B9321B0E2A8F6DFF8278C26E (void);
// 0x00000166 System.Void EditContainer/<LoadAssetBundle>d__5::System.Collections.IEnumerator.Reset()
extern void U3CLoadAssetBundleU3Ed__5_System_Collections_IEnumerator_Reset_mBB48409ABA76CD84FBAB33E3C8ABCBB180DAB092 (void);
// 0x00000167 System.Object EditContainer/<LoadAssetBundle>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CLoadAssetBundleU3Ed__5_System_Collections_IEnumerator_get_Current_m43078AE5648D8BB63F31A6BEE4A8DD7A98E05770 (void);
// 0x00000168 System.Void EditContainer/<>c::.cctor()
extern void U3CU3Ec__cctor_mD71D5D2761802C95344CE87F7DDE538B5498E58C (void);
// 0x00000169 System.Void EditContainer/<>c::.ctor()
extern void U3CU3Ec__ctor_mAC773D6C219250B8805A746495419F4E25B2CE85 (void);
// 0x0000016A System.Void EditContainer/<>c::<takeScreenShot>b__9_0(System.Boolean,System.String)
extern void U3CU3Ec_U3CtakeScreenShotU3Eb__9_0_m6BDA3DB4D420243F4EFD83E9DDA72E758EEB44C3 (void);
// 0x0000016B System.Void EditContainer/<takeScreenShot>d__9::.ctor(System.Int32)
extern void U3CtakeScreenShotU3Ed__9__ctor_mF73AFE0F6E4C864084E40A85C1BFF86855506C26 (void);
// 0x0000016C System.Void EditContainer/<takeScreenShot>d__9::System.IDisposable.Dispose()
extern void U3CtakeScreenShotU3Ed__9_System_IDisposable_Dispose_m524DD52B46B6F2EF8EB74BD59173EC7ED032C47C (void);
// 0x0000016D System.Boolean EditContainer/<takeScreenShot>d__9::MoveNext()
extern void U3CtakeScreenShotU3Ed__9_MoveNext_mFF8B8F0BEC5817E3EC414E4A016623E713ADD22E (void);
// 0x0000016E System.Object EditContainer/<takeScreenShot>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtakeScreenShotU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B2A47712745A9C862F2E23477B6170650D0941D (void);
// 0x0000016F System.Void EditContainer/<takeScreenShot>d__9::System.Collections.IEnumerator.Reset()
extern void U3CtakeScreenShotU3Ed__9_System_Collections_IEnumerator_Reset_mEEF116F652FFE4685D18DFEB0A6942BF12949910 (void);
// 0x00000170 System.Object EditContainer/<takeScreenShot>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CtakeScreenShotU3Ed__9_System_Collections_IEnumerator_get_Current_m02385082BF51D25EA85EB28F7D79FDB947EAC3E5 (void);
// 0x00000171 System.Void ImageProp/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m8324C29611962A04ABD2041EAF2FD37F228F734B (void);
// 0x00000172 System.Void ImageProp/<>c__DisplayClass11_0::<PickImage>b__0(System.String)
extern void U3CU3Ec__DisplayClass11_0_U3CPickImageU3Eb__0_mF939C9A7916DCA8FBC9B6662F0C3DF34938792A1 (void);
// 0x00000173 System.Void ImageProp/<RemoveBG>d__12::.ctor(System.Int32)
extern void U3CRemoveBGU3Ed__12__ctor_m9E0FCE814FC89091A4906D3ACF14F2D7FC1AFC42 (void);
// 0x00000174 System.Void ImageProp/<RemoveBG>d__12::System.IDisposable.Dispose()
extern void U3CRemoveBGU3Ed__12_System_IDisposable_Dispose_m57B090527030200EEEEC9F40BAF3D1E1AE315802 (void);
// 0x00000175 System.Boolean ImageProp/<RemoveBG>d__12::MoveNext()
extern void U3CRemoveBGU3Ed__12_MoveNext_m7EBF1C1AF017BCF2D457A02A0F8767A7BBF1C9B8 (void);
// 0x00000176 System.Object ImageProp/<RemoveBG>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRemoveBGU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1D54C2FAD6B50BDC54F824D5402EB602C744F5B (void);
// 0x00000177 System.Void ImageProp/<RemoveBG>d__12::System.Collections.IEnumerator.Reset()
extern void U3CRemoveBGU3Ed__12_System_Collections_IEnumerator_Reset_mD64314FAFBB54478C70726DC227C0B6AA73B4C19 (void);
// 0x00000178 System.Object ImageProp/<RemoveBG>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CRemoveBGU3Ed__12_System_Collections_IEnumerator_get_Current_mAC4115AA52010DE3B305FA93B87618D8B5B312F9 (void);
// 0x00000179 System.Void Ultils/<LoadImageCor>d__1::.ctor(System.Int32)
extern void U3CLoadImageCorU3Ed__1__ctor_m2F971244D828669C77EEDD65BC2F78D9351C7D8F (void);
// 0x0000017A System.Void Ultils/<LoadImageCor>d__1::System.IDisposable.Dispose()
extern void U3CLoadImageCorU3Ed__1_System_IDisposable_Dispose_mA6C64F49DF03E21CE3773A208CA61EAD95C09C9A (void);
// 0x0000017B System.Boolean Ultils/<LoadImageCor>d__1::MoveNext()
extern void U3CLoadImageCorU3Ed__1_MoveNext_m0C67FA70DC6EBA8159BAC5D4DAA333E36C123792 (void);
// 0x0000017C System.Object Ultils/<LoadImageCor>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadImageCorU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B260ECCECA9D874F0725DB3C8BBAF2CA1667ECA (void);
// 0x0000017D System.Void Ultils/<LoadImageCor>d__1::System.Collections.IEnumerator.Reset()
extern void U3CLoadImageCorU3Ed__1_System_Collections_IEnumerator_Reset_m95B6B5ECE3F5B580ACEA80F7C64875DBA3B6E631 (void);
// 0x0000017E System.Object Ultils/<LoadImageCor>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CLoadImageCorU3Ed__1_System_Collections_IEnumerator_get_Current_m2633AE0C1483B827C2E0D12A76A2166D76C28456 (void);
// 0x0000017F System.Single PluginMaster.PsLayer/ImageComponentProxy::get_alpha()
// 0x00000180 System.Void PluginMaster.PsLayer/ImageComponentProxy::set_alpha(System.Single)
// 0x00000181 System.Boolean PluginMaster.PsLayer/ImageComponentProxy::get_enabled()
// 0x00000182 System.Void PluginMaster.PsLayer/ImageComponentProxy::set_enabled(System.Boolean)
// 0x00000183 UnityEngine.Material PluginMaster.PsLayer/ImageComponentProxy::get_material()
// 0x00000184 System.Void PluginMaster.PsLayer/ImageComponentProxy::set_material(UnityEngine.Material)
// 0x00000185 System.Void PluginMaster.PsLayer/ImageComponentProxy::.ctor()
extern void ImageComponentProxy__ctor_mE0EFF813E1705FF8976994E7E3BB03960206F402 (void);
// 0x00000186 System.Void PluginMaster.PsLayerImage/ImageProxy::.ctor(UnityEngine.UI.Image)
extern void ImageProxy__ctor_mDDA91D15659DFCA39D602F84E133135CC59E19B6 (void);
// 0x00000187 System.Single PluginMaster.PsLayerImage/ImageProxy::get_alpha()
extern void ImageProxy_get_alpha_mB427DB92FCAC59FCD2576C6527776451BA206628 (void);
// 0x00000188 System.Void PluginMaster.PsLayerImage/ImageProxy::set_alpha(System.Single)
extern void ImageProxy_set_alpha_m2D98976F528957682EB3C696F0C83BEF88996D8E (void);
// 0x00000189 System.Boolean PluginMaster.PsLayerImage/ImageProxy::get_enabled()
extern void ImageProxy_get_enabled_m6B091B10C39C2515768FFDB998AD5D70C3E6E290 (void);
// 0x0000018A System.Void PluginMaster.PsLayerImage/ImageProxy::set_enabled(System.Boolean)
extern void ImageProxy_set_enabled_m90FD1963E5589ED7A0DF77711F38D197753DA32C (void);
// 0x0000018B UnityEngine.Material PluginMaster.PsLayerImage/ImageProxy::get_material()
extern void ImageProxy_get_material_m04BE413F0A403992B9D42ECACBEF98FD9BBA4E25 (void);
// 0x0000018C System.Void PluginMaster.PsLayerImage/ImageProxy::set_material(UnityEngine.Material)
extern void ImageProxy_set_material_m8453EE724EC400A1E9A90E1444B6B18275E1A10E (void);
// 0x0000018D System.Void PluginMaster.PsLayerSprite/RendererProxy::.ctor(UnityEngine.SpriteRenderer)
extern void RendererProxy__ctor_m9EBE603500C2A3D2BEA904797F51838DEC3666E4 (void);
// 0x0000018E System.Single PluginMaster.PsLayerSprite/RendererProxy::get_alpha()
extern void RendererProxy_get_alpha_m0316847BEB0F3A379270DDC7BFDDFE5E1446F0CF (void);
// 0x0000018F System.Void PluginMaster.PsLayerSprite/RendererProxy::set_alpha(System.Single)
extern void RendererProxy_set_alpha_m3D915446A975032481CBD79511EC8AE5F05A57C0 (void);
// 0x00000190 System.Boolean PluginMaster.PsLayerSprite/RendererProxy::get_enabled()
extern void RendererProxy_get_enabled_m809537116A88CF180CDA566FDFE76A4F756C9900 (void);
// 0x00000191 System.Void PluginMaster.PsLayerSprite/RendererProxy::set_enabled(System.Boolean)
extern void RendererProxy_set_enabled_m4679149C5EA2086AD8BE790F2E88D7CFCD1D7519 (void);
// 0x00000192 UnityEngine.Material PluginMaster.PsLayerSprite/RendererProxy::get_material()
extern void RendererProxy_get_material_m25F8F42B1F7ACB9B2A69F958832C818E7D247743 (void);
// 0x00000193 System.Void PluginMaster.PsLayerSprite/RendererProxy::set_material(UnityEngine.Material)
extern void RendererProxy_set_material_m9E5074FA34E3CB54FB15579E19F0222209F29602 (void);
// 0x00000194 System.Void Baum2.Cache/CachedGameObject::.ctor(System.String,UnityEngine.GameObject,System.Collections.Generic.List`1<System.String>)
extern void CachedGameObject__ctor_mAEFB3317F18B00B6C71758E13061F8C8479DAA56 (void);
// 0x00000195 System.Void Baum2.Cache/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mBCE7287FD829A21922D73413B53626F24586B9F0 (void);
// 0x00000196 System.Boolean Baum2.Cache/<>c__DisplayClass5_0::<Get>b__0(Baum2.Cache/CachedGameObject)
extern void U3CU3Ec__DisplayClass5_0_U3CGetU3Eb__0_mCE09DB056FD41A55B0F12465974AFC891C99F5FC (void);
// 0x00000197 System.Void Baum2.List/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_mD123A7D22431A863F1D3DFC208EF56E8973FA334 (void);
// 0x00000198 System.Boolean Baum2.List/<>c__DisplayClass37_0::<CreateObject>b__0(UnityEngine.GameObject)
extern void U3CU3Ec__DisplayClass37_0_U3CCreateObjectU3Eb__0_mE1C6C9EFF846CD547F2508D34A5FF628918E943E (void);
// 0x00000199 System.Void Baum2.ListLayoutGroup/<>c::.cctor()
extern void U3CU3Ec__cctor_m520D883DC6AF217CC737109952DF94F37CFD6E7C (void);
// 0x0000019A System.Void Baum2.ListLayoutGroup/<>c::.ctor()
extern void U3CU3Ec__ctor_m2F0FBDE8EC8B45AE12108091694FC1C7E5E9C34E (void);
// 0x0000019B System.Single Baum2.ListLayoutGroup/<>c::<Initialize>b__12_0(UnityEngine.Vector2)
extern void U3CU3Ec_U3CInitializeU3Eb__12_0_m6B0B922A405B96E24C143A1BB402B292D0427F66 (void);
// 0x0000019C System.Single Baum2.ListLayoutGroup/<>c::<Initialize>b__12_1(UnityEngine.Vector2)
extern void U3CU3Ec_U3CInitializeU3Eb__12_1_m1A8A4D8935239BAC8DED151C9DBBE8F2151E0EE4 (void);
// 0x0000019D System.Void Baum2.Sample.Sample/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m868D238F24E32046527AC32B0DD0CA21756DDAED (void);
// 0x0000019E System.Void Baum2.Sample.Sample/<>c__DisplayClass6_0::<ButtonSample>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CButtonSampleU3Eb__0_m7334858BF883529DD82BE6F54884EEF62ADF136A (void);
// 0x0000019F System.Void Baum2.Sample.Sample/<>c__DisplayClass6_0::<ButtonSample>b__1()
extern void U3CU3Ec__DisplayClass6_0_U3CButtonSampleU3Eb__1_m9D33048006AFAE0D55E01935ED8E5A3B91D524E7 (void);
// 0x000001A0 System.Void Baum2.Sample.Sample/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m56E85AF94C3C4B7F88D15A94A87E6067E9F6C665 (void);
// 0x000001A1 System.Void Baum2.Sample.Sample/<>c__DisplayClass7_0::<ListSample>b__2()
extern void U3CU3Ec__DisplayClass7_0_U3CListSampleU3Eb__2_m742E5DA0E742933E1DBA363C9BC4936F1FD7CC17 (void);
static Il2CppMethodPointer s_methodPointers[417] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NativeAPI_OnUnityMessage_m37E97E313FD4FA3B821FC5CAB30F2CF89E42FE11,
	NativeAPI_OnUnitySceneLoaded_mACC814F3778DC1C7D58F7D5E544AC7041830C7F1,
	NativeAPI_OnSceneLoaded_m648E2B7082BA98A576B212037EF0C65DD7E4D81C,
	NativeAPI_SendMessageToFlutter_m64F5E97AD13823EBE038477A3904F5D36A4DD7F7,
	NativeAPI_ShowHostMainWindow_m5B4408679381319B7A871C0DEE7B3635DFCDA1C7,
	NativeAPI_UnloadMainWindow_m550CFAC39B2D199C11186B9A7A36D25307EE241E,
	NativeAPI_QuitUnityWindow_mC7084563F0D750771CBADB5A68B4E46F3B1C8160,
	NativeAPI__ctor_m6AF8C586F2C4674EE98EA0EEC7E4DE11D26FD038,
	NULL,
	NULL,
	NULL,
	NULL,
	MessageHandler_Deserialize_mE6CDB21EAEA2491B135D4DDC69091312F68ED426,
	NULL,
	MessageHandler__ctor_mEF0C5F957F8FAC2E9EA73C4749BFAF9B727547FF,
	MessageHandler_send_m6E4C9DE3BD5C18B8DD958734711E4FA0803B895C,
	UnityMessage__ctor_m418530AF3414A53A37F1433AEE0C8A83206D44A6,
	UnityMessageManager_generateId_m105F25DED7848A21DD040716977EF8B8785C3846,
	UnityMessageManager_add_OnMessage_m2725B086DA28C9E73814BE3D96E21BC3F3E3BAA4,
	UnityMessageManager_remove_OnMessage_m67B6DA617E39FEA4523E49A8D40590E6F6F19B07,
	UnityMessageManager_add_OnFlutterMessage_m7FD992E772A26B34DD346B04C3D68A0B1F92805A,
	UnityMessageManager_remove_OnFlutterMessage_m623F9C78BDD8E605C5EEE5AC1400C4A2F0FFF892,
	UnityMessageManager_Start_m8BEE1ACBB306B976AB4A5BEC9AF6F376AA4D2137,
	UnityMessageManager_OnSceneLoaded_mF79096186CFB8E736D4D3404176A3294583DB278,
	UnityMessageManager_ShowHostMainWindow_m2C68A5D1CE022ECCE2680E4A4E7583C15F0FA311,
	UnityMessageManager_UnloadMainWindow_m0EAB6ABA36AE636F13B9CC0D873093EBA084885E,
	UnityMessageManager_QuitUnityWindow_m617D71DD8130842B8E365A8EF204F050246A4CBC,
	UnityMessageManager_SendMessageToFlutter_mC8D4CB3F70B56F758CC34B3246EE018AF6E4805A,
	UnityMessageManager_SendMessageToFlutter_m32B9FFB546C6D09C86CF79E046F7D00BB5AECD6E,
	UnityMessageManager_onMessage_m6E60A5C3A00B83B070276B32B727847F9CEEF413,
	UnityMessageManager_onFlutterMessage_m7DF88A89869A1FF528735694C4855BD8C0DD74F0,
	UnityMessageManager__ctor_mE89F6AEB4A75A1E52A8DF59747ACC13D3858C7C2,
	UnityMessageManager__cctor_mA16D91AB3FC363648DDA304555ECCFFF699591E5,
	SROptions_get_Current_m82BF8479C330727BB3C7012FFA818837D9B8A856,
	SROptions_OnStartup_m9E3C90F00C547C697A3D9FAF50FEF5D54CDBCDC0,
	SROptions_add_PropertyChanged_mA0871B8690A19A5500B96D4A3AEC937C40B9C1C6,
	SROptions_remove_PropertyChanged_mD4EE8133CEC4A2CA9BACBCBFB13A32C1CEBE367F,
	SROptions_OnPropertyChanged_m6672EED85D4ACBE958B96E759C0BA39290CB90D1,
	SROptions_add_InterfacePropertyChangedEventHandler_mD4C6B3CE455368C51F68C2AAE3FF2B8592034B24,
	SROptions_remove_InterfacePropertyChangedEventHandler_m473DA4B90052E59207C19EB020DACB08A5453225,
	SROptions_System_ComponentModel_INotifyPropertyChanged_add_PropertyChanged_m74570EAFAFB8FC9CCF87D5CBD7CFDDA5DBBA7C29,
	SROptions_System_ComponentModel_INotifyPropertyChanged_remove_PropertyChanged_m482FF52259193EF1549073FCA814052FBA6C0C03,
	SROptions__ctor_m2BCAE5898CD8DFF144134B689C779BD25CD9651E,
	SROptions__cctor_m3E2D64545DADBAFC5987FD427D4F96B03270F8E4,
	SROptionsPropertyChanged__ctor_m61B22632CE2B5113411E2A6EDD21298BDC460684,
	SROptionsPropertyChanged_Invoke_m9E9CC44C4B9BA72583F4A154C180CED87D78BA60,
	SROptionsPropertyChanged_BeginInvoke_m3D3F30B086D91AC6C6C81BAF7E003E57F5F01B06,
	SROptionsPropertyChanged_EndInvoke_mA032503CA4E5AA7C2BF45C08EAE80451817D92D5,
	CanvasScreenShot_add_OnPictureTaken_m4D26FFA7CE4F1D28A3B51AED5E902789C377AB91,
	CanvasScreenShot_remove_OnPictureTaken_mB6683D6A5BDAB7B3C71F4725259FFEFF1D794393,
	CanvasScreenShot_takeScreenShot_m7BBB478C6AC0A6ED6941A53520C83F86B04DF569,
	CanvasScreenShot__takeScreenShot_m1355462C6755EC5299F9F5B708CEC1521FE348D2,
	CanvasScreenShot_duplicateUI_mD7D6CB0A15A6D1871FD48B7D2422C38699A26ECF,
	CanvasScreenShot_getAllImagesFromCanvas_mCBE2EAA165FAA3EBDFB49151180C0F505367289F,
	CanvasScreenShot_getAllTextsFromCanvas_m41B5B9914C519ECDC5855BE4133EDF91FC6F87A4,
	CanvasScreenShot_getAllCanvasFromCanvas_m26BD602102343DEF4D0F7E1CAEDE0CACBA7FB8BA,
	CanvasScreenShot_getAllCanvasInScene_mF1AFC51F811A428ECDFB9EEE3A21AF5E26B96776,
	CanvasScreenShot_showImages_m3ACEACB828C6DC2BC2730E3C61DFCA4231BA7476,
	CanvasScreenShot_showTexts_m23A72E1DDE2D6CFEB46352E8AD5B5C1F33D28014,
	CanvasScreenShot_showCanvas_m6C850980A8CF85ED62F97F358E252D2C20D0094A,
	CanvasScreenShot_showCanvas_mF450C3EE37CAC7D2131B50BE8000C9EABC2C27DD,
	CanvasScreenShot_showCanvasExcept_m7A6B757D4E13FF6BBF06C7957BF9913CA38843FF,
	CanvasScreenShot_showCanvasExcept_m0AECD0FF313C38257D7C84DC14BFEE8909B0CBE8,
	CanvasScreenShot_resetPosAndRot_mF00E4D195379135E43FA325817561AB63736605C,
	CanvasScreenShot__ctor_mEBD4D4C3600BF6A34A40C4363816DD514CF53A6A,
	ColorAdjust_Awake_m900CAA678518F28F7EAA2DA0E8D05D6CF9606016,
	ColorAdjust_Start_m40BF2F361BB0B109D3F0609E37BBBC4963AE05B1,
	ColorAdjust_CallUpdate_m7E6FF358335770ED3EDC20FFFD46C27F0FCA14CC,
	ColorAdjust_Update_m1E54EF5BE42A19C79314E2DBD499CB2EE56593A1,
	ColorAdjust_XUpdate_m24794D1D7152BC71DEE05A4538E7AD5EB732C3F1,
	ColorAdjust_OnDestroy_m948B6A87FAC69D64558EAC76818F2188A8EF013E,
	ColorAdjust_OnDisable_m322372F24255D0DE09E94B73BE596E0E4096CDD3,
	ColorAdjust_OnEnable_m0A815201281A85B62AB0902834E49FC454651C75,
	ColorAdjust__ctor_mCAB92FC2E2B1FB2CCBEB9B641B9B71258BEFAABA,
	NULL,
	NULL,
	CommandHandler_Start_mE70AE4FFCF2EDF710818EFA307EC7848555394BB,
	CommandHandler_UpdateUI_mD8B2DC1480DBE38839DF45AEA0BB4E1F2F7A28DB,
	CommandHandler_AddCommand_m8EC29B56202C4CC55A7BCD191C338992C4C754C6,
	CommandHandler_UndoCommand_m7D84861A533248CDC7AF0576552DF23C890BA4C5,
	CommandHandler_RedoCommand_m6174834550422EA1A42AEEFBB725A40AEE498E16,
	CommandHandler__ctor_m6A9FEAE4B964ED8C24CEC96110FC71EEF21610EF,
	MoveCommand__ctor_m21E8C0DD5B2F1EB200908F88C427D646E26F6952,
	MoveCommand_Execute_mA8B048E56C8109811B9EC54DD403045D9F89E009,
	MoveCommand_Undo_m7A5459289621642AA95D91722E2E80991E5DC181,
	CustomImage_Start_m19B55FF89F25731A2C73D4C68FF514A0D1489C9B,
	CustomImage_IsRaycastLocationValid_m1D6119B97791515A1EDE1FE9354A4DDAC6F6E277,
	CustomImage__ctor_mA59B58EA7B0B73A5B9735A5211106A3FB217A22A,
	EditContainer_Start_m9A80A0E61100FAE6CDF838C81AB8086A258C2672,
	EditContainer_LoadAssetBundle_m755F22CC949A46F08A32D6A9ED2E764286E9549D,
	EditContainer_LateUpdate_mAD5EF4CBFB94488F6661CD6882C9F60356284FB4,
	EditContainer_Export_m5D2CEB6EC87E6432BA6204CA688E13DCC4C3CE47,
	EditContainer_takeScreenShot_m788AC417CB80F52FB72B12F9700D74951FDF4478,
	EditContainer__ctor_mC05773A42475D685DAD6747BE7D96E5E1910B4D1,
	EditTransform_get_SelectedRect_m7F119647DC0C32C1B348489E0DCE35EB340224A4,
	EditTransform_set_SelectedRect_m6E3620F94221577FA410F706FD3209D24F0C3D8B,
	EditTransform_Awake_mB94268E99909F87AFF5DCD61EC7E298AB5A62779,
	EditTransform_OnBegin_m867EF3A0944650E27B065A7FA69AF0E58E4BFEC0,
	EditTransform_OnEnd_m3C5D552B727BFC8AABD0D42C197AB5C552FCDBA5,
	EditTransform_LateUpdate_m156B8BBFC659D75A6E51D2975C50E2E918CEC396,
	EditTransform_UpdateRect_mCD2070CFBDE055E75A48606DC4CD09CD141A5F2B,
	EditTransform_WorldToViewportPoint_mF7593C1FD5F0D4196FDD0BA5D09E4F681D3E6E05,
	EditTransform__ctor_mE7069CC18464C1C8A599AAF15C668542B7E0A2A3,
	ImageProp_Awake_m41D9581CB5B8C307ADE0894363414AEEA57DA79E,
	ImageProp_OnDestroy_mC451746447E3914277053DE45111C2F12C9A101B,
	ImageProp_OnOpen_m5062DA60E9BF388C4CEF74E11006A8C1E815400C,
	ImageProp_PickImage_mBA702034A1664B1A1F6CA31A464AC2341C143A7B,
	ImageProp_RemoveBG_m998A6BDB0F23674C4F3EF98DD6C2A6643315719C,
	ImageProp_LoadByteToTexture_m09822E22A71C80CB80331DB99C322864AB0C6FD4,
	ImageProp_OnSatChange_m1928C4149693886EC9FBB695B03AE2F03B57B558,
	ImageProp_OnConChange_m624719B705B55F6C85FCF830192B35AB428A21C7,
	ImageProp_OnBriChange_m58CA693C3ACF41D8410ED0F1C99F428D15E3F873,
	ImageProp_OnHueChange_m67FD4734A3E5703E86165400216912E74042C793,
	ImageProp_OnAlphaChange_mA4B1CF679035A2E2D318472A13DCA1891265A759,
	ImageProp_Flip_m27AD08E43202B887DB2F64471CF78DA50B546691,
	ImageProp_UpdateLayer_mF4AD7441D3641418B9E4F4562E968C591F97C582,
	ImageProp__ctor_m4C9D4ED85E662EBE32A0A30026B3A317E373D267,
	TextProp_OnOpen_mA1A67C59AB4AFD33BDA9A45120B6A02D9028400B,
	TextProp_UpdateText_mEA57F0159B1185C92DFAE1D22E576F966095DAAF,
	TextProp_UpdateAlign_mEEB8FDCF99FC587A27275C5D842093C4FB6BD1A6,
	TextProp_UpdateLayer_mA58DB76FB822EA8AA0C4BB02DFA6422FAFC43A5A,
	TextProp__ctor_m937A709911F80676C443960DF91C8A5F84B145DF,
	UIFlippable_get_horizontal_mB630C34B97FE69F9250008F231575E05D715B0EC,
	UIFlippable_set_horizontal_m4AF04B475EFEECE0FFA7D1AD3E44707E780B6D5D,
	UIFlippable_get_vertical_m2E216762ED5618E7D8A67C698FEF41742C81A1A1,
	UIFlippable_set_vertical_mC076407E6E44312F226723A90C71AAEB4E631C1F,
	UIFlippable_ModifyMesh_m9B89FC767EDFD495D71845846CEC1CDE24942D37,
	UIFlippable__ctor_m3593D59D6C0E7F258B5C50C48663358FE5D3B07B,
	Ultils_LoadImage_mDB3845CC81EDC0B64F3C6FA391527FC6600017CC,
	Ultils_LoadImageCor_m3FFBC2521C421430947C01A5A985FE64A18EBA97,
	Ultils_SetSizeAndPos_m4693FBFD659F774CDF9F5E9AD784F5FC36F7EF7A,
	Ultils_ToSizePos_m055128823A20965D47F34DFABB27170B3D843054,
	MultiTouchScrollRect_get_MultiTouchPosition_mA132F72E3FE982FDD30944012EA73A0CB471C405,
	MultiTouchScrollRect_Update_m01FB760AD4E0F3B0E047239AAA9E1AAA654AC279,
	MultiTouchScrollRect_OnBeginDrag_m4B4605E853E512E73B607610D6247A86C060FF7E,
	MultiTouchScrollRect_OnDrag_m7D94703FFC72B99ECE8777809A1BB674E1E44F12,
	MultiTouchScrollRect_OnEndDrag_mC1CCC5BB7BBAE5E9C83B5B3FC331524CD1F6AE84,
	MultiTouchScrollRect__ctor_mC7985D545CE4DF81C89975ED1A3237EA5105CB98,
	SimpleZoom_get_Content_mBCD8F5A2DF4A457C1EB4E54F8AA65DFF0793AF00,
	SimpleZoom_get_Viewport_m8505EA0E6A77196CF601FB174EF21E6CB95D5B6A,
	SimpleZoom_get_TargetZoom_mF589E7939FA9147B5E9E5C5E1AC59F646F36C33D,
	SimpleZoom_set_TargetZoom_m532721634919DE5497E9F80448E525FC988EDF34,
	SimpleZoom_get_CurrentZoom_mC8263F84762B1CD1FE4D4135D540A8852C47B6C9,
	SimpleZoom_get_ZoomProgress_m3517281F97F2193634C47DE94160C5B28AEC0EE6,
	SimpleZoom_get_ZoomMargin_m2F30C2B39B85A81CB4379AA869C06BAF5FC4732D,
	SimpleZoom_get_UsingUnityRemote_m49899D5B82010A5D10F67294265D55DC6ACBAF8A,
	SimpleZoom_Start_m143BC264599FA2B77261A1F94BEE54B50EE09E12,
	SimpleZoom_Update_m1847F85AA1F11073F947FF6D9FFA1E49CC4E406D,
	SimpleZoom_OnPointerClick_m3DD5DF50ED65FF46DC28E522946C24EA99356444,
	SimpleZoom_Initialize_m4B27BA6FC6D3982DB407B5FD333983E706100E58,
	SimpleZoom_Validate_mCFDDA0D11E8AD744F7F0685BCD9FAE14C6B10599,
	SimpleZoom_Setup_m29C47BC2F9C476E73AEC83882FC1BCBA329C3C49,
	SimpleZoom_OnPivotZoomUpdate_m830BDF743A688F5AA451571577567CA2C70A9404,
	SimpleZoom_OnHandheldUpdate_m3E34167D3A83B1FBBEA0243FF0DC6A1942250C6E,
	SimpleZoom_OnDesktopUpdate_mBDA10370DEF1D793ECD22A4235D76CA2FBBBE4AF,
	SimpleZoom_OnDoubleTap_mA660D78FA72C8B8D377DF110C8B37C9C184D3FC1,
	SimpleZoom_OnZoomSlider_m12D22BBA3916A1E76AF9BDEC0A80D163D40DF9C7,
	SimpleZoom_OnZoomView_mB177A85BD77EA8BF34FFCF5534DC305A5C87E673,
	SimpleZoom_ElasticClamp_mE9BAAA6946FCA4F69E0C81A594507A83C3A4C1C4,
	SimpleZoom_SetZoom_m42588E17A00D16BA32F336D50D2C002CBA1C0A1E,
	SimpleZoom_SetPivot_m80F23B688EFF8862C0F52DF8BE9235482ABFC00D,
	SimpleZoom_ZoomIn_m14371BB9C505A4CDD56EFB97794BC8A293E56A99,
	SimpleZoom_ZoomOut_m6CDE481D2E227FF663582BD4FFA9FA8E268ED3F9,
	SimpleZoom__ctor_mA54C9790EEA211AE6C84CD5F3CE2A9637AF7B898,
	SimpleZoom_U3CSetupU3Eb__62_0_mA6F82113DC0D8C5122E97AF180D1B7A6ACBFB4CB,
	SimpleZoom_U3CSetupU3Eb__62_1_mFD481E0181AF8A5EAA1E15DC406F638D95B37BBF,
	PsLayerDemo_Awake_mB6C32E494D6B77B2D2705AC77DE8EADF92CC8C88,
	PsLayerDemo_OnModeChange_mF8E33019A2033DD76AFC4B967275B16B47F225BF,
	PsLayerDemo_OnOppacityChange_m3D08C0098E3CA3D4E7583E605F238DFDAB8FC554,
	PsLayerDemo__ctor_m9A073E661C35014426214382FF6933995A17EF78,
	ScaleWithScreen_Update_mA4CA5F94B31B03B3F1FEC5F6174D9736629E1DE8,
	ScaleWithScreen__ctor_m1C580B5BCCE9106346DCA75961037084A86F0C13,
	PsGroup_get_thereAreUiLayersInChildren_mDA5BC41A4968E313BCEA0C1BE53C3A34EFA922F5,
	PsGroup_get_canvas_mA56AEED0D767F121D5CFE5971BB7270EABF5E708,
	PsGroup_GetShaderTypeName_mA8B6ADB9A72726738AAC8BB7891121D69BC41D4F,
	PsGroup_get_BlendingShader_mA1B39CD5CC197CD276FBA5EF054EF3913B933CD8,
	PsGroup_set_BlendingShader_m64727CDC41EA8625CAC02F2B0B9E8BD34F4ED60B,
	PsGroup_get_BlendModeTypeInHierarchy_m63594B7F1061AD36BBA891A606F1857BE038DC8E,
	PsGroup_set_BlendModeTypeInHierarchy_mC22B6660111599601DA50D6F9C88AD49D8E2DBF0,
	PsGroup_get_BlendModeType_m276E6679489B9B0505EA3FEACD51BE351B4DA1F5,
	PsGroup_set_BlendModeType_mD6E7FC2FC6535138A74CF8F3769D6A4D2229E55A,
	PsGroup_get_InheritedBlending_m05FF7D0B951686A4D782D09A541D421D9A4E2D87,
	PsGroup_InitMaterial_mBB6D94C9B5A2C23EB7A209B2E2E0AFCEB584E490,
	PsGroup_get_OpacityInHierarchy_m03E77DF21039ADE501B580A7CF555E35796DEF4A,
	PsGroup_set_OpacityInHierarchy_m317E7448495E1969780AB7AEDDE7753E8AE8D8D7,
	PsGroup_get_Opacity_mFB94637DB52391F3F9C80B968F97C8CCB7E8427D,
	PsGroup_set_Opacity_mA1EA5CBD44E8930B3CFF6C5F37AC8161BC72BACE,
	PsGroup_get_VisibleInHierarchy_m9A39CEB7024E2B0CD42A10171ED0F1C77DE0DC68,
	PsGroup_set_VisibleInHierarchy_m2E156B89CA2FABC240B8A06072F2D5F594C53CDE,
	PsGroup_get_Visible_mECEFB43194DA83EFF4D9C222A9C3C49C0DAC2D83,
	PsGroup_set_Visible_m2D7E8E832A91B73FA0934111037F4CCC43CAD6DE,
	PsGroup_Initialize_m1A3CAD02BC39D72A72133B5D125A9E4118584286,
	PsGroup_UpdateUiLayersInChildren_mF282DB8CD0806A3982918291672CF28D0465B875,
	PsGroup_Start_m4593DDFFC8516337302975163355D2F89C6471D1,
	PsGroup_OnDestroy_m91ADFF4B206A6AAD4CCDB270BA5DF48FF4D413AE,
	PsGroup_OnEnable_m16C6FBB7FD8B02AECD275088F27AE28B59F1EF03,
	PsGroup_OnDisable_m65229058D1F28536B46D56B11CC605B456B8807F,
	PsGroup_OnTransformParentChanged_mEF0B765A3F2A4854DF2AB8510067ABFC4E41DB10,
	PsGroup__ctor_mB1EB07F35D1EA313457B341EFB6BD4191D67F4D5,
	PsLayer_get_BlendingShader_m0022324B3232BC6839CE9E979A59F21B31CF21EF,
	PsLayer_set_BlendingShader_m9147559B86EEA4117C9FF87B6E9570FBC4694F64,
	PsLayer_get_BlendModeTypeInHierarchy_mDD961EC6CD7D2BC774895E3AC50AD22FE369C04A,
	PsLayer_set_BlendModeTypeInHierarchy_mF918E59292F4C531F875E5D0C4F9B7E144E5AFC4,
	PsLayer_Awake_m26982DF17D4D2C4C8A795C63DEFF10CEF9B7E258,
	PsLayer_Initialize_mC0D7F0F3FBCC34BE49D7A30BA90BFED591BD9FCE,
	PsLayer_SetBlendMode_mF7088057363C944290ACBC14DD0730E6541B4413,
	PsLayer_SetBlendOperation1_m1CEEBB9CFB7B62662B0034AF7DBD65DEBC84B2AA,
	PsLayer_SetBlendOperation2_mF5A1C6EDB06058B56E15B940A0C198512C77F037,
	PsLayer_Update_m7EAE925C05138A7C2739B588882FCAF17E5E7D5C,
	NULL,
	PsLayer_get_ShaderName_mD64438F4F72863DE5F75CB97C6B433A599C09149,
	PsLayer_get_Proxy_m85A0D759E4D03E8C4BE62CD689A2BD34558AD1BB,
	PsLayer_set_Proxy_mC7194057B8C794BC9282B419740D934AEDCFDA0A,
	PsLayer_get_OpacityInHierarchy_m9FA17C0716F1E7E371B0EDDD7022B5A580820656,
	PsLayer_set_OpacityInHierarchy_m08BE2B4F3B5306E7E2E93F0DD5F2D9100CD2DE9B,
	PsLayer_get_VisibleInHierarchy_m35AC94EC3C7B7133720D6F70F6898BCC09C12B70,
	PsLayer_set_VisibleInHierarchy_mD5CB9EDC6488C96E2A0A08148E8F4F3EC7A80D1E,
	PsLayer_get_Visible_m43A3B2C13B337EE85AC52826B140B0DB6077551A,
	PsLayer_set_Visible_m145003AF17AA235C32E4576A862C11D1BD10EAC4,
	PsLayer_InitMaterial_m6813EA6634E363699EC4913BEEB4A6051FF84696,
	PsLayer__ctor_m8EF9BC39C58936552ACEE3D0AA9A45FDB8B0E6EA,
	PsLayer__cctor_m419FBD70539D10B926BE8F03ED425DAA1938D6E4,
	PsLayerImage_get_DefaultShaderName_m3FB2042D5676CA10D6B5FDDE5324E8C59A0AE482,
	PsLayerImage_InitMaterial_m01FEA5644EDA481C9CF3383CE86EA16DEF4E552E,
	PsLayerImage__ctor_mBE63B9D33F0C5F5BBDFA50F2068FFF913FF8007F,
	PsLayerSprite_get_DefaultShaderName_mA6872D32CB63FC336FE1B113D39DEF4A61850E05,
	PsLayerSprite_InitMaterial_m53EE63DE710F01E8BE7CD47A04FF873589421E74,
	PsLayerSprite__ctor_mD2D860296E9051E30192B44E5BF06A2DEB5CB126,
	PsdBlendModeType_GetFastBlendModeType_m896378857F26D57952783F7B20FDEF33A4E64797,
	PsdBlendModeType_GetGroupBlendModeType_mABA46E7FB3E154C6293CB5EF7AD2E7FC15A83C32,
	PsdBlendModeType__ctor_m6C2447BFD323EC888CAD6EDF6F6DE7389F9C4693,
	PsdBlendModeType__ctor_m6C417BBF6AA97F44CD5C6C1FCA5339FCE758CC65,
	PsdBlendModeType_op_Implicit_mA820AA78CBC94949BC4199BEE30C28801564A737,
	PsdBlendModeType_op_Implicit_mDA61F08083F06A7F02FF942CDB59AF7446F17B2B,
	PsdBlendModeType_op_Implicit_mA225EAD32EBF4AB92EBF3CDB436487C701515B7E,
	PsdBlendModeType_op_Implicit_mA2F6A2022B82787FA2CA07D63FA74DC02DAFB420,
	PsdBlendModeType_op_Implicit_m97039F1036A92A91883B4B382870FEB28DC8550F,
	PsdBlendModeType_op_Implicit_m2FD2A54C36EF1086FEAB6A68F751CB60F6D3F4A9,
	PsdBlendModeType_IsSimple_m3BF5EEEDFA0822D0A10F5D738E23654B7E17C58F,
	PsdBlendModeType__cctor_m112D4DAA7C21F49BA25E819DF744606BC85A3EE1,
	BaumUI_Instantiate_mF2588C90827124DDDB7FA1B48D9CC4E687F9B77E,
	Cache_CreateCache_m1B1EE5A2DBC69FCC49FFF38076D72835A5DB94F1,
	Cache_ClearCache_m9E0E1D25BF13B5F9FF409078F3B1998B472D6F0F,
	Cache_Get_m8156F01185F25AEF2400DCFB2FB498B0444A8A03,
	Cache__ctor_m86CD81683A0A29924C9ABCA82443DE7B12E4E527,
	Cache__cctor_m87962E808695EE6DA160707A18AAC7C1FDC28E10,
	List_get_Content_m42FB0DF3A5E1F9A3C945705F4E709D7DC6B0A7CB,
	List_get_ContentRectTransform_m2C8702727320BEE67AEAC3574B632242562D8072,
	List_get_RectTransform_m86C9498A130D634ABA846EDADB817302C32D8731,
	List_get_ScrollRect_m5E8DD6F8DAA466538D393B3751C980278C4A0A6F,
	List_set_Scrollbar_m12F7B2A7C22E735072421B725D21A1F8C1306FE7,
	List_get_Count_m68B841D260B93C4A4B21C38D8CAA9F3F47B67F83,
	List_get_UISelector_mFBBB6516CAAE91FED39561855D75788F828C560C,
	List_set_UISelector_m56A593C58E116A040D06561935AAEF7DB3DCCBF9,
	List_get_UIFactory_m792392FB27E412992632870496B188071AF68EAF,
	List_set_UIFactory_mB4BA69F9EAEE05465F0D6CFC18CF6BF2DADA14F5,
	List_get_UIUpdater_m6635D509318C7F732F264C58D61BC6E6005008E8,
	List_set_UIUpdater_m2F08E2CD235FFB7D0C538A0827858204BA3295B7,
	List_Awake_m250C2DA4281FDCDEA7DEC0EF45E11576BD010995,
	List_CreateObject_mB55EA3FF335811DAA9EBAC00208ECF681B8DA9F1,
	List_AddItem_m467A2B01D4BB2E8A7CE7850C19FD9B45A7955E66,
	List_ReturnObjectsToPool_m58B4033D2D01706F6EA91488F22281C044FA68AC,
	List_ToPool_m0E84F8D19EB10F49A8566C8AE679DAD203E41047,
	List_AddItemDirect_m230F23A663838236343A9D4FF28B7E1D4E7E35D9,
	List_Resize_mF029C2EA20F067F238437C6B6687FA2FC5F4E9F8,
	List_LateUpdate_mC90F3EF1FC95EA8ADD5BE51B3F3A9CFFBC60416D,
	List_TryCreate_m82A135305FB715341EB125DA61C9FBDD17BEAFA7,
	List_TryDelete_m92848A90DF1F5F0747D801F4CC8814DA39DDFC12,
	List_UpdateItem_m7102AF2D416E79BE68FAFFEAE7643B447B2022CA,
	List_UpdateAll_m377FC0DFDD97D014F28317FD5E157FD690CF2453,
	List_ResetScroll_mB225DFB5360263D1C0D291BE8CE3C8EFE2E4DE41,
	List__ctor_m07D57134BAC36F9028575E2156DA643FAFECAD67,
	ListLayoutGroup_get_MaxElementSize_m11A0ABC230136F8CDABECFFA50604A681E103188,
	ListLayoutGroup_set_MaxElementSize_m27177BEA11BECEA1FCCE309C7A51B6016051EC7B,
	ListLayoutGroup_get_ElementPositions_m98CAB463C22948EBC6A1AE82A5F72BECD399B537,
	ListLayoutGroup_set_ElementPositions_mEC72450A41D7064023B0B9CE61A74BDAF33FDF15,
	ListLayoutGroup_Initialize_m25E914121212C774B720E3443A49697F2A856EDF,
	ListLayoutGroup_get_RectTransform_mC5401FE6669A55260195A0BCBE2B9F472F88290A,
	ListLayoutGroup_RequestUpdate_mCE71521B218F5BFC351D574221F7C1F5EBF79781,
	ListLayoutGroup_RecalcSize_m9EB9773343788BD8A073B1D9E1402A83D95BA11C,
	ListLayoutGroup_RecalcSizeInternal_mE5C08C8BC80034D9165F304B4ED90BEBE9A063DC,
	ListLayoutGroup_ResetScroll_m9A050EC7ECA4B742120E1B204812012616A58AAB,
	ListLayoutGroup__ctor_mDDD65CFEA363F608A6776669308113FB710E4F47,
	RawData_get_Info_m06AD87522772AF915B4CA149161EC8DEB5C0D300,
	RawData_set_Info_mF9033334F504DD6BBC442785093714C729AF1C9E,
	RawData__ctor_m8F827B84F022B5B8030A4D04A2252E812D8B0D93,
	UIRoot_get_Raw_m769955BB65F2EB60B8A3CBC3D5E531BC62202B56,
	UIRoot_Awake_m922C245AD5C851615679BA2B7C0E76C066543CBA,
	UIRoot_Get_m132DD43E88B4233D10D356155887A2EECF6AF8F2,
	UIRoot_Get_mE5B7329F0096C027D465CBA89068AF2FACDA0BD9,
	NULL,
	NULL,
	NULL,
	UIRoot_CreateWithCache_m3D64CBC28ADEC12B0C30FB7EE74901496F2DA77B,
	UIRoot_SetupCache_m595F80BD2519D6FFD8DD48CAA5623619812C56E9,
	UIRoot_UpdateCache_m36D573DA85C6984ECB658689CE2A06D96D5F5EFE,
	UIRoot_GetChildMap_mBE0E0E6CA340B561EC918545D2069FC4EEB838A4,
	UIRoot_RecalculateBounds_m451F46825F38E6AF32AE637693F08AD6AEE56801,
	UIRoot_RecalculateBounds_m2942583D192B04B94FC78C02900CA1E9445EBB58,
	UIRoot__ctor_m718CEDC4FD727C4E3F4B339D6DA22FB4F3BBF789,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UIMapper__ctor_m8CA5B3BF1BD4D9680788A71DD4FC83B5142C859E,
	UIMapper_Get_m3696884903AA76C5D183E0C06B5AA8CBB207A4F4,
	UIMapper_Get_mA9C50896AE3FC8E31AC469AC6609F16FED5E086B,
	NULL,
	NULL,
	UIMapper_GetChildMap_mD943C2DAF325CB80F3C68BE8E5A2F76FB1B296D3,
	NULL,
	Sample_Start_mE5ABECA6AEC5884D0892751C53C2685D07463649,
	Sample_Update_m4F7D39AF038C42E67A77576D7481F9A3A4F453E2,
	Sample_ImageSample_mFE6FE5E412339F203B798B7A7FE9ADCE38E82F82,
	Sample_ButtonSample_mE5E7AA267C3A468FC10A978DA99C30F08D743EB7,
	Sample_ListSample_mDF88333CB32E73FE0043CD4FD38F1CA9A601F331,
	Sample_SliderSample_m32ADAF04829BA5558D961F06F97D53FBCBBBBC6A,
	Sample__ctor_mF85224DFFC1622B62BFD64B790D69C94821B92A7,
	Sample_U3CListSampleU3Eb__7_0_mBA9622AF8D4108DE9252CD55098C83B18C09A953,
	Sample_U3CListSampleU3Eb__7_1_mB700F85AE910FCC3C3A45AAA9F9AC72B61F9895C,
	MessageDelegate__ctor_mE80DF7F22AC095655F222BA7B808FB750C059479,
	MessageDelegate_Invoke_m71D9C54B8D33C236D4145F5C171F5057C0A08B09,
	MessageDelegate_BeginInvoke_mAEE45C362BD9197D646B59B1414ABE19C53BF4C1,
	MessageDelegate_EndInvoke_m57B0BBB4D9EBF8DC1F6A7D8A6C449B7C1C75796F,
	MessageHandlerDelegate__ctor_mB09E60C8134DF32641562ED528E1E0E35B40184A,
	MessageHandlerDelegate_Invoke_m82C612B57B22FCF40994CAA70914DD9701E5CCCF,
	MessageHandlerDelegate_BeginInvoke_m3E0807D2B4E1CB959FEEA14DFB15AD08299EE994,
	MessageHandlerDelegate_EndInvoke_mE15D4204B18F5409F5B926454790F915652ED3B5,
	DisplayNameAttribute__ctor_m8B7D935FAA9CF5CD055BDEA7856CEDECA0B00A3B,
	IncrementAttribute__ctor_m0D82A5ED7B83C2C3E93B8F5469A4C1E0302B4D14,
	NumberRangeAttribute__ctor_m0EC713F19D0C2DAD6320130466ECB59624102CE8,
	SortAttribute__ctor_m2437D5A00ECCCFA06742718C5E6E153D1672339A,
	takePictureHandler__ctor_mA5DE095B02C3C994487382748B6E8BB1B580B675,
	takePictureHandler_Invoke_m92267B6E686F08546F74EF54E450F0E724F5FD35,
	takePictureHandler_BeginInvoke_m4995F7E6B499267082E7F990FB9DF1F06BFCA87A,
	takePictureHandler_EndInvoke_m1A6265C03A03F3536E7FF12CEAA7B7037EE38C71,
	U3C_takeScreenShotU3Ed__9__ctor_m98D21DEB4F959143BE9A697117F9093CD3E66790,
	U3C_takeScreenShotU3Ed__9_System_IDisposable_Dispose_mBAD312ADCFBE0F7FB1C17BD8523AA6D55D518FDF,
	U3C_takeScreenShotU3Ed__9_MoveNext_m68574E71E009253B4E58A500575E3CBF10B0F319,
	U3C_takeScreenShotU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D1AA346300183E5496293449F8F1478CC772840,
	U3C_takeScreenShotU3Ed__9_System_Collections_IEnumerator_Reset_mAA1066CF404ABF4C4DFF872B95BA570AD3C56E1E,
	U3C_takeScreenShotU3Ed__9_System_Collections_IEnumerator_get_Current_m6F7ACEF03C951FD316561FBA3E939E10A90B3A62,
	U3CLoadAssetBundleU3Ed__5__ctor_m0D24CC471DFE4867573F1CB3CDCB9C33B53E92BB,
	U3CLoadAssetBundleU3Ed__5_System_IDisposable_Dispose_m73DEA100541AB3B3592D440EA2E3675F63613B42,
	U3CLoadAssetBundleU3Ed__5_MoveNext_m5019B4D24927E1AA7F9A471B83B867F24AB62317,
	U3CLoadAssetBundleU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86666B1EBB5C37F1B9321B0E2A8F6DFF8278C26E,
	U3CLoadAssetBundleU3Ed__5_System_Collections_IEnumerator_Reset_mBB48409ABA76CD84FBAB33E3C8ABCBB180DAB092,
	U3CLoadAssetBundleU3Ed__5_System_Collections_IEnumerator_get_Current_m43078AE5648D8BB63F31A6BEE4A8DD7A98E05770,
	U3CU3Ec__cctor_mD71D5D2761802C95344CE87F7DDE538B5498E58C,
	U3CU3Ec__ctor_mAC773D6C219250B8805A746495419F4E25B2CE85,
	U3CU3Ec_U3CtakeScreenShotU3Eb__9_0_m6BDA3DB4D420243F4EFD83E9DDA72E758EEB44C3,
	U3CtakeScreenShotU3Ed__9__ctor_mF73AFE0F6E4C864084E40A85C1BFF86855506C26,
	U3CtakeScreenShotU3Ed__9_System_IDisposable_Dispose_m524DD52B46B6F2EF8EB74BD59173EC7ED032C47C,
	U3CtakeScreenShotU3Ed__9_MoveNext_mFF8B8F0BEC5817E3EC414E4A016623E713ADD22E,
	U3CtakeScreenShotU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B2A47712745A9C862F2E23477B6170650D0941D,
	U3CtakeScreenShotU3Ed__9_System_Collections_IEnumerator_Reset_mEEF116F652FFE4685D18DFEB0A6942BF12949910,
	U3CtakeScreenShotU3Ed__9_System_Collections_IEnumerator_get_Current_m02385082BF51D25EA85EB28F7D79FDB947EAC3E5,
	U3CU3Ec__DisplayClass11_0__ctor_m8324C29611962A04ABD2041EAF2FD37F228F734B,
	U3CU3Ec__DisplayClass11_0_U3CPickImageU3Eb__0_mF939C9A7916DCA8FBC9B6662F0C3DF34938792A1,
	U3CRemoveBGU3Ed__12__ctor_m9E0FCE814FC89091A4906D3ACF14F2D7FC1AFC42,
	U3CRemoveBGU3Ed__12_System_IDisposable_Dispose_m57B090527030200EEEEC9F40BAF3D1E1AE315802,
	U3CRemoveBGU3Ed__12_MoveNext_m7EBF1C1AF017BCF2D457A02A0F8767A7BBF1C9B8,
	U3CRemoveBGU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1D54C2FAD6B50BDC54F824D5402EB602C744F5B,
	U3CRemoveBGU3Ed__12_System_Collections_IEnumerator_Reset_mD64314FAFBB54478C70726DC227C0B6AA73B4C19,
	U3CRemoveBGU3Ed__12_System_Collections_IEnumerator_get_Current_mAC4115AA52010DE3B305FA93B87618D8B5B312F9,
	U3CLoadImageCorU3Ed__1__ctor_m2F971244D828669C77EEDD65BC2F78D9351C7D8F,
	U3CLoadImageCorU3Ed__1_System_IDisposable_Dispose_mA6C64F49DF03E21CE3773A208CA61EAD95C09C9A,
	U3CLoadImageCorU3Ed__1_MoveNext_m0C67FA70DC6EBA8159BAC5D4DAA333E36C123792,
	U3CLoadImageCorU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B260ECCECA9D874F0725DB3C8BBAF2CA1667ECA,
	U3CLoadImageCorU3Ed__1_System_Collections_IEnumerator_Reset_m95B6B5ECE3F5B580ACEA80F7C64875DBA3B6E631,
	U3CLoadImageCorU3Ed__1_System_Collections_IEnumerator_get_Current_m2633AE0C1483B827C2E0D12A76A2166D76C28456,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ImageComponentProxy__ctor_mE0EFF813E1705FF8976994E7E3BB03960206F402,
	ImageProxy__ctor_mDDA91D15659DFCA39D602F84E133135CC59E19B6,
	ImageProxy_get_alpha_mB427DB92FCAC59FCD2576C6527776451BA206628,
	ImageProxy_set_alpha_m2D98976F528957682EB3C696F0C83BEF88996D8E,
	ImageProxy_get_enabled_m6B091B10C39C2515768FFDB998AD5D70C3E6E290,
	ImageProxy_set_enabled_m90FD1963E5589ED7A0DF77711F38D197753DA32C,
	ImageProxy_get_material_m04BE413F0A403992B9D42ECACBEF98FD9BBA4E25,
	ImageProxy_set_material_m8453EE724EC400A1E9A90E1444B6B18275E1A10E,
	RendererProxy__ctor_m9EBE603500C2A3D2BEA904797F51838DEC3666E4,
	RendererProxy_get_alpha_m0316847BEB0F3A379270DDC7BFDDFE5E1446F0CF,
	RendererProxy_set_alpha_m3D915446A975032481CBD79511EC8AE5F05A57C0,
	RendererProxy_get_enabled_m809537116A88CF180CDA566FDFE76A4F756C9900,
	RendererProxy_set_enabled_m4679149C5EA2086AD8BE790F2E88D7CFCD1D7519,
	RendererProxy_get_material_m25F8F42B1F7ACB9B2A69F958832C818E7D247743,
	RendererProxy_set_material_m9E5074FA34E3CB54FB15579E19F0222209F29602,
	CachedGameObject__ctor_mAEFB3317F18B00B6C71758E13061F8C8479DAA56,
	U3CU3Ec__DisplayClass5_0__ctor_mBCE7287FD829A21922D73413B53626F24586B9F0,
	U3CU3Ec__DisplayClass5_0_U3CGetU3Eb__0_mCE09DB056FD41A55B0F12465974AFC891C99F5FC,
	U3CU3Ec__DisplayClass37_0__ctor_mD123A7D22431A863F1D3DFC208EF56E8973FA334,
	U3CU3Ec__DisplayClass37_0_U3CCreateObjectU3Eb__0_mE1C6C9EFF846CD547F2508D34A5FF628918E943E,
	U3CU3Ec__cctor_m520D883DC6AF217CC737109952DF94F37CFD6E7C,
	U3CU3Ec__ctor_m2F0FBDE8EC8B45AE12108091694FC1C7E5E9C34E,
	U3CU3Ec_U3CInitializeU3Eb__12_0_m6B0B922A405B96E24C143A1BB402B292D0427F66,
	U3CU3Ec_U3CInitializeU3Eb__12_1_m1A8A4D8935239BAC8DED151C9DBBE8F2151E0EE4,
	U3CU3Ec__DisplayClass6_0__ctor_m868D238F24E32046527AC32B0DD0CA21756DDAED,
	U3CU3Ec__DisplayClass6_0_U3CButtonSampleU3Eb__0_m7334858BF883529DD82BE6F54884EEF62ADF136A,
	U3CU3Ec__DisplayClass6_0_U3CButtonSampleU3Eb__1_m9D33048006AFAE0D55E01935ED8E5A3B91D524E7,
	U3CU3Ec__DisplayClass7_0__ctor_m56E85AF94C3C4B7F88D15A94A87E6067E9F6C665,
	U3CU3Ec__DisplayClass7_0_U3CListSampleU3Eb__2_m742E5DA0E742933E1DBA363C9BC4936F1FD7CC17,
};
static const int32_t s_InvokerIndices[417] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	154,
	1888,
	1665,
	154,
	3,
	3,
	3,
	23,
	-1,
	-1,
	-1,
	-1,
	0,
	-1,
	40,
	26,
	23,
	106,
	26,
	26,
	26,
	26,
	23,
	2682,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	3,
	4,
	3,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	3,
	124,
	27,
	125,
	26,
	154,
	154,
	362,
	1124,
	105,
	148,
	148,
	148,
	123,
	459,
	459,
	459,
	459,
	147,
	147,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	2683,
	23,
	23,
	23,
	1873,
	23,
	23,
	28,
	23,
	23,
	14,
	23,
	14,
	26,
	23,
	23,
	23,
	23,
	9,
	2250,
	23,
	23,
	23,
	23,
	31,
	28,
	26,
	337,
	337,
	337,
	337,
	337,
	31,
	31,
	23,
	23,
	26,
	32,
	31,
	23,
	89,
	31,
	89,
	31,
	26,
	23,
	137,
	1,
	1470,
	1474,
	1441,
	23,
	26,
	26,
	26,
	23,
	14,
	14,
	731,
	337,
	731,
	731,
	1641,
	49,
	23,
	23,
	26,
	23,
	89,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2215,
	1400,
	1442,
	2684,
	2684,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	14,
	43,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	23,
	731,
	337,
	731,
	337,
	89,
	31,
	89,
	31,
	2685,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	32,
	10,
	32,
	23,
	2685,
	26,
	38,
	38,
	23,
	14,
	14,
	14,
	26,
	731,
	337,
	89,
	31,
	89,
	31,
	23,
	23,
	3,
	14,
	23,
	23,
	14,
	23,
	23,
	21,
	21,
	23,
	2686,
	0,
	94,
	94,
	0,
	43,
	43,
	46,
	3,
	1,
	27,
	23,
	28,
	23,
	3,
	14,
	14,
	14,
	14,
	26,
	10,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	28,
	32,
	23,
	32,
	28,
	32,
	23,
	30,
	30,
	32,
	23,
	23,
	23,
	1441,
	1442,
	14,
	26,
	26,
	14,
	23,
	23,
	2687,
	23,
	23,
	14,
	26,
	23,
	14,
	23,
	14,
	148,
	-1,
	-1,
	-1,
	0,
	154,
	0,
	28,
	23,
	1442,
	23,
	-1,
	14,
	148,
	-1,
	-1,
	28,
	-1,
	27,
	14,
	148,
	-1,
	-1,
	28,
	-1,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	34,
	130,
	124,
	26,
	205,
	26,
	124,
	26,
	205,
	26,
	26,
	338,
	1905,
	32,
	124,
	26,
	205,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	3,
	23,
	88,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	731,
	337,
	89,
	31,
	14,
	26,
	23,
	26,
	731,
	337,
	89,
	31,
	14,
	26,
	26,
	731,
	337,
	89,
	31,
	14,
	26,
	197,
	23,
	9,
	23,
	9,
	3,
	23,
	2551,
	2551,
	23,
	23,
	23,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[10] = 
{
	{ 0x02000002, { 0, 21 } },
	{ 0x02000004, { 21, 9 } },
	{ 0x06000016, { 30, 1 } },
	{ 0x0600012B, { 31, 1 } },
	{ 0x0600012C, { 32, 1 } },
	{ 0x0600012D, { 33, 1 } },
	{ 0x06000135, { 34, 1 } },
	{ 0x0600013F, { 35, 1 } },
	{ 0x06000140, { 36, 1 } },
	{ 0x06000142, { 37, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[38] = 
{
	{ (Il2CppRGCTXDataType)2, 35086 },
	{ (Il2CppRGCTXDataType)3, 26737 },
	{ (Il2CppRGCTXDataType)2, 35087 },
	{ (Il2CppRGCTXDataType)3, 26738 },
	{ (Il2CppRGCTXDataType)3, 26739 },
	{ (Il2CppRGCTXDataType)2, 35088 },
	{ (Il2CppRGCTXDataType)3, 26740 },
	{ (Il2CppRGCTXDataType)3, 26741 },
	{ (Il2CppRGCTXDataType)2, 35089 },
	{ (Il2CppRGCTXDataType)3, 26742 },
	{ (Il2CppRGCTXDataType)3, 26743 },
	{ (Il2CppRGCTXDataType)2, 35090 },
	{ (Il2CppRGCTXDataType)3, 26744 },
	{ (Il2CppRGCTXDataType)3, 26745 },
	{ (Il2CppRGCTXDataType)3, 26746 },
	{ (Il2CppRGCTXDataType)3, 26747 },
	{ (Il2CppRGCTXDataType)3, 26748 },
	{ (Il2CppRGCTXDataType)2, 33932 },
	{ (Il2CppRGCTXDataType)2, 33933 },
	{ (Il2CppRGCTXDataType)2, 33934 },
	{ (Il2CppRGCTXDataType)2, 33935 },
	{ (Il2CppRGCTXDataType)2, 35091 },
	{ (Il2CppRGCTXDataType)3, 26749 },
	{ (Il2CppRGCTXDataType)1, 33944 },
	{ (Il2CppRGCTXDataType)3, 26750 },
	{ (Il2CppRGCTXDataType)3, 26751 },
	{ (Il2CppRGCTXDataType)2, 35092 },
	{ (Il2CppRGCTXDataType)3, 26752 },
	{ (Il2CppRGCTXDataType)2, 35093 },
	{ (Il2CppRGCTXDataType)3, 26753 },
	{ (Il2CppRGCTXDataType)3, 26754 },
	{ (Il2CppRGCTXDataType)3, 26755 },
	{ (Il2CppRGCTXDataType)3, 26756 },
	{ (Il2CppRGCTXDataType)3, 26757 },
	{ (Il2CppRGCTXDataType)3, 26758 },
	{ (Il2CppRGCTXDataType)3, 26759 },
	{ (Il2CppRGCTXDataType)3, 26760 },
	{ (Il2CppRGCTXDataType)3, 26761 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	417,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	10,
	s_rgctxIndices,
	38,
	s_rgctxValues,
	NULL,
};
