﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.AssetBundle::.ctor()
extern void AssetBundle__ctor_mF8A0A863455A7DF958A966C86BE58E07535CD3A4 (void);
// 0x00000002 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync_Internal(System.String,System.UInt32,System.UInt64)
extern void AssetBundle_LoadFromFileAsync_Internal_m3F153468428A31347039E733D86A6AA6470385FD (void);
// 0x00000003 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String)
extern void AssetBundle_LoadFromFileAsync_m9B68068A09AD5E39904C23E69F03B53D24024C75 (void);
// 0x00000004 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String,System.UInt32)
extern void AssetBundle_LoadFromFileAsync_mE752E328810CB5390D38B81BB2C4FCE2267C41B5 (void);
// 0x00000005 UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromFile_Internal(System.String,System.UInt32,System.UInt64)
extern void AssetBundle_LoadFromFile_Internal_m684F1E7201F297DBCDF4654DFCA2159F61ABFA50 (void);
// 0x00000006 UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromFile(System.String,System.UInt32)
extern void AssetBundle_LoadFromFile_m86972EDAFDDF3D5843580DE74CD8F8A3949E7A4D (void);
// 0x00000007 T UnityEngine.AssetBundle::LoadAsset(System.String)
// 0x00000008 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String,System.Type)
extern void AssetBundle_LoadAsset_m36BA6CCFE773DFCF4490EB4910022B74BB6BF283 (void);
// 0x00000009 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)
extern void AssetBundle_LoadAsset_Internal_m7B24CA02317176FEADA41DE03CFF849D92B2392A (void);
// 0x0000000A UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String,System.Type)
extern void AssetBundle_LoadAssetAsync_m53B5A725689D7A0D5F8C002E2F2F693AE6E99AB5 (void);
// 0x0000000B UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets(System.String,System.Type)
extern void AssetBundle_LoadAssetWithSubAssets_m91332A7571E8536FED6BFBEE16B1C06F91C08FF8 (void);
// 0x0000000C UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync(System.String,System.Type)
extern void AssetBundle_LoadAssetWithSubAssetsAsync_mC73675FD84475804F33F140C675EBA814B8C0050 (void);
// 0x0000000D UnityEngine.Object[] UnityEngine.AssetBundle::LoadAllAssets()
extern void AssetBundle_LoadAllAssets_m66CEC478D6E4BEE3CCA9411310556928D075B7A4 (void);
// 0x0000000E UnityEngine.Object[] UnityEngine.AssetBundle::LoadAllAssets(System.Type)
extern void AssetBundle_LoadAllAssets_m1F7E39416EBCC192881058CBDCA490A8019E13EA (void);
// 0x0000000F UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync()
extern void AssetBundle_LoadAllAssetsAsync_m43BB89A76CD93830B37E40BFA8623326E71C2268 (void);
// 0x00000010 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync()
// 0x00000011 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync(System.Type)
extern void AssetBundle_LoadAllAssetsAsync_mAA52E6B1A7B29D97604108AF80CEE54B4954C984 (void);
// 0x00000012 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync_Internal(System.String,System.Type)
extern void AssetBundle_LoadAssetAsync_Internal_m2EF88B897D614B01D8DDBACC25C3FEA7E754B075 (void);
// 0x00000013 System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern void AssetBundle_Unload_m5B7DE41516A059545F8C493EC459EA0A9166EBB3 (void);
// 0x00000014 System.String[] UnityEngine.AssetBundle::GetAllAssetNames()
extern void AssetBundle_GetAllAssetNames_m0E14E731A20685CECDA32C33E44D7710E5194C26 (void);
// 0x00000015 UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)
extern void AssetBundle_LoadAssetWithSubAssets_Internal_m576FBCE3D60FC5E7E4462E6EF55DF8E5F49072B4 (void);
// 0x00000016 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync_Internal(System.String,System.Type)
extern void AssetBundle_LoadAssetWithSubAssetsAsync_Internal_m9E9879B2CD95EAD2142B96E1CDAB0F167E766CD9 (void);
// 0x00000017 UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern void AssetBundleCreateRequest_get_assetBundle_mFBACEFA632A41C43D4394F6770059DDE0E35BA11 (void);
// 0x00000018 System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern void AssetBundleCreateRequest__ctor_m25AE702BB10868BE4521FBBF1481C36AF51501EE (void);
// 0x00000019 UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern void AssetBundleRequest_get_asset_mCB977DEC51A59B1333AEEFC2AA189D2E5BF4FCF1 (void);
// 0x0000001A UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern void AssetBundleRequest_get_allAssets_mF68D1A1901B4F435D675982523CFEC98A96A6EAA (void);
// 0x0000001B System.Void UnityEngine.AssetBundleRequest::.ctor()
extern void AssetBundleRequest__ctor_m752C12271B44EED163182E197EE783D18E36836B (void);
static Il2CppMethodPointer s_methodPointers[27] = 
{
	AssetBundle__ctor_mF8A0A863455A7DF958A966C86BE58E07535CD3A4,
	AssetBundle_LoadFromFileAsync_Internal_m3F153468428A31347039E733D86A6AA6470385FD,
	AssetBundle_LoadFromFileAsync_m9B68068A09AD5E39904C23E69F03B53D24024C75,
	AssetBundle_LoadFromFileAsync_mE752E328810CB5390D38B81BB2C4FCE2267C41B5,
	AssetBundle_LoadFromFile_Internal_m684F1E7201F297DBCDF4654DFCA2159F61ABFA50,
	AssetBundle_LoadFromFile_m86972EDAFDDF3D5843580DE74CD8F8A3949E7A4D,
	NULL,
	AssetBundle_LoadAsset_m36BA6CCFE773DFCF4490EB4910022B74BB6BF283,
	AssetBundle_LoadAsset_Internal_m7B24CA02317176FEADA41DE03CFF849D92B2392A,
	AssetBundle_LoadAssetAsync_m53B5A725689D7A0D5F8C002E2F2F693AE6E99AB5,
	AssetBundle_LoadAssetWithSubAssets_m91332A7571E8536FED6BFBEE16B1C06F91C08FF8,
	AssetBundle_LoadAssetWithSubAssetsAsync_mC73675FD84475804F33F140C675EBA814B8C0050,
	AssetBundle_LoadAllAssets_m66CEC478D6E4BEE3CCA9411310556928D075B7A4,
	AssetBundle_LoadAllAssets_m1F7E39416EBCC192881058CBDCA490A8019E13EA,
	AssetBundle_LoadAllAssetsAsync_m43BB89A76CD93830B37E40BFA8623326E71C2268,
	NULL,
	AssetBundle_LoadAllAssetsAsync_mAA52E6B1A7B29D97604108AF80CEE54B4954C984,
	AssetBundle_LoadAssetAsync_Internal_m2EF88B897D614B01D8DDBACC25C3FEA7E754B075,
	AssetBundle_Unload_m5B7DE41516A059545F8C493EC459EA0A9166EBB3,
	AssetBundle_GetAllAssetNames_m0E14E731A20685CECDA32C33E44D7710E5194C26,
	AssetBundle_LoadAssetWithSubAssets_Internal_m576FBCE3D60FC5E7E4462E6EF55DF8E5F49072B4,
	AssetBundle_LoadAssetWithSubAssetsAsync_Internal_m9E9879B2CD95EAD2142B96E1CDAB0F167E766CD9,
	AssetBundleCreateRequest_get_assetBundle_mFBACEFA632A41C43D4394F6770059DDE0E35BA11,
	AssetBundleCreateRequest__ctor_m25AE702BB10868BE4521FBBF1481C36AF51501EE,
	AssetBundleRequest_get_asset_mCB977DEC51A59B1333AEEFC2AA189D2E5BF4FCF1,
	AssetBundleRequest_get_allAssets_mF68D1A1901B4F435D675982523CFEC98A96A6EAA,
	AssetBundleRequest__ctor_m752C12271B44EED163182E197EE783D18E36836B,
};
static const int32_t s_InvokerIndices[27] = 
{
	23,
	1713,
	0,
	119,
	1713,
	119,
	-1,
	105,
	105,
	105,
	105,
	105,
	14,
	28,
	14,
	-1,
	28,
	105,
	31,
	14,
	105,
	105,
	14,
	23,
	14,
	14,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000007, { 0, 2 } },
	{ 0x06000010, { 2, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[3] = 
{
	{ (Il2CppRGCTXDataType)1, 25989 },
	{ (Il2CppRGCTXDataType)2, 25989 },
	{ (Il2CppRGCTXDataType)1, 34737 },
};
extern const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule = 
{
	"UnityEngine.AssetBundleModule.dll",
	27,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	3,
	s_rgctxValues,
	NULL,
};
