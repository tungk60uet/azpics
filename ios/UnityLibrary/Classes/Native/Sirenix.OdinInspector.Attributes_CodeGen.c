﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Sirenix.OdinInspector.AssetListAttribute::.ctor()
extern void AssetListAttribute__ctor_m8247558D8C9BBEA29FA028089F7A0D7D9AC5A5DC (void);
// 0x00000002 System.Void Sirenix.OdinInspector.AssetSelectorAttribute::set_Paths(System.String)
extern void AssetSelectorAttribute_set_Paths_m76AA7C29F24AED85D381A5DDA6C2E7008BFBB2FF (void);
// 0x00000003 System.String Sirenix.OdinInspector.AssetSelectorAttribute::get_Paths()
extern void AssetSelectorAttribute_get_Paths_mEC8A7AD9115F4A88436030423B8D0D3947E584CA (void);
// 0x00000004 System.Void Sirenix.OdinInspector.AssetSelectorAttribute::.ctor()
extern void AssetSelectorAttribute__ctor_mD7B3574705DCEC423BD460B9A92123C8A2160A26 (void);
// 0x00000005 System.Void Sirenix.OdinInspector.AssetSelectorAttribute/<>c::.cctor()
extern void U3CU3Ec__cctor_m898AC2ABD1D3662F4F6DAC7C5C5E904834908795 (void);
// 0x00000006 System.Void Sirenix.OdinInspector.AssetSelectorAttribute/<>c::.ctor()
extern void U3CU3Ec__ctor_m8030917C27454A00E147CCBAAB8795473574751B (void);
// 0x00000007 System.String Sirenix.OdinInspector.AssetSelectorAttribute/<>c::<set_Paths>b__12_0(System.String)
extern void U3CU3Ec_U3Cset_PathsU3Eb__12_0_m8675AB5D0D72FC737F62615297D2D030A0DC8AB1 (void);
// 0x00000008 System.Void Sirenix.OdinInspector.AssetsOnlyAttribute::.ctor()
extern void AssetsOnlyAttribute__ctor_m77053C898FD25996672ADF31626685B35BF1F79C (void);
// 0x00000009 System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor(System.String,System.Boolean,System.Boolean,System.Single)
extern void BoxGroupAttribute__ctor_m51F35E9FAAE6A6993CECBB636458130D35C88CC0 (void);
// 0x0000000A System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor()
extern void BoxGroupAttribute__ctor_m3735DB49E46EE2E2AE8AE5415D6C9FA0A93233F2 (void);
// 0x0000000B System.Void Sirenix.OdinInspector.BoxGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void BoxGroupAttribute_CombineValuesWith_m79BABBFF9A0B58F8A15369C25F243BFA76128A96 (void);
// 0x0000000C System.Void Sirenix.OdinInspector.ButtonAttribute::set_DrawResult(System.Boolean)
extern void ButtonAttribute_set_DrawResult_mAD70CD6C9145A69B55D15FEFF29A6BA72CCEA8F3 (void);
// 0x0000000D System.Boolean Sirenix.OdinInspector.ButtonAttribute::get_DrawResult()
extern void ButtonAttribute_get_DrawResult_m7F1681E70FF12206C2D4A94195826002882C53BB (void);
// 0x0000000E System.Boolean Sirenix.OdinInspector.ButtonAttribute::get_DrawResultIsSet()
extern void ButtonAttribute_get_DrawResultIsSet_m708AD98B58E8B96D9FEE46B25439DB6077CA2A7A (void);
// 0x0000000F System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor()
extern void ButtonAttribute__ctor_mF27CD9C19F032DF07E9293DD310F5BE840ED26F8 (void);
// 0x00000010 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonSizes)
extern void ButtonAttribute__ctor_mF71AB4EC269B5BD92CA84ED615BDBDF75CCB3111 (void);
// 0x00000011 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.Int32)
extern void ButtonAttribute__ctor_m0879084C1E2C8C02C21A54F97EC16C7B54483B53 (void);
// 0x00000012 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String)
extern void ButtonAttribute__ctor_m70C961C47AD907A198820FF74C6DD81A5427B93A (void);
// 0x00000013 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonSizes)
extern void ButtonAttribute__ctor_mB526E46E3BE5BF1024BB81FF1954A7454BA10E32 (void);
// 0x00000014 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,System.Int32)
extern void ButtonAttribute__ctor_m74C2A9439E312E6658C20984D3BBE10CBB8D941A (void);
// 0x00000015 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonStyle)
extern void ButtonAttribute__ctor_mCB7708CF62C859C98DB8112D7D7877EDA2E5898D (void);
// 0x00000016 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.Int32,Sirenix.OdinInspector.ButtonStyle)
extern void ButtonAttribute__ctor_m17A5EAF23490CFFF3D56F7A8FD1AA7E1ED3BCA9C (void);
// 0x00000017 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonSizes,Sirenix.OdinInspector.ButtonStyle)
extern void ButtonAttribute__ctor_mC8501A7A6F000FBAE5B34D71C67AEFE88E2724D9 (void);
// 0x00000018 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonStyle)
extern void ButtonAttribute__ctor_m98049103344F9DE50470BB6C38AF2EA19F590A3F (void);
// 0x00000019 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonSizes,Sirenix.OdinInspector.ButtonStyle)
extern void ButtonAttribute__ctor_m75DACD3BA27E65DB9A96FD5F64A3DD9A0087A15D (void);
// 0x0000001A System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,System.Int32,Sirenix.OdinInspector.ButtonStyle)
extern void ButtonAttribute__ctor_mFD9745232184D70BC605975B1817E57DFBF4FD59 (void);
// 0x0000001B System.Void Sirenix.OdinInspector.ButtonGroupAttribute::.ctor(System.String,System.Single)
extern void ButtonGroupAttribute__ctor_m0054EA0E67B1D3755C80EB056F5EBD30F6F19200 (void);
// 0x0000001C System.Void Sirenix.OdinInspector.ChildGameObjectsOnlyAttribute::.ctor()
extern void ChildGameObjectsOnlyAttribute__ctor_m72F82790CBA3BE06BF8A5019D17420040B0C9AC9 (void);
// 0x0000001D System.String Sirenix.OdinInspector.CustomValueDrawerAttribute::get_MethodName()
extern void CustomValueDrawerAttribute_get_MethodName_mA5767EFD5CF8207C1BE229FD422FC3FC44230CE4 (void);
// 0x0000001E System.Void Sirenix.OdinInspector.CustomValueDrawerAttribute::set_MethodName(System.String)
extern void CustomValueDrawerAttribute_set_MethodName_mF28068EC65118ECA2A1B38558EC857BD89693878 (void);
// 0x0000001F System.Void Sirenix.OdinInspector.CustomValueDrawerAttribute::.ctor(System.String)
extern void CustomValueDrawerAttribute__ctor_m7B3ECD14426CFC6B76E7239C23B476F684779AE5 (void);
// 0x00000020 System.Void Sirenix.OdinInspector.DisableInInlineEditorsAttribute::.ctor()
extern void DisableInInlineEditorsAttribute__ctor_mF8B7E6813BD5C9AFBD78A5C49721EB37D56C6560 (void);
// 0x00000021 System.Void Sirenix.OdinInspector.DisableInNonPrefabsAttribute::.ctor()
extern void DisableInNonPrefabsAttribute__ctor_m63D4B41F0E0A99B087DCF8B11E5DBBC427F6451A (void);
// 0x00000022 System.Void Sirenix.OdinInspector.DisableInPrefabAssetsAttribute::.ctor()
extern void DisableInPrefabAssetsAttribute__ctor_m011CA2F9971FA999EF734041BAB91872317B359B (void);
// 0x00000023 System.Void Sirenix.OdinInspector.DisableInPrefabInstancesAttribute::.ctor()
extern void DisableInPrefabInstancesAttribute__ctor_m5E47228B190D987477F41C86ECB66484007FD5EC (void);
// 0x00000024 System.Void Sirenix.OdinInspector.DisableInPrefabsAttribute::.ctor()
extern void DisableInPrefabsAttribute__ctor_m4E3DA39C7874E60D102C85506B59280968346E3B (void);
// 0x00000025 System.Void Sirenix.OdinInspector.DoNotDrawAsReferenceAttribute::.ctor()
extern void DoNotDrawAsReferenceAttribute__ctor_m09DF620481A8C06F01488885A40FA3B7749ACB11 (void);
// 0x00000026 System.Void Sirenix.OdinInspector.EnableGUIAttribute::.ctor()
extern void EnableGUIAttribute__ctor_mCC16666621929D16DCBB1A8B4F900EC44EDC9A35 (void);
// 0x00000027 System.Void Sirenix.OdinInspector.EnumPagingAttribute::.ctor()
extern void EnumPagingAttribute__ctor_m749BBBC6A2FC95712729B4DE19C72B8849102F9E (void);
// 0x00000028 System.Boolean Sirenix.OdinInspector.FilePathAttribute::get_ReadOnly()
extern void FilePathAttribute_get_ReadOnly_mE6D353FDC464B3A9EFF55EF78C02C575E41E6C7E (void);
// 0x00000029 System.Void Sirenix.OdinInspector.FilePathAttribute::set_ReadOnly(System.Boolean)
extern void FilePathAttribute_set_ReadOnly_m221DD6A033004FB5626EFF6FA055CADE2B60E780 (void);
// 0x0000002A System.Void Sirenix.OdinInspector.FilePathAttribute::.ctor()
extern void FilePathAttribute__ctor_m141C277EA439B817D75D6A4378EEBBC854B30DC3 (void);
// 0x0000002B System.Void Sirenix.OdinInspector.FolderPathAttribute::.ctor()
extern void FolderPathAttribute__ctor_m86B2038DF79EA2F7847EAAA1CC2344A740D8C965 (void);
// 0x0000002C System.Void Sirenix.OdinInspector.HideDuplicateReferenceBoxAttribute::.ctor()
extern void HideDuplicateReferenceBoxAttribute__ctor_m9003D334EB4C6B39944428C496CA8A0192F04177 (void);
// 0x0000002D System.Boolean Sirenix.OdinInspector.HideIfGroupAttribute::get_Animate()
extern void HideIfGroupAttribute_get_Animate_mF5AA29F3C90F552A9998C8776912B737D66C4DE3 (void);
// 0x0000002E System.Void Sirenix.OdinInspector.HideIfGroupAttribute::set_Animate(System.Boolean)
extern void HideIfGroupAttribute_set_Animate_m4D3CD713242CB20F8E4109EB386B49F2A1231305 (void);
// 0x0000002F System.String Sirenix.OdinInspector.HideIfGroupAttribute::get_MemberName()
extern void HideIfGroupAttribute_get_MemberName_mFEA756BD4855C66E710EE842D93D08B9755EA461 (void);
// 0x00000030 System.Void Sirenix.OdinInspector.HideIfGroupAttribute::set_MemberName(System.String)
extern void HideIfGroupAttribute_set_MemberName_m982CF7DDCA552327AE01B1D09E75DF5DD2D5CF27 (void);
// 0x00000031 System.String Sirenix.OdinInspector.HideIfGroupAttribute::get_Condition()
extern void HideIfGroupAttribute_get_Condition_mE6857BF4B30ABAC93E337FB918D2CF71AD0B0631 (void);
// 0x00000032 System.Void Sirenix.OdinInspector.HideIfGroupAttribute::set_Condition(System.String)
extern void HideIfGroupAttribute_set_Condition_m8FB8D4239B1C0E2A8C112A7AE01B5CD4FF400DDA (void);
// 0x00000033 System.Void Sirenix.OdinInspector.HideIfGroupAttribute::.ctor(System.String,System.Boolean)
extern void HideIfGroupAttribute__ctor_m4906C608EE64D1E886FA86D29A8DCB4CCD197FD1 (void);
// 0x00000034 System.Void Sirenix.OdinInspector.HideIfGroupAttribute::.ctor(System.String,System.Object,System.Boolean)
extern void HideIfGroupAttribute__ctor_m3267514613583C7F46E2568CCF1B657EB1A27474 (void);
// 0x00000035 System.Void Sirenix.OdinInspector.HideIfGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void HideIfGroupAttribute_CombineValuesWith_m75EBFB293E2957D9B6F2C26E79950B90E6ECCBC5 (void);
// 0x00000036 System.Void Sirenix.OdinInspector.HideInInlineEditorsAttribute::.ctor()
extern void HideInInlineEditorsAttribute__ctor_m78318BFF82C4AB6286362D19382FC488B157C37A (void);
// 0x00000037 System.Void Sirenix.OdinInspector.HideInNonPrefabsAttribute::.ctor()
extern void HideInNonPrefabsAttribute__ctor_m38C345ABE1AD02E5CE773566C53D5628C23D59E0 (void);
// 0x00000038 System.Void Sirenix.OdinInspector.HideInPrefabAssetsAttribute::.ctor()
extern void HideInPrefabAssetsAttribute__ctor_m56DF3C5E0803931801A0818BFF80482ECBE185E0 (void);
// 0x00000039 System.Void Sirenix.OdinInspector.HideInPrefabInstancesAttribute::.ctor()
extern void HideInPrefabInstancesAttribute__ctor_m9DCF14D9FC674E03B655300CAAC03356DA89E6C8 (void);
// 0x0000003A System.Void Sirenix.OdinInspector.HideInPrefabsAttribute::.ctor()
extern void HideInPrefabsAttribute__ctor_mB976FD8E6F61588DF5FEAAEDB2D84FE377F291BC (void);
// 0x0000003B System.Void Sirenix.OdinInspector.HideInTablesAttribute::.ctor()
extern void HideInTablesAttribute__ctor_m9082E7F39E533A3F2279713ED33C8D48ACBACFF8 (void);
// 0x0000003C System.Void Sirenix.OdinInspector.HideNetworkBehaviourFieldsAttribute::.ctor()
extern void HideNetworkBehaviourFieldsAttribute__ctor_mD80534B899DAA097D8011A5500EA103C30028950 (void);
// 0x0000003D System.Void Sirenix.OdinInspector.InlinePropertyAttribute::.ctor()
extern void InlinePropertyAttribute__ctor_m38FC46B53AAC32D93B97537E0922EB88CDE2675C (void);
// 0x0000003E System.Void Sirenix.OdinInspector.LabelWidthAttribute::.ctor(System.Single)
extern void LabelWidthAttribute__ctor_m7DCB42E4880AD8CDE03563924EB5AF64BB933BE8 (void);
// 0x0000003F System.Void Sirenix.OdinInspector.OnCollectionChangedAttribute::.ctor()
extern void OnCollectionChangedAttribute__ctor_m982A4122FCD2196709866CB34C8FF8106ACE3A79 (void);
// 0x00000040 System.Void Sirenix.OdinInspector.OnCollectionChangedAttribute::.ctor(System.String)
extern void OnCollectionChangedAttribute__ctor_m343721F74CD7C8FBC42BD3EED73AFB991FDC653E (void);
// 0x00000041 System.Void Sirenix.OdinInspector.OnCollectionChangedAttribute::.ctor(System.String,System.String)
extern void OnCollectionChangedAttribute__ctor_m6DB1F00EBB85AE6A0511B8FD9650A2495E8E1C91 (void);
// 0x00000042 System.Void Sirenix.OdinInspector.OnInspectorDisposeAttribute::.ctor()
extern void OnInspectorDisposeAttribute__ctor_mAD2279BE383A0423F12EF39C4D421C6C3D5ECB08 (void);
// 0x00000043 System.Void Sirenix.OdinInspector.OnInspectorDisposeAttribute::.ctor(System.String)
extern void OnInspectorDisposeAttribute__ctor_mE2255EE8D89336EE7BEBBC5EE596101C2A92778E (void);
// 0x00000044 System.Void Sirenix.OdinInspector.OnInspectorInitAttribute::.ctor()
extern void OnInspectorInitAttribute__ctor_m8D853E5FF4A7439A5BA4CA59234D58BF7ED76EF2 (void);
// 0x00000045 System.Void Sirenix.OdinInspector.OnInspectorInitAttribute::.ctor(System.String)
extern void OnInspectorInitAttribute__ctor_m74CDD7E081A0A0DF8D451390FBEDD2C7AEEEF373 (void);
// 0x00000046 System.Void Sirenix.OdinInspector.OnStateUpdateAttribute::.ctor(System.String)
extern void OnStateUpdateAttribute__ctor_mE21A3A0C3A41C2FA529B2E5D2EDE202F8CBDB303 (void);
// 0x00000047 Sirenix.OdinInspector.ObjectFieldAlignment Sirenix.OdinInspector.PreviewFieldAttribute::get_Alignment()
extern void PreviewFieldAttribute_get_Alignment_mDBED00E78169E0B356F16A8759FF07D67F5974B5 (void);
// 0x00000048 System.Void Sirenix.OdinInspector.PreviewFieldAttribute::set_Alignment(Sirenix.OdinInspector.ObjectFieldAlignment)
extern void PreviewFieldAttribute_set_Alignment_mC639ECEAA0F283F44483506E821E205D611634BA (void);
// 0x00000049 System.Boolean Sirenix.OdinInspector.PreviewFieldAttribute::get_AlignmentHasValue()
extern void PreviewFieldAttribute_get_AlignmentHasValue_mE470BFDF84F8A6C264F3DC2A4414FF82B3939B92 (void);
// 0x0000004A System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor()
extern void PreviewFieldAttribute__ctor_mF4E39532FED54856F331B95BB4BEA2F7022A6BF5 (void);
// 0x0000004B System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(System.Single)
extern void PreviewFieldAttribute__ctor_m73A84428DFFD8A8F6AF80475138256C119CDB6E0 (void);
// 0x0000004C System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(System.Single,Sirenix.OdinInspector.ObjectFieldAlignment)
extern void PreviewFieldAttribute__ctor_m9CB7BB82E1251D2398470AF788A2017DE9E98EFA (void);
// 0x0000004D System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(Sirenix.OdinInspector.ObjectFieldAlignment)
extern void PreviewFieldAttribute__ctor_m42F6284469DF8F7BB78B4DE69BF2BB32EB5FE534 (void);
// 0x0000004E System.String Sirenix.OdinInspector.ProgressBarAttribute::get_MinMember()
extern void ProgressBarAttribute_get_MinMember_m2CC1ED194ED7A3009E79B1C07A0DE4A39E187ADD (void);
// 0x0000004F System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_MinMember(System.String)
extern void ProgressBarAttribute_set_MinMember_m9A6C0D5E9AE335F63782BEC288D6E17C39178581 (void);
// 0x00000050 System.String Sirenix.OdinInspector.ProgressBarAttribute::get_MaxMember()
extern void ProgressBarAttribute_get_MaxMember_m8B59F7BB73FF537EF9292E1C82690A779221C737 (void);
// 0x00000051 System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_MaxMember(System.String)
extern void ProgressBarAttribute_set_MaxMember_mA6E304FA078A043078F079FD1C05822AEE97D75E (void);
// 0x00000052 System.String Sirenix.OdinInspector.ProgressBarAttribute::get_ColorMember()
extern void ProgressBarAttribute_get_ColorMember_m8BBC82073E0759F54EDA83E001F685C1DDF685FC (void);
// 0x00000053 System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ColorMember(System.String)
extern void ProgressBarAttribute_set_ColorMember_m9359E4B9A3400F7553BFB3D00D56B4943B603266 (void);
// 0x00000054 System.String Sirenix.OdinInspector.ProgressBarAttribute::get_BackgroundColorMember()
extern void ProgressBarAttribute_get_BackgroundColorMember_m70AC774398545E0FB91CE18331214809C968AFBF (void);
// 0x00000055 System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_BackgroundColorMember(System.String)
extern void ProgressBarAttribute_set_BackgroundColorMember_mE873BB83D0F4A0918F2C3AF2494C153E094F5AF4 (void);
// 0x00000056 System.String Sirenix.OdinInspector.ProgressBarAttribute::get_CustomValueStringMember()
extern void ProgressBarAttribute_get_CustomValueStringMember_m355C89D38750B48F2088523A21E4A3602647F6DE (void);
// 0x00000057 System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_CustomValueStringMember(System.String)
extern void ProgressBarAttribute_set_CustomValueStringMember_mD3B3908B9FAA46A37B0A74137E96D01399AEB815 (void);
// 0x00000058 System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.Double,System.Double,System.Single,System.Single,System.Single)
extern void ProgressBarAttribute__ctor_m1AA280ED357596B2D727C77828E5E7135150CC2C (void);
// 0x00000059 System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.String,System.Double,System.Single,System.Single,System.Single)
extern void ProgressBarAttribute__ctor_m3EBD7FE0C0BE330BB9420358ACD8BF7309F450EB (void);
// 0x0000005A System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.Double,System.String,System.Single,System.Single,System.Single)
extern void ProgressBarAttribute__ctor_m05442A34E95B454A385A128774C4E57CBE80ACF4 (void);
// 0x0000005B System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.String,System.String,System.Single,System.Single,System.Single)
extern void ProgressBarAttribute__ctor_m4247618AFE4481DE37C0DFAD7801E8F26BA69B99 (void);
// 0x0000005C System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_DrawValueLabel()
extern void ProgressBarAttribute_get_DrawValueLabel_mC0D88C51C77D902A740C75949A6FA5A8553FBFFB (void);
// 0x0000005D System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabel(System.Boolean)
extern void ProgressBarAttribute_set_DrawValueLabel_mC55B7F41936E19227A24A8F109FCAC95BB9A068E (void);
// 0x0000005E System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_DrawValueLabelHasValue()
extern void ProgressBarAttribute_get_DrawValueLabelHasValue_m02DBBB3955C49F325C00C0F297DAB10A808AEE15 (void);
// 0x0000005F System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabelHasValue(System.Boolean)
extern void ProgressBarAttribute_set_DrawValueLabelHasValue_mFE21A81AACD29E509534804C64D7092FC9EC971D (void);
// 0x00000060 UnityEngine.TextAlignment Sirenix.OdinInspector.ProgressBarAttribute::get_ValueLabelAlignment()
extern void ProgressBarAttribute_get_ValueLabelAlignment_mF5ECF715BE3EE10695716E7CB25DBBC2E16E7B0A (void);
// 0x00000061 System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignment(UnityEngine.TextAlignment)
extern void ProgressBarAttribute_set_ValueLabelAlignment_m35FB4443DEB147A679B29DD278AFBFD3D8244A2B (void);
// 0x00000062 System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_ValueLabelAlignmentHasValue()
extern void ProgressBarAttribute_get_ValueLabelAlignmentHasValue_mFD655331D63227292454EB3995BB9B527D642893 (void);
// 0x00000063 System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignmentHasValue(System.Boolean)
extern void ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m2E49A4D0090B1D60036AB25BA0ABAA025D3F25C5 (void);
// 0x00000064 UnityEngine.Color Sirenix.OdinInspector.ProgressBarAttribute::get_Color()
extern void ProgressBarAttribute_get_Color_mD07AE72BEAEFE6EA3175131A3BCF78C4A90516DB (void);
// 0x00000065 System.String Sirenix.OdinInspector.PropertyRangeAttribute::get_MinMember()
extern void PropertyRangeAttribute_get_MinMember_m1AE82B2EF2CF8ED9A2180464F63085D1C92ACC32 (void);
// 0x00000066 System.Void Sirenix.OdinInspector.PropertyRangeAttribute::set_MinMember(System.String)
extern void PropertyRangeAttribute_set_MinMember_mF0665322F7B5EB30944C1468DE272546D38B1BE7 (void);
// 0x00000067 System.String Sirenix.OdinInspector.PropertyRangeAttribute::get_MaxMember()
extern void PropertyRangeAttribute_get_MaxMember_mBAEEA015901D53E5100A7ACC72DF6A66C3845CC4 (void);
// 0x00000068 System.Void Sirenix.OdinInspector.PropertyRangeAttribute::set_MaxMember(System.String)
extern void PropertyRangeAttribute_set_MaxMember_m91ED22ABEFFEC257A3E8F2AB7EAF91EDA4E0ADBF (void);
// 0x00000069 System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.Double,System.Double)
extern void PropertyRangeAttribute__ctor_m46E3E82518F3BD0221513B22406BE53F83109311 (void);
// 0x0000006A System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.String,System.Double)
extern void PropertyRangeAttribute__ctor_m69B10A0AF618ADABAE4384D7EF88D30E339AE6DF (void);
// 0x0000006B System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.Double,System.String)
extern void PropertyRangeAttribute__ctor_m0F5C052C678C9F88EC421C180711890168F82E4B (void);
// 0x0000006C System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.String,System.String)
extern void PropertyRangeAttribute__ctor_mB000DC0552D3FADF73419B765DCAFE5B7ED660C6 (void);
// 0x0000006D System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor()
extern void PropertySpaceAttribute__ctor_m776D7EF54F7BA3A1C86CD2BBBFFA9A466CA4B11F (void);
// 0x0000006E System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor(System.Single)
extern void PropertySpaceAttribute__ctor_m767AFE53798D1B36405151C25F85EA97292F33C9 (void);
// 0x0000006F System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor(System.Single,System.Single)
extern void PropertySpaceAttribute__ctor_m84DFA94A5E36256F8A3A7D88195F2056226A5B23 (void);
// 0x00000070 System.Void Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::.ctor(System.String)
extern void ResponsiveButtonGroupAttribute__ctor_m21EF1D295EFCBD00BBF2E6EA35FC10C0786AE86F (void);
// 0x00000071 System.Void Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void ResponsiveButtonGroupAttribute_CombineValuesWith_m0C46A0FFC4BF1DC38EA47EC0D6E68DAB2FD3EDA4 (void);
// 0x00000072 System.Boolean Sirenix.OdinInspector.ShowIfGroupAttribute::get_Animate()
extern void ShowIfGroupAttribute_get_Animate_mA28D4E294DB07B30D7F199DB20F66A8132674DDC (void);
// 0x00000073 System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::set_Animate(System.Boolean)
extern void ShowIfGroupAttribute_set_Animate_m0C5232E32FBF5570CA9EBF341F4A9A26383C87CD (void);
// 0x00000074 System.String Sirenix.OdinInspector.ShowIfGroupAttribute::get_MemberName()
extern void ShowIfGroupAttribute_get_MemberName_m2EA81E787AF8407FFA0C215D4780EC4FDB75B322 (void);
// 0x00000075 System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::set_MemberName(System.String)
extern void ShowIfGroupAttribute_set_MemberName_m4FFBB055EEEBA05AE6CF9183D34041B7EEB62A32 (void);
// 0x00000076 System.String Sirenix.OdinInspector.ShowIfGroupAttribute::get_Condition()
extern void ShowIfGroupAttribute_get_Condition_mBAA18A45D8F96BC8D5E46B3C141673EF52A50E46 (void);
// 0x00000077 System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::set_Condition(System.String)
extern void ShowIfGroupAttribute_set_Condition_m7CAF3CF1D9803D698E9011B30EC9EA5536EB8FA9 (void);
// 0x00000078 System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::.ctor(System.String,System.Boolean)
extern void ShowIfGroupAttribute__ctor_mD113EDB716F28754313E2BEB0927B6043981329D (void);
// 0x00000079 System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::.ctor(System.String,System.Object,System.Boolean)
extern void ShowIfGroupAttribute__ctor_mF398B06CE4C1F42A320290017D7296BAAEA7A312 (void);
// 0x0000007A System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void ShowIfGroupAttribute_CombineValuesWith_m4D6EAA8F018088F641E92D46A9DD2919BF79CDE0 (void);
// 0x0000007B System.Void Sirenix.OdinInspector.ShowInInlineEditorsAttribute::.ctor()
extern void ShowInInlineEditorsAttribute__ctor_m7A762533C1DDCCEB86F424F6FD7CAD4F2CDACF1B (void);
// 0x0000007C System.Void Sirenix.OdinInspector.ShowPropertyResolverAttribute::.ctor()
extern void ShowPropertyResolverAttribute__ctor_mABC8EECEE1320586632257A6237B96F8CF857F4B (void);
// 0x0000007D System.Void Sirenix.OdinInspector.SuffixLabelAttribute::.ctor(System.String,System.Boolean)
extern void SuffixLabelAttribute__ctor_mABCF82E4DA146381809AAD4677A35A363CEDF858 (void);
// 0x0000007E System.Void Sirenix.OdinInspector.TableColumnWidthAttribute::.ctor(System.Int32,System.Boolean)
extern void TableColumnWidthAttribute__ctor_m19093F5CE091085A12F2DFEE1A59E362E833C50C (void);
// 0x0000007F System.Boolean Sirenix.OdinInspector.TableListAttribute::get_ShowPaging()
extern void TableListAttribute_get_ShowPaging_mAA4CBA1D36E6E33AD266462C1E59D2422E762EE4 (void);
// 0x00000080 System.Void Sirenix.OdinInspector.TableListAttribute::set_ShowPaging(System.Boolean)
extern void TableListAttribute_set_ShowPaging_m70ED81A12B8FBC10D953B5F449CCCECEAA0437CA (void);
// 0x00000081 System.Boolean Sirenix.OdinInspector.TableListAttribute::get_ShowPagingHasValue()
extern void TableListAttribute_get_ShowPagingHasValue_m8E2C05085222E55DECF631D88E8D5B7CFC235940 (void);
// 0x00000082 System.Int32 Sirenix.OdinInspector.TableListAttribute::get_ScrollViewHeight()
extern void TableListAttribute_get_ScrollViewHeight_m8CD7BCAFCF875A2EEDC3B77D27234D748AB0DAAF (void);
// 0x00000083 System.Void Sirenix.OdinInspector.TableListAttribute::set_ScrollViewHeight(System.Int32)
extern void TableListAttribute_set_ScrollViewHeight_m28162A9FBF938888F1993A403454A9342ACAE41C (void);
// 0x00000084 System.Void Sirenix.OdinInspector.TableListAttribute::.ctor()
extern void TableListAttribute__ctor_m74F94D599C39706135C99A2FEC54F490C1541078 (void);
// 0x00000085 System.Void Sirenix.OdinInspector.TableMatrixAttribute::.ctor()
extern void TableMatrixAttribute__ctor_m5A782F83D5669B3E8BED3227A6A019F04DB88983 (void);
// 0x00000086 System.String Sirenix.OdinInspector.TypeFilterAttribute::get_MemberName()
extern void TypeFilterAttribute_get_MemberName_m499B40CD5A86143CEE9AFACCA276449001544A67 (void);
// 0x00000087 System.Void Sirenix.OdinInspector.TypeFilterAttribute::set_MemberName(System.String)
extern void TypeFilterAttribute_set_MemberName_m03F6F8703319609E1AFDB7C484DB0567EFC39C1C (void);
// 0x00000088 System.Void Sirenix.OdinInspector.TypeFilterAttribute::.ctor(System.String)
extern void TypeFilterAttribute__ctor_m07F0EF43034B8C390DC753136654962CCCF80061 (void);
// 0x00000089 System.Void Sirenix.OdinInspector.TypeInfoBoxAttribute::.ctor(System.String)
extern void TypeInfoBoxAttribute__ctor_m51D2E2C1E51F7110C5AE9124C84E4FD6C655695D (void);
// 0x0000008A System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.String,System.Single)
extern void VerticalGroupAttribute__ctor_mD788EEA9634514D13E321236E9BC3A18186E4DA0 (void);
// 0x0000008B System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.Single)
extern void VerticalGroupAttribute__ctor_m655FA45ED58C1BA24F2011314508B7E40FFC69CB (void);
// 0x0000008C System.Void Sirenix.OdinInspector.VerticalGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void VerticalGroupAttribute_CombineValuesWith_m983E6DD2C5697D4CCFFA5F58C29C6F93828F06BE (void);
// 0x0000008D System.Void Sirenix.OdinInspector.SearchableAttribute::.ctor()
extern void SearchableAttribute__ctor_m74A25AAA65FD0FB6893A616BCE3067BFF900AE5C (void);
// 0x0000008E System.Void Sirenix.OdinInspector.IncludeMyAttributesAttribute::.ctor()
extern void IncludeMyAttributesAttribute__ctor_mD6627C9E1A06B23183641DFAD937CBD15577328F (void);
// 0x0000008F System.Boolean Sirenix.OdinInspector.ISearchFilterable::IsMatch(System.String)
// 0x00000090 System.Void Sirenix.OdinInspector.OdinRegisterAttributeAttribute::.ctor(System.Type,System.String,System.String,System.Boolean)
extern void OdinRegisterAttributeAttribute__ctor_m6260347940664F402312B72B951F4072A5CBDCFB (void);
// 0x00000091 System.Void Sirenix.OdinInspector.OdinRegisterAttributeAttribute::.ctor(System.Type,System.String,System.String,System.Boolean,System.String)
extern void OdinRegisterAttributeAttribute__ctor_m305FAA270A22E7BB4406E17C03ADA1830E174B28 (void);
// 0x00000092 System.Void Sirenix.OdinInspector.TitleGroupAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.TitleAlignments,System.Boolean,System.Boolean,System.Boolean,System.Single)
extern void TitleGroupAttribute__ctor_mBBBF50005F89D5F5510014B9B5FBDFAB3934B80D (void);
// 0x00000093 System.Void Sirenix.OdinInspector.TitleGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void TitleGroupAttribute_CombineValuesWith_mD450E1372EF1FD6291F2E9150EF83E074847A679 (void);
// 0x00000094 System.Void Sirenix.OdinInspector.ColorPaletteAttribute::.ctor()
extern void ColorPaletteAttribute__ctor_m0FB1C53EA41531CAF7CDF5F24930769F245E154C (void);
// 0x00000095 System.Void Sirenix.OdinInspector.ColorPaletteAttribute::.ctor(System.String)
extern void ColorPaletteAttribute__ctor_m781B42126F89B6F56DE709163D0A4D74E5A9BCBE (void);
// 0x00000096 System.String Sirenix.OdinInspector.CustomContextMenuAttribute::get_MethodName()
extern void CustomContextMenuAttribute_get_MethodName_m7D7FC1ECD9F4B0434E85FA639C4B3520C5A0A9A3 (void);
// 0x00000097 System.Void Sirenix.OdinInspector.CustomContextMenuAttribute::set_MethodName(System.String)
extern void CustomContextMenuAttribute_set_MethodName_m0DACAC5D5FBB0CCB210649D026953C4907769B4C (void);
// 0x00000098 System.Void Sirenix.OdinInspector.CustomContextMenuAttribute::.ctor(System.String,System.String)
extern void CustomContextMenuAttribute__ctor_mC29FF6E64432B517E79697D8D17715B3F06A0C00 (void);
// 0x00000099 System.Void Sirenix.OdinInspector.DelayedPropertyAttribute::.ctor()
extern void DelayedPropertyAttribute__ctor_m489FF80D8A9B462702E606A4DF2BE009D99FF849 (void);
// 0x0000009A System.Void Sirenix.OdinInspector.DetailedInfoBoxAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType,System.String)
extern void DetailedInfoBoxAttribute__ctor_m145816EE77ED956C065B36607C1DAEA8FA5BAA3C (void);
// 0x0000009B System.Void Sirenix.OdinInspector.DictionaryDrawerSettings::.ctor()
extern void DictionaryDrawerSettings__ctor_mCD32097B13EC46AEFE9ED48441540D7C131140D5 (void);
// 0x0000009C System.Void Sirenix.OdinInspector.DisableContextMenuAttribute::.ctor(System.Boolean,System.Boolean)
extern void DisableContextMenuAttribute__ctor_m3528AA0C5A479BE6ABAF1F4495722FB590D028C2 (void);
// 0x0000009D System.String Sirenix.OdinInspector.DisableIfAttribute::get_MemberName()
extern void DisableIfAttribute_get_MemberName_m1D81D1FA3D91512BB919424574C0EC98F8A0BBCD (void);
// 0x0000009E System.Void Sirenix.OdinInspector.DisableIfAttribute::set_MemberName(System.String)
extern void DisableIfAttribute_set_MemberName_mFEC9F57C525825A0831C81208ED093901CBAD7A7 (void);
// 0x0000009F System.Void Sirenix.OdinInspector.DisableIfAttribute::.ctor(System.String)
extern void DisableIfAttribute__ctor_m461AF14E80789329EF1019B33DE25D56CF6CBD18 (void);
// 0x000000A0 System.Void Sirenix.OdinInspector.DisableIfAttribute::.ctor(System.String,System.Object)
extern void DisableIfAttribute__ctor_mFC765F2149E0A3CCFEA59CD75B2276B87314B20F (void);
// 0x000000A1 System.Void Sirenix.OdinInspector.DisableInEditorModeAttribute::.ctor()
extern void DisableInEditorModeAttribute__ctor_m13F7D48945BD6670A28279DDB616078B9B2F43C6 (void);
// 0x000000A2 System.Void Sirenix.OdinInspector.DisableInPlayModeAttribute::.ctor()
extern void DisableInPlayModeAttribute__ctor_m97B7FB5CCD60F02CEF1099B6743878DA3615A774 (void);
// 0x000000A3 System.Void Sirenix.OdinInspector.DisplayAsStringAttribute::.ctor()
extern void DisplayAsStringAttribute__ctor_m6B84964662A8187BE17BD6334934BA2B409A2ACC (void);
// 0x000000A4 System.Void Sirenix.OdinInspector.DisplayAsStringAttribute::.ctor(System.Boolean)
extern void DisplayAsStringAttribute__ctor_mFD5330EA486F7C1876B7DAD38322173D33A51D7C (void);
// 0x000000A5 System.Void Sirenix.OdinInspector.DontApplyToListElementsAttribute::.ctor()
extern void DontApplyToListElementsAttribute__ctor_m39E251F2A1BA63B4A48895001AC81CD035D6E113 (void);
// 0x000000A6 System.Void Sirenix.OdinInspector.DrawWithUnityAttribute::.ctor()
extern void DrawWithUnityAttribute__ctor_m255ED9DDDBD0EEA71807B8C6BB304604F1B020A5 (void);
// 0x000000A7 System.String Sirenix.OdinInspector.InlineButtonAttribute::get_MemberMethod()
extern void InlineButtonAttribute_get_MemberMethod_mAD5C9BAF96545FE2DB4034B407E97847E5E0EDF3 (void);
// 0x000000A8 System.Void Sirenix.OdinInspector.InlineButtonAttribute::set_MemberMethod(System.String)
extern void InlineButtonAttribute_set_MemberMethod_mCFD34ED6D9B04F9C4112B91AEA84B7546B4B8D24 (void);
// 0x000000A9 System.Void Sirenix.OdinInspector.InlineButtonAttribute::.ctor(System.String,System.String)
extern void InlineButtonAttribute__ctor_m01CB0BA9AF4B90E349CDAF936BB0BD9EDA76C512 (void);
// 0x000000AA System.Void Sirenix.OdinInspector.ShowForPrefabOnlyAttribute::.ctor()
extern void ShowForPrefabOnlyAttribute__ctor_m243ED5ABA1FDC9E3FED7650C390750C02C75518A (void);
// 0x000000AB System.Void Sirenix.OdinInspector.EnableForPrefabOnlyAttribute::.ctor()
extern void EnableForPrefabOnlyAttribute__ctor_m2C520605A683345636EF23DBC8A03AC0E6D2FDAF (void);
// 0x000000AC System.String Sirenix.OdinInspector.EnableIfAttribute::get_MemberName()
extern void EnableIfAttribute_get_MemberName_mD2198B861D98311267B4856B0A450CB382C3BC42 (void);
// 0x000000AD System.Void Sirenix.OdinInspector.EnableIfAttribute::set_MemberName(System.String)
extern void EnableIfAttribute_set_MemberName_m5E4EC1A7D06273D9BBA3305FF49DA57009F1123D (void);
// 0x000000AE System.Void Sirenix.OdinInspector.EnableIfAttribute::.ctor(System.String)
extern void EnableIfAttribute__ctor_m1F1F838D286FAE9370AD489B884E42E1B73AF976 (void);
// 0x000000AF System.Void Sirenix.OdinInspector.EnableIfAttribute::.ctor(System.String,System.Object)
extern void EnableIfAttribute__ctor_m3C1429D1927B76D03B47D67EF295DC6C0614CE39 (void);
// 0x000000B0 System.Void Sirenix.OdinInspector.EnumToggleButtonsAttribute::.ctor()
extern void EnumToggleButtonsAttribute__ctor_mE37FDB25B17CE53DA754A3E0F830F396318F34F5 (void);
// 0x000000B1 System.String Sirenix.OdinInspector.HideIfAttribute::get_MemberName()
extern void HideIfAttribute_get_MemberName_m5C0C4FB895E1A3788F62DDE0A0680920EE3BD8F1 (void);
// 0x000000B2 System.Void Sirenix.OdinInspector.HideIfAttribute::set_MemberName(System.String)
extern void HideIfAttribute_set_MemberName_m475241B5D603C50C78DDED174F3DBE823F7A871C (void);
// 0x000000B3 System.Void Sirenix.OdinInspector.HideIfAttribute::.ctor(System.String,System.Boolean)
extern void HideIfAttribute__ctor_m6DB6CACEF83AE294D4888CC58DA860DC785E0D01 (void);
// 0x000000B4 System.Void Sirenix.OdinInspector.HideIfAttribute::.ctor(System.String,System.Object,System.Boolean)
extern void HideIfAttribute__ctor_m0EF2AB456FB856870BCC79B310E92DB56FC7E3B9 (void);
// 0x000000B5 System.Void Sirenix.OdinInspector.HideInPlayModeAttribute::.ctor()
extern void HideInPlayModeAttribute__ctor_m3BCD0DEAA80C0647A75BDEEF33FAE9D6A4B506A7 (void);
// 0x000000B6 System.Void Sirenix.OdinInspector.HideInEditorModeAttribute::.ctor()
extern void HideInEditorModeAttribute__ctor_mD8E7DE60394D38D6BE9C83A41EF814D5CD92217D (void);
// 0x000000B7 System.Void Sirenix.OdinInspector.HideMonoScriptAttribute::.ctor()
extern void HideMonoScriptAttribute__ctor_mFAD292E42B94F432DE0379201BF2F0C7790B7768 (void);
// 0x000000B8 System.Void Sirenix.OdinInspector.HideReferenceObjectPickerAttribute::.ctor()
extern void HideReferenceObjectPickerAttribute__ctor_m30AC48FD898831668E93D8508CB750E1B3C7D415 (void);
// 0x000000B9 System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.String,System.Single,System.Int32,System.Int32,System.Single)
extern void HorizontalGroupAttribute__ctor_mD3D3178ECF3EC429A1C254046F0A5F589451C9C2 (void);
// 0x000000BA System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.Single,System.Int32,System.Int32,System.Single)
extern void HorizontalGroupAttribute__ctor_mDE8859B5F8E6ACA485847210F51CE37DDE2531A1 (void);
// 0x000000BB System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void HorizontalGroupAttribute_CombineValuesWith_m358F04BFE0348EA6FFE6FDF75342FAA1EB65014A (void);
// 0x000000BC System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::get_Expanded()
extern void InlineEditorAttribute_get_Expanded_m21A55631239C1383A3C08C4DC3C061807A6807EB (void);
// 0x000000BD System.Void Sirenix.OdinInspector.InlineEditorAttribute::set_Expanded(System.Boolean)
extern void InlineEditorAttribute_set_Expanded_mFB30F6438023DAD90B5290595289B5C854A788A4 (void);
// 0x000000BE System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::get_ExpandedHasValue()
extern void InlineEditorAttribute_get_ExpandedHasValue_mFC276BDF5631A5870038BB98C18386F949E01EDF (void);
// 0x000000BF System.Void Sirenix.OdinInspector.InlineEditorAttribute::set_ExpandedHasValue(System.Boolean)
extern void InlineEditorAttribute_set_ExpandedHasValue_m393D00D432705F319C5FCDF2B2571461B781D9C5 (void);
// 0x000000C0 System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorModes,Sirenix.OdinInspector.InlineEditorObjectFieldModes)
extern void InlineEditorAttribute__ctor_m2C122F43FF3CA6D75809623A950AAA6B2BC3939D (void);
// 0x000000C1 System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorObjectFieldModes)
extern void InlineEditorAttribute__ctor_mE7016B8D252EE799B6E68AB45CEEC1F5755E8509 (void);
// 0x000000C2 System.Void Sirenix.OdinInspector.SuppressInvalidAttributeErrorAttribute::.ctor()
extern void SuppressInvalidAttributeErrorAttribute__ctor_mFC93640117AD105D43241199CFF23B41EDAE3545 (void);
// 0x000000C3 System.Void Sirenix.OdinInspector.ShowDrawerChainAttribute::.ctor()
extern void ShowDrawerChainAttribute__ctor_mC28FA3B72111F46DFA3C395F1A4DD86EC43F187F (void);
// 0x000000C4 System.Void Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute::.ctor()
extern void ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m473F5CD90829FF1ADF2BB786906943CB8B80A453 (void);
// 0x000000C5 System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::get_Expanded()
extern void FoldoutGroupAttribute_get_Expanded_m688163003BD99BF4E49564C644275EB8911A236D (void);
// 0x000000C6 System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::set_Expanded(System.Boolean)
extern void FoldoutGroupAttribute_set_Expanded_m48641F7D01ECE8E41D43960FA1F3FD0CB43976B4 (void);
// 0x000000C7 System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::get_HasDefinedExpanded()
extern void FoldoutGroupAttribute_get_HasDefinedExpanded_m3926B57853EA8BB7ACCACCC6740073D46F0C373C (void);
// 0x000000C8 System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::set_HasDefinedExpanded(System.Boolean)
extern void FoldoutGroupAttribute_set_HasDefinedExpanded_mD585B02097F78E9049B2D144D50B503EF882B623 (void);
// 0x000000C9 System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::.ctor(System.String,System.Single)
extern void FoldoutGroupAttribute__ctor_m3806B43B9A2E11EC080F54305AC9F5F3E0CFD909 (void);
// 0x000000CA System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::.ctor(System.String,System.Boolean,System.Single)
extern void FoldoutGroupAttribute__ctor_m8CDE96FF16A2558CA79FDD44704591AE6F2FC2DF (void);
// 0x000000CB System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void FoldoutGroupAttribute_CombineValuesWith_m190C03737485283118946483BC9A40A3256FB6DB (void);
// 0x000000CC System.Void Sirenix.OdinInspector.GUIColorAttribute::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void GUIColorAttribute__ctor_mB266561305C367CE45651057D8D4DF36D99FCAD3 (void);
// 0x000000CD System.Void Sirenix.OdinInspector.GUIColorAttribute::.ctor(System.String)
extern void GUIColorAttribute__ctor_mEFBD166806FD6D2338758F5F368AFDF3E8347E91 (void);
// 0x000000CE System.Void Sirenix.OdinInspector.HideLabelAttribute::.ctor()
extern void HideLabelAttribute__ctor_mB86B50C32B17E33A0A6FAF5AEE634D05E65C10DF (void);
// 0x000000CF System.Void Sirenix.OdinInspector.IndentAttribute::.ctor(System.Int32)
extern void IndentAttribute__ctor_mDCB458272D695BD2953944B28BBC9F235F0AE6A9 (void);
// 0x000000D0 System.Void Sirenix.OdinInspector.InfoBoxAttribute::.ctor(System.String,Sirenix.OdinInspector.InfoMessageType,System.String)
extern void InfoBoxAttribute__ctor_m6BAFAA096773C816F00107526B2D96CAB28441DD (void);
// 0x000000D1 System.Void Sirenix.OdinInspector.InfoBoxAttribute::.ctor(System.String,System.String)
extern void InfoBoxAttribute__ctor_m0BC51BCFA7915BD71C9BFE2C30FD3A7C28ED423A (void);
// 0x000000D2 System.String Sirenix.OdinInspector.MinMaxSliderAttribute::get_MinMember()
extern void MinMaxSliderAttribute_get_MinMember_mE327F8D1415EAFEE019BCD018FBB4462DA236B46 (void);
// 0x000000D3 System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::set_MinMember(System.String)
extern void MinMaxSliderAttribute_set_MinMember_m5E26099BEC8726C15E46128BC6C2B57250FEEA71 (void);
// 0x000000D4 System.String Sirenix.OdinInspector.MinMaxSliderAttribute::get_MaxMember()
extern void MinMaxSliderAttribute_get_MaxMember_m29B06F8A83F2BF8A5B46753119CB2D502EDA3C41 (void);
// 0x000000D5 System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::set_MaxMember(System.String)
extern void MinMaxSliderAttribute_set_MaxMember_m6603B663E5F13F78DEFFC2978E69749C79714083 (void);
// 0x000000D6 System.String Sirenix.OdinInspector.MinMaxSliderAttribute::get_MinMaxMember()
extern void MinMaxSliderAttribute_get_MinMaxMember_m21E19607024528BA48821927215D387356BAB3ED (void);
// 0x000000D7 System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::set_MinMaxMember(System.String)
extern void MinMaxSliderAttribute_set_MinMaxMember_mB42BB3D7E48D263EA75CB69A6EE50CCEE2E5A066 (void);
// 0x000000D8 System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.Single,System.Single,System.Boolean)
extern void MinMaxSliderAttribute__ctor_m50B38305ECA370A12C393F649B02DE1B5ED6C1C8 (void);
// 0x000000D9 System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.Single,System.Boolean)
extern void MinMaxSliderAttribute__ctor_m23366474F31036F91A7B90ACC65892C056806C18 (void);
// 0x000000DA System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.Single,System.String,System.Boolean)
extern void MinMaxSliderAttribute__ctor_m94E10DD17D6C52516D7ED2FF4C3F9C3160582DF4 (void);
// 0x000000DB System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.String,System.Boolean)
extern void MinMaxSliderAttribute__ctor_m31DE6EEF1E8D9EAD8FA216FF3709F206091FE5C0 (void);
// 0x000000DC System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.Boolean)
extern void MinMaxSliderAttribute__ctor_m7FD79AD45A13B4E41D6D8094D863E51CB2234EAE (void);
// 0x000000DD System.Void Sirenix.OdinInspector.ToggleLeftAttribute::.ctor()
extern void ToggleLeftAttribute__ctor_mD9999F9B1B10790453F87DE1463164354765B695 (void);
// 0x000000DE System.Void Sirenix.OdinInspector.LabelTextAttribute::.ctor(System.String)
extern void LabelTextAttribute__ctor_mE3294102CD8D345ED7DC93D561D14F4611E7A06A (void);
// 0x000000DF System.Void Sirenix.OdinInspector.LabelTextAttribute::.ctor(System.String,System.Boolean)
extern void LabelTextAttribute__ctor_m3C6152F987F50040E983A67D681E8B5692C9AC18 (void);
// 0x000000E0 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowPaging()
extern void ListDrawerSettingsAttribute_get_ShowPaging_mF4F4A82094705EF34FC332B5034FF19BAA2780C3 (void);
// 0x000000E1 System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowPaging(System.Boolean)
extern void ListDrawerSettingsAttribute_set_ShowPaging_m7A1AA3022CDB3B19F854B95E437D35437C3ED7FA (void);
// 0x000000E2 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_DraggableItems()
extern void ListDrawerSettingsAttribute_get_DraggableItems_mC99F8EE2C8FEE3CA05418E4A27435C7BC1F4F9CE (void);
// 0x000000E3 System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_DraggableItems(System.Boolean)
extern void ListDrawerSettingsAttribute_set_DraggableItems_mCC2F079A739AC071CA9DB84E6E53ED450A82BB39 (void);
// 0x000000E4 System.Int32 Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_NumberOfItemsPerPage()
extern void ListDrawerSettingsAttribute_get_NumberOfItemsPerPage_mC88191D328036CC550D229C274C049E28EA874BA (void);
// 0x000000E5 System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_NumberOfItemsPerPage(System.Int32)
extern void ListDrawerSettingsAttribute_set_NumberOfItemsPerPage_m212DEF5B7C88C6C4903064572DA880D74241FDD4 (void);
// 0x000000E6 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_IsReadOnly()
extern void ListDrawerSettingsAttribute_get_IsReadOnly_mDA290460ED0767D4D0EBFA60F9B5C2E2862319EC (void);
// 0x000000E7 System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_IsReadOnly(System.Boolean)
extern void ListDrawerSettingsAttribute_set_IsReadOnly_mC95DBB25014CC6602621E836455FFFBD7F5DC510 (void);
// 0x000000E8 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowItemCount()
extern void ListDrawerSettingsAttribute_get_ShowItemCount_mB88A50379410C28ED127B58ADE328A3FDF3B446B (void);
// 0x000000E9 System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowItemCount(System.Boolean)
extern void ListDrawerSettingsAttribute_set_ShowItemCount_mF07F2DBCEFB8F88089CC817C5D72202B7E037C0D (void);
// 0x000000EA System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_Expanded()
extern void ListDrawerSettingsAttribute_get_Expanded_m918C4D4C6AFF9485D88F9CFD870C7216564430B9 (void);
// 0x000000EB System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_Expanded(System.Boolean)
extern void ListDrawerSettingsAttribute_set_Expanded_m54D4856BD28E0AF89BF6D075F91E484C08F6A5E2 (void);
// 0x000000EC System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowIndexLabels()
extern void ListDrawerSettingsAttribute_get_ShowIndexLabels_m631D9046E063074DB394730E852914C5710DFF42 (void);
// 0x000000ED System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowIndexLabels(System.Boolean)
extern void ListDrawerSettingsAttribute_set_ShowIndexLabels_mC054503B8FF36E2F6412CB89BC90D919A9E3ECD9 (void);
// 0x000000EE System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_OnTitleBarGUI()
extern void ListDrawerSettingsAttribute_get_OnTitleBarGUI_mB3DDDC9D3B34DE042FF321FF8025B91D90930299 (void);
// 0x000000EF System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_OnTitleBarGUI(System.String)
extern void ListDrawerSettingsAttribute_set_OnTitleBarGUI_m2CEC49FB7F6BF751A2172DD4CAA74027B27DEE22 (void);
// 0x000000F0 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_PagingHasValue()
extern void ListDrawerSettingsAttribute_get_PagingHasValue_m2324C4179F339EDD69C6F7B60FDF1C31F1EBCA68 (void);
// 0x000000F1 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowItemCountHasValue()
extern void ListDrawerSettingsAttribute_get_ShowItemCountHasValue_m8BC569A45F50C6162F694376D2351640D587BA19 (void);
// 0x000000F2 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_NumberOfItemsPerPageHasValue()
extern void ListDrawerSettingsAttribute_get_NumberOfItemsPerPageHasValue_m91E3D2F6E1370B5439C8A9AB50F1CE91B6AEB7D4 (void);
// 0x000000F3 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_DraggableHasValue()
extern void ListDrawerSettingsAttribute_get_DraggableHasValue_m6199435DD60BE83F70C8A062811F20CEF2C37829 (void);
// 0x000000F4 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_IsReadOnlyHasValue()
extern void ListDrawerSettingsAttribute_get_IsReadOnlyHasValue_mF19449F72F043259F929A4D24112000FAF8E85F6 (void);
// 0x000000F5 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ExpandedHasValue()
extern void ListDrawerSettingsAttribute_get_ExpandedHasValue_m89E37A4A7BF5206FBDB1D49CD0B3E0F131BA21EE (void);
// 0x000000F6 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowIndexLabelsHasValue()
extern void ListDrawerSettingsAttribute_get_ShowIndexLabelsHasValue_mE0FFC3BD473061E060471ECC90F5C7C19DD84056 (void);
// 0x000000F7 System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::.ctor()
extern void ListDrawerSettingsAttribute__ctor_m020F9F15913B88F8D060C4D5C1E16E18ECA1C619 (void);
// 0x000000F8 System.Void Sirenix.OdinInspector.MaxValueAttribute::.ctor(System.Double)
extern void MaxValueAttribute__ctor_mBC345D2BF67E140D3E243199AC7A970EF5AE8E4A (void);
// 0x000000F9 System.Void Sirenix.OdinInspector.MaxValueAttribute::.ctor(System.String)
extern void MaxValueAttribute__ctor_mDC89DCEF0E86A2873FE90B65B1B058AF3747B9CC (void);
// 0x000000FA System.Void Sirenix.OdinInspector.MinValueAttribute::.ctor(System.Double)
extern void MinValueAttribute__ctor_m9D54B01674E7C6523E8A20BCFC44B7E6ACE03C90 (void);
// 0x000000FB System.Void Sirenix.OdinInspector.MinValueAttribute::.ctor(System.String)
extern void MinValueAttribute__ctor_m95ECD927C12D1F8F493EDCDB5D20EB8D283AE242 (void);
// 0x000000FC System.Void Sirenix.OdinInspector.MultiLinePropertyAttribute::.ctor(System.Int32)
extern void MultiLinePropertyAttribute__ctor_mEE7A041CC0A205936F9DA935875F921CE7757D92 (void);
// 0x000000FD System.Void Sirenix.OdinInspector.PropertyTooltipAttribute::.ctor(System.String)
extern void PropertyTooltipAttribute__ctor_m61F2100A096BFDDBE09DE5319FFC27D4E5660EC5 (void);
// 0x000000FE System.Void Sirenix.OdinInspector.ReadOnlyAttribute::.ctor()
extern void ReadOnlyAttribute__ctor_m18A85A0BB01A704BC63961346547A03264778033 (void);
// 0x000000FF System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor()
extern void OnInspectorGUIAttribute__ctor_m8AE905E81618A1750D139BD7A885A4B7C131689E (void);
// 0x00000100 System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor(System.String,System.Boolean)
extern void OnInspectorGUIAttribute__ctor_m1B66AFEDEC6394DC72DFE0817165C151C878AAC3 (void);
// 0x00000101 System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor(System.String,System.String)
extern void OnInspectorGUIAttribute__ctor_mA650AA7C5E8F5D1B6E364A9991041E49F3C75730 (void);
// 0x00000102 System.String Sirenix.OdinInspector.OnValueChangedAttribute::get_MethodName()
extern void OnValueChangedAttribute_get_MethodName_m2C99633B759FE258D9C44EBE80F4439961D1F3DE (void);
// 0x00000103 System.Void Sirenix.OdinInspector.OnValueChangedAttribute::set_MethodName(System.String)
extern void OnValueChangedAttribute_set_MethodName_m4D68854AA62E2D4538912A5B1D94143C9F81D101 (void);
// 0x00000104 System.Void Sirenix.OdinInspector.OnValueChangedAttribute::.ctor(System.String,System.Boolean)
extern void OnValueChangedAttribute__ctor_mC9217ED5F89B0A1BF293A5E1FF0201746C05AE77 (void);
// 0x00000105 System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String,System.Single)
extern void PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A (void);
// 0x00000106 System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String)
extern void PropertyGroupAttribute__ctor_m8F7EF143FEDD12FAC7B22FBAC7BCB4885A09DF88 (void);
// 0x00000107 Sirenix.OdinInspector.PropertyGroupAttribute Sirenix.OdinInspector.PropertyGroupAttribute::Combine(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void PropertyGroupAttribute_Combine_mE33D3E9250CECE2DE657D70D5B9041720362481D (void);
// 0x00000108 System.Void Sirenix.OdinInspector.PropertyGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void PropertyGroupAttribute_CombineValuesWith_mA9D4370FD93B94EE22C250251D79E7C9ADD9E4D9 (void);
// 0x00000109 System.Void Sirenix.OdinInspector.PropertyOrderAttribute::.ctor()
extern void PropertyOrderAttribute__ctor_m0258452EBB74A30BCC712DBB88AA125607B703EA (void);
// 0x0000010A System.Void Sirenix.OdinInspector.PropertyOrderAttribute::.ctor(System.Single)
extern void PropertyOrderAttribute__ctor_mF84FACE1F0E9C501B91344E5D8ED99C41A49732D (void);
// 0x0000010B System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor()
extern void RequiredAttribute__ctor_mD0472E44DB48D0FAEA21E3727FB7D3C97278F14D (void);
// 0x0000010C System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(System.String,Sirenix.OdinInspector.InfoMessageType)
extern void RequiredAttribute__ctor_m7C9A7315A7C85A1389C15A35348A1C6A02F1E818 (void);
// 0x0000010D System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(System.String)
extern void RequiredAttribute__ctor_m356BF7D4C0E2F20CE57E27C5514E94C28C8A149E (void);
// 0x0000010E System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(Sirenix.OdinInspector.InfoMessageType)
extern void RequiredAttribute__ctor_mFED9DD0CBA9487878469BF33C8DF7D0A219B3CB2 (void);
// 0x0000010F System.Void Sirenix.OdinInspector.SceneObjectsOnlyAttribute::.ctor()
extern void SceneObjectsOnlyAttribute__ctor_mAF5D986331A81C1724B79E23ADEE745EB7EFD9E5 (void);
// 0x00000110 System.String Sirenix.OdinInspector.ValueDropdownAttribute::get_MemberName()
extern void ValueDropdownAttribute_get_MemberName_mDDB41A491519276523688B757548FF60EFA21540 (void);
// 0x00000111 System.Void Sirenix.OdinInspector.ValueDropdownAttribute::set_MemberName(System.String)
extern void ValueDropdownAttribute_set_MemberName_m68682707EC773A1CFE11732A80DC2F915DF0F983 (void);
// 0x00000112 System.Void Sirenix.OdinInspector.ValueDropdownAttribute::.ctor(System.String)
extern void ValueDropdownAttribute__ctor_mC8F5D3B5106B55FDAE79C0CCA48CE575C822184E (void);
// 0x00000113 System.String Sirenix.OdinInspector.IValueDropdownItem::GetText()
// 0x00000114 System.Object Sirenix.OdinInspector.IValueDropdownItem::GetValue()
// 0x00000115 System.Void Sirenix.OdinInspector.ValueDropdownList`1::Add(System.String,T)
// 0x00000116 System.Void Sirenix.OdinInspector.ValueDropdownList`1::Add(T)
// 0x00000117 System.Void Sirenix.OdinInspector.ValueDropdownList`1::.ctor()
// 0x00000118 System.Void Sirenix.OdinInspector.ValueDropdownItem::.ctor(System.String,System.Object)
extern void ValueDropdownItem__ctor_mE15D14B9282ADADDB949FB86CEA4A53335BF7AF2 (void);
// 0x00000119 System.String Sirenix.OdinInspector.ValueDropdownItem::ToString()
extern void ValueDropdownItem_ToString_m526C92A7FAA56983098238EEBAB0E726113A1A91 (void);
// 0x0000011A System.String Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetText()
extern void ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mB8AB4A9382BA84731B96E24AB59C475901FC15E3 (void);
// 0x0000011B System.Object Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetValue()
extern void ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m233A24A83540D6FBC8A5593E63BFDA1AA64391E1 (void);
// 0x0000011C System.Void Sirenix.OdinInspector.ValueDropdownItem`1::.ctor(System.String,T)
// 0x0000011D System.String Sirenix.OdinInspector.ValueDropdownItem`1::Sirenix.OdinInspector.IValueDropdownItem.GetText()
// 0x0000011E System.Object Sirenix.OdinInspector.ValueDropdownItem`1::Sirenix.OdinInspector.IValueDropdownItem.GetValue()
// 0x0000011F System.String Sirenix.OdinInspector.ValueDropdownItem`1::ToString()
// 0x00000120 System.Void Sirenix.OdinInspector.ShowInInspectorAttribute::.ctor()
extern void ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7 (void);
// 0x00000121 System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.Boolean,System.Single)
extern void TabGroupAttribute__ctor_m44DBA7BC5910270727E85756FB81AEDA69E277FE (void);
// 0x00000122 System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.String,System.Boolean,System.Single)
extern void TabGroupAttribute__ctor_m3FDCD07DF133DF94BC3015B62269140288334C53 (void);
// 0x00000123 System.Collections.Generic.List`1<System.String> Sirenix.OdinInspector.TabGroupAttribute::get_Tabs()
extern void TabGroupAttribute_get_Tabs_mE3395C3B6ACF15D12A58430AA25626B80AD24A4D (void);
// 0x00000124 System.Void Sirenix.OdinInspector.TabGroupAttribute::set_Tabs(System.Collections.Generic.List`1<System.String>)
extern void TabGroupAttribute_set_Tabs_m40A73E4EC6877859058F36C0D4CEA49C3F4ED537 (void);
// 0x00000125 System.Void Sirenix.OdinInspector.TabGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void TabGroupAttribute_CombineValuesWith_m03FB48456237C348D28C3AC330774077BC4AA84E (void);
// 0x00000126 System.Collections.Generic.IList`1<Sirenix.OdinInspector.PropertyGroupAttribute> Sirenix.OdinInspector.TabGroupAttribute::Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute.GetSubGroupAttributes()
extern void TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_GetSubGroupAttributes_m86FAA7D41551FA38170739A1C4375482F5F0EAF0 (void);
// 0x00000127 System.String Sirenix.OdinInspector.TabGroupAttribute::Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute.RepathMemberAttribute(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_RepathMemberAttribute_m85A61D8AD679C3E6A58CAED8A389F21E92B0CE1F (void);
// 0x00000128 System.Void Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute::.ctor(System.String,System.Single)
extern void TabSubGroupAttribute__ctor_m44ED5201B8DDB858B71AC573473AD2D269FEA381 (void);
// 0x00000129 System.Void Sirenix.OdinInspector.TitleAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.TitleAlignments,System.Boolean,System.Boolean)
extern void TitleAttribute__ctor_m77110B723E6BA8ED4C8EBC8948FC75001867ECD4 (void);
// 0x0000012A System.Void Sirenix.OdinInspector.ToggleAttribute::.ctor(System.String)
extern void ToggleAttribute__ctor_m6114BD54D92655906F8FDA3CAE94B5A456F965DF (void);
// 0x0000012B System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Single,System.String)
extern void ToggleGroupAttribute__ctor_m1E0CEDC90E7C6F85CEE5DBFDDEA3E22CA80321E6 (void);
// 0x0000012C System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.String)
extern void ToggleGroupAttribute__ctor_mC14797C18E0030E086743FE9C2369571298D941E (void);
// 0x0000012D System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Single,System.String,System.String)
extern void ToggleGroupAttribute__ctor_m823D2E960C00F760CEE66D90B5BB3935BFB04A77 (void);
// 0x0000012E System.String Sirenix.OdinInspector.ToggleGroupAttribute::get_ToggleMemberName()
extern void ToggleGroupAttribute_get_ToggleMemberName_m7117F9DDB7B23565ECF50BB6EC86223E6B896C28 (void);
// 0x0000012F System.String Sirenix.OdinInspector.ToggleGroupAttribute::get_TitleStringMemberName()
extern void ToggleGroupAttribute_get_TitleStringMemberName_mA5A6AD70D2A21733F25D5556553A7C0251AF6FAB (void);
// 0x00000130 System.Void Sirenix.OdinInspector.ToggleGroupAttribute::set_TitleStringMemberName(System.String)
extern void ToggleGroupAttribute_set_TitleStringMemberName_mD04BE047F8F988B70FC59476ED83C0E34D706C5C (void);
// 0x00000131 System.Void Sirenix.OdinInspector.ToggleGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void ToggleGroupAttribute_CombineValuesWith_m592F8AF767D119CBD03D064A90A1777E1A0F5C63 (void);
// 0x00000132 System.Boolean Sirenix.OdinInspector.ValidateInputAttribute::get_ContiniousValidationCheck()
extern void ValidateInputAttribute_get_ContiniousValidationCheck_m1C740C2A8CB957F6DC3BAB42D99A4D236AE3F4FE (void);
// 0x00000133 System.Void Sirenix.OdinInspector.ValidateInputAttribute::set_ContiniousValidationCheck(System.Boolean)
extern void ValidateInputAttribute_set_ContiniousValidationCheck_m7081AEBB91BEA03BC0AD1DD6DFD71B57FA882A19 (void);
// 0x00000134 System.Void Sirenix.OdinInspector.ValidateInputAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType)
extern void ValidateInputAttribute__ctor_m3FA8ADE2F0F3327705269A7DD59A90FA30A1FA72 (void);
// 0x00000135 System.Void Sirenix.OdinInspector.ValidateInputAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType,System.Boolean)
extern void ValidateInputAttribute__ctor_mC892E1A76D31818FF5E84A156F41B471F62FFB2C (void);
// 0x00000136 System.String Sirenix.OdinInspector.ShowIfAttribute::get_MemberName()
extern void ShowIfAttribute_get_MemberName_mDF9FD77EE8556F9FC0BE36A70DE6304D8E5A4F29 (void);
// 0x00000137 System.Void Sirenix.OdinInspector.ShowIfAttribute::set_MemberName(System.String)
extern void ShowIfAttribute_set_MemberName_mB198C1AA69C874A57B784B11303A4B059C9A981F (void);
// 0x00000138 System.Void Sirenix.OdinInspector.ShowIfAttribute::.ctor(System.String,System.Boolean)
extern void ShowIfAttribute__ctor_m6B88C445A0D6989BFFFF6826FA8EF58611EF84DF (void);
// 0x00000139 System.Void Sirenix.OdinInspector.ShowIfAttribute::.ctor(System.String,System.Object,System.Boolean)
extern void ShowIfAttribute__ctor_m2939A8F0ED2730D238E2CCE4844FE0B697B3F1D6 (void);
// 0x0000013A System.Void Sirenix.OdinInspector.WrapAttribute::.ctor(System.Double,System.Double)
extern void WrapAttribute__ctor_mBB5B66CD25B9F4AEFA2074CD55D05A6DA788CC77 (void);
// 0x0000013B System.Collections.Generic.IList`1<Sirenix.OdinInspector.PropertyGroupAttribute> Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute::GetSubGroupAttributes()
// 0x0000013C System.String Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute::RepathMemberAttribute(Sirenix.OdinInspector.PropertyGroupAttribute)
static Il2CppMethodPointer s_methodPointers[316] = 
{
	AssetListAttribute__ctor_m8247558D8C9BBEA29FA028089F7A0D7D9AC5A5DC,
	AssetSelectorAttribute_set_Paths_m76AA7C29F24AED85D381A5DDA6C2E7008BFBB2FF,
	AssetSelectorAttribute_get_Paths_mEC8A7AD9115F4A88436030423B8D0D3947E584CA,
	AssetSelectorAttribute__ctor_mD7B3574705DCEC423BD460B9A92123C8A2160A26,
	U3CU3Ec__cctor_m898AC2ABD1D3662F4F6DAC7C5C5E904834908795,
	U3CU3Ec__ctor_m8030917C27454A00E147CCBAAB8795473574751B,
	U3CU3Ec_U3Cset_PathsU3Eb__12_0_m8675AB5D0D72FC737F62615297D2D030A0DC8AB1,
	AssetsOnlyAttribute__ctor_m77053C898FD25996672ADF31626685B35BF1F79C,
	BoxGroupAttribute__ctor_m51F35E9FAAE6A6993CECBB636458130D35C88CC0,
	BoxGroupAttribute__ctor_m3735DB49E46EE2E2AE8AE5415D6C9FA0A93233F2,
	BoxGroupAttribute_CombineValuesWith_m79BABBFF9A0B58F8A15369C25F243BFA76128A96,
	ButtonAttribute_set_DrawResult_mAD70CD6C9145A69B55D15FEFF29A6BA72CCEA8F3,
	ButtonAttribute_get_DrawResult_m7F1681E70FF12206C2D4A94195826002882C53BB,
	ButtonAttribute_get_DrawResultIsSet_m708AD98B58E8B96D9FEE46B25439DB6077CA2A7A,
	ButtonAttribute__ctor_mF27CD9C19F032DF07E9293DD310F5BE840ED26F8,
	ButtonAttribute__ctor_mF71AB4EC269B5BD92CA84ED615BDBDF75CCB3111,
	ButtonAttribute__ctor_m0879084C1E2C8C02C21A54F97EC16C7B54483B53,
	ButtonAttribute__ctor_m70C961C47AD907A198820FF74C6DD81A5427B93A,
	ButtonAttribute__ctor_mB526E46E3BE5BF1024BB81FF1954A7454BA10E32,
	ButtonAttribute__ctor_m74C2A9439E312E6658C20984D3BBE10CBB8D941A,
	ButtonAttribute__ctor_mCB7708CF62C859C98DB8112D7D7877EDA2E5898D,
	ButtonAttribute__ctor_m17A5EAF23490CFFF3D56F7A8FD1AA7E1ED3BCA9C,
	ButtonAttribute__ctor_mC8501A7A6F000FBAE5B34D71C67AEFE88E2724D9,
	ButtonAttribute__ctor_m98049103344F9DE50470BB6C38AF2EA19F590A3F,
	ButtonAttribute__ctor_m75DACD3BA27E65DB9A96FD5F64A3DD9A0087A15D,
	ButtonAttribute__ctor_mFD9745232184D70BC605975B1817E57DFBF4FD59,
	ButtonGroupAttribute__ctor_m0054EA0E67B1D3755C80EB056F5EBD30F6F19200,
	ChildGameObjectsOnlyAttribute__ctor_m72F82790CBA3BE06BF8A5019D17420040B0C9AC9,
	CustomValueDrawerAttribute_get_MethodName_mA5767EFD5CF8207C1BE229FD422FC3FC44230CE4,
	CustomValueDrawerAttribute_set_MethodName_mF28068EC65118ECA2A1B38558EC857BD89693878,
	CustomValueDrawerAttribute__ctor_m7B3ECD14426CFC6B76E7239C23B476F684779AE5,
	DisableInInlineEditorsAttribute__ctor_mF8B7E6813BD5C9AFBD78A5C49721EB37D56C6560,
	DisableInNonPrefabsAttribute__ctor_m63D4B41F0E0A99B087DCF8B11E5DBBC427F6451A,
	DisableInPrefabAssetsAttribute__ctor_m011CA2F9971FA999EF734041BAB91872317B359B,
	DisableInPrefabInstancesAttribute__ctor_m5E47228B190D987477F41C86ECB66484007FD5EC,
	DisableInPrefabsAttribute__ctor_m4E3DA39C7874E60D102C85506B59280968346E3B,
	DoNotDrawAsReferenceAttribute__ctor_m09DF620481A8C06F01488885A40FA3B7749ACB11,
	EnableGUIAttribute__ctor_mCC16666621929D16DCBB1A8B4F900EC44EDC9A35,
	EnumPagingAttribute__ctor_m749BBBC6A2FC95712729B4DE19C72B8849102F9E,
	FilePathAttribute_get_ReadOnly_mE6D353FDC464B3A9EFF55EF78C02C575E41E6C7E,
	FilePathAttribute_set_ReadOnly_m221DD6A033004FB5626EFF6FA055CADE2B60E780,
	FilePathAttribute__ctor_m141C277EA439B817D75D6A4378EEBBC854B30DC3,
	FolderPathAttribute__ctor_m86B2038DF79EA2F7847EAAA1CC2344A740D8C965,
	HideDuplicateReferenceBoxAttribute__ctor_m9003D334EB4C6B39944428C496CA8A0192F04177,
	HideIfGroupAttribute_get_Animate_mF5AA29F3C90F552A9998C8776912B737D66C4DE3,
	HideIfGroupAttribute_set_Animate_m4D3CD713242CB20F8E4109EB386B49F2A1231305,
	HideIfGroupAttribute_get_MemberName_mFEA756BD4855C66E710EE842D93D08B9755EA461,
	HideIfGroupAttribute_set_MemberName_m982CF7DDCA552327AE01B1D09E75DF5DD2D5CF27,
	HideIfGroupAttribute_get_Condition_mE6857BF4B30ABAC93E337FB918D2CF71AD0B0631,
	HideIfGroupAttribute_set_Condition_m8FB8D4239B1C0E2A8C112A7AE01B5CD4FF400DDA,
	HideIfGroupAttribute__ctor_m4906C608EE64D1E886FA86D29A8DCB4CCD197FD1,
	HideIfGroupAttribute__ctor_m3267514613583C7F46E2568CCF1B657EB1A27474,
	HideIfGroupAttribute_CombineValuesWith_m75EBFB293E2957D9B6F2C26E79950B90E6ECCBC5,
	HideInInlineEditorsAttribute__ctor_m78318BFF82C4AB6286362D19382FC488B157C37A,
	HideInNonPrefabsAttribute__ctor_m38C345ABE1AD02E5CE773566C53D5628C23D59E0,
	HideInPrefabAssetsAttribute__ctor_m56DF3C5E0803931801A0818BFF80482ECBE185E0,
	HideInPrefabInstancesAttribute__ctor_m9DCF14D9FC674E03B655300CAAC03356DA89E6C8,
	HideInPrefabsAttribute__ctor_mB976FD8E6F61588DF5FEAAEDB2D84FE377F291BC,
	HideInTablesAttribute__ctor_m9082E7F39E533A3F2279713ED33C8D48ACBACFF8,
	HideNetworkBehaviourFieldsAttribute__ctor_mD80534B899DAA097D8011A5500EA103C30028950,
	InlinePropertyAttribute__ctor_m38FC46B53AAC32D93B97537E0922EB88CDE2675C,
	LabelWidthAttribute__ctor_m7DCB42E4880AD8CDE03563924EB5AF64BB933BE8,
	OnCollectionChangedAttribute__ctor_m982A4122FCD2196709866CB34C8FF8106ACE3A79,
	OnCollectionChangedAttribute__ctor_m343721F74CD7C8FBC42BD3EED73AFB991FDC653E,
	OnCollectionChangedAttribute__ctor_m6DB1F00EBB85AE6A0511B8FD9650A2495E8E1C91,
	OnInspectorDisposeAttribute__ctor_mAD2279BE383A0423F12EF39C4D421C6C3D5ECB08,
	OnInspectorDisposeAttribute__ctor_mE2255EE8D89336EE7BEBBC5EE596101C2A92778E,
	OnInspectorInitAttribute__ctor_m8D853E5FF4A7439A5BA4CA59234D58BF7ED76EF2,
	OnInspectorInitAttribute__ctor_m74CDD7E081A0A0DF8D451390FBEDD2C7AEEEF373,
	OnStateUpdateAttribute__ctor_mE21A3A0C3A41C2FA529B2E5D2EDE202F8CBDB303,
	PreviewFieldAttribute_get_Alignment_mDBED00E78169E0B356F16A8759FF07D67F5974B5,
	PreviewFieldAttribute_set_Alignment_mC639ECEAA0F283F44483506E821E205D611634BA,
	PreviewFieldAttribute_get_AlignmentHasValue_mE470BFDF84F8A6C264F3DC2A4414FF82B3939B92,
	PreviewFieldAttribute__ctor_mF4E39532FED54856F331B95BB4BEA2F7022A6BF5,
	PreviewFieldAttribute__ctor_m73A84428DFFD8A8F6AF80475138256C119CDB6E0,
	PreviewFieldAttribute__ctor_m9CB7BB82E1251D2398470AF788A2017DE9E98EFA,
	PreviewFieldAttribute__ctor_m42F6284469DF8F7BB78B4DE69BF2BB32EB5FE534,
	ProgressBarAttribute_get_MinMember_m2CC1ED194ED7A3009E79B1C07A0DE4A39E187ADD,
	ProgressBarAttribute_set_MinMember_m9A6C0D5E9AE335F63782BEC288D6E17C39178581,
	ProgressBarAttribute_get_MaxMember_m8B59F7BB73FF537EF9292E1C82690A779221C737,
	ProgressBarAttribute_set_MaxMember_mA6E304FA078A043078F079FD1C05822AEE97D75E,
	ProgressBarAttribute_get_ColorMember_m8BBC82073E0759F54EDA83E001F685C1DDF685FC,
	ProgressBarAttribute_set_ColorMember_m9359E4B9A3400F7553BFB3D00D56B4943B603266,
	ProgressBarAttribute_get_BackgroundColorMember_m70AC774398545E0FB91CE18331214809C968AFBF,
	ProgressBarAttribute_set_BackgroundColorMember_mE873BB83D0F4A0918F2C3AF2494C153E094F5AF4,
	ProgressBarAttribute_get_CustomValueStringMember_m355C89D38750B48F2088523A21E4A3602647F6DE,
	ProgressBarAttribute_set_CustomValueStringMember_mD3B3908B9FAA46A37B0A74137E96D01399AEB815,
	ProgressBarAttribute__ctor_m1AA280ED357596B2D727C77828E5E7135150CC2C,
	ProgressBarAttribute__ctor_m3EBD7FE0C0BE330BB9420358ACD8BF7309F450EB,
	ProgressBarAttribute__ctor_m05442A34E95B454A385A128774C4E57CBE80ACF4,
	ProgressBarAttribute__ctor_m4247618AFE4481DE37C0DFAD7801E8F26BA69B99,
	ProgressBarAttribute_get_DrawValueLabel_mC0D88C51C77D902A740C75949A6FA5A8553FBFFB,
	ProgressBarAttribute_set_DrawValueLabel_mC55B7F41936E19227A24A8F109FCAC95BB9A068E,
	ProgressBarAttribute_get_DrawValueLabelHasValue_m02DBBB3955C49F325C00C0F297DAB10A808AEE15,
	ProgressBarAttribute_set_DrawValueLabelHasValue_mFE21A81AACD29E509534804C64D7092FC9EC971D,
	ProgressBarAttribute_get_ValueLabelAlignment_mF5ECF715BE3EE10695716E7CB25DBBC2E16E7B0A,
	ProgressBarAttribute_set_ValueLabelAlignment_m35FB4443DEB147A679B29DD278AFBFD3D8244A2B,
	ProgressBarAttribute_get_ValueLabelAlignmentHasValue_mFD655331D63227292454EB3995BB9B527D642893,
	ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m2E49A4D0090B1D60036AB25BA0ABAA025D3F25C5,
	ProgressBarAttribute_get_Color_mD07AE72BEAEFE6EA3175131A3BCF78C4A90516DB,
	PropertyRangeAttribute_get_MinMember_m1AE82B2EF2CF8ED9A2180464F63085D1C92ACC32,
	PropertyRangeAttribute_set_MinMember_mF0665322F7B5EB30944C1468DE272546D38B1BE7,
	PropertyRangeAttribute_get_MaxMember_mBAEEA015901D53E5100A7ACC72DF6A66C3845CC4,
	PropertyRangeAttribute_set_MaxMember_m91ED22ABEFFEC257A3E8F2AB7EAF91EDA4E0ADBF,
	PropertyRangeAttribute__ctor_m46E3E82518F3BD0221513B22406BE53F83109311,
	PropertyRangeAttribute__ctor_m69B10A0AF618ADABAE4384D7EF88D30E339AE6DF,
	PropertyRangeAttribute__ctor_m0F5C052C678C9F88EC421C180711890168F82E4B,
	PropertyRangeAttribute__ctor_mB000DC0552D3FADF73419B765DCAFE5B7ED660C6,
	PropertySpaceAttribute__ctor_m776D7EF54F7BA3A1C86CD2BBBFFA9A466CA4B11F,
	PropertySpaceAttribute__ctor_m767AFE53798D1B36405151C25F85EA97292F33C9,
	PropertySpaceAttribute__ctor_m84DFA94A5E36256F8A3A7D88195F2056226A5B23,
	ResponsiveButtonGroupAttribute__ctor_m21EF1D295EFCBD00BBF2E6EA35FC10C0786AE86F,
	ResponsiveButtonGroupAttribute_CombineValuesWith_m0C46A0FFC4BF1DC38EA47EC0D6E68DAB2FD3EDA4,
	ShowIfGroupAttribute_get_Animate_mA28D4E294DB07B30D7F199DB20F66A8132674DDC,
	ShowIfGroupAttribute_set_Animate_m0C5232E32FBF5570CA9EBF341F4A9A26383C87CD,
	ShowIfGroupAttribute_get_MemberName_m2EA81E787AF8407FFA0C215D4780EC4FDB75B322,
	ShowIfGroupAttribute_set_MemberName_m4FFBB055EEEBA05AE6CF9183D34041B7EEB62A32,
	ShowIfGroupAttribute_get_Condition_mBAA18A45D8F96BC8D5E46B3C141673EF52A50E46,
	ShowIfGroupAttribute_set_Condition_m7CAF3CF1D9803D698E9011B30EC9EA5536EB8FA9,
	ShowIfGroupAttribute__ctor_mD113EDB716F28754313E2BEB0927B6043981329D,
	ShowIfGroupAttribute__ctor_mF398B06CE4C1F42A320290017D7296BAAEA7A312,
	ShowIfGroupAttribute_CombineValuesWith_m4D6EAA8F018088F641E92D46A9DD2919BF79CDE0,
	ShowInInlineEditorsAttribute__ctor_m7A762533C1DDCCEB86F424F6FD7CAD4F2CDACF1B,
	ShowPropertyResolverAttribute__ctor_mABC8EECEE1320586632257A6237B96F8CF857F4B,
	SuffixLabelAttribute__ctor_mABCF82E4DA146381809AAD4677A35A363CEDF858,
	TableColumnWidthAttribute__ctor_m19093F5CE091085A12F2DFEE1A59E362E833C50C,
	TableListAttribute_get_ShowPaging_mAA4CBA1D36E6E33AD266462C1E59D2422E762EE4,
	TableListAttribute_set_ShowPaging_m70ED81A12B8FBC10D953B5F449CCCECEAA0437CA,
	TableListAttribute_get_ShowPagingHasValue_m8E2C05085222E55DECF631D88E8D5B7CFC235940,
	TableListAttribute_get_ScrollViewHeight_m8CD7BCAFCF875A2EEDC3B77D27234D748AB0DAAF,
	TableListAttribute_set_ScrollViewHeight_m28162A9FBF938888F1993A403454A9342ACAE41C,
	TableListAttribute__ctor_m74F94D599C39706135C99A2FEC54F490C1541078,
	TableMatrixAttribute__ctor_m5A782F83D5669B3E8BED3227A6A019F04DB88983,
	TypeFilterAttribute_get_MemberName_m499B40CD5A86143CEE9AFACCA276449001544A67,
	TypeFilterAttribute_set_MemberName_m03F6F8703319609E1AFDB7C484DB0567EFC39C1C,
	TypeFilterAttribute__ctor_m07F0EF43034B8C390DC753136654962CCCF80061,
	TypeInfoBoxAttribute__ctor_m51D2E2C1E51F7110C5AE9124C84E4FD6C655695D,
	VerticalGroupAttribute__ctor_mD788EEA9634514D13E321236E9BC3A18186E4DA0,
	VerticalGroupAttribute__ctor_m655FA45ED58C1BA24F2011314508B7E40FFC69CB,
	VerticalGroupAttribute_CombineValuesWith_m983E6DD2C5697D4CCFFA5F58C29C6F93828F06BE,
	SearchableAttribute__ctor_m74A25AAA65FD0FB6893A616BCE3067BFF900AE5C,
	IncludeMyAttributesAttribute__ctor_mD6627C9E1A06B23183641DFAD937CBD15577328F,
	NULL,
	OdinRegisterAttributeAttribute__ctor_m6260347940664F402312B72B951F4072A5CBDCFB,
	OdinRegisterAttributeAttribute__ctor_m305FAA270A22E7BB4406E17C03ADA1830E174B28,
	TitleGroupAttribute__ctor_mBBBF50005F89D5F5510014B9B5FBDFAB3934B80D,
	TitleGroupAttribute_CombineValuesWith_mD450E1372EF1FD6291F2E9150EF83E074847A679,
	ColorPaletteAttribute__ctor_m0FB1C53EA41531CAF7CDF5F24930769F245E154C,
	ColorPaletteAttribute__ctor_m781B42126F89B6F56DE709163D0A4D74E5A9BCBE,
	CustomContextMenuAttribute_get_MethodName_m7D7FC1ECD9F4B0434E85FA639C4B3520C5A0A9A3,
	CustomContextMenuAttribute_set_MethodName_m0DACAC5D5FBB0CCB210649D026953C4907769B4C,
	CustomContextMenuAttribute__ctor_mC29FF6E64432B517E79697D8D17715B3F06A0C00,
	DelayedPropertyAttribute__ctor_m489FF80D8A9B462702E606A4DF2BE009D99FF849,
	DetailedInfoBoxAttribute__ctor_m145816EE77ED956C065B36607C1DAEA8FA5BAA3C,
	DictionaryDrawerSettings__ctor_mCD32097B13EC46AEFE9ED48441540D7C131140D5,
	DisableContextMenuAttribute__ctor_m3528AA0C5A479BE6ABAF1F4495722FB590D028C2,
	DisableIfAttribute_get_MemberName_m1D81D1FA3D91512BB919424574C0EC98F8A0BBCD,
	DisableIfAttribute_set_MemberName_mFEC9F57C525825A0831C81208ED093901CBAD7A7,
	DisableIfAttribute__ctor_m461AF14E80789329EF1019B33DE25D56CF6CBD18,
	DisableIfAttribute__ctor_mFC765F2149E0A3CCFEA59CD75B2276B87314B20F,
	DisableInEditorModeAttribute__ctor_m13F7D48945BD6670A28279DDB616078B9B2F43C6,
	DisableInPlayModeAttribute__ctor_m97B7FB5CCD60F02CEF1099B6743878DA3615A774,
	DisplayAsStringAttribute__ctor_m6B84964662A8187BE17BD6334934BA2B409A2ACC,
	DisplayAsStringAttribute__ctor_mFD5330EA486F7C1876B7DAD38322173D33A51D7C,
	DontApplyToListElementsAttribute__ctor_m39E251F2A1BA63B4A48895001AC81CD035D6E113,
	DrawWithUnityAttribute__ctor_m255ED9DDDBD0EEA71807B8C6BB304604F1B020A5,
	InlineButtonAttribute_get_MemberMethod_mAD5C9BAF96545FE2DB4034B407E97847E5E0EDF3,
	InlineButtonAttribute_set_MemberMethod_mCFD34ED6D9B04F9C4112B91AEA84B7546B4B8D24,
	InlineButtonAttribute__ctor_m01CB0BA9AF4B90E349CDAF936BB0BD9EDA76C512,
	ShowForPrefabOnlyAttribute__ctor_m243ED5ABA1FDC9E3FED7650C390750C02C75518A,
	EnableForPrefabOnlyAttribute__ctor_m2C520605A683345636EF23DBC8A03AC0E6D2FDAF,
	EnableIfAttribute_get_MemberName_mD2198B861D98311267B4856B0A450CB382C3BC42,
	EnableIfAttribute_set_MemberName_m5E4EC1A7D06273D9BBA3305FF49DA57009F1123D,
	EnableIfAttribute__ctor_m1F1F838D286FAE9370AD489B884E42E1B73AF976,
	EnableIfAttribute__ctor_m3C1429D1927B76D03B47D67EF295DC6C0614CE39,
	EnumToggleButtonsAttribute__ctor_mE37FDB25B17CE53DA754A3E0F830F396318F34F5,
	HideIfAttribute_get_MemberName_m5C0C4FB895E1A3788F62DDE0A0680920EE3BD8F1,
	HideIfAttribute_set_MemberName_m475241B5D603C50C78DDED174F3DBE823F7A871C,
	HideIfAttribute__ctor_m6DB6CACEF83AE294D4888CC58DA860DC785E0D01,
	HideIfAttribute__ctor_m0EF2AB456FB856870BCC79B310E92DB56FC7E3B9,
	HideInPlayModeAttribute__ctor_m3BCD0DEAA80C0647A75BDEEF33FAE9D6A4B506A7,
	HideInEditorModeAttribute__ctor_mD8E7DE60394D38D6BE9C83A41EF814D5CD92217D,
	HideMonoScriptAttribute__ctor_mFAD292E42B94F432DE0379201BF2F0C7790B7768,
	HideReferenceObjectPickerAttribute__ctor_m30AC48FD898831668E93D8508CB750E1B3C7D415,
	HorizontalGroupAttribute__ctor_mD3D3178ECF3EC429A1C254046F0A5F589451C9C2,
	HorizontalGroupAttribute__ctor_mDE8859B5F8E6ACA485847210F51CE37DDE2531A1,
	HorizontalGroupAttribute_CombineValuesWith_m358F04BFE0348EA6FFE6FDF75342FAA1EB65014A,
	InlineEditorAttribute_get_Expanded_m21A55631239C1383A3C08C4DC3C061807A6807EB,
	InlineEditorAttribute_set_Expanded_mFB30F6438023DAD90B5290595289B5C854A788A4,
	InlineEditorAttribute_get_ExpandedHasValue_mFC276BDF5631A5870038BB98C18386F949E01EDF,
	InlineEditorAttribute_set_ExpandedHasValue_m393D00D432705F319C5FCDF2B2571461B781D9C5,
	InlineEditorAttribute__ctor_m2C122F43FF3CA6D75809623A950AAA6B2BC3939D,
	InlineEditorAttribute__ctor_mE7016B8D252EE799B6E68AB45CEEC1F5755E8509,
	SuppressInvalidAttributeErrorAttribute__ctor_mFC93640117AD105D43241199CFF23B41EDAE3545,
	ShowDrawerChainAttribute__ctor_mC28FA3B72111F46DFA3C395F1A4DD86EC43F187F,
	ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m473F5CD90829FF1ADF2BB786906943CB8B80A453,
	FoldoutGroupAttribute_get_Expanded_m688163003BD99BF4E49564C644275EB8911A236D,
	FoldoutGroupAttribute_set_Expanded_m48641F7D01ECE8E41D43960FA1F3FD0CB43976B4,
	FoldoutGroupAttribute_get_HasDefinedExpanded_m3926B57853EA8BB7ACCACCC6740073D46F0C373C,
	FoldoutGroupAttribute_set_HasDefinedExpanded_mD585B02097F78E9049B2D144D50B503EF882B623,
	FoldoutGroupAttribute__ctor_m3806B43B9A2E11EC080F54305AC9F5F3E0CFD909,
	FoldoutGroupAttribute__ctor_m8CDE96FF16A2558CA79FDD44704591AE6F2FC2DF,
	FoldoutGroupAttribute_CombineValuesWith_m190C03737485283118946483BC9A40A3256FB6DB,
	GUIColorAttribute__ctor_mB266561305C367CE45651057D8D4DF36D99FCAD3,
	GUIColorAttribute__ctor_mEFBD166806FD6D2338758F5F368AFDF3E8347E91,
	HideLabelAttribute__ctor_mB86B50C32B17E33A0A6FAF5AEE634D05E65C10DF,
	IndentAttribute__ctor_mDCB458272D695BD2953944B28BBC9F235F0AE6A9,
	InfoBoxAttribute__ctor_m6BAFAA096773C816F00107526B2D96CAB28441DD,
	InfoBoxAttribute__ctor_m0BC51BCFA7915BD71C9BFE2C30FD3A7C28ED423A,
	MinMaxSliderAttribute_get_MinMember_mE327F8D1415EAFEE019BCD018FBB4462DA236B46,
	MinMaxSliderAttribute_set_MinMember_m5E26099BEC8726C15E46128BC6C2B57250FEEA71,
	MinMaxSliderAttribute_get_MaxMember_m29B06F8A83F2BF8A5B46753119CB2D502EDA3C41,
	MinMaxSliderAttribute_set_MaxMember_m6603B663E5F13F78DEFFC2978E69749C79714083,
	MinMaxSliderAttribute_get_MinMaxMember_m21E19607024528BA48821927215D387356BAB3ED,
	MinMaxSliderAttribute_set_MinMaxMember_mB42BB3D7E48D263EA75CB69A6EE50CCEE2E5A066,
	MinMaxSliderAttribute__ctor_m50B38305ECA370A12C393F649B02DE1B5ED6C1C8,
	MinMaxSliderAttribute__ctor_m23366474F31036F91A7B90ACC65892C056806C18,
	MinMaxSliderAttribute__ctor_m94E10DD17D6C52516D7ED2FF4C3F9C3160582DF4,
	MinMaxSliderAttribute__ctor_m31DE6EEF1E8D9EAD8FA216FF3709F206091FE5C0,
	MinMaxSliderAttribute__ctor_m7FD79AD45A13B4E41D6D8094D863E51CB2234EAE,
	ToggleLeftAttribute__ctor_mD9999F9B1B10790453F87DE1463164354765B695,
	LabelTextAttribute__ctor_mE3294102CD8D345ED7DC93D561D14F4611E7A06A,
	LabelTextAttribute__ctor_m3C6152F987F50040E983A67D681E8B5692C9AC18,
	ListDrawerSettingsAttribute_get_ShowPaging_mF4F4A82094705EF34FC332B5034FF19BAA2780C3,
	ListDrawerSettingsAttribute_set_ShowPaging_m7A1AA3022CDB3B19F854B95E437D35437C3ED7FA,
	ListDrawerSettingsAttribute_get_DraggableItems_mC99F8EE2C8FEE3CA05418E4A27435C7BC1F4F9CE,
	ListDrawerSettingsAttribute_set_DraggableItems_mCC2F079A739AC071CA9DB84E6E53ED450A82BB39,
	ListDrawerSettingsAttribute_get_NumberOfItemsPerPage_mC88191D328036CC550D229C274C049E28EA874BA,
	ListDrawerSettingsAttribute_set_NumberOfItemsPerPage_m212DEF5B7C88C6C4903064572DA880D74241FDD4,
	ListDrawerSettingsAttribute_get_IsReadOnly_mDA290460ED0767D4D0EBFA60F9B5C2E2862319EC,
	ListDrawerSettingsAttribute_set_IsReadOnly_mC95DBB25014CC6602621E836455FFFBD7F5DC510,
	ListDrawerSettingsAttribute_get_ShowItemCount_mB88A50379410C28ED127B58ADE328A3FDF3B446B,
	ListDrawerSettingsAttribute_set_ShowItemCount_mF07F2DBCEFB8F88089CC817C5D72202B7E037C0D,
	ListDrawerSettingsAttribute_get_Expanded_m918C4D4C6AFF9485D88F9CFD870C7216564430B9,
	ListDrawerSettingsAttribute_set_Expanded_m54D4856BD28E0AF89BF6D075F91E484C08F6A5E2,
	ListDrawerSettingsAttribute_get_ShowIndexLabels_m631D9046E063074DB394730E852914C5710DFF42,
	ListDrawerSettingsAttribute_set_ShowIndexLabels_mC054503B8FF36E2F6412CB89BC90D919A9E3ECD9,
	ListDrawerSettingsAttribute_get_OnTitleBarGUI_mB3DDDC9D3B34DE042FF321FF8025B91D90930299,
	ListDrawerSettingsAttribute_set_OnTitleBarGUI_m2CEC49FB7F6BF751A2172DD4CAA74027B27DEE22,
	ListDrawerSettingsAttribute_get_PagingHasValue_m2324C4179F339EDD69C6F7B60FDF1C31F1EBCA68,
	ListDrawerSettingsAttribute_get_ShowItemCountHasValue_m8BC569A45F50C6162F694376D2351640D587BA19,
	ListDrawerSettingsAttribute_get_NumberOfItemsPerPageHasValue_m91E3D2F6E1370B5439C8A9AB50F1CE91B6AEB7D4,
	ListDrawerSettingsAttribute_get_DraggableHasValue_m6199435DD60BE83F70C8A062811F20CEF2C37829,
	ListDrawerSettingsAttribute_get_IsReadOnlyHasValue_mF19449F72F043259F929A4D24112000FAF8E85F6,
	ListDrawerSettingsAttribute_get_ExpandedHasValue_m89E37A4A7BF5206FBDB1D49CD0B3E0F131BA21EE,
	ListDrawerSettingsAttribute_get_ShowIndexLabelsHasValue_mE0FFC3BD473061E060471ECC90F5C7C19DD84056,
	ListDrawerSettingsAttribute__ctor_m020F9F15913B88F8D060C4D5C1E16E18ECA1C619,
	MaxValueAttribute__ctor_mBC345D2BF67E140D3E243199AC7A970EF5AE8E4A,
	MaxValueAttribute__ctor_mDC89DCEF0E86A2873FE90B65B1B058AF3747B9CC,
	MinValueAttribute__ctor_m9D54B01674E7C6523E8A20BCFC44B7E6ACE03C90,
	MinValueAttribute__ctor_m95ECD927C12D1F8F493EDCDB5D20EB8D283AE242,
	MultiLinePropertyAttribute__ctor_mEE7A041CC0A205936F9DA935875F921CE7757D92,
	PropertyTooltipAttribute__ctor_m61F2100A096BFDDBE09DE5319FFC27D4E5660EC5,
	ReadOnlyAttribute__ctor_m18A85A0BB01A704BC63961346547A03264778033,
	OnInspectorGUIAttribute__ctor_m8AE905E81618A1750D139BD7A885A4B7C131689E,
	OnInspectorGUIAttribute__ctor_m1B66AFEDEC6394DC72DFE0817165C151C878AAC3,
	OnInspectorGUIAttribute__ctor_mA650AA7C5E8F5D1B6E364A9991041E49F3C75730,
	OnValueChangedAttribute_get_MethodName_m2C99633B759FE258D9C44EBE80F4439961D1F3DE,
	OnValueChangedAttribute_set_MethodName_m4D68854AA62E2D4538912A5B1D94143C9F81D101,
	OnValueChangedAttribute__ctor_mC9217ED5F89B0A1BF293A5E1FF0201746C05AE77,
	PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A,
	PropertyGroupAttribute__ctor_m8F7EF143FEDD12FAC7B22FBAC7BCB4885A09DF88,
	PropertyGroupAttribute_Combine_mE33D3E9250CECE2DE657D70D5B9041720362481D,
	PropertyGroupAttribute_CombineValuesWith_mA9D4370FD93B94EE22C250251D79E7C9ADD9E4D9,
	PropertyOrderAttribute__ctor_m0258452EBB74A30BCC712DBB88AA125607B703EA,
	PropertyOrderAttribute__ctor_mF84FACE1F0E9C501B91344E5D8ED99C41A49732D,
	RequiredAttribute__ctor_mD0472E44DB48D0FAEA21E3727FB7D3C97278F14D,
	RequiredAttribute__ctor_m7C9A7315A7C85A1389C15A35348A1C6A02F1E818,
	RequiredAttribute__ctor_m356BF7D4C0E2F20CE57E27C5514E94C28C8A149E,
	RequiredAttribute__ctor_mFED9DD0CBA9487878469BF33C8DF7D0A219B3CB2,
	SceneObjectsOnlyAttribute__ctor_mAF5D986331A81C1724B79E23ADEE745EB7EFD9E5,
	ValueDropdownAttribute_get_MemberName_mDDB41A491519276523688B757548FF60EFA21540,
	ValueDropdownAttribute_set_MemberName_m68682707EC773A1CFE11732A80DC2F915DF0F983,
	ValueDropdownAttribute__ctor_mC8F5D3B5106B55FDAE79C0CCA48CE575C822184E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ValueDropdownItem__ctor_mE15D14B9282ADADDB949FB86CEA4A53335BF7AF2,
	ValueDropdownItem_ToString_m526C92A7FAA56983098238EEBAB0E726113A1A91,
	ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mB8AB4A9382BA84731B96E24AB59C475901FC15E3,
	ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m233A24A83540D6FBC8A5593E63BFDA1AA64391E1,
	NULL,
	NULL,
	NULL,
	NULL,
	ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7,
	TabGroupAttribute__ctor_m44DBA7BC5910270727E85756FB81AEDA69E277FE,
	TabGroupAttribute__ctor_m3FDCD07DF133DF94BC3015B62269140288334C53,
	TabGroupAttribute_get_Tabs_mE3395C3B6ACF15D12A58430AA25626B80AD24A4D,
	TabGroupAttribute_set_Tabs_m40A73E4EC6877859058F36C0D4CEA49C3F4ED537,
	TabGroupAttribute_CombineValuesWith_m03FB48456237C348D28C3AC330774077BC4AA84E,
	TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_GetSubGroupAttributes_m86FAA7D41551FA38170739A1C4375482F5F0EAF0,
	TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_RepathMemberAttribute_m85A61D8AD679C3E6A58CAED8A389F21E92B0CE1F,
	TabSubGroupAttribute__ctor_m44ED5201B8DDB858B71AC573473AD2D269FEA381,
	TitleAttribute__ctor_m77110B723E6BA8ED4C8EBC8948FC75001867ECD4,
	ToggleAttribute__ctor_m6114BD54D92655906F8FDA3CAE94B5A456F965DF,
	ToggleGroupAttribute__ctor_m1E0CEDC90E7C6F85CEE5DBFDDEA3E22CA80321E6,
	ToggleGroupAttribute__ctor_mC14797C18E0030E086743FE9C2369571298D941E,
	ToggleGroupAttribute__ctor_m823D2E960C00F760CEE66D90B5BB3935BFB04A77,
	ToggleGroupAttribute_get_ToggleMemberName_m7117F9DDB7B23565ECF50BB6EC86223E6B896C28,
	ToggleGroupAttribute_get_TitleStringMemberName_mA5A6AD70D2A21733F25D5556553A7C0251AF6FAB,
	ToggleGroupAttribute_set_TitleStringMemberName_mD04BE047F8F988B70FC59476ED83C0E34D706C5C,
	ToggleGroupAttribute_CombineValuesWith_m592F8AF767D119CBD03D064A90A1777E1A0F5C63,
	ValidateInputAttribute_get_ContiniousValidationCheck_m1C740C2A8CB957F6DC3BAB42D99A4D236AE3F4FE,
	ValidateInputAttribute_set_ContiniousValidationCheck_m7081AEBB91BEA03BC0AD1DD6DFD71B57FA882A19,
	ValidateInputAttribute__ctor_m3FA8ADE2F0F3327705269A7DD59A90FA30A1FA72,
	ValidateInputAttribute__ctor_mC892E1A76D31818FF5E84A156F41B471F62FFB2C,
	ShowIfAttribute_get_MemberName_mDF9FD77EE8556F9FC0BE36A70DE6304D8E5A4F29,
	ShowIfAttribute_set_MemberName_mB198C1AA69C874A57B784B11303A4B059C9A981F,
	ShowIfAttribute__ctor_m6B88C445A0D6989BFFFF6826FA8EF58611EF84DF,
	ShowIfAttribute__ctor_m2939A8F0ED2730D238E2CCE4844FE0B697B3F1D6,
	WrapAttribute__ctor_mBB5B66CD25B9F4AEFA2074CD55D05A6DA788CC77,
	NULL,
	NULL,
};
extern void ValueDropdownItem__ctor_mE15D14B9282ADADDB949FB86CEA4A53335BF7AF2_AdjustorThunk (void);
extern void ValueDropdownItem_ToString_m526C92A7FAA56983098238EEBAB0E726113A1A91_AdjustorThunk (void);
extern void ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mB8AB4A9382BA84731B96E24AB59C475901FC15E3_AdjustorThunk (void);
extern void ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m233A24A83540D6FBC8A5593E63BFDA1AA64391E1_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[4] = 
{
	{ 0x06000118, ValueDropdownItem__ctor_mE15D14B9282ADADDB949FB86CEA4A53335BF7AF2_AdjustorThunk },
	{ 0x06000119, ValueDropdownItem_ToString_m526C92A7FAA56983098238EEBAB0E726113A1A91_AdjustorThunk },
	{ 0x0600011A, ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mB8AB4A9382BA84731B96E24AB59C475901FC15E3_AdjustorThunk },
	{ 0x0600011B, ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m233A24A83540D6FBC8A5593E63BFDA1AA64391E1_AdjustorThunk },
};
static const int32_t s_InvokerIndices[316] = 
{
	23,
	26,
	14,
	23,
	3,
	23,
	28,
	23,
	1899,
	23,
	26,
	31,
	89,
	89,
	23,
	32,
	32,
	26,
	130,
	130,
	32,
	129,
	129,
	130,
	35,
	35,
	943,
	23,
	14,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	31,
	23,
	23,
	23,
	89,
	31,
	14,
	26,
	14,
	26,
	459,
	147,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	337,
	23,
	26,
	27,
	23,
	26,
	23,
	26,
	26,
	10,
	32,
	89,
	23,
	337,
	1900,
	32,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	1901,
	1902,
	1903,
	1904,
	89,
	31,
	89,
	31,
	10,
	32,
	89,
	31,
	1417,
	14,
	26,
	14,
	26,
	1905,
	1125,
	1906,
	27,
	23,
	337,
	1400,
	26,
	26,
	89,
	31,
	14,
	26,
	14,
	26,
	459,
	147,
	26,
	23,
	23,
	459,
	133,
	89,
	31,
	89,
	10,
	32,
	23,
	23,
	14,
	26,
	26,
	26,
	943,
	337,
	26,
	23,
	23,
	9,
	959,
	1907,
	1908,
	26,
	23,
	26,
	14,
	26,
	27,
	23,
	1313,
	23,
	42,
	14,
	26,
	26,
	27,
	23,
	23,
	23,
	31,
	23,
	23,
	14,
	26,
	27,
	23,
	23,
	14,
	26,
	26,
	27,
	23,
	14,
	26,
	459,
	147,
	23,
	23,
	23,
	23,
	1909,
	1910,
	26,
	89,
	31,
	89,
	31,
	129,
	32,
	23,
	23,
	23,
	89,
	31,
	89,
	31,
	943,
	1911,
	26,
	1438,
	26,
	23,
	32,
	107,
	27,
	14,
	26,
	14,
	26,
	14,
	26,
	1912,
	1913,
	1914,
	147,
	459,
	23,
	26,
	459,
	89,
	31,
	89,
	31,
	10,
	32,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	14,
	26,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	23,
	338,
	26,
	338,
	26,
	32,
	26,
	23,
	23,
	459,
	27,
	14,
	26,
	459,
	943,
	26,
	28,
	26,
	23,
	337,
	23,
	130,
	26,
	32,
	23,
	14,
	26,
	26,
	14,
	14,
	-1,
	-1,
	-1,
	27,
	14,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	23,
	1911,
	1915,
	14,
	26,
	26,
	14,
	28,
	943,
	1107,
	26,
	1916,
	27,
	1917,
	14,
	14,
	26,
	26,
	89,
	31,
	118,
	745,
	14,
	26,
	459,
	147,
	1905,
	14,
	28,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x02000072, { 0, 6 } },
	{ 0x02000074, { 6, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[7] = 
{
	{ (Il2CppRGCTXDataType)2, 26789 },
	{ (Il2CppRGCTXDataType)3, 25311 },
	{ (Il2CppRGCTXDataType)3, 23904 },
	{ (Il2CppRGCTXDataType)2, 26790 },
	{ (Il2CppRGCTXDataType)3, 25312 },
	{ (Il2CppRGCTXDataType)2, 26788 },
	{ (Il2CppRGCTXDataType)2, 26795 },
};
extern const Il2CppCodeGenModule g_Sirenix_OdinInspector_AttributesCodeGenModule;
const Il2CppCodeGenModule g_Sirenix_OdinInspector_AttributesCodeGenModule = 
{
	"Sirenix.OdinInspector.Attributes.dll",
	316,
	s_methodPointers,
	4,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	7,
	s_rgctxValues,
	NULL,
};
