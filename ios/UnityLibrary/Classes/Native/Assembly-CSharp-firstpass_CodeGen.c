﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOFade(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOFade_mB942166E905FA9D9591788D9E19C1D8BD8464AA5 (void);
// 0x00000002 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOPitch(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOPitch_m7B75396CF1F0D9E46C4B73B8F14648D5D5CD462B (void);
// 0x00000003 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOSetFloat(UnityEngine.Audio.AudioMixer,System.String,System.Single,System.Single)
extern void DOTweenModuleAudio_DOSetFloat_m2328D1896174D227CDFA764920A45F0270421F9F (void);
// 0x00000004 System.Int32 DG.Tweening.DOTweenModuleAudio::DOComplete(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOComplete_m11E2C3F50B96B828F59424773B84A12221BFD84E (void);
// 0x00000005 System.Int32 DG.Tweening.DOTweenModuleAudio::DOKill(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOKill_mB33A6E7EC2957B57687C32DB366C2BBECB9B0011 (void);
// 0x00000006 System.Int32 DG.Tweening.DOTweenModuleAudio::DOFlip(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOFlip_m5DDA9A07C4A05BE9D0945D975396164E54267AEA (void);
// 0x00000007 System.Int32 DG.Tweening.DOTweenModuleAudio::DOGoto(UnityEngine.Audio.AudioMixer,System.Single,System.Boolean)
extern void DOTweenModuleAudio_DOGoto_mF7F74E5CB43D64EDBD4D29157BBCCE7B0A708C94 (void);
// 0x00000008 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPause_m22322C2DD0A0BB336568781D42993A44AAB14F71 (void);
// 0x00000009 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlay(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlay_m9C7A6CFD0F43348126CCE5658FBB206F425E543A (void);
// 0x0000000A System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayBackwards(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayBackwards_mC3E6D0146E8C8FE0B527DEFCF4EC5D11A187B92C (void);
// 0x0000000B System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayForward(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayForward_m628B5E119C53D71FD7BE4872BC5F41F1AC318FD0 (void);
// 0x0000000C System.Int32 DG.Tweening.DOTweenModuleAudio::DORestart(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORestart_m558B81091AD5100605BEF48D42864A39B4A79858 (void);
// 0x0000000D System.Int32 DG.Tweening.DOTweenModuleAudio::DORewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORewind_m631E833B8DFE7644B27B86ACA549F4DE7E1BDE4D (void);
// 0x0000000E System.Int32 DG.Tweening.DOTweenModuleAudio::DOSmoothRewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOSmoothRewind_m0155C90819F6AE27F58A6D84585F64CA7482950D (void);
// 0x0000000F System.Int32 DG.Tweening.DOTweenModuleAudio::DOTogglePause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOTogglePause_mCAA35FA3A42138FF4CD14F8445C0C3C5F0EE69CB (void);
// 0x00000010 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMove_mCD4035AEE8BDF7C32463165F9FEE4D0D53EA07D7 (void);
// 0x00000011 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveX(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveX_mABFFF46A3FABA32696A6C27A46FF5DB69EDA1446 (void);
// 0x00000012 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveY(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveY_mAE39D31F9966CB0AFCACD360BEC1F704B347FF8F (void);
// 0x00000013 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveZ(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveZ_m484788D4764841E99F3CE57CF9A5EE5BB09702C3 (void);
// 0x00000014 DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void DOTweenModulePhysics_DORotate_mCE061CE2863BA397B95DC4CBD201262DFB315364 (void);
// 0x00000015 DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DOLookAt(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.AxisConstraint,System.Nullable`1<UnityEngine.Vector3>)
extern void DOTweenModulePhysics_DOLookAt_mF7E3F17E02A027586E3D08A424AB3B0FFBA8FCDF (void);
// 0x00000016 DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics::DOJump(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOJump_m4BC92999B54B934B81B55E1CB91AEE210BECA09D (void);
// 0x00000017 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOPath_m4B7322AC2520EF2663F6773EF009C9E7FB91905D (void);
// 0x00000018 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOLocalPath_m99E7D1A2F80161D531600D1ABF48377D4470CC26 (void);
// 0x00000019 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOPath_m2ADD94A81FE5B4C3A3E3980A682F90E797960E6C (void);
// 0x0000001A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOLocalPath_m5FD44D934098449B3A701AAED8A6CAAB8EE73B3D (void);
// 0x0000001B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMove_m7347A43EB8A8C27D168ABF80BF96E536FB9A72CC (void);
// 0x0000001C DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveX(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveX_m537956DDFACEA2C4337BE948E07CE3409B2E2F6F (void);
// 0x0000001D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveY(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveY_m2AE2056F86B1E0EB3FBC1E1DF7A785B4AD96064D (void);
// 0x0000001E DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModulePhysics2D::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern void DOTweenModulePhysics2D_DORotate_m915B7CE83E09FE79FC09AF3872B994E02835AB68 (void);
// 0x0000001F DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D::DOJump(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOJump_m4A1D9450DA958808ABD3A074BD09890BF0CD3D6C (void);
// 0x00000020 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::DOPath(UnityEngine.Rigidbody2D,UnityEngine.Vector2[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics2D_DOPath_m2843CECE99E3C4D9501C8F0151A3F865ADE17831 (void);
// 0x00000021 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::DOLocalPath(UnityEngine.Rigidbody2D,UnityEngine.Vector2[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics2D_DOLocalPath_m1217AE34C36C424ED747337BC5775804567640F2 (void);
// 0x00000022 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOColor_m78D84C92828C97E15E16AA9CF722983492371EE7 (void);
// 0x00000023 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern void DOTweenModuleSprite_DOFade_m30875D140C666F476ABF36662A9CAFF8C968F8A5 (void);
// 0x00000024 DG.Tweening.Sequence DG.Tweening.DOTweenModuleSprite::DOGradientColor(UnityEngine.SpriteRenderer,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleSprite_DOGradientColor_m20E2FF126401EA952485086F997FF4F0E9295E8B (void);
// 0x00000025 DG.Tweening.Tweener DG.Tweening.DOTweenModuleSprite::DOBlendableColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOBlendableColor_mF49F831B198670BD34E4D58FC3D71FF67B064C20 (void);
// 0x00000026 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mAF6D3F44B8AFBAA181BCF8B68310D8AB2F7E4884 (void);
// 0x00000027 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mEC16AEF6B41EB5B61C31C741F818BC40581F9FDC (void);
// 0x00000028 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Graphic,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m7CC2AB868F4854FD40FB375A4EBA5E271E55E1E6 (void);
// 0x00000029 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m718B3DEBC00EF8F375DC3DD3DCE4C07CDB1A6366 (void);
// 0x0000002A DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m2B58045B88BED858C0DB4D385B237E16CD4B8E53 (void);
// 0x0000002B DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFillAmount(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFillAmount_mA515465CCACB47453C4387A65E18914F03299B1C (void);
// 0x0000002C DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOGradientColor(UnityEngine.UI.Image,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUI_DOGradientColor_m0C2322D164913566D07FC462DE0C9F9DA689B485 (void);
// 0x0000002D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOFlexibleSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOFlexibleSize_m01B661DD6C8BEF38AF06BA083B989AA1D5B59C54 (void);
// 0x0000002E DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOMinSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOMinSize_m2395DFBE826FFD1A4A342626272AA06CEE43194F (void);
// 0x0000002F DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPreferredSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPreferredSize_mFC7CD0E43C0E069DB79DA62850119516F0AA2649 (void);
// 0x00000030 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Outline,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mFC0417A98B850B32BE0D6843D526857328165BB9 (void);
// 0x00000031 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Outline,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m32380BAFA874FE4D8A6773E288A414B626FF916E (void);
// 0x00000032 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOScale(UnityEngine.UI.Outline,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOScale_m5A38F52D5560590F080984855506004587604DF1 (void);
// 0x00000033 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos_mCB659BC7116CAF3401EF356F3C161879FB36BA57 (void);
// 0x00000034 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosX_m1A146C9DD5C93C43A0931C6FF73A64B35386DC8C (void);
// 0x00000035 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosY_m58BC17DAAC54B227E4EBA8F108DC7ECAC3AF48BB (void);
// 0x00000036 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3D_mF92C8F167F799A05852981C2C3610719DC7DF93D (void);
// 0x00000037 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DX_m0FB309BC42714965D6ADB08659FF9954EFBDCB03 (void);
// 0x00000038 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DY_m0742CD6111A0578EB2418F59BB80B1F7D44AD652 (void);
// 0x00000039 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DZ(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DZ_m3EC428DD891F5F13CB490E7212ADD728BBD11BA8 (void);
// 0x0000003A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMax(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMax_m7F276F9C402B5D6169D0337036BB97F5A929CFF8 (void);
// 0x0000003B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMin(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMin_mDB3F3E1FFE30724A6912CBFDE5F519FAB4BABC76 (void);
// 0x0000003C DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivot(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOPivot_mD30001895FDC39E39D55FF0396EA56ACFF2AFAD1 (void);
// 0x0000003D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotX(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotX_m9DC0A394A9D0C43385FBAF25E0C398124DA5C9EC (void);
// 0x0000003E DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotY(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotY_m7960E5F973345C93BD10982EC01FB57A4716A6C1 (void);
// 0x0000003F DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOSizeDelta_m127667EF01B3F23E153AE4DC626B924E3574D54B (void);
// 0x00000040 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPunchAnchorPos_m3F25051D101AF8FCE2C58503B2083BB93969E6B1 (void);
// 0x00000041 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,System.Single,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_mF9176FEC11DC9855990CB010C7503A15420C80EE (void);
// 0x00000042 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m38841320F201AFFE9723D7C9D6D41AE8EC63A9DB (void);
// 0x00000043 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOJumpAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOJumpAnchorPos_m362656D22092B92BE5D600CEFD905963BEACC972 (void);
// 0x00000044 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DONormalizedPos(UnityEngine.UI.ScrollRect,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DONormalizedPos_m9D75DFE5046F51C8D6851ECDD86B109AAFD1488D (void);
// 0x00000045 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOHorizontalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOHorizontalNormalizedPos_m033060F4D73139E7AA4932E33D6437B15648C578 (void);
// 0x00000046 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOVerticalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOVerticalNormalizedPos_mD800102A319273558E7303454140ADB8AE93F3B4 (void);
// 0x00000047 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOValue(UnityEngine.UI.Slider,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOValue_mAACC2D6EC891D068A6AA3716ED1DB6ABBB212505 (void);
// 0x00000048 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mBEE7A9C925CFE32B127E87920EBCD45C8A26FE6A (void);
// 0x00000049 DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions> DG.Tweening.DOTweenModuleUI::DOCounter(UnityEngine.UI.Text,System.Int32,System.Int32,System.Single,System.Boolean,System.Globalization.CultureInfo)
extern void DOTweenModuleUI_DOCounter_m534037AA0B6DC4FBC95AD3BD8353BB9B94168F12 (void);
// 0x0000004A DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m16D54582B55159BE2BAC25DF35060EA524419CBB (void);
// 0x0000004B DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTweenModuleUI::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void DOTweenModuleUI_DOText_mAB85134929DEA1CEE706938AA13C13119F933722 (void);
// 0x0000004C DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m66476F9823E43CA3ED1E517CE0DA20E010A9045A (void);
// 0x0000004D DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m59697DE967F8922CBFBFAF259F460C70EE79D68A (void);
// 0x0000004E DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m62E6B788F5E1F444B1DD391D634E1011826787D7 (void);
// 0x0000004F DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m0A96FA6972F7A27A6DBA12ACF29FC5CEC4134D11 (void);
// 0x00000050 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.String,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m86806E97FBC86955091F65786E676E9ACC7066F1 (void);
// 0x00000051 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForCompletion(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForCompletion_mC857E9A4A1C9D65EB21AFF7E5A162122FE0C7269 (void);
// 0x00000052 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForRewind(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForRewind_m0C19662E8F77E32AB6C5125669ED5BD45C8D31C4 (void);
// 0x00000053 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForKill(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForKill_m567D0C09631A3988277CE5B47C2C2C5F9748DE92 (void);
// 0x00000054 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForElapsedLoops_m975AAA3CF52F0F39647CEA3E1DB61F15B0F52FF7 (void);
// 0x00000055 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForPosition(DG.Tweening.Tween,System.Single,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForPosition_m5802432A7EC6CB2964C531BDE898592023B8AEDA (void);
// 0x00000056 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForStart(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForStart_m2C083E752ADAF55CA262101863B5A8A6244FB5D5 (void);
// 0x00000057 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOOffset_mEED1CC56BEC984D8B39B6375A3115504DB7665DD (void);
// 0x00000058 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOTiling_mED7929FE921959BC6A62840B61349C022E41BCA4 (void);
// 0x00000059 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForCompletion(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForCompletion_m151975C66F07E7ACE030674B69C85F9F67119973 (void);
// 0x0000005A System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForRewind(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForRewind_m6E81BCFBA5B66CABC05453991D43615F4B90A220 (void);
// 0x0000005B System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForKill(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForKill_m6E6F64F4B0698A871E4CF56FC93DEBC34537F224 (void);
// 0x0000005C System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForElapsedLoops(DG.Tweening.Tween,System.Int32)
extern void DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_m62F89260C2AF682A39A3C2FDA9E0326B562140D5 (void);
// 0x0000005D System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForPosition(DG.Tweening.Tween,System.Single)
extern void DOTweenModuleUnityVersion_AsyncWaitForPosition_mBBD8B73E2413D3599602F683F0F7B59AE9F2BC2E (void);
// 0x0000005E System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForStart(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForStart_mCC42BB0C1B1CBEE945664E2F76D24DE98893761C (void);
// 0x0000005F System.Void DG.Tweening.DOTweenModuleUtils::Init()
extern void DOTweenModuleUtils_Init_m2A66692E84542706CF558924C6D142423A389571 (void);
// 0x00000060 System.Void DG.Tweening.DOTweenModuleUtils::Preserver()
extern void DOTweenModuleUtils_Preserver_mE297479CDF906C16415F282D644E84094A8152CB (void);
// 0x00000061 System.Void DG.Tweening.DOTweenAnimation::add_OnReset(System.Action`1<DG.Tweening.DOTweenAnimation>)
extern void DOTweenAnimation_add_OnReset_mE2AC726A3A8FD2B0D79BFC16C9ACF92B68F939CD (void);
// 0x00000062 System.Void DG.Tweening.DOTweenAnimation::remove_OnReset(System.Action`1<DG.Tweening.DOTweenAnimation>)
extern void DOTweenAnimation_remove_OnReset_mF3F9100DFDB441A1F78030B19AFF52E72F855AD4 (void);
// 0x00000063 System.Void DG.Tweening.DOTweenAnimation::Dispatch_OnReset(DG.Tweening.DOTweenAnimation)
extern void DOTweenAnimation_Dispatch_OnReset_m0CC7F0F335E78A9F7967F4B8DADA4E364DF05492 (void);
// 0x00000064 System.Void DG.Tweening.DOTweenAnimation::Awake()
extern void DOTweenAnimation_Awake_m786322982931DFF1165275689D9745B21F4FAD63 (void);
// 0x00000065 System.Void DG.Tweening.DOTweenAnimation::Start()
extern void DOTweenAnimation_Start_mD2D408605D0824A28F80B1351E6A3481103A7755 (void);
// 0x00000066 System.Void DG.Tweening.DOTweenAnimation::Reset()
extern void DOTweenAnimation_Reset_m3EDF475C4E7D85D56DDCAA91B336EE1476B42FA7 (void);
// 0x00000067 System.Void DG.Tweening.DOTweenAnimation::OnDestroy()
extern void DOTweenAnimation_OnDestroy_m3C875DB76703362F7DFB8A38EFF3D98C2DA75FB2 (void);
// 0x00000068 System.Void DG.Tweening.DOTweenAnimation::CreateTween()
extern void DOTweenAnimation_CreateTween_m11A99128B8D021E1D63E8BF7AAFAB39C4B9D96DE (void);
// 0x00000069 System.Void DG.Tweening.DOTweenAnimation::DOPlay()
extern void DOTweenAnimation_DOPlay_m8E2BD848084B87B63786FB940EE174C5DBB22F37 (void);
// 0x0000006A System.Void DG.Tweening.DOTweenAnimation::DOPlayBackwards()
extern void DOTweenAnimation_DOPlayBackwards_mF7C18418FB1AB4FDE4A9DE9327086607F4A9CC7C (void);
// 0x0000006B System.Void DG.Tweening.DOTweenAnimation::DOPlayForward()
extern void DOTweenAnimation_DOPlayForward_mB23F05A8D08303B18A7A0F50881B75DF9BB43E70 (void);
// 0x0000006C System.Void DG.Tweening.DOTweenAnimation::DOPause()
extern void DOTweenAnimation_DOPause_m834CD52451DCCD48AA0AFC07B4521005E8A0A0FE (void);
// 0x0000006D System.Void DG.Tweening.DOTweenAnimation::DOTogglePause()
extern void DOTweenAnimation_DOTogglePause_m39453B68EAF1A48DF9AF2572FAB52FC33C3AD0C7 (void);
// 0x0000006E System.Void DG.Tweening.DOTweenAnimation::DORewind()
extern void DOTweenAnimation_DORewind_m8A44C443CC5130594B43C36FF7DD38116F3112CE (void);
// 0x0000006F System.Void DG.Tweening.DOTweenAnimation::DORestart()
extern void DOTweenAnimation_DORestart_m7CDA1E4ABAF0416F9EEC93DACB190313FED04CE8 (void);
// 0x00000070 System.Void DG.Tweening.DOTweenAnimation::DORestart(System.Boolean)
extern void DOTweenAnimation_DORestart_mA35D60C880CC89F09BBDDA537D7C264E8926B9FF (void);
// 0x00000071 System.Void DG.Tweening.DOTweenAnimation::DOComplete()
extern void DOTweenAnimation_DOComplete_mB32CB25E1C5A543800CAC2E9A30317F25F81F8CD (void);
// 0x00000072 System.Void DG.Tweening.DOTweenAnimation::DOKill()
extern void DOTweenAnimation_DOKill_m3D1F2CF86B02F0D8EBB21BF7DB47C328A62FA3FD (void);
// 0x00000073 System.Void DG.Tweening.DOTweenAnimation::DOPlayById(System.String)
extern void DOTweenAnimation_DOPlayById_m3897F8A67F05D47A006DF9B1364F844FFD10F565 (void);
// 0x00000074 System.Void DG.Tweening.DOTweenAnimation::DOPlayAllById(System.String)
extern void DOTweenAnimation_DOPlayAllById_mEDB4F798D2C29F64001B48D937A2EB299F238523 (void);
// 0x00000075 System.Void DG.Tweening.DOTweenAnimation::DOPauseAllById(System.String)
extern void DOTweenAnimation_DOPauseAllById_m372817130FF60EA203642D8B88F4A7154D75421D (void);
// 0x00000076 System.Void DG.Tweening.DOTweenAnimation::DOPlayBackwardsById(System.String)
extern void DOTweenAnimation_DOPlayBackwardsById_m83411715B049B61D9515871D8D7E6E14468AD53F (void);
// 0x00000077 System.Void DG.Tweening.DOTweenAnimation::DOPlayBackwardsAllById(System.String)
extern void DOTweenAnimation_DOPlayBackwardsAllById_mA8B6DC6E2F0BFD4A6411E293A6C164AD7ED8DFDC (void);
// 0x00000078 System.Void DG.Tweening.DOTweenAnimation::DOPlayForwardById(System.String)
extern void DOTweenAnimation_DOPlayForwardById_mDC67FCE1EDACB7EBBA75DB5133F401E85CDA3A69 (void);
// 0x00000079 System.Void DG.Tweening.DOTweenAnimation::DOPlayForwardAllById(System.String)
extern void DOTweenAnimation_DOPlayForwardAllById_m7B03693886B66F9C78A2BD9DDFE52C85DE713124 (void);
// 0x0000007A System.Void DG.Tweening.DOTweenAnimation::DOPlayNext()
extern void DOTweenAnimation_DOPlayNext_m0FFF4DE3D3C44AC04FFC2098C33A77AE858664D7 (void);
// 0x0000007B System.Void DG.Tweening.DOTweenAnimation::DORewindAndPlayNext()
extern void DOTweenAnimation_DORewindAndPlayNext_m61310307A43B54A051B66661F411595BD94A7BB9 (void);
// 0x0000007C System.Void DG.Tweening.DOTweenAnimation::DORewindAllById(System.String)
extern void DOTweenAnimation_DORewindAllById_mC053DD42517AA16874EA764E2177AC16B8ABDB18 (void);
// 0x0000007D System.Void DG.Tweening.DOTweenAnimation::DORestartById(System.String)
extern void DOTweenAnimation_DORestartById_m4B1EC2CC857288BA99E308C221DC924BC2A51CCE (void);
// 0x0000007E System.Void DG.Tweening.DOTweenAnimation::DORestartAllById(System.String)
extern void DOTweenAnimation_DORestartAllById_mA6CA25E46D3786E89B858B621FBEE49FB719DED2 (void);
// 0x0000007F System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.DOTweenAnimation::GetTweens()
extern void DOTweenAnimation_GetTweens_m927F01FD07C0745158CBB384369195CC4D925AA3 (void);
// 0x00000080 DG.Tweening.DOTweenAnimation/TargetType DG.Tweening.DOTweenAnimation::TypeToDOTargetType(System.Type)
extern void DOTweenAnimation_TypeToDOTargetType_m0B84A99759642746277D13B730F0954E4DA30002 (void);
// 0x00000081 DG.Tweening.Tween DG.Tweening.DOTweenAnimation::CreateEditorPreview()
extern void DOTweenAnimation_CreateEditorPreview_mDCA6EA8D733B8E310BDF04551B6E7D10950FDB42 (void);
// 0x00000082 UnityEngine.GameObject DG.Tweening.DOTweenAnimation::GetTweenGO()
extern void DOTweenAnimation_GetTweenGO_mEA4F79F8CC07E17EDF50897CBA794BCEB5DEA7C8 (void);
// 0x00000083 System.Void DG.Tweening.DOTweenAnimation::ReEvaluateRelativeTween()
extern void DOTweenAnimation_ReEvaluateRelativeTween_m77C2A5D243BFB40C6FC8090EFF6DBE1F27EFC176 (void);
// 0x00000084 System.Void DG.Tweening.DOTweenAnimation::.ctor()
extern void DOTweenAnimation__ctor_m5517AEA84736B94CC875B452D29D44831E559DCC (void);
// 0x00000085 System.Void DG.Tweening.DOTweenAnimation::<CreateTween>b__47_0()
extern void DOTweenAnimation_U3CCreateTweenU3Eb__47_0_m7EF21F58940B00415BFF5170235F86E7B6FB7756 (void);
// 0x00000086 System.Boolean DG.Tweening.DOTweenAnimationExtensions::IsSameOrSubclassOf(UnityEngine.Component)
// 0x00000087 System.Void DG.Tweening.DOTweenProShortcuts::.cctor()
extern void DOTweenProShortcuts__cctor_mAFE7F19DEF41C81FF7F344AA6AC08D47951E223C (void);
// 0x00000088 DG.Tweening.Tweener DG.Tweening.DOTweenProShortcuts::DOSpiral(UnityEngine.Transform,System.Single,System.Nullable`1<UnityEngine.Vector3>,DG.Tweening.SpiralMode,System.Single,System.Single,System.Single,System.Boolean)
extern void DOTweenProShortcuts_DOSpiral_mDE34B9FA70AE32467569D33A39E3A8CD05362451 (void);
// 0x00000089 DG.Tweening.Tweener DG.Tweening.DOTweenProShortcuts::DOSpiral(UnityEngine.Rigidbody,System.Single,System.Nullable`1<UnityEngine.Vector3>,DG.Tweening.SpiralMode,System.Single,System.Single,System.Single,System.Boolean)
extern void DOTweenProShortcuts_DOSpiral_m5DAEE15C47A2D416BE15D33D661671F5B64CBB73 (void);
// 0x0000008A DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.ShortcutExtensionsTMPText::DOColor(TMPro.TMP_Text,UnityEngine.Color,System.Single)
extern void ShortcutExtensionsTMPText_DOColor_m7085A2D722AE069A272C1582BFB12634386F148B (void);
// 0x0000008B DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.ShortcutExtensionsTMPText::DOFaceColor(TMPro.TMP_Text,UnityEngine.Color32,System.Single)
extern void ShortcutExtensionsTMPText_DOFaceColor_mF6FD9E2E1E3F7015DD65D445DD89C2D7691130EB (void);
// 0x0000008C DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.ShortcutExtensionsTMPText::DOOutlineColor(TMPro.TMP_Text,UnityEngine.Color32,System.Single)
extern void ShortcutExtensionsTMPText_DOOutlineColor_mA8E2EF8EDA85E2BD2F5265D2A774C544A2639822 (void);
// 0x0000008D DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.ShortcutExtensionsTMPText::DOGlowColor(TMPro.TMP_Text,UnityEngine.Color,System.Single,System.Boolean)
extern void ShortcutExtensionsTMPText_DOGlowColor_m105AF572BAFA001943D7EAAEF6056E8C815823DC (void);
// 0x0000008E DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.ShortcutExtensionsTMPText::DOFade(TMPro.TMP_Text,System.Single,System.Single)
extern void ShortcutExtensionsTMPText_DOFade_m3EBD70506DDC6917D4AF32CCEFB45455B3A2DC48 (void);
// 0x0000008F DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.ShortcutExtensionsTMPText::DOFaceFade(TMPro.TMP_Text,System.Single,System.Single)
extern void ShortcutExtensionsTMPText_DOFaceFade_m132A69B631D4AFF9527FF4AAF772C197B67CB136 (void);
// 0x00000090 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.ShortcutExtensionsTMPText::DOScale(TMPro.TMP_Text,System.Single,System.Single)
extern void ShortcutExtensionsTMPText_DOScale_m0782C6EEB0A54E8A980429E9B55653BAF866BF38 (void);
// 0x00000091 DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions> DG.Tweening.ShortcutExtensionsTMPText::DOCounter(TMPro.TMP_Text,System.Int32,System.Int32,System.Single,System.Boolean,System.Globalization.CultureInfo)
extern void ShortcutExtensionsTMPText_DOCounter_m9EDB0BC46DDD4DF289A10086CE2A98354A449EC2 (void);
// 0x00000092 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.ShortcutExtensionsTMPText::DOFontSize(TMPro.TMP_Text,System.Single,System.Single)
extern void ShortcutExtensionsTMPText_DOFontSize_m16F61857E74463B9797001DBE3E28756F517AC75 (void);
// 0x00000093 DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions> DG.Tweening.ShortcutExtensionsTMPText::DOMaxVisibleCharacters(TMPro.TMP_Text,System.Int32,System.Single)
extern void ShortcutExtensionsTMPText_DOMaxVisibleCharacters_m8E0FFB73842950591003B38E5E6F8747F46DA237 (void);
// 0x00000094 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.ShortcutExtensionsTMPText::DOText(TMPro.TMP_Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void ShortcutExtensionsTMPText_DOText_mAE4827EF068E8F2EABF866D88822887FC48FCFE2 (void);
// 0x00000095 TMPro.TMP_Text DG.Tweening.DOTweenTMPAnimator::get_target()
extern void DOTweenTMPAnimator_get_target_mE0549E1F6D3DB90526E04578F4E6D77769A4E5E8 (void);
// 0x00000096 System.Void DG.Tweening.DOTweenTMPAnimator::set_target(TMPro.TMP_Text)
extern void DOTweenTMPAnimator_set_target_mD835615121F2C8749EBD922CAC4FE419ADAECCD2 (void);
// 0x00000097 TMPro.TMP_TextInfo DG.Tweening.DOTweenTMPAnimator::get_textInfo()
extern void DOTweenTMPAnimator_get_textInfo_mC8D2BDFED3A59DB0F1FC32EE55542B4963076770 (void);
// 0x00000098 System.Void DG.Tweening.DOTweenTMPAnimator::set_textInfo(TMPro.TMP_TextInfo)
extern void DOTweenTMPAnimator_set_textInfo_m7FA393DE99C081275B7D89CC19FC7F0E24AF40E7 (void);
// 0x00000099 System.Void DG.Tweening.DOTweenTMPAnimator::.ctor(TMPro.TMP_Text)
extern void DOTweenTMPAnimator__ctor_m49558141052BE834A2182246CB7401FCEBD1CB26 (void);
// 0x0000009A System.Void DG.Tweening.DOTweenTMPAnimator::Dispose()
extern void DOTweenTMPAnimator_Dispose_mB69FFB809A4C0F6A1F86BCFE5FE1F948D1A8FC2B (void);
// 0x0000009B System.Void DG.Tweening.DOTweenTMPAnimator::Refresh()
extern void DOTweenTMPAnimator_Refresh_m68461DD488AF2C7CD465E32BF4D76ACD91BA0744 (void);
// 0x0000009C System.Void DG.Tweening.DOTweenTMPAnimator::Reset()
extern void DOTweenTMPAnimator_Reset_mA6474D53EAF23757CE59D1B725C4B905F90ED959 (void);
// 0x0000009D System.Void DG.Tweening.DOTweenTMPAnimator::OnTextChanged(UnityEngine.Object)
extern void DOTweenTMPAnimator_OnTextChanged_m6148210317D764238ACA460E1A8C0FE9B733084D (void);
// 0x0000009E System.Boolean DG.Tweening.DOTweenTMPAnimator::ValidateChar(System.Int32,System.Boolean)
extern void DOTweenTMPAnimator_ValidateChar_mC1ABA593C1C8769556D52A68B18B7F755194685D (void);
// 0x0000009F System.Boolean DG.Tweening.DOTweenTMPAnimator::ValidateSpan(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern void DOTweenTMPAnimator_ValidateSpan_m2C3B6561DDC8C059C621AB2751643F59CE6ECC8C (void);
// 0x000000A0 System.Void DG.Tweening.DOTweenTMPAnimator::SkewSpanX(System.Int32,System.Int32,System.Single,System.Boolean)
extern void DOTweenTMPAnimator_SkewSpanX_m120803289811FFFDCB4F794F29A80E257D185626 (void);
// 0x000000A1 System.Void DG.Tweening.DOTweenTMPAnimator::SkewSpanY(System.Int32,System.Int32,System.Single,DG.Tweening.TMPSkewSpanMode,System.Boolean)
extern void DOTweenTMPAnimator_SkewSpanY_m3B9B571B4F81CB5476EE47385CAEB1536C4990FB (void);
// 0x000000A2 UnityEngine.Color DG.Tweening.DOTweenTMPAnimator::GetCharColor(System.Int32)
extern void DOTweenTMPAnimator_GetCharColor_m8A31C8F67338656206AB96CCB31290F7DC0DB6BE (void);
// 0x000000A3 UnityEngine.Vector3 DG.Tweening.DOTweenTMPAnimator::GetCharOffset(System.Int32)
extern void DOTweenTMPAnimator_GetCharOffset_mC048ACD5693D80D3AC9DDDED4C443EE346CCA491 (void);
// 0x000000A4 UnityEngine.Vector3 DG.Tweening.DOTweenTMPAnimator::GetCharRotation(System.Int32)
extern void DOTweenTMPAnimator_GetCharRotation_mC07E82AE9DEDE77D4A8EE550C4B89021EC8D3339 (void);
// 0x000000A5 UnityEngine.Vector3 DG.Tweening.DOTweenTMPAnimator::GetCharScale(System.Int32)
extern void DOTweenTMPAnimator_GetCharScale_m6E5D73CAE4E665AC16DD7CEC6FE593E1BCD99B17 (void);
// 0x000000A6 System.Void DG.Tweening.DOTweenTMPAnimator::SetCharColor(System.Int32,UnityEngine.Color32)
extern void DOTweenTMPAnimator_SetCharColor_mEB5FDA324D642B6ABBF060560F323C5B9E539813 (void);
// 0x000000A7 System.Void DG.Tweening.DOTweenTMPAnimator::SetCharOffset(System.Int32,UnityEngine.Vector3)
extern void DOTweenTMPAnimator_SetCharOffset_m51CF473BA53EE3F7EFEB0B6F4E95DC8571710EEC (void);
// 0x000000A8 System.Void DG.Tweening.DOTweenTMPAnimator::SetCharRotation(System.Int32,UnityEngine.Vector3)
extern void DOTweenTMPAnimator_SetCharRotation_m9E35976A69003DED36757A88A9587F53DE0C2F6C (void);
// 0x000000A9 System.Void DG.Tweening.DOTweenTMPAnimator::SetCharScale(System.Int32,UnityEngine.Vector3)
extern void DOTweenTMPAnimator_SetCharScale_m33D05C9ECF05DA768DADA25D6BC3C3C4A864BD9F (void);
// 0x000000AA System.Void DG.Tweening.DOTweenTMPAnimator::ShiftCharVertices(System.Int32,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void DOTweenTMPAnimator_ShiftCharVertices_m1E5EE862EE07F95EFBE6283D1D340A3402F82168 (void);
// 0x000000AB System.Single DG.Tweening.DOTweenTMPAnimator::SkewCharX(System.Int32,System.Single,System.Boolean)
extern void DOTweenTMPAnimator_SkewCharX_mA1F6A28AF9ABDB50759E7BC5E702501DA11517DC (void);
// 0x000000AC System.Single DG.Tweening.DOTweenTMPAnimator::SkewCharY(System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenTMPAnimator_SkewCharY_mE58154BD78AE4ACE78AAB443DE6E3FB1CA461F6E (void);
// 0x000000AD System.Void DG.Tweening.DOTweenTMPAnimator::ResetVerticesShift(System.Int32)
extern void DOTweenTMPAnimator_ResetVerticesShift_mDA654753E566BBF9CA37FDFB5BE62FC26F1FA3F9 (void);
// 0x000000AE DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenTMPAnimator::DOFadeChar(System.Int32,System.Single,System.Single)
extern void DOTweenTMPAnimator_DOFadeChar_m735FD942D0C4C6CA970650F382903E8F85083269 (void);
// 0x000000AF DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenTMPAnimator::DOColorChar(System.Int32,UnityEngine.Color,System.Single)
extern void DOTweenTMPAnimator_DOColorChar_m0FC72C84AC2DFAAEACB4C3A622C69425385FDB20 (void);
// 0x000000B0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenTMPAnimator::DOOffsetChar(System.Int32,UnityEngine.Vector3,System.Single)
extern void DOTweenTMPAnimator_DOOffsetChar_mACD3E564D1D0344E58FA2AF1D041F7612920A9EE (void);
// 0x000000B1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenTMPAnimator::DORotateChar(System.Int32,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void DOTweenTMPAnimator_DORotateChar_m6BF42A304C3A161E123E283CE18FBF5A273A2DE6 (void);
// 0x000000B2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenTMPAnimator::DOScaleChar(System.Int32,System.Single,System.Single)
extern void DOTweenTMPAnimator_DOScaleChar_mC6514255D4279D893585CEAD8619DA6F876129F9 (void);
// 0x000000B3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenTMPAnimator::DOScaleChar(System.Int32,UnityEngine.Vector3,System.Single)
extern void DOTweenTMPAnimator_DOScaleChar_mBC13D3FB90C65046A18BBB084D81930F27B896FE (void);
// 0x000000B4 DG.Tweening.Tweener DG.Tweening.DOTweenTMPAnimator::DOPunchCharOffset(System.Int32,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern void DOTweenTMPAnimator_DOPunchCharOffset_mA1BAAF441876F719155FEDAC9C7A880C06263F57 (void);
// 0x000000B5 DG.Tweening.Tweener DG.Tweening.DOTweenTMPAnimator::DOPunchCharRotation(System.Int32,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern void DOTweenTMPAnimator_DOPunchCharRotation_m8B4F99C782C290FD45B668BF1FA73B806EC17CF7 (void);
// 0x000000B6 DG.Tweening.Tweener DG.Tweening.DOTweenTMPAnimator::DOPunchCharScale(System.Int32,System.Single,System.Single,System.Int32,System.Single)
extern void DOTweenTMPAnimator_DOPunchCharScale_mD9FA80620F41399291C83D27C18F9680EFB82581 (void);
// 0x000000B7 DG.Tweening.Tweener DG.Tweening.DOTweenTMPAnimator::DOPunchCharScale(System.Int32,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern void DOTweenTMPAnimator_DOPunchCharScale_mA2C16439ECF6A5DFDBE85B5E6999673DAE3D6FFE (void);
// 0x000000B8 DG.Tweening.Tweener DG.Tweening.DOTweenTMPAnimator::DOShakeCharOffset(System.Int32,System.Single,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenTMPAnimator_DOShakeCharOffset_mFDA6AF7E9BB3F09F8DD280D3960CCA327B9750A2 (void);
// 0x000000B9 DG.Tweening.Tweener DG.Tweening.DOTweenTMPAnimator::DOShakeCharOffset(System.Int32,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern void DOTweenTMPAnimator_DOShakeCharOffset_m09AED4227BA2ABB56276D9F9049678F1427D74A3 (void);
// 0x000000BA DG.Tweening.Tweener DG.Tweening.DOTweenTMPAnimator::DOShakeCharRotation(System.Int32,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern void DOTweenTMPAnimator_DOShakeCharRotation_mB9FABC829E380CBB05FC548F114055DCD9E16391 (void);
// 0x000000BB DG.Tweening.Tweener DG.Tweening.DOTweenTMPAnimator::DOShakeCharScale(System.Int32,System.Single,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenTMPAnimator_DOShakeCharScale_mCAA843847970A19C385D638469BC96E3B43083DE (void);
// 0x000000BC DG.Tweening.Tweener DG.Tweening.DOTweenTMPAnimator::DOShakeCharScale(System.Int32,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern void DOTweenTMPAnimator_DOShakeCharScale_mC4C670ACE8FABB57882D9E2B4DBF8C26DE13FA2D (void);
// 0x000000BD System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m60990EF6DED999DB0545FD5306D54E919965C5BB (void);
// 0x000000BE System.Single DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m2E712D095F38EFF8613E3FC9CDAFFF369D8E8E95 (void);
// 0x000000BF System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mA9535CA5F922CD4162D7B7EB0F0A298A923D2985 (void);
// 0x000000C0 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m8805F7B5496887519CF23A58F6D75794F5758657 (void);
// 0x000000C1 System.Single DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0::<DOPitch>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_m78A8C06D83D92E4AF3C524E330BBFF7869B4E945 (void);
// 0x000000C2 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0::<DOPitch>b__1(System.Single)
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_mA3894D1D54F6E6D4F3F2C8CA1A404E6C1410A6C8 (void);
// 0x000000C3 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m71180E2D0EA27D2B70FB04CCB232FF40201D0385 (void);
// 0x000000C4 System.Single DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::<DOSetFloat>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_m802F28A755D17457C6907F4D914080FD53C72975 (void);
// 0x000000C5 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::<DOSetFloat>b__1(System.Single)
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_m2C85AED977CB0722BC27B69E52F945C712A09EF0 (void);
// 0x000000C6 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m8D10F29A28AA70367B94826B210BE7AC28661D90 (void);
// 0x000000C7 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_m6D639DA5AF1F1070919A2AD6CFFB5C2DFC674CEF (void);
// 0x000000C8 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m32FDCE2F1BFA2CFC3521F6E7004AAC11A34016C4 (void);
// 0x000000C9 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m3728CFC4A5D5D57207EC9C0FE05C208362CD61E5 (void);
// 0x000000CA System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mDE175763216F603B2DA30F4A45B3FFF5ED153552 (void);
// 0x000000CB UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m26D852ED9489654A1F4F2783F1A1379952718554 (void);
// 0x000000CC System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m88E356FCBB56C0B4E21EF41B616C3C2E19AA3649 (void);
// 0x000000CD UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass3_0::<DOMoveZ>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_m10A9F6B598BC53801C9FBFC517EBA855BB7F088B (void);
// 0x000000CE System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m2687EA4B489F002E41214EF6F7672680730B0E66 (void);
// 0x000000CF UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass4_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_m1D42EF9DF4A2359CB646B559D74156F5337DA19C (void);
// 0x000000D0 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mE08B5612496DA11D695A973F1B7E50FC1625E8BC (void);
// 0x000000D1 UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass5_0::<DOLookAt>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_m44E2401A5E5BF3BCD6F32CAC9265AB855891ABE6 (void);
// 0x000000D2 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m4282C7CF312BCB82425278AE251B9ED79C309766 (void);
// 0x000000D3 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_mF9631396276B023DE2F01B2881BF896580058089 (void);
// 0x000000D4 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__1()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_m13EB6AC489A2346B2C4EF58D4EB7B121D48066A4 (void);
// 0x000000D5 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_mFE960B15DC2191AC258134DC69D42820C8BBA4AC (void);
// 0x000000D6 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_m750600C44CEF5FBAD9B81A3AB29DFA963E369A82 (void);
// 0x000000D7 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__4()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_mE6B0096A24F8A30429FDBA1FE23091E07FFBCD70 (void);
// 0x000000D8 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m9403D70752B0400788496A4B3E1CF88B2BD77556 (void);
// 0x000000D9 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass7_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_m80CA4871DE4B68B81FE817354C066A20D1CE2532 (void);
// 0x000000DA System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mD27CF675B05C30206F9710A975DEC8689270102F (void);
// 0x000000DB UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_mFC29F40E26FD9AB1ED15E7252015EB636A901F16 (void);
// 0x000000DC System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m9280F41F1BF4816E0E1141488AC566A38AED4877 (void);
// 0x000000DD System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m4389304DB98C045D7081797CF2619E6912DA4469 (void);
// 0x000000DE UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_m7D3154BA32BBC522A7B022C8BCB38B83AD162D21 (void);
// 0x000000DF System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mD439CDE8A04C3D6D891ADFFA53389A28FC45CF15 (void);
// 0x000000E0 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m431E707654D8B7DA87663674ABB75CFC9421B5D1 (void);
// 0x000000E1 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mACA0A26F9256CC583302147759E3D8D311A485EE (void);
// 0x000000E2 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m987E55F6B96E1B70E08FE936B62C8FD0FBAC093F (void);
// 0x000000E3 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mAAA4470F96A27457911C5E2DD5AAFB634F9063D4 (void);
// 0x000000E4 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mBC64465B357F91BE4BAC81CC826B2E47F020140C (void);
// 0x000000E5 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_mDB08C891796EA4B4C3B111166165B2B5335B48FA (void);
// 0x000000E6 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m3D438CF85609374F00FF2D5AE5567C35CEF994F2 (void);
// 0x000000E7 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_mA4C7FA3BAE450758E38B0AF38B0D9B123F9CB160 (void);
// 0x000000E8 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mA797C925AE17E2198FAE355EC675B2C462B692DE (void);
// 0x000000E9 System.Single DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass3_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_mDC1BDC928B45102C8674F9242FED8406B600B8F6 (void);
// 0x000000EA System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m6ED642429831E03D7D9A904BBA51AA670F51DB1A (void);
// 0x000000EB UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_mD3AD7B539BF754001FAE15CC5C5190E7108B13C6 (void);
// 0x000000EC System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_mA0D81831AD1D1F68358C86FE0321F99515584F20 (void);
// 0x000000ED System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_mE1F172F92451A5CB9E4116D8FFF960D49DDF81F7 (void);
// 0x000000EE UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_mE871DBCA3C2C4A20DD6DF40603CE929F6B728C17 (void);
// 0x000000EF System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_m63C3139CB577DF5F9A0F687E78FCBC7D31142016 (void);
// 0x000000F0 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__5()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_m71AAD9F749C8821E7B768B0F5BB9DD956767B3BC (void);
// 0x000000F1 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m2F1EA3E8F6B47999B69AC2A94327CDB012B8FBDA (void);
// 0x000000F2 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass5_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__0_m8F9D5E2B6A15FC25A6FE82BA74DBB9B1DE9776DC (void);
// 0x000000F3 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass5_0::<DOPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__1_m28D443C97DD9B05515FFA8FEA9B68559DF4C382A (void);
// 0x000000F4 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m6190B985BEFFEBB6530C42AA214D90BE33CA498F (void);
// 0x000000F5 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__0_m9FAC46A73884CEC27CA5BCE8EDCBB7CE40D17947 (void);
// 0x000000F6 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__1_m9C65CF8D5DCE3B549526FB3D8B83881DE059FD3B (void);
// 0x000000F7 System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mACAACDE36FA35CF551A1754248F010ADF4A7720B (void);
// 0x000000F8 UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m4D09BE9FA37B0F054B64878C7589ACAA63D7E9E7 (void);
// 0x000000F9 System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_mB02EE5E4E43469D0779797C599ECBCA7925CD04F (void);
// 0x000000FA System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m193FC2381BE613D6EC468F7DD16F22C9AEBDDC7D (void);
// 0x000000FB UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_mC0E4CD30D9BAF4B4DB0C18D40FF5D009C259F974 (void);
// 0x000000FC System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_mD34D43501AB67E6829C8EBB832CD6332AE19D6C9 (void);
// 0x000000FD System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m1DF4ABD4338E56F3CB0ABFC5F313EB409E7FC587 (void);
// 0x000000FE UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_m119DDCF7364B723DF560FC5A49515A485FB20F63 (void);
// 0x000000FF System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_mF179A7F107A8534794A0EDC9BE37B3866F6652D6 (void);
// 0x00000100 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/Utils::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern void Utils_SwitchToRectTransform_m587394DF33387D8FFD49567ACF0D3D72A1E9348B (void);
// 0x00000101 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m2E2D8A29DEE97D79779A6BC5FE87B8F2B00F9A96 (void);
// 0x00000102 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m76430B008C99FDC1DF8E4DF9E6BB9A6B4DDB6FEE (void);
// 0x00000103 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mAEB3C38498B2659828B4FDC13F71304B688F45B6 (void);
// 0x00000104 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m9C1271AE340911E2B78F2C5C503BC339A699D1DA (void);
// 0x00000105 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mF2954B74DFF433419CDEBC960721F61CA6DDC7AB (void);
// 0x00000106 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m9EAC3EE75B9D8196B65F12EC3DF489506F6B34E8 (void);
// 0x00000107 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m65F9922873298B65A3BC20045F9AA5302DEE4CC8 (void);
// 0x00000108 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m6657306991563E41E037C07818139BE80438ED8C (void);
// 0x00000109 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_mB29925037B8A73358D9E235E6A856CDD1C711065 (void);
// 0x0000010A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m8DB2ED589F7593B42BB6313BB24B801EDDB8E9A6 (void);
// 0x0000010B UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mEE30B8BD593EF80709EF57F5E377D0375A6BE2C7 (void);
// 0x0000010C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m2B1031C4C67640FE649A967E5CE404759FF2BFF9 (void);
// 0x0000010D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m6AEB13C4296D4B4B6BEF37A414A2DD7E6A86DF2B (void);
// 0x0000010E UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_mBA25EAFC5BAD1C0BB16EA5F359814BB13F8066D7 (void);
// 0x0000010F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m664BDF508E93CC4C2ACEDC7EC665F0898132C309 (void);
// 0x00000110 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m544D9C161709CFFFD5716AD0738D287A0EAF495B (void);
// 0x00000111 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::<DOFillAmount>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m34FDEA6E1E49D673A071A550E8738ABBBE70811B (void);
// 0x00000112 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::<DOFillAmount>b__1(System.Single)
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_mE8753F1EA15EC271C568F0AC148C3B9D9EF321CD (void);
// 0x00000113 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m89D8D1AA7F2DEB1C6AD83B363D4B80EA0088D7FE (void);
// 0x00000114 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::<DOFlexibleSize>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m9BE6F420825B56A735DC4ED217E4F579AC904D03 (void);
// 0x00000115 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::<DOFlexibleSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m4D99F430ABC6F8571734F8D0195818315B5C9340 (void);
// 0x00000116 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m52371CA3AD03014FD2E3338AC4243ADEBB7E9E34 (void);
// 0x00000117 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::<DOMinSize>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_mE2D6CF43309A578A1EB0AE3B5A029F08085A4A9A (void);
// 0x00000118 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::<DOMinSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m2D067AED94C7E4DCD78FC176C3D1FC6316748852 (void);
// 0x00000119 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m5A1037E3CD723BE2A684DC5B724583886F16A732 (void);
// 0x0000011A UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::<DOPreferredSize>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mD79F05224F02EF24DA4E5F1B9430494FBD13C991 (void);
// 0x0000011B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::<DOPreferredSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_mEF0B8DB508828917ECDA8614D7391A61FAC70CDC (void);
// 0x0000011C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m1C34D9DB0C7D7B1A77E05C188D35417309DB826E (void);
// 0x0000011D UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_mFC3529F25FC1E4FE81AE923D79752E20D32186FD (void);
// 0x0000011E System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m35D35F2F701BD9E7581BC98A67DEBF93E3FC59E0 (void);
// 0x0000011F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m95A09B9E17C97BACA2AE9D66A3957A9702A77A03 (void);
// 0x00000120 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_mBE8F5EE84C08A6127A749BF303073D5D6B7CFD6F (void);
// 0x00000121 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_mAB91C7B6A40CE5A3CB7C67CB6D3D96A3E2876715 (void);
// 0x00000122 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m367EFAEC33689D5A978026CED103F74564179859 (void);
// 0x00000123 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::<DOScale>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_m3BA1AF0ED10AA0AB85419F3314F09E39613D9274 (void);
// 0x00000124 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::<DOScale>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m97887EE3D0C5AA741E96430BCBF2A6904A35776C (void);
// 0x00000125 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m85CF8E49BC7259F362A6D5276F3283AEA3BE6CD0 (void);
// 0x00000126 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::<DOAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m8448DB43B34DC403A54FD076DA1A939163132F2E (void);
// 0x00000127 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::<DOAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_mDC840285042D121935390A684D52A56910837185 (void);
// 0x00000128 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m134730746C60289D671F21D934CDBE177E7F1DDB (void);
// 0x00000129 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::<DOAnchorPosX>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_mA77379A317EDC6EC6D6B9CF994F4F5A1C4AF11CA (void);
// 0x0000012A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::<DOAnchorPosX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m1ABEC4ED71326C7D91C19A11763EDE3D7923B99A (void);
// 0x0000012B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m04C93F784970AD85021055C9C5C9FD8B38E3B6A9 (void);
// 0x0000012C UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::<DOAnchorPosY>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_m87B8CF6051C5DF301DFA23CBCC090BFB2BF9644F (void);
// 0x0000012D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::<DOAnchorPosY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m79BDB7F3559B6338C1193FA6A7C65EC281DE272E (void);
// 0x0000012E System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m2B97AF2BD10B97B92BD05A77E7F5CC304F3094EA (void);
// 0x0000012F UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m509844967852E71C964B003A85857622695072A5 (void);
// 0x00000130 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m4ADDF7752F07269426CA82E1A1763B7437465D4D (void);
// 0x00000131 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mA66423C52E58DF1625C6973F7F9258F943CD0892 (void);
// 0x00000132 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::<DOAnchorPos3DX>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_m9F0F2C2A9C1B5D0BC15642C125BEFBC20BF9D173 (void);
// 0x00000133 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::<DOAnchorPos3DX>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_m8462038ED9881108299EE6902A0F4E068F70D8DB (void);
// 0x00000134 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mE8F0C5046749DE447B5228615AF8C2F89414BF38 (void);
// 0x00000135 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::<DOAnchorPos3DY>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_mA2A8E3A1047F3956638F8ED5BAF2F33F0348B292 (void);
// 0x00000136 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::<DOAnchorPos3DY>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_m39BA974886ED0A2ECAD6EE070D48D9DFDBACA9B0 (void);
// 0x00000137 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m7291A4617641DC932D99CD15053D3A99C7DDEA6F (void);
// 0x00000138 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_m5FF6AE83E8F55A381F4152F7886345950E6C2EB9 (void);
// 0x00000139 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_m2BC52D1611B4C7F3A71CD579E1E8F0957BAAB48E (void);
// 0x0000013A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m0000CC2D81B8F28315CCBB9401F3E2C191C767D2 (void);
// 0x0000013B UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::<DOAnchorMax>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_m679B143365FA775A1515D24991CF4655A8E7F1B5 (void);
// 0x0000013C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::<DOAnchorMax>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m8CF05C7D1617D1623FB013A5546FEFC54096C4D7 (void);
// 0x0000013D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m4FD48BD1067B2EC82D34AC8F45DFDCDAF2A01A65 (void);
// 0x0000013E UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::<DOAnchorMin>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_mC77FADB47D8CDC6A3D3B7E72BB48353704EAB95D (void);
// 0x0000013F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::<DOAnchorMin>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m34ABD6F68B660DEB32F7389E737D8A9A3142DFC2 (void);
// 0x00000140 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m865A219F912286F14BD73D44A44A67352076012F (void);
// 0x00000141 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::<DOPivot>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_m628D7EE4274F663EF57D9C6B5352135EDD94303A (void);
// 0x00000142 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::<DOPivot>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_m5B09727CF5FD4531C0BEE60B0AB11A5668AA1863 (void);
// 0x00000143 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m2D23312119F630EF7804933573D7CFB2CB624AEB (void);
// 0x00000144 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::<DOPivotX>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mC879B312D787AA978C651C145219973E08CCBEF7 (void);
// 0x00000145 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::<DOPivotX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m64A3BC621212D7262FEFB19619A70E5827EBCAB2 (void);
// 0x00000146 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m0E5CBD40AEA9118740AF8E4C53EAFEA549673953 (void);
// 0x00000147 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::<DOPivotY>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_m9EC674DED204F15F1C8DB419633B1124640FB886 (void);
// 0x00000148 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::<DOPivotY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_m775C3827A550559B9F49CDF8EB3A55AB5AF43560 (void);
// 0x00000149 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mAEF06B667D476334159A793F538C52C20D14450A (void);
// 0x0000014A UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::<DOSizeDelta>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_mBE6DBDBD3EADB003D181A9DFD11983872C7C442A (void);
// 0x0000014B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m24E5538342C165E3CEA8124B4DDB77A088B9102E (void);
// 0x0000014C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m9E8EEDDCE11B6B023718D4019B73622B199DE48B (void);
// 0x0000014D UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::<DOPunchAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_mC39D8DDD1ECE2E3B5E9E0498EAB9053F87255466 (void);
// 0x0000014E System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_m8FBF0634C0D8B3651BAA35B82C97CB71304DD71F (void);
// 0x0000014F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_mBD2E9C7EDF772EAF8F2B0B0B7CD0A6789880F890 (void);
// 0x00000150 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m0A50C59F05CACD972A9F98BC0BEF2A9E871AAFC2 (void);
// 0x00000151 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_m43994C2202088DD917DA67CBBC8B1DE214C644EF (void);
// 0x00000152 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_mE7A025D8B734E90027C9850483503C9024253CFD (void);
// 0x00000153 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mF4702E1505986C12553795321B548A1A9750F5D9 (void);
// 0x00000154 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_m597A2FFE319F9505FF37D108D03FFDAAB3790835 (void);
// 0x00000155 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_mEDA64593F15A06EFFEF07BDD559238398C180F07 (void);
// 0x00000156 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_m26D1B1FBEC75E21C3BC7460A6F6AEDF3869BD02E (void);
// 0x00000157 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_m8A65ED4244D2D66C8B195C2B5D7895D5F393D693 (void);
// 0x00000158 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__2()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_m3FE34ECBD0DFEAF454F8E3CB6728E47222E320BD (void);
// 0x00000159 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__3()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_m25B6B4053D4DDA398B31C8C3D432CFD64F2D99D1 (void);
// 0x0000015A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_mEB78BA0AEF7347204681525508BAD16E160E627E (void);
// 0x0000015B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__5()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_mA2F4071C8C9EDDD1156F1D27CA95DB4C0251B898 (void);
// 0x0000015C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_mBF1B2ACB453EA5DEB2C7D26ABAD2A0B337511195 (void);
// 0x0000015D UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::<DONormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_m4C242B50758F1472DDF1095D409D95B17596DC0E (void);
// 0x0000015E System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::<DONormalizedPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_m867C1E81139C07A563E97D5C208801EDA9A6997F (void);
// 0x0000015F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_mCAD8CE557B3575FD996CBF8DE85A71B76759397D (void);
// 0x00000160 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_m9F4BDD5987201BF5DFB4886EEE3D36A7A0605609 (void);
// 0x00000161 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mD6ACE8AE7B5B702644E85B11F11744379245F2A5 (void);
// 0x00000162 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_mECC8545D996A1946ACED515283631837338603DF (void);
// 0x00000163 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_m93993E9A0A0393E4A658F3FA8954BD110EFB56B8 (void);
// 0x00000164 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_mCD6DAD94AC8EFF74B635F5F27786A24E8E2B55AF (void);
// 0x00000165 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m8BAA719FDFB93A7041EAEED96573A25252598172 (void);
// 0x00000166 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::<DOValue>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_m571DAD629BB2A7E247A34032822DB78D7C234D3B (void);
// 0x00000167 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::<DOValue>b__1(System.Single)
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_m588F0906D3ACC8114D82D8C51D4C57506C4E8D2A (void);
// 0x00000168 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m1346A0B5A9E068DF5A660F45494935C89240A7E4 (void);
// 0x00000169 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_m395886181C1C9F00F94E95441EAF42A7F4DA5961 (void);
// 0x0000016A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m655020AFBDF0213AEECF03620F60ACBF08F02C50 (void);
// 0x0000016B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m0C2A2269004D87C9B3039CFA62C5929AF472750E (void);
// 0x0000016C System.Int32 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::<DOCounter>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__0_m59AA8701B7D169FA493A52D5E27D224B2B24098F (void);
// 0x0000016D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::<DOCounter>b__1(System.Int32)
extern void U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__1_m48791F78DFE52AD7123EC1CB171D7CEDA89E5D32 (void);
// 0x0000016E System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mF1AC9E7A782807ECF7F5EEAD0BA3DEA7663A0E15 (void);
// 0x0000016F UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_m72863DEF930A265133E32414FF2E46C82790D227 (void);
// 0x00000170 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m31054CF1BD5BBD8C52FD41135422F4791C2C1FBB (void);
// 0x00000171 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_mDB3C942B824C44520D3DBA518428E36C04E67881 (void);
// 0x00000172 System.String DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::<DOText>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__0_mC980985E07BF926EF3EDA4B517AAF971F1BCD9E8 (void);
// 0x00000173 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::<DOText>b__1(System.String)
extern void U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__1_m62B1A67B95362FC16FFB24D81106C886756FEE19 (void);
// 0x00000174 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m41B23D6C59A281568AE8628FF01DFDF420CEB4DD (void);
// 0x00000175 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_m386B242E76F3CA79F0E819516D71413C1BA40D15 (void);
// 0x00000176 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_m7DCABD3F5D0F1A6BCD0DFF422B9B5F94E0F05FF6 (void);
// 0x00000177 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m49CA712882542F1C56DBC8CB70E8E670C1AE933B (void);
// 0x00000178 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_m7EBBEACAFEF74FDCF6F5D266DD0A09A3E8859104 (void);
// 0x00000179 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m68DDF02C1E24B27188E8975BD74D30318F64FAD4 (void);
// 0x0000017A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m37A9BAB59395B45B2C1B6E7F8B2DB5089D66562A (void);
// 0x0000017B UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__0_m317F5EAE3318F8ADCCCFCFB02E8234E891F7C6C4 (void);
// 0x0000017C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__1_mF2152A4D18B012979446027D2F1A9FB11C88806D (void);
// 0x0000017D System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m4F8C823CD49F7430EEB2DAEC4F163845F95F4132 (void);
// 0x0000017E UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::<DOOffset>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m8E405CF03EFF4C3C4D9D73DE056BC984CCFBFD15 (void);
// 0x0000017F System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::<DOOffset>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m2C44275F67B84D88C6978406F74640CAE5F98A8D (void);
// 0x00000180 System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m88A0120B3B465522B86D19206FD5F510DE6EB225 (void);
// 0x00000181 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::<DOTiling>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m13A85E8BF1EBC69FB32059DD893D9D6A80F18A88 (void);
// 0x00000182 System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::<DOTiling>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_mBC9F33C47CB788D0F55943940F95319048812B0A (void);
// 0x00000183 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::MoveNext()
extern void U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m53BB7AAA77CFFB563552DD029D2C14B2B3A235F4 (void);
// 0x00000184 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_mB281D6F4074CA4B8D8A5DF116404A8B3DDCDB281 (void);
// 0x00000185 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::MoveNext()
extern void U3CAsyncWaitForRewindU3Ed__11_MoveNext_m3E7CC965C8B33BFE1C281C3D6A2DDF3B781F3F6C (void);
// 0x00000186 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mCC8B4F2EAFA2D6D42E1EC0FD856DCD46CD0998EC (void);
// 0x00000187 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::MoveNext()
extern void U3CAsyncWaitForKillU3Ed__12_MoveNext_m7F739727954790989E703DD2BC36A7CBE87D0B89 (void);
// 0x00000188 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m7061BB49F48763C47ABF5C459AC152607C362358 (void);
// 0x00000189 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::MoveNext()
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_m6985530DD3AA94DD8BCD36C455EEBE62FD432E6F (void);
// 0x0000018A System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m003ABD1151A01F9FE30FD1C8FC494CEF141C1E19 (void);
// 0x0000018B System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::MoveNext()
extern void U3CAsyncWaitForPositionU3Ed__14_MoveNext_m1577C29F05ABDB7F6F3C614EE9D50E16959077D7 (void);
// 0x0000018C System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_m4644A2CA89AB22E4F1EEF3D10C658340266C2FCC (void);
// 0x0000018D System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::MoveNext()
extern void U3CAsyncWaitForStartU3Ed__15_MoveNext_mEA777818FD7DCE82624C0E60A57BBBCB7FFDAA2D (void);
// 0x0000018E System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForStartU3Ed__15_SetStateMachine_m746EB287294C0C65BF9EF0CA89D9F7B04DFD4DDE (void);
// 0x0000018F System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForCompletion::get_keepWaiting()
extern void WaitForCompletion_get_keepWaiting_m8B0961B44B29700998F40013D0DFA462ACE6164C (void);
// 0x00000190 System.Void DG.Tweening.DOTweenCYInstruction/WaitForCompletion::.ctor(DG.Tweening.Tween)
extern void WaitForCompletion__ctor_mBCE4900965B00C7C8B07C11599DB9C5EF7C0DB2B (void);
// 0x00000191 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForRewind::get_keepWaiting()
extern void WaitForRewind_get_keepWaiting_mB8039B20DAB0A5B3AD08C48FF011CE56CA5AFA36 (void);
// 0x00000192 System.Void DG.Tweening.DOTweenCYInstruction/WaitForRewind::.ctor(DG.Tweening.Tween)
extern void WaitForRewind__ctor_m8A743D8EDA6B53753132BB6B2BBB4BF95863269E (void);
// 0x00000193 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForKill::get_keepWaiting()
extern void WaitForKill_get_keepWaiting_m04AE96DD30619281334E76D4477FACD6E4EB4938 (void);
// 0x00000194 System.Void DG.Tweening.DOTweenCYInstruction/WaitForKill::.ctor(DG.Tweening.Tween)
extern void WaitForKill__ctor_mA4DF17E431350F30D2B035A3044FBB9518B463F0 (void);
// 0x00000195 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::get_keepWaiting()
extern void WaitForElapsedLoops_get_keepWaiting_m5A9CF13BB9065A506FA38DDDDD11C469F8961282 (void);
// 0x00000196 System.Void DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::.ctor(DG.Tweening.Tween,System.Int32)
extern void WaitForElapsedLoops__ctor_mD1DCB74D74DFF0544F9DD0ECFCB5B843ED820478 (void);
// 0x00000197 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForPosition::get_keepWaiting()
extern void WaitForPosition_get_keepWaiting_m36E26FBE3FD2E8FA7AD365D6BE60CC4AE8489C39 (void);
// 0x00000198 System.Void DG.Tweening.DOTweenCYInstruction/WaitForPosition::.ctor(DG.Tweening.Tween,System.Single)
extern void WaitForPosition__ctor_mF059542AE7EDB09EE57B3A67AAF741DDD4778601 (void);
// 0x00000199 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForStart::get_keepWaiting()
extern void WaitForStart_get_keepWaiting_mFAC851C1E4D398A41076EAA2F63258AB220D8C52 (void);
// 0x0000019A System.Void DG.Tweening.DOTweenCYInstruction/WaitForStart::.ctor(DG.Tweening.Tween)
extern void WaitForStart__ctor_mC8A9A805AD9A41782CDB8E4156F661D5B63CF1D2 (void);
// 0x0000019B System.Void DG.Tweening.DOTweenModuleUtils/Physics::SetOrientationOnPath(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Physics_SetOrientationOnPath_m8C23022A41E6D526E7B6C54C948A6F72B80F0182 (void);
// 0x0000019C System.Boolean DG.Tweening.DOTweenModuleUtils/Physics::HasRigidbody2D(UnityEngine.Component)
extern void Physics_HasRigidbody2D_mEF17D135EC4B317363B799DF0DE9A8B8764C7CB4 (void);
// 0x0000019D System.Boolean DG.Tweening.DOTweenModuleUtils/Physics::HasRigidbody(UnityEngine.Component)
extern void Physics_HasRigidbody_m24799D22419D4640E42A704221C086B6B60B98A6 (void);
// 0x0000019E DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModuleUtils/Physics::CreateDOTweenPathTween(UnityEngine.MonoBehaviour,System.Boolean,System.Boolean,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void Physics_CreateDOTweenPathTween_mD91665CB83CC7477D41957AAE719C05CD4DB013F (void);
// 0x0000019F System.Void DG.Tweening.DOTweenProShortcuts/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mCA33651A4C9BAFBDC5B3EA1AE3B067D41B70BDEF (void);
// 0x000001A0 UnityEngine.Vector3 DG.Tweening.DOTweenProShortcuts/<>c__DisplayClass1_0::<DOSpiral>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOSpiralU3Eb__0_m3545A1B63D5FF61C6934F3F7F568C60109764F55 (void);
// 0x000001A1 System.Void DG.Tweening.DOTweenProShortcuts/<>c__DisplayClass1_0::<DOSpiral>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass1_0_U3CDOSpiralU3Eb__1_m266C4EA9AA325F1999A88D392150A876D6C20702 (void);
// 0x000001A2 System.Void DG.Tweening.DOTweenProShortcuts/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m7BC1E9D40A414AD376A9BDFA3BA249C52DF4882B (void);
// 0x000001A3 UnityEngine.Vector3 DG.Tweening.DOTweenProShortcuts/<>c__DisplayClass2_0::<DOSpiral>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOSpiralU3Eb__0_m61D3D7FEF99911050A6B46935FF15541E82C53AE (void);
// 0x000001A4 System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mEBC1C720C79D847C19B50A63966893CFEE43F842 (void);
// 0x000001A5 UnityEngine.Color DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass0_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m95B7F6A07959A0DFAB9012ECCDFACCCB92EAF61C (void);
// 0x000001A6 System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass0_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m41E14FD6B6105AFD18CA839DD78882A845D2DCD2 (void);
// 0x000001A7 System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mD3A70E9F18297DF3A0306570A2159CBFA63210D6 (void);
// 0x000001A8 UnityEngine.Color DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass1_0::<DOFaceColor>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOFaceColorU3Eb__0_m9EB41EBAD131FEA2F852EBD662203CB56D34DE9D (void);
// 0x000001A9 System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass1_0::<DOFaceColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOFaceColorU3Eb__1_mD2F84AFD42FA2CA18F6E5D028ED638A87828A2F5 (void);
// 0x000001AA System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m8DB7BC71FFDC9972AAD385463F7F53E8A120A85B (void);
// 0x000001AB UnityEngine.Color DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass2_0::<DOOutlineColor>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOOutlineColorU3Eb__0_m95474CD26D00BE2B758C3DA0A0EA048BA943FAC7 (void);
// 0x000001AC System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass2_0::<DOOutlineColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass2_0_U3CDOOutlineColorU3Eb__1_m8E1CB02CC7890BFAD7FE3D24BD8CA38A2D85FD4F (void);
// 0x000001AD System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mE8A5BE5691848CC6E8ADD4424446F3F6DC0A5DF2 (void);
// 0x000001AE UnityEngine.Color DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass4_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_mE457BB2E7E671A74FD757F70EB566CC6C12D0102 (void);
// 0x000001AF System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m3B73407A005BD6A7F9E62F9882E4BEE31D20B984 (void);
// 0x000001B0 System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mF6496FF71D08236F0F379C13FDA889E47343F61D (void);
// 0x000001B1 UnityEngine.Color DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass5_0::<DOFaceFade>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOFaceFadeU3Eb__0_m04D9ECD99A5C18DE82A425E8748716DC76AE59A7 (void);
// 0x000001B2 System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass5_0::<DOFaceFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass5_0_U3CDOFaceFadeU3Eb__1_m1529421C97D98BE190EF75A525E9CED90BAE5947 (void);
// 0x000001B3 System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m6CD7B56A136A74EA6A016264BB44ECE9480A9CA8 (void);
// 0x000001B4 UnityEngine.Vector3 DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass6_0::<DOScale>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDOScaleU3Eb__0_m5F25C281472D7C9BB7210CBE8FC184B2830BA09E (void);
// 0x000001B5 System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass6_0::<DOScale>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass6_0_U3CDOScaleU3Eb__1_mFFA7F71E6188451CBF72D0557614A35C640F34FE (void);
// 0x000001B6 System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m051353B10E58213A7E342CEC468E18BA98B4BE06 (void);
// 0x000001B7 System.Int32 DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass7_0::<DOCounter>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOCounterU3Eb__0_mA2223BE4B007FC694DEC95FE9EFA406731FBFD51 (void);
// 0x000001B8 System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass7_0::<DOCounter>b__1(System.Int32)
extern void U3CU3Ec__DisplayClass7_0_U3CDOCounterU3Eb__1_mDBD811A0E7A22FBC88FFF278A927E6C220661E14 (void);
// 0x000001B9 System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m648E81874697266D01238C255687E0D36FF79AC2 (void);
// 0x000001BA System.Single DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass8_0::<DOFontSize>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOFontSizeU3Eb__0_m4C7BA0881A493DA9AFEB8D8C03420AD6BC432BBE (void);
// 0x000001BB System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass8_0::<DOFontSize>b__1(System.Single)
extern void U3CU3Ec__DisplayClass8_0_U3CDOFontSizeU3Eb__1_mB264CF20A9C6D5D1FD565B2DF7E857F9EE11FCA8 (void);
// 0x000001BC System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m229C4CB85B7B3BA914EBA1C4A57A563C62380497 (void);
// 0x000001BD System.Int32 DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass9_0::<DOMaxVisibleCharacters>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOMaxVisibleCharactersU3Eb__0_mEE8BCB74655D900F8D6DB05DD665C8061E377FFC (void);
// 0x000001BE System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass9_0::<DOMaxVisibleCharacters>b__1(System.Int32)
extern void U3CU3Ec__DisplayClass9_0_U3CDOMaxVisibleCharactersU3Eb__1_m37C8F6FEDE9E24235CE80019C90A0BB903F7E4AA (void);
// 0x000001BF System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m1407CF1E8857BC1FBA67C89CBC46960E419F2B90 (void);
// 0x000001C0 System.String DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass10_0::<DOText>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOTextU3Eb__0_m854E8E3BAB1276B9EC913BE14EB09E74E9AEEF7E (void);
// 0x000001C1 System.Void DG.Tweening.ShortcutExtensionsTMPText/<>c__DisplayClass10_0::<DOText>b__1(System.String)
extern void U3CU3Ec__DisplayClass10_0_U3CDOTextU3Eb__1_m2CDBEB6ADF4D7837446632EC320E8AA084231793 (void);
// 0x000001C2 System.Void DG.Tweening.DOTweenTMPAnimator/CharVertices::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void CharVertices__ctor_m1FCBFBAFD52DDACE798A2EDABF552B5E33578F5F (void);
// 0x000001C3 System.Boolean DG.Tweening.DOTweenTMPAnimator/CharTransform::get_isVisible()
extern void CharTransform_get_isVisible_m97898D0FF5F7CA81E23A0E8F2EF1E8BADDFF4DD2 (void);
// 0x000001C4 System.Void DG.Tweening.DOTweenTMPAnimator/CharTransform::set_isVisible(System.Boolean)
extern void CharTransform_set_isVisible_mF97ED6A45E13DA5C5318D64DF3013281EF85ECEC (void);
// 0x000001C5 System.Void DG.Tweening.DOTweenTMPAnimator/CharTransform::.ctor(System.Int32,TMPro.TMP_TextInfo,TMPro.TMP_MeshInfo[])
extern void CharTransform__ctor_m5FA41A9B1244DEF0906D681E5D80D0C5B6F9793F (void);
// 0x000001C6 System.Void DG.Tweening.DOTweenTMPAnimator/CharTransform::Refresh(TMPro.TMP_TextInfo,TMPro.TMP_MeshInfo[])
extern void CharTransform_Refresh_m3DB3034DB6BF2CF7D5835F60F518DF8F3BE58986 (void);
// 0x000001C7 System.Void DG.Tweening.DOTweenTMPAnimator/CharTransform::ResetAll(TMPro.TMP_Text,TMPro.TMP_MeshInfo[],TMPro.TMP_MeshInfo[])
extern void CharTransform_ResetAll_m68EEBE49DF0B5A8B3A75DBB7C1086DA57461BD76 (void);
// 0x000001C8 System.Void DG.Tweening.DOTweenTMPAnimator/CharTransform::ResetTransformationData()
extern void CharTransform_ResetTransformationData_mE1856880BEAB9581C30A0478CFD050ADA4808E1D (void);
// 0x000001C9 System.Void DG.Tweening.DOTweenTMPAnimator/CharTransform::ResetGeometry(TMPro.TMP_Text,TMPro.TMP_MeshInfo[])
extern void CharTransform_ResetGeometry_m237FE94A9C2338F16DE6532CD1E512C18DB2B9F3 (void);
// 0x000001CA System.Void DG.Tweening.DOTweenTMPAnimator/CharTransform::ResetColors(TMPro.TMP_Text,TMPro.TMP_MeshInfo[])
extern void CharTransform_ResetColors_m27F59B44398219ED6B8CA45881A230ABE0E87FE1 (void);
// 0x000001CB UnityEngine.Color32 DG.Tweening.DOTweenTMPAnimator/CharTransform::GetColor(TMPro.TMP_MeshInfo[])
extern void CharTransform_GetColor_mEFF9D07B75A32FDEF7C893EC282E0F41F0D9BF81 (void);
// 0x000001CC DG.Tweening.DOTweenTMPAnimator/CharVertices DG.Tweening.DOTweenTMPAnimator/CharTransform::GetVertices()
extern void CharTransform_GetVertices_m38B3288CB017F93D4F5393B63C3349C4370FE71C (void);
// 0x000001CD System.Void DG.Tweening.DOTweenTMPAnimator/CharTransform::UpdateAlpha(TMPro.TMP_Text,UnityEngine.Color,TMPro.TMP_MeshInfo[],System.Boolean)
extern void CharTransform_UpdateAlpha_mA3B92AF01736C296EFBB7C4C28FFB6B220F3C0A3 (void);
// 0x000001CE System.Void DG.Tweening.DOTweenTMPAnimator/CharTransform::UpdateColor(TMPro.TMP_Text,UnityEngine.Color32,TMPro.TMP_MeshInfo[],System.Boolean)
extern void CharTransform_UpdateColor_m87B9B3A77292EF39A4013218773CF9AAE5A3A7A1 (void);
// 0x000001CF System.Void DG.Tweening.DOTweenTMPAnimator/CharTransform::UpdateGeometry(TMPro.TMP_Text,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,TMPro.TMP_MeshInfo[],System.Boolean)
extern void CharTransform_UpdateGeometry_mE90A834A75963030FC69703AE6137436B616D066 (void);
// 0x000001D0 System.Void DG.Tweening.DOTweenTMPAnimator/CharTransform::ShiftVertices(TMPro.TMP_Text,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void CharTransform_ShiftVertices_mD41EF655A839834A7EDB80474B9E1CB396FB5500 (void);
// 0x000001D1 System.Void DG.Tweening.DOTweenTMPAnimator/CharTransform::ResetVerticesShift(TMPro.TMP_Text)
extern void CharTransform_ResetVerticesShift_m901074CEF0A096D4ED59365924F959045CCC362A (void);
// 0x000001D2 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_m7D4FDB2D12D71B4B0675399D5E51FF54B7815628 (void);
// 0x000001D3 UnityEngine.Color DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass32_0::<DOFadeChar>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CDOFadeCharU3Eb__0_mE72F0304748B71608B06BDDCC9D24198D31860E0 (void);
// 0x000001D4 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass32_0::<DOFadeChar>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass32_0_U3CDOFadeCharU3Eb__1_m606CACDE3B0031175F930BA0440A18DCB5573C2A (void);
// 0x000001D5 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m3CBDED481539780EE909C4D0AC56CB6D0FAEE507 (void);
// 0x000001D6 UnityEngine.Color DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass33_0::<DOColorChar>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CDOColorCharU3Eb__0_mEFC9894705586383A07DBF13C7B5819312745E84 (void);
// 0x000001D7 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass33_0::<DOColorChar>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass33_0_U3CDOColorCharU3Eb__1_mECA00476DDB787DB96D496C0E6BFCCB5A9999F4A (void);
// 0x000001D8 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mAAED55EE2A2FACDCD6688CD39CCC03CF4980C430 (void);
// 0x000001D9 UnityEngine.Vector3 DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass34_0::<DOOffsetChar>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CDOOffsetCharU3Eb__0_m749362D65F56CC5D9323ECDF5C556872F7E848E6 (void);
// 0x000001DA System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass34_0::<DOOffsetChar>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass34_0_U3CDOOffsetCharU3Eb__1_m4FB2192A4D86B7C5EBC336BF380E5B6F0FDEACD5 (void);
// 0x000001DB System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m9310D1BB6A4BAE2F3B166D40640AA539CC8090BB (void);
// 0x000001DC UnityEngine.Quaternion DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass35_0::<DORotateChar>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CDORotateCharU3Eb__0_m3D5203BA104AF85D985C13AD23DFB45ADD5F1103 (void);
// 0x000001DD System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass35_0::<DORotateChar>b__1(UnityEngine.Quaternion)
extern void U3CU3Ec__DisplayClass35_0_U3CDORotateCharU3Eb__1_mA45F56F418829A8295C8577BBA3231684AA3B959 (void);
// 0x000001DE System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_m68054C6FD917CD74993E5AB0E8A5D8BB5CDDF7D4 (void);
// 0x000001DF UnityEngine.Vector3 DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass37_0::<DOScaleChar>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CDOScaleCharU3Eb__0_m27A0754CF952093C19B68DEC73F5D589A7D266EA (void);
// 0x000001E0 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass37_0::<DOScaleChar>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass37_0_U3CDOScaleCharU3Eb__1_m5D352034B738D87BFEF2FB0A66DF1C8D3C1D3E95 (void);
// 0x000001E1 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m522E116755B166DBEF447E2FCBFAE4D64EABDA71 (void);
// 0x000001E2 UnityEngine.Vector3 DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass38_0::<DOPunchCharOffset>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CDOPunchCharOffsetU3Eb__0_mEB117773BB5C8E6577BFE26D4EA3A6936B3041C1 (void);
// 0x000001E3 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass38_0::<DOPunchCharOffset>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass38_0_U3CDOPunchCharOffsetU3Eb__1_m03B79EA169E8B28734921381C5CE3BFEF316CB2A (void);
// 0x000001E4 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m14456F59E7A699089AE521E5424E9E08AB89E382 (void);
// 0x000001E5 UnityEngine.Vector3 DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass39_0::<DOPunchCharRotation>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CDOPunchCharRotationU3Eb__0_m3740742E7C9F5B51567E75E752B661C73EEFEEDD (void);
// 0x000001E6 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass39_0::<DOPunchCharRotation>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass39_0_U3CDOPunchCharRotationU3Eb__1_mE51E8C07D4C5542172AD93A9F4AD5FD0C8FF6F31 (void);
// 0x000001E7 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_mFB320502276555AEDFFA47DA609ADE212334BFCD (void);
// 0x000001E8 UnityEngine.Vector3 DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass41_0::<DOPunchCharScale>b__0()
extern void U3CU3Ec__DisplayClass41_0_U3CDOPunchCharScaleU3Eb__0_m89605520DB89C64B63A7FCC28F90403D03A4201D (void);
// 0x000001E9 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass41_0::<DOPunchCharScale>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass41_0_U3CDOPunchCharScaleU3Eb__1_m1A24D26AF729657FB12D1870334DBD88E8233E35 (void);
// 0x000001EA System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_m81BC63A222611EE7F35B1095FC78943FDBBA2027 (void);
// 0x000001EB UnityEngine.Vector3 DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass43_0::<DOShakeCharOffset>b__0()
extern void U3CU3Ec__DisplayClass43_0_U3CDOShakeCharOffsetU3Eb__0_m85226B06E888AE781A499D7262F8F8AEF1FE7E89 (void);
// 0x000001EC System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass43_0::<DOShakeCharOffset>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass43_0_U3CDOShakeCharOffsetU3Eb__1_mAA2936E2FE58BF767465677745A2B9FE807351F6 (void);
// 0x000001ED System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m27BA42C3153CA56D442B19D3961961A7983802C0 (void);
// 0x000001EE UnityEngine.Vector3 DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass44_0::<DOShakeCharRotation>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CDOShakeCharRotationU3Eb__0_mFE27C33BE386A5B99D3DC87F835F5D74AC7CD9AD (void);
// 0x000001EF System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass44_0::<DOShakeCharRotation>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass44_0_U3CDOShakeCharRotationU3Eb__1_m2006BF303D6A052DFBCFD0302708B1A75D356DC8 (void);
// 0x000001F0 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_mF521FBE68DF94B9231D4CEB6A8DD2A07344E7E10 (void);
// 0x000001F1 UnityEngine.Vector3 DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass46_0::<DOShakeCharScale>b__0()
extern void U3CU3Ec__DisplayClass46_0_U3CDOShakeCharScaleU3Eb__0_mEE481A349E56AD3F0CB114A0527078A92151BBF0 (void);
// 0x000001F2 System.Void DG.Tweening.DOTweenTMPAnimator/<>c__DisplayClass46_0::<DOShakeCharScale>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass46_0_U3CDOShakeCharScaleU3Eb__1_m702C078F2C3428BAF87D4FBAB93EDD4A621B0122 (void);
static Il2CppMethodPointer s_methodPointers[498] = 
{
	DOTweenModuleAudio_DOFade_mB942166E905FA9D9591788D9E19C1D8BD8464AA5,
	DOTweenModuleAudio_DOPitch_m7B75396CF1F0D9E46C4B73B8F14648D5D5CD462B,
	DOTweenModuleAudio_DOSetFloat_m2328D1896174D227CDFA764920A45F0270421F9F,
	DOTweenModuleAudio_DOComplete_m11E2C3F50B96B828F59424773B84A12221BFD84E,
	DOTweenModuleAudio_DOKill_mB33A6E7EC2957B57687C32DB366C2BBECB9B0011,
	DOTweenModuleAudio_DOFlip_m5DDA9A07C4A05BE9D0945D975396164E54267AEA,
	DOTweenModuleAudio_DOGoto_mF7F74E5CB43D64EDBD4D29157BBCCE7B0A708C94,
	DOTweenModuleAudio_DOPause_m22322C2DD0A0BB336568781D42993A44AAB14F71,
	DOTweenModuleAudio_DOPlay_m9C7A6CFD0F43348126CCE5658FBB206F425E543A,
	DOTweenModuleAudio_DOPlayBackwards_mC3E6D0146E8C8FE0B527DEFCF4EC5D11A187B92C,
	DOTweenModuleAudio_DOPlayForward_m628B5E119C53D71FD7BE4872BC5F41F1AC318FD0,
	DOTweenModuleAudio_DORestart_m558B81091AD5100605BEF48D42864A39B4A79858,
	DOTweenModuleAudio_DORewind_m631E833B8DFE7644B27B86ACA549F4DE7E1BDE4D,
	DOTweenModuleAudio_DOSmoothRewind_m0155C90819F6AE27F58A6D84585F64CA7482950D,
	DOTweenModuleAudio_DOTogglePause_mCAA35FA3A42138FF4CD14F8445C0C3C5F0EE69CB,
	DOTweenModulePhysics_DOMove_mCD4035AEE8BDF7C32463165F9FEE4D0D53EA07D7,
	DOTweenModulePhysics_DOMoveX_mABFFF46A3FABA32696A6C27A46FF5DB69EDA1446,
	DOTweenModulePhysics_DOMoveY_mAE39D31F9966CB0AFCACD360BEC1F704B347FF8F,
	DOTweenModulePhysics_DOMoveZ_m484788D4764841E99F3CE57CF9A5EE5BB09702C3,
	DOTweenModulePhysics_DORotate_mCE061CE2863BA397B95DC4CBD201262DFB315364,
	DOTweenModulePhysics_DOLookAt_mF7E3F17E02A027586E3D08A424AB3B0FFBA8FCDF,
	DOTweenModulePhysics_DOJump_m4BC92999B54B934B81B55E1CB91AEE210BECA09D,
	DOTweenModulePhysics_DOPath_m4B7322AC2520EF2663F6773EF009C9E7FB91905D,
	DOTweenModulePhysics_DOLocalPath_m99E7D1A2F80161D531600D1ABF48377D4470CC26,
	DOTweenModulePhysics_DOPath_m2ADD94A81FE5B4C3A3E3980A682F90E797960E6C,
	DOTweenModulePhysics_DOLocalPath_m5FD44D934098449B3A701AAED8A6CAAB8EE73B3D,
	DOTweenModulePhysics2D_DOMove_m7347A43EB8A8C27D168ABF80BF96E536FB9A72CC,
	DOTweenModulePhysics2D_DOMoveX_m537956DDFACEA2C4337BE948E07CE3409B2E2F6F,
	DOTweenModulePhysics2D_DOMoveY_m2AE2056F86B1E0EB3FBC1E1DF7A785B4AD96064D,
	DOTweenModulePhysics2D_DORotate_m915B7CE83E09FE79FC09AF3872B994E02835AB68,
	DOTweenModulePhysics2D_DOJump_m4A1D9450DA958808ABD3A074BD09890BF0CD3D6C,
	DOTweenModulePhysics2D_DOPath_m2843CECE99E3C4D9501C8F0151A3F865ADE17831,
	DOTweenModulePhysics2D_DOLocalPath_m1217AE34C36C424ED747337BC5775804567640F2,
	DOTweenModuleSprite_DOColor_m78D84C92828C97E15E16AA9CF722983492371EE7,
	DOTweenModuleSprite_DOFade_m30875D140C666F476ABF36662A9CAFF8C968F8A5,
	DOTweenModuleSprite_DOGradientColor_m20E2FF126401EA952485086F997FF4F0E9295E8B,
	DOTweenModuleSprite_DOBlendableColor_mF49F831B198670BD34E4D58FC3D71FF67B064C20,
	DOTweenModuleUI_DOFade_mAF6D3F44B8AFBAA181BCF8B68310D8AB2F7E4884,
	DOTweenModuleUI_DOColor_mEC16AEF6B41EB5B61C31C741F818BC40581F9FDC,
	DOTweenModuleUI_DOFade_m7CC2AB868F4854FD40FB375A4EBA5E271E55E1E6,
	DOTweenModuleUI_DOColor_m718B3DEBC00EF8F375DC3DD3DCE4C07CDB1A6366,
	DOTweenModuleUI_DOFade_m2B58045B88BED858C0DB4D385B237E16CD4B8E53,
	DOTweenModuleUI_DOFillAmount_mA515465CCACB47453C4387A65E18914F03299B1C,
	DOTweenModuleUI_DOGradientColor_m0C2322D164913566D07FC462DE0C9F9DA689B485,
	DOTweenModuleUI_DOFlexibleSize_m01B661DD6C8BEF38AF06BA083B989AA1D5B59C54,
	DOTweenModuleUI_DOMinSize_m2395DFBE826FFD1A4A342626272AA06CEE43194F,
	DOTweenModuleUI_DOPreferredSize_mFC7CD0E43C0E069DB79DA62850119516F0AA2649,
	DOTweenModuleUI_DOColor_mFC0417A98B850B32BE0D6843D526857328165BB9,
	DOTweenModuleUI_DOFade_m32380BAFA874FE4D8A6773E288A414B626FF916E,
	DOTweenModuleUI_DOScale_m5A38F52D5560590F080984855506004587604DF1,
	DOTweenModuleUI_DOAnchorPos_mCB659BC7116CAF3401EF356F3C161879FB36BA57,
	DOTweenModuleUI_DOAnchorPosX_m1A146C9DD5C93C43A0931C6FF73A64B35386DC8C,
	DOTweenModuleUI_DOAnchorPosY_m58BC17DAAC54B227E4EBA8F108DC7ECAC3AF48BB,
	DOTweenModuleUI_DOAnchorPos3D_mF92C8F167F799A05852981C2C3610719DC7DF93D,
	DOTweenModuleUI_DOAnchorPos3DX_m0FB309BC42714965D6ADB08659FF9954EFBDCB03,
	DOTweenModuleUI_DOAnchorPos3DY_m0742CD6111A0578EB2418F59BB80B1F7D44AD652,
	DOTweenModuleUI_DOAnchorPos3DZ_m3EC428DD891F5F13CB490E7212ADD728BBD11BA8,
	DOTweenModuleUI_DOAnchorMax_m7F276F9C402B5D6169D0337036BB97F5A929CFF8,
	DOTweenModuleUI_DOAnchorMin_mDB3F3E1FFE30724A6912CBFDE5F519FAB4BABC76,
	DOTweenModuleUI_DOPivot_mD30001895FDC39E39D55FF0396EA56ACFF2AFAD1,
	DOTweenModuleUI_DOPivotX_m9DC0A394A9D0C43385FBAF25E0C398124DA5C9EC,
	DOTweenModuleUI_DOPivotY_m7960E5F973345C93BD10982EC01FB57A4716A6C1,
	DOTweenModuleUI_DOSizeDelta_m127667EF01B3F23E153AE4DC626B924E3574D54B,
	DOTweenModuleUI_DOPunchAnchorPos_m3F25051D101AF8FCE2C58503B2083BB93969E6B1,
	DOTweenModuleUI_DOShakeAnchorPos_mF9176FEC11DC9855990CB010C7503A15420C80EE,
	DOTweenModuleUI_DOShakeAnchorPos_m38841320F201AFFE9723D7C9D6D41AE8EC63A9DB,
	DOTweenModuleUI_DOJumpAnchorPos_m362656D22092B92BE5D600CEFD905963BEACC972,
	DOTweenModuleUI_DONormalizedPos_m9D75DFE5046F51C8D6851ECDD86B109AAFD1488D,
	DOTweenModuleUI_DOHorizontalNormalizedPos_m033060F4D73139E7AA4932E33D6437B15648C578,
	DOTweenModuleUI_DOVerticalNormalizedPos_mD800102A319273558E7303454140ADB8AE93F3B4,
	DOTweenModuleUI_DOValue_mAACC2D6EC891D068A6AA3716ED1DB6ABBB212505,
	DOTweenModuleUI_DOColor_mBEE7A9C925CFE32B127E87920EBCD45C8A26FE6A,
	DOTweenModuleUI_DOCounter_m534037AA0B6DC4FBC95AD3BD8353BB9B94168F12,
	DOTweenModuleUI_DOFade_m16D54582B55159BE2BAC25DF35060EA524419CBB,
	DOTweenModuleUI_DOText_mAB85134929DEA1CEE706938AA13C13119F933722,
	DOTweenModuleUI_DOBlendableColor_m66476F9823E43CA3ED1E517CE0DA20E010A9045A,
	DOTweenModuleUI_DOBlendableColor_m59697DE967F8922CBFBFAF259F460C70EE79D68A,
	DOTweenModuleUI_DOBlendableColor_m62E6B788F5E1F444B1DD391D634E1011826787D7,
	DOTweenModuleUnityVersion_DOGradientColor_m0A96FA6972F7A27A6DBA12ACF29FC5CEC4134D11,
	DOTweenModuleUnityVersion_DOGradientColor_m86806E97FBC86955091F65786E676E9ACC7066F1,
	DOTweenModuleUnityVersion_WaitForCompletion_mC857E9A4A1C9D65EB21AFF7E5A162122FE0C7269,
	DOTweenModuleUnityVersion_WaitForRewind_m0C19662E8F77E32AB6C5125669ED5BD45C8D31C4,
	DOTweenModuleUnityVersion_WaitForKill_m567D0C09631A3988277CE5B47C2C2C5F9748DE92,
	DOTweenModuleUnityVersion_WaitForElapsedLoops_m975AAA3CF52F0F39647CEA3E1DB61F15B0F52FF7,
	DOTweenModuleUnityVersion_WaitForPosition_m5802432A7EC6CB2964C531BDE898592023B8AEDA,
	DOTweenModuleUnityVersion_WaitForStart_m2C083E752ADAF55CA262101863B5A8A6244FB5D5,
	DOTweenModuleUnityVersion_DOOffset_mEED1CC56BEC984D8B39B6375A3115504DB7665DD,
	DOTweenModuleUnityVersion_DOTiling_mED7929FE921959BC6A62840B61349C022E41BCA4,
	DOTweenModuleUnityVersion_AsyncWaitForCompletion_m151975C66F07E7ACE030674B69C85F9F67119973,
	DOTweenModuleUnityVersion_AsyncWaitForRewind_m6E81BCFBA5B66CABC05453991D43615F4B90A220,
	DOTweenModuleUnityVersion_AsyncWaitForKill_m6E6F64F4B0698A871E4CF56FC93DEBC34537F224,
	DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_m62F89260C2AF682A39A3C2FDA9E0326B562140D5,
	DOTweenModuleUnityVersion_AsyncWaitForPosition_mBBD8B73E2413D3599602F683F0F7B59AE9F2BC2E,
	DOTweenModuleUnityVersion_AsyncWaitForStart_mCC42BB0C1B1CBEE945664E2F76D24DE98893761C,
	DOTweenModuleUtils_Init_m2A66692E84542706CF558924C6D142423A389571,
	DOTweenModuleUtils_Preserver_mE297479CDF906C16415F282D644E84094A8152CB,
	DOTweenAnimation_add_OnReset_mE2AC726A3A8FD2B0D79BFC16C9ACF92B68F939CD,
	DOTweenAnimation_remove_OnReset_mF3F9100DFDB441A1F78030B19AFF52E72F855AD4,
	DOTweenAnimation_Dispatch_OnReset_m0CC7F0F335E78A9F7967F4B8DADA4E364DF05492,
	DOTweenAnimation_Awake_m786322982931DFF1165275689D9745B21F4FAD63,
	DOTweenAnimation_Start_mD2D408605D0824A28F80B1351E6A3481103A7755,
	DOTweenAnimation_Reset_m3EDF475C4E7D85D56DDCAA91B336EE1476B42FA7,
	DOTweenAnimation_OnDestroy_m3C875DB76703362F7DFB8A38EFF3D98C2DA75FB2,
	DOTweenAnimation_CreateTween_m11A99128B8D021E1D63E8BF7AAFAB39C4B9D96DE,
	DOTweenAnimation_DOPlay_m8E2BD848084B87B63786FB940EE174C5DBB22F37,
	DOTweenAnimation_DOPlayBackwards_mF7C18418FB1AB4FDE4A9DE9327086607F4A9CC7C,
	DOTweenAnimation_DOPlayForward_mB23F05A8D08303B18A7A0F50881B75DF9BB43E70,
	DOTweenAnimation_DOPause_m834CD52451DCCD48AA0AFC07B4521005E8A0A0FE,
	DOTweenAnimation_DOTogglePause_m39453B68EAF1A48DF9AF2572FAB52FC33C3AD0C7,
	DOTweenAnimation_DORewind_m8A44C443CC5130594B43C36FF7DD38116F3112CE,
	DOTweenAnimation_DORestart_m7CDA1E4ABAF0416F9EEC93DACB190313FED04CE8,
	DOTweenAnimation_DORestart_mA35D60C880CC89F09BBDDA537D7C264E8926B9FF,
	DOTweenAnimation_DOComplete_mB32CB25E1C5A543800CAC2E9A30317F25F81F8CD,
	DOTweenAnimation_DOKill_m3D1F2CF86B02F0D8EBB21BF7DB47C328A62FA3FD,
	DOTweenAnimation_DOPlayById_m3897F8A67F05D47A006DF9B1364F844FFD10F565,
	DOTweenAnimation_DOPlayAllById_mEDB4F798D2C29F64001B48D937A2EB299F238523,
	DOTweenAnimation_DOPauseAllById_m372817130FF60EA203642D8B88F4A7154D75421D,
	DOTweenAnimation_DOPlayBackwardsById_m83411715B049B61D9515871D8D7E6E14468AD53F,
	DOTweenAnimation_DOPlayBackwardsAllById_mA8B6DC6E2F0BFD4A6411E293A6C164AD7ED8DFDC,
	DOTweenAnimation_DOPlayForwardById_mDC67FCE1EDACB7EBBA75DB5133F401E85CDA3A69,
	DOTweenAnimation_DOPlayForwardAllById_m7B03693886B66F9C78A2BD9DDFE52C85DE713124,
	DOTweenAnimation_DOPlayNext_m0FFF4DE3D3C44AC04FFC2098C33A77AE858664D7,
	DOTweenAnimation_DORewindAndPlayNext_m61310307A43B54A051B66661F411595BD94A7BB9,
	DOTweenAnimation_DORewindAllById_mC053DD42517AA16874EA764E2177AC16B8ABDB18,
	DOTweenAnimation_DORestartById_m4B1EC2CC857288BA99E308C221DC924BC2A51CCE,
	DOTweenAnimation_DORestartAllById_mA6CA25E46D3786E89B858B621FBEE49FB719DED2,
	DOTweenAnimation_GetTweens_m927F01FD07C0745158CBB384369195CC4D925AA3,
	DOTweenAnimation_TypeToDOTargetType_m0B84A99759642746277D13B730F0954E4DA30002,
	DOTweenAnimation_CreateEditorPreview_mDCA6EA8D733B8E310BDF04551B6E7D10950FDB42,
	DOTweenAnimation_GetTweenGO_mEA4F79F8CC07E17EDF50897CBA794BCEB5DEA7C8,
	DOTweenAnimation_ReEvaluateRelativeTween_m77C2A5D243BFB40C6FC8090EFF6DBE1F27EFC176,
	DOTweenAnimation__ctor_m5517AEA84736B94CC875B452D29D44831E559DCC,
	DOTweenAnimation_U3CCreateTweenU3Eb__47_0_m7EF21F58940B00415BFF5170235F86E7B6FB7756,
	NULL,
	DOTweenProShortcuts__cctor_mAFE7F19DEF41C81FF7F344AA6AC08D47951E223C,
	DOTweenProShortcuts_DOSpiral_mDE34B9FA70AE32467569D33A39E3A8CD05362451,
	DOTweenProShortcuts_DOSpiral_m5DAEE15C47A2D416BE15D33D661671F5B64CBB73,
	ShortcutExtensionsTMPText_DOColor_m7085A2D722AE069A272C1582BFB12634386F148B,
	ShortcutExtensionsTMPText_DOFaceColor_mF6FD9E2E1E3F7015DD65D445DD89C2D7691130EB,
	ShortcutExtensionsTMPText_DOOutlineColor_mA8E2EF8EDA85E2BD2F5265D2A774C544A2639822,
	ShortcutExtensionsTMPText_DOGlowColor_m105AF572BAFA001943D7EAAEF6056E8C815823DC,
	ShortcutExtensionsTMPText_DOFade_m3EBD70506DDC6917D4AF32CCEFB45455B3A2DC48,
	ShortcutExtensionsTMPText_DOFaceFade_m132A69B631D4AFF9527FF4AAF772C197B67CB136,
	ShortcutExtensionsTMPText_DOScale_m0782C6EEB0A54E8A980429E9B55653BAF866BF38,
	ShortcutExtensionsTMPText_DOCounter_m9EDB0BC46DDD4DF289A10086CE2A98354A449EC2,
	ShortcutExtensionsTMPText_DOFontSize_m16F61857E74463B9797001DBE3E28756F517AC75,
	ShortcutExtensionsTMPText_DOMaxVisibleCharacters_m8E0FFB73842950591003B38E5E6F8747F46DA237,
	ShortcutExtensionsTMPText_DOText_mAE4827EF068E8F2EABF866D88822887FC48FCFE2,
	DOTweenTMPAnimator_get_target_mE0549E1F6D3DB90526E04578F4E6D77769A4E5E8,
	DOTweenTMPAnimator_set_target_mD835615121F2C8749EBD922CAC4FE419ADAECCD2,
	DOTweenTMPAnimator_get_textInfo_mC8D2BDFED3A59DB0F1FC32EE55542B4963076770,
	DOTweenTMPAnimator_set_textInfo_m7FA393DE99C081275B7D89CC19FC7F0E24AF40E7,
	DOTweenTMPAnimator__ctor_m49558141052BE834A2182246CB7401FCEBD1CB26,
	DOTweenTMPAnimator_Dispose_mB69FFB809A4C0F6A1F86BCFE5FE1F948D1A8FC2B,
	DOTweenTMPAnimator_Refresh_m68461DD488AF2C7CD465E32BF4D76ACD91BA0744,
	DOTweenTMPAnimator_Reset_mA6474D53EAF23757CE59D1B725C4B905F90ED959,
	DOTweenTMPAnimator_OnTextChanged_m6148210317D764238ACA460E1A8C0FE9B733084D,
	DOTweenTMPAnimator_ValidateChar_mC1ABA593C1C8769556D52A68B18B7F755194685D,
	DOTweenTMPAnimator_ValidateSpan_m2C3B6561DDC8C059C621AB2751643F59CE6ECC8C,
	DOTweenTMPAnimator_SkewSpanX_m120803289811FFFDCB4F794F29A80E257D185626,
	DOTweenTMPAnimator_SkewSpanY_m3B9B571B4F81CB5476EE47385CAEB1536C4990FB,
	DOTweenTMPAnimator_GetCharColor_m8A31C8F67338656206AB96CCB31290F7DC0DB6BE,
	DOTweenTMPAnimator_GetCharOffset_mC048ACD5693D80D3AC9DDDED4C443EE346CCA491,
	DOTweenTMPAnimator_GetCharRotation_mC07E82AE9DEDE77D4A8EE550C4B89021EC8D3339,
	DOTweenTMPAnimator_GetCharScale_m6E5D73CAE4E665AC16DD7CEC6FE593E1BCD99B17,
	DOTweenTMPAnimator_SetCharColor_mEB5FDA324D642B6ABBF060560F323C5B9E539813,
	DOTweenTMPAnimator_SetCharOffset_m51CF473BA53EE3F7EFEB0B6F4E95DC8571710EEC,
	DOTweenTMPAnimator_SetCharRotation_m9E35976A69003DED36757A88A9587F53DE0C2F6C,
	DOTweenTMPAnimator_SetCharScale_m33D05C9ECF05DA768DADA25D6BC3C3C4A864BD9F,
	DOTweenTMPAnimator_ShiftCharVertices_m1E5EE862EE07F95EFBE6283D1D340A3402F82168,
	DOTweenTMPAnimator_SkewCharX_mA1F6A28AF9ABDB50759E7BC5E702501DA11517DC,
	DOTweenTMPAnimator_SkewCharY_mE58154BD78AE4ACE78AAB443DE6E3FB1CA461F6E,
	DOTweenTMPAnimator_ResetVerticesShift_mDA654753E566BBF9CA37FDFB5BE62FC26F1FA3F9,
	DOTweenTMPAnimator_DOFadeChar_m735FD942D0C4C6CA970650F382903E8F85083269,
	DOTweenTMPAnimator_DOColorChar_m0FC72C84AC2DFAAEACB4C3A622C69425385FDB20,
	DOTweenTMPAnimator_DOOffsetChar_mACD3E564D1D0344E58FA2AF1D041F7612920A9EE,
	DOTweenTMPAnimator_DORotateChar_m6BF42A304C3A161E123E283CE18FBF5A273A2DE6,
	DOTweenTMPAnimator_DOScaleChar_mC6514255D4279D893585CEAD8619DA6F876129F9,
	DOTweenTMPAnimator_DOScaleChar_mBC13D3FB90C65046A18BBB084D81930F27B896FE,
	DOTweenTMPAnimator_DOPunchCharOffset_mA1BAAF441876F719155FEDAC9C7A880C06263F57,
	DOTweenTMPAnimator_DOPunchCharRotation_m8B4F99C782C290FD45B668BF1FA73B806EC17CF7,
	DOTweenTMPAnimator_DOPunchCharScale_mD9FA80620F41399291C83D27C18F9680EFB82581,
	DOTweenTMPAnimator_DOPunchCharScale_mA2C16439ECF6A5DFDBE85B5E6999673DAE3D6FFE,
	DOTweenTMPAnimator_DOShakeCharOffset_mFDA6AF7E9BB3F09F8DD280D3960CCA327B9750A2,
	DOTweenTMPAnimator_DOShakeCharOffset_m09AED4227BA2ABB56276D9F9049678F1427D74A3,
	DOTweenTMPAnimator_DOShakeCharRotation_mB9FABC829E380CBB05FC548F114055DCD9E16391,
	DOTweenTMPAnimator_DOShakeCharScale_mCAA843847970A19C385D638469BC96E3B43083DE,
	DOTweenTMPAnimator_DOShakeCharScale_mC4C670ACE8FABB57882D9E2B4DBF8C26DE13FA2D,
	U3CU3Ec__DisplayClass0_0__ctor_m60990EF6DED999DB0545FD5306D54E919965C5BB,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m2E712D095F38EFF8613E3FC9CDAFFF369D8E8E95,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mA9535CA5F922CD4162D7B7EB0F0A298A923D2985,
	U3CU3Ec__DisplayClass1_0__ctor_m8805F7B5496887519CF23A58F6D75794F5758657,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_m78A8C06D83D92E4AF3C524E330BBFF7869B4E945,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_mA3894D1D54F6E6D4F3F2C8CA1A404E6C1410A6C8,
	U3CU3Ec__DisplayClass2_0__ctor_m71180E2D0EA27D2B70FB04CCB232FF40201D0385,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_m802F28A755D17457C6907F4D914080FD53C72975,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_m2C85AED977CB0722BC27B69E52F945C712A09EF0,
	U3CU3Ec__DisplayClass0_0__ctor_m8D10F29A28AA70367B94826B210BE7AC28661D90,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_m6D639DA5AF1F1070919A2AD6CFFB5C2DFC674CEF,
	U3CU3Ec__DisplayClass1_0__ctor_m32FDCE2F1BFA2CFC3521F6E7004AAC11A34016C4,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m3728CFC4A5D5D57207EC9C0FE05C208362CD61E5,
	U3CU3Ec__DisplayClass2_0__ctor_mDE175763216F603B2DA30F4A45B3FFF5ED153552,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m26D852ED9489654A1F4F2783F1A1379952718554,
	U3CU3Ec__DisplayClass3_0__ctor_m88E356FCBB56C0B4E21EF41B616C3C2E19AA3649,
	U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_m10A9F6B598BC53801C9FBFC517EBA855BB7F088B,
	U3CU3Ec__DisplayClass4_0__ctor_m2687EA4B489F002E41214EF6F7672680730B0E66,
	U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_m1D42EF9DF4A2359CB646B559D74156F5337DA19C,
	U3CU3Ec__DisplayClass5_0__ctor_mE08B5612496DA11D695A973F1B7E50FC1625E8BC,
	U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_m44E2401A5E5BF3BCD6F32CAC9265AB855891ABE6,
	U3CU3Ec__DisplayClass6_0__ctor_m4282C7CF312BCB82425278AE251B9ED79C309766,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_mF9631396276B023DE2F01B2881BF896580058089,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_m13EB6AC489A2346B2C4EF58D4EB7B121D48066A4,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_mFE960B15DC2191AC258134DC69D42820C8BBA4AC,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_m750600C44CEF5FBAD9B81A3AB29DFA963E369A82,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_mE6B0096A24F8A30429FDBA1FE23091E07FFBCD70,
	U3CU3Ec__DisplayClass7_0__ctor_m9403D70752B0400788496A4B3E1CF88B2BD77556,
	U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_m80CA4871DE4B68B81FE817354C066A20D1CE2532,
	U3CU3Ec__DisplayClass8_0__ctor_mD27CF675B05C30206F9710A975DEC8689270102F,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_mFC29F40E26FD9AB1ED15E7252015EB636A901F16,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m9280F41F1BF4816E0E1141488AC566A38AED4877,
	U3CU3Ec__DisplayClass9_0__ctor_m4389304DB98C045D7081797CF2619E6912DA4469,
	U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_m7D3154BA32BBC522A7B022C8BCB38B83AD162D21,
	U3CU3Ec__DisplayClass10_0__ctor_mD439CDE8A04C3D6D891ADFFA53389A28FC45CF15,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m431E707654D8B7DA87663674ABB75CFC9421B5D1,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mACA0A26F9256CC583302147759E3D8D311A485EE,
	U3CU3Ec__DisplayClass0_0__ctor_m987E55F6B96E1B70E08FE936B62C8FD0FBAC093F,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mAAA4470F96A27457911C5E2DD5AAFB634F9063D4,
	U3CU3Ec__DisplayClass1_0__ctor_mBC64465B357F91BE4BAC81CC826B2E47F020140C,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_mDB08C891796EA4B4C3B111166165B2B5335B48FA,
	U3CU3Ec__DisplayClass2_0__ctor_m3D438CF85609374F00FF2D5AE5567C35CEF994F2,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_mA4C7FA3BAE450758E38B0AF38B0D9B123F9CB160,
	U3CU3Ec__DisplayClass3_0__ctor_mA797C925AE17E2198FAE355EC675B2C462B692DE,
	U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_mDC1BDC928B45102C8674F9242FED8406B600B8F6,
	U3CU3Ec__DisplayClass4_0__ctor_m6ED642429831E03D7D9A904BBA51AA670F51DB1A,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_mD3AD7B539BF754001FAE15CC5C5190E7108B13C6,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_mA0D81831AD1D1F68358C86FE0321F99515584F20,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_mE1F172F92451A5CB9E4116D8FFF960D49DDF81F7,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_mE871DBCA3C2C4A20DD6DF40603CE929F6B728C17,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_m63C3139CB577DF5F9A0F687E78FCBC7D31142016,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_m71AAD9F749C8821E7B768B0F5BB9DD956767B3BC,
	U3CU3Ec__DisplayClass5_0__ctor_m2F1EA3E8F6B47999B69AC2A94327CDB012B8FBDA,
	U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__0_m8F9D5E2B6A15FC25A6FE82BA74DBB9B1DE9776DC,
	U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__1_m28D443C97DD9B05515FFA8FEA9B68559DF4C382A,
	U3CU3Ec__DisplayClass6_0__ctor_m6190B985BEFFEBB6530C42AA214D90BE33CA498F,
	U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__0_m9FAC46A73884CEC27CA5BCE8EDCBB7CE40D17947,
	U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__1_m9C65CF8D5DCE3B549526FB3D8B83881DE059FD3B,
	U3CU3Ec__DisplayClass0_0__ctor_mACAACDE36FA35CF551A1754248F010ADF4A7720B,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m4D09BE9FA37B0F054B64878C7589ACAA63D7E9E7,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_mB02EE5E4E43469D0779797C599ECBCA7925CD04F,
	U3CU3Ec__DisplayClass1_0__ctor_m193FC2381BE613D6EC468F7DD16F22C9AEBDDC7D,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_mC0E4CD30D9BAF4B4DB0C18D40FF5D009C259F974,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_mD34D43501AB67E6829C8EBB832CD6332AE19D6C9,
	U3CU3Ec__DisplayClass3_0__ctor_m1DF4ABD4338E56F3CB0ABFC5F313EB409E7FC587,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_m119DDCF7364B723DF560FC5A49515A485FB20F63,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_mF179A7F107A8534794A0EDC9BE37B3866F6652D6,
	Utils_SwitchToRectTransform_m587394DF33387D8FFD49567ACF0D3D72A1E9348B,
	U3CU3Ec__DisplayClass0_0__ctor_m2E2D8A29DEE97D79779A6BC5FE87B8F2B00F9A96,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m76430B008C99FDC1DF8E4DF9E6BB9A6B4DDB6FEE,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mAEB3C38498B2659828B4FDC13F71304B688F45B6,
	U3CU3Ec__DisplayClass1_0__ctor_m9C1271AE340911E2B78F2C5C503BC339A699D1DA,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mF2954B74DFF433419CDEBC960721F61CA6DDC7AB,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m9EAC3EE75B9D8196B65F12EC3DF489506F6B34E8,
	U3CU3Ec__DisplayClass2_0__ctor_m65F9922873298B65A3BC20045F9AA5302DEE4CC8,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m6657306991563E41E037C07818139BE80438ED8C,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_mB29925037B8A73358D9E235E6A856CDD1C711065,
	U3CU3Ec__DisplayClass3_0__ctor_m8DB2ED589F7593B42BB6313BB24B801EDDB8E9A6,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mEE30B8BD593EF80709EF57F5E377D0375A6BE2C7,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m2B1031C4C67640FE649A967E5CE404759FF2BFF9,
	U3CU3Ec__DisplayClass4_0__ctor_m6AEB13C4296D4B4B6BEF37A414A2DD7E6A86DF2B,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_mBA25EAFC5BAD1C0BB16EA5F359814BB13F8066D7,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m664BDF508E93CC4C2ACEDC7EC665F0898132C309,
	U3CU3Ec__DisplayClass5_0__ctor_m544D9C161709CFFFD5716AD0738D287A0EAF495B,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m34FDEA6E1E49D673A071A550E8738ABBBE70811B,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_mE8753F1EA15EC271C568F0AC148C3B9D9EF321CD,
	U3CU3Ec__DisplayClass7_0__ctor_m89D8D1AA7F2DEB1C6AD83B363D4B80EA0088D7FE,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m9BE6F420825B56A735DC4ED217E4F579AC904D03,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m4D99F430ABC6F8571734F8D0195818315B5C9340,
	U3CU3Ec__DisplayClass8_0__ctor_m52371CA3AD03014FD2E3338AC4243ADEBB7E9E34,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_mE2D6CF43309A578A1EB0AE3B5A029F08085A4A9A,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m2D067AED94C7E4DCD78FC176C3D1FC6316748852,
	U3CU3Ec__DisplayClass9_0__ctor_m5A1037E3CD723BE2A684DC5B724583886F16A732,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mD79F05224F02EF24DA4E5F1B9430494FBD13C991,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_mEF0B8DB508828917ECDA8614D7391A61FAC70CDC,
	U3CU3Ec__DisplayClass10_0__ctor_m1C34D9DB0C7D7B1A77E05C188D35417309DB826E,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_mFC3529F25FC1E4FE81AE923D79752E20D32186FD,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m35D35F2F701BD9E7581BC98A67DEBF93E3FC59E0,
	U3CU3Ec__DisplayClass11_0__ctor_m95A09B9E17C97BACA2AE9D66A3957A9702A77A03,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_mBE8F5EE84C08A6127A749BF303073D5D6B7CFD6F,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_mAB91C7B6A40CE5A3CB7C67CB6D3D96A3E2876715,
	U3CU3Ec__DisplayClass12_0__ctor_m367EFAEC33689D5A978026CED103F74564179859,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_m3BA1AF0ED10AA0AB85419F3314F09E39613D9274,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m97887EE3D0C5AA741E96430BCBF2A6904A35776C,
	U3CU3Ec__DisplayClass13_0__ctor_m85CF8E49BC7259F362A6D5276F3283AEA3BE6CD0,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m8448DB43B34DC403A54FD076DA1A939163132F2E,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_mDC840285042D121935390A684D52A56910837185,
	U3CU3Ec__DisplayClass14_0__ctor_m134730746C60289D671F21D934CDBE177E7F1DDB,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_mA77379A317EDC6EC6D6B9CF994F4F5A1C4AF11CA,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m1ABEC4ED71326C7D91C19A11763EDE3D7923B99A,
	U3CU3Ec__DisplayClass15_0__ctor_m04C93F784970AD85021055C9C5C9FD8B38E3B6A9,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_m87B8CF6051C5DF301DFA23CBCC090BFB2BF9644F,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m79BDB7F3559B6338C1193FA6A7C65EC281DE272E,
	U3CU3Ec__DisplayClass16_0__ctor_m2B97AF2BD10B97B92BD05A77E7F5CC304F3094EA,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m509844967852E71C964B003A85857622695072A5,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m4ADDF7752F07269426CA82E1A1763B7437465D4D,
	U3CU3Ec__DisplayClass17_0__ctor_mA66423C52E58DF1625C6973F7F9258F943CD0892,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_m9F0F2C2A9C1B5D0BC15642C125BEFBC20BF9D173,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_m8462038ED9881108299EE6902A0F4E068F70D8DB,
	U3CU3Ec__DisplayClass18_0__ctor_mE8F0C5046749DE447B5228615AF8C2F89414BF38,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_mA2A8E3A1047F3956638F8ED5BAF2F33F0348B292,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_m39BA974886ED0A2ECAD6EE070D48D9DFDBACA9B0,
	U3CU3Ec__DisplayClass19_0__ctor_m7291A4617641DC932D99CD15053D3A99C7DDEA6F,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_m5FF6AE83E8F55A381F4152F7886345950E6C2EB9,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_m2BC52D1611B4C7F3A71CD579E1E8F0957BAAB48E,
	U3CU3Ec__DisplayClass20_0__ctor_m0000CC2D81B8F28315CCBB9401F3E2C191C767D2,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_m679B143365FA775A1515D24991CF4655A8E7F1B5,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m8CF05C7D1617D1623FB013A5546FEFC54096C4D7,
	U3CU3Ec__DisplayClass21_0__ctor_m4FD48BD1067B2EC82D34AC8F45DFDCDAF2A01A65,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_mC77FADB47D8CDC6A3D3B7E72BB48353704EAB95D,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m34ABD6F68B660DEB32F7389E737D8A9A3142DFC2,
	U3CU3Ec__DisplayClass22_0__ctor_m865A219F912286F14BD73D44A44A67352076012F,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_m628D7EE4274F663EF57D9C6B5352135EDD94303A,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_m5B09727CF5FD4531C0BEE60B0AB11A5668AA1863,
	U3CU3Ec__DisplayClass23_0__ctor_m2D23312119F630EF7804933573D7CFB2CB624AEB,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mC879B312D787AA978C651C145219973E08CCBEF7,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m64A3BC621212D7262FEFB19619A70E5827EBCAB2,
	U3CU3Ec__DisplayClass24_0__ctor_m0E5CBD40AEA9118740AF8E4C53EAFEA549673953,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_m9EC674DED204F15F1C8DB419633B1124640FB886,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_m775C3827A550559B9F49CDF8EB3A55AB5AF43560,
	U3CU3Ec__DisplayClass25_0__ctor_mAEF06B667D476334159A793F538C52C20D14450A,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_mBE6DBDBD3EADB003D181A9DFD11983872C7C442A,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m24E5538342C165E3CEA8124B4DDB77A088B9102E,
	U3CU3Ec__DisplayClass26_0__ctor_m9E8EEDDCE11B6B023718D4019B73622B199DE48B,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_mC39D8DDD1ECE2E3B5E9E0498EAB9053F87255466,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_m8FBF0634C0D8B3651BAA35B82C97CB71304DD71F,
	U3CU3Ec__DisplayClass27_0__ctor_mBD2E9C7EDF772EAF8F2B0B0B7CD0A6789880F890,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m0A50C59F05CACD972A9F98BC0BEF2A9E871AAFC2,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_m43994C2202088DD917DA67CBBC8B1DE214C644EF,
	U3CU3Ec__DisplayClass28_0__ctor_mE7A025D8B734E90027C9850483503C9024253CFD,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mF4702E1505986C12553795321B548A1A9750F5D9,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_m597A2FFE319F9505FF37D108D03FFDAAB3790835,
	U3CU3Ec__DisplayClass29_0__ctor_mEDA64593F15A06EFFEF07BDD559238398C180F07,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_m26D1B1FBEC75E21C3BC7460A6F6AEDF3869BD02E,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_m8A65ED4244D2D66C8B195C2B5D7895D5F393D693,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_m3FE34ECBD0DFEAF454F8E3CB6728E47222E320BD,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_m25B6B4053D4DDA398B31C8C3D432CFD64F2D99D1,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_mEB78BA0AEF7347204681525508BAD16E160E627E,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_mA2F4071C8C9EDDD1156F1D27CA95DB4C0251B898,
	U3CU3Ec__DisplayClass30_0__ctor_mBF1B2ACB453EA5DEB2C7D26ABAD2A0B337511195,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_m4C242B50758F1472DDF1095D409D95B17596DC0E,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_m867C1E81139C07A563E97D5C208801EDA9A6997F,
	U3CU3Ec__DisplayClass31_0__ctor_mCAD8CE557B3575FD996CBF8DE85A71B76759397D,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_m9F4BDD5987201BF5DFB4886EEE3D36A7A0605609,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mD6ACE8AE7B5B702644E85B11F11744379245F2A5,
	U3CU3Ec__DisplayClass32_0__ctor_mECC8545D996A1946ACED515283631837338603DF,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_m93993E9A0A0393E4A658F3FA8954BD110EFB56B8,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_mCD6DAD94AC8EFF74B635F5F27786A24E8E2B55AF,
	U3CU3Ec__DisplayClass33_0__ctor_m8BAA719FDFB93A7041EAEED96573A25252598172,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_m571DAD629BB2A7E247A34032822DB78D7C234D3B,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_m588F0906D3ACC8114D82D8C51D4C57506C4E8D2A,
	U3CU3Ec__DisplayClass34_0__ctor_m1346A0B5A9E068DF5A660F45494935C89240A7E4,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_m395886181C1C9F00F94E95441EAF42A7F4DA5961,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m655020AFBDF0213AEECF03620F60ACBF08F02C50,
	U3CU3Ec__DisplayClass35_0__ctor_m0C2A2269004D87C9B3039CFA62C5929AF472750E,
	U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__0_m59AA8701B7D169FA493A52D5E27D224B2B24098F,
	U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__1_m48791F78DFE52AD7123EC1CB171D7CEDA89E5D32,
	U3CU3Ec__DisplayClass36_0__ctor_mF1AC9E7A782807ECF7F5EEAD0BA3DEA7663A0E15,
	U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_m72863DEF930A265133E32414FF2E46C82790D227,
	U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m31054CF1BD5BBD8C52FD41135422F4791C2C1FBB,
	U3CU3Ec__DisplayClass37_0__ctor_mDB3C942B824C44520D3DBA518428E36C04E67881,
	U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__0_mC980985E07BF926EF3EDA4B517AAF971F1BCD9E8,
	U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__1_m62B1A67B95362FC16FFB24D81106C886756FEE19,
	U3CU3Ec__DisplayClass38_0__ctor_m41B23D6C59A281568AE8628FF01DFDF420CEB4DD,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_m386B242E76F3CA79F0E819516D71413C1BA40D15,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_m7DCABD3F5D0F1A6BCD0DFF422B9B5F94E0F05FF6,
	U3CU3Ec__DisplayClass39_0__ctor_m49CA712882542F1C56DBC8CB70E8E670C1AE933B,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_m7EBBEACAFEF74FDCF6F5D266DD0A09A3E8859104,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m68DDF02C1E24B27188E8975BD74D30318F64FAD4,
	U3CU3Ec__DisplayClass40_0__ctor_m37A9BAB59395B45B2C1B6E7F8B2DB5089D66562A,
	U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__0_m317F5EAE3318F8ADCCCFCFB02E8234E891F7C6C4,
	U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__1_mF2152A4D18B012979446027D2F1A9FB11C88806D,
	U3CU3Ec__DisplayClass8_0__ctor_m4F8C823CD49F7430EEB2DAEC4F163845F95F4132,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m8E405CF03EFF4C3C4D9D73DE056BC984CCFBFD15,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m2C44275F67B84D88C6978406F74640CAE5F98A8D,
	U3CU3Ec__DisplayClass9_0__ctor_m88A0120B3B465522B86D19206FD5F510DE6EB225,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m13A85E8BF1EBC69FB32059DD893D9D6A80F18A88,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_mBC9F33C47CB788D0F55943940F95319048812B0A,
	U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m53BB7AAA77CFFB563552DD029D2C14B2B3A235F4,
	U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_mB281D6F4074CA4B8D8A5DF116404A8B3DDCDB281,
	U3CAsyncWaitForRewindU3Ed__11_MoveNext_m3E7CC965C8B33BFE1C281C3D6A2DDF3B781F3F6C,
	U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mCC8B4F2EAFA2D6D42E1EC0FD856DCD46CD0998EC,
	U3CAsyncWaitForKillU3Ed__12_MoveNext_m7F739727954790989E703DD2BC36A7CBE87D0B89,
	U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m7061BB49F48763C47ABF5C459AC152607C362358,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_m6985530DD3AA94DD8BCD36C455EEBE62FD432E6F,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m003ABD1151A01F9FE30FD1C8FC494CEF141C1E19,
	U3CAsyncWaitForPositionU3Ed__14_MoveNext_m1577C29F05ABDB7F6F3C614EE9D50E16959077D7,
	U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_m4644A2CA89AB22E4F1EEF3D10C658340266C2FCC,
	U3CAsyncWaitForStartU3Ed__15_MoveNext_mEA777818FD7DCE82624C0E60A57BBBCB7FFDAA2D,
	U3CAsyncWaitForStartU3Ed__15_SetStateMachine_m746EB287294C0C65BF9EF0CA89D9F7B04DFD4DDE,
	WaitForCompletion_get_keepWaiting_m8B0961B44B29700998F40013D0DFA462ACE6164C,
	WaitForCompletion__ctor_mBCE4900965B00C7C8B07C11599DB9C5EF7C0DB2B,
	WaitForRewind_get_keepWaiting_mB8039B20DAB0A5B3AD08C48FF011CE56CA5AFA36,
	WaitForRewind__ctor_m8A743D8EDA6B53753132BB6B2BBB4BF95863269E,
	WaitForKill_get_keepWaiting_m04AE96DD30619281334E76D4477FACD6E4EB4938,
	WaitForKill__ctor_mA4DF17E431350F30D2B035A3044FBB9518B463F0,
	WaitForElapsedLoops_get_keepWaiting_m5A9CF13BB9065A506FA38DDDDD11C469F8961282,
	WaitForElapsedLoops__ctor_mD1DCB74D74DFF0544F9DD0ECFCB5B843ED820478,
	WaitForPosition_get_keepWaiting_m36E26FBE3FD2E8FA7AD365D6BE60CC4AE8489C39,
	WaitForPosition__ctor_mF059542AE7EDB09EE57B3A67AAF741DDD4778601,
	WaitForStart_get_keepWaiting_mFAC851C1E4D398A41076EAA2F63258AB220D8C52,
	WaitForStart__ctor_mC8A9A805AD9A41782CDB8E4156F661D5B63CF1D2,
	Physics_SetOrientationOnPath_m8C23022A41E6D526E7B6C54C948A6F72B80F0182,
	Physics_HasRigidbody2D_mEF17D135EC4B317363B799DF0DE9A8B8764C7CB4,
	Physics_HasRigidbody_m24799D22419D4640E42A704221C086B6B60B98A6,
	Physics_CreateDOTweenPathTween_mD91665CB83CC7477D41957AAE719C05CD4DB013F,
	U3CU3Ec__DisplayClass1_0__ctor_mCA33651A4C9BAFBDC5B3EA1AE3B067D41B70BDEF,
	U3CU3Ec__DisplayClass1_0_U3CDOSpiralU3Eb__0_m3545A1B63D5FF61C6934F3F7F568C60109764F55,
	U3CU3Ec__DisplayClass1_0_U3CDOSpiralU3Eb__1_m266C4EA9AA325F1999A88D392150A876D6C20702,
	U3CU3Ec__DisplayClass2_0__ctor_m7BC1E9D40A414AD376A9BDFA3BA249C52DF4882B,
	U3CU3Ec__DisplayClass2_0_U3CDOSpiralU3Eb__0_m61D3D7FEF99911050A6B46935FF15541E82C53AE,
	U3CU3Ec__DisplayClass0_0__ctor_mEBC1C720C79D847C19B50A63966893CFEE43F842,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m95B7F6A07959A0DFAB9012ECCDFACCCB92EAF61C,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m41E14FD6B6105AFD18CA839DD78882A845D2DCD2,
	U3CU3Ec__DisplayClass1_0__ctor_mD3A70E9F18297DF3A0306570A2159CBFA63210D6,
	U3CU3Ec__DisplayClass1_0_U3CDOFaceColorU3Eb__0_m9EB41EBAD131FEA2F852EBD662203CB56D34DE9D,
	U3CU3Ec__DisplayClass1_0_U3CDOFaceColorU3Eb__1_mD2F84AFD42FA2CA18F6E5D028ED638A87828A2F5,
	U3CU3Ec__DisplayClass2_0__ctor_m8DB7BC71FFDC9972AAD385463F7F53E8A120A85B,
	U3CU3Ec__DisplayClass2_0_U3CDOOutlineColorU3Eb__0_m95474CD26D00BE2B758C3DA0A0EA048BA943FAC7,
	U3CU3Ec__DisplayClass2_0_U3CDOOutlineColorU3Eb__1_m8E1CB02CC7890BFAD7FE3D24BD8CA38A2D85FD4F,
	U3CU3Ec__DisplayClass4_0__ctor_mE8A5BE5691848CC6E8ADD4424446F3F6DC0A5DF2,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_mE457BB2E7E671A74FD757F70EB566CC6C12D0102,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m3B73407A005BD6A7F9E62F9882E4BEE31D20B984,
	U3CU3Ec__DisplayClass5_0__ctor_mF6496FF71D08236F0F379C13FDA889E47343F61D,
	U3CU3Ec__DisplayClass5_0_U3CDOFaceFadeU3Eb__0_m04D9ECD99A5C18DE82A425E8748716DC76AE59A7,
	U3CU3Ec__DisplayClass5_0_U3CDOFaceFadeU3Eb__1_m1529421C97D98BE190EF75A525E9CED90BAE5947,
	U3CU3Ec__DisplayClass6_0__ctor_m6CD7B56A136A74EA6A016264BB44ECE9480A9CA8,
	U3CU3Ec__DisplayClass6_0_U3CDOScaleU3Eb__0_m5F25C281472D7C9BB7210CBE8FC184B2830BA09E,
	U3CU3Ec__DisplayClass6_0_U3CDOScaleU3Eb__1_mFFA7F71E6188451CBF72D0557614A35C640F34FE,
	U3CU3Ec__DisplayClass7_0__ctor_m051353B10E58213A7E342CEC468E18BA98B4BE06,
	U3CU3Ec__DisplayClass7_0_U3CDOCounterU3Eb__0_mA2223BE4B007FC694DEC95FE9EFA406731FBFD51,
	U3CU3Ec__DisplayClass7_0_U3CDOCounterU3Eb__1_mDBD811A0E7A22FBC88FFF278A927E6C220661E14,
	U3CU3Ec__DisplayClass8_0__ctor_m648E81874697266D01238C255687E0D36FF79AC2,
	U3CU3Ec__DisplayClass8_0_U3CDOFontSizeU3Eb__0_m4C7BA0881A493DA9AFEB8D8C03420AD6BC432BBE,
	U3CU3Ec__DisplayClass8_0_U3CDOFontSizeU3Eb__1_mB264CF20A9C6D5D1FD565B2DF7E857F9EE11FCA8,
	U3CU3Ec__DisplayClass9_0__ctor_m229C4CB85B7B3BA914EBA1C4A57A563C62380497,
	U3CU3Ec__DisplayClass9_0_U3CDOMaxVisibleCharactersU3Eb__0_mEE8BCB74655D900F8D6DB05DD665C8061E377FFC,
	U3CU3Ec__DisplayClass9_0_U3CDOMaxVisibleCharactersU3Eb__1_m37C8F6FEDE9E24235CE80019C90A0BB903F7E4AA,
	U3CU3Ec__DisplayClass10_0__ctor_m1407CF1E8857BC1FBA67C89CBC46960E419F2B90,
	U3CU3Ec__DisplayClass10_0_U3CDOTextU3Eb__0_m854E8E3BAB1276B9EC913BE14EB09E74E9AEEF7E,
	U3CU3Ec__DisplayClass10_0_U3CDOTextU3Eb__1_m2CDBEB6ADF4D7837446632EC320E8AA084231793,
	CharVertices__ctor_m1FCBFBAFD52DDACE798A2EDABF552B5E33578F5F,
	CharTransform_get_isVisible_m97898D0FF5F7CA81E23A0E8F2EF1E8BADDFF4DD2,
	CharTransform_set_isVisible_mF97ED6A45E13DA5C5318D64DF3013281EF85ECEC,
	CharTransform__ctor_m5FA41A9B1244DEF0906D681E5D80D0C5B6F9793F,
	CharTransform_Refresh_m3DB3034DB6BF2CF7D5835F60F518DF8F3BE58986,
	CharTransform_ResetAll_m68EEBE49DF0B5A8B3A75DBB7C1086DA57461BD76,
	CharTransform_ResetTransformationData_mE1856880BEAB9581C30A0478CFD050ADA4808E1D,
	CharTransform_ResetGeometry_m237FE94A9C2338F16DE6532CD1E512C18DB2B9F3,
	CharTransform_ResetColors_m27F59B44398219ED6B8CA45881A230ABE0E87FE1,
	CharTransform_GetColor_mEFF9D07B75A32FDEF7C893EC282E0F41F0D9BF81,
	CharTransform_GetVertices_m38B3288CB017F93D4F5393B63C3349C4370FE71C,
	CharTransform_UpdateAlpha_mA3B92AF01736C296EFBB7C4C28FFB6B220F3C0A3,
	CharTransform_UpdateColor_m87B9B3A77292EF39A4013218773CF9AAE5A3A7A1,
	CharTransform_UpdateGeometry_mE90A834A75963030FC69703AE6137436B616D066,
	CharTransform_ShiftVertices_mD41EF655A839834A7EDB80474B9E1CB396FB5500,
	CharTransform_ResetVerticesShift_m901074CEF0A096D4ED59365924F959045CCC362A,
	U3CU3Ec__DisplayClass32_0__ctor_m7D4FDB2D12D71B4B0675399D5E51FF54B7815628,
	U3CU3Ec__DisplayClass32_0_U3CDOFadeCharU3Eb__0_mE72F0304748B71608B06BDDCC9D24198D31860E0,
	U3CU3Ec__DisplayClass32_0_U3CDOFadeCharU3Eb__1_m606CACDE3B0031175F930BA0440A18DCB5573C2A,
	U3CU3Ec__DisplayClass33_0__ctor_m3CBDED481539780EE909C4D0AC56CB6D0FAEE507,
	U3CU3Ec__DisplayClass33_0_U3CDOColorCharU3Eb__0_mEFC9894705586383A07DBF13C7B5819312745E84,
	U3CU3Ec__DisplayClass33_0_U3CDOColorCharU3Eb__1_mECA00476DDB787DB96D496C0E6BFCCB5A9999F4A,
	U3CU3Ec__DisplayClass34_0__ctor_mAAED55EE2A2FACDCD6688CD39CCC03CF4980C430,
	U3CU3Ec__DisplayClass34_0_U3CDOOffsetCharU3Eb__0_m749362D65F56CC5D9323ECDF5C556872F7E848E6,
	U3CU3Ec__DisplayClass34_0_U3CDOOffsetCharU3Eb__1_m4FB2192A4D86B7C5EBC336BF380E5B6F0FDEACD5,
	U3CU3Ec__DisplayClass35_0__ctor_m9310D1BB6A4BAE2F3B166D40640AA539CC8090BB,
	U3CU3Ec__DisplayClass35_0_U3CDORotateCharU3Eb__0_m3D5203BA104AF85D985C13AD23DFB45ADD5F1103,
	U3CU3Ec__DisplayClass35_0_U3CDORotateCharU3Eb__1_mA45F56F418829A8295C8577BBA3231684AA3B959,
	U3CU3Ec__DisplayClass37_0__ctor_m68054C6FD917CD74993E5AB0E8A5D8BB5CDDF7D4,
	U3CU3Ec__DisplayClass37_0_U3CDOScaleCharU3Eb__0_m27A0754CF952093C19B68DEC73F5D589A7D266EA,
	U3CU3Ec__DisplayClass37_0_U3CDOScaleCharU3Eb__1_m5D352034B738D87BFEF2FB0A66DF1C8D3C1D3E95,
	U3CU3Ec__DisplayClass38_0__ctor_m522E116755B166DBEF447E2FCBFAE4D64EABDA71,
	U3CU3Ec__DisplayClass38_0_U3CDOPunchCharOffsetU3Eb__0_mEB117773BB5C8E6577BFE26D4EA3A6936B3041C1,
	U3CU3Ec__DisplayClass38_0_U3CDOPunchCharOffsetU3Eb__1_m03B79EA169E8B28734921381C5CE3BFEF316CB2A,
	U3CU3Ec__DisplayClass39_0__ctor_m14456F59E7A699089AE521E5424E9E08AB89E382,
	U3CU3Ec__DisplayClass39_0_U3CDOPunchCharRotationU3Eb__0_m3740742E7C9F5B51567E75E752B661C73EEFEEDD,
	U3CU3Ec__DisplayClass39_0_U3CDOPunchCharRotationU3Eb__1_mE51E8C07D4C5542172AD93A9F4AD5FD0C8FF6F31,
	U3CU3Ec__DisplayClass41_0__ctor_mFB320502276555AEDFFA47DA609ADE212334BFCD,
	U3CU3Ec__DisplayClass41_0_U3CDOPunchCharScaleU3Eb__0_m89605520DB89C64B63A7FCC28F90403D03A4201D,
	U3CU3Ec__DisplayClass41_0_U3CDOPunchCharScaleU3Eb__1_m1A24D26AF729657FB12D1870334DBD88E8233E35,
	U3CU3Ec__DisplayClass43_0__ctor_m81BC63A222611EE7F35B1095FC78943FDBBA2027,
	U3CU3Ec__DisplayClass43_0_U3CDOShakeCharOffsetU3Eb__0_m85226B06E888AE781A499D7262F8F8AEF1FE7E89,
	U3CU3Ec__DisplayClass43_0_U3CDOShakeCharOffsetU3Eb__1_mAA2936E2FE58BF767465677745A2B9FE807351F6,
	U3CU3Ec__DisplayClass44_0__ctor_m27BA42C3153CA56D442B19D3961961A7983802C0,
	U3CU3Ec__DisplayClass44_0_U3CDOShakeCharRotationU3Eb__0_mFE27C33BE386A5B99D3DC87F835F5D74AC7CD9AD,
	U3CU3Ec__DisplayClass44_0_U3CDOShakeCharRotationU3Eb__1_m2006BF303D6A052DFBCFD0302708B1A75D356DC8,
	U3CU3Ec__DisplayClass46_0__ctor_mF521FBE68DF94B9231D4CEB6A8DD2A07344E7E10,
	U3CU3Ec__DisplayClass46_0_U3CDOShakeCharScaleU3Eb__0_mEE481A349E56AD3F0CB114A0527078A92151BBF0,
	U3CU3Ec__DisplayClass46_0_U3CDOShakeCharScaleU3Eb__1_m702C078F2C3428BAF87D4FBAB93EDD4A621B0122,
};
extern void U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m53BB7AAA77CFFB563552DD029D2C14B2B3A235F4_AdjustorThunk (void);
extern void U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_mB281D6F4074CA4B8D8A5DF116404A8B3DDCDB281_AdjustorThunk (void);
extern void U3CAsyncWaitForRewindU3Ed__11_MoveNext_m3E7CC965C8B33BFE1C281C3D6A2DDF3B781F3F6C_AdjustorThunk (void);
extern void U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mCC8B4F2EAFA2D6D42E1EC0FD856DCD46CD0998EC_AdjustorThunk (void);
extern void U3CAsyncWaitForKillU3Ed__12_MoveNext_m7F739727954790989E703DD2BC36A7CBE87D0B89_AdjustorThunk (void);
extern void U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m7061BB49F48763C47ABF5C459AC152607C362358_AdjustorThunk (void);
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_m6985530DD3AA94DD8BCD36C455EEBE62FD432E6F_AdjustorThunk (void);
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m003ABD1151A01F9FE30FD1C8FC494CEF141C1E19_AdjustorThunk (void);
extern void U3CAsyncWaitForPositionU3Ed__14_MoveNext_m1577C29F05ABDB7F6F3C614EE9D50E16959077D7_AdjustorThunk (void);
extern void U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_m4644A2CA89AB22E4F1EEF3D10C658340266C2FCC_AdjustorThunk (void);
extern void U3CAsyncWaitForStartU3Ed__15_MoveNext_mEA777818FD7DCE82624C0E60A57BBBCB7FFDAA2D_AdjustorThunk (void);
extern void U3CAsyncWaitForStartU3Ed__15_SetStateMachine_m746EB287294C0C65BF9EF0CA89D9F7B04DFD4DDE_AdjustorThunk (void);
extern void CharVertices__ctor_m1FCBFBAFD52DDACE798A2EDABF552B5E33578F5F_AdjustorThunk (void);
extern void CharTransform_get_isVisible_m97898D0FF5F7CA81E23A0E8F2EF1E8BADDFF4DD2_AdjustorThunk (void);
extern void CharTransform_set_isVisible_mF97ED6A45E13DA5C5318D64DF3013281EF85ECEC_AdjustorThunk (void);
extern void CharTransform__ctor_m5FA41A9B1244DEF0906D681E5D80D0C5B6F9793F_AdjustorThunk (void);
extern void CharTransform_Refresh_m3DB3034DB6BF2CF7D5835F60F518DF8F3BE58986_AdjustorThunk (void);
extern void CharTransform_ResetAll_m68EEBE49DF0B5A8B3A75DBB7C1086DA57461BD76_AdjustorThunk (void);
extern void CharTransform_ResetTransformationData_mE1856880BEAB9581C30A0478CFD050ADA4808E1D_AdjustorThunk (void);
extern void CharTransform_ResetGeometry_m237FE94A9C2338F16DE6532CD1E512C18DB2B9F3_AdjustorThunk (void);
extern void CharTransform_ResetColors_m27F59B44398219ED6B8CA45881A230ABE0E87FE1_AdjustorThunk (void);
extern void CharTransform_GetColor_mEFF9D07B75A32FDEF7C893EC282E0F41F0D9BF81_AdjustorThunk (void);
extern void CharTransform_GetVertices_m38B3288CB017F93D4F5393B63C3349C4370FE71C_AdjustorThunk (void);
extern void CharTransform_UpdateAlpha_mA3B92AF01736C296EFBB7C4C28FFB6B220F3C0A3_AdjustorThunk (void);
extern void CharTransform_UpdateColor_m87B9B3A77292EF39A4013218773CF9AAE5A3A7A1_AdjustorThunk (void);
extern void CharTransform_UpdateGeometry_mE90A834A75963030FC69703AE6137436B616D066_AdjustorThunk (void);
extern void CharTransform_ShiftVertices_mD41EF655A839834A7EDB80474B9E1CB396FB5500_AdjustorThunk (void);
extern void CharTransform_ResetVerticesShift_m901074CEF0A096D4ED59365924F959045CCC362A_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[28] = 
{
	{ 0x06000183, U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m53BB7AAA77CFFB563552DD029D2C14B2B3A235F4_AdjustorThunk },
	{ 0x06000184, U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_mB281D6F4074CA4B8D8A5DF116404A8B3DDCDB281_AdjustorThunk },
	{ 0x06000185, U3CAsyncWaitForRewindU3Ed__11_MoveNext_m3E7CC965C8B33BFE1C281C3D6A2DDF3B781F3F6C_AdjustorThunk },
	{ 0x06000186, U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mCC8B4F2EAFA2D6D42E1EC0FD856DCD46CD0998EC_AdjustorThunk },
	{ 0x06000187, U3CAsyncWaitForKillU3Ed__12_MoveNext_m7F739727954790989E703DD2BC36A7CBE87D0B89_AdjustorThunk },
	{ 0x06000188, U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m7061BB49F48763C47ABF5C459AC152607C362358_AdjustorThunk },
	{ 0x06000189, U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_m6985530DD3AA94DD8BCD36C455EEBE62FD432E6F_AdjustorThunk },
	{ 0x0600018A, U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m003ABD1151A01F9FE30FD1C8FC494CEF141C1E19_AdjustorThunk },
	{ 0x0600018B, U3CAsyncWaitForPositionU3Ed__14_MoveNext_m1577C29F05ABDB7F6F3C614EE9D50E16959077D7_AdjustorThunk },
	{ 0x0600018C, U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_m4644A2CA89AB22E4F1EEF3D10C658340266C2FCC_AdjustorThunk },
	{ 0x0600018D, U3CAsyncWaitForStartU3Ed__15_MoveNext_mEA777818FD7DCE82624C0E60A57BBBCB7FFDAA2D_AdjustorThunk },
	{ 0x0600018E, U3CAsyncWaitForStartU3Ed__15_SetStateMachine_m746EB287294C0C65BF9EF0CA89D9F7B04DFD4DDE_AdjustorThunk },
	{ 0x060001C2, CharVertices__ctor_m1FCBFBAFD52DDACE798A2EDABF552B5E33578F5F_AdjustorThunk },
	{ 0x060001C3, CharTransform_get_isVisible_m97898D0FF5F7CA81E23A0E8F2EF1E8BADDFF4DD2_AdjustorThunk },
	{ 0x060001C4, CharTransform_set_isVisible_mF97ED6A45E13DA5C5318D64DF3013281EF85ECEC_AdjustorThunk },
	{ 0x060001C5, CharTransform__ctor_m5FA41A9B1244DEF0906D681E5D80D0C5B6F9793F_AdjustorThunk },
	{ 0x060001C6, CharTransform_Refresh_m3DB3034DB6BF2CF7D5835F60F518DF8F3BE58986_AdjustorThunk },
	{ 0x060001C7, CharTransform_ResetAll_m68EEBE49DF0B5A8B3A75DBB7C1086DA57461BD76_AdjustorThunk },
	{ 0x060001C8, CharTransform_ResetTransformationData_mE1856880BEAB9581C30A0478CFD050ADA4808E1D_AdjustorThunk },
	{ 0x060001C9, CharTransform_ResetGeometry_m237FE94A9C2338F16DE6532CD1E512C18DB2B9F3_AdjustorThunk },
	{ 0x060001CA, CharTransform_ResetColors_m27F59B44398219ED6B8CA45881A230ABE0E87FE1_AdjustorThunk },
	{ 0x060001CB, CharTransform_GetColor_mEFF9D07B75A32FDEF7C893EC282E0F41F0D9BF81_AdjustorThunk },
	{ 0x060001CC, CharTransform_GetVertices_m38B3288CB017F93D4F5393B63C3349C4370FE71C_AdjustorThunk },
	{ 0x060001CD, CharTransform_UpdateAlpha_mA3B92AF01736C296EFBB7C4C28FFB6B220F3C0A3_AdjustorThunk },
	{ 0x060001CE, CharTransform_UpdateColor_m87B9B3A77292EF39A4013218773CF9AAE5A3A7A1_AdjustorThunk },
	{ 0x060001CF, CharTransform_UpdateGeometry_mE90A834A75963030FC69703AE6137436B616D066_AdjustorThunk },
	{ 0x060001D0, CharTransform_ShiftVertices_mD41EF655A839834A7EDB80474B9E1CB396FB5500_AdjustorThunk },
	{ 0x060001D1, CharTransform_ResetVerticesShift_m901074CEF0A096D4ED59365924F959045CCC362A_AdjustorThunk },
};
static const int32_t s_InvokerIndices[498] = 
{
	2084,
	2084,
	2046,
	2063,
	2063,
	94,
	2065,
	94,
	94,
	94,
	94,
	94,
	94,
	94,
	94,
	2098,
	2099,
	2099,
	2099,
	2100,
	2103,
	2104,
	2108,
	2108,
	2109,
	2109,
	2688,
	2099,
	2099,
	2084,
	2689,
	2108,
	2108,
	2085,
	2084,
	2080,
	2085,
	2084,
	2085,
	2084,
	2085,
	2084,
	2084,
	2080,
	2688,
	2688,
	2688,
	2085,
	2084,
	2094,
	2688,
	2099,
	2099,
	2098,
	2099,
	2099,
	2099,
	2688,
	2688,
	2094,
	2084,
	2084,
	2688,
	2689,
	2106,
	2690,
	2689,
	2688,
	2099,
	2099,
	2099,
	2085,
	2691,
	2084,
	2692,
	2085,
	2085,
	2085,
	2080,
	2050,
	153,
	153,
	153,
	704,
	2121,
	153,
	2694,
	2694,
	0,
	0,
	0,
	119,
	2078,
	0,
	3,
	3,
	154,
	154,
	154,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	26,
	26,
	26,
	14,
	94,
	14,
	14,
	23,
	23,
	23,
	-1,
	3,
	2696,
	2696,
	2085,
	2697,
	2697,
	2698,
	2084,
	2084,
	2084,
	2691,
	2084,
	2699,
	2692,
	14,
	26,
	14,
	26,
	26,
	23,
	23,
	23,
	26,
	860,
	2700,
	2701,
	2702,
	1485,
	2703,
	2703,
	2703,
	2704,
	1458,
	1458,
	1458,
	2705,
	2706,
	2707,
	32,
	2708,
	2709,
	2710,
	2711,
	2708,
	2710,
	2712,
	2712,
	2713,
	2712,
	2714,
	2715,
	2715,
	2714,
	2715,
	23,
	731,
	337,
	23,
	731,
	337,
	23,
	731,
	337,
	23,
	1433,
	23,
	1433,
	23,
	1433,
	23,
	1433,
	23,
	1635,
	23,
	1635,
	23,
	1433,
	23,
	1433,
	1433,
	23,
	23,
	1433,
	23,
	1433,
	1434,
	23,
	1433,
	23,
	1433,
	1434,
	23,
	1441,
	23,
	1441,
	23,
	1441,
	23,
	731,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	23,
	1433,
	1434,
	23,
	1433,
	1434,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	2693,
	23,
	731,
	337,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	23,
	731,
	337,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	1433,
	1434,
	23,
	1433,
	1434,
	23,
	1433,
	1434,
	23,
	1433,
	1434,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	1433,
	1434,
	23,
	1433,
	1434,
	23,
	1433,
	1434,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	23,
	1441,
	1442,
	23,
	731,
	337,
	23,
	731,
	337,
	23,
	731,
	337,
	23,
	1417,
	1418,
	23,
	10,
	32,
	23,
	1417,
	1418,
	23,
	14,
	26,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	23,
	1441,
	1442,
	23,
	1441,
	1442,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	89,
	26,
	89,
	26,
	89,
	26,
	89,
	130,
	89,
	943,
	89,
	26,
	2191,
	114,
	114,
	2695,
	23,
	1433,
	1434,
	23,
	1433,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	23,
	1433,
	1434,
	23,
	10,
	32,
	23,
	731,
	337,
	23,
	10,
	32,
	23,
	14,
	26,
	2716,
	89,
	31,
	370,
	27,
	197,
	23,
	27,
	27,
	2717,
	2718,
	2719,
	2720,
	2721,
	2722,
	26,
	23,
	1417,
	1418,
	23,
	1417,
	1418,
	23,
	1433,
	1434,
	23,
	1635,
	1636,
	23,
	1433,
	1434,
	23,
	1433,
	1434,
	23,
	1433,
	1434,
	23,
	1433,
	1434,
	23,
	1433,
	1434,
	23,
	1433,
	1434,
	23,
	1433,
	1434,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000086, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)2, 35094 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	498,
	s_methodPointers,
	28,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
};
