﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 SRDebugger.Services.IDebugService SRDebug::get_Instance()
extern void SRDebug_get_Instance_mB4BE4C8F06C044E84F01EE2DC9BDD58BF01A856C (void);
// 0x00000002 System.Void SRDebug::Init()
extern void SRDebug_Init_mFF260B51422A91E4B294F263D3CED83EDB96B2B7 (void);
// 0x00000003 System.Void SRDebugger.AutoInitialize::OnLoadBeforeScene()
extern void AutoInitialize_OnLoadBeforeScene_mC51AA04A369A9792F095E9FC8CD93F412B05E621 (void);
// 0x00000004 System.Void SRDebugger.AutoInitialize::OnLoad()
extern void AutoInitialize_OnLoad_m6AECD5EAC00B20AA997B4D8284BD5A2A950D5B15 (void);
// 0x00000005 System.Int32 SRDebugger.IReadOnlyList`1::get_Count()
// 0x00000006 T SRDebugger.IReadOnlyList`1::get_Item(System.Int32)
// 0x00000007 System.Void SRDebugger.CircularBuffer`1::.ctor(System.Int32)
// 0x00000008 System.Void SRDebugger.CircularBuffer`1::.ctor(System.Int32,T[])
// 0x00000009 System.Int32 SRDebugger.CircularBuffer`1::get_Capacity()
// 0x0000000A System.Boolean SRDebugger.CircularBuffer`1::get_IsFull()
// 0x0000000B System.Boolean SRDebugger.CircularBuffer`1::get_IsEmpty()
// 0x0000000C System.Int32 SRDebugger.CircularBuffer`1::get_Count()
// 0x0000000D T SRDebugger.CircularBuffer`1::get_Item(System.Int32)
// 0x0000000E System.Void SRDebugger.CircularBuffer`1::set_Item(System.Int32,T)
// 0x0000000F System.Void SRDebugger.CircularBuffer`1::Clear()
// 0x00000010 System.Collections.Generic.IEnumerator`1<T> SRDebugger.CircularBuffer`1::GetEnumerator()
// 0x00000011 System.Collections.IEnumerator SRDebugger.CircularBuffer`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000012 T SRDebugger.CircularBuffer`1::Front()
// 0x00000013 T SRDebugger.CircularBuffer`1::Back()
// 0x00000014 System.Void SRDebugger.CircularBuffer`1::PushBack(T)
// 0x00000015 System.Void SRDebugger.CircularBuffer`1::PushFront(T)
// 0x00000016 System.Void SRDebugger.CircularBuffer`1::PopBack()
// 0x00000017 System.Void SRDebugger.CircularBuffer`1::PopFront()
// 0x00000018 T[] SRDebugger.CircularBuffer`1::ToArray()
// 0x00000019 System.Void SRDebugger.CircularBuffer`1::ThrowIfEmpty(System.String)
// 0x0000001A System.Void SRDebugger.CircularBuffer`1::Increment(System.Int32&)
// 0x0000001B System.Void SRDebugger.CircularBuffer`1::Decrement(System.Int32&)
// 0x0000001C System.Int32 SRDebugger.CircularBuffer`1::InternalIndex(System.Int32)
// 0x0000001D System.ArraySegment`1<T> SRDebugger.CircularBuffer`1::ArrayOne()
// 0x0000001E System.ArraySegment`1<T> SRDebugger.CircularBuffer`1::ArrayTwo()
// 0x0000001F System.Void SRDebugger.SRDebuggerInit::Awake()
extern void SRDebuggerInit_Awake_mCC6E97C2E710091F3EC5FB99217E9793DD6D81CC (void);
// 0x00000020 System.Void SRDebugger.SRDebuggerInit::Start()
extern void SRDebuggerInit_Start_mEF7AE717F34EA9C4C5EA5F34825A0D1AF43CF7AA (void);
// 0x00000021 System.Void SRDebugger.SRDebuggerInit::.ctor()
extern void SRDebuggerInit__ctor_m9E2C8919EC46D13C57183C1C24AA337A4496365A (void);
// 0x00000022 System.Void SRDebugger.NumberRangeAttribute::.ctor(System.Double,System.Double)
extern void NumberRangeAttribute__ctor_m64C812C1045CFABBBCB4E227BFC303BF02A742AD (void);
// 0x00000023 System.Void SRDebugger.IncrementAttribute::.ctor(System.Double)
extern void IncrementAttribute__ctor_mC14B810ACB99606543BB86BF15CFC42E0B3CFEDF (void);
// 0x00000024 System.Void SRDebugger.SortAttribute::.ctor(System.Int32)
extern void SortAttribute__ctor_m1608F4290865C48EBCF13A307C817628C15633F0 (void);
// 0x00000025 System.Void SRDebugger.VisibilityChangedDelegate::.ctor(System.Object,System.IntPtr)
extern void VisibilityChangedDelegate__ctor_mA69F3D3B38FECE8C82F05C8497796A95ED1A92DE (void);
// 0x00000026 System.Void SRDebugger.VisibilityChangedDelegate::Invoke(System.Boolean)
extern void VisibilityChangedDelegate_Invoke_m4801E3D870E54CBBC16B8CBCF0D7D55EECF34A2E (void);
// 0x00000027 System.IAsyncResult SRDebugger.VisibilityChangedDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void VisibilityChangedDelegate_BeginInvoke_m3BA556BD1A461DA21E6AE6A4376ECC6440C59574 (void);
// 0x00000028 System.Void SRDebugger.VisibilityChangedDelegate::EndInvoke(System.IAsyncResult)
extern void VisibilityChangedDelegate_EndInvoke_m70486619CF861D2077628C808F31DA0DC070CB25 (void);
// 0x00000029 System.Void SRDebugger.ActionCompleteCallback::.ctor(System.Object,System.IntPtr)
extern void ActionCompleteCallback__ctor_m34F550FAB11F3B1B852D5269DE48D5581D47D52B (void);
// 0x0000002A System.Void SRDebugger.ActionCompleteCallback::Invoke(System.Boolean)
extern void ActionCompleteCallback_Invoke_mEF74031661072303EA07193593FCA05822BA50AE (void);
// 0x0000002B System.IAsyncResult SRDebugger.ActionCompleteCallback::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void ActionCompleteCallback_BeginInvoke_mB0F8AA67B05180B67CACEF829C6F70503D5BE63A (void);
// 0x0000002C System.Void SRDebugger.ActionCompleteCallback::EndInvoke(System.IAsyncResult)
extern void ActionCompleteCallback_EndInvoke_m95971C4D321D57494DC3DFB9AF27D194EFE64C76 (void);
// 0x0000002D System.Void SRDebugger.PinnedUiCanvasCreated::.ctor(System.Object,System.IntPtr)
extern void PinnedUiCanvasCreated__ctor_m4626EB6EC9A00724E7BF27C57A2DCBB1E221B22B (void);
// 0x0000002E System.Void SRDebugger.PinnedUiCanvasCreated::Invoke(UnityEngine.RectTransform)
extern void PinnedUiCanvasCreated_Invoke_m66B17536A9F3CD24F89792C5643F3721C2C13E87 (void);
// 0x0000002F System.IAsyncResult SRDebugger.PinnedUiCanvasCreated::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern void PinnedUiCanvasCreated_BeginInvoke_m3D9D12ACD418BACC37499121B0E3FA1290E24679 (void);
// 0x00000030 System.Void SRDebugger.PinnedUiCanvasCreated::EndInvoke(System.IAsyncResult)
extern void PinnedUiCanvasCreated_EndInvoke_m4CFF9A00E0B343A828D6F505B1FA9EE06DB6D4B4 (void);
// 0x00000031 System.String SRDebugger.InfoEntry::get_Title()
extern void InfoEntry_get_Title_m9F3BB0A779E6F1494E23EA48AF739394019FF4C7 (void);
// 0x00000032 System.Void SRDebugger.InfoEntry::set_Title(System.String)
extern void InfoEntry_set_Title_m8D0F6C0F903E76356DF6AC2403E72CC401003E72 (void);
// 0x00000033 System.Object SRDebugger.InfoEntry::get_Value()
extern void InfoEntry_get_Value_m699AF99FDE2A3432F36BECB509343CDCD8E4F329 (void);
// 0x00000034 System.Boolean SRDebugger.InfoEntry::get_IsPrivate()
extern void InfoEntry_get_IsPrivate_m39C089EA07B38E3DC26D1170CBBBECBBEAD460E7 (void);
// 0x00000035 System.Void SRDebugger.InfoEntry::set_IsPrivate(System.Boolean)
extern void InfoEntry_set_IsPrivate_m513C94B346AC14448347D609E3C9543B2A781FE3 (void);
// 0x00000036 SRDebugger.InfoEntry SRDebugger.InfoEntry::Create(System.String,System.Func`1<System.Object>,System.Boolean)
extern void InfoEntry_Create_mEC46B70788CC0B63D07AEB6EDB1CB12C5094864C (void);
// 0x00000037 SRDebugger.InfoEntry SRDebugger.InfoEntry::Create(System.String,System.Object,System.Boolean)
extern void InfoEntry_Create_m213C0A76CA49CB85483DB24AB0DCE864A663815B (void);
// 0x00000038 System.Void SRDebugger.InfoEntry::.ctor()
extern void InfoEntry__ctor_mBF361724358903CC41A428BD805B22897F0C506E (void);
// 0x00000039 SRDebugger.Settings SRDebugger.Settings::get_Instance()
extern void Settings_get_Instance_mBBD8FC5D3FE62C9BE5FE820E80243B1F3353B2F7 (void);
// 0x0000003A SRDebugger.Settings/KeyboardShortcut[] SRDebugger.Settings::GetDefaultKeyboardShortcuts()
extern void Settings_GetDefaultKeyboardShortcuts_m651F8B3F0BBA3C72D0B5DA302AEA7CB69E541578 (void);
// 0x0000003B System.Void SRDebugger.Settings::UpgradeKeyboardShortcuts()
extern void Settings_UpgradeKeyboardShortcuts_m128AF601AC5E2037AEEE794B0550157A27756F32 (void);
// 0x0000003C System.Void SRDebugger.Settings::add_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void Settings_add_PropertyChanged_m89E574F01F8E03540F62F7436C58A5F6D7052332 (void);
// 0x0000003D System.Void SRDebugger.Settings::remove_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern void Settings_remove_PropertyChanged_m4D13E1BED14D18BBE812E11C3BAB9C201DB11581 (void);
// 0x0000003E System.Boolean SRDebugger.Settings::get_IsEnabled()
extern void Settings_get_IsEnabled_m0130C2C81F49E2556607C9A76612D4A3D27BA772 (void);
// 0x0000003F SRDebugger.DefaultTabs SRDebugger.Settings::get_DefaultTab()
extern void Settings_get_DefaultTab_mA2479E147902813751A03DA3C61EB5B4A61BABF6 (void);
// 0x00000040 SRDebugger.Settings/TriggerEnableModes SRDebugger.Settings::get_EnableTrigger()
extern void Settings_get_EnableTrigger_mD00CDA77B1CAD5A0330397A8A2CA4636291A8A20 (void);
// 0x00000041 SRDebugger.Settings/TriggerBehaviours SRDebugger.Settings::get_TriggerBehaviour()
extern void Settings_get_TriggerBehaviour_m4A07CBBF85492454471DE070678FC9182936102A (void);
// 0x00000042 System.Boolean SRDebugger.Settings::get_ErrorNotification()
extern void Settings_get_ErrorNotification_m6C748B6A2E159158FF0FCC509BDB244E57CB5FB0 (void);
// 0x00000043 System.Boolean SRDebugger.Settings::get_EnableKeyboardShortcuts()
extern void Settings_get_EnableKeyboardShortcuts_mBB1D85FF0FA91C7AB00F9ED7D59E91F41F94D072 (void);
// 0x00000044 System.Collections.Generic.IList`1<SRDebugger.Settings/KeyboardShortcut> SRDebugger.Settings::get_KeyboardShortcuts()
extern void Settings_get_KeyboardShortcuts_m63FF935C0BE58DC3D2F40015A73B1F41A5D3A68E (void);
// 0x00000045 System.Boolean SRDebugger.Settings::get_KeyboardEscapeClose()
extern void Settings_get_KeyboardEscapeClose_mEE4C32AE837E19D29B834B450BB9C438CB9CF43F (void);
// 0x00000046 System.Boolean SRDebugger.Settings::get_EnableBackgroundTransparency()
extern void Settings_get_EnableBackgroundTransparency_mB881F84B6F8ACEB5866DF538E691AECEDE382C60 (void);
// 0x00000047 System.Boolean SRDebugger.Settings::get_RequireCode()
extern void Settings_get_RequireCode_m7E20D504B607DEF4FADAED12C38928446C4D1D60 (void);
// 0x00000048 System.Boolean SRDebugger.Settings::get_RequireEntryCodeEveryTime()
extern void Settings_get_RequireEntryCodeEveryTime_m825387D4607DAED5C8396ED5D02A69FCD4A9688C (void);
// 0x00000049 System.Collections.Generic.IList`1<System.Int32> SRDebugger.Settings::get_EntryCode()
extern void Settings_get_EntryCode_m84781F11E68EF1AA5C433D26693E0E2F5A7AA803 (void);
// 0x0000004A System.Void SRDebugger.Settings::set_EntryCode(System.Collections.Generic.IList`1<System.Int32>)
extern void Settings_set_EntryCode_m20147BAFCE90AE9AD4723791BB74537686ABD974 (void);
// 0x0000004B System.Boolean SRDebugger.Settings::get_UseDebugCamera()
extern void Settings_get_UseDebugCamera_m89455CFC085692F72C69791920EC4ECB541DF869 (void);
// 0x0000004C System.Int32 SRDebugger.Settings::get_DebugLayer()
extern void Settings_get_DebugLayer_m6C0A588A6E50713384F02B38863A33FA2F58FEFA (void);
// 0x0000004D System.Single SRDebugger.Settings::get_DebugCameraDepth()
extern void Settings_get_DebugCameraDepth_m1D259780633312F5D19C323AF7580838D072C69E (void);
// 0x0000004E System.Boolean SRDebugger.Settings::get_CollapseDuplicateLogEntries()
extern void Settings_get_CollapseDuplicateLogEntries_m59FE302FC278D78F8AF0224468677CCFC5FC5D9C (void);
// 0x0000004F System.Boolean SRDebugger.Settings::get_RichTextInConsole()
extern void Settings_get_RichTextInConsole_m8052F17006959E918E9364AA07527F579CB1D542 (void);
// 0x00000050 System.String SRDebugger.Settings::get_ApiKey()
extern void Settings_get_ApiKey_m607DE8F5A70211CC99B49955461893D4E23EE52E (void);
// 0x00000051 System.Boolean SRDebugger.Settings::get_EnableBugReporter()
extern void Settings_get_EnableBugReporter_mD06784CD6BF0CCA1E11B6253BD94E63E9ED607AA (void);
// 0x00000052 System.Collections.Generic.IList`1<SRDebugger.DefaultTabs> SRDebugger.Settings::get_DisabledTabs()
extern void Settings_get_DisabledTabs_mC86ACED4E3B46B276214B6495524A89381291477 (void);
// 0x00000053 SRDebugger.PinAlignment SRDebugger.Settings::get_TriggerPosition()
extern void Settings_get_TriggerPosition_mB753C19E94ADCC17DB967E4A0118CAA3BC9035B3 (void);
// 0x00000054 SRDebugger.PinAlignment SRDebugger.Settings::get_ProfilerAlignment()
extern void Settings_get_ProfilerAlignment_m5C8C7A75EC56ADB1BB646033813E5BE702CA7AE0 (void);
// 0x00000055 SRDebugger.PinAlignment SRDebugger.Settings::get_OptionsAlignment()
extern void Settings_get_OptionsAlignment_mBE7B27F5E8E21F46590F43A7EA59A3F3BD5D7533 (void);
// 0x00000056 SRDebugger.ConsoleAlignment SRDebugger.Settings::get_ConsoleAlignment()
extern void Settings_get_ConsoleAlignment_m09582118F8F9ABF69D2131E71EC2E0478F3AD56A (void);
// 0x00000057 System.Void SRDebugger.Settings::set_ConsoleAlignment(SRDebugger.ConsoleAlignment)
extern void Settings_set_ConsoleAlignment_m855A1392810466841E53727A128AA18B47028415 (void);
// 0x00000058 System.Int32 SRDebugger.Settings::get_MaximumConsoleEntries()
extern void Settings_get_MaximumConsoleEntries_m51294BAC16C304E1414986B66B690093587DF283 (void);
// 0x00000059 System.Void SRDebugger.Settings::set_MaximumConsoleEntries(System.Int32)
extern void Settings_set_MaximumConsoleEntries_m765244602EECBEB6850E7839CF187B1F1ED9A4AB (void);
// 0x0000005A System.Boolean SRDebugger.Settings::get_EnableEventSystemGeneration()
extern void Settings_get_EnableEventSystemGeneration_m69C6E1AB9F03C7CE7B0F9A8160F0E42CB122C242 (void);
// 0x0000005B System.Void SRDebugger.Settings::set_EnableEventSystemGeneration(System.Boolean)
extern void Settings_set_EnableEventSystemGeneration_m9B65A9C4A5DB9D60FBF9EE17151E3AB43D45A271 (void);
// 0x0000005C System.Boolean SRDebugger.Settings::get_AutomaticallyShowCursor()
extern void Settings_get_AutomaticallyShowCursor_m2762D1936BF88C94BB9677F7697213B7228E773D (void);
// 0x0000005D System.Single SRDebugger.Settings::get_UIScale()
extern void Settings_get_UIScale_m6F312D6CA8327B042DB4271750C1FEC6A06A9642 (void);
// 0x0000005E System.Void SRDebugger.Settings::set_UIScale(System.Single)
extern void Settings_set_UIScale_mDE4282F0D0FEB42FD1583FDE443184B1C6EC8729 (void);
// 0x0000005F System.Boolean SRDebugger.Settings::get_UnloadOnClose()
extern void Settings_get_UnloadOnClose_m6FD10BC2BF1C97CC0483D1DD3CE726144691780F (void);
// 0x00000060 System.Void SRDebugger.Settings::OnPropertyChanged(System.String)
extern void Settings_OnPropertyChanged_m37A086F12EF0D080FBD3187BDE1171DC1A2BF4BF (void);
// 0x00000061 SRDebugger.Settings SRDebugger.Settings::GetOrCreateInstance()
extern void Settings_GetOrCreateInstance_m07FFD416079B89806912A2CE82311454D7725969 (void);
// 0x00000062 System.Void SRDebugger.Settings::.ctor()
extern void Settings__ctor_m9C5FFE05A19B1829629B5E221DE4940B19F86F39 (void);
// 0x00000063 System.Nullable`1<SRDebugger.DefaultTabs> SRDebugger.Scripts.DebuggerTabController::get_ActiveTab()
extern void DebuggerTabController_get_ActiveTab_mD508ED33E21785C80555305C59464C11BC7A1DD8 (void);
// 0x00000064 System.Void SRDebugger.Scripts.DebuggerTabController::Start()
extern void DebuggerTabController_Start_mCAD4447C9DB239575E8910345B4CCDA90083D730 (void);
// 0x00000065 System.Boolean SRDebugger.Scripts.DebuggerTabController::OpenTab(SRDebugger.DefaultTabs)
extern void DebuggerTabController_OpenTab_m535CF3A62C1BE32A7EA12BC5DD0663A17147CC40 (void);
// 0x00000066 System.Void SRDebugger.Scripts.DebuggerTabController::ShowAboutTab()
extern void DebuggerTabController_ShowAboutTab_mD240BE96B960A4FAEA56194C51FD457E5C3F4627 (void);
// 0x00000067 System.Void SRDebugger.Scripts.DebuggerTabController::.ctor()
extern void DebuggerTabController__ctor_mEC7EF921CC357E6A8B003DF407AEDD5FC7316B03 (void);
// 0x00000068 System.Void SRDebugger.UI.ProfilerFPSLabel::Update()
extern void ProfilerFPSLabel_Update_m97CC44D272BDF99176E18FB7A69AD347DE2AD8E1 (void);
// 0x00000069 System.Void SRDebugger.UI.ProfilerFPSLabel::Refresh()
extern void ProfilerFPSLabel_Refresh_m721FE1F6543BCA93C2FC35218B215096E3244241 (void);
// 0x0000006A System.Void SRDebugger.UI.ProfilerFPSLabel::.ctor()
extern void ProfilerFPSLabel__ctor_mB9B7C8BE16BECF3BEEAE904AA920D98225592775 (void);
// 0x0000006B System.Void SRDebugger.UI.DebugPanelRoot::Close()
extern void DebugPanelRoot_Close_mD951522C698798FF93B4F515EA5C6DC29811635D (void);
// 0x0000006C System.Void SRDebugger.UI.DebugPanelRoot::CloseAndDestroy()
extern void DebugPanelRoot_CloseAndDestroy_m8C2A28986566A8D685A7F95F55879488A2E979E9 (void);
// 0x0000006D System.Void SRDebugger.UI.DebugPanelRoot::.ctor()
extern void DebugPanelRoot__ctor_mF9C509A3FE7CD0D2EC553C386488EA8CD89C9B29 (void);
// 0x0000006E System.Single SRDebugger.UI.MobileMenuController::get_PeekAmount()
extern void MobileMenuController_get_PeekAmount_mE9018743F58F21709B68DDE2DB384375B403C95A (void);
// 0x0000006F System.Single SRDebugger.UI.MobileMenuController::get_MaxMenuWidth()
extern void MobileMenuController_get_MaxMenuWidth_m6E607731DB85073584412ED7AF6EEB119C4FA833 (void);
// 0x00000070 System.Void SRDebugger.UI.MobileMenuController::OnEnable()
extern void MobileMenuController_OnEnable_m24E154018F43C43507E78853DEEF2EA843312BBC (void);
// 0x00000071 System.Void SRDebugger.UI.MobileMenuController::OnDisable()
extern void MobileMenuController_OnDisable_mB1AB08B5A8CF4DCCA19D275FB34F353A551A7697 (void);
// 0x00000072 System.Void SRDebugger.UI.MobileMenuController::CreateCloseButton()
extern void MobileMenuController_CreateCloseButton_mE3BCAA47767B4F4A7F5CEDF3A1309666CC7B667E (void);
// 0x00000073 System.Void SRDebugger.UI.MobileMenuController::SetRectSize(UnityEngine.RectTransform)
extern void MobileMenuController_SetRectSize_mD3E9970664F135EABBF60CD53B55954219B5532A (void);
// 0x00000074 System.Void SRDebugger.UI.MobileMenuController::CloseButtonClicked()
extern void MobileMenuController_CloseButtonClicked_mE721054F8789C7FD6143E975DB23043AEA116C1E (void);
// 0x00000075 System.Void SRDebugger.UI.MobileMenuController::Update()
extern void MobileMenuController_Update_m37E0B39E5C86FA8903358422D4BD4763DAA48539 (void);
// 0x00000076 System.Void SRDebugger.UI.MobileMenuController::TabControllerOnActiveTabChanged(SRDebugger.UI.Other.SRTabController,SRDebugger.UI.Other.SRTab)
extern void MobileMenuController_TabControllerOnActiveTabChanged_m3D4324224C61562C274B640D41E315C2FC29737C (void);
// 0x00000077 System.Void SRDebugger.UI.MobileMenuController::Open()
extern void MobileMenuController_Open_m705CBF15445F6541FED9542E7768DF03A9861EF3 (void);
// 0x00000078 System.Void SRDebugger.UI.MobileMenuController::Close()
extern void MobileMenuController_Close_mB998421D5BD045DB9D9D3C98CB24A4B37CAFC4A5 (void);
// 0x00000079 System.Void SRDebugger.UI.MobileMenuController::.ctor()
extern void MobileMenuController__ctor_m56C6EBCE93AC16F89EB25794B0E5F0348556C372 (void);
// 0x0000007A System.Boolean SRDebugger.UI.Tabs.BugReportTabController::get_IsEnabled()
extern void BugReportTabController_get_IsEnabled_m39E3AE771CF5CC5CC650CDEA2647DB04C75C6DAB (void);
// 0x0000007B System.Void SRDebugger.UI.Tabs.BugReportTabController::Start()
extern void BugReportTabController_Start_m417150D24DA5C2A399DB7C98E4EFCF477379C9A9 (void);
// 0x0000007C System.Void SRDebugger.UI.Tabs.BugReportTabController::TakingScreenshot()
extern void BugReportTabController_TakingScreenshot_m7C8356FFB6EFC152DC5BF716B8728A71D435FAD5 (void);
// 0x0000007D System.Void SRDebugger.UI.Tabs.BugReportTabController::ScreenshotComplete()
extern void BugReportTabController_ScreenshotComplete_mE855FE88DB5475E7BE30B2D0246D20D626A6F42B (void);
// 0x0000007E System.Void SRDebugger.UI.Tabs.BugReportTabController::.ctor()
extern void BugReportTabController__ctor_m806A126FBFFC141D06B454844C3689491D8A53ED (void);
// 0x0000007F System.Void SRDebugger.UI.Tabs.ConsoleTabController::Start()
extern void ConsoleTabController_Start_m9E200D4BCBE8E79702D3CBA5AC4744CC807AEFDA (void);
// 0x00000080 System.Void SRDebugger.UI.Tabs.ConsoleTabController::FilterToggleValueChanged(System.Boolean)
extern void ConsoleTabController_FilterToggleValueChanged_m5602E87CF935C774DD7927371508D98A3036C70A (void);
// 0x00000081 System.Void SRDebugger.UI.Tabs.ConsoleTabController::FilterValueChanged(System.String)
extern void ConsoleTabController_FilterValueChanged_mD7508E463E1C45A96621651F13B229A8E661537F (void);
// 0x00000082 System.Void SRDebugger.UI.Tabs.ConsoleTabController::PanelOnVisibilityChanged(SRDebugger.Services.IDebugPanelService,System.Boolean)
extern void ConsoleTabController_PanelOnVisibilityChanged_mDCB4A62E33A242FC88D916F1EA4A59BCF25CA4A2 (void);
// 0x00000083 System.Void SRDebugger.UI.Tabs.ConsoleTabController::PinToggleValueChanged(System.Boolean)
extern void ConsoleTabController_PinToggleValueChanged_mF8FE7308470D22980D2552FE94B849A26007D2B5 (void);
// 0x00000084 System.Void SRDebugger.UI.Tabs.ConsoleTabController::OnDestroy()
extern void ConsoleTabController_OnDestroy_m74713328ADB03FC5F29FEEA63E992FAA2E588E1C (void);
// 0x00000085 System.Void SRDebugger.UI.Tabs.ConsoleTabController::OnEnable()
extern void ConsoleTabController_OnEnable_mC563387E05F855E58017B39C6A2FE72386A43012 (void);
// 0x00000086 System.Void SRDebugger.UI.Tabs.ConsoleTabController::ConsoleLogSelectedItemChanged(System.Object)
extern void ConsoleTabController_ConsoleLogSelectedItemChanged_mCC9C9E465A44E06443E7989E4B7E2C6E7E3F485C (void);
// 0x00000087 System.Void SRDebugger.UI.Tabs.ConsoleTabController::Update()
extern void ConsoleTabController_Update_mF03AF53056E2628730FCD924077EFCDDC92A8231 (void);
// 0x00000088 System.Void SRDebugger.UI.Tabs.ConsoleTabController::PopulateStackTraceArea(SRDebugger.Services.ConsoleEntry)
extern void ConsoleTabController_PopulateStackTraceArea_m59FAE85DE2EEFC428BAC16EFF69CB2A601F20507 (void);
// 0x00000089 System.Void SRDebugger.UI.Tabs.ConsoleTabController::Refresh()
extern void ConsoleTabController_Refresh_m2BB6284F9B09ACB9DA2AA3A28E39B1BCA903EAC2 (void);
// 0x0000008A System.Void SRDebugger.UI.Tabs.ConsoleTabController::ConsoleOnUpdated(SRDebugger.Services.IConsoleService)
extern void ConsoleTabController_ConsoleOnUpdated_m89B50D01881D3F8525B2BBE0C8E7E55562969D3D (void);
// 0x0000008B System.Void SRDebugger.UI.Tabs.ConsoleTabController::Clear()
extern void ConsoleTabController_Clear_mDD1AE919F930A60BC03AE59C92D8EB59303A53A8 (void);
// 0x0000008C System.Void SRDebugger.UI.Tabs.ConsoleTabController::.ctor()
extern void ConsoleTabController__ctor_mC8EDA0D22110A1BA65BA144084EDC74A7B82625A (void);
// 0x0000008D System.Void SRDebugger.UI.Tabs.ConsoleTabController::<Start>b__16_0(System.Boolean)
extern void ConsoleTabController_U3CStartU3Eb__16_0_mD00A9DFA9DA20BFBE147F3A02639C90B8C84E237 (void);
// 0x0000008E System.Void SRDebugger.UI.Tabs.ConsoleTabController::<Start>b__16_1(System.Boolean)
extern void ConsoleTabController_U3CStartU3Eb__16_1_mD97DD51575E0A7848342E409E297DBA5670B23CC (void);
// 0x0000008F System.Void SRDebugger.UI.Tabs.ConsoleTabController::<Start>b__16_2(System.Boolean)
extern void ConsoleTabController_U3CStartU3Eb__16_2_mF47E6D19BF3D7B758C43D0C67B7331F47D1C75E5 (void);
// 0x00000090 System.Void SRDebugger.UI.Tabs.InfoTabController::OnEnable()
extern void InfoTabController_OnEnable_m5EFF292E9061F3DE09DB0CE623B82AB0F474C587 (void);
// 0x00000091 System.Void SRDebugger.UI.Tabs.InfoTabController::Refresh()
extern void InfoTabController_Refresh_m9D574E17D6E91C541BE1407DDA2E6CA9CAFB380D (void);
// 0x00000092 System.Void SRDebugger.UI.Tabs.InfoTabController::FillInfoBlock(SRDebugger.UI.Controls.InfoBlock,System.Collections.Generic.IList`1<SRDebugger.InfoEntry>)
extern void InfoTabController_FillInfoBlock_mE6ABDBB78DC3AF29ADA9BAD1EB8EFFDDEC402DA6 (void);
// 0x00000093 SRDebugger.UI.Controls.InfoBlock SRDebugger.UI.Tabs.InfoTabController::CreateBlock(System.String)
extern void InfoTabController_CreateBlock_m4E39AEF142C5B2AA58B9691D6A0DD8CFE8809AC8 (void);
// 0x00000094 System.Void SRDebugger.UI.Tabs.InfoTabController::.ctor()
extern void InfoTabController__ctor_m5DBB0DB8B91E5A2A118AEFFBEDA3270646969F76 (void);
// 0x00000095 System.Void SRDebugger.UI.Tabs.OptionsTabController::Start()
extern void OptionsTabController_Start_m10AF6F99B707CC774BAC8E69CD62A86837DD2604 (void);
// 0x00000096 System.Void SRDebugger.UI.Tabs.OptionsTabController::OnOptionPinnedStateChanged(SRDebugger.Internal.OptionDefinition,System.Boolean)
extern void OptionsTabController_OnOptionPinnedStateChanged_m112CD84C80131EE9198F25EAF8C13AF97EBD3E46 (void);
// 0x00000097 System.Void SRDebugger.UI.Tabs.OptionsTabController::OnOptionsUpdated(System.Object,System.EventArgs)
extern void OptionsTabController_OnOptionsUpdated_m4FDF545C7FEF362A66655A4FBFF1BDAF4E4A2533 (void);
// 0x00000098 System.Void SRDebugger.UI.Tabs.OptionsTabController::OnOptionsValueChanged(System.Object,System.ComponentModel.PropertyChangedEventArgs)
extern void OptionsTabController_OnOptionsValueChanged_m056EB4604F4A268177C0D359D049F6151059624D (void);
// 0x00000099 System.Void SRDebugger.UI.Tabs.OptionsTabController::OnEnable()
extern void OptionsTabController_OnEnable_m7238254DF93881C56F57BA278C2CCE04C6ED933F (void);
// 0x0000009A System.Void SRDebugger.UI.Tabs.OptionsTabController::OnDisable()
extern void OptionsTabController_OnDisable_m8F77784D2F8710CB91AAB8272D0693FFFCCF1FBF (void);
// 0x0000009B System.Void SRDebugger.UI.Tabs.OptionsTabController::Update()
extern void OptionsTabController_Update_mFC4E136192919986388BAA152AF5B7FF5F00BC5F (void);
// 0x0000009C System.Void SRDebugger.UI.Tabs.OptionsTabController::PanelOnVisibilityChanged(SRDebugger.Services.IDebugPanelService,System.Boolean)
extern void OptionsTabController_PanelOnVisibilityChanged_m83694216530DB824B04D7CB2D3428F1DCE8ABF76 (void);
// 0x0000009D System.Void SRDebugger.UI.Tabs.OptionsTabController::SetSelectionModeEnabled(System.Boolean)
extern void OptionsTabController_SetSelectionModeEnabled_m01E6FB428729944905EDFC5889101824151575FC (void);
// 0x0000009E System.Void SRDebugger.UI.Tabs.OptionsTabController::Refresh()
extern void OptionsTabController_Refresh_mB7751B3716267D93B61C6599C2A2A1FF23710C75 (void);
// 0x0000009F System.Void SRDebugger.UI.Tabs.OptionsTabController::CommitPinnedOptions()
extern void OptionsTabController_CommitPinnedOptions_m9519909BC8BD9EE49F1BEC19D353E4D5710B6AC1 (void);
// 0x000000A0 System.Void SRDebugger.UI.Tabs.OptionsTabController::RefreshCategorySelection()
extern void OptionsTabController_RefreshCategorySelection_m6DE8F21676F4651DF38DE416D00278F4C3EDBC88 (void);
// 0x000000A1 System.Void SRDebugger.UI.Tabs.OptionsTabController::OnOptionSelectionToggle(System.Boolean)
extern void OptionsTabController_OnOptionSelectionToggle_mDF470D5EBCACD99987F56D2455A2FD550CD30B19 (void);
// 0x000000A2 System.Void SRDebugger.UI.Tabs.OptionsTabController::OnCategorySelectionToggle(SRDebugger.UI.Tabs.OptionsTabController/CategoryInstance,System.Boolean)
extern void OptionsTabController_OnCategorySelectionToggle_mAD8DFB11A479E006AAB7CC151691AC3A65A86890 (void);
// 0x000000A3 System.Void SRDebugger.UI.Tabs.OptionsTabController::Populate()
extern void OptionsTabController_Populate_mED9BCCFA78967D44A367D38C32EC560C699D8CC8 (void);
// 0x000000A4 System.Void SRDebugger.UI.Tabs.OptionsTabController::CreateCategory(System.String,System.Collections.Generic.List`1<SRDebugger.Internal.OptionDefinition>)
extern void OptionsTabController_CreateCategory_mD4834701571026B828A460AFC8D1D9520598709D (void);
// 0x000000A5 System.Void SRDebugger.UI.Tabs.OptionsTabController::Clear()
extern void OptionsTabController_Clear_m667D25646A60A7C2D1454B940705B92EF302EC71 (void);
// 0x000000A6 System.Void SRDebugger.UI.Tabs.OptionsTabController::.ctor()
extern void OptionsTabController__ctor_m26A39CB99E82C1B53379AF5879BC5EF2A1E18EBB (void);
// 0x000000A7 System.Void SRDebugger.UI.Tabs.ProfilerTabController::Start()
extern void ProfilerTabController_Start_m14309F4ED0EC0492A77FF4AD9594D15549702EDB (void);
// 0x000000A8 System.Void SRDebugger.UI.Tabs.ProfilerTabController::PinToggleValueChanged(System.Boolean)
extern void ProfilerTabController_PinToggleValueChanged_m1F783BD1F40E639B136D957D585C4A97CBC8FEB2 (void);
// 0x000000A9 System.Void SRDebugger.UI.Tabs.ProfilerTabController::OnEnable()
extern void ProfilerTabController_OnEnable_mC535AD61FA74DF071724248A342BF2D76E2A4F81 (void);
// 0x000000AA System.Void SRDebugger.UI.Tabs.ProfilerTabController::Update()
extern void ProfilerTabController_Update_mE560C03596ADE8F3F2B4D11ED7C6D42E4B2640E0 (void);
// 0x000000AB System.Void SRDebugger.UI.Tabs.ProfilerTabController::Refresh()
extern void ProfilerTabController_Refresh_mBE0E8202274CCCA30625B24ECFE122B62BAF4F0C (void);
// 0x000000AC System.Void SRDebugger.UI.Tabs.ProfilerTabController::.ctor()
extern void ProfilerTabController__ctor_mB68179DF4EF754CE4846EF6D59B901F63A8BF6AF (void);
// 0x000000AD System.Void SRDebugger.UI.Other.BugReportPopoverRoot::.ctor()
extern void BugReportPopoverRoot__ctor_m4A574A7752B0AAFD6996EE07AEE0010FA9E5B3EC (void);
// 0x000000AE System.Boolean SRDebugger.UI.Other.BugReportSheetController::get_IsCancelButtonEnabled()
extern void BugReportSheetController_get_IsCancelButtonEnabled_mAC0A7E32F1DA100B8285667472E0F273B80D6148 (void);
// 0x000000AF System.Void SRDebugger.UI.Other.BugReportSheetController::set_IsCancelButtonEnabled(System.Boolean)
extern void BugReportSheetController_set_IsCancelButtonEnabled_mAE83FAFB2D01ED12CE82A9918EBDEF6B0B027A5A (void);
// 0x000000B0 System.Void SRDebugger.UI.Other.BugReportSheetController::Start()
extern void BugReportSheetController_Start_mC32DCFE2616F8A4A2FF9DB70F969EE9702C1B9A3 (void);
// 0x000000B1 System.Void SRDebugger.UI.Other.BugReportSheetController::Submit()
extern void BugReportSheetController_Submit_m9488242C632F0F9E675169A82C46951BC21F2D99 (void);
// 0x000000B2 System.Void SRDebugger.UI.Other.BugReportSheetController::Cancel()
extern void BugReportSheetController_Cancel_mEA98185D47E56F56B32279ECA2C7C07BDF7D4F4F (void);
// 0x000000B3 System.Collections.IEnumerator SRDebugger.UI.Other.BugReportSheetController::SubmitCo()
extern void BugReportSheetController_SubmitCo_mF1A24CE34A40BEDD7660EF56F80EA531B441D76A (void);
// 0x000000B4 System.Void SRDebugger.UI.Other.BugReportSheetController::OnBugReportProgress(System.Single)
extern void BugReportSheetController_OnBugReportProgress_m46D1176163ADA36284E7B67D69634F8F91264DEF (void);
// 0x000000B5 System.Void SRDebugger.UI.Other.BugReportSheetController::OnBugReportComplete(System.Boolean,System.String)
extern void BugReportSheetController_OnBugReportComplete_m10B486FF493D947842414907FDDCB4494ED45808 (void);
// 0x000000B6 System.Void SRDebugger.UI.Other.BugReportSheetController::SetLoadingSpinnerVisible(System.Boolean)
extern void BugReportSheetController_SetLoadingSpinnerVisible_m79E7B6B434B26E009E589B533532DDCA5639D976 (void);
// 0x000000B7 System.Void SRDebugger.UI.Other.BugReportSheetController::ClearForm()
extern void BugReportSheetController_ClearForm_mA38771090B5DDF8108E69543D59CDC523A0AFB9A (void);
// 0x000000B8 System.Void SRDebugger.UI.Other.BugReportSheetController::ShowErrorMessage(System.String,System.String)
extern void BugReportSheetController_ShowErrorMessage_m3EC687544062DF6DB8EBE125D22AB6D9104C492F (void);
// 0x000000B9 System.Void SRDebugger.UI.Other.BugReportSheetController::ClearErrorMessage()
extern void BugReportSheetController_ClearErrorMessage_mF9DD33393D18AC5F347D45AB369F2EAEB302E8C1 (void);
// 0x000000BA System.Void SRDebugger.UI.Other.BugReportSheetController::SetFormEnabled(System.Boolean)
extern void BugReportSheetController_SetFormEnabled_m9608B74185A7B9C51AE4BDD8C587BDB4EDCE31EA (void);
// 0x000000BB System.String SRDebugger.UI.Other.BugReportSheetController::GetDefaultEmailFieldContents()
extern void BugReportSheetController_GetDefaultEmailFieldContents_m386544D6F7F04D2131135C353F0CDB9F28EA1984 (void);
// 0x000000BC System.Void SRDebugger.UI.Other.BugReportSheetController::SetDefaultEmailFieldContents(System.String)
extern void BugReportSheetController_SetDefaultEmailFieldContents_m18B6CBF3F2496AF2E766D5AAE2078E45D5B5086B (void);
// 0x000000BD System.Void SRDebugger.UI.Other.BugReportSheetController::.ctor()
extern void BugReportSheetController__ctor_m38F92447F16B61FE3AD52E5C48D19B450D734ABE (void);
// 0x000000BE System.Boolean SRDebugger.UI.Other.CategoryGroup::get_IsSelected()
extern void CategoryGroup_get_IsSelected_mE3FFF25BFE08F3FAC632835B23E0617A24ACC9DF (void);
// 0x000000BF System.Void SRDebugger.UI.Other.CategoryGroup::set_IsSelected(System.Boolean)
extern void CategoryGroup_set_IsSelected_mDA05DAC71962FDABA5FF1E97FF3C39AD0C4A8DFF (void);
// 0x000000C0 System.Boolean SRDebugger.UI.Other.CategoryGroup::get_SelectionModeEnabled()
extern void CategoryGroup_get_SelectionModeEnabled_mD7DB21F3F44F44B3E30BE8C09FFBA74EBE44E825 (void);
// 0x000000C1 System.Void SRDebugger.UI.Other.CategoryGroup::set_SelectionModeEnabled(System.Boolean)
extern void CategoryGroup_set_SelectionModeEnabled_m832684B4546321EBECD942F894F96D4F090B224A (void);
// 0x000000C2 System.Void SRDebugger.UI.Other.CategoryGroup::.ctor()
extern void CategoryGroup__ctor_m95C9D23F6E099968F14AD74C13DC5E66BB0A2C96 (void);
// 0x000000C3 System.Void SRDebugger.UI.Other.ConfigureCanvasFromSettings::Start()
extern void ConfigureCanvasFromSettings_Start_m028BA54F0F8BA7623F942E967D4B916F0A870CD5 (void);
// 0x000000C4 System.Void SRDebugger.UI.Other.ConfigureCanvasFromSettings::OnDestroy()
extern void ConfigureCanvasFromSettings_OnDestroy_mB0E41DC9ED7B28036867943D8EF6ABFCC8243068 (void);
// 0x000000C5 System.Void SRDebugger.UI.Other.ConfigureCanvasFromSettings::SettingsOnPropertyChanged(System.Object,System.ComponentModel.PropertyChangedEventArgs)
extern void ConfigureCanvasFromSettings_SettingsOnPropertyChanged_mF54B34FE38AA458E6CC652F0593CB570D0DE265F (void);
// 0x000000C6 System.Void SRDebugger.UI.Other.ConfigureCanvasFromSettings::.ctor()
extern void ConfigureCanvasFromSettings__ctor_m49B858913FBAD8DF0CE30A245B1B1388866853B0 (void);
// 0x000000C7 System.Void SRDebugger.UI.Other.ConsoleTabQuickViewControl::Awake()
extern void ConsoleTabQuickViewControl_Awake_mC47A9B636FAF9AB0F5C828BD9140C19FE5EDECF3 (void);
// 0x000000C8 System.Void SRDebugger.UI.Other.ConsoleTabQuickViewControl::Update()
extern void ConsoleTabQuickViewControl_Update_mCBF8DBB53B71BF71D974212EA504403A7C9C504E (void);
// 0x000000C9 System.Boolean SRDebugger.UI.Other.ConsoleTabQuickViewControl::HasChanged(System.Int32,System.Int32&,System.Int32)
extern void ConsoleTabQuickViewControl_HasChanged_m4DC1735D4ABB02BE90CD354F89C2213858B59906 (void);
// 0x000000CA System.Void SRDebugger.UI.Other.ConsoleTabQuickViewControl::.ctor()
extern void ConsoleTabQuickViewControl__ctor_mCC0CF7D0784D8F8DA8D77A957C434F6E0013E840 (void);
// 0x000000CB System.Void SRDebugger.UI.Other.ConsoleTabQuickViewControl::.cctor()
extern void ConsoleTabQuickViewControl__cctor_m62E03F627F57CA2A56DED3D6236DFA3F69079B3F (void);
// 0x000000CC System.Void SRDebugger.UI.Other.DebugPanelBackgroundBehaviour::Awake()
extern void DebugPanelBackgroundBehaviour_Awake_m08933A92A9716F28F0781D315395A7F6A222550D (void);
// 0x000000CD System.Void SRDebugger.UI.Other.DebugPanelBackgroundBehaviour::Update()
extern void DebugPanelBackgroundBehaviour_Update_m28B8DE5A6D2060A6E632BEAB0C9938CC595094D7 (void);
// 0x000000CE System.Void SRDebugger.UI.Other.DebugPanelBackgroundBehaviour::.ctor()
extern void DebugPanelBackgroundBehaviour__ctor_m6042FABA9DB86A650D02ABE99229A81354BEB0E3 (void);
// 0x000000CF System.Boolean SRDebugger.UI.Other.DockConsoleController::get_IsVisible()
extern void DockConsoleController_get_IsVisible_m9A93603019C1029B8DA99F0D894131B489E0B080 (void);
// 0x000000D0 System.Void SRDebugger.UI.Other.DockConsoleController::set_IsVisible(System.Boolean)
extern void DockConsoleController_set_IsVisible_m5550EBDD156910079CEE467E10CA6C35A0D12EA1 (void);
// 0x000000D1 System.Void SRDebugger.UI.Other.DockConsoleController::Start()
extern void DockConsoleController_Start_mA270144F2A757B440370C29FA6DB71FE509BC760 (void);
// 0x000000D2 System.Void SRDebugger.UI.Other.DockConsoleController::OnDestroy()
extern void DockConsoleController_OnDestroy_m19C13B823FA607D9B692D135864B5499C6F960DD (void);
// 0x000000D3 System.Void SRDebugger.UI.Other.DockConsoleController::OnEnable()
extern void DockConsoleController_OnEnable_m971C640771EE7305A0FDC37F109EA259ECA68ED7 (void);
// 0x000000D4 System.Void SRDebugger.UI.Other.DockConsoleController::OnDisable()
extern void DockConsoleController_OnDisable_m55A514F7089CE261B40C349EBF8D2DDB62375D21 (void);
// 0x000000D5 System.Void SRDebugger.UI.Other.DockConsoleController::Update()
extern void DockConsoleController_Update_m34ABF4D475EF59461236A484BED98E06352F4C9A (void);
// 0x000000D6 System.Void SRDebugger.UI.Other.DockConsoleController::ConsoleOnUpdated(SRDebugger.Services.IConsoleService)
extern void DockConsoleController_ConsoleOnUpdated_m06B918B4E786C50087553DB5A5FFF67EB794A514 (void);
// 0x000000D7 System.Void SRDebugger.UI.Other.DockConsoleController::SetDropdownVisibility(System.Boolean)
extern void DockConsoleController_SetDropdownVisibility_m7F028A81B9599BF3DEABBB34C4628649BDEF213C (void);
// 0x000000D8 System.Void SRDebugger.UI.Other.DockConsoleController::SetAlignmentMode(SRDebugger.ConsoleAlignment)
extern void DockConsoleController_SetAlignmentMode_m60151647B95E5C8206A2E402FB1D46CF71FC9831 (void);
// 0x000000D9 System.Void SRDebugger.UI.Other.DockConsoleController::Refresh()
extern void DockConsoleController_Refresh_m7D3A2F9FBA840FF113EE1E861E67B837FB7E6A71 (void);
// 0x000000DA System.Void SRDebugger.UI.Other.DockConsoleController::RefreshAlpha()
extern void DockConsoleController_RefreshAlpha_m406B6203BB8C27BAB06348CE0D2A1A13A5C7C3AA (void);
// 0x000000DB System.Void SRDebugger.UI.Other.DockConsoleController::ToggleDropdownVisible()
extern void DockConsoleController_ToggleDropdownVisible_m5F2129C94BAD74FA3D104FDC553FE14344BE15F4 (void);
// 0x000000DC System.Void SRDebugger.UI.Other.DockConsoleController::MenuButtonPressed()
extern void DockConsoleController_MenuButtonPressed_m2683A5A3B6E9B12C6BDE490966081F5809762F7D (void);
// 0x000000DD System.Void SRDebugger.UI.Other.DockConsoleController::ClearButtonPressed()
extern void DockConsoleController_ClearButtonPressed_m60A2B907C0A9705E528312984E791F1C976A2362 (void);
// 0x000000DE System.Void SRDebugger.UI.Other.DockConsoleController::TogglesUpdated()
extern void DockConsoleController_TogglesUpdated_m2CD34F0929499E15ED4B37F4D2A14585AC1D6E19 (void);
// 0x000000DF System.Void SRDebugger.UI.Other.DockConsoleController::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void DockConsoleController_OnPointerEnter_m1AC0707D8B983EE7E08F8BAACA32DE1C041FACA9 (void);
// 0x000000E0 System.Void SRDebugger.UI.Other.DockConsoleController::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void DockConsoleController_OnPointerExit_m60BA2369FC13F567D226541B0E3DC4FD029BDD48 (void);
// 0x000000E1 System.Void SRDebugger.UI.Other.DockConsoleController::OnBeginDrag()
extern void DockConsoleController_OnBeginDrag_mCB4183BBBDCE680DCB1AEC58C782D9AC7E6A6BA0 (void);
// 0x000000E2 System.Void SRDebugger.UI.Other.DockConsoleController::OnEndDrag()
extern void DockConsoleController_OnEndDrag_m40E1990F866DE41F230E1C76C19CE5347F781190 (void);
// 0x000000E3 System.Void SRDebugger.UI.Other.DockConsoleController::.ctor()
extern void DockConsoleController__ctor_m2587A46DC92F304D3ADB703C72C08E1AFB40EEDF (void);
// 0x000000E4 System.Boolean SRDebugger.UI.Other.ErrorNotifier::get_IsVisible()
extern void ErrorNotifier_get_IsVisible_m67CFFD30BB221C81EC296828A7F42CA63A204A01 (void);
// 0x000000E5 System.Void SRDebugger.UI.Other.ErrorNotifier::Awake()
extern void ErrorNotifier_Awake_m63EB7A589008A25DD462E3D8E95B415B06B0EADF (void);
// 0x000000E6 System.Void SRDebugger.UI.Other.ErrorNotifier::ShowErrorWarning()
extern void ErrorNotifier_ShowErrorWarning_m6A6362C5BD44A44EE9946D37177E4726CF49FF7C (void);
// 0x000000E7 System.Void SRDebugger.UI.Other.ErrorNotifier::Update()
extern void ErrorNotifier_Update_m98CA626F77013067BF4A075402E0D3D45E269500 (void);
// 0x000000E8 System.Void SRDebugger.UI.Other.ErrorNotifier::.ctor()
extern void ErrorNotifier__ctor_m1BC463B2EFF2C95E05576476D86701C59CC89B63 (void);
// 0x000000E9 System.Void SRDebugger.UI.Other.HandleManager::Start()
extern void HandleManager_Start_mE5B1E952E6FF0953B09610842B516E75428C2113 (void);
// 0x000000EA System.Void SRDebugger.UI.Other.HandleManager::SetAlignment(SRDebugger.PinAlignment)
extern void HandleManager_SetAlignment_mCAB95A83E614CEF0621B6C02AA5C6BA199629DB2 (void);
// 0x000000EB System.Void SRDebugger.UI.Other.HandleManager::SetActive(UnityEngine.GameObject,System.Boolean)
extern void HandleManager_SetActive_m9C5B71504ACDB35926CCDBC764E6370564D1B3B9 (void);
// 0x000000EC System.Void SRDebugger.UI.Other.HandleManager::.ctor()
extern void HandleManager__ctor_mFC19187A2ABF4C444592C76A92CD907261020C9B (void);
// 0x000000ED System.Boolean SRDebugger.UI.Other.IEnableTab::get_IsEnabled()
// 0x000000EE System.Void SRDebugger.UI.Other.LoadingSpinnerBehaviour::Update()
extern void LoadingSpinnerBehaviour_Update_m2844BD52D379AB773F0A92E72E1F4FF431688AC8 (void);
// 0x000000EF System.Void SRDebugger.UI.Other.LoadingSpinnerBehaviour::.ctor()
extern void LoadingSpinnerBehaviour__ctor_m32E843D154FA7B9925C36B5EBFFAADFD7765E186 (void);
// 0x000000F0 System.Void SRDebugger.UI.Other.PinnedUIRoot::.ctor()
extern void PinnedUIRoot__ctor_m042696C7BC61A5C68AACD2B6DABB4EE3FA0CCB76 (void);
// 0x000000F1 System.String SRDebugger.UI.Other.SRTab::get_Title()
extern void SRTab_get_Title_m11B520C29B44EED6F6524EBA986B7F655CD353A8 (void);
// 0x000000F2 System.String SRDebugger.UI.Other.SRTab::get_LongTitle()
extern void SRTab_get_LongTitle_mFE958DE9DF7A6DB6C877ED62DE27BB309E4A0E06 (void);
// 0x000000F3 System.String SRDebugger.UI.Other.SRTab::get_Key()
extern void SRTab_get_Key_m750D8D8155AA02D8EAD1A7D1B1B612FFE51EB398 (void);
// 0x000000F4 System.Void SRDebugger.UI.Other.SRTab::.ctor()
extern void SRTab__ctor_m7DF82E9E895D7C0657D2504090E6F0F805ED4FF4 (void);
// 0x000000F5 SRDebugger.UI.Other.SRTab SRDebugger.UI.Other.SRTabController::get_ActiveTab()
extern void SRTabController_get_ActiveTab_mF5B9A2BC42571E4F89087B7CD6A3952516511E99 (void);
// 0x000000F6 System.Void SRDebugger.UI.Other.SRTabController::set_ActiveTab(SRDebugger.UI.Other.SRTab)
extern void SRTabController_set_ActiveTab_m9852C323DD5A85C97AA608D0A4C5086CA5B57419 (void);
// 0x000000F7 System.Collections.Generic.IList`1<SRDebugger.UI.Other.SRTab> SRDebugger.UI.Other.SRTabController::get_Tabs()
extern void SRTabController_get_Tabs_m6B6793BB0BBA5783D425C66139EE5575535AD6E3 (void);
// 0x000000F8 System.Void SRDebugger.UI.Other.SRTabController::add_ActiveTabChanged(System.Action`2<SRDebugger.UI.Other.SRTabController,SRDebugger.UI.Other.SRTab>)
extern void SRTabController_add_ActiveTabChanged_m3F6CF99462DC6B3D124C941646B299AFC12EDA7E (void);
// 0x000000F9 System.Void SRDebugger.UI.Other.SRTabController::remove_ActiveTabChanged(System.Action`2<SRDebugger.UI.Other.SRTabController,SRDebugger.UI.Other.SRTab>)
extern void SRTabController_remove_ActiveTabChanged_m7AD66B064EAA7B66193E0C2E074171D86E02A388 (void);
// 0x000000FA System.Void SRDebugger.UI.Other.SRTabController::AddTab(SRDebugger.UI.Other.SRTab,System.Boolean)
extern void SRTabController_AddTab_m59EDB3D3D90B9FFA9BA7688BD378BCA3908923A5 (void);
// 0x000000FB System.Void SRDebugger.UI.Other.SRTabController::MakeActive(SRDebugger.UI.Other.SRTab)
extern void SRTabController_MakeActive_m71265A8F1B3E64360C71470BE080E2586D6D6F7F (void);
// 0x000000FC System.Void SRDebugger.UI.Other.SRTabController::SortTabs()
extern void SRTabController_SortTabs_mA35573CB9A7B02361ACBC3E9DC123669BED8DD7C (void);
// 0x000000FD System.Void SRDebugger.UI.Other.SRTabController::.ctor()
extern void SRTabController__ctor_mFA9C7CB47A0301701A21ADE0C7CFC9EAF3BEFB2E (void);
// 0x000000FE System.Void SRDebugger.UI.Other.ScrollRectPatch::Awake()
extern void ScrollRectPatch_Awake_m6E45762E82EBF63291BF93CD63B1D3EF97D7A77E (void);
// 0x000000FF System.Void SRDebugger.UI.Other.ScrollRectPatch::.ctor()
extern void ScrollRectPatch__ctor_m35433DE9C0A08DA636FF68D877EA275700CF53C3 (void);
// 0x00000100 System.Void SRDebugger.UI.Other.ScrollSettingsBehaviour::Awake()
extern void ScrollSettingsBehaviour_Awake_mF7CAD8F64444BC853460B69C11D3A152C1799586 (void);
// 0x00000101 System.Void SRDebugger.UI.Other.ScrollSettingsBehaviour::.ctor()
extern void ScrollSettingsBehaviour__ctor_mF76FE2D118A3CB4D68591EC545A43416E290B0E9 (void);
// 0x00000102 System.Void SRDebugger.UI.Other.SetLayerFromSettings::Start()
extern void SetLayerFromSettings_Start_mCEDD638BE3830D613B9BBCCC15CBFCB9F1170F43 (void);
// 0x00000103 System.Void SRDebugger.UI.Other.SetLayerFromSettings::.ctor()
extern void SetLayerFromSettings__ctor_m14690E55727F97141E79FB27EF667AD40DA482C5 (void);
// 0x00000104 System.Void SRDebugger.UI.Other.TriggerRoot::.ctor()
extern void TriggerRoot__ctor_mE01DA04F380944BB91DC2ACBB8E4484F3495F651 (void);
// 0x00000105 System.Void SRDebugger.UI.Other.VersionTextBehaviour::Start()
extern void VersionTextBehaviour_Start_m924205D0CD12CF5A973B784A9BE40784498BB7F3 (void);
// 0x00000106 System.Void SRDebugger.UI.Other.VersionTextBehaviour::.ctor()
extern void VersionTextBehaviour__ctor_mFD97E71AB97EEBA0F140613259F78A7C057BD014 (void);
// 0x00000107 System.Void SRDebugger.UI.Controls.ConsoleEntryView::SetDataContext(System.Object)
extern void ConsoleEntryView_SetDataContext_mA913C2FAE3A5D658D93B73362440DC0DAF2388E6 (void);
// 0x00000108 System.Void SRDebugger.UI.Controls.ConsoleEntryView::Awake()
extern void ConsoleEntryView_Awake_mCA96C0EC35DA40F90D51BCC21CA88BE87C939704 (void);
// 0x00000109 System.Void SRDebugger.UI.Controls.ConsoleEntryView::.ctor()
extern void ConsoleEntryView__ctor_m04766042B66C2C23160CD618E80453E80E5F9CF9 (void);
// 0x0000010A System.Boolean SRDebugger.UI.Controls.ConsoleLogControl::get_ShowErrors()
extern void ConsoleLogControl_get_ShowErrors_mE2898A4D87C6D68638D7693E841D193C4284439E (void);
// 0x0000010B System.Void SRDebugger.UI.Controls.ConsoleLogControl::set_ShowErrors(System.Boolean)
extern void ConsoleLogControl_set_ShowErrors_mD94EC39E79D4802AD180AC09E7E3B603AE0AF564 (void);
// 0x0000010C System.Boolean SRDebugger.UI.Controls.ConsoleLogControl::get_ShowWarnings()
extern void ConsoleLogControl_get_ShowWarnings_m252D110D54598EB2872F3B65883247CA1B2A3AEC (void);
// 0x0000010D System.Void SRDebugger.UI.Controls.ConsoleLogControl::set_ShowWarnings(System.Boolean)
extern void ConsoleLogControl_set_ShowWarnings_mDDF0C7F8AB7182118FE67FD595848109D5B4D35E (void);
// 0x0000010E System.Boolean SRDebugger.UI.Controls.ConsoleLogControl::get_ShowInfo()
extern void ConsoleLogControl_get_ShowInfo_m941035A10580FDD257BE9A7D8D853A957D16621A (void);
// 0x0000010F System.Void SRDebugger.UI.Controls.ConsoleLogControl::set_ShowInfo(System.Boolean)
extern void ConsoleLogControl_set_ShowInfo_m65CB22ED9FFBD81F5153551304A2E101B3A49FE5 (void);
// 0x00000110 System.Boolean SRDebugger.UI.Controls.ConsoleLogControl::get_EnableSelection()
extern void ConsoleLogControl_get_EnableSelection_m5A41F357895A79837D0BA6D74B7516D64D803B86 (void);
// 0x00000111 System.Void SRDebugger.UI.Controls.ConsoleLogControl::set_EnableSelection(System.Boolean)
extern void ConsoleLogControl_set_EnableSelection_m8405D98E1304F2CBE4F124ABC715E660682D555E (void);
// 0x00000112 System.String SRDebugger.UI.Controls.ConsoleLogControl::get_Filter()
extern void ConsoleLogControl_get_Filter_m0DE946320418D7760BE891474569E61A222BEF5B (void);
// 0x00000113 System.Void SRDebugger.UI.Controls.ConsoleLogControl::set_Filter(System.String)
extern void ConsoleLogControl_set_Filter_m428CE6D787570E20EDFDF96D7F79A42B1EB62BB0 (void);
// 0x00000114 System.Void SRDebugger.UI.Controls.ConsoleLogControl::Awake()
extern void ConsoleLogControl_Awake_m209CC9A0AA477333228380F012274BC0DC365012 (void);
// 0x00000115 System.Void SRDebugger.UI.Controls.ConsoleLogControl::Start()
extern void ConsoleLogControl_Start_mF81FAEC5142F4AAA33BDC99FCC508EE9DF3AD9F8 (void);
// 0x00000116 System.Collections.IEnumerator SRDebugger.UI.Controls.ConsoleLogControl::ScrollToBottom()
extern void ConsoleLogControl_ScrollToBottom_m693B0277637AACE265741312405CD162BB9B901D (void);
// 0x00000117 System.Void SRDebugger.UI.Controls.ConsoleLogControl::OnDestroy()
extern void ConsoleLogControl_OnDestroy_m417AAEA07E080E20B32A185061BF2DC5A9378496 (void);
// 0x00000118 System.Void SRDebugger.UI.Controls.ConsoleLogControl::OnSelectedItemChanged(System.Object)
extern void ConsoleLogControl_OnSelectedItemChanged_m99E53C343006E7C7E18716C294472F47967BE741 (void);
// 0x00000119 System.Void SRDebugger.UI.Controls.ConsoleLogControl::Update()
extern void ConsoleLogControl_Update_m9B9046E248910545B2F3BBC01790B15E28A0545E (void);
// 0x0000011A System.Void SRDebugger.UI.Controls.ConsoleLogControl::Refresh()
extern void ConsoleLogControl_Refresh_mA1CE8F0B6243FAC49FF278A64F3AF762855A42B1 (void);
// 0x0000011B System.Void SRDebugger.UI.Controls.ConsoleLogControl::SetIsDirty()
extern void ConsoleLogControl_SetIsDirty_m0A3FDCAC2D2926DA9804606B8BC00BE908A54EF4 (void);
// 0x0000011C System.Void SRDebugger.UI.Controls.ConsoleLogControl::ConsoleOnUpdated(SRDebugger.Services.IConsoleService)
extern void ConsoleLogControl_ConsoleOnUpdated_m95D80C28284014D49A6D9F157E6DBBF44DA4B9AB (void);
// 0x0000011D System.Void SRDebugger.UI.Controls.ConsoleLogControl::.ctor()
extern void ConsoleLogControl__ctor_mB8E454312D725AA3FDA732E323426040AD0DF1C2 (void);
// 0x0000011E SRF.Helpers.PropertyReference SRDebugger.UI.Controls.DataBoundControl::get_Property()
extern void DataBoundControl_get_Property_m5F991B3284FE886D27C8F315A2A46D95B47B62F1 (void);
// 0x0000011F System.Boolean SRDebugger.UI.Controls.DataBoundControl::get_IsReadOnly()
extern void DataBoundControl_get_IsReadOnly_m68BE55030740ADA6AFD78204DB4FF058081C718A (void);
// 0x00000120 System.String SRDebugger.UI.Controls.DataBoundControl::get_PropertyName()
extern void DataBoundControl_get_PropertyName_m60026BF1A47D8220660DBAFB4F1873160AD29A3D (void);
// 0x00000121 System.Void SRDebugger.UI.Controls.DataBoundControl::set_PropertyName(System.String)
extern void DataBoundControl_set_PropertyName_mCEA9801AA1402F5099580C98CCCD4DA81EE98AF3 (void);
// 0x00000122 System.Void SRDebugger.UI.Controls.DataBoundControl::Bind(System.String,SRF.Helpers.PropertyReference)
extern void DataBoundControl_Bind_m9CD9105A8A0FE45EF4B546F598819D295E8A27F7 (void);
// 0x00000123 System.Void SRDebugger.UI.Controls.DataBoundControl::UpdateValue(System.Object)
extern void DataBoundControl_UpdateValue_m8043360BCD829C35BF12E4F0A09B9CAB73E696AA (void);
// 0x00000124 System.Void SRDebugger.UI.Controls.DataBoundControl::Refresh()
extern void DataBoundControl_Refresh_m56274BF1AE902EB6F6F91D4C34F0DF45EC4DA172 (void);
// 0x00000125 System.Void SRDebugger.UI.Controls.DataBoundControl::OnBind(System.String,System.Type)
extern void DataBoundControl_OnBind_mA0A59D3769BD9CDB0EFEC1BA4B6697ED02C6EFF5 (void);
// 0x00000126 System.Void SRDebugger.UI.Controls.DataBoundControl::OnValueUpdated(System.Object)
// 0x00000127 System.Boolean SRDebugger.UI.Controls.DataBoundControl::CanBind(System.Type,System.Boolean)
// 0x00000128 System.Void SRDebugger.UI.Controls.DataBoundControl::Start()
extern void DataBoundControl_Start_mE47E42E4E4B870CE1968309C60B809C3B6F448E9 (void);
// 0x00000129 System.Void SRDebugger.UI.Controls.DataBoundControl::OnEnable()
extern void DataBoundControl_OnEnable_m912E56F06BEE78BEBB324E2361C90B48CCB279D6 (void);
// 0x0000012A System.Void SRDebugger.UI.Controls.DataBoundControl::.ctor()
extern void DataBoundControl__ctor_m73184E5E1B83A246F2D602726A48ADE9DF63D108 (void);
// 0x0000012B System.Boolean SRDebugger.UI.Controls.OptionsControlBase::get_SelectionModeEnabled()
extern void OptionsControlBase_get_SelectionModeEnabled_m495FB680879E0E208BA1F188539796873212030E (void);
// 0x0000012C System.Void SRDebugger.UI.Controls.OptionsControlBase::set_SelectionModeEnabled(System.Boolean)
extern void OptionsControlBase_set_SelectionModeEnabled_mEC811FB41EA4DBE21EBF001609E44B7DEC34285E (void);
// 0x0000012D System.Boolean SRDebugger.UI.Controls.OptionsControlBase::get_IsSelected()
extern void OptionsControlBase_get_IsSelected_mF93AF8924058BDB88F13981B75C9D4997AD3A80A (void);
// 0x0000012E System.Void SRDebugger.UI.Controls.OptionsControlBase::set_IsSelected(System.Boolean)
extern void OptionsControlBase_set_IsSelected_m2DA440B23103A9E3B4FBA7C46B7AE73EB5F3DB71 (void);
// 0x0000012F System.Void SRDebugger.UI.Controls.OptionsControlBase::Awake()
extern void OptionsControlBase_Awake_m11072F2419B7984C647C260ED4D7C76D0C35C498 (void);
// 0x00000130 System.Void SRDebugger.UI.Controls.OptionsControlBase::OnEnable()
extern void OptionsControlBase_OnEnable_mC24A03395267A2E5D22CF2263D9875BAD2E1E4CE (void);
// 0x00000131 System.Void SRDebugger.UI.Controls.OptionsControlBase::Refresh()
extern void OptionsControlBase_Refresh_mDEB82F87BCDE85F9D69CD734B9100224CA0503C4 (void);
// 0x00000132 System.Void SRDebugger.UI.Controls.OptionsControlBase::.ctor()
extern void OptionsControlBase__ctor_m55AC55BEA87783CE7B9C944E0A845D9FA0969E18 (void);
// 0x00000133 System.Void SRDebugger.UI.Controls.InfoBlock::.ctor()
extern void InfoBlock__ctor_m5E7CC3D780D5BA13CEF105E7FD53293EB12DD2F2 (void);
// 0x00000134 System.Void SRDebugger.UI.Controls.MultiTapButton::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void MultiTapButton_OnPointerClick_m44C6E3E9BB8620FCF8880B7B47AA590A704E68FF (void);
// 0x00000135 System.Void SRDebugger.UI.Controls.MultiTapButton::.ctor()
extern void MultiTapButton__ctor_m1CEE7BCAEDD2CE9010AD4DF39B6AD13CC5773396 (void);
// 0x00000136 System.Void SRDebugger.UI.Controls.PinEntryControlCallback::.ctor(System.Object,System.IntPtr)
extern void PinEntryControlCallback__ctor_mB0D2FFB32F4DEE039AC9E5DE66A90F234A004746 (void);
// 0x00000137 System.Void SRDebugger.UI.Controls.PinEntryControlCallback::Invoke(System.Collections.Generic.IList`1<System.Int32>,System.Boolean)
extern void PinEntryControlCallback_Invoke_m97522D755F000DE5EE4AF71FEE25CC179E95554C (void);
// 0x00000138 System.IAsyncResult SRDebugger.UI.Controls.PinEntryControlCallback::BeginInvoke(System.Collections.Generic.IList`1<System.Int32>,System.Boolean,System.AsyncCallback,System.Object)
extern void PinEntryControlCallback_BeginInvoke_mBA815892B34430F37DF28173796AA385D961ADAF (void);
// 0x00000139 System.Void SRDebugger.UI.Controls.PinEntryControlCallback::EndInvoke(System.IAsyncResult)
extern void PinEntryControlCallback_EndInvoke_m8FB19C9C487591CA940BE9FBEB8507EDFD99740C (void);
// 0x0000013A System.Void SRDebugger.UI.Controls.PinEntryControl::add_Complete(SRDebugger.UI.Controls.PinEntryControlCallback)
extern void PinEntryControl_add_Complete_mAC3FA4E9418A61FD6361C3F5C81444A85A6039D5 (void);
// 0x0000013B System.Void SRDebugger.UI.Controls.PinEntryControl::remove_Complete(SRDebugger.UI.Controls.PinEntryControlCallback)
extern void PinEntryControl_remove_Complete_m5311024470631FD13B52F4E3ACE2AEA02EECF423 (void);
// 0x0000013C System.Void SRDebugger.UI.Controls.PinEntryControl::Awake()
extern void PinEntryControl_Awake_mB05CBEB732ECECD18C249D837158D7416967E447 (void);
// 0x0000013D System.Void SRDebugger.UI.Controls.PinEntryControl::Update()
extern void PinEntryControl_Update_m47E9FDB259B6B05853580925E566318516DC8CA6 (void);
// 0x0000013E System.Void SRDebugger.UI.Controls.PinEntryControl::Show()
extern void PinEntryControl_Show_m7806CBA0DBBD1FB5AFB83AA248CC5B52A1AA4224 (void);
// 0x0000013F System.Void SRDebugger.UI.Controls.PinEntryControl::Hide()
extern void PinEntryControl_Hide_m938DBB024550C1471D3C0FA4254B12FE05AF7D34 (void);
// 0x00000140 System.Void SRDebugger.UI.Controls.PinEntryControl::Clear()
extern void PinEntryControl_Clear_mACE962439CE5785C9CDDF929F6E590476C068ABD (void);
// 0x00000141 System.Void SRDebugger.UI.Controls.PinEntryControl::PlayInvalidCodeAnimation()
extern void PinEntryControl_PlayInvalidCodeAnimation_m6A884A02E25182F16BAB731BE0CF5459A9297B92 (void);
// 0x00000142 System.Void SRDebugger.UI.Controls.PinEntryControl::OnComplete()
extern void PinEntryControl_OnComplete_m9FF538EB049BDCC1C874337761FFA076555E9A8A (void);
// 0x00000143 System.Void SRDebugger.UI.Controls.PinEntryControl::OnCancel()
extern void PinEntryControl_OnCancel_mCE4F865F8025877C817041EEDAFB6629F202D561 (void);
// 0x00000144 System.Void SRDebugger.UI.Controls.PinEntryControl::CancelButtonPressed()
extern void PinEntryControl_CancelButtonPressed_mF527227303235C1128D005601A1F8E704DA91BE6 (void);
// 0x00000145 System.Void SRDebugger.UI.Controls.PinEntryControl::PushNumber(System.Int32)
extern void PinEntryControl_PushNumber_m44B8F6D8C784BB2F790ADFFFBDCF6F5E4405C2CA (void);
// 0x00000146 System.Void SRDebugger.UI.Controls.PinEntryControl::RefreshState()
extern void PinEntryControl_RefreshState_m101B41AAD963F421EFE0530550396A1E52E7C03E (void);
// 0x00000147 System.Void SRDebugger.UI.Controls.PinEntryControl::.ctor()
extern void PinEntryControl__ctor_m9666243DC6A514FF98A3A540DD7F5C4AE78F2F0F (void);
// 0x00000148 System.Void SRDebugger.UI.Controls.ProfilerMemoryBlock::OnEnable()
extern void ProfilerMemoryBlock_OnEnable_m14DA18FFE85953D09CE48F35C617D38B356CA12C (void);
// 0x00000149 System.Void SRDebugger.UI.Controls.ProfilerMemoryBlock::Update()
extern void ProfilerMemoryBlock_Update_m6DAEDF402DF959D8604CD73C07819B309F5C83D7 (void);
// 0x0000014A System.Void SRDebugger.UI.Controls.ProfilerMemoryBlock::TriggerRefresh()
extern void ProfilerMemoryBlock_TriggerRefresh_mD3AADC6124FB85B6F1310597D574E9F181F204B5 (void);
// 0x0000014B System.Void SRDebugger.UI.Controls.ProfilerMemoryBlock::TriggerCleanup()
extern void ProfilerMemoryBlock_TriggerCleanup_m421E1A2E1812189132EC53B894D1340EFF9D9285 (void);
// 0x0000014C System.Collections.IEnumerator SRDebugger.UI.Controls.ProfilerMemoryBlock::CleanUp()
extern void ProfilerMemoryBlock_CleanUp_m136D3FE5FA37AEE662278E308780AFB851180F5D (void);
// 0x0000014D System.Void SRDebugger.UI.Controls.ProfilerMemoryBlock::.ctor()
extern void ProfilerMemoryBlock__ctor_m14511FD971869B71E4B1DA28927BF2F1C232AC6D (void);
// 0x0000014E System.Void SRDebugger.UI.Controls.ProfilerMonoBlock::OnEnable()
extern void ProfilerMonoBlock_OnEnable_m2CA458F7DCE6923AA25AB89A66A44B239E3F7499 (void);
// 0x0000014F System.Void SRDebugger.UI.Controls.ProfilerMonoBlock::Update()
extern void ProfilerMonoBlock_Update_mD6FEEFB6199B13F025B1940AA8D11F94334F641D (void);
// 0x00000150 System.Void SRDebugger.UI.Controls.ProfilerMonoBlock::TriggerRefresh()
extern void ProfilerMonoBlock_TriggerRefresh_m62536D119807386AFB0B89A4F62704B3B1D5745E (void);
// 0x00000151 System.Void SRDebugger.UI.Controls.ProfilerMonoBlock::TriggerCollection()
extern void ProfilerMonoBlock_TriggerCollection_m35231433A40708F39CBF1345661F4CDE2DF78547 (void);
// 0x00000152 System.Void SRDebugger.UI.Controls.ProfilerMonoBlock::.ctor()
extern void ProfilerMonoBlock__ctor_m22269EAB87064C7DC77B82A2F99ADCC0F1256608 (void);
// 0x00000153 System.Void SRDebugger.UI.Controls.ProfilerEnableControl::Start()
extern void ProfilerEnableControl_Start_mB04A0A63E5A98347D3B6A7D2E80B9F2C26943810 (void);
// 0x00000154 System.Void SRDebugger.UI.Controls.ProfilerEnableControl::UpdateLabels()
extern void ProfilerEnableControl_UpdateLabels_m079656E781DABF7A91699C01C3F805F7AC7F8BAA (void);
// 0x00000155 System.Void SRDebugger.UI.Controls.ProfilerEnableControl::Update()
extern void ProfilerEnableControl_Update_mB767DBE8DA636EAFA1449C35790E5DB65CBDE562 (void);
// 0x00000156 System.Void SRDebugger.UI.Controls.ProfilerEnableControl::ToggleProfiler()
extern void ProfilerEnableControl_ToggleProfiler_mA0555DF2A3E8C83F02D714F646EE8A633ED744D4 (void);
// 0x00000157 System.Void SRDebugger.UI.Controls.ProfilerEnableControl::.ctor()
extern void ProfilerEnableControl__ctor_m03216744E6CCE203FE377CA38782385BB4EA6AF8 (void);
// 0x00000158 System.Void SRDebugger.UI.Controls.ProfilerGraphAxisLabel::Update()
extern void ProfilerGraphAxisLabel_Update_mC04347429BE14F8F02449E60C0DEA126B6232365 (void);
// 0x00000159 System.Void SRDebugger.UI.Controls.ProfilerGraphAxisLabel::SetValue(System.Single,System.Single)
extern void ProfilerGraphAxisLabel_SetValue_mC1605FF4E6ABA23FB088227C3DCDA7F9B542ADA0 (void);
// 0x0000015A System.Void SRDebugger.UI.Controls.ProfilerGraphAxisLabel::SetValueInternal(System.Single)
extern void ProfilerGraphAxisLabel_SetValueInternal_m136556544EBA74B2A56519E5F87A4C937202CD58 (void);
// 0x0000015B System.Void SRDebugger.UI.Controls.ProfilerGraphAxisLabel::.ctor()
extern void ProfilerGraphAxisLabel__ctor_m068F74E672BA78441A6BC039FDDEE484ECDC64AB (void);
// 0x0000015C System.Void SRDebugger.UI.Controls.ProfilerGraphControl::Awake()
extern void ProfilerGraphControl_Awake_mACE59E0243C36EB98AA8C2CCC5D5EC729BCE6D70 (void);
// 0x0000015D System.Void SRDebugger.UI.Controls.ProfilerGraphControl::Start()
extern void ProfilerGraphControl_Start_m9354A0EDDBD30AE6FDB783A98A4A9A72D73AB492 (void);
// 0x0000015E System.Void SRDebugger.UI.Controls.ProfilerGraphControl::Update()
extern void ProfilerGraphControl_Update_mA960AD00604C50DFFB4C911A9D0F94DD5AC3F247 (void);
// 0x0000015F System.Void SRDebugger.UI.Controls.ProfilerGraphControl::OnPopulateMesh(UnityEngine.Mesh)
extern void ProfilerGraphControl_OnPopulateMesh_mC269E99E558F5B87485254DEACF90FF0975F3512 (void);
// 0x00000160 System.Void SRDebugger.UI.Controls.ProfilerGraphControl::DrawDataPoint(System.Single,System.Single,SRDebugger.Services.ProfilerFrame)
extern void ProfilerGraphControl_DrawDataPoint_m1EC4BE9B3972563274449999579423E72912D1A2 (void);
// 0x00000161 System.Void SRDebugger.UI.Controls.ProfilerGraphControl::DrawAxis(System.Single,System.Single,SRDebugger.UI.Controls.ProfilerGraphAxisLabel)
extern void ProfilerGraphControl_DrawAxis_m36CAFD6B3BBE5757D1594FE3108C964227682C35 (void);
// 0x00000162 System.Void SRDebugger.UI.Controls.ProfilerGraphControl::AddRect(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern void ProfilerGraphControl_AddRect_m8E1E0F717D296DF4AC32F417AF1ED41CBFA50DC1 (void);
// 0x00000163 SRDebugger.Services.ProfilerFrame SRDebugger.UI.Controls.ProfilerGraphControl::GetFrame(System.Int32)
extern void ProfilerGraphControl_GetFrame_m52BAE866B6348AA71B6E96153132B2368EAE07A1 (void);
// 0x00000164 System.Int32 SRDebugger.UI.Controls.ProfilerGraphControl::CalculateVisibleDataPointCount()
extern void ProfilerGraphControl_CalculateVisibleDataPointCount_m459DCF2792A2D6524EBD09CCD33B62FB20B3B986 (void);
// 0x00000165 System.Int32 SRDebugger.UI.Controls.ProfilerGraphControl::GetFrameBufferCurrentSize()
extern void ProfilerGraphControl_GetFrameBufferCurrentSize_mFC45B36A8E59C0B4D78A9D2BD49F611D50437FEC (void);
// 0x00000166 System.Int32 SRDebugger.UI.Controls.ProfilerGraphControl::GetFrameBufferMaxSize()
extern void ProfilerGraphControl_GetFrameBufferMaxSize_m6067991A69A735736454B248737E607120524E4D (void);
// 0x00000167 System.Single SRDebugger.UI.Controls.ProfilerGraphControl::CalculateMaxFrameTime()
extern void ProfilerGraphControl_CalculateMaxFrameTime_m25AAB2A70060F591D0CD09DE947B4D6EA948B7D0 (void);
// 0x00000168 SRDebugger.UI.Controls.ProfilerGraphAxisLabel SRDebugger.UI.Controls.ProfilerGraphControl::GetAxisLabel(System.Int32)
extern void ProfilerGraphControl_GetAxisLabel_m32C3EC33E25EDCD988916088C1F0A1739430D1A8 (void);
// 0x00000169 System.Void SRDebugger.UI.Controls.ProfilerGraphControl::.ctor()
extern void ProfilerGraphControl__ctor_m40C428DFF7A32B51241C04CE0C907B8CFBDFD11F (void);
// 0x0000016A System.Void SRDebugger.UI.Controls.ProfilerGraphControl::.cctor()
extern void ProfilerGraphControl__cctor_m35C21A6CA9D6421B3CD0EB643563828CC881B972 (void);
// 0x0000016B System.Boolean SRDebugger.UI.Controls.SRTabButton::get_IsActive()
extern void SRTabButton_get_IsActive_m8FF75A9268816A50D91459B808D0248AFFD65FAE (void);
// 0x0000016C System.Void SRDebugger.UI.Controls.SRTabButton::set_IsActive(System.Boolean)
extern void SRTabButton_set_IsActive_m74BD99618AB4F702078BE69F6DB51CE6A408C7F4 (void);
// 0x0000016D System.Void SRDebugger.UI.Controls.SRTabButton::.ctor()
extern void SRTabButton__ctor_mCC38FEE371562F6385515992101FA36633CAA888 (void);
// 0x0000016E SRF.Helpers.MethodReference SRDebugger.UI.Controls.Data.ActionControl::get_Method()
extern void ActionControl_get_Method_mB335111A30E63EF41278A888276133BA3036DCC3 (void);
// 0x0000016F System.Void SRDebugger.UI.Controls.Data.ActionControl::Start()
extern void ActionControl_Start_m71EAFB2FE493618040AA7FDBA70E2F53F1670DD4 (void);
// 0x00000170 System.Void SRDebugger.UI.Controls.Data.ActionControl::ButtonOnClick()
extern void ActionControl_ButtonOnClick_m02F8DD7BCD1E1764070B0A17F8F70003131F8192 (void);
// 0x00000171 System.Void SRDebugger.UI.Controls.Data.ActionControl::SetMethod(System.String,SRF.Helpers.MethodReference)
extern void ActionControl_SetMethod_m9C81AD38B1E7C2395C5E968C44FFD681B37F944F (void);
// 0x00000172 System.Void SRDebugger.UI.Controls.Data.ActionControl::.ctor()
extern void ActionControl__ctor_mEF06B8692763DBE6DC92A7230880324C71998E51 (void);
// 0x00000173 System.Void SRDebugger.UI.Controls.Data.BoolControl::Start()
extern void BoolControl_Start_m6766657C170BDABD643F3C154D211C68501E308F (void);
// 0x00000174 System.Void SRDebugger.UI.Controls.Data.BoolControl::ToggleOnValueChanged(System.Boolean)
extern void BoolControl_ToggleOnValueChanged_mF1D75E19EA19041089D471BE8C1032684D785105 (void);
// 0x00000175 System.Void SRDebugger.UI.Controls.Data.BoolControl::OnBind(System.String,System.Type)
extern void BoolControl_OnBind_mFC4B881D0F42CE880E2A7220B0D5CC4B3CDFC924 (void);
// 0x00000176 System.Void SRDebugger.UI.Controls.Data.BoolControl::OnValueUpdated(System.Object)
extern void BoolControl_OnValueUpdated_m93210E016B513BE108EF3B28E7A8D3DC27880329 (void);
// 0x00000177 System.Boolean SRDebugger.UI.Controls.Data.BoolControl::CanBind(System.Type,System.Boolean)
extern void BoolControl_CanBind_mF99504B0FC3BC77F6697EFDE27EC4EF8D5DF0008 (void);
// 0x00000178 System.Void SRDebugger.UI.Controls.Data.BoolControl::.ctor()
extern void BoolControl__ctor_m021E63B02E82C4B386670062948F84438980405F (void);
// 0x00000179 System.Void SRDebugger.UI.Controls.Data.EnumControl::Start()
extern void EnumControl_Start_mD8339DF53A036F7EE2B32E6943D8C3AFC2F702DE (void);
// 0x0000017A System.Void SRDebugger.UI.Controls.Data.EnumControl::OnBind(System.String,System.Type)
extern void EnumControl_OnBind_m947B07C3FF95069D1CB279B0A7681C77469DE739 (void);
// 0x0000017B System.Void SRDebugger.UI.Controls.Data.EnumControl::OnValueUpdated(System.Object)
extern void EnumControl_OnValueUpdated_mA7523F3C0CC7BACFE323D7003CAFC1973E739303 (void);
// 0x0000017C System.Boolean SRDebugger.UI.Controls.Data.EnumControl::CanBind(System.Type,System.Boolean)
extern void EnumControl_CanBind_mCA03B23E414B7899C9C9FA5D2CCAC874E087EA45 (void);
// 0x0000017D System.Void SRDebugger.UI.Controls.Data.EnumControl::SetIndex(System.Int32)
extern void EnumControl_SetIndex_m9DE6AAB5FE88153580DC61DB6B28BCD6E74C2C7B (void);
// 0x0000017E System.Void SRDebugger.UI.Controls.Data.EnumControl::GoToNext()
extern void EnumControl_GoToNext_m9AA191C30CEAA89426B59DBC80952B619E0D78E1 (void);
// 0x0000017F System.Void SRDebugger.UI.Controls.Data.EnumControl::GoToPrevious()
extern void EnumControl_GoToPrevious_mDFA0FE20BE8129CA9055C03B29B86E95AFA0FBCD (void);
// 0x00000180 System.Void SRDebugger.UI.Controls.Data.EnumControl::.ctor()
extern void EnumControl__ctor_mDA6C8F637B2D921A4E63EF7B5BC2AB35C3B463D8 (void);
// 0x00000181 System.Void SRDebugger.UI.Controls.Data.NumberControl::Start()
extern void NumberControl_Start_m154F62C16A841BE25B5CB3E06F14979DEAB9DC50 (void);
// 0x00000182 System.Void SRDebugger.UI.Controls.Data.NumberControl::OnValueChanged(System.String)
extern void NumberControl_OnValueChanged_mCF3796638E0273B9CEE7DED3B1C56E3CD76EEC01 (void);
// 0x00000183 System.Void SRDebugger.UI.Controls.Data.NumberControl::OnBind(System.String,System.Type)
extern void NumberControl_OnBind_m5B5B25F9C15C3DDB9A81F80532AB1DE1EECAD833 (void);
// 0x00000184 System.Void SRDebugger.UI.Controls.Data.NumberControl::OnValueUpdated(System.Object)
extern void NumberControl_OnValueUpdated_m1401CDE51125A8A3C2BC9300B69342D0903BE5E7 (void);
// 0x00000185 System.Boolean SRDebugger.UI.Controls.Data.NumberControl::CanBind(System.Type,System.Boolean)
extern void NumberControl_CanBind_m12E52AB7A3693EDD25634EA00783BAA1D36848C5 (void);
// 0x00000186 System.Boolean SRDebugger.UI.Controls.Data.NumberControl::IsIntegerType(System.Type)
extern void NumberControl_IsIntegerType_mA6BF49E032940452A875985DA5FB1F861070698E (void);
// 0x00000187 System.Boolean SRDebugger.UI.Controls.Data.NumberControl::IsDecimalType(System.Type)
extern void NumberControl_IsDecimalType_m7732FB59605E35A236965D90EC37EDD20DBEEA48 (void);
// 0x00000188 System.Double SRDebugger.UI.Controls.Data.NumberControl::GetMaxValue(System.Type)
extern void NumberControl_GetMaxValue_m9AC535BBB2E8C642178B18B7CA33BC8159D5E53A (void);
// 0x00000189 System.Double SRDebugger.UI.Controls.Data.NumberControl::GetMinValue(System.Type)
extern void NumberControl_GetMinValue_mADAE6529D0EDC8B78A5598849A30D706228AC4AE (void);
// 0x0000018A System.Void SRDebugger.UI.Controls.Data.NumberControl::.ctor()
extern void NumberControl__ctor_mC0ADA44BDA875A47949C5FF7F0ACD631804D83B3 (void);
// 0x0000018B System.Void SRDebugger.UI.Controls.Data.NumberControl::.cctor()
extern void NumberControl__cctor_m2A3AF00D0881594B4569D01644D44809210B1D21 (void);
// 0x0000018C System.Void SRDebugger.UI.Controls.Data.ReadOnlyControl::Start()
extern void ReadOnlyControl_Start_m71DBC1EB9EBB10F47EA95AEF6DFFECF0A5214FF7 (void);
// 0x0000018D System.Void SRDebugger.UI.Controls.Data.ReadOnlyControl::OnBind(System.String,System.Type)
extern void ReadOnlyControl_OnBind_mEDBC04066ED37526EE2F1A1029D8B28CA84778FC (void);
// 0x0000018E System.Void SRDebugger.UI.Controls.Data.ReadOnlyControl::OnValueUpdated(System.Object)
extern void ReadOnlyControl_OnValueUpdated_mCF4AFC87CC5573F1F5ADC9C7D9578B98134315F2 (void);
// 0x0000018F System.Boolean SRDebugger.UI.Controls.Data.ReadOnlyControl::CanBind(System.Type,System.Boolean)
extern void ReadOnlyControl_CanBind_mA35D75186D867C1AEECCCC15D862C7F5C0EB30B2 (void);
// 0x00000190 System.Void SRDebugger.UI.Controls.Data.ReadOnlyControl::.ctor()
extern void ReadOnlyControl__ctor_mCB64B2A7A5A3585E4481788A78EE612E2776CE8A (void);
// 0x00000191 System.Void SRDebugger.UI.Controls.Data.StringControl::Start()
extern void StringControl_Start_m23811B91F9EC11CD0E7857E001ABB131A9A0AC7A (void);
// 0x00000192 System.Void SRDebugger.UI.Controls.Data.StringControl::OnValueChanged(System.String)
extern void StringControl_OnValueChanged_mF53E8EDA7B2B3D2314E2F1682F133561D0EA08C4 (void);
// 0x00000193 System.Void SRDebugger.UI.Controls.Data.StringControl::OnBind(System.String,System.Type)
extern void StringControl_OnBind_m27D88B67D88823252C4BBF13667BF7CC5AC54342 (void);
// 0x00000194 System.Void SRDebugger.UI.Controls.Data.StringControl::OnValueUpdated(System.Object)
extern void StringControl_OnValueUpdated_m69E39889E5C8BD0DC5B4338299206CE9B54171BF (void);
// 0x00000195 System.Boolean SRDebugger.UI.Controls.Data.StringControl::CanBind(System.Type,System.Boolean)
extern void StringControl_CanBind_mA3C802775981E37459B77468EBB5A87FB2F99892 (void);
// 0x00000196 System.Void SRDebugger.UI.Controls.Data.StringControl::.ctor()
extern void StringControl__ctor_m116CB6B1A1C41F217BCD588036E46FEC3E26C235 (void);
// 0x00000197 System.Void SRDebugger.Services.BugReport::.ctor()
extern void BugReport__ctor_m0F7DCA7A97A71AEC5F743C8D5522E571EFA0E299 (void);
// 0x00000198 System.Void SRDebugger.Services.BugReportCompleteCallback::.ctor(System.Object,System.IntPtr)
extern void BugReportCompleteCallback__ctor_m7E9DC54D186F331ABF7DCD65E98F0539EFD4F7F6 (void);
// 0x00000199 System.Void SRDebugger.Services.BugReportCompleteCallback::Invoke(System.Boolean,System.String)
extern void BugReportCompleteCallback_Invoke_m3EB23A169711E88A56349D15AAB77B8EB2980BD7 (void);
// 0x0000019A System.IAsyncResult SRDebugger.Services.BugReportCompleteCallback::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern void BugReportCompleteCallback_BeginInvoke_m7C85F653D1C17B29FC4D5268D7CE05445D07799F (void);
// 0x0000019B System.Void SRDebugger.Services.BugReportCompleteCallback::EndInvoke(System.IAsyncResult)
extern void BugReportCompleteCallback_EndInvoke_m1AB06FCA285D06B965742D459A360C5C87578EB8 (void);
// 0x0000019C System.Void SRDebugger.Services.BugReportProgressCallback::.ctor(System.Object,System.IntPtr)
extern void BugReportProgressCallback__ctor_mE1B5AE82F88DB214714C3754284A3AE530CF71CC (void);
// 0x0000019D System.Void SRDebugger.Services.BugReportProgressCallback::Invoke(System.Single)
extern void BugReportProgressCallback_Invoke_mC2315CA5EB755394967BBCEC4EF5D67366296C5A (void);
// 0x0000019E System.IAsyncResult SRDebugger.Services.BugReportProgressCallback::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern void BugReportProgressCallback_BeginInvoke_m11199B9570B4B5655DAEA61A72005C9FF79BC477 (void);
// 0x0000019F System.Void SRDebugger.Services.BugReportProgressCallback::EndInvoke(System.IAsyncResult)
extern void BugReportProgressCallback_EndInvoke_m9A785D9E4D1411C262138630E1396B301B4761F1 (void);
// 0x000001A0 System.Void SRDebugger.Services.IBugReportService::SendBugReport(SRDebugger.Services.BugReport,SRDebugger.Services.BugReportCompleteCallback,SRDebugger.Services.BugReportProgressCallback)
// 0x000001A1 System.Void SRDebugger.Services.ConsoleUpdatedEventHandler::.ctor(System.Object,System.IntPtr)
extern void ConsoleUpdatedEventHandler__ctor_m9BC3C608183B85D9F6D2F9EE52C415F9CD58EC63 (void);
// 0x000001A2 System.Void SRDebugger.Services.ConsoleUpdatedEventHandler::Invoke(SRDebugger.Services.IConsoleService)
extern void ConsoleUpdatedEventHandler_Invoke_m21A2E6A247BF71B6AC0DADA38A65DA1C6068EC8D (void);
// 0x000001A3 System.IAsyncResult SRDebugger.Services.ConsoleUpdatedEventHandler::BeginInvoke(SRDebugger.Services.IConsoleService,System.AsyncCallback,System.Object)
extern void ConsoleUpdatedEventHandler_BeginInvoke_m5161547483546EEEBFD201DD0A28EDAC5EB2E292 (void);
// 0x000001A4 System.Void SRDebugger.Services.ConsoleUpdatedEventHandler::EndInvoke(System.IAsyncResult)
extern void ConsoleUpdatedEventHandler_EndInvoke_m3642D984B8ACFF8940C776075CB78F3F0EBA74D4 (void);
// 0x000001A5 System.Int32 SRDebugger.Services.IConsoleService::get_ErrorCount()
// 0x000001A6 System.Int32 SRDebugger.Services.IConsoleService::get_WarningCount()
// 0x000001A7 System.Int32 SRDebugger.Services.IConsoleService::get_InfoCount()
// 0x000001A8 SRDebugger.IReadOnlyList`1<SRDebugger.Services.ConsoleEntry> SRDebugger.Services.IConsoleService::get_Entries()
// 0x000001A9 SRDebugger.IReadOnlyList`1<SRDebugger.Services.ConsoleEntry> SRDebugger.Services.IConsoleService::get_AllEntries()
// 0x000001AA System.Void SRDebugger.Services.IConsoleService::add_Updated(SRDebugger.Services.ConsoleUpdatedEventHandler)
// 0x000001AB System.Void SRDebugger.Services.IConsoleService::remove_Updated(SRDebugger.Services.ConsoleUpdatedEventHandler)
// 0x000001AC System.Void SRDebugger.Services.IConsoleService::add_Error(SRDebugger.Services.ConsoleUpdatedEventHandler)
// 0x000001AD System.Void SRDebugger.Services.IConsoleService::remove_Error(SRDebugger.Services.ConsoleUpdatedEventHandler)
// 0x000001AE System.Void SRDebugger.Services.IConsoleService::Clear()
// 0x000001AF System.Void SRDebugger.Services.ConsoleEntry::.ctor()
extern void ConsoleEntry__ctor_m65CE2BA141122C89A191EC1C581E229E864F379C (void);
// 0x000001B0 System.Void SRDebugger.Services.ConsoleEntry::.ctor(SRDebugger.Services.ConsoleEntry)
extern void ConsoleEntry__ctor_m47C36A7EA63A8ACC14000F5F5925D25088976FC8 (void);
// 0x000001B1 System.String SRDebugger.Services.ConsoleEntry::get_MessagePreview()
extern void ConsoleEntry_get_MessagePreview_mD74EF661A9344D0DDA16867C7C126E7C23EE1532 (void);
// 0x000001B2 System.String SRDebugger.Services.ConsoleEntry::get_StackTracePreview()
extern void ConsoleEntry_get_StackTracePreview_m7379982D4D0F16EFB21DED0AC49D9C63B54CB961 (void);
// 0x000001B3 System.Boolean SRDebugger.Services.ConsoleEntry::Matches(SRDebugger.Services.ConsoleEntry)
extern void ConsoleEntry_Matches_mBB49D0A1B29D25E80636B885CC2546FD1DC2DCCB (void);
// 0x000001B4 UnityEngine.Camera SRDebugger.Services.IDebugCameraService::get_Camera()
// 0x000001B5 System.Boolean SRDebugger.Services.IDebugPanelService::get_IsLoaded()
// 0x000001B6 System.Boolean SRDebugger.Services.IDebugPanelService::get_IsVisible()
// 0x000001B7 System.Void SRDebugger.Services.IDebugPanelService::set_IsVisible(System.Boolean)
// 0x000001B8 System.Nullable`1<SRDebugger.DefaultTabs> SRDebugger.Services.IDebugPanelService::get_ActiveTab()
// 0x000001B9 System.Void SRDebugger.Services.IDebugPanelService::add_VisibilityChanged(System.Action`2<SRDebugger.Services.IDebugPanelService,System.Boolean>)
// 0x000001BA System.Void SRDebugger.Services.IDebugPanelService::remove_VisibilityChanged(System.Action`2<SRDebugger.Services.IDebugPanelService,System.Boolean>)
// 0x000001BB System.Void SRDebugger.Services.IDebugPanelService::Unload()
// 0x000001BC System.Void SRDebugger.Services.IDebugPanelService::OpenTab(SRDebugger.DefaultTabs)
// 0x000001BD SRDebugger.Settings SRDebugger.Services.IDebugService::get_Settings()
// 0x000001BE System.Boolean SRDebugger.Services.IDebugService::get_IsDebugPanelVisible()
// 0x000001BF System.Boolean SRDebugger.Services.IDebugService::get_IsTriggerEnabled()
// 0x000001C0 System.Void SRDebugger.Services.IDebugService::set_IsTriggerEnabled(System.Boolean)
// 0x000001C1 SRDebugger.Services.IDockConsoleService SRDebugger.Services.IDebugService::get_DockConsole()
// 0x000001C2 System.Boolean SRDebugger.Services.IDebugService::get_IsProfilerDocked()
// 0x000001C3 System.Void SRDebugger.Services.IDebugService::set_IsProfilerDocked(System.Boolean)
// 0x000001C4 System.Void SRDebugger.Services.IDebugService::AddSystemInfo(SRDebugger.InfoEntry,System.String)
// 0x000001C5 System.Void SRDebugger.Services.IDebugService::ShowDebugPanel(System.Boolean)
// 0x000001C6 System.Void SRDebugger.Services.IDebugService::ShowDebugPanel(SRDebugger.DefaultTabs,System.Boolean)
// 0x000001C7 System.Void SRDebugger.Services.IDebugService::HideDebugPanel()
// 0x000001C8 System.Void SRDebugger.Services.IDebugService::DestroyDebugPanel()
// 0x000001C9 System.Void SRDebugger.Services.IDebugService::AddOptionContainer(System.Object)
// 0x000001CA System.Void SRDebugger.Services.IDebugService::RemoveOptionContainer(System.Object)
// 0x000001CB System.Void SRDebugger.Services.IDebugService::PinAllOptions(System.String)
// 0x000001CC System.Void SRDebugger.Services.IDebugService::UnpinAllOptions(System.String)
// 0x000001CD System.Void SRDebugger.Services.IDebugService::PinOption(System.String)
// 0x000001CE System.Void SRDebugger.Services.IDebugService::UnpinOption(System.String)
// 0x000001CF System.Void SRDebugger.Services.IDebugService::ClearPinnedOptions()
// 0x000001D0 System.Void SRDebugger.Services.IDebugService::ShowBugReportSheet(SRDebugger.ActionCompleteCallback,System.Boolean,System.String)
// 0x000001D1 System.Void SRDebugger.Services.IDebugService::add_PanelVisibilityChanged(SRDebugger.VisibilityChangedDelegate)
// 0x000001D2 System.Void SRDebugger.Services.IDebugService::remove_PanelVisibilityChanged(SRDebugger.VisibilityChangedDelegate)
// 0x000001D3 System.Void SRDebugger.Services.IDebugService::add_PinnedUiCanvasCreated(SRDebugger.PinnedUiCanvasCreated)
// 0x000001D4 System.Void SRDebugger.Services.IDebugService::remove_PinnedUiCanvasCreated(SRDebugger.PinnedUiCanvasCreated)
// 0x000001D5 UnityEngine.RectTransform SRDebugger.Services.IDebugService::EnableWorldSpaceMode()
// 0x000001D6 System.Boolean SRDebugger.Services.IDebugTriggerService::get_IsEnabled()
// 0x000001D7 System.Void SRDebugger.Services.IDebugTriggerService::set_IsEnabled(System.Boolean)
// 0x000001D8 SRDebugger.PinAlignment SRDebugger.Services.IDebugTriggerService::get_Position()
// 0x000001D9 System.Void SRDebugger.Services.IDebugTriggerService::set_Position(SRDebugger.PinAlignment)
// 0x000001DA System.Boolean SRDebugger.Services.IDockConsoleService::get_IsVisible()
// 0x000001DB System.Void SRDebugger.Services.IDockConsoleService::set_IsVisible(System.Boolean)
// 0x000001DC System.Boolean SRDebugger.Services.IDockConsoleService::get_IsExpanded()
// 0x000001DD System.Void SRDebugger.Services.IDockConsoleService::set_IsExpanded(System.Boolean)
// 0x000001DE SRDebugger.ConsoleAlignment SRDebugger.Services.IDockConsoleService::get_Alignment()
// 0x000001DF System.Void SRDebugger.Services.IDockConsoleService::set_Alignment(SRDebugger.ConsoleAlignment)
// 0x000001E0 System.Void SRDebugger.Services.IOptionsService::add_OptionsUpdated(System.EventHandler)
// 0x000001E1 System.Void SRDebugger.Services.IOptionsService::remove_OptionsUpdated(System.EventHandler)
// 0x000001E2 System.Void SRDebugger.Services.IOptionsService::add_OptionsValueUpdated(System.EventHandler`1<System.ComponentModel.PropertyChangedEventArgs>)
// 0x000001E3 System.Void SRDebugger.Services.IOptionsService::remove_OptionsValueUpdated(System.EventHandler`1<System.ComponentModel.PropertyChangedEventArgs>)
// 0x000001E4 System.Collections.Generic.ICollection`1<SRDebugger.Internal.OptionDefinition> SRDebugger.Services.IOptionsService::get_Options()
// 0x000001E5 System.Void SRDebugger.Services.IOptionsService::Scan(System.Object)
// 0x000001E6 System.Void SRDebugger.Services.IOptionsService::AddContainer(System.Object)
// 0x000001E7 System.Void SRDebugger.Services.IOptionsService::RemoveContainer(System.Object)
// 0x000001E8 System.Void SRDebugger.Services.PinEntryCompleteCallback::.ctor(System.Object,System.IntPtr)
extern void PinEntryCompleteCallback__ctor_m395D6DE59E8805524F139DDC6310A043216EC1C7 (void);
// 0x000001E9 System.Void SRDebugger.Services.PinEntryCompleteCallback::Invoke(System.Boolean)
extern void PinEntryCompleteCallback_Invoke_m2B523BCE54DA179E20B0691DCDE47B539CC67A0C (void);
// 0x000001EA System.IAsyncResult SRDebugger.Services.PinEntryCompleteCallback::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void PinEntryCompleteCallback_BeginInvoke_m5EB1453368ECE175EFCDA083A5645D69AC94F34C (void);
// 0x000001EB System.Void SRDebugger.Services.PinEntryCompleteCallback::EndInvoke(System.IAsyncResult)
extern void PinEntryCompleteCallback_EndInvoke_m33D166EF11E8D4E0ABB5FFE3303159C0596EB30F (void);
// 0x000001EC System.Boolean SRDebugger.Services.IPinEntryService::get_IsShowingKeypad()
// 0x000001ED System.Void SRDebugger.Services.IPinEntryService::ShowPinEntry(System.Collections.Generic.IList`1<System.Int32>,System.String,SRDebugger.Services.PinEntryCompleteCallback,System.Boolean)
// 0x000001EE System.Void SRDebugger.Services.IPinEntryService::ShowPinEntry(System.Collections.Generic.IList`1<System.Int32>,System.String,SRDebugger.Services.PinEntryCompleteCallback,System.Boolean,System.Boolean)
// 0x000001EF System.Void SRDebugger.Services.IPinnedUIService::add_OptionPinStateChanged(System.Action`2<SRDebugger.Internal.OptionDefinition,System.Boolean>)
// 0x000001F0 System.Void SRDebugger.Services.IPinnedUIService::remove_OptionPinStateChanged(System.Action`2<SRDebugger.Internal.OptionDefinition,System.Boolean>)
// 0x000001F1 System.Void SRDebugger.Services.IPinnedUIService::add_OptionsCanvasCreated(System.Action`1<UnityEngine.RectTransform>)
// 0x000001F2 System.Void SRDebugger.Services.IPinnedUIService::remove_OptionsCanvasCreated(System.Action`1<UnityEngine.RectTransform>)
// 0x000001F3 System.Boolean SRDebugger.Services.IPinnedUIService::get_IsProfilerPinned()
// 0x000001F4 System.Void SRDebugger.Services.IPinnedUIService::set_IsProfilerPinned(System.Boolean)
// 0x000001F5 System.Void SRDebugger.Services.IPinnedUIService::Pin(SRDebugger.Internal.OptionDefinition,System.Int32)
// 0x000001F6 System.Void SRDebugger.Services.IPinnedUIService::Unpin(SRDebugger.Internal.OptionDefinition)
// 0x000001F7 System.Void SRDebugger.Services.IPinnedUIService::UnpinAll()
// 0x000001F8 System.Boolean SRDebugger.Services.IPinnedUIService::HasPinned(SRDebugger.Internal.OptionDefinition)
// 0x000001F9 System.Type SRDebugger.Services.ProfilerServiceSelector::GetProfilerServiceType()
extern void ProfilerServiceSelector_GetProfilerServiceType_m64C3D7FCA9902885C302C1B828CABEDFAB95303E (void);
// 0x000001FA System.Single SRDebugger.Services.IProfilerService::get_AverageFrameTime()
// 0x000001FB System.Single SRDebugger.Services.IProfilerService::get_LastFrameTime()
// 0x000001FC SRDebugger.CircularBuffer`1<SRDebugger.Services.ProfilerFrame> SRDebugger.Services.IProfilerService::get_FrameBuffer()
// 0x000001FD System.Collections.Generic.IEnumerable`1<System.String> SRDebugger.Services.ISystemInformationService::GetCategories()
// 0x000001FE System.Collections.Generic.IList`1<SRDebugger.InfoEntry> SRDebugger.Services.ISystemInformationService::GetInfo(System.String)
// 0x000001FF System.Void SRDebugger.Services.ISystemInformationService::Add(SRDebugger.InfoEntry,System.String)
// 0x00000200 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> SRDebugger.Services.ISystemInformationService::CreateReport(System.Boolean)
// 0x00000201 System.Void SRDebugger.Services.Implementation.BugReportApiService::SendBugReport(SRDebugger.Services.BugReport,SRDebugger.Services.BugReportCompleteCallback,SRDebugger.Services.BugReportProgressCallback)
extern void BugReportApiService_SendBugReport_m23AC9C7337D698041ADE0915A724CBCE36512306 (void);
// 0x00000202 System.Void SRDebugger.Services.Implementation.BugReportApiService::Awake()
extern void BugReportApiService_Awake_m8EEFD758BE014504EB6BE9A1E2087048C7177BC2 (void);
// 0x00000203 System.Void SRDebugger.Services.Implementation.BugReportApiService::OnProgress(System.Single)
extern void BugReportApiService_OnProgress_mBA9BF2BE4DF55CCA902D8E6288442A9931BF6927 (void);
// 0x00000204 System.Void SRDebugger.Services.Implementation.BugReportApiService::OnComplete()
extern void BugReportApiService_OnComplete_m6B1F2146651145E9A3D43DCD447A7D18C404E7B7 (void);
// 0x00000205 System.Void SRDebugger.Services.Implementation.BugReportApiService::Update()
extern void BugReportApiService_Update_mF5ACDCAFD26D8E5344C638C52EA72BA488DA59B7 (void);
// 0x00000206 System.Void SRDebugger.Services.Implementation.BugReportApiService::.ctor()
extern void BugReportApiService__ctor_mFF9E2EE284EB0067398D1C97555BB8B1FB9538FC (void);
// 0x00000207 System.Boolean SRDebugger.Services.Implementation.BugReportPopoverService::get_IsShowingPopover()
extern void BugReportPopoverService_get_IsShowingPopover_mDAD558ECBA8283235FBD5B6ADC60876A924A425A (void);
// 0x00000208 System.Void SRDebugger.Services.Implementation.BugReportPopoverService::ShowBugReporter(SRDebugger.Services.BugReportCompleteCallback,System.Boolean,System.String)
extern void BugReportPopoverService_ShowBugReporter_m4814A24731F7CBB5F5FBFB7E5CF55BF3BAAC6BAB (void);
// 0x00000209 System.Collections.IEnumerator SRDebugger.Services.Implementation.BugReportPopoverService::OpenCo(System.Boolean,System.String)
extern void BugReportPopoverService_OpenCo_mF8E6026CB3904F9125CD1E33EC159B56A8B07D4E (void);
// 0x0000020A System.Void SRDebugger.Services.Implementation.BugReportPopoverService::SubmitComplete(System.Boolean,System.String)
extern void BugReportPopoverService_SubmitComplete_m02D43C03928E129D61003757C66E499D8C6D63F3 (void);
// 0x0000020B System.Void SRDebugger.Services.Implementation.BugReportPopoverService::CancelPressed()
extern void BugReportPopoverService_CancelPressed_mBA82E47FC9A8B0384DBE863172CD9D29D9140091 (void);
// 0x0000020C System.Void SRDebugger.Services.Implementation.BugReportPopoverService::OnComplete(System.Boolean,System.String,System.Boolean)
extern void BugReportPopoverService_OnComplete_m0D016FDA3C37B234AB1336CFFFFD0E571FBE0EB2 (void);
// 0x0000020D System.Void SRDebugger.Services.Implementation.BugReportPopoverService::TakingScreenshot()
extern void BugReportPopoverService_TakingScreenshot_m6042C6A0141F4782CC37BB202040813ADA2BA4FA (void);
// 0x0000020E System.Void SRDebugger.Services.Implementation.BugReportPopoverService::ScreenshotComplete()
extern void BugReportPopoverService_ScreenshotComplete_m203C3A80D2163386CE46DFC8E2944857454C49DD (void);
// 0x0000020F System.Void SRDebugger.Services.Implementation.BugReportPopoverService::Awake()
extern void BugReportPopoverService_Awake_mF239C64CF04F28BAC4669C0C40154231662B1CEC (void);
// 0x00000210 System.Void SRDebugger.Services.Implementation.BugReportPopoverService::Load()
extern void BugReportPopoverService_Load_mC3E27173706B3A72E13B2176920657EE313419B4 (void);
// 0x00000211 System.Void SRDebugger.Services.Implementation.BugReportPopoverService::.ctor()
extern void BugReportPopoverService__ctor_m0BBBFAAD3D7ABA6B68C202673E8E65AC04420C31 (void);
// 0x00000212 System.Void SRDebugger.Services.Implementation.DebugCameraServiceImpl::.ctor()
extern void DebugCameraServiceImpl__ctor_m5835CAFF14D37CF1E23C8E33621EE3CE50CEB1EA (void);
// 0x00000213 UnityEngine.Camera SRDebugger.Services.Implementation.DebugCameraServiceImpl::get_Camera()
extern void DebugCameraServiceImpl_get_Camera_m72FBBB87C9A676F021E89617BE967C8C5C4700CA (void);
// 0x00000214 System.Void SRDebugger.Services.Implementation.DebugPanelServiceImpl::add_VisibilityChanged(System.Action`2<SRDebugger.Services.IDebugPanelService,System.Boolean>)
extern void DebugPanelServiceImpl_add_VisibilityChanged_m439646FBE3B05FB02CA91A3CFAE10E5163D1FA74 (void);
// 0x00000215 System.Void SRDebugger.Services.Implementation.DebugPanelServiceImpl::remove_VisibilityChanged(System.Action`2<SRDebugger.Services.IDebugPanelService,System.Boolean>)
extern void DebugPanelServiceImpl_remove_VisibilityChanged_m8232AD3E67302226F04668726249ED7686DF9398 (void);
// 0x00000216 SRDebugger.UI.DebugPanelRoot SRDebugger.Services.Implementation.DebugPanelServiceImpl::get_RootObject()
extern void DebugPanelServiceImpl_get_RootObject_m36F59FFCEC66A108EEB8399FEF16CFF84E254741 (void);
// 0x00000217 System.Boolean SRDebugger.Services.Implementation.DebugPanelServiceImpl::get_IsLoaded()
extern void DebugPanelServiceImpl_get_IsLoaded_m6AE65A3DEB2CDF7AFAD1C504310DC0A9B4D7F448 (void);
// 0x00000218 System.Boolean SRDebugger.Services.Implementation.DebugPanelServiceImpl::get_IsVisible()
extern void DebugPanelServiceImpl_get_IsVisible_m5E8EB20A0E17AFEFE21A737C5F1DDECDADC3A3D5 (void);
// 0x00000219 System.Void SRDebugger.Services.Implementation.DebugPanelServiceImpl::set_IsVisible(System.Boolean)
extern void DebugPanelServiceImpl_set_IsVisible_m7ED37A4D4BA39CEDD1AD14EA91DBB532052B893D (void);
// 0x0000021A System.Nullable`1<SRDebugger.DefaultTabs> SRDebugger.Services.Implementation.DebugPanelServiceImpl::get_ActiveTab()
extern void DebugPanelServiceImpl_get_ActiveTab_mC431FB776EDC94B4BF61EF6920F6E37B34868675 (void);
// 0x0000021B System.Void SRDebugger.Services.Implementation.DebugPanelServiceImpl::OpenTab(SRDebugger.DefaultTabs)
extern void DebugPanelServiceImpl_OpenTab_m1BA64B2303679C9BE69DC966D24322D7EAFC6BB7 (void);
// 0x0000021C System.Void SRDebugger.Services.Implementation.DebugPanelServiceImpl::Unload()
extern void DebugPanelServiceImpl_Unload_m26AC6E0338C0938FAEA8187237E7BE4FB6E93028 (void);
// 0x0000021D System.Void SRDebugger.Services.Implementation.DebugPanelServiceImpl::Load()
extern void DebugPanelServiceImpl_Load_m3513C1D8F1A5E9D1CAFAA1DE3758BEE50C106C1A (void);
// 0x0000021E System.Void SRDebugger.Services.Implementation.DebugPanelServiceImpl::.ctor()
extern void DebugPanelServiceImpl__ctor_m29DCBE555E420134F0B5828084B993C6291DBDA3 (void);
// 0x0000021F System.Boolean SRDebugger.Services.Implementation.DebugTriggerImpl::get_IsEnabled()
extern void DebugTriggerImpl_get_IsEnabled_m2A7F07BFDDA5B03040EC59A0B37FF5FB99A2CC04 (void);
// 0x00000220 System.Void SRDebugger.Services.Implementation.DebugTriggerImpl::set_IsEnabled(System.Boolean)
extern void DebugTriggerImpl_set_IsEnabled_m1FD15DF186EE95C2A365905749B0B1E2B3F9268A (void);
// 0x00000221 SRDebugger.PinAlignment SRDebugger.Services.Implementation.DebugTriggerImpl::get_Position()
extern void DebugTriggerImpl_get_Position_m1FD13997FC8FB6448A2F3FA5ACE38F1E280C9B09 (void);
// 0x00000222 System.Void SRDebugger.Services.Implementation.DebugTriggerImpl::set_Position(SRDebugger.PinAlignment)
extern void DebugTriggerImpl_set_Position_mD437A087ADC3B751802CF7855C7C9298C631A0FC (void);
// 0x00000223 System.Void SRDebugger.Services.Implementation.DebugTriggerImpl::Awake()
extern void DebugTriggerImpl_Awake_mC92DD5797E7DB28C7E9F233464823D24DE314E41 (void);
// 0x00000224 System.Void SRDebugger.Services.Implementation.DebugTriggerImpl::OnEnable()
extern void DebugTriggerImpl_OnEnable_m8CF564318BF1C9B008637175558E1034AB23E18C (void);
// 0x00000225 System.Void SRDebugger.Services.Implementation.DebugTriggerImpl::OnDisable()
extern void DebugTriggerImpl_OnDisable_m5B616BC4B7412D19DA9E1F6417A8F23E343F7589 (void);
// 0x00000226 System.Void SRDebugger.Services.Implementation.DebugTriggerImpl::OnError(SRDebugger.Services.IConsoleService)
extern void DebugTriggerImpl_OnError_m5B9EC3BF8E1B2D12BE9785BECE99124D6762DDA5 (void);
// 0x00000227 System.Void SRDebugger.Services.Implementation.DebugTriggerImpl::CreateTrigger()
extern void DebugTriggerImpl_CreateTrigger_m0B9DCDD5D65992513F35BD79F3CF37B10DF5D081 (void);
// 0x00000228 System.Void SRDebugger.Services.Implementation.DebugTriggerImpl::OnDestroy()
extern void DebugTriggerImpl_OnDestroy_m5339585CCA073BC56866C26185213A253A091DE4 (void);
// 0x00000229 System.Void SRDebugger.Services.Implementation.DebugTriggerImpl::OnActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void DebugTriggerImpl_OnActiveSceneChanged_m076C5B7F8E6D1354C13FAE14A2A9CB03C0FBC263 (void);
// 0x0000022A System.Void SRDebugger.Services.Implementation.DebugTriggerImpl::OnTriggerButtonClick()
extern void DebugTriggerImpl_OnTriggerButtonClick_m7A169FEA4C5E36CB02802552D41D701D455493AE (void);
// 0x0000022B System.Void SRDebugger.Services.Implementation.DebugTriggerImpl::SetTriggerPosition(UnityEngine.RectTransform,SRDebugger.PinAlignment)
extern void DebugTriggerImpl_SetTriggerPosition_mACFFA8C372B6735F1BC450F5E2E2A07DFC532234 (void);
// 0x0000022C System.Void SRDebugger.Services.Implementation.DebugTriggerImpl::.ctor()
extern void DebugTriggerImpl__ctor_mFF3CE28EEB2FF6CB52C038D63BE48F4B27443CE5 (void);
// 0x0000022D System.Void SRDebugger.Services.Implementation.DockConsoleServiceImpl::.ctor()
extern void DockConsoleServiceImpl__ctor_mFBC87A533806289C577A45DCE47EF01D5FBA4B58 (void);
// 0x0000022E System.Boolean SRDebugger.Services.Implementation.DockConsoleServiceImpl::get_IsVisible()
extern void DockConsoleServiceImpl_get_IsVisible_mB113071D3E91EAAB1E00D9AADEEE15C52A41AD8F (void);
// 0x0000022F System.Void SRDebugger.Services.Implementation.DockConsoleServiceImpl::set_IsVisible(System.Boolean)
extern void DockConsoleServiceImpl_set_IsVisible_mC5EAF2CD1EB47E18E7154D79265FDAB77672A5FC (void);
// 0x00000230 System.Boolean SRDebugger.Services.Implementation.DockConsoleServiceImpl::get_IsExpanded()
extern void DockConsoleServiceImpl_get_IsExpanded_m2D1BABA5E030C0656BB8B17523A7A52EA92D054A (void);
// 0x00000231 System.Void SRDebugger.Services.Implementation.DockConsoleServiceImpl::set_IsExpanded(System.Boolean)
extern void DockConsoleServiceImpl_set_IsExpanded_m2252CFF687A411D3374146E8DC2144B9A1F831A6 (void);
// 0x00000232 SRDebugger.ConsoleAlignment SRDebugger.Services.Implementation.DockConsoleServiceImpl::get_Alignment()
extern void DockConsoleServiceImpl_get_Alignment_m52EBF1EEA525049376E86C97351EED6E556083C1 (void);
// 0x00000233 System.Void SRDebugger.Services.Implementation.DockConsoleServiceImpl::set_Alignment(SRDebugger.ConsoleAlignment)
extern void DockConsoleServiceImpl_set_Alignment_m429A1AE6950F9E676821953F6935ABECFCF7C401 (void);
// 0x00000234 System.Void SRDebugger.Services.Implementation.DockConsoleServiceImpl::Load()
extern void DockConsoleServiceImpl_Load_m9697071AA3701B849AD46AA73C79A67990A63B4F (void);
// 0x00000235 System.Void SRDebugger.Services.Implementation.DockConsoleServiceImpl::CheckTrigger()
extern void DockConsoleServiceImpl_CheckTrigger_mFC3DF9030E46292CA5C7191017B3158C53D5369D (void);
// 0x00000236 System.Void SRDebugger.Services.Implementation.KeyboardShortcutListenerService::Awake()
extern void KeyboardShortcutListenerService_Awake_m910D651D6330F27D179C090715B3E5957194D047 (void);
// 0x00000237 System.Void SRDebugger.Services.Implementation.KeyboardShortcutListenerService::ToggleTab(SRDebugger.DefaultTabs)
extern void KeyboardShortcutListenerService_ToggleTab_m32D44A5191F6822A95161FAC586811B13287F40A (void);
// 0x00000238 System.Void SRDebugger.Services.Implementation.KeyboardShortcutListenerService::ExecuteShortcut(SRDebugger.Settings/KeyboardShortcut)
extern void KeyboardShortcutListenerService_ExecuteShortcut_mCD3CDDF2DE1F83C67AB698CB458DFF914AD0E15B (void);
// 0x00000239 System.Void SRDebugger.Services.Implementation.KeyboardShortcutListenerService::Update()
extern void KeyboardShortcutListenerService_Update_m83957AE5F1A40D721FD1CDA2FAB7132109B9D61D (void);
// 0x0000023A System.Void SRDebugger.Services.Implementation.KeyboardShortcutListenerService::.ctor()
extern void KeyboardShortcutListenerService__ctor_mF9967E784C23860A5767CFD0B1FCC54F12ECBC4E (void);
// 0x0000023B System.Void SRDebugger.Services.Implementation.OptionsServiceImpl::add_OptionsUpdated(System.EventHandler)
extern void OptionsServiceImpl_add_OptionsUpdated_mF8310A7FB5776A3EC6E946A7A9ECD2D26B46268F (void);
// 0x0000023C System.Void SRDebugger.Services.Implementation.OptionsServiceImpl::remove_OptionsUpdated(System.EventHandler)
extern void OptionsServiceImpl_remove_OptionsUpdated_m4EFEA262FC3EB0390F411A43AE3C3102A20672E9 (void);
// 0x0000023D System.Void SRDebugger.Services.Implementation.OptionsServiceImpl::add_OptionsValueUpdated(System.EventHandler`1<System.ComponentModel.PropertyChangedEventArgs>)
extern void OptionsServiceImpl_add_OptionsValueUpdated_mCA5075018ED8ADDFAA262C651AD169BE74E6B198 (void);
// 0x0000023E System.Void SRDebugger.Services.Implementation.OptionsServiceImpl::remove_OptionsValueUpdated(System.EventHandler`1<System.ComponentModel.PropertyChangedEventArgs>)
extern void OptionsServiceImpl_remove_OptionsValueUpdated_m97394B0CA9BF542AF13B062FC27C88637DD6798B (void);
// 0x0000023F System.Collections.Generic.ICollection`1<SRDebugger.Internal.OptionDefinition> SRDebugger.Services.Implementation.OptionsServiceImpl::get_Options()
extern void OptionsServiceImpl_get_Options_mB6D67085309B055F8379FE8F2915F6ABF2642C55 (void);
// 0x00000240 System.Void SRDebugger.Services.Implementation.OptionsServiceImpl::.ctor()
extern void OptionsServiceImpl__ctor_m8D9A26BBF69016C43B3BEAE8FD00473C0747FAA2 (void);
// 0x00000241 System.Void SRDebugger.Services.Implementation.OptionsServiceImpl::Scan(System.Object)
extern void OptionsServiceImpl_Scan_m2F88990D5A363E15D351EE5FAF7080591B135BED (void);
// 0x00000242 System.Void SRDebugger.Services.Implementation.OptionsServiceImpl::AddContainer(System.Object)
extern void OptionsServiceImpl_AddContainer_m51CC9D71DD05B15BE30AE2641B75DC0CECB37C87 (void);
// 0x00000243 System.Void SRDebugger.Services.Implementation.OptionsServiceImpl::RemoveContainer(System.Object)
extern void OptionsServiceImpl_RemoveContainer_m0B3F03B1ADB67DB9D6BCD48794ED4FBB9F4D62D2 (void);
// 0x00000244 System.Void SRDebugger.Services.Implementation.OptionsServiceImpl::OnPropertyChanged(System.Object,System.ComponentModel.PropertyChangedEventArgs)
extern void OptionsServiceImpl_OnPropertyChanged_m4421A2DF6C8F7100CFED6833A2BB5D665FCF462C (void);
// 0x00000245 System.Void SRDebugger.Services.Implementation.OptionsServiceImpl::OnOptionsUpdated()
extern void OptionsServiceImpl_OnOptionsUpdated_m0A7B8045451396EA8DB41183C98428FE1D522CCF (void);
// 0x00000246 System.Boolean SRDebugger.Services.Implementation.PinEntryServiceImpl::get_IsShowingKeypad()
extern void PinEntryServiceImpl_get_IsShowingKeypad_m2406035A3FCE684FB5C6172AF1C890E3FCDC72C8 (void);
// 0x00000247 System.Void SRDebugger.Services.Implementation.PinEntryServiceImpl::ShowPinEntry(System.Collections.Generic.IList`1<System.Int32>,System.String,SRDebugger.Services.PinEntryCompleteCallback,System.Boolean)
extern void PinEntryServiceImpl_ShowPinEntry_m9EB3C180BED2645F3F922DE69159670ACDEB9CE4 (void);
// 0x00000248 System.Void SRDebugger.Services.Implementation.PinEntryServiceImpl::ShowPinEntry(System.Collections.Generic.IList`1<System.Int32>,System.String,SRDebugger.Services.PinEntryCompleteCallback,System.Boolean,System.Boolean)
extern void PinEntryServiceImpl_ShowPinEntry_m593D54AFE157FD43443E1D5B0C8E75B1B50CADCD (void);
// 0x00000249 System.Void SRDebugger.Services.Implementation.PinEntryServiceImpl::Awake()
extern void PinEntryServiceImpl_Awake_m3B40F39CBDEB1980ED17C6173F4B34FCB2414E7A (void);
// 0x0000024A System.Void SRDebugger.Services.Implementation.PinEntryServiceImpl::Load()
extern void PinEntryServiceImpl_Load_m83E18B81FB46F8F205EC84A9786DAE276ECB8030 (void);
// 0x0000024B System.Void SRDebugger.Services.Implementation.PinEntryServiceImpl::PinControlOnComplete(System.Collections.Generic.IList`1<System.Int32>,System.Boolean)
extern void PinEntryServiceImpl_PinControlOnComplete_m70BDD73335A72EC6C283B9BB4D356BA5A820D925 (void);
// 0x0000024C System.Void SRDebugger.Services.Implementation.PinEntryServiceImpl::VerifyPin(System.Collections.Generic.IList`1<System.Int32>)
extern void PinEntryServiceImpl_VerifyPin_m806C55B2C9BC28DF4A80AE1A5C4B3F8F8C29D853 (void);
// 0x0000024D System.Void SRDebugger.Services.Implementation.PinEntryServiceImpl::.ctor()
extern void PinEntryServiceImpl__ctor_mDB9F8A158643A64AD436E27D172D4C1D2E28B219 (void);
// 0x0000024E SRDebugger.UI.Other.DockConsoleController SRDebugger.Services.Implementation.PinnedUIServiceImpl::get_DockConsoleController()
extern void PinnedUIServiceImpl_get_DockConsoleController_mD94B4FC45BFE7EDAE0B20A4973324C5B41D7DD7E (void);
// 0x0000024F System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::add_OptionPinStateChanged(System.Action`2<SRDebugger.Internal.OptionDefinition,System.Boolean>)
extern void PinnedUIServiceImpl_add_OptionPinStateChanged_mA26BDCA3583ED03E9424F13D5374C916E4B6BF11 (void);
// 0x00000250 System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::remove_OptionPinStateChanged(System.Action`2<SRDebugger.Internal.OptionDefinition,System.Boolean>)
extern void PinnedUIServiceImpl_remove_OptionPinStateChanged_mA0DDED8C0E5F45D69A8577D06193A1A1FB66932A (void);
// 0x00000251 System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::add_OptionsCanvasCreated(System.Action`1<UnityEngine.RectTransform>)
extern void PinnedUIServiceImpl_add_OptionsCanvasCreated_m987520D1FD78ECD7286022A627BA8B78B4223B01 (void);
// 0x00000252 System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::remove_OptionsCanvasCreated(System.Action`1<UnityEngine.RectTransform>)
extern void PinnedUIServiceImpl_remove_OptionsCanvasCreated_m0F962AEF9CEE18AE07DD16AEBC623F4C6B89D71E (void);
// 0x00000253 System.Boolean SRDebugger.Services.Implementation.PinnedUIServiceImpl::get_IsProfilerPinned()
extern void PinnedUIServiceImpl_get_IsProfilerPinned_m3C05A77E0CF40841438006E4F2BBB10F8F48093F (void);
// 0x00000254 System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::set_IsProfilerPinned(System.Boolean)
extern void PinnedUIServiceImpl_set_IsProfilerPinned_m6AD8F2C77FDE7399DA257F74330A8D85D59DC303 (void);
// 0x00000255 System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::Pin(SRDebugger.Internal.OptionDefinition,System.Int32)
extern void PinnedUIServiceImpl_Pin_mF2ACAFCFE5C603A8EA0286E8D46F09231D827A5C (void);
// 0x00000256 System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::Unpin(SRDebugger.Internal.OptionDefinition)
extern void PinnedUIServiceImpl_Unpin_m6B9136B948378E8D36FE95CFF3EC5B7A066C9F05 (void);
// 0x00000257 System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::OnPinnedStateChanged(SRDebugger.Internal.OptionDefinition,System.Boolean)
extern void PinnedUIServiceImpl_OnPinnedStateChanged_m8C49B7CDEF0D3680CE47A79ED6D5040D671AA23E (void);
// 0x00000258 System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::UnpinAll()
extern void PinnedUIServiceImpl_UnpinAll_m261A02E05A62BBD7980F68FAB592A2FFF4DB3DB8 (void);
// 0x00000259 System.Boolean SRDebugger.Services.Implementation.PinnedUIServiceImpl::HasPinned(SRDebugger.Internal.OptionDefinition)
extern void PinnedUIServiceImpl_HasPinned_mF67FE6968B7012B7754FA2512ACB48C5BF743BC8 (void);
// 0x0000025A System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::Awake()
extern void PinnedUIServiceImpl_Awake_m89EB23C06B5E668BF9177C26AA82D8EB818D2F75 (void);
// 0x0000025B System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::Load()
extern void PinnedUIServiceImpl_Load_m21FCD4CB4C742B7F74780EB13CC3050B45937FA0 (void);
// 0x0000025C System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::UpdateAnchors()
extern void PinnedUIServiceImpl_UpdateAnchors_m6C3A7A8F254EDE22771D17C56EF523149A518D0D (void);
// 0x0000025D System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::Update()
extern void PinnedUIServiceImpl_Update_m693F45EF8304E2ABD39EEBF4C905C432AEBBDA1E (void);
// 0x0000025E System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::OnOptionsUpdated(System.Object,System.EventArgs)
extern void PinnedUIServiceImpl_OnOptionsUpdated_m6C130C3330946988F3A559C4377727AA9A2D174C (void);
// 0x0000025F System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::OptionsOnPropertyChanged(System.Object,System.ComponentModel.PropertyChangedEventArgs)
extern void PinnedUIServiceImpl_OptionsOnPropertyChanged_m8CCE2ED9D7E9958C43C4D0A1AC959490F7FE5C5B (void);
// 0x00000260 System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::OnDebugPanelVisibilityChanged(System.Boolean)
extern void PinnedUIServiceImpl_OnDebugPanelVisibilityChanged_m22ADC62D30ED454F974D736D295D1F49FDFCDBE9 (void);
// 0x00000261 System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::Refresh()
extern void PinnedUIServiceImpl_Refresh_m14CFC4201FEB2D93D139D6F321F90AACEBA9ACD5 (void);
// 0x00000262 System.Void SRDebugger.Services.Implementation.PinnedUIServiceImpl::.ctor()
extern void PinnedUIServiceImpl__ctor_m0B5AE017F5BDC7B57CF3E247E5229B4399B38C72 (void);
// 0x00000263 SRDebugger.Services.IDockConsoleService SRDebugger.Services.Implementation.SRDebugService::get_DockConsole()
extern void SRDebugService_get_DockConsole_m67043FD09717BE5C0C1875E14495C5A1A3BF4AE7 (void);
// 0x00000264 System.Void SRDebugger.Services.Implementation.SRDebugService::add_PanelVisibilityChanged(SRDebugger.VisibilityChangedDelegate)
extern void SRDebugService_add_PanelVisibilityChanged_mD04E10B62CA9DC20EE5639BCBA82BF2254894AE1 (void);
// 0x00000265 System.Void SRDebugger.Services.Implementation.SRDebugService::remove_PanelVisibilityChanged(SRDebugger.VisibilityChangedDelegate)
extern void SRDebugService_remove_PanelVisibilityChanged_mF34CC516990D006DC530CD819A829AA5E2E1555D (void);
// 0x00000266 System.Void SRDebugger.Services.Implementation.SRDebugService::add_PinnedUiCanvasCreated(SRDebugger.PinnedUiCanvasCreated)
extern void SRDebugService_add_PinnedUiCanvasCreated_m58BA48906BE8A60D5A45560E42A46EB17D65EB03 (void);
// 0x00000267 System.Void SRDebugger.Services.Implementation.SRDebugService::remove_PinnedUiCanvasCreated(SRDebugger.PinnedUiCanvasCreated)
extern void SRDebugService_remove_PinnedUiCanvasCreated_mF49633C25B13A4282092E2AA7585B07327B4598C (void);
// 0x00000268 System.Void SRDebugger.Services.Implementation.SRDebugService::.ctor()
extern void SRDebugService__ctor_m8BD64D72F6306594A1DFE62800B1619D13AD4D2A (void);
// 0x00000269 SRDebugger.Settings SRDebugger.Services.Implementation.SRDebugService::get_Settings()
extern void SRDebugService_get_Settings_m1F9E06873331B7C225A47D8D4B8D99EB60984E0C (void);
// 0x0000026A System.Boolean SRDebugger.Services.Implementation.SRDebugService::get_IsDebugPanelVisible()
extern void SRDebugService_get_IsDebugPanelVisible_mD37A15633FE95EE7B6D098A7BBB70F6BC0B90A64 (void);
// 0x0000026B System.Boolean SRDebugger.Services.Implementation.SRDebugService::get_IsTriggerEnabled()
extern void SRDebugService_get_IsTriggerEnabled_m482DC371BE202425C2F7C116FC6A61ED602CDD59 (void);
// 0x0000026C System.Void SRDebugger.Services.Implementation.SRDebugService::set_IsTriggerEnabled(System.Boolean)
extern void SRDebugService_set_IsTriggerEnabled_m03A05870B80BFA49CA22C47F6BADEAEA9217A095 (void);
// 0x0000026D System.Boolean SRDebugger.Services.Implementation.SRDebugService::get_IsProfilerDocked()
extern void SRDebugService_get_IsProfilerDocked_mB787D6DF7A5CCA8B56841651CC0C30C99AC192B7 (void);
// 0x0000026E System.Void SRDebugger.Services.Implementation.SRDebugService::set_IsProfilerDocked(System.Boolean)
extern void SRDebugService_set_IsProfilerDocked_m0EE3AECF3BD2EE07D7FC9C3781C58D02ABC8BD8D (void);
// 0x0000026F System.Void SRDebugger.Services.Implementation.SRDebugService::AddSystemInfo(SRDebugger.InfoEntry,System.String)
extern void SRDebugService_AddSystemInfo_mDF76B2555B16ABAD293461EC5EFD3413F52C2ACD (void);
// 0x00000270 System.Void SRDebugger.Services.Implementation.SRDebugService::ShowDebugPanel(System.Boolean)
extern void SRDebugService_ShowDebugPanel_m2FE6EA38C29C353AE81A18F447594B5FA1147916 (void);
// 0x00000271 System.Void SRDebugger.Services.Implementation.SRDebugService::ShowDebugPanel(SRDebugger.DefaultTabs,System.Boolean)
extern void SRDebugService_ShowDebugPanel_m5547603E4994D428626298CE4D5059317483C562 (void);
// 0x00000272 System.Void SRDebugger.Services.Implementation.SRDebugService::HideDebugPanel()
extern void SRDebugService_HideDebugPanel_mBFDED4410FE80250B366FE2F571DEEB1A0C70304 (void);
// 0x00000273 System.Void SRDebugger.Services.Implementation.SRDebugService::DestroyDebugPanel()
extern void SRDebugService_DestroyDebugPanel_m055DBC56C2D96DE26213CF9930A67B8EF49357C1 (void);
// 0x00000274 System.Void SRDebugger.Services.Implementation.SRDebugService::AddOptionContainer(System.Object)
extern void SRDebugService_AddOptionContainer_m216AF82CA40199235AD0989341D1C20368E91252 (void);
// 0x00000275 System.Void SRDebugger.Services.Implementation.SRDebugService::RemoveOptionContainer(System.Object)
extern void SRDebugService_RemoveOptionContainer_m40F5907824D92D1075F6817F4D8A74FA213FD26A (void);
// 0x00000276 System.Void SRDebugger.Services.Implementation.SRDebugService::PinAllOptions(System.String)
extern void SRDebugService_PinAllOptions_m5B7454F34CD87C8A29358FD0E6949BFFDF8A6E70 (void);
// 0x00000277 System.Void SRDebugger.Services.Implementation.SRDebugService::UnpinAllOptions(System.String)
extern void SRDebugService_UnpinAllOptions_m969B4331B55DA1900F950CF75E7F4C217FE79179 (void);
// 0x00000278 System.Void SRDebugger.Services.Implementation.SRDebugService::PinOption(System.String)
extern void SRDebugService_PinOption_m022BE092793F572CF9923AEF5FF3979643A9A53A (void);
// 0x00000279 System.Void SRDebugger.Services.Implementation.SRDebugService::UnpinOption(System.String)
extern void SRDebugService_UnpinOption_m46331E4E586608E3617183083FFD902BD8F23CA8 (void);
// 0x0000027A System.Void SRDebugger.Services.Implementation.SRDebugService::ClearPinnedOptions()
extern void SRDebugService_ClearPinnedOptions_m496AA7FF5B224DC4687FC63062B14E4F20A17B25 (void);
// 0x0000027B System.Void SRDebugger.Services.Implementation.SRDebugService::ShowBugReportSheet(SRDebugger.ActionCompleteCallback,System.Boolean,System.String)
extern void SRDebugService_ShowBugReportSheet_m7A1EDF7B1439B1420E8BEFA86D4E6B1B5475B06C (void);
// 0x0000027C System.Void SRDebugger.Services.Implementation.SRDebugService::DebugPanelServiceOnVisibilityChanged(SRDebugger.Services.IDebugPanelService,System.Boolean)
extern void SRDebugService_DebugPanelServiceOnVisibilityChanged_mDFB5D0FA0BFA5B45FA1693E4F21439F17C58267A (void);
// 0x0000027D System.Void SRDebugger.Services.Implementation.SRDebugService::PromptEntryCode()
extern void SRDebugService_PromptEntryCode_m55DF0E63918CBF15AAB34FEE352631076D119893 (void);
// 0x0000027E UnityEngine.RectTransform SRDebugger.Services.Implementation.SRDebugService::EnableWorldSpaceMode()
extern void SRDebugService_EnableWorldSpaceMode_m1602A2E18C3661CE055C48F4F28685F13B56B3B6 (void);
// 0x0000027F System.Void SRDebugger.Services.Implementation.SRDebugService::<.ctor>b__17_0(UnityEngine.RectTransform)
extern void SRDebugService_U3C_ctorU3Eb__17_0_m3D17959F155D1C3816B09321E4568D6D4C029012 (void);
// 0x00000280 System.Void SRDebugger.Services.Implementation.SRDebugService::<PromptEntryCode>b__42_0(System.Boolean)
extern void SRDebugService_U3CPromptEntryCodeU3Eb__42_0_mBD7EBD5856B90F750517FA56B59BDB27946B34DF (void);
// 0x00000281 System.Void SRDebugger.Services.Implementation.StandardConsoleService::.ctor()
extern void StandardConsoleService__ctor_m530287CC47638982F26349C993F132E7C4E44F02 (void);
// 0x00000282 System.Int32 SRDebugger.Services.Implementation.StandardConsoleService::get_ErrorCount()
extern void StandardConsoleService_get_ErrorCount_m686A6F50DF2B31AFBF7AFDAD4FF426D0E4448D03 (void);
// 0x00000283 System.Void SRDebugger.Services.Implementation.StandardConsoleService::set_ErrorCount(System.Int32)
extern void StandardConsoleService_set_ErrorCount_m607C784CEC0D4A499772FE0BE8D187AA7D1A71A8 (void);
// 0x00000284 System.Int32 SRDebugger.Services.Implementation.StandardConsoleService::get_WarningCount()
extern void StandardConsoleService_get_WarningCount_mD54BD80FE59CD6EA189245E5B9AAF41AEE1FB64C (void);
// 0x00000285 System.Void SRDebugger.Services.Implementation.StandardConsoleService::set_WarningCount(System.Int32)
extern void StandardConsoleService_set_WarningCount_mCB4D790A0F613A86EF4518BA6BF5B73140F86AE7 (void);
// 0x00000286 System.Int32 SRDebugger.Services.Implementation.StandardConsoleService::get_InfoCount()
extern void StandardConsoleService_get_InfoCount_m246BFCFE5A29277CA9CF45658EAF77999B76444D (void);
// 0x00000287 System.Void SRDebugger.Services.Implementation.StandardConsoleService::set_InfoCount(System.Int32)
extern void StandardConsoleService_set_InfoCount_mE79F4A35D40F1385940787F19D68FE20404F986B (void);
// 0x00000288 System.Void SRDebugger.Services.Implementation.StandardConsoleService::add_Updated(SRDebugger.Services.ConsoleUpdatedEventHandler)
extern void StandardConsoleService_add_Updated_m17690F25430063605BA953874310EE8BEB57A9DC (void);
// 0x00000289 System.Void SRDebugger.Services.Implementation.StandardConsoleService::remove_Updated(SRDebugger.Services.ConsoleUpdatedEventHandler)
extern void StandardConsoleService_remove_Updated_m078FABE27828C8460B88A8C0E61F71E2BEDEF227 (void);
// 0x0000028A System.Void SRDebugger.Services.Implementation.StandardConsoleService::add_Error(SRDebugger.Services.ConsoleUpdatedEventHandler)
extern void StandardConsoleService_add_Error_m2985359698A913F2EB741A314981671086FAD89F (void);
// 0x0000028B System.Void SRDebugger.Services.Implementation.StandardConsoleService::remove_Error(SRDebugger.Services.ConsoleUpdatedEventHandler)
extern void StandardConsoleService_remove_Error_mAA3501165A27586280300D1A7DFEEEFCC2A43D77 (void);
// 0x0000028C SRDebugger.IReadOnlyList`1<SRDebugger.Services.ConsoleEntry> SRDebugger.Services.Implementation.StandardConsoleService::get_Entries()
extern void StandardConsoleService_get_Entries_mCCE676FAA6ED3C266A99E47D7E9F3C5976601247 (void);
// 0x0000028D SRDebugger.IReadOnlyList`1<SRDebugger.Services.ConsoleEntry> SRDebugger.Services.Implementation.StandardConsoleService::get_AllEntries()
extern void StandardConsoleService_get_AllEntries_m6A607736C9C64094416EE86520BE8DE7BC4D672C (void);
// 0x0000028E System.Void SRDebugger.Services.Implementation.StandardConsoleService::Clear()
extern void StandardConsoleService_Clear_mE6F1881331FFAFC542CF7D7D3D62E76F5F4A8A13 (void);
// 0x0000028F System.Void SRDebugger.Services.Implementation.StandardConsoleService::OnEntryAdded(SRDebugger.Services.ConsoleEntry)
extern void StandardConsoleService_OnEntryAdded_mC2A0A5D4539F7259BEB6464AEF9E8679D21E9319 (void);
// 0x00000290 System.Void SRDebugger.Services.Implementation.StandardConsoleService::OnEntryDuplicated(SRDebugger.Services.ConsoleEntry)
extern void StandardConsoleService_OnEntryDuplicated_mDDD73EF2613FE414A58D4CC8C5A561CD2E83C64D (void);
// 0x00000291 System.Void SRDebugger.Services.Implementation.StandardConsoleService::OnUpdated()
extern void StandardConsoleService_OnUpdated_mF42F9BF4E7F04B81F4D993B4F75774458AAAF9F4 (void);
// 0x00000292 System.Void SRDebugger.Services.Implementation.StandardConsoleService::UnityLogCallback(System.String,System.String,UnityEngine.LogType)
extern void StandardConsoleService_UnityLogCallback_mC3027AF4D31FF0CE7CD3DC9885AB3471F2D793A4 (void);
// 0x00000293 System.Void SRDebugger.Services.Implementation.StandardConsoleService::AdjustCounter(UnityEngine.LogType,System.Int32)
extern void StandardConsoleService_AdjustCounter_mC6160E771BB18E1B38556F7A5FEFD00D22AB5121 (void);
// 0x00000294 System.Void SRDebugger.Services.Implementation.StandardSystemInformationService::.ctor()
extern void StandardSystemInformationService__ctor_m64000316953C0B0F1853E5047E7C25E00DD2574F (void);
// 0x00000295 System.Collections.Generic.IEnumerable`1<System.String> SRDebugger.Services.Implementation.StandardSystemInformationService::GetCategories()
extern void StandardSystemInformationService_GetCategories_m0634CFDAEF61B3BCB1CA8515FA6F3B7BB2AE28A5 (void);
// 0x00000296 System.Collections.Generic.IList`1<SRDebugger.InfoEntry> SRDebugger.Services.Implementation.StandardSystemInformationService::GetInfo(System.String)
extern void StandardSystemInformationService_GetInfo_mEB00B72DF8ED4A3EDCDB20C02A48E9AD9EDFB63A (void);
// 0x00000297 System.Void SRDebugger.Services.Implementation.StandardSystemInformationService::Add(SRDebugger.InfoEntry,System.String)
extern void StandardSystemInformationService_Add_m271AA79E87B565E1BD7098B61906F5724FCD31DE (void);
// 0x00000298 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> SRDebugger.Services.Implementation.StandardSystemInformationService::CreateReport(System.Boolean)
extern void StandardSystemInformationService_CreateReport_m31DBC644F19392010C35B9843A094448270F9065 (void);
// 0x00000299 System.Void SRDebugger.Services.Implementation.StandardSystemInformationService::CreateDefaultSet()
extern void StandardSystemInformationService_CreateDefaultSet_mF3E379DECFE4F47B92CCC12B587A5DC16E075313 (void);
// 0x0000029A System.String SRDebugger.Services.Implementation.StandardSystemInformationService::GetCloudManifestPrettyName(System.String)
extern void StandardSystemInformationService_GetCloudManifestPrettyName_m7DF8EE9627B4A7AAD22978284AF5F17E44ED026B (void);
// 0x0000029B System.Diagnostics.Stopwatch SRDebugger.Profiler.ProfilerCameraListener::get_Stopwatch()
extern void ProfilerCameraListener_get_Stopwatch_m0FD664B474CA99FB8DA67ED1EC6D532D0A9E1C86 (void);
// 0x0000029C UnityEngine.Camera SRDebugger.Profiler.ProfilerCameraListener::get_Camera()
extern void ProfilerCameraListener_get_Camera_mFA69193B2E9AA14E4ADCF32AEAA089EF31C8B845 (void);
// 0x0000029D System.Void SRDebugger.Profiler.ProfilerCameraListener::OnPreCull()
extern void ProfilerCameraListener_OnPreCull_mB5D80740377C2F135F5A7AF0A50CECB88243ABBD (void);
// 0x0000029E System.Void SRDebugger.Profiler.ProfilerCameraListener::OnPostRender()
extern void ProfilerCameraListener_OnPostRender_m6EC85139D78AB0C2C245BE9723E186827A777B69 (void);
// 0x0000029F System.Void SRDebugger.Profiler.ProfilerCameraListener::.ctor()
extern void ProfilerCameraListener__ctor_m6894835ED312BE27D999DFABD6B8426CDB2A08B8 (void);
// 0x000002A0 System.Void SRDebugger.Profiler.ProfilerLateUpdateListener::LateUpdate()
extern void ProfilerLateUpdateListener_LateUpdate_mF47D1579B8CCE4F05BBEC438BD959BA96C05303D (void);
// 0x000002A1 System.Void SRDebugger.Profiler.ProfilerLateUpdateListener::.ctor()
extern void ProfilerLateUpdateListener__ctor_mE9722A8D56B64E77EB0CA6B3B985271E3559A731 (void);
// 0x000002A2 System.Single SRDebugger.Profiler.ProfilerServiceImpl::get_AverageFrameTime()
extern void ProfilerServiceImpl_get_AverageFrameTime_mFD8B748BC6B288DFF94A367BA88DF17772939F0A (void);
// 0x000002A3 System.Void SRDebugger.Profiler.ProfilerServiceImpl::set_AverageFrameTime(System.Single)
extern void ProfilerServiceImpl_set_AverageFrameTime_m8B6A685560D468ABEEBCF01BB9E24D531E034948 (void);
// 0x000002A4 System.Single SRDebugger.Profiler.ProfilerServiceImpl::get_LastFrameTime()
extern void ProfilerServiceImpl_get_LastFrameTime_m8D25FAC77621FD5905E623928FBE4482DCCBDF5B (void);
// 0x000002A5 System.Void SRDebugger.Profiler.ProfilerServiceImpl::set_LastFrameTime(System.Single)
extern void ProfilerServiceImpl_set_LastFrameTime_mFE8CC70FA930ABAF52771A39DCCA456082B5F68E (void);
// 0x000002A6 SRDebugger.CircularBuffer`1<SRDebugger.Services.ProfilerFrame> SRDebugger.Profiler.ProfilerServiceImpl::get_FrameBuffer()
extern void ProfilerServiceImpl_get_FrameBuffer_mE767DD62B9DCC63EDB4E5D50FC373B221C2455F1 (void);
// 0x000002A7 System.Void SRDebugger.Profiler.ProfilerServiceImpl::Awake()
extern void ProfilerServiceImpl_Awake_mCCA11B55A9ED09C9CD2D92DAEA59B874630BB3D3 (void);
// 0x000002A8 System.Void SRDebugger.Profiler.ProfilerServiceImpl::Update()
extern void ProfilerServiceImpl_Update_mED7C102958C5B0753CAAA197D296A99EC51F574C (void);
// 0x000002A9 System.Void SRDebugger.Profiler.ProfilerServiceImpl::PushFrame(System.Double,System.Double,System.Double)
extern void ProfilerServiceImpl_PushFrame_m8509C4C17F917EB83FEF5F370F082680E032AF8B (void);
// 0x000002AA System.Void SRDebugger.Profiler.ProfilerServiceImpl::OnLateUpdate()
extern void ProfilerServiceImpl_OnLateUpdate_mC9AF1858BD89EB8CB3AF9065AD1585BA04EA870C (void);
// 0x000002AB System.Void SRDebugger.Profiler.ProfilerServiceImpl::OnCameraPreRender(UnityEngine.Camera)
extern void ProfilerServiceImpl_OnCameraPreRender_mC6E96895AA212856B0F0074F048A498905F89C95 (void);
// 0x000002AC System.Void SRDebugger.Profiler.ProfilerServiceImpl::OnCameraPostRender(UnityEngine.Camera)
extern void ProfilerServiceImpl_OnCameraPostRender_m2C590E73C9E11B8A8D66C2F13B287390CBDC8B84 (void);
// 0x000002AD System.Void SRDebugger.Profiler.ProfilerServiceImpl::EndFrame()
extern void ProfilerServiceImpl_EndFrame_mB98FD6094CC7D7EFB5F0BB3B9FE7B3A0CED9613C (void);
// 0x000002AE System.Void SRDebugger.Profiler.ProfilerServiceImpl::.ctor()
extern void ProfilerServiceImpl__ctor_m394337F148D5AECF78280B24837446A9CAA943F9 (void);
// 0x000002AF System.Single SRDebugger.Profiler.SRPProfilerService::get_AverageFrameTime()
extern void SRPProfilerService_get_AverageFrameTime_mE7F0E1803BCAB4A65644C20DEC45DDF1BA4C1010 (void);
// 0x000002B0 System.Void SRDebugger.Profiler.SRPProfilerService::set_AverageFrameTime(System.Single)
extern void SRPProfilerService_set_AverageFrameTime_m0C3659A7BC2CA1090361DD4369054E67BF9A9452 (void);
// 0x000002B1 System.Single SRDebugger.Profiler.SRPProfilerService::get_LastFrameTime()
extern void SRPProfilerService_get_LastFrameTime_m247CDEF60F76D06EF7D2821B35A82D139FE4F87B (void);
// 0x000002B2 System.Void SRDebugger.Profiler.SRPProfilerService::set_LastFrameTime(System.Single)
extern void SRPProfilerService_set_LastFrameTime_m8347FD675025EF9A2F1F5CBE7C43948F012625E5 (void);
// 0x000002B3 SRDebugger.CircularBuffer`1<SRDebugger.Services.ProfilerFrame> SRDebugger.Profiler.SRPProfilerService::get_FrameBuffer()
extern void SRPProfilerService_get_FrameBuffer_mC8F76C4A43644A4624E54783CF1B2A963427D334 (void);
// 0x000002B4 System.Void SRDebugger.Profiler.SRPProfilerService::Awake()
extern void SRPProfilerService_Awake_mC7654AAECC4A13486A46397BB0DF63861265358D (void);
// 0x000002B5 System.Void SRDebugger.Profiler.SRPProfilerService::Update()
extern void SRPProfilerService_Update_mACA9C61C1A44D0C1890EA8734FE3E3BA7FCD8E99 (void);
// 0x000002B6 System.Collections.IEnumerator SRDebugger.Profiler.SRPProfilerService::EndOfFrameCoroutine()
extern void SRPProfilerService_EndOfFrameCoroutine_mEC3EB4C3237E9F8C9A69A3ADCDFC8E351C0E317A (void);
// 0x000002B7 System.Void SRDebugger.Profiler.SRPProfilerService::PushFrame(System.Double,System.Double,System.Double)
extern void SRPProfilerService_PushFrame_m261ABCCA3BA419E0752A9DFDC7D5FBCDAB60FD38 (void);
// 0x000002B8 System.Void SRDebugger.Profiler.SRPProfilerService::OnLateUpdate()
extern void SRPProfilerService_OnLateUpdate_mFE85887B967F826739555315F98138DA456835AA (void);
// 0x000002B9 System.Void SRDebugger.Profiler.SRPProfilerService::RenderPipelineOnBeginFrameRendering(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera[])
extern void SRPProfilerService_RenderPipelineOnBeginFrameRendering_mC197E4C8E75BD70642282C1D3C3FD29A3CCDF8DB (void);
// 0x000002BA System.Void SRDebugger.Profiler.SRPProfilerService::EndFrame()
extern void SRPProfilerService_EndFrame_mF87BC462987AD000FE0EB7C24A17D16BFA2FD41A (void);
// 0x000002BB System.Void SRDebugger.Profiler.SRPProfilerService::.ctor()
extern void SRPProfilerService__ctor_m30B02CEE722710106E4430EAAC9D5D7C443034FA (void);
// 0x000002BC System.String SRDebugger.Internal.SRDebugApiUtil::ParseErrorException(System.Net.WebException)
extern void SRDebugApiUtil_ParseErrorException_mCD44292F1916A3EF97B791528B82E67EBFDC3C80 (void);
// 0x000002BD System.String SRDebugger.Internal.SRDebugApiUtil::ParseErrorResponse(System.String,System.String)
extern void SRDebugApiUtil_ParseErrorResponse_mF3E768D16B0F728E268ECB794D0100E7412807ED (void);
// 0x000002BE System.Boolean SRDebugger.Internal.SRDebugApiUtil::ReadResponse(System.Net.HttpWebRequest,System.String&)
extern void SRDebugApiUtil_ReadResponse_m0332AD7A2AAC87AB90482649AEF55613DBA7C704 (void);
// 0x000002BF System.String SRDebugger.Internal.SRDebugApiUtil::ReadResponseStream(System.Net.WebResponse)
extern void SRDebugApiUtil_ReadResponseStream_mB84508E3F5E4AD2DCAA5CEA56F0FABF2224A6D3D (void);
// 0x000002C0 System.Void SRDebugger.Internal.BugReportApi::.ctor(SRDebugger.Services.BugReport,System.String)
extern void BugReportApi__ctor_m7E3DB20C34AA7A620AEEF8D9360522C92C2AD5D1 (void);
// 0x000002C1 System.Boolean SRDebugger.Internal.BugReportApi::get_IsComplete()
extern void BugReportApi_get_IsComplete_m239C9CD8871DD79CA112991A1D5B25211094757D (void);
// 0x000002C2 System.Void SRDebugger.Internal.BugReportApi::set_IsComplete(System.Boolean)
extern void BugReportApi_set_IsComplete_m944FEE2334E36A65FFAE2FCAE8C40C85E942724A (void);
// 0x000002C3 System.Boolean SRDebugger.Internal.BugReportApi::get_WasSuccessful()
extern void BugReportApi_get_WasSuccessful_m5BD57961583B30E42E474AFEFAE7ED3C48DBA9ED (void);
// 0x000002C4 System.Void SRDebugger.Internal.BugReportApi::set_WasSuccessful(System.Boolean)
extern void BugReportApi_set_WasSuccessful_m8B92B07ABB7CB78178B83D29EEC380DD8F21E0EC (void);
// 0x000002C5 System.String SRDebugger.Internal.BugReportApi::get_ErrorMessage()
extern void BugReportApi_get_ErrorMessage_m9334F4D3E6FC7C1C35EA5D2165D7703BF9DA432A (void);
// 0x000002C6 System.Void SRDebugger.Internal.BugReportApi::set_ErrorMessage(System.String)
extern void BugReportApi_set_ErrorMessage_m82E621F2A7042725B16B1E5FC463FC0B7D769F24 (void);
// 0x000002C7 System.Single SRDebugger.Internal.BugReportApi::get_Progress()
extern void BugReportApi_get_Progress_mAEC08E334E155C8478C1F72726094A286BEC9601 (void);
// 0x000002C8 System.Collections.IEnumerator SRDebugger.Internal.BugReportApi::Submit()
extern void BugReportApi_Submit_m0B663F1CE5E41841C1959E20229D15F804DAEA46 (void);
// 0x000002C9 System.Void SRDebugger.Internal.BugReportApi::SetCompletionState(System.Boolean)
extern void BugReportApi_SetCompletionState_mAFBDD1265FB718F61517969E95D1AA075F33A00D (void);
// 0x000002CA System.String SRDebugger.Internal.BugReportApi::BuildJsonRequest(SRDebugger.Services.BugReport)
extern void BugReportApi_BuildJsonRequest_m7D75087A6E530A14E96A8B5D00BF368209B66E31 (void);
// 0x000002CB System.Collections.Generic.IList`1<System.Collections.Generic.IList`1<System.String>> SRDebugger.Internal.BugReportApi::CreateConsoleDump()
extern void BugReportApi_CreateConsoleDump_m5A497BE1AC79B7ED5A23C8DF1CA36BC999C4CAA9 (void);
// 0x000002CC System.Collections.IEnumerator SRDebugger.Internal.BugReportScreenshotUtil::ScreenshotCaptureCo()
extern void BugReportScreenshotUtil_ScreenshotCaptureCo_m38453CD773E465E52A15516991BFFEAFF8DFE8DC (void);
// 0x000002CD System.Void SRDebugger.Internal.BugReportScreenshotUtil::.ctor()
extern void BugReportScreenshotUtil__ctor_mFC3DBD45259BCB5479026549D2137D0E97C0DC6C (void);
// 0x000002CE SRDebugger.UI.Controls.OptionsControlBase SRDebugger.Internal.OptionControlFactory::CreateControl(SRDebugger.Internal.OptionDefinition,System.String)
extern void OptionControlFactory_CreateControl_m93351447E169FC78CBEF0B3AEC8D03482425BA79 (void);
// 0x000002CF SRDebugger.UI.Controls.Data.ActionControl SRDebugger.Internal.OptionControlFactory::CreateActionControl(SRDebugger.Internal.OptionDefinition,System.String)
extern void OptionControlFactory_CreateActionControl_m7252BDA25BDB6822EE49C126CD012CB4B9E88DBD (void);
// 0x000002D0 SRDebugger.UI.Controls.DataBoundControl SRDebugger.Internal.OptionControlFactory::CreateDataControl(SRDebugger.Internal.OptionDefinition,System.String)
extern void OptionControlFactory_CreateDataControl_m73BEF30F4D040F2644E9B8567E17CE6335257CAD (void);
// 0x000002D1 System.Void SRDebugger.Internal.OptionDefinition::.ctor(System.String,System.String,System.Int32)
extern void OptionDefinition__ctor_mC7C03BAE345D2D082C98F8F603EF6DCD2575533E (void);
// 0x000002D2 System.Void SRDebugger.Internal.OptionDefinition::.ctor(System.String,System.String,System.Int32,SRF.Helpers.MethodReference)
extern void OptionDefinition__ctor_mC1524E4D94E76E7CBF534E6BAD197A763EF4415E (void);
// 0x000002D3 System.Void SRDebugger.Internal.OptionDefinition::.ctor(System.String,System.String,System.Int32,SRF.Helpers.PropertyReference)
extern void OptionDefinition__ctor_m32855307150D279A02BDE7C388D3E27BDFACBFC6 (void);
// 0x000002D4 System.String SRDebugger.Internal.OptionDefinition::get_Name()
extern void OptionDefinition_get_Name_m7AD2F87ECE626DFACE9496F3FBBBB5C66AFFCCC3 (void);
// 0x000002D5 System.Void SRDebugger.Internal.OptionDefinition::set_Name(System.String)
extern void OptionDefinition_set_Name_m55D7259C2C01368D6138CCFF7B9F05EB6CA0F1F1 (void);
// 0x000002D6 System.String SRDebugger.Internal.OptionDefinition::get_Category()
extern void OptionDefinition_get_Category_m197FC276709EE8FE62F425D5C7A304369B13A3CA (void);
// 0x000002D7 System.Void SRDebugger.Internal.OptionDefinition::set_Category(System.String)
extern void OptionDefinition_set_Category_mFCBA432A8A7475EF10629B00D80E148711DFA173 (void);
// 0x000002D8 System.Int32 SRDebugger.Internal.OptionDefinition::get_SortPriority()
extern void OptionDefinition_get_SortPriority_mD6F59479B34FD332BC20AC19D76F1D8E7764D728 (void);
// 0x000002D9 System.Void SRDebugger.Internal.OptionDefinition::set_SortPriority(System.Int32)
extern void OptionDefinition_set_SortPriority_m2B27795D63EF5C771A1257D93F840C6F1CEDD0AF (void);
// 0x000002DA SRF.Helpers.MethodReference SRDebugger.Internal.OptionDefinition::get_Method()
extern void OptionDefinition_get_Method_m1A3D5A8347F3949421CF5E8A5BF64CBA73373F89 (void);
// 0x000002DB System.Void SRDebugger.Internal.OptionDefinition::set_Method(SRF.Helpers.MethodReference)
extern void OptionDefinition_set_Method_m11F6D1545717DEEBE949097DEB61AFF71D082318 (void);
// 0x000002DC SRF.Helpers.PropertyReference SRDebugger.Internal.OptionDefinition::get_Property()
extern void OptionDefinition_get_Property_mE9F143EE8E54ED1B59E98E268E8A8D0B998583BD (void);
// 0x000002DD System.Void SRDebugger.Internal.OptionDefinition::set_Property(SRF.Helpers.PropertyReference)
extern void OptionDefinition_set_Property_mA9CDB4C0D2DD1FC06E53219A1FF4C9530E18F10C (void);
// 0x000002DE SRDebugger.Services.IConsoleService SRDebugger.Internal.Service::get_Console()
extern void Service_get_Console_mB19C044101C12C39163410A4387EAB83C52EB4C4 (void);
// 0x000002DF SRDebugger.Services.IDockConsoleService SRDebugger.Internal.Service::get_DockConsole()
extern void Service_get_DockConsole_mE3AFF49073DAF614B94B9069FA4EE2BAC8612CAD (void);
// 0x000002E0 SRDebugger.Services.IDebugPanelService SRDebugger.Internal.Service::get_Panel()
extern void Service_get_Panel_mE2A411B809619F046769C5C711C7E3AA14FEBFBC (void);
// 0x000002E1 SRDebugger.Services.IDebugTriggerService SRDebugger.Internal.Service::get_Trigger()
extern void Service_get_Trigger_m6940331FAA8443427EAF83F198FBC317EE623BF6 (void);
// 0x000002E2 SRDebugger.Services.IPinnedUIService SRDebugger.Internal.Service::get_PinnedUI()
extern void Service_get_PinnedUI_mF5FBC9824EDB4FB71518874C74F940DA59ECB19B (void);
// 0x000002E3 SRDebugger.Services.IDebugCameraService SRDebugger.Internal.Service::get_DebugCamera()
extern void Service_get_DebugCamera_mFF06ED8965E0711CCE8333EA3013461AADDA743C (void);
// 0x000002E4 SRDebugger.Services.IOptionsService SRDebugger.Internal.Service::get_Options()
extern void Service_get_Options_m4217D5DC4725D7EA7792F80D2E7F9CFA114E1F32 (void);
// 0x000002E5 System.Void SRDebugger.Internal.SRDebugStrings::.ctor()
extern void SRDebugStrings__ctor_mEBF0511978EF7A52BF21BDF9D377A46B7AEBA38B (void);
// 0x000002E6 System.Void SRDebugger.Internal.SRDebugStrings::.cctor()
extern void SRDebugStrings__cctor_m7878194242ADB8CC7D1555615D0601318A64BAEA (void);
// 0x000002E7 System.Boolean SRDebugger.Internal.SRDebuggerUtil::get_IsMobilePlatform()
extern void SRDebuggerUtil_get_IsMobilePlatform_m59D579F7172A6C80503A4C6EC492250810917FAB (void);
// 0x000002E8 System.Boolean SRDebugger.Internal.SRDebuggerUtil::EnsureEventSystemExists()
extern void SRDebuggerUtil_EnsureEventSystemExists_m4513F13DA57E73542A96631885ABA19F6A40AFE8 (void);
// 0x000002E9 System.Void SRDebugger.Internal.SRDebuggerUtil::CreateDefaultEventSystem()
extern void SRDebuggerUtil_CreateDefaultEventSystem_mF2C4F8FBA456827F81BD478B957137A48FC3669D (void);
// 0x000002EA System.Collections.Generic.ICollection`1<SRDebugger.Internal.OptionDefinition> SRDebugger.Internal.SRDebuggerUtil::ScanForOptions(System.Object)
extern void SRDebuggerUtil_ScanForOptions_mA54D2FDAE28B16CC3A9A6A43D6211616B3AF815C (void);
// 0x000002EB System.String SRDebugger.Internal.SRDebuggerUtil::GetNumberString(System.Int32,System.Int32,System.String)
extern void SRDebuggerUtil_GetNumberString_m9664949DD262E7EE1285ABA15C2DE4CDE8A87F14 (void);
// 0x000002EC System.Void SRDebugger.Internal.SRDebuggerUtil::ConfigureCanvas(UnityEngine.Canvas)
extern void SRDebuggerUtil_ConfigureCanvas_m5A2075167FF767D281D75C7AF13EAE0E07A3A42C (void);
// 0x000002ED System.Void SRDebugger.CircularBuffer`1/<GetEnumerator>d__18::.ctor(System.Int32)
// 0x000002EE System.Void SRDebugger.CircularBuffer`1/<GetEnumerator>d__18::System.IDisposable.Dispose()
// 0x000002EF System.Boolean SRDebugger.CircularBuffer`1/<GetEnumerator>d__18::MoveNext()
// 0x000002F0 T SRDebugger.CircularBuffer`1/<GetEnumerator>d__18::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000002F1 System.Void SRDebugger.CircularBuffer`1/<GetEnumerator>d__18::System.Collections.IEnumerator.Reset()
// 0x000002F2 System.Object SRDebugger.CircularBuffer`1/<GetEnumerator>d__18::System.Collections.IEnumerator.get_Current()
// 0x000002F3 System.Void SRDebugger.InfoEntry/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mEFC1F5BC444046D230C8D0DF60CF176E6A7ACD1C (void);
// 0x000002F4 System.Object SRDebugger.InfoEntry/<>c__DisplayClass12_0::<Create>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CCreateU3Eb__0_m3F7A54786AD5C080C1B47A667995ABFCED3766D1 (void);
// 0x000002F5 System.Void SRDebugger.Settings/KeyboardShortcut::.ctor()
extern void KeyboardShortcut__ctor_mF0395A215A10A44B9D4CD248B9D976C6AE741AB6 (void);
// 0x000002F6 System.Void SRDebugger.Settings/<>c::.cctor()
extern void U3CU3Ec__cctor_mAE49B2AC7DE99BCC5DBF12694D82B7417084B3DD (void);
// 0x000002F7 System.Void SRDebugger.Settings/<>c::.ctor()
extern void U3CU3Ec__ctor_m40AA1AC7E87A64EF869B11B9342D6FA4D009FDF7 (void);
// 0x000002F8 System.Boolean SRDebugger.Settings/<>c::<set_EntryCode>b__38_0(System.Int32)
extern void U3CU3Ec_U3Cset_EntryCodeU3Eb__38_0_m18E0E7599D99188371E00D61E5CD7BAED90F4E26 (void);
// 0x000002F9 SRDebugger.UI.Other.CategoryGroup SRDebugger.UI.Tabs.OptionsTabController/CategoryInstance::get_CategoryGroup()
extern void CategoryInstance_get_CategoryGroup_m617396D57ADA63D1E62A7D36F5BDF30869DDBF5A (void);
// 0x000002FA System.Void SRDebugger.UI.Tabs.OptionsTabController/CategoryInstance::set_CategoryGroup(SRDebugger.UI.Other.CategoryGroup)
extern void CategoryInstance_set_CategoryGroup_m45CFD343FAA9D6818999C43F3CE4888BE0CBD61D (void);
// 0x000002FB System.Void SRDebugger.UI.Tabs.OptionsTabController/CategoryInstance::.ctor(SRDebugger.UI.Other.CategoryGroup)
extern void CategoryInstance__ctor_mBBFD23909122498B12A84BE37928EFD4B514706B (void);
// 0x000002FC System.Void SRDebugger.UI.Tabs.OptionsTabController/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_mD9B0B8A0BF21A4D5A7A339B6B264B2E3FD2E50AD (void);
// 0x000002FD System.Void SRDebugger.UI.Tabs.OptionsTabController/<>c__DisplayClass30_0::<CreateCategory>b__1(System.Boolean)
extern void U3CU3Ec__DisplayClass30_0_U3CCreateCategoryU3Eb__1_mEC94D53BF187CB174E0924DC15EAD4BA23D53205 (void);
// 0x000002FE System.Void SRDebugger.UI.Tabs.OptionsTabController/<>c::.cctor()
extern void U3CU3Ec__cctor_m66400E3A3C8A177EFD4FC453119AFEF08F462D97 (void);
// 0x000002FF System.Void SRDebugger.UI.Tabs.OptionsTabController/<>c::.ctor()
extern void U3CU3Ec__ctor_mCEC5AD597573283C2B5BC0B07C79337BA300404B (void);
// 0x00000300 System.Int32 SRDebugger.UI.Tabs.OptionsTabController/<>c::<CreateCategory>b__30_0(SRDebugger.Internal.OptionDefinition,SRDebugger.Internal.OptionDefinition)
extern void U3CU3Ec_U3CCreateCategoryU3Eb__30_0_mB9258D008607638E5379AFD95A442154BAD2EADD (void);
// 0x00000301 System.Void SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>d__18::.ctor(System.Int32)
extern void U3CSubmitCoU3Ed__18__ctor_mF45B81252BAC44E4DF687C08BC97A2CC12325FDD (void);
// 0x00000302 System.Void SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>d__18::System.IDisposable.Dispose()
extern void U3CSubmitCoU3Ed__18_System_IDisposable_Dispose_m4BA0C12EC7C20064EC8909778631597684320483 (void);
// 0x00000303 System.Boolean SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>d__18::MoveNext()
extern void U3CSubmitCoU3Ed__18_MoveNext_mACBC2798B96CB86064E3BD8ED2D6A207269F8C08 (void);
// 0x00000304 System.Object SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSubmitCoU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7ACB779280A4258AE13AD75E74EFE3BEA81D4925 (void);
// 0x00000305 System.Void SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>d__18::System.Collections.IEnumerator.Reset()
extern void U3CSubmitCoU3Ed__18_System_Collections_IEnumerator_Reset_mA844CFAA67D2F18092B1D5D77C03DB50EC2BB3DB (void);
// 0x00000306 System.Object SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CSubmitCoU3Ed__18_System_Collections_IEnumerator_get_Current_m6ECD4043E7FEA98340CAFF99CC771AF4BE5B445E (void);
// 0x00000307 System.Void SRDebugger.UI.Other.SRTabController/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m91A79DC13DADEB5929902581F1A25F1D89B30334 (void);
// 0x00000308 System.Void SRDebugger.UI.Other.SRTabController/<>c__DisplayClass15_0::<AddTab>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CAddTabU3Eb__0_mF491332A356B43651576F7FEE1A0A3FBCA17EB10 (void);
// 0x00000309 System.Void SRDebugger.UI.Other.SRTabController/<>c::.cctor()
extern void U3CU3Ec__cctor_m6B161CAD8643D5749ECFDF1BB67EE33CC4935AEF (void);
// 0x0000030A System.Void SRDebugger.UI.Other.SRTabController/<>c::.ctor()
extern void U3CU3Ec__ctor_m821A6AC8CED4E03754E8FE78DC03D41841E8C98F (void);
// 0x0000030B System.Int32 SRDebugger.UI.Other.SRTabController/<>c::<SortTabs>b__17_0(SRDebugger.UI.Other.SRTab,SRDebugger.UI.Other.SRTab)
extern void U3CU3Ec_U3CSortTabsU3Eb__17_0_m86A2FCB5B43784E50F47B8DEC32E01F7091CE02C (void);
// 0x0000030C System.Void SRDebugger.UI.Controls.ConsoleLogControl/<ScrollToBottom>d__26::.ctor(System.Int32)
extern void U3CScrollToBottomU3Ed__26__ctor_mDDC76ACC285FD20F628A0031DF192DDA9F757B14 (void);
// 0x0000030D System.Void SRDebugger.UI.Controls.ConsoleLogControl/<ScrollToBottom>d__26::System.IDisposable.Dispose()
extern void U3CScrollToBottomU3Ed__26_System_IDisposable_Dispose_mE7BA618EAEA24669FA3515E3B31FE9AB454D3ABB (void);
// 0x0000030E System.Boolean SRDebugger.UI.Controls.ConsoleLogControl/<ScrollToBottom>d__26::MoveNext()
extern void U3CScrollToBottomU3Ed__26_MoveNext_m05277D9ED3B38CDDA9EFF8CA0B762FB778F34356 (void);
// 0x0000030F System.Object SRDebugger.UI.Controls.ConsoleLogControl/<ScrollToBottom>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScrollToBottomU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7531FA8FD37CF6A14371055FA2E62F4200B70675 (void);
// 0x00000310 System.Void SRDebugger.UI.Controls.ConsoleLogControl/<ScrollToBottom>d__26::System.Collections.IEnumerator.Reset()
extern void U3CScrollToBottomU3Ed__26_System_Collections_IEnumerator_Reset_m13735294241FB36868C90E7052A971C71B8315F3 (void);
// 0x00000311 System.Object SRDebugger.UI.Controls.ConsoleLogControl/<ScrollToBottom>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CScrollToBottomU3Ed__26_System_Collections_IEnumerator_get_Current_m2AB9BEB17195DBF1E7132E95D095D5DF5FA93F8E (void);
// 0x00000312 System.Void SRDebugger.UI.Controls.PinEntryControl/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_mBE7C7AA95BDB4B7260FDCBCDDDBDB8F4EC7BEF39 (void);
// 0x00000313 System.Void SRDebugger.UI.Controls.PinEntryControl/<>c__DisplayClass14_0::<Awake>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CAwakeU3Eb__0_m3B63C5C0195D9DD9745CE448B64486E90590EE50 (void);
// 0x00000314 System.Void SRDebugger.UI.Controls.ProfilerMemoryBlock/<CleanUp>d__8::.ctor(System.Int32)
extern void U3CCleanUpU3Ed__8__ctor_m09B6C0F80D2F1F8425779FE81DBA0A73E4CBF24E (void);
// 0x00000315 System.Void SRDebugger.UI.Controls.ProfilerMemoryBlock/<CleanUp>d__8::System.IDisposable.Dispose()
extern void U3CCleanUpU3Ed__8_System_IDisposable_Dispose_mC65EC5E736BEB7EE7B54BA925A29122D0C6ECEA3 (void);
// 0x00000316 System.Boolean SRDebugger.UI.Controls.ProfilerMemoryBlock/<CleanUp>d__8::MoveNext()
extern void U3CCleanUpU3Ed__8_MoveNext_mC3BBA058A823D5714AE6FC0BD54155D8FF653848 (void);
// 0x00000317 System.Object SRDebugger.UI.Controls.ProfilerMemoryBlock/<CleanUp>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCleanUpU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m36EA00BA737412310EEEB51E8C5DD716BD64B7BC (void);
// 0x00000318 System.Void SRDebugger.UI.Controls.ProfilerMemoryBlock/<CleanUp>d__8::System.Collections.IEnumerator.Reset()
extern void U3CCleanUpU3Ed__8_System_Collections_IEnumerator_Reset_mA0DA5EEB4CF6EB7EF7ECDCAA13161EFAAEAA6EF4 (void);
// 0x00000319 System.Object SRDebugger.UI.Controls.ProfilerMemoryBlock/<CleanUp>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CCleanUpU3Ed__8_System_Collections_IEnumerator_get_Current_m58E7B5F723ECF83BEE77702E7F2D826C3E4C24E9 (void);
// 0x0000031A System.Void SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>d__7::.ctor(System.Int32)
extern void U3COpenCoU3Ed__7__ctor_m51C816BEEF8634CD74B9F8960EE2B4ABCD06E4FD (void);
// 0x0000031B System.Void SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>d__7::System.IDisposable.Dispose()
extern void U3COpenCoU3Ed__7_System_IDisposable_Dispose_m3FCD595F400FC9580B795B941ECF449C26BEB1C7 (void);
// 0x0000031C System.Boolean SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>d__7::MoveNext()
extern void U3COpenCoU3Ed__7_MoveNext_mF14B246559A29D251EDBBCD9DAA187C52533FC6D (void);
// 0x0000031D System.Object SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COpenCoU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67D741D0A2A05E0F30A3771E1BBA6C1B2B2F54B9 (void);
// 0x0000031E System.Void SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>d__7::System.Collections.IEnumerator.Reset()
extern void U3COpenCoU3Ed__7_System_Collections_IEnumerator_Reset_m4B656BBC380C713A13B2CA27DD20CA075A1DF666 (void);
// 0x0000031F System.Object SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>d__7::System.Collections.IEnumerator.get_Current()
extern void U3COpenCoU3Ed__7_System_Collections_IEnumerator_get_Current_mCFE72A97927EE324AAE5563DC85691ED6EB23D48 (void);
// 0x00000320 System.Void SRDebugger.Services.Implementation.SRDebugService/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m8BFE434EF5E1B8C05B6D8769DC73B842E2640D87 (void);
// 0x00000321 System.Void SRDebugger.Services.Implementation.SRDebugService/<>c__DisplayClass40_0::<ShowBugReportSheet>b__0(System.Boolean,System.String)
extern void U3CU3Ec__DisplayClass40_0_U3CShowBugReportSheetU3Eb__0_m11D53F3B3C743D094EAAF5329A4537217000AB66 (void);
// 0x00000322 System.Void SRDebugger.Services.Implementation.StandardSystemInformationService/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m780DF290AFFDFA954688D1A6ABE1DA1B7D5AB789 (void);
// 0x00000323 System.Boolean SRDebugger.Services.Implementation.StandardSystemInformationService/<>c__DisplayClass4_0::<Add>b__0(SRDebugger.InfoEntry)
extern void U3CU3Ec__DisplayClass4_0_U3CAddU3Eb__0_m03489BF90553336C8E1FF206220316C67E29C0D4 (void);
// 0x00000324 System.Void SRDebugger.Services.Implementation.StandardSystemInformationService/<>c::.cctor()
extern void U3CU3Ec__cctor_m311E1BD44127673B053E34144B070FDB5DE87DDC (void);
// 0x00000325 System.Void SRDebugger.Services.Implementation.StandardSystemInformationService/<>c::.ctor()
extern void U3CU3Ec__ctor_mA66E0704BCA5699CC1A44A787E425F53E90D74CF (void);
// 0x00000326 System.Object SRDebugger.Services.Implementation.StandardSystemInformationService/<>c::<CreateDefaultSet>b__6_0()
extern void U3CU3Ec_U3CCreateDefaultSetU3Eb__6_0_mCC31459404788CCA60E270CADACEFE01CA35FB73 (void);
// 0x00000327 System.Object SRDebugger.Services.Implementation.StandardSystemInformationService/<>c::<CreateDefaultSet>b__6_1()
extern void U3CU3Ec_U3CCreateDefaultSetU3Eb__6_1_m26275796C79F6A0B76472D1653FF25A8D8B89FCA (void);
// 0x00000328 System.Object SRDebugger.Services.Implementation.StandardSystemInformationService/<>c::<CreateDefaultSet>b__6_2()
extern void U3CU3Ec_U3CCreateDefaultSetU3Eb__6_2_m90568DBB5580EBF208043C9EED38B5013AAF0B0B (void);
// 0x00000329 System.Object SRDebugger.Services.Implementation.StandardSystemInformationService/<>c::<CreateDefaultSet>b__6_3()
extern void U3CU3Ec_U3CCreateDefaultSetU3Eb__6_3_mC7D088CE326D7967B99FA6BEB028C03BEC01DFAA (void);
// 0x0000032A System.Object SRDebugger.Services.Implementation.StandardSystemInformationService/<>c::<CreateDefaultSet>b__6_4()
extern void U3CU3Ec_U3CCreateDefaultSetU3Eb__6_4_mF708BBFFB80B8C66C19B9822A9834084DFBF500B (void);
// 0x0000032B System.Object SRDebugger.Services.Implementation.StandardSystemInformationService/<>c::<CreateDefaultSet>b__6_5()
extern void U3CU3Ec_U3CCreateDefaultSetU3Eb__6_5_mFE074367C3F7A575692CA22607C475A6D6E410B6 (void);
// 0x0000032C System.Object SRDebugger.Services.Implementation.StandardSystemInformationService/<>c::<CreateDefaultSet>b__6_6()
extern void U3CU3Ec_U3CCreateDefaultSetU3Eb__6_6_m6C3533D1FDC2BFF85D7713365A5851D865768C0F (void);
// 0x0000032D System.Object SRDebugger.Services.Implementation.StandardSystemInformationService/<>c::<CreateDefaultSet>b__6_7()
extern void U3CU3Ec_U3CCreateDefaultSetU3Eb__6_7_mF4CEA030F60C48E4121C0270F5FD9D82FF51D803 (void);
// 0x0000032E System.Void SRDebugger.Profiler.SRPProfilerService/<EndOfFrameCoroutine>d__19::.ctor(System.Int32)
extern void U3CEndOfFrameCoroutineU3Ed__19__ctor_m9BD6420C8E25B789010F049ABB2E30135F2D269B (void);
// 0x0000032F System.Void SRDebugger.Profiler.SRPProfilerService/<EndOfFrameCoroutine>d__19::System.IDisposable.Dispose()
extern void U3CEndOfFrameCoroutineU3Ed__19_System_IDisposable_Dispose_m7C73699707AFF88E8713D9E0471E41266301A6D6 (void);
// 0x00000330 System.Boolean SRDebugger.Profiler.SRPProfilerService/<EndOfFrameCoroutine>d__19::MoveNext()
extern void U3CEndOfFrameCoroutineU3Ed__19_MoveNext_m7652F6599D23FBEC6953F847D7EC329AF513C94B (void);
// 0x00000331 System.Object SRDebugger.Profiler.SRPProfilerService/<EndOfFrameCoroutine>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEndOfFrameCoroutineU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE9623B72061554E2CECFD9DF05B4697844FD54C (void);
// 0x00000332 System.Void SRDebugger.Profiler.SRPProfilerService/<EndOfFrameCoroutine>d__19::System.Collections.IEnumerator.Reset()
extern void U3CEndOfFrameCoroutineU3Ed__19_System_Collections_IEnumerator_Reset_m6914F20DB646162870681825EA0E1F44A690E4EC (void);
// 0x00000333 System.Object SRDebugger.Profiler.SRPProfilerService/<EndOfFrameCoroutine>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CEndOfFrameCoroutineU3Ed__19_System_Collections_IEnumerator_get_Current_m739E8570CD152353EDF9812BFA76667C5A041AD7 (void);
// 0x00000334 System.Void SRDebugger.Internal.BugReportApi/<Submit>d__19::.ctor(System.Int32)
extern void U3CSubmitU3Ed__19__ctor_mC42CEC4BB53FF5D50E50DAE4543DC9A003A52F13 (void);
// 0x00000335 System.Void SRDebugger.Internal.BugReportApi/<Submit>d__19::System.IDisposable.Dispose()
extern void U3CSubmitU3Ed__19_System_IDisposable_Dispose_mD7B1971A5C9BA13268BFAC1DB0ED5D40E06EB990 (void);
// 0x00000336 System.Boolean SRDebugger.Internal.BugReportApi/<Submit>d__19::MoveNext()
extern void U3CSubmitU3Ed__19_MoveNext_mC9874B8C6419C852C81D6615A96641F6F0B1B9A9 (void);
// 0x00000337 System.Object SRDebugger.Internal.BugReportApi/<Submit>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSubmitU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF0028B3AFF6CC14B02089A366B4B1CAEF575505 (void);
// 0x00000338 System.Void SRDebugger.Internal.BugReportApi/<Submit>d__19::System.Collections.IEnumerator.Reset()
extern void U3CSubmitU3Ed__19_System_Collections_IEnumerator_Reset_m12CBF9045F7AEF4972CD431C135E81E7C472084F (void);
// 0x00000339 System.Object SRDebugger.Internal.BugReportApi/<Submit>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CSubmitU3Ed__19_System_Collections_IEnumerator_get_Current_m300F67CDAC762733A4D015C3D924F87D61CCE877 (void);
// 0x0000033A System.Void SRDebugger.Internal.BugReportScreenshotUtil/<ScreenshotCaptureCo>d__1::.ctor(System.Int32)
extern void U3CScreenshotCaptureCoU3Ed__1__ctor_m14FA22D6D7EE02781AC3442B52F4D781C227FA73 (void);
// 0x0000033B System.Void SRDebugger.Internal.BugReportScreenshotUtil/<ScreenshotCaptureCo>d__1::System.IDisposable.Dispose()
extern void U3CScreenshotCaptureCoU3Ed__1_System_IDisposable_Dispose_mB88E951136FC3F3DAE6D0A8DD71F24F8183A770B (void);
// 0x0000033C System.Boolean SRDebugger.Internal.BugReportScreenshotUtil/<ScreenshotCaptureCo>d__1::MoveNext()
extern void U3CScreenshotCaptureCoU3Ed__1_MoveNext_m6A4A60572581017257180CAF5C6C69A7739D0E86 (void);
// 0x0000033D System.Object SRDebugger.Internal.BugReportScreenshotUtil/<ScreenshotCaptureCo>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScreenshotCaptureCoU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m625C4252957889AD96C5A5D520EBC3EAC84CD30E (void);
// 0x0000033E System.Void SRDebugger.Internal.BugReportScreenshotUtil/<ScreenshotCaptureCo>d__1::System.Collections.IEnumerator.Reset()
extern void U3CScreenshotCaptureCoU3Ed__1_System_Collections_IEnumerator_Reset_m15965BC6A9CB646CE9FCCB068BAD1E2C47608343 (void);
// 0x0000033F System.Object SRDebugger.Internal.BugReportScreenshotUtil/<ScreenshotCaptureCo>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CScreenshotCaptureCoU3Ed__1_System_Collections_IEnumerator_get_Current_mF8DD6390E4D88778A77B5D8967BBA76C8F16F232 (void);
// 0x00000340 System.Void SRDebugger.Internal.OptionControlFactory/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mC70C83EB9067836394923435F9B04CDE6DBD983F (void);
// 0x00000341 System.Boolean SRDebugger.Internal.OptionControlFactory/<>c__DisplayClass4_0::<CreateDataControl>b__0(SRDebugger.UI.Controls.DataBoundControl)
extern void U3CU3Ec__DisplayClass4_0_U3CCreateDataControlU3Eb__0_m2E7643913F282E7189821D3A63A72CFD49DFA077 (void);
static Il2CppMethodPointer s_methodPointers[833] = 
{
	SRDebug_get_Instance_mB4BE4C8F06C044E84F01EE2DC9BDD58BF01A856C,
	SRDebug_Init_mFF260B51422A91E4B294F263D3CED83EDB96B2B7,
	AutoInitialize_OnLoadBeforeScene_mC51AA04A369A9792F095E9FC8CD93F412B05E621,
	AutoInitialize_OnLoad_m6AECD5EAC00B20AA997B4D8284BD5A2A950D5B15,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SRDebuggerInit_Awake_mCC6E97C2E710091F3EC5FB99217E9793DD6D81CC,
	SRDebuggerInit_Start_mEF7AE717F34EA9C4C5EA5F34825A0D1AF43CF7AA,
	SRDebuggerInit__ctor_m9E2C8919EC46D13C57183C1C24AA337A4496365A,
	NumberRangeAttribute__ctor_m64C812C1045CFABBBCB4E227BFC303BF02A742AD,
	IncrementAttribute__ctor_mC14B810ACB99606543BB86BF15CFC42E0B3CFEDF,
	SortAttribute__ctor_m1608F4290865C48EBCF13A307C817628C15633F0,
	VisibilityChangedDelegate__ctor_mA69F3D3B38FECE8C82F05C8497796A95ED1A92DE,
	VisibilityChangedDelegate_Invoke_m4801E3D870E54CBBC16B8CBCF0D7D55EECF34A2E,
	VisibilityChangedDelegate_BeginInvoke_m3BA556BD1A461DA21E6AE6A4376ECC6440C59574,
	VisibilityChangedDelegate_EndInvoke_m70486619CF861D2077628C808F31DA0DC070CB25,
	ActionCompleteCallback__ctor_m34F550FAB11F3B1B852D5269DE48D5581D47D52B,
	ActionCompleteCallback_Invoke_mEF74031661072303EA07193593FCA05822BA50AE,
	ActionCompleteCallback_BeginInvoke_mB0F8AA67B05180B67CACEF829C6F70503D5BE63A,
	ActionCompleteCallback_EndInvoke_m95971C4D321D57494DC3DFB9AF27D194EFE64C76,
	PinnedUiCanvasCreated__ctor_m4626EB6EC9A00724E7BF27C57A2DCBB1E221B22B,
	PinnedUiCanvasCreated_Invoke_m66B17536A9F3CD24F89792C5643F3721C2C13E87,
	PinnedUiCanvasCreated_BeginInvoke_m3D9D12ACD418BACC37499121B0E3FA1290E24679,
	PinnedUiCanvasCreated_EndInvoke_m4CFF9A00E0B343A828D6F505B1FA9EE06DB6D4B4,
	InfoEntry_get_Title_m9F3BB0A779E6F1494E23EA48AF739394019FF4C7,
	InfoEntry_set_Title_m8D0F6C0F903E76356DF6AC2403E72CC401003E72,
	InfoEntry_get_Value_m699AF99FDE2A3432F36BECB509343CDCD8E4F329,
	InfoEntry_get_IsPrivate_m39C089EA07B38E3DC26D1170CBBBECBBEAD460E7,
	InfoEntry_set_IsPrivate_m513C94B346AC14448347D609E3C9543B2A781FE3,
	InfoEntry_Create_mEC46B70788CC0B63D07AEB6EDB1CB12C5094864C,
	InfoEntry_Create_m213C0A76CA49CB85483DB24AB0DCE864A663815B,
	InfoEntry__ctor_mBF361724358903CC41A428BD805B22897F0C506E,
	Settings_get_Instance_mBBD8FC5D3FE62C9BE5FE820E80243B1F3353B2F7,
	Settings_GetDefaultKeyboardShortcuts_m651F8B3F0BBA3C72D0B5DA302AEA7CB69E541578,
	Settings_UpgradeKeyboardShortcuts_m128AF601AC5E2037AEEE794B0550157A27756F32,
	Settings_add_PropertyChanged_m89E574F01F8E03540F62F7436C58A5F6D7052332,
	Settings_remove_PropertyChanged_m4D13E1BED14D18BBE812E11C3BAB9C201DB11581,
	Settings_get_IsEnabled_m0130C2C81F49E2556607C9A76612D4A3D27BA772,
	Settings_get_DefaultTab_mA2479E147902813751A03DA3C61EB5B4A61BABF6,
	Settings_get_EnableTrigger_mD00CDA77B1CAD5A0330397A8A2CA4636291A8A20,
	Settings_get_TriggerBehaviour_m4A07CBBF85492454471DE070678FC9182936102A,
	Settings_get_ErrorNotification_m6C748B6A2E159158FF0FCC509BDB244E57CB5FB0,
	Settings_get_EnableKeyboardShortcuts_mBB1D85FF0FA91C7AB00F9ED7D59E91F41F94D072,
	Settings_get_KeyboardShortcuts_m63FF935C0BE58DC3D2F40015A73B1F41A5D3A68E,
	Settings_get_KeyboardEscapeClose_mEE4C32AE837E19D29B834B450BB9C438CB9CF43F,
	Settings_get_EnableBackgroundTransparency_mB881F84B6F8ACEB5866DF538E691AECEDE382C60,
	Settings_get_RequireCode_m7E20D504B607DEF4FADAED12C38928446C4D1D60,
	Settings_get_RequireEntryCodeEveryTime_m825387D4607DAED5C8396ED5D02A69FCD4A9688C,
	Settings_get_EntryCode_m84781F11E68EF1AA5C433D26693E0E2F5A7AA803,
	Settings_set_EntryCode_m20147BAFCE90AE9AD4723791BB74537686ABD974,
	Settings_get_UseDebugCamera_m89455CFC085692F72C69791920EC4ECB541DF869,
	Settings_get_DebugLayer_m6C0A588A6E50713384F02B38863A33FA2F58FEFA,
	Settings_get_DebugCameraDepth_m1D259780633312F5D19C323AF7580838D072C69E,
	Settings_get_CollapseDuplicateLogEntries_m59FE302FC278D78F8AF0224468677CCFC5FC5D9C,
	Settings_get_RichTextInConsole_m8052F17006959E918E9364AA07527F579CB1D542,
	Settings_get_ApiKey_m607DE8F5A70211CC99B49955461893D4E23EE52E,
	Settings_get_EnableBugReporter_mD06784CD6BF0CCA1E11B6253BD94E63E9ED607AA,
	Settings_get_DisabledTabs_mC86ACED4E3B46B276214B6495524A89381291477,
	Settings_get_TriggerPosition_mB753C19E94ADCC17DB967E4A0118CAA3BC9035B3,
	Settings_get_ProfilerAlignment_m5C8C7A75EC56ADB1BB646033813E5BE702CA7AE0,
	Settings_get_OptionsAlignment_mBE7B27F5E8E21F46590F43A7EA59A3F3BD5D7533,
	Settings_get_ConsoleAlignment_m09582118F8F9ABF69D2131E71EC2E0478F3AD56A,
	Settings_set_ConsoleAlignment_m855A1392810466841E53727A128AA18B47028415,
	Settings_get_MaximumConsoleEntries_m51294BAC16C304E1414986B66B690093587DF283,
	Settings_set_MaximumConsoleEntries_m765244602EECBEB6850E7839CF187B1F1ED9A4AB,
	Settings_get_EnableEventSystemGeneration_m69C6E1AB9F03C7CE7B0F9A8160F0E42CB122C242,
	Settings_set_EnableEventSystemGeneration_m9B65A9C4A5DB9D60FBF9EE17151E3AB43D45A271,
	Settings_get_AutomaticallyShowCursor_m2762D1936BF88C94BB9677F7697213B7228E773D,
	Settings_get_UIScale_m6F312D6CA8327B042DB4271750C1FEC6A06A9642,
	Settings_set_UIScale_mDE4282F0D0FEB42FD1583FDE443184B1C6EC8729,
	Settings_get_UnloadOnClose_m6FD10BC2BF1C97CC0483D1DD3CE726144691780F,
	Settings_OnPropertyChanged_m37A086F12EF0D080FBD3187BDE1171DC1A2BF4BF,
	Settings_GetOrCreateInstance_m07FFD416079B89806912A2CE82311454D7725969,
	Settings__ctor_m9C5FFE05A19B1829629B5E221DE4940B19F86F39,
	DebuggerTabController_get_ActiveTab_mD508ED33E21785C80555305C59464C11BC7A1DD8,
	DebuggerTabController_Start_mCAD4447C9DB239575E8910345B4CCDA90083D730,
	DebuggerTabController_OpenTab_m535CF3A62C1BE32A7EA12BC5DD0663A17147CC40,
	DebuggerTabController_ShowAboutTab_mD240BE96B960A4FAEA56194C51FD457E5C3F4627,
	DebuggerTabController__ctor_mEC7EF921CC357E6A8B003DF407AEDD5FC7316B03,
	ProfilerFPSLabel_Update_m97CC44D272BDF99176E18FB7A69AD347DE2AD8E1,
	ProfilerFPSLabel_Refresh_m721FE1F6543BCA93C2FC35218B215096E3244241,
	ProfilerFPSLabel__ctor_mB9B7C8BE16BECF3BEEAE904AA920D98225592775,
	DebugPanelRoot_Close_mD951522C698798FF93B4F515EA5C6DC29811635D,
	DebugPanelRoot_CloseAndDestroy_m8C2A28986566A8D685A7F95F55879488A2E979E9,
	DebugPanelRoot__ctor_mF9C509A3FE7CD0D2EC553C386488EA8CD89C9B29,
	MobileMenuController_get_PeekAmount_mE9018743F58F21709B68DDE2DB384375B403C95A,
	MobileMenuController_get_MaxMenuWidth_m6E607731DB85073584412ED7AF6EEB119C4FA833,
	MobileMenuController_OnEnable_m24E154018F43C43507E78853DEEF2EA843312BBC,
	MobileMenuController_OnDisable_mB1AB08B5A8CF4DCCA19D275FB34F353A551A7697,
	MobileMenuController_CreateCloseButton_mE3BCAA47767B4F4A7F5CEDF3A1309666CC7B667E,
	MobileMenuController_SetRectSize_mD3E9970664F135EABBF60CD53B55954219B5532A,
	MobileMenuController_CloseButtonClicked_mE721054F8789C7FD6143E975DB23043AEA116C1E,
	MobileMenuController_Update_m37E0B39E5C86FA8903358422D4BD4763DAA48539,
	MobileMenuController_TabControllerOnActiveTabChanged_m3D4324224C61562C274B640D41E315C2FC29737C,
	MobileMenuController_Open_m705CBF15445F6541FED9542E7768DF03A9861EF3,
	MobileMenuController_Close_mB998421D5BD045DB9D9D3C98CB24A4B37CAFC4A5,
	MobileMenuController__ctor_m56C6EBCE93AC16F89EB25794B0E5F0348556C372,
	BugReportTabController_get_IsEnabled_m39E3AE771CF5CC5CC650CDEA2647DB04C75C6DAB,
	BugReportTabController_Start_m417150D24DA5C2A399DB7C98E4EFCF477379C9A9,
	BugReportTabController_TakingScreenshot_m7C8356FFB6EFC152DC5BF716B8728A71D435FAD5,
	BugReportTabController_ScreenshotComplete_mE855FE88DB5475E7BE30B2D0246D20D626A6F42B,
	BugReportTabController__ctor_m806A126FBFFC141D06B454844C3689491D8A53ED,
	ConsoleTabController_Start_m9E200D4BCBE8E79702D3CBA5AC4744CC807AEFDA,
	ConsoleTabController_FilterToggleValueChanged_m5602E87CF935C774DD7927371508D98A3036C70A,
	ConsoleTabController_FilterValueChanged_mD7508E463E1C45A96621651F13B229A8E661537F,
	ConsoleTabController_PanelOnVisibilityChanged_mDCB4A62E33A242FC88D916F1EA4A59BCF25CA4A2,
	ConsoleTabController_PinToggleValueChanged_mF8FE7308470D22980D2552FE94B849A26007D2B5,
	ConsoleTabController_OnDestroy_m74713328ADB03FC5F29FEEA63E992FAA2E588E1C,
	ConsoleTabController_OnEnable_mC563387E05F855E58017B39C6A2FE72386A43012,
	ConsoleTabController_ConsoleLogSelectedItemChanged_mCC9C9E465A44E06443E7989E4B7E2C6E7E3F485C,
	ConsoleTabController_Update_mF03AF53056E2628730FCD924077EFCDDC92A8231,
	ConsoleTabController_PopulateStackTraceArea_m59FAE85DE2EEFC428BAC16EFF69CB2A601F20507,
	ConsoleTabController_Refresh_m2BB6284F9B09ACB9DA2AA3A28E39B1BCA903EAC2,
	ConsoleTabController_ConsoleOnUpdated_m89B50D01881D3F8525B2BBE0C8E7E55562969D3D,
	ConsoleTabController_Clear_mDD1AE919F930A60BC03AE59C92D8EB59303A53A8,
	ConsoleTabController__ctor_mC8EDA0D22110A1BA65BA144084EDC74A7B82625A,
	ConsoleTabController_U3CStartU3Eb__16_0_mD00A9DFA9DA20BFBE147F3A02639C90B8C84E237,
	ConsoleTabController_U3CStartU3Eb__16_1_mD97DD51575E0A7848342E409E297DBA5670B23CC,
	ConsoleTabController_U3CStartU3Eb__16_2_mF47E6D19BF3D7B758C43D0C67B7331F47D1C75E5,
	InfoTabController_OnEnable_m5EFF292E9061F3DE09DB0CE623B82AB0F474C587,
	InfoTabController_Refresh_m9D574E17D6E91C541BE1407DDA2E6CA9CAFB380D,
	InfoTabController_FillInfoBlock_mE6ABDBB78DC3AF29ADA9BAD1EB8EFFDDEC402DA6,
	InfoTabController_CreateBlock_m4E39AEF142C5B2AA58B9691D6A0DD8CFE8809AC8,
	InfoTabController__ctor_m5DBB0DB8B91E5A2A118AEFFBEDA3270646969F76,
	OptionsTabController_Start_m10AF6F99B707CC774BAC8E69CD62A86837DD2604,
	OptionsTabController_OnOptionPinnedStateChanged_m112CD84C80131EE9198F25EAF8C13AF97EBD3E46,
	OptionsTabController_OnOptionsUpdated_m4FDF545C7FEF362A66655A4FBFF1BDAF4E4A2533,
	OptionsTabController_OnOptionsValueChanged_m056EB4604F4A268177C0D359D049F6151059624D,
	OptionsTabController_OnEnable_m7238254DF93881C56F57BA278C2CCE04C6ED933F,
	OptionsTabController_OnDisable_m8F77784D2F8710CB91AAB8272D0693FFFCCF1FBF,
	OptionsTabController_Update_mFC4E136192919986388BAA152AF5B7FF5F00BC5F,
	OptionsTabController_PanelOnVisibilityChanged_m83694216530DB824B04D7CB2D3428F1DCE8ABF76,
	OptionsTabController_SetSelectionModeEnabled_m01E6FB428729944905EDFC5889101824151575FC,
	OptionsTabController_Refresh_mB7751B3716267D93B61C6599C2A2A1FF23710C75,
	OptionsTabController_CommitPinnedOptions_m9519909BC8BD9EE49F1BEC19D353E4D5710B6AC1,
	OptionsTabController_RefreshCategorySelection_m6DE8F21676F4651DF38DE416D00278F4C3EDBC88,
	OptionsTabController_OnOptionSelectionToggle_mDF470D5EBCACD99987F56D2455A2FD550CD30B19,
	OptionsTabController_OnCategorySelectionToggle_mAD8DFB11A479E006AAB7CC151691AC3A65A86890,
	OptionsTabController_Populate_mED9BCCFA78967D44A367D38C32EC560C699D8CC8,
	OptionsTabController_CreateCategory_mD4834701571026B828A460AFC8D1D9520598709D,
	OptionsTabController_Clear_m667D25646A60A7C2D1454B940705B92EF302EC71,
	OptionsTabController__ctor_m26A39CB99E82C1B53379AF5879BC5EF2A1E18EBB,
	ProfilerTabController_Start_m14309F4ED0EC0492A77FF4AD9594D15549702EDB,
	ProfilerTabController_PinToggleValueChanged_m1F783BD1F40E639B136D957D585C4A97CBC8FEB2,
	ProfilerTabController_OnEnable_mC535AD61FA74DF071724248A342BF2D76E2A4F81,
	ProfilerTabController_Update_mE560C03596ADE8F3F2B4D11ED7C6D42E4B2640E0,
	ProfilerTabController_Refresh_mBE0E8202274CCCA30625B24ECFE122B62BAF4F0C,
	ProfilerTabController__ctor_mB68179DF4EF754CE4846EF6D59B901F63A8BF6AF,
	BugReportPopoverRoot__ctor_m4A574A7752B0AAFD6996EE07AEE0010FA9E5B3EC,
	BugReportSheetController_get_IsCancelButtonEnabled_mAC0A7E32F1DA100B8285667472E0F273B80D6148,
	BugReportSheetController_set_IsCancelButtonEnabled_mAE83FAFB2D01ED12CE82A9918EBDEF6B0B027A5A,
	BugReportSheetController_Start_mC32DCFE2616F8A4A2FF9DB70F969EE9702C1B9A3,
	BugReportSheetController_Submit_m9488242C632F0F9E675169A82C46951BC21F2D99,
	BugReportSheetController_Cancel_mEA98185D47E56F56B32279ECA2C7C07BDF7D4F4F,
	BugReportSheetController_SubmitCo_mF1A24CE34A40BEDD7660EF56F80EA531B441D76A,
	BugReportSheetController_OnBugReportProgress_m46D1176163ADA36284E7B67D69634F8F91264DEF,
	BugReportSheetController_OnBugReportComplete_m10B486FF493D947842414907FDDCB4494ED45808,
	BugReportSheetController_SetLoadingSpinnerVisible_m79E7B6B434B26E009E589B533532DDCA5639D976,
	BugReportSheetController_ClearForm_mA38771090B5DDF8108E69543D59CDC523A0AFB9A,
	BugReportSheetController_ShowErrorMessage_m3EC687544062DF6DB8EBE125D22AB6D9104C492F,
	BugReportSheetController_ClearErrorMessage_mF9DD33393D18AC5F347D45AB369F2EAEB302E8C1,
	BugReportSheetController_SetFormEnabled_m9608B74185A7B9C51AE4BDD8C587BDB4EDCE31EA,
	BugReportSheetController_GetDefaultEmailFieldContents_m386544D6F7F04D2131135C353F0CDB9F28EA1984,
	BugReportSheetController_SetDefaultEmailFieldContents_m18B6CBF3F2496AF2E766D5AAE2078E45D5B5086B,
	BugReportSheetController__ctor_m38F92447F16B61FE3AD52E5C48D19B450D734ABE,
	CategoryGroup_get_IsSelected_mE3FFF25BFE08F3FAC632835B23E0617A24ACC9DF,
	CategoryGroup_set_IsSelected_mDA05DAC71962FDABA5FF1E97FF3C39AD0C4A8DFF,
	CategoryGroup_get_SelectionModeEnabled_mD7DB21F3F44F44B3E30BE8C09FFBA74EBE44E825,
	CategoryGroup_set_SelectionModeEnabled_m832684B4546321EBECD942F894F96D4F090B224A,
	CategoryGroup__ctor_m95C9D23F6E099968F14AD74C13DC5E66BB0A2C96,
	ConfigureCanvasFromSettings_Start_m028BA54F0F8BA7623F942E967D4B916F0A870CD5,
	ConfigureCanvasFromSettings_OnDestroy_mB0E41DC9ED7B28036867943D8EF6ABFCC8243068,
	ConfigureCanvasFromSettings_SettingsOnPropertyChanged_mF54B34FE38AA458E6CC652F0593CB570D0DE265F,
	ConfigureCanvasFromSettings__ctor_m49B858913FBAD8DF0CE30A245B1B1388866853B0,
	ConsoleTabQuickViewControl_Awake_mC47A9B636FAF9AB0F5C828BD9140C19FE5EDECF3,
	ConsoleTabQuickViewControl_Update_mCBF8DBB53B71BF71D974212EA504403A7C9C504E,
	ConsoleTabQuickViewControl_HasChanged_m4DC1735D4ABB02BE90CD354F89C2213858B59906,
	ConsoleTabQuickViewControl__ctor_mCC0CF7D0784D8F8DA8D77A957C434F6E0013E840,
	ConsoleTabQuickViewControl__cctor_m62E03F627F57CA2A56DED3D6236DFA3F69079B3F,
	DebugPanelBackgroundBehaviour_Awake_m08933A92A9716F28F0781D315395A7F6A222550D,
	DebugPanelBackgroundBehaviour_Update_m28B8DE5A6D2060A6E632BEAB0C9938CC595094D7,
	DebugPanelBackgroundBehaviour__ctor_m6042FABA9DB86A650D02ABE99229A81354BEB0E3,
	DockConsoleController_get_IsVisible_m9A93603019C1029B8DA99F0D894131B489E0B080,
	DockConsoleController_set_IsVisible_m5550EBDD156910079CEE467E10CA6C35A0D12EA1,
	DockConsoleController_Start_mA270144F2A757B440370C29FA6DB71FE509BC760,
	DockConsoleController_OnDestroy_m19C13B823FA607D9B692D135864B5499C6F960DD,
	DockConsoleController_OnEnable_m971C640771EE7305A0FDC37F109EA259ECA68ED7,
	DockConsoleController_OnDisable_m55A514F7089CE261B40C349EBF8D2DDB62375D21,
	DockConsoleController_Update_m34ABF4D475EF59461236A484BED98E06352F4C9A,
	DockConsoleController_ConsoleOnUpdated_m06B918B4E786C50087553DB5A5FFF67EB794A514,
	DockConsoleController_SetDropdownVisibility_m7F028A81B9599BF3DEABBB34C4628649BDEF213C,
	DockConsoleController_SetAlignmentMode_m60151647B95E5C8206A2E402FB1D46CF71FC9831,
	DockConsoleController_Refresh_m7D3A2F9FBA840FF113EE1E861E67B837FB7E6A71,
	DockConsoleController_RefreshAlpha_m406B6203BB8C27BAB06348CE0D2A1A13A5C7C3AA,
	DockConsoleController_ToggleDropdownVisible_m5F2129C94BAD74FA3D104FDC553FE14344BE15F4,
	DockConsoleController_MenuButtonPressed_m2683A5A3B6E9B12C6BDE490966081F5809762F7D,
	DockConsoleController_ClearButtonPressed_m60A2B907C0A9705E528312984E791F1C976A2362,
	DockConsoleController_TogglesUpdated_m2CD34F0929499E15ED4B37F4D2A14585AC1D6E19,
	DockConsoleController_OnPointerEnter_m1AC0707D8B983EE7E08F8BAACA32DE1C041FACA9,
	DockConsoleController_OnPointerExit_m60BA2369FC13F567D226541B0E3DC4FD029BDD48,
	DockConsoleController_OnBeginDrag_mCB4183BBBDCE680DCB1AEC58C782D9AC7E6A6BA0,
	DockConsoleController_OnEndDrag_m40E1990F866DE41F230E1C76C19CE5347F781190,
	DockConsoleController__ctor_m2587A46DC92F304D3ADB703C72C08E1AFB40EEDF,
	ErrorNotifier_get_IsVisible_m67CFFD30BB221C81EC296828A7F42CA63A204A01,
	ErrorNotifier_Awake_m63EB7A589008A25DD462E3D8E95B415B06B0EADF,
	ErrorNotifier_ShowErrorWarning_m6A6362C5BD44A44EE9946D37177E4726CF49FF7C,
	ErrorNotifier_Update_m98CA626F77013067BF4A075402E0D3D45E269500,
	ErrorNotifier__ctor_m1BC463B2EFF2C95E05576476D86701C59CC89B63,
	HandleManager_Start_mE5B1E952E6FF0953B09610842B516E75428C2113,
	HandleManager_SetAlignment_mCAB95A83E614CEF0621B6C02AA5C6BA199629DB2,
	HandleManager_SetActive_m9C5B71504ACDB35926CCDBC764E6370564D1B3B9,
	HandleManager__ctor_mFC19187A2ABF4C444592C76A92CD907261020C9B,
	NULL,
	LoadingSpinnerBehaviour_Update_m2844BD52D379AB773F0A92E72E1F4FF431688AC8,
	LoadingSpinnerBehaviour__ctor_m32E843D154FA7B9925C36B5EBFFAADFD7765E186,
	PinnedUIRoot__ctor_m042696C7BC61A5C68AACD2B6DABB4EE3FA0CCB76,
	SRTab_get_Title_m11B520C29B44EED6F6524EBA986B7F655CD353A8,
	SRTab_get_LongTitle_mFE958DE9DF7A6DB6C877ED62DE27BB309E4A0E06,
	SRTab_get_Key_m750D8D8155AA02D8EAD1A7D1B1B612FFE51EB398,
	SRTab__ctor_m7DF82E9E895D7C0657D2504090E6F0F805ED4FF4,
	SRTabController_get_ActiveTab_mF5B9A2BC42571E4F89087B7CD6A3952516511E99,
	SRTabController_set_ActiveTab_m9852C323DD5A85C97AA608D0A4C5086CA5B57419,
	SRTabController_get_Tabs_m6B6793BB0BBA5783D425C66139EE5575535AD6E3,
	SRTabController_add_ActiveTabChanged_m3F6CF99462DC6B3D124C941646B299AFC12EDA7E,
	SRTabController_remove_ActiveTabChanged_m7AD66B064EAA7B66193E0C2E074171D86E02A388,
	SRTabController_AddTab_m59EDB3D3D90B9FFA9BA7688BD378BCA3908923A5,
	SRTabController_MakeActive_m71265A8F1B3E64360C71470BE080E2586D6D6F7F,
	SRTabController_SortTabs_mA35573CB9A7B02361ACBC3E9DC123669BED8DD7C,
	SRTabController__ctor_mFA9C7CB47A0301701A21ADE0C7CFC9EAF3BEFB2E,
	ScrollRectPatch_Awake_m6E45762E82EBF63291BF93CD63B1D3EF97D7A77E,
	ScrollRectPatch__ctor_m35433DE9C0A08DA636FF68D877EA275700CF53C3,
	ScrollSettingsBehaviour_Awake_mF7CAD8F64444BC853460B69C11D3A152C1799586,
	ScrollSettingsBehaviour__ctor_mF76FE2D118A3CB4D68591EC545A43416E290B0E9,
	SetLayerFromSettings_Start_mCEDD638BE3830D613B9BBCCC15CBFCB9F1170F43,
	SetLayerFromSettings__ctor_m14690E55727F97141E79FB27EF667AD40DA482C5,
	TriggerRoot__ctor_mE01DA04F380944BB91DC2ACBB8E4484F3495F651,
	VersionTextBehaviour_Start_m924205D0CD12CF5A973B784A9BE40784498BB7F3,
	VersionTextBehaviour__ctor_mFD97E71AB97EEBA0F140613259F78A7C057BD014,
	ConsoleEntryView_SetDataContext_mA913C2FAE3A5D658D93B73362440DC0DAF2388E6,
	ConsoleEntryView_Awake_mCA96C0EC35DA40F90D51BCC21CA88BE87C939704,
	ConsoleEntryView__ctor_m04766042B66C2C23160CD618E80453E80E5F9CF9,
	ConsoleLogControl_get_ShowErrors_mE2898A4D87C6D68638D7693E841D193C4284439E,
	ConsoleLogControl_set_ShowErrors_mD94EC39E79D4802AD180AC09E7E3B603AE0AF564,
	ConsoleLogControl_get_ShowWarnings_m252D110D54598EB2872F3B65883247CA1B2A3AEC,
	ConsoleLogControl_set_ShowWarnings_mDDF0C7F8AB7182118FE67FD595848109D5B4D35E,
	ConsoleLogControl_get_ShowInfo_m941035A10580FDD257BE9A7D8D853A957D16621A,
	ConsoleLogControl_set_ShowInfo_m65CB22ED9FFBD81F5153551304A2E101B3A49FE5,
	ConsoleLogControl_get_EnableSelection_m5A41F357895A79837D0BA6D74B7516D64D803B86,
	ConsoleLogControl_set_EnableSelection_m8405D98E1304F2CBE4F124ABC715E660682D555E,
	ConsoleLogControl_get_Filter_m0DE946320418D7760BE891474569E61A222BEF5B,
	ConsoleLogControl_set_Filter_m428CE6D787570E20EDFDF96D7F79A42B1EB62BB0,
	ConsoleLogControl_Awake_m209CC9A0AA477333228380F012274BC0DC365012,
	ConsoleLogControl_Start_mF81FAEC5142F4AAA33BDC99FCC508EE9DF3AD9F8,
	ConsoleLogControl_ScrollToBottom_m693B0277637AACE265741312405CD162BB9B901D,
	ConsoleLogControl_OnDestroy_m417AAEA07E080E20B32A185061BF2DC5A9378496,
	ConsoleLogControl_OnSelectedItemChanged_m99E53C343006E7C7E18716C294472F47967BE741,
	ConsoleLogControl_Update_m9B9046E248910545B2F3BBC01790B15E28A0545E,
	ConsoleLogControl_Refresh_mA1CE8F0B6243FAC49FF278A64F3AF762855A42B1,
	ConsoleLogControl_SetIsDirty_m0A3FDCAC2D2926DA9804606B8BC00BE908A54EF4,
	ConsoleLogControl_ConsoleOnUpdated_m95D80C28284014D49A6D9F157E6DBBF44DA4B9AB,
	ConsoleLogControl__ctor_mB8E454312D725AA3FDA732E323426040AD0DF1C2,
	DataBoundControl_get_Property_m5F991B3284FE886D27C8F315A2A46D95B47B62F1,
	DataBoundControl_get_IsReadOnly_m68BE55030740ADA6AFD78204DB4FF058081C718A,
	DataBoundControl_get_PropertyName_m60026BF1A47D8220660DBAFB4F1873160AD29A3D,
	DataBoundControl_set_PropertyName_mCEA9801AA1402F5099580C98CCCD4DA81EE98AF3,
	DataBoundControl_Bind_m9CD9105A8A0FE45EF4B546F598819D295E8A27F7,
	DataBoundControl_UpdateValue_m8043360BCD829C35BF12E4F0A09B9CAB73E696AA,
	DataBoundControl_Refresh_m56274BF1AE902EB6F6F91D4C34F0DF45EC4DA172,
	DataBoundControl_OnBind_mA0A59D3769BD9CDB0EFEC1BA4B6697ED02C6EFF5,
	NULL,
	NULL,
	DataBoundControl_Start_mE47E42E4E4B870CE1968309C60B809C3B6F448E9,
	DataBoundControl_OnEnable_m912E56F06BEE78BEBB324E2361C90B48CCB279D6,
	DataBoundControl__ctor_m73184E5E1B83A246F2D602726A48ADE9DF63D108,
	OptionsControlBase_get_SelectionModeEnabled_m495FB680879E0E208BA1F188539796873212030E,
	OptionsControlBase_set_SelectionModeEnabled_mEC811FB41EA4DBE21EBF001609E44B7DEC34285E,
	OptionsControlBase_get_IsSelected_mF93AF8924058BDB88F13981B75C9D4997AD3A80A,
	OptionsControlBase_set_IsSelected_m2DA440B23103A9E3B4FBA7C46B7AE73EB5F3DB71,
	OptionsControlBase_Awake_m11072F2419B7984C647C260ED4D7C76D0C35C498,
	OptionsControlBase_OnEnable_mC24A03395267A2E5D22CF2263D9875BAD2E1E4CE,
	OptionsControlBase_Refresh_mDEB82F87BCDE85F9D69CD734B9100224CA0503C4,
	OptionsControlBase__ctor_m55AC55BEA87783CE7B9C944E0A845D9FA0969E18,
	InfoBlock__ctor_m5E7CC3D780D5BA13CEF105E7FD53293EB12DD2F2,
	MultiTapButton_OnPointerClick_m44C6E3E9BB8620FCF8880B7B47AA590A704E68FF,
	MultiTapButton__ctor_m1CEE7BCAEDD2CE9010AD4DF39B6AD13CC5773396,
	PinEntryControlCallback__ctor_mB0D2FFB32F4DEE039AC9E5DE66A90F234A004746,
	PinEntryControlCallback_Invoke_m97522D755F000DE5EE4AF71FEE25CC179E95554C,
	PinEntryControlCallback_BeginInvoke_mBA815892B34430F37DF28173796AA385D961ADAF,
	PinEntryControlCallback_EndInvoke_m8FB19C9C487591CA940BE9FBEB8507EDFD99740C,
	PinEntryControl_add_Complete_mAC3FA4E9418A61FD6361C3F5C81444A85A6039D5,
	PinEntryControl_remove_Complete_m5311024470631FD13B52F4E3ACE2AEA02EECF423,
	PinEntryControl_Awake_mB05CBEB732ECECD18C249D837158D7416967E447,
	PinEntryControl_Update_m47E9FDB259B6B05853580925E566318516DC8CA6,
	PinEntryControl_Show_m7806CBA0DBBD1FB5AFB83AA248CC5B52A1AA4224,
	PinEntryControl_Hide_m938DBB024550C1471D3C0FA4254B12FE05AF7D34,
	PinEntryControl_Clear_mACE962439CE5785C9CDDF929F6E590476C068ABD,
	PinEntryControl_PlayInvalidCodeAnimation_m6A884A02E25182F16BAB731BE0CF5459A9297B92,
	PinEntryControl_OnComplete_m9FF538EB049BDCC1C874337761FFA076555E9A8A,
	PinEntryControl_OnCancel_mCE4F865F8025877C817041EEDAFB6629F202D561,
	PinEntryControl_CancelButtonPressed_mF527227303235C1128D005601A1F8E704DA91BE6,
	PinEntryControl_PushNumber_m44B8F6D8C784BB2F790ADFFFBDCF6F5E4405C2CA,
	PinEntryControl_RefreshState_m101B41AAD963F421EFE0530550396A1E52E7C03E,
	PinEntryControl__ctor_m9666243DC6A514FF98A3A540DD7F5C4AE78F2F0F,
	ProfilerMemoryBlock_OnEnable_m14DA18FFE85953D09CE48F35C617D38B356CA12C,
	ProfilerMemoryBlock_Update_m6DAEDF402DF959D8604CD73C07819B309F5C83D7,
	ProfilerMemoryBlock_TriggerRefresh_mD3AADC6124FB85B6F1310597D574E9F181F204B5,
	ProfilerMemoryBlock_TriggerCleanup_m421E1A2E1812189132EC53B894D1340EFF9D9285,
	ProfilerMemoryBlock_CleanUp_m136D3FE5FA37AEE662278E308780AFB851180F5D,
	ProfilerMemoryBlock__ctor_m14511FD971869B71E4B1DA28927BF2F1C232AC6D,
	ProfilerMonoBlock_OnEnable_m2CA458F7DCE6923AA25AB89A66A44B239E3F7499,
	ProfilerMonoBlock_Update_mD6FEEFB6199B13F025B1940AA8D11F94334F641D,
	ProfilerMonoBlock_TriggerRefresh_m62536D119807386AFB0B89A4F62704B3B1D5745E,
	ProfilerMonoBlock_TriggerCollection_m35231433A40708F39CBF1345661F4CDE2DF78547,
	ProfilerMonoBlock__ctor_m22269EAB87064C7DC77B82A2F99ADCC0F1256608,
	ProfilerEnableControl_Start_mB04A0A63E5A98347D3B6A7D2E80B9F2C26943810,
	ProfilerEnableControl_UpdateLabels_m079656E781DABF7A91699C01C3F805F7AC7F8BAA,
	ProfilerEnableControl_Update_mB767DBE8DA636EAFA1449C35790E5DB65CBDE562,
	ProfilerEnableControl_ToggleProfiler_mA0555DF2A3E8C83F02D714F646EE8A633ED744D4,
	ProfilerEnableControl__ctor_m03216744E6CCE203FE377CA38782385BB4EA6AF8,
	ProfilerGraphAxisLabel_Update_mC04347429BE14F8F02449E60C0DEA126B6232365,
	ProfilerGraphAxisLabel_SetValue_mC1605FF4E6ABA23FB088227C3DCDA7F9B542ADA0,
	ProfilerGraphAxisLabel_SetValueInternal_m136556544EBA74B2A56519E5F87A4C937202CD58,
	ProfilerGraphAxisLabel__ctor_m068F74E672BA78441A6BC039FDDEE484ECDC64AB,
	ProfilerGraphControl_Awake_mACE59E0243C36EB98AA8C2CCC5D5EC729BCE6D70,
	ProfilerGraphControl_Start_m9354A0EDDBD30AE6FDB783A98A4A9A72D73AB492,
	ProfilerGraphControl_Update_mA960AD00604C50DFFB4C911A9D0F94DD5AC3F247,
	ProfilerGraphControl_OnPopulateMesh_mC269E99E558F5B87485254DEACF90FF0975F3512,
	ProfilerGraphControl_DrawDataPoint_m1EC4BE9B3972563274449999579423E72912D1A2,
	ProfilerGraphControl_DrawAxis_m36CAFD6B3BBE5757D1594FE3108C964227682C35,
	ProfilerGraphControl_AddRect_m8E1E0F717D296DF4AC32F417AF1ED41CBFA50DC1,
	ProfilerGraphControl_GetFrame_m52BAE866B6348AA71B6E96153132B2368EAE07A1,
	ProfilerGraphControl_CalculateVisibleDataPointCount_m459DCF2792A2D6524EBD09CCD33B62FB20B3B986,
	ProfilerGraphControl_GetFrameBufferCurrentSize_mFC45B36A8E59C0B4D78A9D2BD49F611D50437FEC,
	ProfilerGraphControl_GetFrameBufferMaxSize_m6067991A69A735736454B248737E607120524E4D,
	ProfilerGraphControl_CalculateMaxFrameTime_m25AAB2A70060F591D0CD09DE947B4D6EA948B7D0,
	ProfilerGraphControl_GetAxisLabel_m32C3EC33E25EDCD988916088C1F0A1739430D1A8,
	ProfilerGraphControl__ctor_m40C428DFF7A32B51241C04CE0C907B8CFBDFD11F,
	ProfilerGraphControl__cctor_m35C21A6CA9D6421B3CD0EB643563828CC881B972,
	SRTabButton_get_IsActive_m8FF75A9268816A50D91459B808D0248AFFD65FAE,
	SRTabButton_set_IsActive_m74BD99618AB4F702078BE69F6DB51CE6A408C7F4,
	SRTabButton__ctor_mCC38FEE371562F6385515992101FA36633CAA888,
	ActionControl_get_Method_mB335111A30E63EF41278A888276133BA3036DCC3,
	ActionControl_Start_m71EAFB2FE493618040AA7FDBA70E2F53F1670DD4,
	ActionControl_ButtonOnClick_m02F8DD7BCD1E1764070B0A17F8F70003131F8192,
	ActionControl_SetMethod_m9C81AD38B1E7C2395C5E968C44FFD681B37F944F,
	ActionControl__ctor_mEF06B8692763DBE6DC92A7230880324C71998E51,
	BoolControl_Start_m6766657C170BDABD643F3C154D211C68501E308F,
	BoolControl_ToggleOnValueChanged_mF1D75E19EA19041089D471BE8C1032684D785105,
	BoolControl_OnBind_mFC4B881D0F42CE880E2A7220B0D5CC4B3CDFC924,
	BoolControl_OnValueUpdated_m93210E016B513BE108EF3B28E7A8D3DC27880329,
	BoolControl_CanBind_mF99504B0FC3BC77F6697EFDE27EC4EF8D5DF0008,
	BoolControl__ctor_m021E63B02E82C4B386670062948F84438980405F,
	EnumControl_Start_mD8339DF53A036F7EE2B32E6943D8C3AFC2F702DE,
	EnumControl_OnBind_m947B07C3FF95069D1CB279B0A7681C77469DE739,
	EnumControl_OnValueUpdated_mA7523F3C0CC7BACFE323D7003CAFC1973E739303,
	EnumControl_CanBind_mCA03B23E414B7899C9C9FA5D2CCAC874E087EA45,
	EnumControl_SetIndex_m9DE6AAB5FE88153580DC61DB6B28BCD6E74C2C7B,
	EnumControl_GoToNext_m9AA191C30CEAA89426B59DBC80952B619E0D78E1,
	EnumControl_GoToPrevious_mDFA0FE20BE8129CA9055C03B29B86E95AFA0FBCD,
	EnumControl__ctor_mDA6C8F637B2D921A4E63EF7B5BC2AB35C3B463D8,
	NumberControl_Start_m154F62C16A841BE25B5CB3E06F14979DEAB9DC50,
	NumberControl_OnValueChanged_mCF3796638E0273B9CEE7DED3B1C56E3CD76EEC01,
	NumberControl_OnBind_m5B5B25F9C15C3DDB9A81F80532AB1DE1EECAD833,
	NumberControl_OnValueUpdated_m1401CDE51125A8A3C2BC9300B69342D0903BE5E7,
	NumberControl_CanBind_m12E52AB7A3693EDD25634EA00783BAA1D36848C5,
	NumberControl_IsIntegerType_mA6BF49E032940452A875985DA5FB1F861070698E,
	NumberControl_IsDecimalType_m7732FB59605E35A236965D90EC37EDD20DBEEA48,
	NumberControl_GetMaxValue_m9AC535BBB2E8C642178B18B7CA33BC8159D5E53A,
	NumberControl_GetMinValue_mADAE6529D0EDC8B78A5598849A30D706228AC4AE,
	NumberControl__ctor_mC0ADA44BDA875A47949C5FF7F0ACD631804D83B3,
	NumberControl__cctor_m2A3AF00D0881594B4569D01644D44809210B1D21,
	ReadOnlyControl_Start_m71DBC1EB9EBB10F47EA95AEF6DFFECF0A5214FF7,
	ReadOnlyControl_OnBind_mEDBC04066ED37526EE2F1A1029D8B28CA84778FC,
	ReadOnlyControl_OnValueUpdated_mCF4AFC87CC5573F1F5ADC9C7D9578B98134315F2,
	ReadOnlyControl_CanBind_mA35D75186D867C1AEECCCC15D862C7F5C0EB30B2,
	ReadOnlyControl__ctor_mCB64B2A7A5A3585E4481788A78EE612E2776CE8A,
	StringControl_Start_m23811B91F9EC11CD0E7857E001ABB131A9A0AC7A,
	StringControl_OnValueChanged_mF53E8EDA7B2B3D2314E2F1682F133561D0EA08C4,
	StringControl_OnBind_m27D88B67D88823252C4BBF13667BF7CC5AC54342,
	StringControl_OnValueUpdated_m69E39889E5C8BD0DC5B4338299206CE9B54171BF,
	StringControl_CanBind_mA3C802775981E37459B77468EBB5A87FB2F99892,
	StringControl__ctor_m116CB6B1A1C41F217BCD588036E46FEC3E26C235,
	BugReport__ctor_m0F7DCA7A97A71AEC5F743C8D5522E571EFA0E299,
	BugReportCompleteCallback__ctor_m7E9DC54D186F331ABF7DCD65E98F0539EFD4F7F6,
	BugReportCompleteCallback_Invoke_m3EB23A169711E88A56349D15AAB77B8EB2980BD7,
	BugReportCompleteCallback_BeginInvoke_m7C85F653D1C17B29FC4D5268D7CE05445D07799F,
	BugReportCompleteCallback_EndInvoke_m1AB06FCA285D06B965742D459A360C5C87578EB8,
	BugReportProgressCallback__ctor_mE1B5AE82F88DB214714C3754284A3AE530CF71CC,
	BugReportProgressCallback_Invoke_mC2315CA5EB755394967BBCEC4EF5D67366296C5A,
	BugReportProgressCallback_BeginInvoke_m11199B9570B4B5655DAEA61A72005C9FF79BC477,
	BugReportProgressCallback_EndInvoke_m9A785D9E4D1411C262138630E1396B301B4761F1,
	NULL,
	ConsoleUpdatedEventHandler__ctor_m9BC3C608183B85D9F6D2F9EE52C415F9CD58EC63,
	ConsoleUpdatedEventHandler_Invoke_m21A2E6A247BF71B6AC0DADA38A65DA1C6068EC8D,
	ConsoleUpdatedEventHandler_BeginInvoke_m5161547483546EEEBFD201DD0A28EDAC5EB2E292,
	ConsoleUpdatedEventHandler_EndInvoke_m3642D984B8ACFF8940C776075CB78F3F0EBA74D4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ConsoleEntry__ctor_m65CE2BA141122C89A191EC1C581E229E864F379C,
	ConsoleEntry__ctor_m47C36A7EA63A8ACC14000F5F5925D25088976FC8,
	ConsoleEntry_get_MessagePreview_mD74EF661A9344D0DDA16867C7C126E7C23EE1532,
	ConsoleEntry_get_StackTracePreview_m7379982D4D0F16EFB21DED0AC49D9C63B54CB961,
	ConsoleEntry_Matches_mBB49D0A1B29D25E80636B885CC2546FD1DC2DCCB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PinEntryCompleteCallback__ctor_m395D6DE59E8805524F139DDC6310A043216EC1C7,
	PinEntryCompleteCallback_Invoke_m2B523BCE54DA179E20B0691DCDE47B539CC67A0C,
	PinEntryCompleteCallback_BeginInvoke_m5EB1453368ECE175EFCDA083A5645D69AC94F34C,
	PinEntryCompleteCallback_EndInvoke_m33D166EF11E8D4E0ABB5FFE3303159C0596EB30F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ProfilerServiceSelector_GetProfilerServiceType_m64C3D7FCA9902885C302C1B828CABEDFAB95303E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BugReportApiService_SendBugReport_m23AC9C7337D698041ADE0915A724CBCE36512306,
	BugReportApiService_Awake_m8EEFD758BE014504EB6BE9A1E2087048C7177BC2,
	BugReportApiService_OnProgress_mBA9BF2BE4DF55CCA902D8E6288442A9931BF6927,
	BugReportApiService_OnComplete_m6B1F2146651145E9A3D43DCD447A7D18C404E7B7,
	BugReportApiService_Update_mF5ACDCAFD26D8E5344C638C52EA72BA488DA59B7,
	BugReportApiService__ctor_mFF9E2EE284EB0067398D1C97555BB8B1FB9538FC,
	BugReportPopoverService_get_IsShowingPopover_mDAD558ECBA8283235FBD5B6ADC60876A924A425A,
	BugReportPopoverService_ShowBugReporter_m4814A24731F7CBB5F5FBFB7E5CF55BF3BAAC6BAB,
	BugReportPopoverService_OpenCo_mF8E6026CB3904F9125CD1E33EC159B56A8B07D4E,
	BugReportPopoverService_SubmitComplete_m02D43C03928E129D61003757C66E499D8C6D63F3,
	BugReportPopoverService_CancelPressed_mBA82E47FC9A8B0384DBE863172CD9D29D9140091,
	BugReportPopoverService_OnComplete_m0D016FDA3C37B234AB1336CFFFFD0E571FBE0EB2,
	BugReportPopoverService_TakingScreenshot_m6042C6A0141F4782CC37BB202040813ADA2BA4FA,
	BugReportPopoverService_ScreenshotComplete_m203C3A80D2163386CE46DFC8E2944857454C49DD,
	BugReportPopoverService_Awake_mF239C64CF04F28BAC4669C0C40154231662B1CEC,
	BugReportPopoverService_Load_mC3E27173706B3A72E13B2176920657EE313419B4,
	BugReportPopoverService__ctor_m0BBBFAAD3D7ABA6B68C202673E8E65AC04420C31,
	DebugCameraServiceImpl__ctor_m5835CAFF14D37CF1E23C8E33621EE3CE50CEB1EA,
	DebugCameraServiceImpl_get_Camera_m72FBBB87C9A676F021E89617BE967C8C5C4700CA,
	DebugPanelServiceImpl_add_VisibilityChanged_m439646FBE3B05FB02CA91A3CFAE10E5163D1FA74,
	DebugPanelServiceImpl_remove_VisibilityChanged_m8232AD3E67302226F04668726249ED7686DF9398,
	DebugPanelServiceImpl_get_RootObject_m36F59FFCEC66A108EEB8399FEF16CFF84E254741,
	DebugPanelServiceImpl_get_IsLoaded_m6AE65A3DEB2CDF7AFAD1C504310DC0A9B4D7F448,
	DebugPanelServiceImpl_get_IsVisible_m5E8EB20A0E17AFEFE21A737C5F1DDECDADC3A3D5,
	DebugPanelServiceImpl_set_IsVisible_m7ED37A4D4BA39CEDD1AD14EA91DBB532052B893D,
	DebugPanelServiceImpl_get_ActiveTab_mC431FB776EDC94B4BF61EF6920F6E37B34868675,
	DebugPanelServiceImpl_OpenTab_m1BA64B2303679C9BE69DC966D24322D7EAFC6BB7,
	DebugPanelServiceImpl_Unload_m26AC6E0338C0938FAEA8187237E7BE4FB6E93028,
	DebugPanelServiceImpl_Load_m3513C1D8F1A5E9D1CAFAA1DE3758BEE50C106C1A,
	DebugPanelServiceImpl__ctor_m29DCBE555E420134F0B5828084B993C6291DBDA3,
	DebugTriggerImpl_get_IsEnabled_m2A7F07BFDDA5B03040EC59A0B37FF5FB99A2CC04,
	DebugTriggerImpl_set_IsEnabled_m1FD15DF186EE95C2A365905749B0B1E2B3F9268A,
	DebugTriggerImpl_get_Position_m1FD13997FC8FB6448A2F3FA5ACE38F1E280C9B09,
	DebugTriggerImpl_set_Position_mD437A087ADC3B751802CF7855C7C9298C631A0FC,
	DebugTriggerImpl_Awake_mC92DD5797E7DB28C7E9F233464823D24DE314E41,
	DebugTriggerImpl_OnEnable_m8CF564318BF1C9B008637175558E1034AB23E18C,
	DebugTriggerImpl_OnDisable_m5B616BC4B7412D19DA9E1F6417A8F23E343F7589,
	DebugTriggerImpl_OnError_m5B9EC3BF8E1B2D12BE9785BECE99124D6762DDA5,
	DebugTriggerImpl_CreateTrigger_m0B9DCDD5D65992513F35BD79F3CF37B10DF5D081,
	DebugTriggerImpl_OnDestroy_m5339585CCA073BC56866C26185213A253A091DE4,
	DebugTriggerImpl_OnActiveSceneChanged_m076C5B7F8E6D1354C13FAE14A2A9CB03C0FBC263,
	DebugTriggerImpl_OnTriggerButtonClick_m7A169FEA4C5E36CB02802552D41D701D455493AE,
	DebugTriggerImpl_SetTriggerPosition_mACFFA8C372B6735F1BC450F5E2E2A07DFC532234,
	DebugTriggerImpl__ctor_mFF3CE28EEB2FF6CB52C038D63BE48F4B27443CE5,
	DockConsoleServiceImpl__ctor_mFBC87A533806289C577A45DCE47EF01D5FBA4B58,
	DockConsoleServiceImpl_get_IsVisible_mB113071D3E91EAAB1E00D9AADEEE15C52A41AD8F,
	DockConsoleServiceImpl_set_IsVisible_mC5EAF2CD1EB47E18E7154D79265FDAB77672A5FC,
	DockConsoleServiceImpl_get_IsExpanded_m2D1BABA5E030C0656BB8B17523A7A52EA92D054A,
	DockConsoleServiceImpl_set_IsExpanded_m2252CFF687A411D3374146E8DC2144B9A1F831A6,
	DockConsoleServiceImpl_get_Alignment_m52EBF1EEA525049376E86C97351EED6E556083C1,
	DockConsoleServiceImpl_set_Alignment_m429A1AE6950F9E676821953F6935ABECFCF7C401,
	DockConsoleServiceImpl_Load_m9697071AA3701B849AD46AA73C79A67990A63B4F,
	DockConsoleServiceImpl_CheckTrigger_mFC3DF9030E46292CA5C7191017B3158C53D5369D,
	KeyboardShortcutListenerService_Awake_m910D651D6330F27D179C090715B3E5957194D047,
	KeyboardShortcutListenerService_ToggleTab_m32D44A5191F6822A95161FAC586811B13287F40A,
	KeyboardShortcutListenerService_ExecuteShortcut_mCD3CDDF2DE1F83C67AB698CB458DFF914AD0E15B,
	KeyboardShortcutListenerService_Update_m83957AE5F1A40D721FD1CDA2FAB7132109B9D61D,
	KeyboardShortcutListenerService__ctor_mF9967E784C23860A5767CFD0B1FCC54F12ECBC4E,
	OptionsServiceImpl_add_OptionsUpdated_mF8310A7FB5776A3EC6E946A7A9ECD2D26B46268F,
	OptionsServiceImpl_remove_OptionsUpdated_m4EFEA262FC3EB0390F411A43AE3C3102A20672E9,
	OptionsServiceImpl_add_OptionsValueUpdated_mCA5075018ED8ADDFAA262C651AD169BE74E6B198,
	OptionsServiceImpl_remove_OptionsValueUpdated_m97394B0CA9BF542AF13B062FC27C88637DD6798B,
	OptionsServiceImpl_get_Options_mB6D67085309B055F8379FE8F2915F6ABF2642C55,
	OptionsServiceImpl__ctor_m8D9A26BBF69016C43B3BEAE8FD00473C0747FAA2,
	OptionsServiceImpl_Scan_m2F88990D5A363E15D351EE5FAF7080591B135BED,
	OptionsServiceImpl_AddContainer_m51CC9D71DD05B15BE30AE2641B75DC0CECB37C87,
	OptionsServiceImpl_RemoveContainer_m0B3F03B1ADB67DB9D6BCD48794ED4FBB9F4D62D2,
	OptionsServiceImpl_OnPropertyChanged_m4421A2DF6C8F7100CFED6833A2BB5D665FCF462C,
	OptionsServiceImpl_OnOptionsUpdated_m0A7B8045451396EA8DB41183C98428FE1D522CCF,
	PinEntryServiceImpl_get_IsShowingKeypad_m2406035A3FCE684FB5C6172AF1C890E3FCDC72C8,
	PinEntryServiceImpl_ShowPinEntry_m9EB3C180BED2645F3F922DE69159670ACDEB9CE4,
	PinEntryServiceImpl_ShowPinEntry_m593D54AFE157FD43443E1D5B0C8E75B1B50CADCD,
	PinEntryServiceImpl_Awake_m3B40F39CBDEB1980ED17C6173F4B34FCB2414E7A,
	PinEntryServiceImpl_Load_m83E18B81FB46F8F205EC84A9786DAE276ECB8030,
	PinEntryServiceImpl_PinControlOnComplete_m70BDD73335A72EC6C283B9BB4D356BA5A820D925,
	PinEntryServiceImpl_VerifyPin_m806C55B2C9BC28DF4A80AE1A5C4B3F8F8C29D853,
	PinEntryServiceImpl__ctor_mDB9F8A158643A64AD436E27D172D4C1D2E28B219,
	PinnedUIServiceImpl_get_DockConsoleController_mD94B4FC45BFE7EDAE0B20A4973324C5B41D7DD7E,
	PinnedUIServiceImpl_add_OptionPinStateChanged_mA26BDCA3583ED03E9424F13D5374C916E4B6BF11,
	PinnedUIServiceImpl_remove_OptionPinStateChanged_mA0DDED8C0E5F45D69A8577D06193A1A1FB66932A,
	PinnedUIServiceImpl_add_OptionsCanvasCreated_m987520D1FD78ECD7286022A627BA8B78B4223B01,
	PinnedUIServiceImpl_remove_OptionsCanvasCreated_m0F962AEF9CEE18AE07DD16AEBC623F4C6B89D71E,
	PinnedUIServiceImpl_get_IsProfilerPinned_m3C05A77E0CF40841438006E4F2BBB10F8F48093F,
	PinnedUIServiceImpl_set_IsProfilerPinned_m6AD8F2C77FDE7399DA257F74330A8D85D59DC303,
	PinnedUIServiceImpl_Pin_mF2ACAFCFE5C603A8EA0286E8D46F09231D827A5C,
	PinnedUIServiceImpl_Unpin_m6B9136B948378E8D36FE95CFF3EC5B7A066C9F05,
	PinnedUIServiceImpl_OnPinnedStateChanged_m8C49B7CDEF0D3680CE47A79ED6D5040D671AA23E,
	PinnedUIServiceImpl_UnpinAll_m261A02E05A62BBD7980F68FAB592A2FFF4DB3DB8,
	PinnedUIServiceImpl_HasPinned_mF67FE6968B7012B7754FA2512ACB48C5BF743BC8,
	PinnedUIServiceImpl_Awake_m89EB23C06B5E668BF9177C26AA82D8EB818D2F75,
	PinnedUIServiceImpl_Load_m21FCD4CB4C742B7F74780EB13CC3050B45937FA0,
	PinnedUIServiceImpl_UpdateAnchors_m6C3A7A8F254EDE22771D17C56EF523149A518D0D,
	PinnedUIServiceImpl_Update_m693F45EF8304E2ABD39EEBF4C905C432AEBBDA1E,
	PinnedUIServiceImpl_OnOptionsUpdated_m6C130C3330946988F3A559C4377727AA9A2D174C,
	PinnedUIServiceImpl_OptionsOnPropertyChanged_m8CCE2ED9D7E9958C43C4D0A1AC959490F7FE5C5B,
	PinnedUIServiceImpl_OnDebugPanelVisibilityChanged_m22ADC62D30ED454F974D736D295D1F49FDFCDBE9,
	PinnedUIServiceImpl_Refresh_m14CFC4201FEB2D93D139D6F321F90AACEBA9ACD5,
	PinnedUIServiceImpl__ctor_m0B5AE017F5BDC7B57CF3E247E5229B4399B38C72,
	SRDebugService_get_DockConsole_m67043FD09717BE5C0C1875E14495C5A1A3BF4AE7,
	SRDebugService_add_PanelVisibilityChanged_mD04E10B62CA9DC20EE5639BCBA82BF2254894AE1,
	SRDebugService_remove_PanelVisibilityChanged_mF34CC516990D006DC530CD819A829AA5E2E1555D,
	SRDebugService_add_PinnedUiCanvasCreated_m58BA48906BE8A60D5A45560E42A46EB17D65EB03,
	SRDebugService_remove_PinnedUiCanvasCreated_mF49633C25B13A4282092E2AA7585B07327B4598C,
	SRDebugService__ctor_m8BD64D72F6306594A1DFE62800B1619D13AD4D2A,
	SRDebugService_get_Settings_m1F9E06873331B7C225A47D8D4B8D99EB60984E0C,
	SRDebugService_get_IsDebugPanelVisible_mD37A15633FE95EE7B6D098A7BBB70F6BC0B90A64,
	SRDebugService_get_IsTriggerEnabled_m482DC371BE202425C2F7C116FC6A61ED602CDD59,
	SRDebugService_set_IsTriggerEnabled_m03A05870B80BFA49CA22C47F6BADEAEA9217A095,
	SRDebugService_get_IsProfilerDocked_mB787D6DF7A5CCA8B56841651CC0C30C99AC192B7,
	SRDebugService_set_IsProfilerDocked_m0EE3AECF3BD2EE07D7FC9C3781C58D02ABC8BD8D,
	SRDebugService_AddSystemInfo_mDF76B2555B16ABAD293461EC5EFD3413F52C2ACD,
	SRDebugService_ShowDebugPanel_m2FE6EA38C29C353AE81A18F447594B5FA1147916,
	SRDebugService_ShowDebugPanel_m5547603E4994D428626298CE4D5059317483C562,
	SRDebugService_HideDebugPanel_mBFDED4410FE80250B366FE2F571DEEB1A0C70304,
	SRDebugService_DestroyDebugPanel_m055DBC56C2D96DE26213CF9930A67B8EF49357C1,
	SRDebugService_AddOptionContainer_m216AF82CA40199235AD0989341D1C20368E91252,
	SRDebugService_RemoveOptionContainer_m40F5907824D92D1075F6817F4D8A74FA213FD26A,
	SRDebugService_PinAllOptions_m5B7454F34CD87C8A29358FD0E6949BFFDF8A6E70,
	SRDebugService_UnpinAllOptions_m969B4331B55DA1900F950CF75E7F4C217FE79179,
	SRDebugService_PinOption_m022BE092793F572CF9923AEF5FF3979643A9A53A,
	SRDebugService_UnpinOption_m46331E4E586608E3617183083FFD902BD8F23CA8,
	SRDebugService_ClearPinnedOptions_m496AA7FF5B224DC4687FC63062B14E4F20A17B25,
	SRDebugService_ShowBugReportSheet_m7A1EDF7B1439B1420E8BEFA86D4E6B1B5475B06C,
	SRDebugService_DebugPanelServiceOnVisibilityChanged_mDFB5D0FA0BFA5B45FA1693E4F21439F17C58267A,
	SRDebugService_PromptEntryCode_m55DF0E63918CBF15AAB34FEE352631076D119893,
	SRDebugService_EnableWorldSpaceMode_m1602A2E18C3661CE055C48F4F28685F13B56B3B6,
	SRDebugService_U3C_ctorU3Eb__17_0_m3D17959F155D1C3816B09321E4568D6D4C029012,
	SRDebugService_U3CPromptEntryCodeU3Eb__42_0_mBD7EBD5856B90F750517FA56B59BDB27946B34DF,
	StandardConsoleService__ctor_m530287CC47638982F26349C993F132E7C4E44F02,
	StandardConsoleService_get_ErrorCount_m686A6F50DF2B31AFBF7AFDAD4FF426D0E4448D03,
	StandardConsoleService_set_ErrorCount_m607C784CEC0D4A499772FE0BE8D187AA7D1A71A8,
	StandardConsoleService_get_WarningCount_mD54BD80FE59CD6EA189245E5B9AAF41AEE1FB64C,
	StandardConsoleService_set_WarningCount_mCB4D790A0F613A86EF4518BA6BF5B73140F86AE7,
	StandardConsoleService_get_InfoCount_m246BFCFE5A29277CA9CF45658EAF77999B76444D,
	StandardConsoleService_set_InfoCount_mE79F4A35D40F1385940787F19D68FE20404F986B,
	StandardConsoleService_add_Updated_m17690F25430063605BA953874310EE8BEB57A9DC,
	StandardConsoleService_remove_Updated_m078FABE27828C8460B88A8C0E61F71E2BEDEF227,
	StandardConsoleService_add_Error_m2985359698A913F2EB741A314981671086FAD89F,
	StandardConsoleService_remove_Error_mAA3501165A27586280300D1A7DFEEEFCC2A43D77,
	StandardConsoleService_get_Entries_mCCE676FAA6ED3C266A99E47D7E9F3C5976601247,
	StandardConsoleService_get_AllEntries_m6A607736C9C64094416EE86520BE8DE7BC4D672C,
	StandardConsoleService_Clear_mE6F1881331FFAFC542CF7D7D3D62E76F5F4A8A13,
	StandardConsoleService_OnEntryAdded_mC2A0A5D4539F7259BEB6464AEF9E8679D21E9319,
	StandardConsoleService_OnEntryDuplicated_mDDD73EF2613FE414A58D4CC8C5A561CD2E83C64D,
	StandardConsoleService_OnUpdated_mF42F9BF4E7F04B81F4D993B4F75774458AAAF9F4,
	StandardConsoleService_UnityLogCallback_mC3027AF4D31FF0CE7CD3DC9885AB3471F2D793A4,
	StandardConsoleService_AdjustCounter_mC6160E771BB18E1B38556F7A5FEFD00D22AB5121,
	StandardSystemInformationService__ctor_m64000316953C0B0F1853E5047E7C25E00DD2574F,
	StandardSystemInformationService_GetCategories_m0634CFDAEF61B3BCB1CA8515FA6F3B7BB2AE28A5,
	StandardSystemInformationService_GetInfo_mEB00B72DF8ED4A3EDCDB20C02A48E9AD9EDFB63A,
	StandardSystemInformationService_Add_m271AA79E87B565E1BD7098B61906F5724FCD31DE,
	StandardSystemInformationService_CreateReport_m31DBC644F19392010C35B9843A094448270F9065,
	StandardSystemInformationService_CreateDefaultSet_mF3E379DECFE4F47B92CCC12B587A5DC16E075313,
	StandardSystemInformationService_GetCloudManifestPrettyName_m7DF8EE9627B4A7AAD22978284AF5F17E44ED026B,
	ProfilerCameraListener_get_Stopwatch_m0FD664B474CA99FB8DA67ED1EC6D532D0A9E1C86,
	ProfilerCameraListener_get_Camera_mFA69193B2E9AA14E4ADCF32AEAA089EF31C8B845,
	ProfilerCameraListener_OnPreCull_mB5D80740377C2F135F5A7AF0A50CECB88243ABBD,
	ProfilerCameraListener_OnPostRender_m6EC85139D78AB0C2C245BE9723E186827A777B69,
	ProfilerCameraListener__ctor_m6894835ED312BE27D999DFABD6B8426CDB2A08B8,
	ProfilerLateUpdateListener_LateUpdate_mF47D1579B8CCE4F05BBEC438BD959BA96C05303D,
	ProfilerLateUpdateListener__ctor_mE9722A8D56B64E77EB0CA6B3B985271E3559A731,
	ProfilerServiceImpl_get_AverageFrameTime_mFD8B748BC6B288DFF94A367BA88DF17772939F0A,
	ProfilerServiceImpl_set_AverageFrameTime_m8B6A685560D468ABEEBCF01BB9E24D531E034948,
	ProfilerServiceImpl_get_LastFrameTime_m8D25FAC77621FD5905E623928FBE4482DCCBDF5B,
	ProfilerServiceImpl_set_LastFrameTime_mFE8CC70FA930ABAF52771A39DCCA456082B5F68E,
	ProfilerServiceImpl_get_FrameBuffer_mE767DD62B9DCC63EDB4E5D50FC373B221C2455F1,
	ProfilerServiceImpl_Awake_mCCA11B55A9ED09C9CD2D92DAEA59B874630BB3D3,
	ProfilerServiceImpl_Update_mED7C102958C5B0753CAAA197D296A99EC51F574C,
	ProfilerServiceImpl_PushFrame_m8509C4C17F917EB83FEF5F370F082680E032AF8B,
	ProfilerServiceImpl_OnLateUpdate_mC9AF1858BD89EB8CB3AF9065AD1585BA04EA870C,
	ProfilerServiceImpl_OnCameraPreRender_mC6E96895AA212856B0F0074F048A498905F89C95,
	ProfilerServiceImpl_OnCameraPostRender_m2C590E73C9E11B8A8D66C2F13B287390CBDC8B84,
	ProfilerServiceImpl_EndFrame_mB98FD6094CC7D7EFB5F0BB3B9FE7B3A0CED9613C,
	ProfilerServiceImpl__ctor_m394337F148D5AECF78280B24837446A9CAA943F9,
	SRPProfilerService_get_AverageFrameTime_mE7F0E1803BCAB4A65644C20DEC45DDF1BA4C1010,
	SRPProfilerService_set_AverageFrameTime_m0C3659A7BC2CA1090361DD4369054E67BF9A9452,
	SRPProfilerService_get_LastFrameTime_m247CDEF60F76D06EF7D2821B35A82D139FE4F87B,
	SRPProfilerService_set_LastFrameTime_m8347FD675025EF9A2F1F5CBE7C43948F012625E5,
	SRPProfilerService_get_FrameBuffer_mC8F76C4A43644A4624E54783CF1B2A963427D334,
	SRPProfilerService_Awake_mC7654AAECC4A13486A46397BB0DF63861265358D,
	SRPProfilerService_Update_mACA9C61C1A44D0C1890EA8734FE3E3BA7FCD8E99,
	SRPProfilerService_EndOfFrameCoroutine_mEC3EB4C3237E9F8C9A69A3ADCDFC8E351C0E317A,
	SRPProfilerService_PushFrame_m261ABCCA3BA419E0752A9DFDC7D5FBCDAB60FD38,
	SRPProfilerService_OnLateUpdate_mFE85887B967F826739555315F98138DA456835AA,
	SRPProfilerService_RenderPipelineOnBeginFrameRendering_mC197E4C8E75BD70642282C1D3C3FD29A3CCDF8DB,
	SRPProfilerService_EndFrame_mF87BC462987AD000FE0EB7C24A17D16BFA2FD41A,
	SRPProfilerService__ctor_m30B02CEE722710106E4430EAAC9D5D7C443034FA,
	SRDebugApiUtil_ParseErrorException_mCD44292F1916A3EF97B791528B82E67EBFDC3C80,
	SRDebugApiUtil_ParseErrorResponse_mF3E768D16B0F728E268ECB794D0100E7412807ED,
	SRDebugApiUtil_ReadResponse_m0332AD7A2AAC87AB90482649AEF55613DBA7C704,
	SRDebugApiUtil_ReadResponseStream_mB84508E3F5E4AD2DCAA5CEA56F0FABF2224A6D3D,
	BugReportApi__ctor_m7E3DB20C34AA7A620AEEF8D9360522C92C2AD5D1,
	BugReportApi_get_IsComplete_m239C9CD8871DD79CA112991A1D5B25211094757D,
	BugReportApi_set_IsComplete_m944FEE2334E36A65FFAE2FCAE8C40C85E942724A,
	BugReportApi_get_WasSuccessful_m5BD57961583B30E42E474AFEFAE7ED3C48DBA9ED,
	BugReportApi_set_WasSuccessful_m8B92B07ABB7CB78178B83D29EEC380DD8F21E0EC,
	BugReportApi_get_ErrorMessage_m9334F4D3E6FC7C1C35EA5D2165D7703BF9DA432A,
	BugReportApi_set_ErrorMessage_m82E621F2A7042725B16B1E5FC463FC0B7D769F24,
	BugReportApi_get_Progress_mAEC08E334E155C8478C1F72726094A286BEC9601,
	BugReportApi_Submit_m0B663F1CE5E41841C1959E20229D15F804DAEA46,
	BugReportApi_SetCompletionState_mAFBDD1265FB718F61517969E95D1AA075F33A00D,
	BugReportApi_BuildJsonRequest_m7D75087A6E530A14E96A8B5D00BF368209B66E31,
	BugReportApi_CreateConsoleDump_m5A497BE1AC79B7ED5A23C8DF1CA36BC999C4CAA9,
	BugReportScreenshotUtil_ScreenshotCaptureCo_m38453CD773E465E52A15516991BFFEAFF8DFE8DC,
	BugReportScreenshotUtil__ctor_mFC3DBD45259BCB5479026549D2137D0E97C0DC6C,
	OptionControlFactory_CreateControl_m93351447E169FC78CBEF0B3AEC8D03482425BA79,
	OptionControlFactory_CreateActionControl_m7252BDA25BDB6822EE49C126CD012CB4B9E88DBD,
	OptionControlFactory_CreateDataControl_m73BEF30F4D040F2644E9B8567E17CE6335257CAD,
	OptionDefinition__ctor_mC7C03BAE345D2D082C98F8F603EF6DCD2575533E,
	OptionDefinition__ctor_mC1524E4D94E76E7CBF534E6BAD197A763EF4415E,
	OptionDefinition__ctor_m32855307150D279A02BDE7C388D3E27BDFACBFC6,
	OptionDefinition_get_Name_m7AD2F87ECE626DFACE9496F3FBBBB5C66AFFCCC3,
	OptionDefinition_set_Name_m55D7259C2C01368D6138CCFF7B9F05EB6CA0F1F1,
	OptionDefinition_get_Category_m197FC276709EE8FE62F425D5C7A304369B13A3CA,
	OptionDefinition_set_Category_mFCBA432A8A7475EF10629B00D80E148711DFA173,
	OptionDefinition_get_SortPriority_mD6F59479B34FD332BC20AC19D76F1D8E7764D728,
	OptionDefinition_set_SortPriority_m2B27795D63EF5C771A1257D93F840C6F1CEDD0AF,
	OptionDefinition_get_Method_m1A3D5A8347F3949421CF5E8A5BF64CBA73373F89,
	OptionDefinition_set_Method_m11F6D1545717DEEBE949097DEB61AFF71D082318,
	OptionDefinition_get_Property_mE9F143EE8E54ED1B59E98E268E8A8D0B998583BD,
	OptionDefinition_set_Property_mA9CDB4C0D2DD1FC06E53219A1FF4C9530E18F10C,
	Service_get_Console_mB19C044101C12C39163410A4387EAB83C52EB4C4,
	Service_get_DockConsole_mE3AFF49073DAF614B94B9069FA4EE2BAC8612CAD,
	Service_get_Panel_mE2A411B809619F046769C5C711C7E3AA14FEBFBC,
	Service_get_Trigger_m6940331FAA8443427EAF83F198FBC317EE623BF6,
	Service_get_PinnedUI_mF5FBC9824EDB4FB71518874C74F940DA59ECB19B,
	Service_get_DebugCamera_mFF06ED8965E0711CCE8333EA3013461AADDA743C,
	Service_get_Options_m4217D5DC4725D7EA7792F80D2E7F9CFA114E1F32,
	SRDebugStrings__ctor_mEBF0511978EF7A52BF21BDF9D377A46B7AEBA38B,
	SRDebugStrings__cctor_m7878194242ADB8CC7D1555615D0601318A64BAEA,
	SRDebuggerUtil_get_IsMobilePlatform_m59D579F7172A6C80503A4C6EC492250810917FAB,
	SRDebuggerUtil_EnsureEventSystemExists_m4513F13DA57E73542A96631885ABA19F6A40AFE8,
	SRDebuggerUtil_CreateDefaultEventSystem_mF2C4F8FBA456827F81BD478B957137A48FC3669D,
	SRDebuggerUtil_ScanForOptions_mA54D2FDAE28B16CC3A9A6A43D6211616B3AF815C,
	SRDebuggerUtil_GetNumberString_m9664949DD262E7EE1285ABA15C2DE4CDE8A87F14,
	SRDebuggerUtil_ConfigureCanvas_m5A2075167FF767D281D75C7AF13EAE0E07A3A42C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass12_0__ctor_mEFC1F5BC444046D230C8D0DF60CF176E6A7ACD1C,
	U3CU3Ec__DisplayClass12_0_U3CCreateU3Eb__0_m3F7A54786AD5C080C1B47A667995ABFCED3766D1,
	KeyboardShortcut__ctor_mF0395A215A10A44B9D4CD248B9D976C6AE741AB6,
	U3CU3Ec__cctor_mAE49B2AC7DE99BCC5DBF12694D82B7417084B3DD,
	U3CU3Ec__ctor_m40AA1AC7E87A64EF869B11B9342D6FA4D009FDF7,
	U3CU3Ec_U3Cset_EntryCodeU3Eb__38_0_m18E0E7599D99188371E00D61E5CD7BAED90F4E26,
	CategoryInstance_get_CategoryGroup_m617396D57ADA63D1E62A7D36F5BDF30869DDBF5A,
	CategoryInstance_set_CategoryGroup_m45CFD343FAA9D6818999C43F3CE4888BE0CBD61D,
	CategoryInstance__ctor_mBBFD23909122498B12A84BE37928EFD4B514706B,
	U3CU3Ec__DisplayClass30_0__ctor_mD9B0B8A0BF21A4D5A7A339B6B264B2E3FD2E50AD,
	U3CU3Ec__DisplayClass30_0_U3CCreateCategoryU3Eb__1_mEC94D53BF187CB174E0924DC15EAD4BA23D53205,
	U3CU3Ec__cctor_m66400E3A3C8A177EFD4FC453119AFEF08F462D97,
	U3CU3Ec__ctor_mCEC5AD597573283C2B5BC0B07C79337BA300404B,
	U3CU3Ec_U3CCreateCategoryU3Eb__30_0_mB9258D008607638E5379AFD95A442154BAD2EADD,
	U3CSubmitCoU3Ed__18__ctor_mF45B81252BAC44E4DF687C08BC97A2CC12325FDD,
	U3CSubmitCoU3Ed__18_System_IDisposable_Dispose_m4BA0C12EC7C20064EC8909778631597684320483,
	U3CSubmitCoU3Ed__18_MoveNext_mACBC2798B96CB86064E3BD8ED2D6A207269F8C08,
	U3CSubmitCoU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7ACB779280A4258AE13AD75E74EFE3BEA81D4925,
	U3CSubmitCoU3Ed__18_System_Collections_IEnumerator_Reset_mA844CFAA67D2F18092B1D5D77C03DB50EC2BB3DB,
	U3CSubmitCoU3Ed__18_System_Collections_IEnumerator_get_Current_m6ECD4043E7FEA98340CAFF99CC771AF4BE5B445E,
	U3CU3Ec__DisplayClass15_0__ctor_m91A79DC13DADEB5929902581F1A25F1D89B30334,
	U3CU3Ec__DisplayClass15_0_U3CAddTabU3Eb__0_mF491332A356B43651576F7FEE1A0A3FBCA17EB10,
	U3CU3Ec__cctor_m6B161CAD8643D5749ECFDF1BB67EE33CC4935AEF,
	U3CU3Ec__ctor_m821A6AC8CED4E03754E8FE78DC03D41841E8C98F,
	U3CU3Ec_U3CSortTabsU3Eb__17_0_m86A2FCB5B43784E50F47B8DEC32E01F7091CE02C,
	U3CScrollToBottomU3Ed__26__ctor_mDDC76ACC285FD20F628A0031DF192DDA9F757B14,
	U3CScrollToBottomU3Ed__26_System_IDisposable_Dispose_mE7BA618EAEA24669FA3515E3B31FE9AB454D3ABB,
	U3CScrollToBottomU3Ed__26_MoveNext_m05277D9ED3B38CDDA9EFF8CA0B762FB778F34356,
	U3CScrollToBottomU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7531FA8FD37CF6A14371055FA2E62F4200B70675,
	U3CScrollToBottomU3Ed__26_System_Collections_IEnumerator_Reset_m13735294241FB36868C90E7052A971C71B8315F3,
	U3CScrollToBottomU3Ed__26_System_Collections_IEnumerator_get_Current_m2AB9BEB17195DBF1E7132E95D095D5DF5FA93F8E,
	U3CU3Ec__DisplayClass14_0__ctor_mBE7C7AA95BDB4B7260FDCBCDDDBDB8F4EC7BEF39,
	U3CU3Ec__DisplayClass14_0_U3CAwakeU3Eb__0_m3B63C5C0195D9DD9745CE448B64486E90590EE50,
	U3CCleanUpU3Ed__8__ctor_m09B6C0F80D2F1F8425779FE81DBA0A73E4CBF24E,
	U3CCleanUpU3Ed__8_System_IDisposable_Dispose_mC65EC5E736BEB7EE7B54BA925A29122D0C6ECEA3,
	U3CCleanUpU3Ed__8_MoveNext_mC3BBA058A823D5714AE6FC0BD54155D8FF653848,
	U3CCleanUpU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m36EA00BA737412310EEEB51E8C5DD716BD64B7BC,
	U3CCleanUpU3Ed__8_System_Collections_IEnumerator_Reset_mA0DA5EEB4CF6EB7EF7ECDCAA13161EFAAEAA6EF4,
	U3CCleanUpU3Ed__8_System_Collections_IEnumerator_get_Current_m58E7B5F723ECF83BEE77702E7F2D826C3E4C24E9,
	U3COpenCoU3Ed__7__ctor_m51C816BEEF8634CD74B9F8960EE2B4ABCD06E4FD,
	U3COpenCoU3Ed__7_System_IDisposable_Dispose_m3FCD595F400FC9580B795B941ECF449C26BEB1C7,
	U3COpenCoU3Ed__7_MoveNext_mF14B246559A29D251EDBBCD9DAA187C52533FC6D,
	U3COpenCoU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67D741D0A2A05E0F30A3771E1BBA6C1B2B2F54B9,
	U3COpenCoU3Ed__7_System_Collections_IEnumerator_Reset_m4B656BBC380C713A13B2CA27DD20CA075A1DF666,
	U3COpenCoU3Ed__7_System_Collections_IEnumerator_get_Current_mCFE72A97927EE324AAE5563DC85691ED6EB23D48,
	U3CU3Ec__DisplayClass40_0__ctor_m8BFE434EF5E1B8C05B6D8769DC73B842E2640D87,
	U3CU3Ec__DisplayClass40_0_U3CShowBugReportSheetU3Eb__0_m11D53F3B3C743D094EAAF5329A4537217000AB66,
	U3CU3Ec__DisplayClass4_0__ctor_m780DF290AFFDFA954688D1A6ABE1DA1B7D5AB789,
	U3CU3Ec__DisplayClass4_0_U3CAddU3Eb__0_m03489BF90553336C8E1FF206220316C67E29C0D4,
	U3CU3Ec__cctor_m311E1BD44127673B053E34144B070FDB5DE87DDC,
	U3CU3Ec__ctor_mA66E0704BCA5699CC1A44A787E425F53E90D74CF,
	U3CU3Ec_U3CCreateDefaultSetU3Eb__6_0_mCC31459404788CCA60E270CADACEFE01CA35FB73,
	U3CU3Ec_U3CCreateDefaultSetU3Eb__6_1_m26275796C79F6A0B76472D1653FF25A8D8B89FCA,
	U3CU3Ec_U3CCreateDefaultSetU3Eb__6_2_m90568DBB5580EBF208043C9EED38B5013AAF0B0B,
	U3CU3Ec_U3CCreateDefaultSetU3Eb__6_3_mC7D088CE326D7967B99FA6BEB028C03BEC01DFAA,
	U3CU3Ec_U3CCreateDefaultSetU3Eb__6_4_mF708BBFFB80B8C66C19B9822A9834084DFBF500B,
	U3CU3Ec_U3CCreateDefaultSetU3Eb__6_5_mFE074367C3F7A575692CA22607C475A6D6E410B6,
	U3CU3Ec_U3CCreateDefaultSetU3Eb__6_6_m6C3533D1FDC2BFF85D7713365A5851D865768C0F,
	U3CU3Ec_U3CCreateDefaultSetU3Eb__6_7_mF4CEA030F60C48E4121C0270F5FD9D82FF51D803,
	U3CEndOfFrameCoroutineU3Ed__19__ctor_m9BD6420C8E25B789010F049ABB2E30135F2D269B,
	U3CEndOfFrameCoroutineU3Ed__19_System_IDisposable_Dispose_m7C73699707AFF88E8713D9E0471E41266301A6D6,
	U3CEndOfFrameCoroutineU3Ed__19_MoveNext_m7652F6599D23FBEC6953F847D7EC329AF513C94B,
	U3CEndOfFrameCoroutineU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE9623B72061554E2CECFD9DF05B4697844FD54C,
	U3CEndOfFrameCoroutineU3Ed__19_System_Collections_IEnumerator_Reset_m6914F20DB646162870681825EA0E1F44A690E4EC,
	U3CEndOfFrameCoroutineU3Ed__19_System_Collections_IEnumerator_get_Current_m739E8570CD152353EDF9812BFA76667C5A041AD7,
	U3CSubmitU3Ed__19__ctor_mC42CEC4BB53FF5D50E50DAE4543DC9A003A52F13,
	U3CSubmitU3Ed__19_System_IDisposable_Dispose_mD7B1971A5C9BA13268BFAC1DB0ED5D40E06EB990,
	U3CSubmitU3Ed__19_MoveNext_mC9874B8C6419C852C81D6615A96641F6F0B1B9A9,
	U3CSubmitU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF0028B3AFF6CC14B02089A366B4B1CAEF575505,
	U3CSubmitU3Ed__19_System_Collections_IEnumerator_Reset_m12CBF9045F7AEF4972CD431C135E81E7C472084F,
	U3CSubmitU3Ed__19_System_Collections_IEnumerator_get_Current_m300F67CDAC762733A4D015C3D924F87D61CCE877,
	U3CScreenshotCaptureCoU3Ed__1__ctor_m14FA22D6D7EE02781AC3442B52F4D781C227FA73,
	U3CScreenshotCaptureCoU3Ed__1_System_IDisposable_Dispose_mB88E951136FC3F3DAE6D0A8DD71F24F8183A770B,
	U3CScreenshotCaptureCoU3Ed__1_MoveNext_m6A4A60572581017257180CAF5C6C69A7739D0E86,
	U3CScreenshotCaptureCoU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m625C4252957889AD96C5A5D520EBC3EAC84CD30E,
	U3CScreenshotCaptureCoU3Ed__1_System_Collections_IEnumerator_Reset_m15965BC6A9CB646CE9FCCB068BAD1E2C47608343,
	U3CScreenshotCaptureCoU3Ed__1_System_Collections_IEnumerator_get_Current_mF8DD6390E4D88778A77B5D8967BBA76C8F16F232,
	U3CU3Ec__DisplayClass4_0__ctor_mC70C83EB9067836394923435F9B04CDE6DBD983F,
	U3CU3Ec__DisplayClass4_0_U3CCreateDataControlU3Eb__0_m2E7643913F282E7189821D3A63A72CFD49DFA077,
};
static const int32_t s_InvokerIndices[833] = 
{
	4,
	3,
	3,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	1905,
	338,
	32,
	124,
	31,
	1356,
	26,
	124,
	31,
	1356,
	26,
	124,
	26,
	205,
	26,
	14,
	26,
	14,
	89,
	31,
	206,
	206,
	23,
	4,
	4,
	23,
	26,
	26,
	89,
	10,
	10,
	10,
	89,
	89,
	14,
	89,
	89,
	89,
	89,
	14,
	26,
	89,
	10,
	731,
	89,
	89,
	14,
	89,
	14,
	10,
	10,
	10,
	10,
	32,
	10,
	32,
	89,
	31,
	89,
	731,
	337,
	89,
	26,
	4,
	23,
	2431,
	23,
	30,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	731,
	731,
	23,
	23,
	23,
	26,
	23,
	23,
	27,
	23,
	23,
	23,
	89,
	23,
	23,
	23,
	23,
	23,
	31,
	26,
	459,
	31,
	23,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	23,
	31,
	31,
	31,
	23,
	23,
	27,
	28,
	23,
	23,
	459,
	27,
	27,
	23,
	23,
	23,
	459,
	31,
	23,
	23,
	23,
	31,
	459,
	23,
	27,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	89,
	31,
	23,
	23,
	23,
	14,
	337,
	88,
	31,
	23,
	27,
	23,
	31,
	14,
	26,
	23,
	89,
	31,
	89,
	31,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	2432,
	23,
	3,
	23,
	23,
	23,
	89,
	31,
	23,
	23,
	23,
	23,
	23,
	26,
	31,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	89,
	23,
	23,
	23,
	23,
	23,
	32,
	459,
	23,
	89,
	23,
	23,
	23,
	14,
	14,
	14,
	23,
	14,
	26,
	14,
	26,
	26,
	459,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	14,
	26,
	23,
	23,
	14,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	14,
	89,
	14,
	26,
	27,
	26,
	23,
	27,
	26,
	481,
	23,
	23,
	23,
	89,
	31,
	89,
	31,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	124,
	459,
	850,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1400,
	337,
	23,
	23,
	23,
	23,
	26,
	2433,
	2434,
	2435,
	2436,
	10,
	10,
	10,
	731,
	34,
	23,
	3,
	89,
	31,
	23,
	14,
	23,
	23,
	27,
	23,
	23,
	31,
	27,
	26,
	481,
	23,
	23,
	27,
	26,
	481,
	32,
	23,
	23,
	23,
	23,
	26,
	27,
	26,
	481,
	114,
	114,
	220,
	220,
	23,
	3,
	23,
	27,
	26,
	481,
	23,
	23,
	26,
	27,
	26,
	481,
	23,
	23,
	124,
	88,
	2256,
	26,
	124,
	337,
	2437,
	26,
	197,
	124,
	26,
	205,
	26,
	10,
	10,
	10,
	14,
	14,
	26,
	26,
	26,
	26,
	23,
	23,
	26,
	14,
	14,
	9,
	14,
	89,
	89,
	31,
	2431,
	26,
	26,
	23,
	32,
	14,
	89,
	89,
	31,
	14,
	89,
	31,
	27,
	31,
	133,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	126,
	26,
	26,
	26,
	26,
	14,
	89,
	31,
	10,
	32,
	89,
	31,
	89,
	31,
	10,
	32,
	26,
	26,
	26,
	26,
	14,
	26,
	26,
	26,
	124,
	31,
	1356,
	26,
	89,
	959,
	2438,
	26,
	26,
	26,
	26,
	89,
	31,
	130,
	26,
	23,
	9,
	4,
	731,
	731,
	14,
	14,
	28,
	27,
	123,
	197,
	23,
	337,
	23,
	23,
	23,
	89,
	126,
	908,
	88,
	23,
	2439,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	26,
	14,
	89,
	89,
	31,
	2431,
	32,
	23,
	23,
	23,
	89,
	31,
	10,
	32,
	23,
	23,
	23,
	26,
	23,
	23,
	1667,
	23,
	373,
	23,
	23,
	89,
	31,
	89,
	31,
	10,
	32,
	23,
	23,
	23,
	32,
	26,
	23,
	23,
	26,
	26,
	26,
	26,
	14,
	23,
	26,
	26,
	26,
	27,
	23,
	89,
	959,
	2438,
	23,
	23,
	459,
	26,
	23,
	14,
	26,
	26,
	26,
	26,
	89,
	31,
	130,
	26,
	459,
	23,
	9,
	23,
	23,
	23,
	23,
	27,
	27,
	31,
	23,
	23,
	14,
	26,
	26,
	26,
	26,
	23,
	14,
	89,
	89,
	31,
	89,
	31,
	27,
	31,
	133,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	126,
	459,
	23,
	14,
	26,
	31,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	26,
	26,
	26,
	26,
	14,
	14,
	23,
	26,
	26,
	23,
	118,
	129,
	23,
	14,
	28,
	27,
	123,
	23,
	0,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	731,
	337,
	731,
	337,
	14,
	23,
	23,
	2440,
	23,
	26,
	26,
	23,
	23,
	731,
	337,
	731,
	337,
	14,
	23,
	23,
	14,
	2440,
	23,
	1674,
	23,
	23,
	0,
	1,
	216,
	0,
	27,
	89,
	31,
	89,
	31,
	14,
	26,
	731,
	14,
	31,
	0,
	4,
	4,
	23,
	1,
	1,
	1,
	118,
	1313,
	1313,
	14,
	26,
	14,
	26,
	10,
	32,
	14,
	26,
	14,
	26,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	23,
	3,
	49,
	49,
	3,
	0,
	375,
	154,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	14,
	23,
	3,
	23,
	30,
	14,
	26,
	26,
	23,
	31,
	3,
	23,
	41,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	3,
	23,
	41,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	88,
	23,
	9,
	3,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	9,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x02000005, { 0, 21 } },
	{ 0x02000073, { 21, 7 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[28] = 
{
	{ (Il2CppRGCTXDataType)2, 32133 },
	{ (Il2CppRGCTXDataType)3, 26329 },
	{ (Il2CppRGCTXDataType)3, 26330 },
	{ (Il2CppRGCTXDataType)3, 26331 },
	{ (Il2CppRGCTXDataType)3, 26332 },
	{ (Il2CppRGCTXDataType)3, 26333 },
	{ (Il2CppRGCTXDataType)2, 35032 },
	{ (Il2CppRGCTXDataType)3, 26334 },
	{ (Il2CppRGCTXDataType)3, 26335 },
	{ (Il2CppRGCTXDataType)3, 26336 },
	{ (Il2CppRGCTXDataType)3, 26337 },
	{ (Il2CppRGCTXDataType)3, 26338 },
	{ (Il2CppRGCTXDataType)3, 26339 },
	{ (Il2CppRGCTXDataType)2, 35033 },
	{ (Il2CppRGCTXDataType)3, 26340 },
	{ (Il2CppRGCTXDataType)3, 26341 },
	{ (Il2CppRGCTXDataType)3, 26342 },
	{ (Il2CppRGCTXDataType)3, 26343 },
	{ (Il2CppRGCTXDataType)3, 26344 },
	{ (Il2CppRGCTXDataType)2, 32136 },
	{ (Il2CppRGCTXDataType)3, 26345 },
	{ (Il2CppRGCTXDataType)2, 35034 },
	{ (Il2CppRGCTXDataType)3, 26346 },
	{ (Il2CppRGCTXDataType)3, 26347 },
	{ (Il2CppRGCTXDataType)3, 26348 },
	{ (Il2CppRGCTXDataType)3, 26349 },
	{ (Il2CppRGCTXDataType)3, 26350 },
	{ (Il2CppRGCTXDataType)2, 32140 },
};
extern const Il2CppCodeGenModule g_StompyRobot_SRDebuggerCodeGenModule;
const Il2CppCodeGenModule g_StompyRobot_SRDebuggerCodeGenModule = 
{
	"StompyRobot.SRDebugger.dll",
	833,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	28,
	s_rgctxValues,
	NULL,
};
