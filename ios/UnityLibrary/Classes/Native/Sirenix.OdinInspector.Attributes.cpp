﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// Sirenix.OdinInspector.AssetListAttribute
struct AssetListAttribute_tE254CEED1A60ABA5995D73442A2A7272B477DA55;
// Sirenix.OdinInspector.AssetSelectorAttribute
struct AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5;
// Sirenix.OdinInspector.AssetSelectorAttribute/<>c
struct U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5;
// Sirenix.OdinInspector.AssetsOnlyAttribute
struct AssetsOnlyAttribute_t2F7CC8E0514CB2767616E03312FD23EF8FA03D63;
// Sirenix.OdinInspector.BoxGroupAttribute
struct BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283;
// Sirenix.OdinInspector.ButtonAttribute
struct ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381;
// Sirenix.OdinInspector.ButtonGroupAttribute
struct ButtonGroupAttribute_tCDADB0FE0D3E9BFA27DF33D6451869B8EC8E857C;
// Sirenix.OdinInspector.ChildGameObjectsOnlyAttribute
struct ChildGameObjectsOnlyAttribute_t64A28A6D4B2AAE55BA804CDFC266C0A8681BD920;
// Sirenix.OdinInspector.ColorPaletteAttribute
struct ColorPaletteAttribute_t933AEAC8970C941B1074D80041861812D7170DA4;
// Sirenix.OdinInspector.CustomContextMenuAttribute
struct CustomContextMenuAttribute_tB2D58375FEB292B02B5442B5F1282A7796A4B007;
// Sirenix.OdinInspector.CustomValueDrawerAttribute
struct CustomValueDrawerAttribute_tF14F13C43F3B695A6A04C52ABC0BCED76D8D4278;
// Sirenix.OdinInspector.DelayedPropertyAttribute
struct DelayedPropertyAttribute_t63A7B3586325CC6B399DA0D36EBF16D3905EE679;
// Sirenix.OdinInspector.DetailedInfoBoxAttribute
struct DetailedInfoBoxAttribute_tCBB726C7E8955A4F2261373DF09951C49BB79F97;
// Sirenix.OdinInspector.DictionaryDrawerSettings
struct DictionaryDrawerSettings_t6F42380B0928BCFD6B9B3CFF92D3558518CDD011;
// Sirenix.OdinInspector.DisableContextMenuAttribute
struct DisableContextMenuAttribute_t69DDDBD94727983567A3793C3CFCFA00814E6DC2;
// Sirenix.OdinInspector.DisableIfAttribute
struct DisableIfAttribute_t44366AD5F4EF373AF85E952BEA715D5A40715377;
// Sirenix.OdinInspector.DisableInEditorModeAttribute
struct DisableInEditorModeAttribute_t07461DBCB20EE922ABC319603E5954C3F00CBA44;
// Sirenix.OdinInspector.DisableInInlineEditorsAttribute
struct DisableInInlineEditorsAttribute_t5E128B9CE96EC7F787672A3C8950E055F4022110;
// Sirenix.OdinInspector.DisableInNonPrefabsAttribute
struct DisableInNonPrefabsAttribute_t0D00848E037C1AAAB99D462E6FF885078A300D7E;
// Sirenix.OdinInspector.DisableInPlayModeAttribute
struct DisableInPlayModeAttribute_t9E36813C99F4DC97259581CB1CE03B0F174B32BA;
// Sirenix.OdinInspector.DisableInPrefabAssetsAttribute
struct DisableInPrefabAssetsAttribute_t461A48998B4E32C9C110A5641BF71D57484DF76D;
// Sirenix.OdinInspector.DisableInPrefabInstancesAttribute
struct DisableInPrefabInstancesAttribute_tBDF4782E7ED0010E287CBE47AB4EFCDC3D2C915C;
// Sirenix.OdinInspector.DisableInPrefabsAttribute
struct DisableInPrefabsAttribute_tC5A57835853CBCBAFF19B5678BEAA7E5A050EE9B;
// Sirenix.OdinInspector.DisplayAsStringAttribute
struct DisplayAsStringAttribute_t677D6873B0CED543705D9C9D9C7C02108AE705E9;
// Sirenix.OdinInspector.DoNotDrawAsReferenceAttribute
struct DoNotDrawAsReferenceAttribute_tB5A4B9DEAF2766E27A1DB535A8A65C3C37E12C47;
// Sirenix.OdinInspector.DontApplyToListElementsAttribute
struct DontApplyToListElementsAttribute_tC34D036BA277A188748A5E8D6C5C2FEB90EBF666;
// Sirenix.OdinInspector.DrawWithUnityAttribute
struct DrawWithUnityAttribute_t3995454B2F87DDEE9CDF30C1E9674EE65F1A5961;
// Sirenix.OdinInspector.EnableForPrefabOnlyAttribute
struct EnableForPrefabOnlyAttribute_t097E39FDE22B623C6201BBEEEF3278A093AE0648;
// Sirenix.OdinInspector.EnableGUIAttribute
struct EnableGUIAttribute_t0A76255CB3C8DADBA1259980D6551ABF6A91E4A2;
// Sirenix.OdinInspector.EnableIfAttribute
struct EnableIfAttribute_t28619931329D231822A79B006B44F096DE437E9A;
// Sirenix.OdinInspector.EnumPagingAttribute
struct EnumPagingAttribute_t1FEB069556F6126E3D05095600132A589FB0F135;
// Sirenix.OdinInspector.EnumToggleButtonsAttribute
struct EnumToggleButtonsAttribute_tCE472D5DA47F333E38C7537FDF0776B73659E2DE;
// Sirenix.OdinInspector.FilePathAttribute
struct FilePathAttribute_tB6DD8A50FC2A4C8ED0FD8FB5158A2BBB47AC045F;
// Sirenix.OdinInspector.FolderPathAttribute
struct FolderPathAttribute_t63E1631A434EE753FCD86E7273812B2E89FFDF94;
// Sirenix.OdinInspector.FoldoutGroupAttribute
struct FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD;
// Sirenix.OdinInspector.GUIColorAttribute
struct GUIColorAttribute_tDE7A1E234D4277B5079B9921967945DB5CA56B03;
// Sirenix.OdinInspector.HideDuplicateReferenceBoxAttribute
struct HideDuplicateReferenceBoxAttribute_t91004BA0AB0CD17A308E7FE988D0038FB00150E5;
// Sirenix.OdinInspector.HideIfAttribute
struct HideIfAttribute_t011B476D6F3C9156B1886271EFB256C8EC3D76FA;
// Sirenix.OdinInspector.HideIfGroupAttribute
struct HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238;
// Sirenix.OdinInspector.HideInEditorModeAttribute
struct HideInEditorModeAttribute_t65C98A206B4DC8AC9240C5546D0A6D3827CE9E57;
// Sirenix.OdinInspector.HideInInlineEditorsAttribute
struct HideInInlineEditorsAttribute_t177610D85D2123274AD4EA5FE2C8A7DFF7D2240C;
// Sirenix.OdinInspector.HideInNonPrefabsAttribute
struct HideInNonPrefabsAttribute_tBD5D04C53C1C275F341CD8AD160EFADA158427C0;
// Sirenix.OdinInspector.HideInPlayModeAttribute
struct HideInPlayModeAttribute_t627CDE4C0FADA1E6E9197842268B6077ED7004FC;
// Sirenix.OdinInspector.HideInPrefabAssetsAttribute
struct HideInPrefabAssetsAttribute_t7BB906033FB4DF85FABE31388AF559725E98ED77;
// Sirenix.OdinInspector.HideInPrefabInstancesAttribute
struct HideInPrefabInstancesAttribute_t7148B73699DA6BE500EF82AE3223E5C89DC4D98F;
// Sirenix.OdinInspector.HideInPrefabsAttribute
struct HideInPrefabsAttribute_t28D32D1AD69258DF301288D58E42CD5E14FDABAA;
// Sirenix.OdinInspector.HideInTablesAttribute
struct HideInTablesAttribute_tCC2FB41AEC83E4CA159714F7238B51FD9AC5D546;
// Sirenix.OdinInspector.HideLabelAttribute
struct HideLabelAttribute_t451BC6D3C8B4E1C9A9E75D4C1F308C3ED301C71F;
// Sirenix.OdinInspector.HideMonoScriptAttribute
struct HideMonoScriptAttribute_t8EF9302A9377F51F515B10BB0AB16844636A9599;
// Sirenix.OdinInspector.HideNetworkBehaviourFieldsAttribute
struct HideNetworkBehaviourFieldsAttribute_tCD095386566090284A9A9E3806805FF73B9E83F3;
// Sirenix.OdinInspector.HideReferenceObjectPickerAttribute
struct HideReferenceObjectPickerAttribute_tE06BCAACEB5B5CA565BA3BC25EA11D7EA22C6F28;
// Sirenix.OdinInspector.HorizontalGroupAttribute
struct HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5;
// Sirenix.OdinInspector.IncludeMyAttributesAttribute
struct IncludeMyAttributesAttribute_tFCF24E3A971A142A66755F0C43C6B140D1D55327;
// Sirenix.OdinInspector.IndentAttribute
struct IndentAttribute_t8DB2B60FF1214619E8901509CE31C77EA2CD6DE8;
// Sirenix.OdinInspector.InfoBoxAttribute
struct InfoBoxAttribute_t4134D5269F4439BB4423FC022453606650597B3A;
// Sirenix.OdinInspector.InlineButtonAttribute
struct InlineButtonAttribute_t4E0014F9C6D3D55B24D12C714E8E35E4364B1AB6;
// Sirenix.OdinInspector.InlineEditorAttribute
struct InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216;
// Sirenix.OdinInspector.InlinePropertyAttribute
struct InlinePropertyAttribute_t7094D1D53D31ACA38BD2F40D93D24BB00ADFD6FA;
// Sirenix.OdinInspector.LabelTextAttribute
struct LabelTextAttribute_t05B2F25125BFF01D694EC3DBED7B9B045B16ACC9;
// Sirenix.OdinInspector.LabelWidthAttribute
struct LabelWidthAttribute_tD46C5A741BE80B8CA6CFC7DAB33E13508A88FF6C;
// Sirenix.OdinInspector.ListDrawerSettingsAttribute
struct ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D;
// Sirenix.OdinInspector.MaxValueAttribute
struct MaxValueAttribute_t7C88F52D1B04C874C07D6E937F2C83CAED093185;
// Sirenix.OdinInspector.MinMaxSliderAttribute
struct MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461;
// Sirenix.OdinInspector.MinValueAttribute
struct MinValueAttribute_t69E9653BC5DF176433CF88D0A72F150A73DDAFCD;
// Sirenix.OdinInspector.MultiLinePropertyAttribute
struct MultiLinePropertyAttribute_t6C8A6AE59171CD7C3D77EE421C7FDD48D846D4CD;
// Sirenix.OdinInspector.OdinRegisterAttributeAttribute
struct OdinRegisterAttributeAttribute_tF3FE293A763B5D61CA1902CF48AAFEAC64543A3F;
// Sirenix.OdinInspector.OnCollectionChangedAttribute
struct OnCollectionChangedAttribute_tA3CC1CAE8C5A0C81E25B52D82D049EFE8615C73C;
// Sirenix.OdinInspector.OnInspectorDisposeAttribute
struct OnInspectorDisposeAttribute_t9CF5B6A832469F06C52D7EDC96C3F547A6989BDF;
// Sirenix.OdinInspector.OnInspectorGUIAttribute
struct OnInspectorGUIAttribute_t82649BAD99432C3B20C66F0126D74AE5FAA2AE55;
// Sirenix.OdinInspector.OnInspectorInitAttribute
struct OnInspectorInitAttribute_t7564436864B4C2ABC648107C388558D26170EB85;
// Sirenix.OdinInspector.OnStateUpdateAttribute
struct OnStateUpdateAttribute_tA9908491BF4CB898A9F306ADC408624E9B34A19A;
// Sirenix.OdinInspector.OnValueChangedAttribute
struct OnValueChangedAttribute_t833863276762F1AA2950B2A58F560B381E73940C;
// Sirenix.OdinInspector.PreviewFieldAttribute
struct PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96;
// Sirenix.OdinInspector.ProgressBarAttribute
struct ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB;
// Sirenix.OdinInspector.PropertyGroupAttribute
struct PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310;
// Sirenix.OdinInspector.PropertyGroupAttribute[]
struct PropertyGroupAttributeU5BU5D_tED02C908838ADCEFF5D1EC3E81EAE74AAA409F68;
// Sirenix.OdinInspector.PropertyOrderAttribute
struct PropertyOrderAttribute_t142EA22A39E39E022D9DBB6BDFD6649CAD914996;
// Sirenix.OdinInspector.PropertyRangeAttribute
struct PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F;
// Sirenix.OdinInspector.PropertySpaceAttribute
struct PropertySpaceAttribute_tB1663AC8EFAC09BE967F6C9EA8A4E04FA39B9A47;
// Sirenix.OdinInspector.PropertyTooltipAttribute
struct PropertyTooltipAttribute_tB379999F5A87587FF4F62D7F604CC275FCB76DB5;
// Sirenix.OdinInspector.ReadOnlyAttribute
struct ReadOnlyAttribute_t993E550B14FD6CBF965160564F37009B72C9F8D4;
// Sirenix.OdinInspector.RequiredAttribute
struct RequiredAttribute_t233038081C694B76DACAFC3BA8624900426D42B0;
// Sirenix.OdinInspector.ResponsiveButtonGroupAttribute
struct ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848;
// Sirenix.OdinInspector.SceneObjectsOnlyAttribute
struct SceneObjectsOnlyAttribute_tE48AE7C0CE377268490681FAD4EC7108D742DB19;
// Sirenix.OdinInspector.SearchableAttribute
struct SearchableAttribute_t36B1B00CA28BBBC11E1E354D4A0F50BAD302BB5A;
// Sirenix.OdinInspector.ShowDrawerChainAttribute
struct ShowDrawerChainAttribute_tBF48731E167BC72578A8E8FEC742BAB51E993694;
// Sirenix.OdinInspector.ShowForPrefabOnlyAttribute
struct ShowForPrefabOnlyAttribute_t52839F490AF48EB0AD275E18D1F2A81392FC4BC5;
// Sirenix.OdinInspector.ShowIfAttribute
struct ShowIfAttribute_tBC21F2EA58071ED9C4DBA0E5AA6967FF19C543C2;
// Sirenix.OdinInspector.ShowIfGroupAttribute
struct ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549;
// Sirenix.OdinInspector.ShowInInlineEditorsAttribute
struct ShowInInlineEditorsAttribute_t51DF69747A3CE1570890953F2B8536E8082867CD;
// Sirenix.OdinInspector.ShowInInspectorAttribute
struct ShowInInspectorAttribute_t035C9790B6F7942286B0D6607580AB3393B1115B;
// Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute
struct ShowOdinSerializedPropertiesInInspectorAttribute_t85D03D92F6E429EE8CA403011C588CC9AD651DFE;
// Sirenix.OdinInspector.ShowPropertyResolverAttribute
struct ShowPropertyResolverAttribute_t3C66CA8BD48FC58876E4DC2633C0D4B5B660C633;
// Sirenix.OdinInspector.SuffixLabelAttribute
struct SuffixLabelAttribute_t7B942FEB352DA81BAE4FE6C86A26838563BE3942;
// Sirenix.OdinInspector.SuppressInvalidAttributeErrorAttribute
struct SuppressInvalidAttributeErrorAttribute_tD041EA2EC15B10F0A0E7B48E887B652EE6C6C427;
// Sirenix.OdinInspector.TabGroupAttribute
struct TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED;
// Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute
struct TabSubGroupAttribute_t163D9E025CE046202E35327D9858BA138474EED6;
// Sirenix.OdinInspector.TableColumnWidthAttribute
struct TableColumnWidthAttribute_t45DDD415E5FB012D5979A1A163D214EFF48FDB2A;
// Sirenix.OdinInspector.TableListAttribute
struct TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5;
// Sirenix.OdinInspector.TableMatrixAttribute
struct TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C;
// Sirenix.OdinInspector.TitleAttribute
struct TitleAttribute_t03734F3839AB42F82F5DD8EA32BD8ECB79F567CC;
// Sirenix.OdinInspector.TitleGroupAttribute
struct TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A;
// Sirenix.OdinInspector.ToggleAttribute
struct ToggleAttribute_t4612A2D02256B1199715ACC4C56E742576D3C6B5;
// Sirenix.OdinInspector.ToggleGroupAttribute
struct ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5;
// Sirenix.OdinInspector.ToggleLeftAttribute
struct ToggleLeftAttribute_t6E6A2BD826555DA4871D9BE2A6BB0CA822D7D525;
// Sirenix.OdinInspector.TypeFilterAttribute
struct TypeFilterAttribute_t367745A090DA5E0ED152768927A09BE02DE708E8;
// Sirenix.OdinInspector.TypeInfoBoxAttribute
struct TypeInfoBoxAttribute_t884BA53D12B62C5C7E9F1AA20525F4D2898A16E8;
// Sirenix.OdinInspector.ValidateInputAttribute
struct ValidateInputAttribute_tDC0CF1D228B37E27B6AFC85476E9EC04CBFC2E6D;
// Sirenix.OdinInspector.ValueDropdownAttribute
struct ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478;
// Sirenix.OdinInspector.VerticalGroupAttribute
struct VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420;
// Sirenix.OdinInspector.WrapAttribute
struct WrapAttribute_t1359861280D8003582A215C7DF8F794A72B60437;
// System.ArgumentException
struct ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1;
// System.ArgumentNullException
struct ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2F75FCBEC68AFE08982DA43985F9D04056E2BE73;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t31EF1520A3A805598500BB6033C14ABDA7116D5E;
// System.Collections.Generic.IList`1<Sirenix.OdinInspector.PropertyGroupAttribute>
struct IList_1_t57C39ABE7458C0B2F927FD424CF3821AEDFF97E5;
// System.Collections.Generic.List`1<Sirenix.OdinInspector.PropertyGroupAttribute>
struct List_1_tEA3588639D49099355676AB395F7428E02EFB383;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`2<System.Object,System.Object>
struct Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4;
// System.Func`2<System.String,System.String>
struct Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.NotImplementedException
struct NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

IL2CPP_EXTERN_C RuntimeClass* ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tEA3588639D49099355676AB395F7428E02EFB383_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TabSubGroupAttribute_t163D9E025CE046202E35327D9858BA138474EED6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral05FC6F56975CF45104FFBC8B64CA1534C38AD0F9;
IL2CPP_EXTERN_C String_t* _stringLiteral2BE88CA4242C76E8253AC62474851065032D6833;
IL2CPP_EXTERN_C String_t* _stringLiteral3D52B3CB7FE05D311C52AD3EA343EAE34ABDEACD;
IL2CPP_EXTERN_C String_t* _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8;
IL2CPP_EXTERN_C String_t* _stringLiteral5C10B5B2CD673A0616D529AA5234B12EE7153808;
IL2CPP_EXTERN_C String_t* _stringLiteral7BC503D8B9F755E76AE4D6FB13E223FBA124E957;
IL2CPP_EXTERN_C String_t* _stringLiteral8DCE170DE238B1FEDA2ECD9674EA3CA0D068FBCB;
IL2CPP_EXTERN_C String_t* _stringLiteralA61CECD51CDCE38AA6025799993B421942C72EC7;
IL2CPP_EXTERN_C String_t* _stringLiteralC67DD20EE842986086A0A915DDC2A279490130ED;
IL2CPP_EXTERN_C String_t* _stringLiteralC8E20896D265CDCB3B545CCC0D79FBBB6F7F2F3E;
IL2CPP_EXTERN_C String_t* _stringLiteralD0941E68DA8F38151FF86A61FC59F7C5CF9FCAA2;
IL2CPP_EXTERN_C String_t* _stringLiteralD2E5419C223893DD079C1E7DFFE254D8D03B6B36;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisString_t_TisString_t_m96ED0707DFF4FB4E203C941F8BC60B13F18F5A72_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisString_t_m1BAD76FB02571EB3CCC5BB00D4D581399F8523FC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m6CD2F1158D4A0D24923FBB9479F1A0E987B98F2F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m11D1A253155BD0CBF9B0E59ECEA4F7EEBF86C09F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m73A2CF304FA1FEDE0FB84D5A90287D398804BCFE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_mC276B952F432851DB174123FCF79CFABDF753BB0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InlineEditorAttribute__ctor_m2C122F43FF3CA6D75809623A950AAA6B2BC3939D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m22BEC6E130081220F517BD7875962489A6478DD7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_mC1D01A0D94C03E4225EEF9D6506D7D91C6976B7B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m79AA93F4A95A51200A8FC7433950476FD84D83B7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m32270ECBE19DDAA80F0E83559F63FDAD74A5C328_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mE9FDDA3E872C3CB2DBDC8562E9ABA76CA3124599_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m4151A68BD4CB1D737213E7595F574987F8C812B4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyGroupAttribute_Combine_mE33D3E9250CECE2DE657D70D5B9041720362481D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3Cset_PathsU3Eb__12_0_m8675AB5D0D72FC737F62615297D2D030A0DC8AB1_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t AssetSelectorAttribute_get_Paths_mEC8A7AD9115F4A88436030423B8D0D3947E584CA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AssetSelectorAttribute_set_Paths_m76AA7C29F24AED85D381A5DDA6C2E7008BFBB2FF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t BoxGroupAttribute_CombineValuesWith_m79BABBFF9A0B58F8A15369C25F243BFA76128A96_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t BoxGroupAttribute__ctor_m3735DB49E46EE2E2AE8AE5415D6C9FA0A93233F2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t DictionaryDrawerSettings__ctor_mCD32097B13EC46AEFE9ED48441540D7C131140D5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FoldoutGroupAttribute_CombineValuesWith_m190C03737485283118946483BC9A40A3256FB6DB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HideIfGroupAttribute_CombineValuesWith_m75EBFB293E2957D9B6F2C26E79950B90E6ECCBC5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HorizontalGroupAttribute_CombineValuesWith_m358F04BFE0348EA6FFE6FDF75342FAA1EB65014A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HorizontalGroupAttribute__ctor_mDE8859B5F8E6ACA485847210F51CE37DDE2531A1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InlineEditorAttribute__ctor_m2C122F43FF3CA6D75809623A950AAA6B2BC3939D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MultiLinePropertyAttribute__ctor_mEE7A041CC0A205936F9DA935875F921CE7757D92_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PropertyGroupAttribute_Combine_mE33D3E9250CECE2DE657D70D5B9041720362481D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ResponsiveButtonGroupAttribute_CombineValuesWith_m0C46A0FFC4BF1DC38EA47EC0D6E68DAB2FD3EDA4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ShowIfGroupAttribute_CombineValuesWith_m4D6EAA8F018088F641E92D46A9DD2919BF79CDE0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TabGroupAttribute_CombineValuesWith_m03FB48456237C348D28C3AC330774077BC4AA84E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_GetSubGroupAttributes_m86FAA7D41551FA38170739A1C4375482F5F0EAF0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_RepathMemberAttribute_m85A61D8AD679C3E6A58CAED8A389F21E92B0CE1F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TabGroupAttribute__ctor_m3FDCD07DF133DF94BC3015B62269140288334C53_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TabGroupAttribute__ctor_m44DBA7BC5910270727E85756FB81AEDA69E277FE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TableListAttribute_get_ScrollViewHeight_m8CD7BCAFCF875A2EEDC3B77D27234D748AB0DAAF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TitleAttribute__ctor_m77110B723E6BA8ED4C8EBC8948FC75001867ECD4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TitleGroupAttribute_CombineValuesWith_mD450E1372EF1FD6291F2E9150EF83E074847A679_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ToggleGroupAttribute_CombineValuesWith_m592F8AF767D119CBD03D064A90A1777E1A0F5C63_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3Cset_PathsU3Eb__12_0_m8675AB5D0D72FC737F62615297D2D030A0DC8AB1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_m898AC2ABD1D3662F4F6DAC7C5C5E904834908795_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_com_FromNativeMethodDefinition_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VerticalGroupAttribute_CombineValuesWith_m983E6DD2C5697D4CCFFA5F58C29C6F93828F06BE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VerticalGroupAttribute__ctor_m655FA45ED58C1BA24F2011314508B7E40FFC69CB_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t0371F7EAB81F04D3FE805B1CE0618C2B1BB6FAD1 
{
public:

public:
};


// System.Object


// Sirenix.OdinInspector.AssetSelectorAttribute/<>c
struct U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_StaticFields
{
public:
	// Sirenix.OdinInspector.AssetSelectorAttribute/<>c Sirenix.OdinInspector.AssetSelectorAttribute/<>c::<>9
	U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String> Sirenix.OdinInspector.AssetSelectorAttribute/<>c::<>9__12_0
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * ___U3CU3E9__12_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__12_0_1), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};


// System.Collections.Generic.List`1<Sirenix.OdinInspector.PropertyGroupAttribute>
struct List_1_tEA3588639D49099355676AB395F7428E02EFB383  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PropertyGroupAttributeU5BU5D_tED02C908838ADCEFF5D1EC3E81EAE74AAA409F68* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tEA3588639D49099355676AB395F7428E02EFB383, ____items_1)); }
	inline PropertyGroupAttributeU5BU5D_tED02C908838ADCEFF5D1EC3E81EAE74AAA409F68* get__items_1() const { return ____items_1; }
	inline PropertyGroupAttributeU5BU5D_tED02C908838ADCEFF5D1EC3E81EAE74AAA409F68** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PropertyGroupAttributeU5BU5D_tED02C908838ADCEFF5D1EC3E81EAE74AAA409F68* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tEA3588639D49099355676AB395F7428E02EFB383, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tEA3588639D49099355676AB395F7428E02EFB383, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tEA3588639D49099355676AB395F7428E02EFB383, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tEA3588639D49099355676AB395F7428E02EFB383_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	PropertyGroupAttributeU5BU5D_tED02C908838ADCEFF5D1EC3E81EAE74AAA409F68* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tEA3588639D49099355676AB395F7428E02EFB383_StaticFields, ____emptyArray_5)); }
	inline PropertyGroupAttributeU5BU5D_tED02C908838ADCEFF5D1EC3E81EAE74AAA409F68* get__emptyArray_5() const { return ____emptyArray_5; }
	inline PropertyGroupAttributeU5BU5D_tED02C908838ADCEFF5D1EC3E81EAE74AAA409F68** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(PropertyGroupAttributeU5BU5D_tED02C908838ADCEFF5D1EC3E81EAE74AAA409F68* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____items_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// Sirenix.OdinInspector.AssetListAttribute
struct AssetListAttribute_tE254CEED1A60ABA5995D73442A2A7272B477DA55  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Sirenix.OdinInspector.AssetListAttribute::AutoPopulate
	bool ___AutoPopulate_0;
	// System.String Sirenix.OdinInspector.AssetListAttribute::Tags
	String_t* ___Tags_1;
	// System.String Sirenix.OdinInspector.AssetListAttribute::LayerNames
	String_t* ___LayerNames_2;
	// System.String Sirenix.OdinInspector.AssetListAttribute::AssetNamePrefix
	String_t* ___AssetNamePrefix_3;
	// System.String Sirenix.OdinInspector.AssetListAttribute::Path
	String_t* ___Path_4;
	// System.String Sirenix.OdinInspector.AssetListAttribute::CustomFilterMethod
	String_t* ___CustomFilterMethod_5;

public:
	inline static int32_t get_offset_of_AutoPopulate_0() { return static_cast<int32_t>(offsetof(AssetListAttribute_tE254CEED1A60ABA5995D73442A2A7272B477DA55, ___AutoPopulate_0)); }
	inline bool get_AutoPopulate_0() const { return ___AutoPopulate_0; }
	inline bool* get_address_of_AutoPopulate_0() { return &___AutoPopulate_0; }
	inline void set_AutoPopulate_0(bool value)
	{
		___AutoPopulate_0 = value;
	}

	inline static int32_t get_offset_of_Tags_1() { return static_cast<int32_t>(offsetof(AssetListAttribute_tE254CEED1A60ABA5995D73442A2A7272B477DA55, ___Tags_1)); }
	inline String_t* get_Tags_1() const { return ___Tags_1; }
	inline String_t** get_address_of_Tags_1() { return &___Tags_1; }
	inline void set_Tags_1(String_t* value)
	{
		___Tags_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Tags_1), (void*)value);
	}

	inline static int32_t get_offset_of_LayerNames_2() { return static_cast<int32_t>(offsetof(AssetListAttribute_tE254CEED1A60ABA5995D73442A2A7272B477DA55, ___LayerNames_2)); }
	inline String_t* get_LayerNames_2() const { return ___LayerNames_2; }
	inline String_t** get_address_of_LayerNames_2() { return &___LayerNames_2; }
	inline void set_LayerNames_2(String_t* value)
	{
		___LayerNames_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LayerNames_2), (void*)value);
	}

	inline static int32_t get_offset_of_AssetNamePrefix_3() { return static_cast<int32_t>(offsetof(AssetListAttribute_tE254CEED1A60ABA5995D73442A2A7272B477DA55, ___AssetNamePrefix_3)); }
	inline String_t* get_AssetNamePrefix_3() const { return ___AssetNamePrefix_3; }
	inline String_t** get_address_of_AssetNamePrefix_3() { return &___AssetNamePrefix_3; }
	inline void set_AssetNamePrefix_3(String_t* value)
	{
		___AssetNamePrefix_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AssetNamePrefix_3), (void*)value);
	}

	inline static int32_t get_offset_of_Path_4() { return static_cast<int32_t>(offsetof(AssetListAttribute_tE254CEED1A60ABA5995D73442A2A7272B477DA55, ___Path_4)); }
	inline String_t* get_Path_4() const { return ___Path_4; }
	inline String_t** get_address_of_Path_4() { return &___Path_4; }
	inline void set_Path_4(String_t* value)
	{
		___Path_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Path_4), (void*)value);
	}

	inline static int32_t get_offset_of_CustomFilterMethod_5() { return static_cast<int32_t>(offsetof(AssetListAttribute_tE254CEED1A60ABA5995D73442A2A7272B477DA55, ___CustomFilterMethod_5)); }
	inline String_t* get_CustomFilterMethod_5() const { return ___CustomFilterMethod_5; }
	inline String_t** get_address_of_CustomFilterMethod_5() { return &___CustomFilterMethod_5; }
	inline void set_CustomFilterMethod_5(String_t* value)
	{
		___CustomFilterMethod_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CustomFilterMethod_5), (void*)value);
	}
};


// Sirenix.OdinInspector.AssetSelectorAttribute
struct AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Sirenix.OdinInspector.AssetSelectorAttribute::IsUniqueList
	bool ___IsUniqueList_0;
	// System.Boolean Sirenix.OdinInspector.AssetSelectorAttribute::DrawDropdownForListElements
	bool ___DrawDropdownForListElements_1;
	// System.Boolean Sirenix.OdinInspector.AssetSelectorAttribute::DisableListAddButtonBehaviour
	bool ___DisableListAddButtonBehaviour_2;
	// System.Boolean Sirenix.OdinInspector.AssetSelectorAttribute::ExcludeExistingValuesInList
	bool ___ExcludeExistingValuesInList_3;
	// System.Boolean Sirenix.OdinInspector.AssetSelectorAttribute::ExpandAllMenuItems
	bool ___ExpandAllMenuItems_4;
	// System.Boolean Sirenix.OdinInspector.AssetSelectorAttribute::FlattenTreeView
	bool ___FlattenTreeView_5;
	// System.Int32 Sirenix.OdinInspector.AssetSelectorAttribute::DropdownWidth
	int32_t ___DropdownWidth_6;
	// System.Int32 Sirenix.OdinInspector.AssetSelectorAttribute::DropdownHeight
	int32_t ___DropdownHeight_7;
	// System.String Sirenix.OdinInspector.AssetSelectorAttribute::DropdownTitle
	String_t* ___DropdownTitle_8;
	// System.String[] Sirenix.OdinInspector.AssetSelectorAttribute::SearchInFolders
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___SearchInFolders_9;
	// System.String Sirenix.OdinInspector.AssetSelectorAttribute::Filter
	String_t* ___Filter_10;

public:
	inline static int32_t get_offset_of_IsUniqueList_0() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5, ___IsUniqueList_0)); }
	inline bool get_IsUniqueList_0() const { return ___IsUniqueList_0; }
	inline bool* get_address_of_IsUniqueList_0() { return &___IsUniqueList_0; }
	inline void set_IsUniqueList_0(bool value)
	{
		___IsUniqueList_0 = value;
	}

	inline static int32_t get_offset_of_DrawDropdownForListElements_1() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5, ___DrawDropdownForListElements_1)); }
	inline bool get_DrawDropdownForListElements_1() const { return ___DrawDropdownForListElements_1; }
	inline bool* get_address_of_DrawDropdownForListElements_1() { return &___DrawDropdownForListElements_1; }
	inline void set_DrawDropdownForListElements_1(bool value)
	{
		___DrawDropdownForListElements_1 = value;
	}

	inline static int32_t get_offset_of_DisableListAddButtonBehaviour_2() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5, ___DisableListAddButtonBehaviour_2)); }
	inline bool get_DisableListAddButtonBehaviour_2() const { return ___DisableListAddButtonBehaviour_2; }
	inline bool* get_address_of_DisableListAddButtonBehaviour_2() { return &___DisableListAddButtonBehaviour_2; }
	inline void set_DisableListAddButtonBehaviour_2(bool value)
	{
		___DisableListAddButtonBehaviour_2 = value;
	}

	inline static int32_t get_offset_of_ExcludeExistingValuesInList_3() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5, ___ExcludeExistingValuesInList_3)); }
	inline bool get_ExcludeExistingValuesInList_3() const { return ___ExcludeExistingValuesInList_3; }
	inline bool* get_address_of_ExcludeExistingValuesInList_3() { return &___ExcludeExistingValuesInList_3; }
	inline void set_ExcludeExistingValuesInList_3(bool value)
	{
		___ExcludeExistingValuesInList_3 = value;
	}

	inline static int32_t get_offset_of_ExpandAllMenuItems_4() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5, ___ExpandAllMenuItems_4)); }
	inline bool get_ExpandAllMenuItems_4() const { return ___ExpandAllMenuItems_4; }
	inline bool* get_address_of_ExpandAllMenuItems_4() { return &___ExpandAllMenuItems_4; }
	inline void set_ExpandAllMenuItems_4(bool value)
	{
		___ExpandAllMenuItems_4 = value;
	}

	inline static int32_t get_offset_of_FlattenTreeView_5() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5, ___FlattenTreeView_5)); }
	inline bool get_FlattenTreeView_5() const { return ___FlattenTreeView_5; }
	inline bool* get_address_of_FlattenTreeView_5() { return &___FlattenTreeView_5; }
	inline void set_FlattenTreeView_5(bool value)
	{
		___FlattenTreeView_5 = value;
	}

	inline static int32_t get_offset_of_DropdownWidth_6() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5, ___DropdownWidth_6)); }
	inline int32_t get_DropdownWidth_6() const { return ___DropdownWidth_6; }
	inline int32_t* get_address_of_DropdownWidth_6() { return &___DropdownWidth_6; }
	inline void set_DropdownWidth_6(int32_t value)
	{
		___DropdownWidth_6 = value;
	}

	inline static int32_t get_offset_of_DropdownHeight_7() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5, ___DropdownHeight_7)); }
	inline int32_t get_DropdownHeight_7() const { return ___DropdownHeight_7; }
	inline int32_t* get_address_of_DropdownHeight_7() { return &___DropdownHeight_7; }
	inline void set_DropdownHeight_7(int32_t value)
	{
		___DropdownHeight_7 = value;
	}

	inline static int32_t get_offset_of_DropdownTitle_8() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5, ___DropdownTitle_8)); }
	inline String_t* get_DropdownTitle_8() const { return ___DropdownTitle_8; }
	inline String_t** get_address_of_DropdownTitle_8() { return &___DropdownTitle_8; }
	inline void set_DropdownTitle_8(String_t* value)
	{
		___DropdownTitle_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DropdownTitle_8), (void*)value);
	}

	inline static int32_t get_offset_of_SearchInFolders_9() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5, ___SearchInFolders_9)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_SearchInFolders_9() const { return ___SearchInFolders_9; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_SearchInFolders_9() { return &___SearchInFolders_9; }
	inline void set_SearchInFolders_9(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___SearchInFolders_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SearchInFolders_9), (void*)value);
	}

	inline static int32_t get_offset_of_Filter_10() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5, ___Filter_10)); }
	inline String_t* get_Filter_10() const { return ___Filter_10; }
	inline String_t** get_address_of_Filter_10() { return &___Filter_10; }
	inline void set_Filter_10(String_t* value)
	{
		___Filter_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Filter_10), (void*)value);
	}
};


// Sirenix.OdinInspector.AssetsOnlyAttribute
struct AssetsOnlyAttribute_t2F7CC8E0514CB2767616E03312FD23EF8FA03D63  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.ChildGameObjectsOnlyAttribute
struct ChildGameObjectsOnlyAttribute_t64A28A6D4B2AAE55BA804CDFC266C0A8681BD920  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Sirenix.OdinInspector.ChildGameObjectsOnlyAttribute::IncludeSelf
	bool ___IncludeSelf_0;

public:
	inline static int32_t get_offset_of_IncludeSelf_0() { return static_cast<int32_t>(offsetof(ChildGameObjectsOnlyAttribute_t64A28A6D4B2AAE55BA804CDFC266C0A8681BD920, ___IncludeSelf_0)); }
	inline bool get_IncludeSelf_0() const { return ___IncludeSelf_0; }
	inline bool* get_address_of_IncludeSelf_0() { return &___IncludeSelf_0; }
	inline void set_IncludeSelf_0(bool value)
	{
		___IncludeSelf_0 = value;
	}
};


// Sirenix.OdinInspector.ColorPaletteAttribute
struct ColorPaletteAttribute_t933AEAC8970C941B1074D80041861812D7170DA4  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.ColorPaletteAttribute::PaletteName
	String_t* ___PaletteName_0;
	// System.Boolean Sirenix.OdinInspector.ColorPaletteAttribute::ShowAlpha
	bool ___ShowAlpha_1;

public:
	inline static int32_t get_offset_of_PaletteName_0() { return static_cast<int32_t>(offsetof(ColorPaletteAttribute_t933AEAC8970C941B1074D80041861812D7170DA4, ___PaletteName_0)); }
	inline String_t* get_PaletteName_0() const { return ___PaletteName_0; }
	inline String_t** get_address_of_PaletteName_0() { return &___PaletteName_0; }
	inline void set_PaletteName_0(String_t* value)
	{
		___PaletteName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PaletteName_0), (void*)value);
	}

	inline static int32_t get_offset_of_ShowAlpha_1() { return static_cast<int32_t>(offsetof(ColorPaletteAttribute_t933AEAC8970C941B1074D80041861812D7170DA4, ___ShowAlpha_1)); }
	inline bool get_ShowAlpha_1() const { return ___ShowAlpha_1; }
	inline bool* get_address_of_ShowAlpha_1() { return &___ShowAlpha_1; }
	inline void set_ShowAlpha_1(bool value)
	{
		___ShowAlpha_1 = value;
	}
};


// Sirenix.OdinInspector.CustomContextMenuAttribute
struct CustomContextMenuAttribute_tB2D58375FEB292B02B5442B5F1282A7796A4B007  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.CustomContextMenuAttribute::MenuItem
	String_t* ___MenuItem_0;
	// System.String Sirenix.OdinInspector.CustomContextMenuAttribute::Action
	String_t* ___Action_1;

public:
	inline static int32_t get_offset_of_MenuItem_0() { return static_cast<int32_t>(offsetof(CustomContextMenuAttribute_tB2D58375FEB292B02B5442B5F1282A7796A4B007, ___MenuItem_0)); }
	inline String_t* get_MenuItem_0() const { return ___MenuItem_0; }
	inline String_t** get_address_of_MenuItem_0() { return &___MenuItem_0; }
	inline void set_MenuItem_0(String_t* value)
	{
		___MenuItem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MenuItem_0), (void*)value);
	}

	inline static int32_t get_offset_of_Action_1() { return static_cast<int32_t>(offsetof(CustomContextMenuAttribute_tB2D58375FEB292B02B5442B5F1282A7796A4B007, ___Action_1)); }
	inline String_t* get_Action_1() const { return ___Action_1; }
	inline String_t** get_address_of_Action_1() { return &___Action_1; }
	inline void set_Action_1(String_t* value)
	{
		___Action_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Action_1), (void*)value);
	}
};


// Sirenix.OdinInspector.CustomValueDrawerAttribute
struct CustomValueDrawerAttribute_tF14F13C43F3B695A6A04C52ABC0BCED76D8D4278  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.CustomValueDrawerAttribute::Action
	String_t* ___Action_0;

public:
	inline static int32_t get_offset_of_Action_0() { return static_cast<int32_t>(offsetof(CustomValueDrawerAttribute_tF14F13C43F3B695A6A04C52ABC0BCED76D8D4278, ___Action_0)); }
	inline String_t* get_Action_0() const { return ___Action_0; }
	inline String_t** get_address_of_Action_0() { return &___Action_0; }
	inline void set_Action_0(String_t* value)
	{
		___Action_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Action_0), (void*)value);
	}
};


// Sirenix.OdinInspector.DelayedPropertyAttribute
struct DelayedPropertyAttribute_t63A7B3586325CC6B399DA0D36EBF16D3905EE679  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.DisableContextMenuAttribute
struct DisableContextMenuAttribute_t69DDDBD94727983567A3793C3CFCFA00814E6DC2  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Sirenix.OdinInspector.DisableContextMenuAttribute::DisableForMember
	bool ___DisableForMember_0;
	// System.Boolean Sirenix.OdinInspector.DisableContextMenuAttribute::DisableForCollectionElements
	bool ___DisableForCollectionElements_1;

public:
	inline static int32_t get_offset_of_DisableForMember_0() { return static_cast<int32_t>(offsetof(DisableContextMenuAttribute_t69DDDBD94727983567A3793C3CFCFA00814E6DC2, ___DisableForMember_0)); }
	inline bool get_DisableForMember_0() const { return ___DisableForMember_0; }
	inline bool* get_address_of_DisableForMember_0() { return &___DisableForMember_0; }
	inline void set_DisableForMember_0(bool value)
	{
		___DisableForMember_0 = value;
	}

	inline static int32_t get_offset_of_DisableForCollectionElements_1() { return static_cast<int32_t>(offsetof(DisableContextMenuAttribute_t69DDDBD94727983567A3793C3CFCFA00814E6DC2, ___DisableForCollectionElements_1)); }
	inline bool get_DisableForCollectionElements_1() const { return ___DisableForCollectionElements_1; }
	inline bool* get_address_of_DisableForCollectionElements_1() { return &___DisableForCollectionElements_1; }
	inline void set_DisableForCollectionElements_1(bool value)
	{
		___DisableForCollectionElements_1 = value;
	}
};


// Sirenix.OdinInspector.DisableIfAttribute
struct DisableIfAttribute_t44366AD5F4EF373AF85E952BEA715D5A40715377  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.DisableIfAttribute::Condition
	String_t* ___Condition_0;
	// System.Object Sirenix.OdinInspector.DisableIfAttribute::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Condition_0() { return static_cast<int32_t>(offsetof(DisableIfAttribute_t44366AD5F4EF373AF85E952BEA715D5A40715377, ___Condition_0)); }
	inline String_t* get_Condition_0() const { return ___Condition_0; }
	inline String_t** get_address_of_Condition_0() { return &___Condition_0; }
	inline void set_Condition_0(String_t* value)
	{
		___Condition_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Condition_0), (void*)value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(DisableIfAttribute_t44366AD5F4EF373AF85E952BEA715D5A40715377, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_1), (void*)value);
	}
};


// Sirenix.OdinInspector.DisableInEditorModeAttribute
struct DisableInEditorModeAttribute_t07461DBCB20EE922ABC319603E5954C3F00CBA44  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.DisableInInlineEditorsAttribute
struct DisableInInlineEditorsAttribute_t5E128B9CE96EC7F787672A3C8950E055F4022110  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.DisableInNonPrefabsAttribute
struct DisableInNonPrefabsAttribute_t0D00848E037C1AAAB99D462E6FF885078A300D7E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.DisableInPlayModeAttribute
struct DisableInPlayModeAttribute_t9E36813C99F4DC97259581CB1CE03B0F174B32BA  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.DisableInPrefabAssetsAttribute
struct DisableInPrefabAssetsAttribute_t461A48998B4E32C9C110A5641BF71D57484DF76D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.DisableInPrefabInstancesAttribute
struct DisableInPrefabInstancesAttribute_tBDF4782E7ED0010E287CBE47AB4EFCDC3D2C915C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.DisableInPrefabsAttribute
struct DisableInPrefabsAttribute_tC5A57835853CBCBAFF19B5678BEAA7E5A050EE9B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.DisplayAsStringAttribute
struct DisplayAsStringAttribute_t677D6873B0CED543705D9C9D9C7C02108AE705E9  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Sirenix.OdinInspector.DisplayAsStringAttribute::Overflow
	bool ___Overflow_0;

public:
	inline static int32_t get_offset_of_Overflow_0() { return static_cast<int32_t>(offsetof(DisplayAsStringAttribute_t677D6873B0CED543705D9C9D9C7C02108AE705E9, ___Overflow_0)); }
	inline bool get_Overflow_0() const { return ___Overflow_0; }
	inline bool* get_address_of_Overflow_0() { return &___Overflow_0; }
	inline void set_Overflow_0(bool value)
	{
		___Overflow_0 = value;
	}
};


// Sirenix.OdinInspector.DoNotDrawAsReferenceAttribute
struct DoNotDrawAsReferenceAttribute_tB5A4B9DEAF2766E27A1DB535A8A65C3C37E12C47  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.DontApplyToListElementsAttribute
struct DontApplyToListElementsAttribute_tC34D036BA277A188748A5E8D6C5C2FEB90EBF666  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.DrawWithUnityAttribute
struct DrawWithUnityAttribute_t3995454B2F87DDEE9CDF30C1E9674EE65F1A5961  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.EnableForPrefabOnlyAttribute
struct EnableForPrefabOnlyAttribute_t097E39FDE22B623C6201BBEEEF3278A093AE0648  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.EnableGUIAttribute
struct EnableGUIAttribute_t0A76255CB3C8DADBA1259980D6551ABF6A91E4A2  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.EnableIfAttribute
struct EnableIfAttribute_t28619931329D231822A79B006B44F096DE437E9A  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.EnableIfAttribute::Condition
	String_t* ___Condition_0;
	// System.Object Sirenix.OdinInspector.EnableIfAttribute::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Condition_0() { return static_cast<int32_t>(offsetof(EnableIfAttribute_t28619931329D231822A79B006B44F096DE437E9A, ___Condition_0)); }
	inline String_t* get_Condition_0() const { return ___Condition_0; }
	inline String_t** get_address_of_Condition_0() { return &___Condition_0; }
	inline void set_Condition_0(String_t* value)
	{
		___Condition_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Condition_0), (void*)value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(EnableIfAttribute_t28619931329D231822A79B006B44F096DE437E9A, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_1), (void*)value);
	}
};


// Sirenix.OdinInspector.EnumPagingAttribute
struct EnumPagingAttribute_t1FEB069556F6126E3D05095600132A589FB0F135  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.EnumToggleButtonsAttribute
struct EnumToggleButtonsAttribute_tCE472D5DA47F333E38C7537FDF0776B73659E2DE  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.FilePathAttribute
struct FilePathAttribute_tB6DD8A50FC2A4C8ED0FD8FB5158A2BBB47AC045F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::AbsolutePath
	bool ___AbsolutePath_0;
	// System.String Sirenix.OdinInspector.FilePathAttribute::Extensions
	String_t* ___Extensions_1;
	// System.String Sirenix.OdinInspector.FilePathAttribute::ParentFolder
	String_t* ___ParentFolder_2;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::RequireValidPath
	bool ___RequireValidPath_3;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::RequireExistingPath
	bool ___RequireExistingPath_4;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::UseBackslashes
	bool ___UseBackslashes_5;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::<ReadOnly>k__BackingField
	bool ___U3CReadOnlyU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_AbsolutePath_0() { return static_cast<int32_t>(offsetof(FilePathAttribute_tB6DD8A50FC2A4C8ED0FD8FB5158A2BBB47AC045F, ___AbsolutePath_0)); }
	inline bool get_AbsolutePath_0() const { return ___AbsolutePath_0; }
	inline bool* get_address_of_AbsolutePath_0() { return &___AbsolutePath_0; }
	inline void set_AbsolutePath_0(bool value)
	{
		___AbsolutePath_0 = value;
	}

	inline static int32_t get_offset_of_Extensions_1() { return static_cast<int32_t>(offsetof(FilePathAttribute_tB6DD8A50FC2A4C8ED0FD8FB5158A2BBB47AC045F, ___Extensions_1)); }
	inline String_t* get_Extensions_1() const { return ___Extensions_1; }
	inline String_t** get_address_of_Extensions_1() { return &___Extensions_1; }
	inline void set_Extensions_1(String_t* value)
	{
		___Extensions_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Extensions_1), (void*)value);
	}

	inline static int32_t get_offset_of_ParentFolder_2() { return static_cast<int32_t>(offsetof(FilePathAttribute_tB6DD8A50FC2A4C8ED0FD8FB5158A2BBB47AC045F, ___ParentFolder_2)); }
	inline String_t* get_ParentFolder_2() const { return ___ParentFolder_2; }
	inline String_t** get_address_of_ParentFolder_2() { return &___ParentFolder_2; }
	inline void set_ParentFolder_2(String_t* value)
	{
		___ParentFolder_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ParentFolder_2), (void*)value);
	}

	inline static int32_t get_offset_of_RequireValidPath_3() { return static_cast<int32_t>(offsetof(FilePathAttribute_tB6DD8A50FC2A4C8ED0FD8FB5158A2BBB47AC045F, ___RequireValidPath_3)); }
	inline bool get_RequireValidPath_3() const { return ___RequireValidPath_3; }
	inline bool* get_address_of_RequireValidPath_3() { return &___RequireValidPath_3; }
	inline void set_RequireValidPath_3(bool value)
	{
		___RequireValidPath_3 = value;
	}

	inline static int32_t get_offset_of_RequireExistingPath_4() { return static_cast<int32_t>(offsetof(FilePathAttribute_tB6DD8A50FC2A4C8ED0FD8FB5158A2BBB47AC045F, ___RequireExistingPath_4)); }
	inline bool get_RequireExistingPath_4() const { return ___RequireExistingPath_4; }
	inline bool* get_address_of_RequireExistingPath_4() { return &___RequireExistingPath_4; }
	inline void set_RequireExistingPath_4(bool value)
	{
		___RequireExistingPath_4 = value;
	}

	inline static int32_t get_offset_of_UseBackslashes_5() { return static_cast<int32_t>(offsetof(FilePathAttribute_tB6DD8A50FC2A4C8ED0FD8FB5158A2BBB47AC045F, ___UseBackslashes_5)); }
	inline bool get_UseBackslashes_5() const { return ___UseBackslashes_5; }
	inline bool* get_address_of_UseBackslashes_5() { return &___UseBackslashes_5; }
	inline void set_UseBackslashes_5(bool value)
	{
		___UseBackslashes_5 = value;
	}

	inline static int32_t get_offset_of_U3CReadOnlyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FilePathAttribute_tB6DD8A50FC2A4C8ED0FD8FB5158A2BBB47AC045F, ___U3CReadOnlyU3Ek__BackingField_6)); }
	inline bool get_U3CReadOnlyU3Ek__BackingField_6() const { return ___U3CReadOnlyU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CReadOnlyU3Ek__BackingField_6() { return &___U3CReadOnlyU3Ek__BackingField_6; }
	inline void set_U3CReadOnlyU3Ek__BackingField_6(bool value)
	{
		___U3CReadOnlyU3Ek__BackingField_6 = value;
	}
};


// Sirenix.OdinInspector.FolderPathAttribute
struct FolderPathAttribute_t63E1631A434EE753FCD86E7273812B2E89FFDF94  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::AbsolutePath
	bool ___AbsolutePath_0;
	// System.String Sirenix.OdinInspector.FolderPathAttribute::ParentFolder
	String_t* ___ParentFolder_1;
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::RequireValidPath
	bool ___RequireValidPath_2;
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::RequireExistingPath
	bool ___RequireExistingPath_3;
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::UseBackslashes
	bool ___UseBackslashes_4;

public:
	inline static int32_t get_offset_of_AbsolutePath_0() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t63E1631A434EE753FCD86E7273812B2E89FFDF94, ___AbsolutePath_0)); }
	inline bool get_AbsolutePath_0() const { return ___AbsolutePath_0; }
	inline bool* get_address_of_AbsolutePath_0() { return &___AbsolutePath_0; }
	inline void set_AbsolutePath_0(bool value)
	{
		___AbsolutePath_0 = value;
	}

	inline static int32_t get_offset_of_ParentFolder_1() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t63E1631A434EE753FCD86E7273812B2E89FFDF94, ___ParentFolder_1)); }
	inline String_t* get_ParentFolder_1() const { return ___ParentFolder_1; }
	inline String_t** get_address_of_ParentFolder_1() { return &___ParentFolder_1; }
	inline void set_ParentFolder_1(String_t* value)
	{
		___ParentFolder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ParentFolder_1), (void*)value);
	}

	inline static int32_t get_offset_of_RequireValidPath_2() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t63E1631A434EE753FCD86E7273812B2E89FFDF94, ___RequireValidPath_2)); }
	inline bool get_RequireValidPath_2() const { return ___RequireValidPath_2; }
	inline bool* get_address_of_RequireValidPath_2() { return &___RequireValidPath_2; }
	inline void set_RequireValidPath_2(bool value)
	{
		___RequireValidPath_2 = value;
	}

	inline static int32_t get_offset_of_RequireExistingPath_3() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t63E1631A434EE753FCD86E7273812B2E89FFDF94, ___RequireExistingPath_3)); }
	inline bool get_RequireExistingPath_3() const { return ___RequireExistingPath_3; }
	inline bool* get_address_of_RequireExistingPath_3() { return &___RequireExistingPath_3; }
	inline void set_RequireExistingPath_3(bool value)
	{
		___RequireExistingPath_3 = value;
	}

	inline static int32_t get_offset_of_UseBackslashes_4() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t63E1631A434EE753FCD86E7273812B2E89FFDF94, ___UseBackslashes_4)); }
	inline bool get_UseBackslashes_4() const { return ___UseBackslashes_4; }
	inline bool* get_address_of_UseBackslashes_4() { return &___UseBackslashes_4; }
	inline void set_UseBackslashes_4(bool value)
	{
		___UseBackslashes_4 = value;
	}
};


// Sirenix.OdinInspector.HideDuplicateReferenceBoxAttribute
struct HideDuplicateReferenceBoxAttribute_t91004BA0AB0CD17A308E7FE988D0038FB00150E5  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.HideIfAttribute
struct HideIfAttribute_t011B476D6F3C9156B1886271EFB256C8EC3D76FA  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.HideIfAttribute::Condition
	String_t* ___Condition_0;
	// System.Object Sirenix.OdinInspector.HideIfAttribute::Value
	RuntimeObject * ___Value_1;
	// System.Boolean Sirenix.OdinInspector.HideIfAttribute::Animate
	bool ___Animate_2;

public:
	inline static int32_t get_offset_of_Condition_0() { return static_cast<int32_t>(offsetof(HideIfAttribute_t011B476D6F3C9156B1886271EFB256C8EC3D76FA, ___Condition_0)); }
	inline String_t* get_Condition_0() const { return ___Condition_0; }
	inline String_t** get_address_of_Condition_0() { return &___Condition_0; }
	inline void set_Condition_0(String_t* value)
	{
		___Condition_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Condition_0), (void*)value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(HideIfAttribute_t011B476D6F3C9156B1886271EFB256C8EC3D76FA, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_1), (void*)value);
	}

	inline static int32_t get_offset_of_Animate_2() { return static_cast<int32_t>(offsetof(HideIfAttribute_t011B476D6F3C9156B1886271EFB256C8EC3D76FA, ___Animate_2)); }
	inline bool get_Animate_2() const { return ___Animate_2; }
	inline bool* get_address_of_Animate_2() { return &___Animate_2; }
	inline void set_Animate_2(bool value)
	{
		___Animate_2 = value;
	}
};


// Sirenix.OdinInspector.HideInEditorModeAttribute
struct HideInEditorModeAttribute_t65C98A206B4DC8AC9240C5546D0A6D3827CE9E57  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.HideInInlineEditorsAttribute
struct HideInInlineEditorsAttribute_t177610D85D2123274AD4EA5FE2C8A7DFF7D2240C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.HideInNonPrefabsAttribute
struct HideInNonPrefabsAttribute_tBD5D04C53C1C275F341CD8AD160EFADA158427C0  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.HideInPlayModeAttribute
struct HideInPlayModeAttribute_t627CDE4C0FADA1E6E9197842268B6077ED7004FC  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.HideInPrefabAssetsAttribute
struct HideInPrefabAssetsAttribute_t7BB906033FB4DF85FABE31388AF559725E98ED77  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.HideInPrefabInstancesAttribute
struct HideInPrefabInstancesAttribute_t7148B73699DA6BE500EF82AE3223E5C89DC4D98F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.HideInPrefabsAttribute
struct HideInPrefabsAttribute_t28D32D1AD69258DF301288D58E42CD5E14FDABAA  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.HideInTablesAttribute
struct HideInTablesAttribute_tCC2FB41AEC83E4CA159714F7238B51FD9AC5D546  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.HideLabelAttribute
struct HideLabelAttribute_t451BC6D3C8B4E1C9A9E75D4C1F308C3ED301C71F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.HideMonoScriptAttribute
struct HideMonoScriptAttribute_t8EF9302A9377F51F515B10BB0AB16844636A9599  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.HideNetworkBehaviourFieldsAttribute
struct HideNetworkBehaviourFieldsAttribute_tCD095386566090284A9A9E3806805FF73B9E83F3  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.HideReferenceObjectPickerAttribute
struct HideReferenceObjectPickerAttribute_tE06BCAACEB5B5CA565BA3BC25EA11D7EA22C6F28  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.IncludeMyAttributesAttribute
struct IncludeMyAttributesAttribute_tFCF24E3A971A142A66755F0C43C6B140D1D55327  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.IndentAttribute
struct IndentAttribute_t8DB2B60FF1214619E8901509CE31C77EA2CD6DE8  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 Sirenix.OdinInspector.IndentAttribute::IndentLevel
	int32_t ___IndentLevel_0;

public:
	inline static int32_t get_offset_of_IndentLevel_0() { return static_cast<int32_t>(offsetof(IndentAttribute_t8DB2B60FF1214619E8901509CE31C77EA2CD6DE8, ___IndentLevel_0)); }
	inline int32_t get_IndentLevel_0() const { return ___IndentLevel_0; }
	inline int32_t* get_address_of_IndentLevel_0() { return &___IndentLevel_0; }
	inline void set_IndentLevel_0(int32_t value)
	{
		___IndentLevel_0 = value;
	}
};


// Sirenix.OdinInspector.InlineButtonAttribute
struct InlineButtonAttribute_t4E0014F9C6D3D55B24D12C714E8E35E4364B1AB6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.InlineButtonAttribute::Action
	String_t* ___Action_0;
	// System.String Sirenix.OdinInspector.InlineButtonAttribute::Label
	String_t* ___Label_1;

public:
	inline static int32_t get_offset_of_Action_0() { return static_cast<int32_t>(offsetof(InlineButtonAttribute_t4E0014F9C6D3D55B24D12C714E8E35E4364B1AB6, ___Action_0)); }
	inline String_t* get_Action_0() const { return ___Action_0; }
	inline String_t** get_address_of_Action_0() { return &___Action_0; }
	inline void set_Action_0(String_t* value)
	{
		___Action_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Action_0), (void*)value);
	}

	inline static int32_t get_offset_of_Label_1() { return static_cast<int32_t>(offsetof(InlineButtonAttribute_t4E0014F9C6D3D55B24D12C714E8E35E4364B1AB6, ___Label_1)); }
	inline String_t* get_Label_1() const { return ___Label_1; }
	inline String_t** get_address_of_Label_1() { return &___Label_1; }
	inline void set_Label_1(String_t* value)
	{
		___Label_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Label_1), (void*)value);
	}
};


// Sirenix.OdinInspector.InlinePropertyAttribute
struct InlinePropertyAttribute_t7094D1D53D31ACA38BD2F40D93D24BB00ADFD6FA  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 Sirenix.OdinInspector.InlinePropertyAttribute::LabelWidth
	int32_t ___LabelWidth_0;

public:
	inline static int32_t get_offset_of_LabelWidth_0() { return static_cast<int32_t>(offsetof(InlinePropertyAttribute_t7094D1D53D31ACA38BD2F40D93D24BB00ADFD6FA, ___LabelWidth_0)); }
	inline int32_t get_LabelWidth_0() const { return ___LabelWidth_0; }
	inline int32_t* get_address_of_LabelWidth_0() { return &___LabelWidth_0; }
	inline void set_LabelWidth_0(int32_t value)
	{
		___LabelWidth_0 = value;
	}
};


// Sirenix.OdinInspector.LabelTextAttribute
struct LabelTextAttribute_t05B2F25125BFF01D694EC3DBED7B9B045B16ACC9  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.LabelTextAttribute::Text
	String_t* ___Text_0;
	// System.Boolean Sirenix.OdinInspector.LabelTextAttribute::NicifyText
	bool ___NicifyText_1;

public:
	inline static int32_t get_offset_of_Text_0() { return static_cast<int32_t>(offsetof(LabelTextAttribute_t05B2F25125BFF01D694EC3DBED7B9B045B16ACC9, ___Text_0)); }
	inline String_t* get_Text_0() const { return ___Text_0; }
	inline String_t** get_address_of_Text_0() { return &___Text_0; }
	inline void set_Text_0(String_t* value)
	{
		___Text_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text_0), (void*)value);
	}

	inline static int32_t get_offset_of_NicifyText_1() { return static_cast<int32_t>(offsetof(LabelTextAttribute_t05B2F25125BFF01D694EC3DBED7B9B045B16ACC9, ___NicifyText_1)); }
	inline bool get_NicifyText_1() const { return ___NicifyText_1; }
	inline bool* get_address_of_NicifyText_1() { return &___NicifyText_1; }
	inline void set_NicifyText_1(bool value)
	{
		___NicifyText_1 = value;
	}
};


// Sirenix.OdinInspector.LabelWidthAttribute
struct LabelWidthAttribute_tD46C5A741BE80B8CA6CFC7DAB33E13508A88FF6C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Single Sirenix.OdinInspector.LabelWidthAttribute::Width
	float ___Width_0;

public:
	inline static int32_t get_offset_of_Width_0() { return static_cast<int32_t>(offsetof(LabelWidthAttribute_tD46C5A741BE80B8CA6CFC7DAB33E13508A88FF6C, ___Width_0)); }
	inline float get_Width_0() const { return ___Width_0; }
	inline float* get_address_of_Width_0() { return &___Width_0; }
	inline void set_Width_0(float value)
	{
		___Width_0 = value;
	}
};


// Sirenix.OdinInspector.ListDrawerSettingsAttribute
struct ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::HideAddButton
	bool ___HideAddButton_0;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::HideRemoveButton
	bool ___HideRemoveButton_1;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::ListElementLabelName
	String_t* ___ListElementLabelName_2;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::CustomAddFunction
	String_t* ___CustomAddFunction_3;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::CustomRemoveIndexFunction
	String_t* ___CustomRemoveIndexFunction_4;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::CustomRemoveElementFunction
	String_t* ___CustomRemoveElementFunction_5;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::OnBeginListElementGUI
	String_t* ___OnBeginListElementGUI_6;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::OnEndListElementGUI
	String_t* ___OnEndListElementGUI_7;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::AlwaysAddDefaultValue
	bool ___AlwaysAddDefaultValue_8;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::AddCopiesLastElement
	bool ___AddCopiesLastElement_9;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::onTitleBarGUI
	String_t* ___onTitleBarGUI_10;
	// System.Int32 Sirenix.OdinInspector.ListDrawerSettingsAttribute::numberOfItemsPerPage
	int32_t ___numberOfItemsPerPage_11;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::paging
	bool ___paging_12;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::draggable
	bool ___draggable_13;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::isReadOnly
	bool ___isReadOnly_14;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showItemCount
	bool ___showItemCount_15;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::pagingHasValue
	bool ___pagingHasValue_16;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::draggableHasValue
	bool ___draggableHasValue_17;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::isReadOnlyHasValue
	bool ___isReadOnlyHasValue_18;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showItemCountHasValue
	bool ___showItemCountHasValue_19;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::expanded
	bool ___expanded_20;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::expandedHasValue
	bool ___expandedHasValue_21;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::numberOfItemsPerPageHasValue
	bool ___numberOfItemsPerPageHasValue_22;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showIndexLabels
	bool ___showIndexLabels_23;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showIndexLabelsHasValue
	bool ___showIndexLabelsHasValue_24;

public:
	inline static int32_t get_offset_of_HideAddButton_0() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___HideAddButton_0)); }
	inline bool get_HideAddButton_0() const { return ___HideAddButton_0; }
	inline bool* get_address_of_HideAddButton_0() { return &___HideAddButton_0; }
	inline void set_HideAddButton_0(bool value)
	{
		___HideAddButton_0 = value;
	}

	inline static int32_t get_offset_of_HideRemoveButton_1() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___HideRemoveButton_1)); }
	inline bool get_HideRemoveButton_1() const { return ___HideRemoveButton_1; }
	inline bool* get_address_of_HideRemoveButton_1() { return &___HideRemoveButton_1; }
	inline void set_HideRemoveButton_1(bool value)
	{
		___HideRemoveButton_1 = value;
	}

	inline static int32_t get_offset_of_ListElementLabelName_2() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___ListElementLabelName_2)); }
	inline String_t* get_ListElementLabelName_2() const { return ___ListElementLabelName_2; }
	inline String_t** get_address_of_ListElementLabelName_2() { return &___ListElementLabelName_2; }
	inline void set_ListElementLabelName_2(String_t* value)
	{
		___ListElementLabelName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ListElementLabelName_2), (void*)value);
	}

	inline static int32_t get_offset_of_CustomAddFunction_3() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___CustomAddFunction_3)); }
	inline String_t* get_CustomAddFunction_3() const { return ___CustomAddFunction_3; }
	inline String_t** get_address_of_CustomAddFunction_3() { return &___CustomAddFunction_3; }
	inline void set_CustomAddFunction_3(String_t* value)
	{
		___CustomAddFunction_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CustomAddFunction_3), (void*)value);
	}

	inline static int32_t get_offset_of_CustomRemoveIndexFunction_4() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___CustomRemoveIndexFunction_4)); }
	inline String_t* get_CustomRemoveIndexFunction_4() const { return ___CustomRemoveIndexFunction_4; }
	inline String_t** get_address_of_CustomRemoveIndexFunction_4() { return &___CustomRemoveIndexFunction_4; }
	inline void set_CustomRemoveIndexFunction_4(String_t* value)
	{
		___CustomRemoveIndexFunction_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CustomRemoveIndexFunction_4), (void*)value);
	}

	inline static int32_t get_offset_of_CustomRemoveElementFunction_5() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___CustomRemoveElementFunction_5)); }
	inline String_t* get_CustomRemoveElementFunction_5() const { return ___CustomRemoveElementFunction_5; }
	inline String_t** get_address_of_CustomRemoveElementFunction_5() { return &___CustomRemoveElementFunction_5; }
	inline void set_CustomRemoveElementFunction_5(String_t* value)
	{
		___CustomRemoveElementFunction_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CustomRemoveElementFunction_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnBeginListElementGUI_6() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___OnBeginListElementGUI_6)); }
	inline String_t* get_OnBeginListElementGUI_6() const { return ___OnBeginListElementGUI_6; }
	inline String_t** get_address_of_OnBeginListElementGUI_6() { return &___OnBeginListElementGUI_6; }
	inline void set_OnBeginListElementGUI_6(String_t* value)
	{
		___OnBeginListElementGUI_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnBeginListElementGUI_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnEndListElementGUI_7() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___OnEndListElementGUI_7)); }
	inline String_t* get_OnEndListElementGUI_7() const { return ___OnEndListElementGUI_7; }
	inline String_t** get_address_of_OnEndListElementGUI_7() { return &___OnEndListElementGUI_7; }
	inline void set_OnEndListElementGUI_7(String_t* value)
	{
		___OnEndListElementGUI_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEndListElementGUI_7), (void*)value);
	}

	inline static int32_t get_offset_of_AlwaysAddDefaultValue_8() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___AlwaysAddDefaultValue_8)); }
	inline bool get_AlwaysAddDefaultValue_8() const { return ___AlwaysAddDefaultValue_8; }
	inline bool* get_address_of_AlwaysAddDefaultValue_8() { return &___AlwaysAddDefaultValue_8; }
	inline void set_AlwaysAddDefaultValue_8(bool value)
	{
		___AlwaysAddDefaultValue_8 = value;
	}

	inline static int32_t get_offset_of_AddCopiesLastElement_9() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___AddCopiesLastElement_9)); }
	inline bool get_AddCopiesLastElement_9() const { return ___AddCopiesLastElement_9; }
	inline bool* get_address_of_AddCopiesLastElement_9() { return &___AddCopiesLastElement_9; }
	inline void set_AddCopiesLastElement_9(bool value)
	{
		___AddCopiesLastElement_9 = value;
	}

	inline static int32_t get_offset_of_onTitleBarGUI_10() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___onTitleBarGUI_10)); }
	inline String_t* get_onTitleBarGUI_10() const { return ___onTitleBarGUI_10; }
	inline String_t** get_address_of_onTitleBarGUI_10() { return &___onTitleBarGUI_10; }
	inline void set_onTitleBarGUI_10(String_t* value)
	{
		___onTitleBarGUI_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onTitleBarGUI_10), (void*)value);
	}

	inline static int32_t get_offset_of_numberOfItemsPerPage_11() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___numberOfItemsPerPage_11)); }
	inline int32_t get_numberOfItemsPerPage_11() const { return ___numberOfItemsPerPage_11; }
	inline int32_t* get_address_of_numberOfItemsPerPage_11() { return &___numberOfItemsPerPage_11; }
	inline void set_numberOfItemsPerPage_11(int32_t value)
	{
		___numberOfItemsPerPage_11 = value;
	}

	inline static int32_t get_offset_of_paging_12() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___paging_12)); }
	inline bool get_paging_12() const { return ___paging_12; }
	inline bool* get_address_of_paging_12() { return &___paging_12; }
	inline void set_paging_12(bool value)
	{
		___paging_12 = value;
	}

	inline static int32_t get_offset_of_draggable_13() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___draggable_13)); }
	inline bool get_draggable_13() const { return ___draggable_13; }
	inline bool* get_address_of_draggable_13() { return &___draggable_13; }
	inline void set_draggable_13(bool value)
	{
		___draggable_13 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_14() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___isReadOnly_14)); }
	inline bool get_isReadOnly_14() const { return ___isReadOnly_14; }
	inline bool* get_address_of_isReadOnly_14() { return &___isReadOnly_14; }
	inline void set_isReadOnly_14(bool value)
	{
		___isReadOnly_14 = value;
	}

	inline static int32_t get_offset_of_showItemCount_15() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___showItemCount_15)); }
	inline bool get_showItemCount_15() const { return ___showItemCount_15; }
	inline bool* get_address_of_showItemCount_15() { return &___showItemCount_15; }
	inline void set_showItemCount_15(bool value)
	{
		___showItemCount_15 = value;
	}

	inline static int32_t get_offset_of_pagingHasValue_16() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___pagingHasValue_16)); }
	inline bool get_pagingHasValue_16() const { return ___pagingHasValue_16; }
	inline bool* get_address_of_pagingHasValue_16() { return &___pagingHasValue_16; }
	inline void set_pagingHasValue_16(bool value)
	{
		___pagingHasValue_16 = value;
	}

	inline static int32_t get_offset_of_draggableHasValue_17() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___draggableHasValue_17)); }
	inline bool get_draggableHasValue_17() const { return ___draggableHasValue_17; }
	inline bool* get_address_of_draggableHasValue_17() { return &___draggableHasValue_17; }
	inline void set_draggableHasValue_17(bool value)
	{
		___draggableHasValue_17 = value;
	}

	inline static int32_t get_offset_of_isReadOnlyHasValue_18() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___isReadOnlyHasValue_18)); }
	inline bool get_isReadOnlyHasValue_18() const { return ___isReadOnlyHasValue_18; }
	inline bool* get_address_of_isReadOnlyHasValue_18() { return &___isReadOnlyHasValue_18; }
	inline void set_isReadOnlyHasValue_18(bool value)
	{
		___isReadOnlyHasValue_18 = value;
	}

	inline static int32_t get_offset_of_showItemCountHasValue_19() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___showItemCountHasValue_19)); }
	inline bool get_showItemCountHasValue_19() const { return ___showItemCountHasValue_19; }
	inline bool* get_address_of_showItemCountHasValue_19() { return &___showItemCountHasValue_19; }
	inline void set_showItemCountHasValue_19(bool value)
	{
		___showItemCountHasValue_19 = value;
	}

	inline static int32_t get_offset_of_expanded_20() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___expanded_20)); }
	inline bool get_expanded_20() const { return ___expanded_20; }
	inline bool* get_address_of_expanded_20() { return &___expanded_20; }
	inline void set_expanded_20(bool value)
	{
		___expanded_20 = value;
	}

	inline static int32_t get_offset_of_expandedHasValue_21() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___expandedHasValue_21)); }
	inline bool get_expandedHasValue_21() const { return ___expandedHasValue_21; }
	inline bool* get_address_of_expandedHasValue_21() { return &___expandedHasValue_21; }
	inline void set_expandedHasValue_21(bool value)
	{
		___expandedHasValue_21 = value;
	}

	inline static int32_t get_offset_of_numberOfItemsPerPageHasValue_22() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___numberOfItemsPerPageHasValue_22)); }
	inline bool get_numberOfItemsPerPageHasValue_22() const { return ___numberOfItemsPerPageHasValue_22; }
	inline bool* get_address_of_numberOfItemsPerPageHasValue_22() { return &___numberOfItemsPerPageHasValue_22; }
	inline void set_numberOfItemsPerPageHasValue_22(bool value)
	{
		___numberOfItemsPerPageHasValue_22 = value;
	}

	inline static int32_t get_offset_of_showIndexLabels_23() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___showIndexLabels_23)); }
	inline bool get_showIndexLabels_23() const { return ___showIndexLabels_23; }
	inline bool* get_address_of_showIndexLabels_23() { return &___showIndexLabels_23; }
	inline void set_showIndexLabels_23(bool value)
	{
		___showIndexLabels_23 = value;
	}

	inline static int32_t get_offset_of_showIndexLabelsHasValue_24() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D, ___showIndexLabelsHasValue_24)); }
	inline bool get_showIndexLabelsHasValue_24() const { return ___showIndexLabelsHasValue_24; }
	inline bool* get_address_of_showIndexLabelsHasValue_24() { return &___showIndexLabelsHasValue_24; }
	inline void set_showIndexLabelsHasValue_24(bool value)
	{
		___showIndexLabelsHasValue_24 = value;
	}
};


// Sirenix.OdinInspector.MaxValueAttribute
struct MaxValueAttribute_t7C88F52D1B04C874C07D6E937F2C83CAED093185  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Double Sirenix.OdinInspector.MaxValueAttribute::MaxValue
	double ___MaxValue_0;
	// System.String Sirenix.OdinInspector.MaxValueAttribute::Expression
	String_t* ___Expression_1;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(MaxValueAttribute_t7C88F52D1B04C874C07D6E937F2C83CAED093185, ___MaxValue_0)); }
	inline double get_MaxValue_0() const { return ___MaxValue_0; }
	inline double* get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(double value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_Expression_1() { return static_cast<int32_t>(offsetof(MaxValueAttribute_t7C88F52D1B04C874C07D6E937F2C83CAED093185, ___Expression_1)); }
	inline String_t* get_Expression_1() const { return ___Expression_1; }
	inline String_t** get_address_of_Expression_1() { return &___Expression_1; }
	inline void set_Expression_1(String_t* value)
	{
		___Expression_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Expression_1), (void*)value);
	}
};


// Sirenix.OdinInspector.MinMaxSliderAttribute
struct MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Single Sirenix.OdinInspector.MinMaxSliderAttribute::MinValue
	float ___MinValue_0;
	// System.Single Sirenix.OdinInspector.MinMaxSliderAttribute::MaxValue
	float ___MaxValue_1;
	// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::MinValueGetter
	String_t* ___MinValueGetter_2;
	// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::MaxValueGetter
	String_t* ___MaxValueGetter_3;
	// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::MinMaxValueGetter
	String_t* ___MinMaxValueGetter_4;
	// System.Boolean Sirenix.OdinInspector.MinMaxSliderAttribute::ShowFields
	bool ___ShowFields_5;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461, ___MinValue_0)); }
	inline float get_MinValue_0() const { return ___MinValue_0; }
	inline float* get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(float value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461, ___MaxValue_1)); }
	inline float get_MaxValue_1() const { return ___MaxValue_1; }
	inline float* get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(float value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinValueGetter_2() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461, ___MinValueGetter_2)); }
	inline String_t* get_MinValueGetter_2() const { return ___MinValueGetter_2; }
	inline String_t** get_address_of_MinValueGetter_2() { return &___MinValueGetter_2; }
	inline void set_MinValueGetter_2(String_t* value)
	{
		___MinValueGetter_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MinValueGetter_2), (void*)value);
	}

	inline static int32_t get_offset_of_MaxValueGetter_3() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461, ___MaxValueGetter_3)); }
	inline String_t* get_MaxValueGetter_3() const { return ___MaxValueGetter_3; }
	inline String_t** get_address_of_MaxValueGetter_3() { return &___MaxValueGetter_3; }
	inline void set_MaxValueGetter_3(String_t* value)
	{
		___MaxValueGetter_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MaxValueGetter_3), (void*)value);
	}

	inline static int32_t get_offset_of_MinMaxValueGetter_4() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461, ___MinMaxValueGetter_4)); }
	inline String_t* get_MinMaxValueGetter_4() const { return ___MinMaxValueGetter_4; }
	inline String_t** get_address_of_MinMaxValueGetter_4() { return &___MinMaxValueGetter_4; }
	inline void set_MinMaxValueGetter_4(String_t* value)
	{
		___MinMaxValueGetter_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MinMaxValueGetter_4), (void*)value);
	}

	inline static int32_t get_offset_of_ShowFields_5() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461, ___ShowFields_5)); }
	inline bool get_ShowFields_5() const { return ___ShowFields_5; }
	inline bool* get_address_of_ShowFields_5() { return &___ShowFields_5; }
	inline void set_ShowFields_5(bool value)
	{
		___ShowFields_5 = value;
	}
};


// Sirenix.OdinInspector.MinValueAttribute
struct MinValueAttribute_t69E9653BC5DF176433CF88D0A72F150A73DDAFCD  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Double Sirenix.OdinInspector.MinValueAttribute::MinValue
	double ___MinValue_0;
	// System.String Sirenix.OdinInspector.MinValueAttribute::Expression
	String_t* ___Expression_1;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(MinValueAttribute_t69E9653BC5DF176433CF88D0A72F150A73DDAFCD, ___MinValue_0)); }
	inline double get_MinValue_0() const { return ___MinValue_0; }
	inline double* get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(double value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_Expression_1() { return static_cast<int32_t>(offsetof(MinValueAttribute_t69E9653BC5DF176433CF88D0A72F150A73DDAFCD, ___Expression_1)); }
	inline String_t* get_Expression_1() const { return ___Expression_1; }
	inline String_t** get_address_of_Expression_1() { return &___Expression_1; }
	inline void set_Expression_1(String_t* value)
	{
		___Expression_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Expression_1), (void*)value);
	}
};


// Sirenix.OdinInspector.MultiLinePropertyAttribute
struct MultiLinePropertyAttribute_t6C8A6AE59171CD7C3D77EE421C7FDD48D846D4CD  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 Sirenix.OdinInspector.MultiLinePropertyAttribute::Lines
	int32_t ___Lines_0;

public:
	inline static int32_t get_offset_of_Lines_0() { return static_cast<int32_t>(offsetof(MultiLinePropertyAttribute_t6C8A6AE59171CD7C3D77EE421C7FDD48D846D4CD, ___Lines_0)); }
	inline int32_t get_Lines_0() const { return ___Lines_0; }
	inline int32_t* get_address_of_Lines_0() { return &___Lines_0; }
	inline void set_Lines_0(int32_t value)
	{
		___Lines_0 = value;
	}
};


// Sirenix.OdinInspector.OdinRegisterAttributeAttribute
struct OdinRegisterAttributeAttribute_tF3FE293A763B5D61CA1902CF48AAFEAC64543A3F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type Sirenix.OdinInspector.OdinRegisterAttributeAttribute::AttributeType
	Type_t * ___AttributeType_0;
	// System.String Sirenix.OdinInspector.OdinRegisterAttributeAttribute::Categories
	String_t* ___Categories_1;
	// System.String Sirenix.OdinInspector.OdinRegisterAttributeAttribute::Description
	String_t* ___Description_2;
	// System.String Sirenix.OdinInspector.OdinRegisterAttributeAttribute::DocumentationUrl
	String_t* ___DocumentationUrl_3;
	// System.Boolean Sirenix.OdinInspector.OdinRegisterAttributeAttribute::IsEnterprise
	bool ___IsEnterprise_4;

public:
	inline static int32_t get_offset_of_AttributeType_0() { return static_cast<int32_t>(offsetof(OdinRegisterAttributeAttribute_tF3FE293A763B5D61CA1902CF48AAFEAC64543A3F, ___AttributeType_0)); }
	inline Type_t * get_AttributeType_0() const { return ___AttributeType_0; }
	inline Type_t ** get_address_of_AttributeType_0() { return &___AttributeType_0; }
	inline void set_AttributeType_0(Type_t * value)
	{
		___AttributeType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AttributeType_0), (void*)value);
	}

	inline static int32_t get_offset_of_Categories_1() { return static_cast<int32_t>(offsetof(OdinRegisterAttributeAttribute_tF3FE293A763B5D61CA1902CF48AAFEAC64543A3F, ___Categories_1)); }
	inline String_t* get_Categories_1() const { return ___Categories_1; }
	inline String_t** get_address_of_Categories_1() { return &___Categories_1; }
	inline void set_Categories_1(String_t* value)
	{
		___Categories_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Categories_1), (void*)value);
	}

	inline static int32_t get_offset_of_Description_2() { return static_cast<int32_t>(offsetof(OdinRegisterAttributeAttribute_tF3FE293A763B5D61CA1902CF48AAFEAC64543A3F, ___Description_2)); }
	inline String_t* get_Description_2() const { return ___Description_2; }
	inline String_t** get_address_of_Description_2() { return &___Description_2; }
	inline void set_Description_2(String_t* value)
	{
		___Description_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Description_2), (void*)value);
	}

	inline static int32_t get_offset_of_DocumentationUrl_3() { return static_cast<int32_t>(offsetof(OdinRegisterAttributeAttribute_tF3FE293A763B5D61CA1902CF48AAFEAC64543A3F, ___DocumentationUrl_3)); }
	inline String_t* get_DocumentationUrl_3() const { return ___DocumentationUrl_3; }
	inline String_t** get_address_of_DocumentationUrl_3() { return &___DocumentationUrl_3; }
	inline void set_DocumentationUrl_3(String_t* value)
	{
		___DocumentationUrl_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DocumentationUrl_3), (void*)value);
	}

	inline static int32_t get_offset_of_IsEnterprise_4() { return static_cast<int32_t>(offsetof(OdinRegisterAttributeAttribute_tF3FE293A763B5D61CA1902CF48AAFEAC64543A3F, ___IsEnterprise_4)); }
	inline bool get_IsEnterprise_4() const { return ___IsEnterprise_4; }
	inline bool* get_address_of_IsEnterprise_4() { return &___IsEnterprise_4; }
	inline void set_IsEnterprise_4(bool value)
	{
		___IsEnterprise_4 = value;
	}
};


// Sirenix.OdinInspector.OnCollectionChangedAttribute
struct OnCollectionChangedAttribute_tA3CC1CAE8C5A0C81E25B52D82D049EFE8615C73C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.OnCollectionChangedAttribute::Before
	String_t* ___Before_0;
	// System.String Sirenix.OdinInspector.OnCollectionChangedAttribute::After
	String_t* ___After_1;

public:
	inline static int32_t get_offset_of_Before_0() { return static_cast<int32_t>(offsetof(OnCollectionChangedAttribute_tA3CC1CAE8C5A0C81E25B52D82D049EFE8615C73C, ___Before_0)); }
	inline String_t* get_Before_0() const { return ___Before_0; }
	inline String_t** get_address_of_Before_0() { return &___Before_0; }
	inline void set_Before_0(String_t* value)
	{
		___Before_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Before_0), (void*)value);
	}

	inline static int32_t get_offset_of_After_1() { return static_cast<int32_t>(offsetof(OnCollectionChangedAttribute_tA3CC1CAE8C5A0C81E25B52D82D049EFE8615C73C, ___After_1)); }
	inline String_t* get_After_1() const { return ___After_1; }
	inline String_t** get_address_of_After_1() { return &___After_1; }
	inline void set_After_1(String_t* value)
	{
		___After_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___After_1), (void*)value);
	}
};


// Sirenix.OdinInspector.OnStateUpdateAttribute
struct OnStateUpdateAttribute_tA9908491BF4CB898A9F306ADC408624E9B34A19A  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.OnStateUpdateAttribute::Action
	String_t* ___Action_0;

public:
	inline static int32_t get_offset_of_Action_0() { return static_cast<int32_t>(offsetof(OnStateUpdateAttribute_tA9908491BF4CB898A9F306ADC408624E9B34A19A, ___Action_0)); }
	inline String_t* get_Action_0() const { return ___Action_0; }
	inline String_t** get_address_of_Action_0() { return &___Action_0; }
	inline void set_Action_0(String_t* value)
	{
		___Action_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Action_0), (void*)value);
	}
};


// Sirenix.OdinInspector.OnValueChangedAttribute
struct OnValueChangedAttribute_t833863276762F1AA2950B2A58F560B381E73940C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.OnValueChangedAttribute::Action
	String_t* ___Action_0;
	// System.Boolean Sirenix.OdinInspector.OnValueChangedAttribute::IncludeChildren
	bool ___IncludeChildren_1;
	// System.Boolean Sirenix.OdinInspector.OnValueChangedAttribute::InvokeOnUndoRedo
	bool ___InvokeOnUndoRedo_2;

public:
	inline static int32_t get_offset_of_Action_0() { return static_cast<int32_t>(offsetof(OnValueChangedAttribute_t833863276762F1AA2950B2A58F560B381E73940C, ___Action_0)); }
	inline String_t* get_Action_0() const { return ___Action_0; }
	inline String_t** get_address_of_Action_0() { return &___Action_0; }
	inline void set_Action_0(String_t* value)
	{
		___Action_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Action_0), (void*)value);
	}

	inline static int32_t get_offset_of_IncludeChildren_1() { return static_cast<int32_t>(offsetof(OnValueChangedAttribute_t833863276762F1AA2950B2A58F560B381E73940C, ___IncludeChildren_1)); }
	inline bool get_IncludeChildren_1() const { return ___IncludeChildren_1; }
	inline bool* get_address_of_IncludeChildren_1() { return &___IncludeChildren_1; }
	inline void set_IncludeChildren_1(bool value)
	{
		___IncludeChildren_1 = value;
	}

	inline static int32_t get_offset_of_InvokeOnUndoRedo_2() { return static_cast<int32_t>(offsetof(OnValueChangedAttribute_t833863276762F1AA2950B2A58F560B381E73940C, ___InvokeOnUndoRedo_2)); }
	inline bool get_InvokeOnUndoRedo_2() const { return ___InvokeOnUndoRedo_2; }
	inline bool* get_address_of_InvokeOnUndoRedo_2() { return &___InvokeOnUndoRedo_2; }
	inline void set_InvokeOnUndoRedo_2(bool value)
	{
		___InvokeOnUndoRedo_2 = value;
	}
};


// Sirenix.OdinInspector.PropertyGroupAttribute
struct PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.PropertyGroupAttribute::GroupID
	String_t* ___GroupID_0;
	// System.String Sirenix.OdinInspector.PropertyGroupAttribute::GroupName
	String_t* ___GroupName_1;
	// System.Single Sirenix.OdinInspector.PropertyGroupAttribute::Order
	float ___Order_2;
	// System.Boolean Sirenix.OdinInspector.PropertyGroupAttribute::HideWhenChildrenAreInvisible
	bool ___HideWhenChildrenAreInvisible_3;
	// System.String Sirenix.OdinInspector.PropertyGroupAttribute::VisibleIf
	String_t* ___VisibleIf_4;
	// System.Boolean Sirenix.OdinInspector.PropertyGroupAttribute::AnimateVisibility
	bool ___AnimateVisibility_5;

public:
	inline static int32_t get_offset_of_GroupID_0() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310, ___GroupID_0)); }
	inline String_t* get_GroupID_0() const { return ___GroupID_0; }
	inline String_t** get_address_of_GroupID_0() { return &___GroupID_0; }
	inline void set_GroupID_0(String_t* value)
	{
		___GroupID_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GroupID_0), (void*)value);
	}

	inline static int32_t get_offset_of_GroupName_1() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310, ___GroupName_1)); }
	inline String_t* get_GroupName_1() const { return ___GroupName_1; }
	inline String_t** get_address_of_GroupName_1() { return &___GroupName_1; }
	inline void set_GroupName_1(String_t* value)
	{
		___GroupName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GroupName_1), (void*)value);
	}

	inline static int32_t get_offset_of_Order_2() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310, ___Order_2)); }
	inline float get_Order_2() const { return ___Order_2; }
	inline float* get_address_of_Order_2() { return &___Order_2; }
	inline void set_Order_2(float value)
	{
		___Order_2 = value;
	}

	inline static int32_t get_offset_of_HideWhenChildrenAreInvisible_3() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310, ___HideWhenChildrenAreInvisible_3)); }
	inline bool get_HideWhenChildrenAreInvisible_3() const { return ___HideWhenChildrenAreInvisible_3; }
	inline bool* get_address_of_HideWhenChildrenAreInvisible_3() { return &___HideWhenChildrenAreInvisible_3; }
	inline void set_HideWhenChildrenAreInvisible_3(bool value)
	{
		___HideWhenChildrenAreInvisible_3 = value;
	}

	inline static int32_t get_offset_of_VisibleIf_4() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310, ___VisibleIf_4)); }
	inline String_t* get_VisibleIf_4() const { return ___VisibleIf_4; }
	inline String_t** get_address_of_VisibleIf_4() { return &___VisibleIf_4; }
	inline void set_VisibleIf_4(String_t* value)
	{
		___VisibleIf_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VisibleIf_4), (void*)value);
	}

	inline static int32_t get_offset_of_AnimateVisibility_5() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310, ___AnimateVisibility_5)); }
	inline bool get_AnimateVisibility_5() const { return ___AnimateVisibility_5; }
	inline bool* get_address_of_AnimateVisibility_5() { return &___AnimateVisibility_5; }
	inline void set_AnimateVisibility_5(bool value)
	{
		___AnimateVisibility_5 = value;
	}
};


// Sirenix.OdinInspector.PropertyOrderAttribute
struct PropertyOrderAttribute_t142EA22A39E39E022D9DBB6BDFD6649CAD914996  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Single Sirenix.OdinInspector.PropertyOrderAttribute::Order
	float ___Order_0;

public:
	inline static int32_t get_offset_of_Order_0() { return static_cast<int32_t>(offsetof(PropertyOrderAttribute_t142EA22A39E39E022D9DBB6BDFD6649CAD914996, ___Order_0)); }
	inline float get_Order_0() const { return ___Order_0; }
	inline float* get_address_of_Order_0() { return &___Order_0; }
	inline void set_Order_0(float value)
	{
		___Order_0 = value;
	}
};


// Sirenix.OdinInspector.PropertyRangeAttribute
struct PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Double Sirenix.OdinInspector.PropertyRangeAttribute::Min
	double ___Min_0;
	// System.Double Sirenix.OdinInspector.PropertyRangeAttribute::Max
	double ___Max_1;
	// System.String Sirenix.OdinInspector.PropertyRangeAttribute::MinGetter
	String_t* ___MinGetter_2;
	// System.String Sirenix.OdinInspector.PropertyRangeAttribute::MaxGetter
	String_t* ___MaxGetter_3;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F, ___Min_0)); }
	inline double get_Min_0() const { return ___Min_0; }
	inline double* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(double value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F, ___Max_1)); }
	inline double get_Max_1() const { return ___Max_1; }
	inline double* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(double value)
	{
		___Max_1 = value;
	}

	inline static int32_t get_offset_of_MinGetter_2() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F, ___MinGetter_2)); }
	inline String_t* get_MinGetter_2() const { return ___MinGetter_2; }
	inline String_t** get_address_of_MinGetter_2() { return &___MinGetter_2; }
	inline void set_MinGetter_2(String_t* value)
	{
		___MinGetter_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MinGetter_2), (void*)value);
	}

	inline static int32_t get_offset_of_MaxGetter_3() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F, ___MaxGetter_3)); }
	inline String_t* get_MaxGetter_3() const { return ___MaxGetter_3; }
	inline String_t** get_address_of_MaxGetter_3() { return &___MaxGetter_3; }
	inline void set_MaxGetter_3(String_t* value)
	{
		___MaxGetter_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MaxGetter_3), (void*)value);
	}
};


// Sirenix.OdinInspector.PropertySpaceAttribute
struct PropertySpaceAttribute_tB1663AC8EFAC09BE967F6C9EA8A4E04FA39B9A47  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Single Sirenix.OdinInspector.PropertySpaceAttribute::SpaceBefore
	float ___SpaceBefore_0;
	// System.Single Sirenix.OdinInspector.PropertySpaceAttribute::SpaceAfter
	float ___SpaceAfter_1;

public:
	inline static int32_t get_offset_of_SpaceBefore_0() { return static_cast<int32_t>(offsetof(PropertySpaceAttribute_tB1663AC8EFAC09BE967F6C9EA8A4E04FA39B9A47, ___SpaceBefore_0)); }
	inline float get_SpaceBefore_0() const { return ___SpaceBefore_0; }
	inline float* get_address_of_SpaceBefore_0() { return &___SpaceBefore_0; }
	inline void set_SpaceBefore_0(float value)
	{
		___SpaceBefore_0 = value;
	}

	inline static int32_t get_offset_of_SpaceAfter_1() { return static_cast<int32_t>(offsetof(PropertySpaceAttribute_tB1663AC8EFAC09BE967F6C9EA8A4E04FA39B9A47, ___SpaceAfter_1)); }
	inline float get_SpaceAfter_1() const { return ___SpaceAfter_1; }
	inline float* get_address_of_SpaceAfter_1() { return &___SpaceAfter_1; }
	inline void set_SpaceAfter_1(float value)
	{
		___SpaceAfter_1 = value;
	}
};


// Sirenix.OdinInspector.PropertyTooltipAttribute
struct PropertyTooltipAttribute_tB379999F5A87587FF4F62D7F604CC275FCB76DB5  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.PropertyTooltipAttribute::Tooltip
	String_t* ___Tooltip_0;

public:
	inline static int32_t get_offset_of_Tooltip_0() { return static_cast<int32_t>(offsetof(PropertyTooltipAttribute_tB379999F5A87587FF4F62D7F604CC275FCB76DB5, ___Tooltip_0)); }
	inline String_t* get_Tooltip_0() const { return ___Tooltip_0; }
	inline String_t** get_address_of_Tooltip_0() { return &___Tooltip_0; }
	inline void set_Tooltip_0(String_t* value)
	{
		___Tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Tooltip_0), (void*)value);
	}
};


// Sirenix.OdinInspector.ReadOnlyAttribute
struct ReadOnlyAttribute_t993E550B14FD6CBF965160564F37009B72C9F8D4  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.SceneObjectsOnlyAttribute
struct SceneObjectsOnlyAttribute_tE48AE7C0CE377268490681FAD4EC7108D742DB19  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.ShowDrawerChainAttribute
struct ShowDrawerChainAttribute_tBF48731E167BC72578A8E8FEC742BAB51E993694  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.ShowForPrefabOnlyAttribute
struct ShowForPrefabOnlyAttribute_t52839F490AF48EB0AD275E18D1F2A81392FC4BC5  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.ShowIfAttribute
struct ShowIfAttribute_tBC21F2EA58071ED9C4DBA0E5AA6967FF19C543C2  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.ShowIfAttribute::Condition
	String_t* ___Condition_0;
	// System.Object Sirenix.OdinInspector.ShowIfAttribute::Value
	RuntimeObject * ___Value_1;
	// System.Boolean Sirenix.OdinInspector.ShowIfAttribute::Animate
	bool ___Animate_2;

public:
	inline static int32_t get_offset_of_Condition_0() { return static_cast<int32_t>(offsetof(ShowIfAttribute_tBC21F2EA58071ED9C4DBA0E5AA6967FF19C543C2, ___Condition_0)); }
	inline String_t* get_Condition_0() const { return ___Condition_0; }
	inline String_t** get_address_of_Condition_0() { return &___Condition_0; }
	inline void set_Condition_0(String_t* value)
	{
		___Condition_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Condition_0), (void*)value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(ShowIfAttribute_tBC21F2EA58071ED9C4DBA0E5AA6967FF19C543C2, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_1), (void*)value);
	}

	inline static int32_t get_offset_of_Animate_2() { return static_cast<int32_t>(offsetof(ShowIfAttribute_tBC21F2EA58071ED9C4DBA0E5AA6967FF19C543C2, ___Animate_2)); }
	inline bool get_Animate_2() const { return ___Animate_2; }
	inline bool* get_address_of_Animate_2() { return &___Animate_2; }
	inline void set_Animate_2(bool value)
	{
		___Animate_2 = value;
	}
};


// Sirenix.OdinInspector.ShowInInlineEditorsAttribute
struct ShowInInlineEditorsAttribute_t51DF69747A3CE1570890953F2B8536E8082867CD  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.ShowInInspectorAttribute
struct ShowInInspectorAttribute_t035C9790B6F7942286B0D6607580AB3393B1115B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute
struct ShowOdinSerializedPropertiesInInspectorAttribute_t85D03D92F6E429EE8CA403011C588CC9AD651DFE  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.ShowPropertyResolverAttribute
struct ShowPropertyResolverAttribute_t3C66CA8BD48FC58876E4DC2633C0D4B5B660C633  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.SuffixLabelAttribute
struct SuffixLabelAttribute_t7B942FEB352DA81BAE4FE6C86A26838563BE3942  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.SuffixLabelAttribute::Label
	String_t* ___Label_0;
	// System.Boolean Sirenix.OdinInspector.SuffixLabelAttribute::Overlay
	bool ___Overlay_1;

public:
	inline static int32_t get_offset_of_Label_0() { return static_cast<int32_t>(offsetof(SuffixLabelAttribute_t7B942FEB352DA81BAE4FE6C86A26838563BE3942, ___Label_0)); }
	inline String_t* get_Label_0() const { return ___Label_0; }
	inline String_t** get_address_of_Label_0() { return &___Label_0; }
	inline void set_Label_0(String_t* value)
	{
		___Label_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Label_0), (void*)value);
	}

	inline static int32_t get_offset_of_Overlay_1() { return static_cast<int32_t>(offsetof(SuffixLabelAttribute_t7B942FEB352DA81BAE4FE6C86A26838563BE3942, ___Overlay_1)); }
	inline bool get_Overlay_1() const { return ___Overlay_1; }
	inline bool* get_address_of_Overlay_1() { return &___Overlay_1; }
	inline void set_Overlay_1(bool value)
	{
		___Overlay_1 = value;
	}
};


// Sirenix.OdinInspector.SuppressInvalidAttributeErrorAttribute
struct SuppressInvalidAttributeErrorAttribute_tD041EA2EC15B10F0A0E7B48E887B652EE6C6C427  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.TableColumnWidthAttribute
struct TableColumnWidthAttribute_t45DDD415E5FB012D5979A1A163D214EFF48FDB2A  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 Sirenix.OdinInspector.TableColumnWidthAttribute::Width
	int32_t ___Width_0;
	// System.Boolean Sirenix.OdinInspector.TableColumnWidthAttribute::Resizable
	bool ___Resizable_1;

public:
	inline static int32_t get_offset_of_Width_0() { return static_cast<int32_t>(offsetof(TableColumnWidthAttribute_t45DDD415E5FB012D5979A1A163D214EFF48FDB2A, ___Width_0)); }
	inline int32_t get_Width_0() const { return ___Width_0; }
	inline int32_t* get_address_of_Width_0() { return &___Width_0; }
	inline void set_Width_0(int32_t value)
	{
		___Width_0 = value;
	}

	inline static int32_t get_offset_of_Resizable_1() { return static_cast<int32_t>(offsetof(TableColumnWidthAttribute_t45DDD415E5FB012D5979A1A163D214EFF48FDB2A, ___Resizable_1)); }
	inline bool get_Resizable_1() const { return ___Resizable_1; }
	inline bool* get_address_of_Resizable_1() { return &___Resizable_1; }
	inline void set_Resizable_1(bool value)
	{
		___Resizable_1 = value;
	}
};


// Sirenix.OdinInspector.TableListAttribute
struct TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::NumberOfItemsPerPage
	int32_t ___NumberOfItemsPerPage_0;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::IsReadOnly
	bool ___IsReadOnly_1;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::DefaultMinColumnWidth
	int32_t ___DefaultMinColumnWidth_2;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::ShowIndexLabels
	bool ___ShowIndexLabels_3;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::DrawScrollView
	bool ___DrawScrollView_4;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::MinScrollViewHeight
	int32_t ___MinScrollViewHeight_5;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::MaxScrollViewHeight
	int32_t ___MaxScrollViewHeight_6;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::AlwaysExpanded
	bool ___AlwaysExpanded_7;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::HideToolbar
	bool ___HideToolbar_8;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::CellPadding
	int32_t ___CellPadding_9;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::showPagingHasValue
	bool ___showPagingHasValue_10;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::showPaging
	bool ___showPaging_11;

public:
	inline static int32_t get_offset_of_NumberOfItemsPerPage_0() { return static_cast<int32_t>(offsetof(TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5, ___NumberOfItemsPerPage_0)); }
	inline int32_t get_NumberOfItemsPerPage_0() const { return ___NumberOfItemsPerPage_0; }
	inline int32_t* get_address_of_NumberOfItemsPerPage_0() { return &___NumberOfItemsPerPage_0; }
	inline void set_NumberOfItemsPerPage_0(int32_t value)
	{
		___NumberOfItemsPerPage_0 = value;
	}

	inline static int32_t get_offset_of_IsReadOnly_1() { return static_cast<int32_t>(offsetof(TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5, ___IsReadOnly_1)); }
	inline bool get_IsReadOnly_1() const { return ___IsReadOnly_1; }
	inline bool* get_address_of_IsReadOnly_1() { return &___IsReadOnly_1; }
	inline void set_IsReadOnly_1(bool value)
	{
		___IsReadOnly_1 = value;
	}

	inline static int32_t get_offset_of_DefaultMinColumnWidth_2() { return static_cast<int32_t>(offsetof(TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5, ___DefaultMinColumnWidth_2)); }
	inline int32_t get_DefaultMinColumnWidth_2() const { return ___DefaultMinColumnWidth_2; }
	inline int32_t* get_address_of_DefaultMinColumnWidth_2() { return &___DefaultMinColumnWidth_2; }
	inline void set_DefaultMinColumnWidth_2(int32_t value)
	{
		___DefaultMinColumnWidth_2 = value;
	}

	inline static int32_t get_offset_of_ShowIndexLabels_3() { return static_cast<int32_t>(offsetof(TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5, ___ShowIndexLabels_3)); }
	inline bool get_ShowIndexLabels_3() const { return ___ShowIndexLabels_3; }
	inline bool* get_address_of_ShowIndexLabels_3() { return &___ShowIndexLabels_3; }
	inline void set_ShowIndexLabels_3(bool value)
	{
		___ShowIndexLabels_3 = value;
	}

	inline static int32_t get_offset_of_DrawScrollView_4() { return static_cast<int32_t>(offsetof(TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5, ___DrawScrollView_4)); }
	inline bool get_DrawScrollView_4() const { return ___DrawScrollView_4; }
	inline bool* get_address_of_DrawScrollView_4() { return &___DrawScrollView_4; }
	inline void set_DrawScrollView_4(bool value)
	{
		___DrawScrollView_4 = value;
	}

	inline static int32_t get_offset_of_MinScrollViewHeight_5() { return static_cast<int32_t>(offsetof(TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5, ___MinScrollViewHeight_5)); }
	inline int32_t get_MinScrollViewHeight_5() const { return ___MinScrollViewHeight_5; }
	inline int32_t* get_address_of_MinScrollViewHeight_5() { return &___MinScrollViewHeight_5; }
	inline void set_MinScrollViewHeight_5(int32_t value)
	{
		___MinScrollViewHeight_5 = value;
	}

	inline static int32_t get_offset_of_MaxScrollViewHeight_6() { return static_cast<int32_t>(offsetof(TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5, ___MaxScrollViewHeight_6)); }
	inline int32_t get_MaxScrollViewHeight_6() const { return ___MaxScrollViewHeight_6; }
	inline int32_t* get_address_of_MaxScrollViewHeight_6() { return &___MaxScrollViewHeight_6; }
	inline void set_MaxScrollViewHeight_6(int32_t value)
	{
		___MaxScrollViewHeight_6 = value;
	}

	inline static int32_t get_offset_of_AlwaysExpanded_7() { return static_cast<int32_t>(offsetof(TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5, ___AlwaysExpanded_7)); }
	inline bool get_AlwaysExpanded_7() const { return ___AlwaysExpanded_7; }
	inline bool* get_address_of_AlwaysExpanded_7() { return &___AlwaysExpanded_7; }
	inline void set_AlwaysExpanded_7(bool value)
	{
		___AlwaysExpanded_7 = value;
	}

	inline static int32_t get_offset_of_HideToolbar_8() { return static_cast<int32_t>(offsetof(TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5, ___HideToolbar_8)); }
	inline bool get_HideToolbar_8() const { return ___HideToolbar_8; }
	inline bool* get_address_of_HideToolbar_8() { return &___HideToolbar_8; }
	inline void set_HideToolbar_8(bool value)
	{
		___HideToolbar_8 = value;
	}

	inline static int32_t get_offset_of_CellPadding_9() { return static_cast<int32_t>(offsetof(TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5, ___CellPadding_9)); }
	inline int32_t get_CellPadding_9() const { return ___CellPadding_9; }
	inline int32_t* get_address_of_CellPadding_9() { return &___CellPadding_9; }
	inline void set_CellPadding_9(int32_t value)
	{
		___CellPadding_9 = value;
	}

	inline static int32_t get_offset_of_showPagingHasValue_10() { return static_cast<int32_t>(offsetof(TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5, ___showPagingHasValue_10)); }
	inline bool get_showPagingHasValue_10() const { return ___showPagingHasValue_10; }
	inline bool* get_address_of_showPagingHasValue_10() { return &___showPagingHasValue_10; }
	inline void set_showPagingHasValue_10(bool value)
	{
		___showPagingHasValue_10 = value;
	}

	inline static int32_t get_offset_of_showPaging_11() { return static_cast<int32_t>(offsetof(TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5, ___showPaging_11)); }
	inline bool get_showPaging_11() const { return ___showPaging_11; }
	inline bool* get_address_of_showPaging_11() { return &___showPaging_11; }
	inline void set_showPaging_11(bool value)
	{
		___showPaging_11 = value;
	}
};


// Sirenix.OdinInspector.TableMatrixAttribute
struct TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::IsReadOnly
	bool ___IsReadOnly_0;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::ResizableColumns
	bool ___ResizableColumns_1;
	// System.String Sirenix.OdinInspector.TableMatrixAttribute::VerticalTitle
	String_t* ___VerticalTitle_2;
	// System.String Sirenix.OdinInspector.TableMatrixAttribute::HorizontalTitle
	String_t* ___HorizontalTitle_3;
	// System.String Sirenix.OdinInspector.TableMatrixAttribute::DrawElementMethod
	String_t* ___DrawElementMethod_4;
	// System.Int32 Sirenix.OdinInspector.TableMatrixAttribute::RowHeight
	int32_t ___RowHeight_5;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::SquareCells
	bool ___SquareCells_6;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::HideColumnIndices
	bool ___HideColumnIndices_7;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::HideRowIndices
	bool ___HideRowIndices_8;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::RespectIndentLevel
	bool ___RespectIndentLevel_9;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::Transpose
	bool ___Transpose_10;

public:
	inline static int32_t get_offset_of_IsReadOnly_0() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C, ___IsReadOnly_0)); }
	inline bool get_IsReadOnly_0() const { return ___IsReadOnly_0; }
	inline bool* get_address_of_IsReadOnly_0() { return &___IsReadOnly_0; }
	inline void set_IsReadOnly_0(bool value)
	{
		___IsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_ResizableColumns_1() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C, ___ResizableColumns_1)); }
	inline bool get_ResizableColumns_1() const { return ___ResizableColumns_1; }
	inline bool* get_address_of_ResizableColumns_1() { return &___ResizableColumns_1; }
	inline void set_ResizableColumns_1(bool value)
	{
		___ResizableColumns_1 = value;
	}

	inline static int32_t get_offset_of_VerticalTitle_2() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C, ___VerticalTitle_2)); }
	inline String_t* get_VerticalTitle_2() const { return ___VerticalTitle_2; }
	inline String_t** get_address_of_VerticalTitle_2() { return &___VerticalTitle_2; }
	inline void set_VerticalTitle_2(String_t* value)
	{
		___VerticalTitle_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VerticalTitle_2), (void*)value);
	}

	inline static int32_t get_offset_of_HorizontalTitle_3() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C, ___HorizontalTitle_3)); }
	inline String_t* get_HorizontalTitle_3() const { return ___HorizontalTitle_3; }
	inline String_t** get_address_of_HorizontalTitle_3() { return &___HorizontalTitle_3; }
	inline void set_HorizontalTitle_3(String_t* value)
	{
		___HorizontalTitle_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HorizontalTitle_3), (void*)value);
	}

	inline static int32_t get_offset_of_DrawElementMethod_4() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C, ___DrawElementMethod_4)); }
	inline String_t* get_DrawElementMethod_4() const { return ___DrawElementMethod_4; }
	inline String_t** get_address_of_DrawElementMethod_4() { return &___DrawElementMethod_4; }
	inline void set_DrawElementMethod_4(String_t* value)
	{
		___DrawElementMethod_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DrawElementMethod_4), (void*)value);
	}

	inline static int32_t get_offset_of_RowHeight_5() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C, ___RowHeight_5)); }
	inline int32_t get_RowHeight_5() const { return ___RowHeight_5; }
	inline int32_t* get_address_of_RowHeight_5() { return &___RowHeight_5; }
	inline void set_RowHeight_5(int32_t value)
	{
		___RowHeight_5 = value;
	}

	inline static int32_t get_offset_of_SquareCells_6() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C, ___SquareCells_6)); }
	inline bool get_SquareCells_6() const { return ___SquareCells_6; }
	inline bool* get_address_of_SquareCells_6() { return &___SquareCells_6; }
	inline void set_SquareCells_6(bool value)
	{
		___SquareCells_6 = value;
	}

	inline static int32_t get_offset_of_HideColumnIndices_7() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C, ___HideColumnIndices_7)); }
	inline bool get_HideColumnIndices_7() const { return ___HideColumnIndices_7; }
	inline bool* get_address_of_HideColumnIndices_7() { return &___HideColumnIndices_7; }
	inline void set_HideColumnIndices_7(bool value)
	{
		___HideColumnIndices_7 = value;
	}

	inline static int32_t get_offset_of_HideRowIndices_8() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C, ___HideRowIndices_8)); }
	inline bool get_HideRowIndices_8() const { return ___HideRowIndices_8; }
	inline bool* get_address_of_HideRowIndices_8() { return &___HideRowIndices_8; }
	inline void set_HideRowIndices_8(bool value)
	{
		___HideRowIndices_8 = value;
	}

	inline static int32_t get_offset_of_RespectIndentLevel_9() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C, ___RespectIndentLevel_9)); }
	inline bool get_RespectIndentLevel_9() const { return ___RespectIndentLevel_9; }
	inline bool* get_address_of_RespectIndentLevel_9() { return &___RespectIndentLevel_9; }
	inline void set_RespectIndentLevel_9(bool value)
	{
		___RespectIndentLevel_9 = value;
	}

	inline static int32_t get_offset_of_Transpose_10() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C, ___Transpose_10)); }
	inline bool get_Transpose_10() const { return ___Transpose_10; }
	inline bool* get_address_of_Transpose_10() { return &___Transpose_10; }
	inline void set_Transpose_10(bool value)
	{
		___Transpose_10 = value;
	}
};


// Sirenix.OdinInspector.ToggleAttribute
struct ToggleAttribute_t4612A2D02256B1199715ACC4C56E742576D3C6B5  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.ToggleAttribute::ToggleMemberName
	String_t* ___ToggleMemberName_0;
	// System.Boolean Sirenix.OdinInspector.ToggleAttribute::CollapseOthersOnExpand
	bool ___CollapseOthersOnExpand_1;

public:
	inline static int32_t get_offset_of_ToggleMemberName_0() { return static_cast<int32_t>(offsetof(ToggleAttribute_t4612A2D02256B1199715ACC4C56E742576D3C6B5, ___ToggleMemberName_0)); }
	inline String_t* get_ToggleMemberName_0() const { return ___ToggleMemberName_0; }
	inline String_t** get_address_of_ToggleMemberName_0() { return &___ToggleMemberName_0; }
	inline void set_ToggleMemberName_0(String_t* value)
	{
		___ToggleMemberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ToggleMemberName_0), (void*)value);
	}

	inline static int32_t get_offset_of_CollapseOthersOnExpand_1() { return static_cast<int32_t>(offsetof(ToggleAttribute_t4612A2D02256B1199715ACC4C56E742576D3C6B5, ___CollapseOthersOnExpand_1)); }
	inline bool get_CollapseOthersOnExpand_1() const { return ___CollapseOthersOnExpand_1; }
	inline bool* get_address_of_CollapseOthersOnExpand_1() { return &___CollapseOthersOnExpand_1; }
	inline void set_CollapseOthersOnExpand_1(bool value)
	{
		___CollapseOthersOnExpand_1 = value;
	}
};


// Sirenix.OdinInspector.ToggleLeftAttribute
struct ToggleLeftAttribute_t6E6A2BD826555DA4871D9BE2A6BB0CA822D7D525  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Sirenix.OdinInspector.TypeFilterAttribute
struct TypeFilterAttribute_t367745A090DA5E0ED152768927A09BE02DE708E8  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.TypeFilterAttribute::FilterGetter
	String_t* ___FilterGetter_0;
	// System.String Sirenix.OdinInspector.TypeFilterAttribute::DropdownTitle
	String_t* ___DropdownTitle_1;

public:
	inline static int32_t get_offset_of_FilterGetter_0() { return static_cast<int32_t>(offsetof(TypeFilterAttribute_t367745A090DA5E0ED152768927A09BE02DE708E8, ___FilterGetter_0)); }
	inline String_t* get_FilterGetter_0() const { return ___FilterGetter_0; }
	inline String_t** get_address_of_FilterGetter_0() { return &___FilterGetter_0; }
	inline void set_FilterGetter_0(String_t* value)
	{
		___FilterGetter_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterGetter_0), (void*)value);
	}

	inline static int32_t get_offset_of_DropdownTitle_1() { return static_cast<int32_t>(offsetof(TypeFilterAttribute_t367745A090DA5E0ED152768927A09BE02DE708E8, ___DropdownTitle_1)); }
	inline String_t* get_DropdownTitle_1() const { return ___DropdownTitle_1; }
	inline String_t** get_address_of_DropdownTitle_1() { return &___DropdownTitle_1; }
	inline void set_DropdownTitle_1(String_t* value)
	{
		___DropdownTitle_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DropdownTitle_1), (void*)value);
	}
};


// Sirenix.OdinInspector.TypeInfoBoxAttribute
struct TypeInfoBoxAttribute_t884BA53D12B62C5C7E9F1AA20525F4D2898A16E8  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.TypeInfoBoxAttribute::Message
	String_t* ___Message_0;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(TypeInfoBoxAttribute_t884BA53D12B62C5C7E9F1AA20525F4D2898A16E8, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Message_0), (void*)value);
	}
};


// Sirenix.OdinInspector.ValueDropdownAttribute
struct ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.ValueDropdownAttribute::ValuesGetter
	String_t* ___ValuesGetter_0;
	// System.Int32 Sirenix.OdinInspector.ValueDropdownAttribute::NumberOfItemsBeforeEnablingSearch
	int32_t ___NumberOfItemsBeforeEnablingSearch_1;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::IsUniqueList
	bool ___IsUniqueList_2;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DrawDropdownForListElements
	bool ___DrawDropdownForListElements_3;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DisableListAddButtonBehaviour
	bool ___DisableListAddButtonBehaviour_4;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::ExcludeExistingValuesInList
	bool ___ExcludeExistingValuesInList_5;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::ExpandAllMenuItems
	bool ___ExpandAllMenuItems_6;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::AppendNextDrawer
	bool ___AppendNextDrawer_7;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DisableGUIInAppendedDrawer
	bool ___DisableGUIInAppendedDrawer_8;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DoubleClickToConfirm
	bool ___DoubleClickToConfirm_9;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::FlattenTreeView
	bool ___FlattenTreeView_10;
	// System.Int32 Sirenix.OdinInspector.ValueDropdownAttribute::DropdownWidth
	int32_t ___DropdownWidth_11;
	// System.Int32 Sirenix.OdinInspector.ValueDropdownAttribute::DropdownHeight
	int32_t ___DropdownHeight_12;
	// System.String Sirenix.OdinInspector.ValueDropdownAttribute::DropdownTitle
	String_t* ___DropdownTitle_13;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::SortDropdownItems
	bool ___SortDropdownItems_14;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::HideChildProperties
	bool ___HideChildProperties_15;

public:
	inline static int32_t get_offset_of_ValuesGetter_0() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___ValuesGetter_0)); }
	inline String_t* get_ValuesGetter_0() const { return ___ValuesGetter_0; }
	inline String_t** get_address_of_ValuesGetter_0() { return &___ValuesGetter_0; }
	inline void set_ValuesGetter_0(String_t* value)
	{
		___ValuesGetter_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ValuesGetter_0), (void*)value);
	}

	inline static int32_t get_offset_of_NumberOfItemsBeforeEnablingSearch_1() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___NumberOfItemsBeforeEnablingSearch_1)); }
	inline int32_t get_NumberOfItemsBeforeEnablingSearch_1() const { return ___NumberOfItemsBeforeEnablingSearch_1; }
	inline int32_t* get_address_of_NumberOfItemsBeforeEnablingSearch_1() { return &___NumberOfItemsBeforeEnablingSearch_1; }
	inline void set_NumberOfItemsBeforeEnablingSearch_1(int32_t value)
	{
		___NumberOfItemsBeforeEnablingSearch_1 = value;
	}

	inline static int32_t get_offset_of_IsUniqueList_2() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___IsUniqueList_2)); }
	inline bool get_IsUniqueList_2() const { return ___IsUniqueList_2; }
	inline bool* get_address_of_IsUniqueList_2() { return &___IsUniqueList_2; }
	inline void set_IsUniqueList_2(bool value)
	{
		___IsUniqueList_2 = value;
	}

	inline static int32_t get_offset_of_DrawDropdownForListElements_3() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___DrawDropdownForListElements_3)); }
	inline bool get_DrawDropdownForListElements_3() const { return ___DrawDropdownForListElements_3; }
	inline bool* get_address_of_DrawDropdownForListElements_3() { return &___DrawDropdownForListElements_3; }
	inline void set_DrawDropdownForListElements_3(bool value)
	{
		___DrawDropdownForListElements_3 = value;
	}

	inline static int32_t get_offset_of_DisableListAddButtonBehaviour_4() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___DisableListAddButtonBehaviour_4)); }
	inline bool get_DisableListAddButtonBehaviour_4() const { return ___DisableListAddButtonBehaviour_4; }
	inline bool* get_address_of_DisableListAddButtonBehaviour_4() { return &___DisableListAddButtonBehaviour_4; }
	inline void set_DisableListAddButtonBehaviour_4(bool value)
	{
		___DisableListAddButtonBehaviour_4 = value;
	}

	inline static int32_t get_offset_of_ExcludeExistingValuesInList_5() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___ExcludeExistingValuesInList_5)); }
	inline bool get_ExcludeExistingValuesInList_5() const { return ___ExcludeExistingValuesInList_5; }
	inline bool* get_address_of_ExcludeExistingValuesInList_5() { return &___ExcludeExistingValuesInList_5; }
	inline void set_ExcludeExistingValuesInList_5(bool value)
	{
		___ExcludeExistingValuesInList_5 = value;
	}

	inline static int32_t get_offset_of_ExpandAllMenuItems_6() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___ExpandAllMenuItems_6)); }
	inline bool get_ExpandAllMenuItems_6() const { return ___ExpandAllMenuItems_6; }
	inline bool* get_address_of_ExpandAllMenuItems_6() { return &___ExpandAllMenuItems_6; }
	inline void set_ExpandAllMenuItems_6(bool value)
	{
		___ExpandAllMenuItems_6 = value;
	}

	inline static int32_t get_offset_of_AppendNextDrawer_7() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___AppendNextDrawer_7)); }
	inline bool get_AppendNextDrawer_7() const { return ___AppendNextDrawer_7; }
	inline bool* get_address_of_AppendNextDrawer_7() { return &___AppendNextDrawer_7; }
	inline void set_AppendNextDrawer_7(bool value)
	{
		___AppendNextDrawer_7 = value;
	}

	inline static int32_t get_offset_of_DisableGUIInAppendedDrawer_8() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___DisableGUIInAppendedDrawer_8)); }
	inline bool get_DisableGUIInAppendedDrawer_8() const { return ___DisableGUIInAppendedDrawer_8; }
	inline bool* get_address_of_DisableGUIInAppendedDrawer_8() { return &___DisableGUIInAppendedDrawer_8; }
	inline void set_DisableGUIInAppendedDrawer_8(bool value)
	{
		___DisableGUIInAppendedDrawer_8 = value;
	}

	inline static int32_t get_offset_of_DoubleClickToConfirm_9() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___DoubleClickToConfirm_9)); }
	inline bool get_DoubleClickToConfirm_9() const { return ___DoubleClickToConfirm_9; }
	inline bool* get_address_of_DoubleClickToConfirm_9() { return &___DoubleClickToConfirm_9; }
	inline void set_DoubleClickToConfirm_9(bool value)
	{
		___DoubleClickToConfirm_9 = value;
	}

	inline static int32_t get_offset_of_FlattenTreeView_10() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___FlattenTreeView_10)); }
	inline bool get_FlattenTreeView_10() const { return ___FlattenTreeView_10; }
	inline bool* get_address_of_FlattenTreeView_10() { return &___FlattenTreeView_10; }
	inline void set_FlattenTreeView_10(bool value)
	{
		___FlattenTreeView_10 = value;
	}

	inline static int32_t get_offset_of_DropdownWidth_11() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___DropdownWidth_11)); }
	inline int32_t get_DropdownWidth_11() const { return ___DropdownWidth_11; }
	inline int32_t* get_address_of_DropdownWidth_11() { return &___DropdownWidth_11; }
	inline void set_DropdownWidth_11(int32_t value)
	{
		___DropdownWidth_11 = value;
	}

	inline static int32_t get_offset_of_DropdownHeight_12() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___DropdownHeight_12)); }
	inline int32_t get_DropdownHeight_12() const { return ___DropdownHeight_12; }
	inline int32_t* get_address_of_DropdownHeight_12() { return &___DropdownHeight_12; }
	inline void set_DropdownHeight_12(int32_t value)
	{
		___DropdownHeight_12 = value;
	}

	inline static int32_t get_offset_of_DropdownTitle_13() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___DropdownTitle_13)); }
	inline String_t* get_DropdownTitle_13() const { return ___DropdownTitle_13; }
	inline String_t** get_address_of_DropdownTitle_13() { return &___DropdownTitle_13; }
	inline void set_DropdownTitle_13(String_t* value)
	{
		___DropdownTitle_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DropdownTitle_13), (void*)value);
	}

	inline static int32_t get_offset_of_SortDropdownItems_14() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___SortDropdownItems_14)); }
	inline bool get_SortDropdownItems_14() const { return ___SortDropdownItems_14; }
	inline bool* get_address_of_SortDropdownItems_14() { return &___SortDropdownItems_14; }
	inline void set_SortDropdownItems_14(bool value)
	{
		___SortDropdownItems_14 = value;
	}

	inline static int32_t get_offset_of_HideChildProperties_15() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478, ___HideChildProperties_15)); }
	inline bool get_HideChildProperties_15() const { return ___HideChildProperties_15; }
	inline bool* get_address_of_HideChildProperties_15() { return &___HideChildProperties_15; }
	inline void set_HideChildProperties_15(bool value)
	{
		___HideChildProperties_15 = value;
	}
};


// Sirenix.OdinInspector.ValueDropdownItem
struct ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 
{
public:
	// System.String Sirenix.OdinInspector.ValueDropdownItem::Text
	String_t* ___Text_0;
	// System.Object Sirenix.OdinInspector.ValueDropdownItem::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Text_0() { return static_cast<int32_t>(offsetof(ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99, ___Text_0)); }
	inline String_t* get_Text_0() const { return ___Text_0; }
	inline String_t** get_address_of_Text_0() { return &___Text_0; }
	inline void set_Text_0(String_t* value)
	{
		___Text_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text_0), (void*)value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Sirenix.OdinInspector.ValueDropdownItem
struct ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshaled_pinvoke
{
	char* ___Text_0;
	Il2CppIUnknown* ___Value_1;
};
// Native definition for COM marshalling of Sirenix.OdinInspector.ValueDropdownItem
struct ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshaled_com
{
	Il2CppChar* ___Text_0;
	Il2CppIUnknown* ___Value_1;
};

// Sirenix.OdinInspector.WrapAttribute
struct WrapAttribute_t1359861280D8003582A215C7DF8F794A72B60437  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Double Sirenix.OdinInspector.WrapAttribute::Min
	double ___Min_0;
	// System.Double Sirenix.OdinInspector.WrapAttribute::Max
	double ___Max_1;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(WrapAttribute_t1359861280D8003582A215C7DF8F794A72B60437, ___Min_0)); }
	inline double get_Min_0() const { return ___Min_0; }
	inline double* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(double value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(WrapAttribute_t1359861280D8003582A215C7DF8F794A72B60437, ___Max_1)); }
	inline double get_Max_1() const { return ___Max_1; }
	inline double* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(double value)
	{
		___Max_1 = value;
	}
};


// System.Boolean
struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___list_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_list_0() const { return ___list_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.String>
struct Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819, ___list_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_list_0() const { return ___list_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Double
struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// Sirenix.OdinInspector.BoxGroupAttribute
struct BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283  : public PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310
{
public:
	// System.Boolean Sirenix.OdinInspector.BoxGroupAttribute::ShowLabel
	bool ___ShowLabel_6;
	// System.Boolean Sirenix.OdinInspector.BoxGroupAttribute::CenterLabel
	bool ___CenterLabel_7;
	// System.String Sirenix.OdinInspector.BoxGroupAttribute::LabelText
	String_t* ___LabelText_8;

public:
	inline static int32_t get_offset_of_ShowLabel_6() { return static_cast<int32_t>(offsetof(BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283, ___ShowLabel_6)); }
	inline bool get_ShowLabel_6() const { return ___ShowLabel_6; }
	inline bool* get_address_of_ShowLabel_6() { return &___ShowLabel_6; }
	inline void set_ShowLabel_6(bool value)
	{
		___ShowLabel_6 = value;
	}

	inline static int32_t get_offset_of_CenterLabel_7() { return static_cast<int32_t>(offsetof(BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283, ___CenterLabel_7)); }
	inline bool get_CenterLabel_7() const { return ___CenterLabel_7; }
	inline bool* get_address_of_CenterLabel_7() { return &___CenterLabel_7; }
	inline void set_CenterLabel_7(bool value)
	{
		___CenterLabel_7 = value;
	}

	inline static int32_t get_offset_of_LabelText_8() { return static_cast<int32_t>(offsetof(BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283, ___LabelText_8)); }
	inline String_t* get_LabelText_8() const { return ___LabelText_8; }
	inline String_t** get_address_of_LabelText_8() { return &___LabelText_8; }
	inline void set_LabelText_8(String_t* value)
	{
		___LabelText_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LabelText_8), (void*)value);
	}
};


// Sirenix.OdinInspector.ButtonGroupAttribute
struct ButtonGroupAttribute_tCDADB0FE0D3E9BFA27DF33D6451869B8EC8E857C  : public PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310
{
public:

public:
};


// Sirenix.OdinInspector.ButtonSizes
struct ButtonSizes_tD396A87EF9A952FFADF5BE80BDF2B73F041B4F7A 
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonSizes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonSizes_tD396A87EF9A952FFADF5BE80BDF2B73F041B4F7A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.ButtonStyle
struct ButtonStyle_t6AEAFCC00C701AB514286D8B7223D2BDCFA4E301 
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStyle_t6AEAFCC00C701AB514286D8B7223D2BDCFA4E301, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.DictionaryDisplayOptions
struct DictionaryDisplayOptions_t234B77158AEEF07A6AE40940D7BCFBC661CC9E98 
{
public:
	// System.Int32 Sirenix.OdinInspector.DictionaryDisplayOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DictionaryDisplayOptions_t234B77158AEEF07A6AE40940D7BCFBC661CC9E98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.FoldoutGroupAttribute
struct FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD  : public PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310
{
public:
	// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::expanded
	bool ___expanded_6;
	// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::<HasDefinedExpanded>k__BackingField
	bool ___U3CHasDefinedExpandedU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_expanded_6() { return static_cast<int32_t>(offsetof(FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD, ___expanded_6)); }
	inline bool get_expanded_6() const { return ___expanded_6; }
	inline bool* get_address_of_expanded_6() { return &___expanded_6; }
	inline void set_expanded_6(bool value)
	{
		___expanded_6 = value;
	}

	inline static int32_t get_offset_of_U3CHasDefinedExpandedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD, ___U3CHasDefinedExpandedU3Ek__BackingField_7)); }
	inline bool get_U3CHasDefinedExpandedU3Ek__BackingField_7() const { return ___U3CHasDefinedExpandedU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CHasDefinedExpandedU3Ek__BackingField_7() { return &___U3CHasDefinedExpandedU3Ek__BackingField_7; }
	inline void set_U3CHasDefinedExpandedU3Ek__BackingField_7(bool value)
	{
		___U3CHasDefinedExpandedU3Ek__BackingField_7 = value;
	}
};


// Sirenix.OdinInspector.GUIColorAttribute
struct GUIColorAttribute_tDE7A1E234D4277B5079B9921967945DB5CA56B03  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// UnityEngine.Color Sirenix.OdinInspector.GUIColorAttribute::Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___Color_0;
	// System.String Sirenix.OdinInspector.GUIColorAttribute::GetColor
	String_t* ___GetColor_1;

public:
	inline static int32_t get_offset_of_Color_0() { return static_cast<int32_t>(offsetof(GUIColorAttribute_tDE7A1E234D4277B5079B9921967945DB5CA56B03, ___Color_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_Color_0() const { return ___Color_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_Color_0() { return &___Color_0; }
	inline void set_Color_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___Color_0 = value;
	}

	inline static int32_t get_offset_of_GetColor_1() { return static_cast<int32_t>(offsetof(GUIColorAttribute_tDE7A1E234D4277B5079B9921967945DB5CA56B03, ___GetColor_1)); }
	inline String_t* get_GetColor_1() const { return ___GetColor_1; }
	inline String_t** get_address_of_GetColor_1() { return &___GetColor_1; }
	inline void set_GetColor_1(String_t* value)
	{
		___GetColor_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GetColor_1), (void*)value);
	}
};


// Sirenix.OdinInspector.HideIfGroupAttribute
struct HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238  : public PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310
{
public:
	// System.Object Sirenix.OdinInspector.HideIfGroupAttribute::Value
	RuntimeObject * ___Value_6;

public:
	inline static int32_t get_offset_of_Value_6() { return static_cast<int32_t>(offsetof(HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238, ___Value_6)); }
	inline RuntimeObject * get_Value_6() const { return ___Value_6; }
	inline RuntimeObject ** get_address_of_Value_6() { return &___Value_6; }
	inline void set_Value_6(RuntimeObject * value)
	{
		___Value_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_6), (void*)value);
	}
};


// Sirenix.OdinInspector.HorizontalGroupAttribute
struct HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5  : public PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310
{
public:
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::Width
	float ___Width_6;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MarginLeft
	float ___MarginLeft_7;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MarginRight
	float ___MarginRight_8;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::PaddingLeft
	float ___PaddingLeft_9;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::PaddingRight
	float ___PaddingRight_10;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MinWidth
	float ___MinWidth_11;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MaxWidth
	float ___MaxWidth_12;
	// System.String Sirenix.OdinInspector.HorizontalGroupAttribute::Title
	String_t* ___Title_13;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::LabelWidth
	float ___LabelWidth_14;

public:
	inline static int32_t get_offset_of_Width_6() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5, ___Width_6)); }
	inline float get_Width_6() const { return ___Width_6; }
	inline float* get_address_of_Width_6() { return &___Width_6; }
	inline void set_Width_6(float value)
	{
		___Width_6 = value;
	}

	inline static int32_t get_offset_of_MarginLeft_7() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5, ___MarginLeft_7)); }
	inline float get_MarginLeft_7() const { return ___MarginLeft_7; }
	inline float* get_address_of_MarginLeft_7() { return &___MarginLeft_7; }
	inline void set_MarginLeft_7(float value)
	{
		___MarginLeft_7 = value;
	}

	inline static int32_t get_offset_of_MarginRight_8() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5, ___MarginRight_8)); }
	inline float get_MarginRight_8() const { return ___MarginRight_8; }
	inline float* get_address_of_MarginRight_8() { return &___MarginRight_8; }
	inline void set_MarginRight_8(float value)
	{
		___MarginRight_8 = value;
	}

	inline static int32_t get_offset_of_PaddingLeft_9() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5, ___PaddingLeft_9)); }
	inline float get_PaddingLeft_9() const { return ___PaddingLeft_9; }
	inline float* get_address_of_PaddingLeft_9() { return &___PaddingLeft_9; }
	inline void set_PaddingLeft_9(float value)
	{
		___PaddingLeft_9 = value;
	}

	inline static int32_t get_offset_of_PaddingRight_10() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5, ___PaddingRight_10)); }
	inline float get_PaddingRight_10() const { return ___PaddingRight_10; }
	inline float* get_address_of_PaddingRight_10() { return &___PaddingRight_10; }
	inline void set_PaddingRight_10(float value)
	{
		___PaddingRight_10 = value;
	}

	inline static int32_t get_offset_of_MinWidth_11() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5, ___MinWidth_11)); }
	inline float get_MinWidth_11() const { return ___MinWidth_11; }
	inline float* get_address_of_MinWidth_11() { return &___MinWidth_11; }
	inline void set_MinWidth_11(float value)
	{
		___MinWidth_11 = value;
	}

	inline static int32_t get_offset_of_MaxWidth_12() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5, ___MaxWidth_12)); }
	inline float get_MaxWidth_12() const { return ___MaxWidth_12; }
	inline float* get_address_of_MaxWidth_12() { return &___MaxWidth_12; }
	inline void set_MaxWidth_12(float value)
	{
		___MaxWidth_12 = value;
	}

	inline static int32_t get_offset_of_Title_13() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5, ___Title_13)); }
	inline String_t* get_Title_13() const { return ___Title_13; }
	inline String_t** get_address_of_Title_13() { return &___Title_13; }
	inline void set_Title_13(String_t* value)
	{
		___Title_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Title_13), (void*)value);
	}

	inline static int32_t get_offset_of_LabelWidth_14() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5, ___LabelWidth_14)); }
	inline float get_LabelWidth_14() const { return ___LabelWidth_14; }
	inline float* get_address_of_LabelWidth_14() { return &___LabelWidth_14; }
	inline void set_LabelWidth_14(float value)
	{
		___LabelWidth_14 = value;
	}
};


// Sirenix.OdinInspector.InfoMessageType
struct InfoMessageType_tFC7A3497806F1785820060C9FC37EECC5945A3EC 
{
public:
	// System.Int32 Sirenix.OdinInspector.InfoMessageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InfoMessageType_tFC7A3497806F1785820060C9FC37EECC5945A3EC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.InlineEditorModes
struct InlineEditorModes_t80DEB95116CA958EDEDA16F36A77CD338509C0D8 
{
public:
	// System.Int32 Sirenix.OdinInspector.InlineEditorModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InlineEditorModes_t80DEB95116CA958EDEDA16F36A77CD338509C0D8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.InlineEditorObjectFieldModes
struct InlineEditorObjectFieldModes_t473F9A33A9D19B941F14D7FBF3ADBF6C51B9A532 
{
public:
	// System.Int32 Sirenix.OdinInspector.InlineEditorObjectFieldModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InlineEditorObjectFieldModes_t473F9A33A9D19B941F14D7FBF3ADBF6C51B9A532, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.ObjectFieldAlignment
struct ObjectFieldAlignment_tB339CB26239CBD39D58591B1B02ACC7504148EBA 
{
public:
	// System.Int32 Sirenix.OdinInspector.ObjectFieldAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectFieldAlignment_tB339CB26239CBD39D58591B1B02ACC7504148EBA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.OnInspectorDisposeAttribute
struct OnInspectorDisposeAttribute_t9CF5B6A832469F06C52D7EDC96C3F547A6989BDF  : public ShowInInspectorAttribute_t035C9790B6F7942286B0D6607580AB3393B1115B
{
public:
	// System.String Sirenix.OdinInspector.OnInspectorDisposeAttribute::Action
	String_t* ___Action_0;

public:
	inline static int32_t get_offset_of_Action_0() { return static_cast<int32_t>(offsetof(OnInspectorDisposeAttribute_t9CF5B6A832469F06C52D7EDC96C3F547A6989BDF, ___Action_0)); }
	inline String_t* get_Action_0() const { return ___Action_0; }
	inline String_t** get_address_of_Action_0() { return &___Action_0; }
	inline void set_Action_0(String_t* value)
	{
		___Action_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Action_0), (void*)value);
	}
};


// Sirenix.OdinInspector.OnInspectorGUIAttribute
struct OnInspectorGUIAttribute_t82649BAD99432C3B20C66F0126D74AE5FAA2AE55  : public ShowInInspectorAttribute_t035C9790B6F7942286B0D6607580AB3393B1115B
{
public:
	// System.String Sirenix.OdinInspector.OnInspectorGUIAttribute::Prepend
	String_t* ___Prepend_0;
	// System.String Sirenix.OdinInspector.OnInspectorGUIAttribute::Append
	String_t* ___Append_1;
	// System.String Sirenix.OdinInspector.OnInspectorGUIAttribute::PrependMethodName
	String_t* ___PrependMethodName_2;
	// System.String Sirenix.OdinInspector.OnInspectorGUIAttribute::AppendMethodName
	String_t* ___AppendMethodName_3;

public:
	inline static int32_t get_offset_of_Prepend_0() { return static_cast<int32_t>(offsetof(OnInspectorGUIAttribute_t82649BAD99432C3B20C66F0126D74AE5FAA2AE55, ___Prepend_0)); }
	inline String_t* get_Prepend_0() const { return ___Prepend_0; }
	inline String_t** get_address_of_Prepend_0() { return &___Prepend_0; }
	inline void set_Prepend_0(String_t* value)
	{
		___Prepend_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Prepend_0), (void*)value);
	}

	inline static int32_t get_offset_of_Append_1() { return static_cast<int32_t>(offsetof(OnInspectorGUIAttribute_t82649BAD99432C3B20C66F0126D74AE5FAA2AE55, ___Append_1)); }
	inline String_t* get_Append_1() const { return ___Append_1; }
	inline String_t** get_address_of_Append_1() { return &___Append_1; }
	inline void set_Append_1(String_t* value)
	{
		___Append_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Append_1), (void*)value);
	}

	inline static int32_t get_offset_of_PrependMethodName_2() { return static_cast<int32_t>(offsetof(OnInspectorGUIAttribute_t82649BAD99432C3B20C66F0126D74AE5FAA2AE55, ___PrependMethodName_2)); }
	inline String_t* get_PrependMethodName_2() const { return ___PrependMethodName_2; }
	inline String_t** get_address_of_PrependMethodName_2() { return &___PrependMethodName_2; }
	inline void set_PrependMethodName_2(String_t* value)
	{
		___PrependMethodName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PrependMethodName_2), (void*)value);
	}

	inline static int32_t get_offset_of_AppendMethodName_3() { return static_cast<int32_t>(offsetof(OnInspectorGUIAttribute_t82649BAD99432C3B20C66F0126D74AE5FAA2AE55, ___AppendMethodName_3)); }
	inline String_t* get_AppendMethodName_3() const { return ___AppendMethodName_3; }
	inline String_t** get_address_of_AppendMethodName_3() { return &___AppendMethodName_3; }
	inline void set_AppendMethodName_3(String_t* value)
	{
		___AppendMethodName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AppendMethodName_3), (void*)value);
	}
};


// Sirenix.OdinInspector.OnInspectorInitAttribute
struct OnInspectorInitAttribute_t7564436864B4C2ABC648107C388558D26170EB85  : public ShowInInspectorAttribute_t035C9790B6F7942286B0D6607580AB3393B1115B
{
public:
	// System.String Sirenix.OdinInspector.OnInspectorInitAttribute::Action
	String_t* ___Action_0;

public:
	inline static int32_t get_offset_of_Action_0() { return static_cast<int32_t>(offsetof(OnInspectorInitAttribute_t7564436864B4C2ABC648107C388558D26170EB85, ___Action_0)); }
	inline String_t* get_Action_0() const { return ___Action_0; }
	inline String_t** get_address_of_Action_0() { return &___Action_0; }
	inline void set_Action_0(String_t* value)
	{
		___Action_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Action_0), (void*)value);
	}
};


// Sirenix.OdinInspector.SearchFilterOptions
struct SearchFilterOptions_tB0F58D247CECD11ED817EDED7523D1BF918A9142 
{
public:
	// System.Int32 Sirenix.OdinInspector.SearchFilterOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SearchFilterOptions_tB0F58D247CECD11ED817EDED7523D1BF918A9142, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.ShowIfGroupAttribute
struct ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549  : public PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310
{
public:
	// System.Object Sirenix.OdinInspector.ShowIfGroupAttribute::Value
	RuntimeObject * ___Value_6;

public:
	inline static int32_t get_offset_of_Value_6() { return static_cast<int32_t>(offsetof(ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549, ___Value_6)); }
	inline RuntimeObject * get_Value_6() const { return ___Value_6; }
	inline RuntimeObject ** get_address_of_Value_6() { return &___Value_6; }
	inline void set_Value_6(RuntimeObject * value)
	{
		___Value_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_6), (void*)value);
	}
};


// Sirenix.OdinInspector.TabGroupAttribute
struct TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED  : public PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310
{
public:
	// System.String Sirenix.OdinInspector.TabGroupAttribute::TabName
	String_t* ___TabName_7;
	// System.Boolean Sirenix.OdinInspector.TabGroupAttribute::UseFixedHeight
	bool ___UseFixedHeight_8;
	// System.Boolean Sirenix.OdinInspector.TabGroupAttribute::Paddingless
	bool ___Paddingless_9;
	// System.Boolean Sirenix.OdinInspector.TabGroupAttribute::HideTabGroupIfTabGroupOnlyHasOneTab
	bool ___HideTabGroupIfTabGroupOnlyHasOneTab_10;
	// System.Collections.Generic.List`1<System.String> Sirenix.OdinInspector.TabGroupAttribute::<Tabs>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CTabsU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_TabName_7() { return static_cast<int32_t>(offsetof(TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED, ___TabName_7)); }
	inline String_t* get_TabName_7() const { return ___TabName_7; }
	inline String_t** get_address_of_TabName_7() { return &___TabName_7; }
	inline void set_TabName_7(String_t* value)
	{
		___TabName_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TabName_7), (void*)value);
	}

	inline static int32_t get_offset_of_UseFixedHeight_8() { return static_cast<int32_t>(offsetof(TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED, ___UseFixedHeight_8)); }
	inline bool get_UseFixedHeight_8() const { return ___UseFixedHeight_8; }
	inline bool* get_address_of_UseFixedHeight_8() { return &___UseFixedHeight_8; }
	inline void set_UseFixedHeight_8(bool value)
	{
		___UseFixedHeight_8 = value;
	}

	inline static int32_t get_offset_of_Paddingless_9() { return static_cast<int32_t>(offsetof(TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED, ___Paddingless_9)); }
	inline bool get_Paddingless_9() const { return ___Paddingless_9; }
	inline bool* get_address_of_Paddingless_9() { return &___Paddingless_9; }
	inline void set_Paddingless_9(bool value)
	{
		___Paddingless_9 = value;
	}

	inline static int32_t get_offset_of_HideTabGroupIfTabGroupOnlyHasOneTab_10() { return static_cast<int32_t>(offsetof(TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED, ___HideTabGroupIfTabGroupOnlyHasOneTab_10)); }
	inline bool get_HideTabGroupIfTabGroupOnlyHasOneTab_10() const { return ___HideTabGroupIfTabGroupOnlyHasOneTab_10; }
	inline bool* get_address_of_HideTabGroupIfTabGroupOnlyHasOneTab_10() { return &___HideTabGroupIfTabGroupOnlyHasOneTab_10; }
	inline void set_HideTabGroupIfTabGroupOnlyHasOneTab_10(bool value)
	{
		___HideTabGroupIfTabGroupOnlyHasOneTab_10 = value;
	}

	inline static int32_t get_offset_of_U3CTabsU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED, ___U3CTabsU3Ek__BackingField_11)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CTabsU3Ek__BackingField_11() const { return ___U3CTabsU3Ek__BackingField_11; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CTabsU3Ek__BackingField_11() { return &___U3CTabsU3Ek__BackingField_11; }
	inline void set_U3CTabsU3Ek__BackingField_11(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CTabsU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTabsU3Ek__BackingField_11), (void*)value);
	}
};


// Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute
struct TabSubGroupAttribute_t163D9E025CE046202E35327D9858BA138474EED6  : public PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310
{
public:

public:
};


// Sirenix.OdinInspector.TitleAlignments
struct TitleAlignments_t5570C0F08DE7274D7911E6D4F29E5916B4FDDBDE 
{
public:
	// System.Int32 Sirenix.OdinInspector.TitleAlignments::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TitleAlignments_t5570C0F08DE7274D7911E6D4F29E5916B4FDDBDE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.ToggleGroupAttribute
struct ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5  : public PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310
{
public:
	// System.String Sirenix.OdinInspector.ToggleGroupAttribute::ToggleGroupTitle
	String_t* ___ToggleGroupTitle_6;
	// System.Boolean Sirenix.OdinInspector.ToggleGroupAttribute::CollapseOthersOnExpand
	bool ___CollapseOthersOnExpand_7;
	// System.String Sirenix.OdinInspector.ToggleGroupAttribute::<TitleStringMemberName>k__BackingField
	String_t* ___U3CTitleStringMemberNameU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_ToggleGroupTitle_6() { return static_cast<int32_t>(offsetof(ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5, ___ToggleGroupTitle_6)); }
	inline String_t* get_ToggleGroupTitle_6() const { return ___ToggleGroupTitle_6; }
	inline String_t** get_address_of_ToggleGroupTitle_6() { return &___ToggleGroupTitle_6; }
	inline void set_ToggleGroupTitle_6(String_t* value)
	{
		___ToggleGroupTitle_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ToggleGroupTitle_6), (void*)value);
	}

	inline static int32_t get_offset_of_CollapseOthersOnExpand_7() { return static_cast<int32_t>(offsetof(ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5, ___CollapseOthersOnExpand_7)); }
	inline bool get_CollapseOthersOnExpand_7() const { return ___CollapseOthersOnExpand_7; }
	inline bool* get_address_of_CollapseOthersOnExpand_7() { return &___CollapseOthersOnExpand_7; }
	inline void set_CollapseOthersOnExpand_7(bool value)
	{
		___CollapseOthersOnExpand_7 = value;
	}

	inline static int32_t get_offset_of_U3CTitleStringMemberNameU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5, ___U3CTitleStringMemberNameU3Ek__BackingField_8)); }
	inline String_t* get_U3CTitleStringMemberNameU3Ek__BackingField_8() const { return ___U3CTitleStringMemberNameU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CTitleStringMemberNameU3Ek__BackingField_8() { return &___U3CTitleStringMemberNameU3Ek__BackingField_8; }
	inline void set_U3CTitleStringMemberNameU3Ek__BackingField_8(String_t* value)
	{
		___U3CTitleStringMemberNameU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTitleStringMemberNameU3Ek__BackingField_8), (void*)value);
	}
};


// Sirenix.OdinInspector.VerticalGroupAttribute
struct VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420  : public PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310
{
public:
	// System.Single Sirenix.OdinInspector.VerticalGroupAttribute::PaddingTop
	float ___PaddingTop_6;
	// System.Single Sirenix.OdinInspector.VerticalGroupAttribute::PaddingBottom
	float ___PaddingBottom_7;

public:
	inline static int32_t get_offset_of_PaddingTop_6() { return static_cast<int32_t>(offsetof(VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420, ___PaddingTop_6)); }
	inline float get_PaddingTop_6() const { return ___PaddingTop_6; }
	inline float* get_address_of_PaddingTop_6() { return &___PaddingTop_6; }
	inline void set_PaddingTop_6(float value)
	{
		___PaddingTop_6 = value;
	}

	inline static int32_t get_offset_of_PaddingBottom_7() { return static_cast<int32_t>(offsetof(VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420, ___PaddingBottom_7)); }
	inline float get_PaddingBottom_7() const { return ___PaddingBottom_7; }
	inline float* get_address_of_PaddingBottom_7() { return &___PaddingBottom_7; }
	inline void set_PaddingBottom_7(float value)
	{
		___PaddingBottom_7 = value;
	}
};


// System.AttributeTargets
struct AttributeTargets_t7CC0DE6D2B11C951E525EE69AD02313792932741 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t7CC0DE6D2B11C951E525EE69AD02313792932741, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Reflection.BindingFlags
struct BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TextAlignment
struct TextAlignment_tA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD 
{
public:
	// System.Int32 UnityEngine.TextAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignment_tA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.AttributeTargetFlags
struct AttributeTargetFlags_t89CE7177F30A6751D0871245A93C9E8181C36C8A  : public RuntimeObject
{
public:

public:
};


// Sirenix.OdinInspector.ButtonAttribute
struct ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381  : public ShowInInspectorAttribute_t035C9790B6F7942286B0D6607580AB3393B1115B
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonAttribute::ButtonHeight
	int32_t ___ButtonHeight_0;
	// System.String Sirenix.OdinInspector.ButtonAttribute::Name
	String_t* ___Name_1;
	// Sirenix.OdinInspector.ButtonStyle Sirenix.OdinInspector.ButtonAttribute::Style
	int32_t ___Style_2;
	// System.Boolean Sirenix.OdinInspector.ButtonAttribute::Expanded
	bool ___Expanded_3;
	// System.Boolean Sirenix.OdinInspector.ButtonAttribute::DisplayParameters
	bool ___DisplayParameters_4;
	// System.Boolean Sirenix.OdinInspector.ButtonAttribute::drawResult
	bool ___drawResult_5;
	// System.Boolean Sirenix.OdinInspector.ButtonAttribute::drawResultIsSet
	bool ___drawResultIsSet_6;

public:
	inline static int32_t get_offset_of_ButtonHeight_0() { return static_cast<int32_t>(offsetof(ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381, ___ButtonHeight_0)); }
	inline int32_t get_ButtonHeight_0() const { return ___ButtonHeight_0; }
	inline int32_t* get_address_of_ButtonHeight_0() { return &___ButtonHeight_0; }
	inline void set_ButtonHeight_0(int32_t value)
	{
		___ButtonHeight_0 = value;
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_1), (void*)value);
	}

	inline static int32_t get_offset_of_Style_2() { return static_cast<int32_t>(offsetof(ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381, ___Style_2)); }
	inline int32_t get_Style_2() const { return ___Style_2; }
	inline int32_t* get_address_of_Style_2() { return &___Style_2; }
	inline void set_Style_2(int32_t value)
	{
		___Style_2 = value;
	}

	inline static int32_t get_offset_of_Expanded_3() { return static_cast<int32_t>(offsetof(ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381, ___Expanded_3)); }
	inline bool get_Expanded_3() const { return ___Expanded_3; }
	inline bool* get_address_of_Expanded_3() { return &___Expanded_3; }
	inline void set_Expanded_3(bool value)
	{
		___Expanded_3 = value;
	}

	inline static int32_t get_offset_of_DisplayParameters_4() { return static_cast<int32_t>(offsetof(ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381, ___DisplayParameters_4)); }
	inline bool get_DisplayParameters_4() const { return ___DisplayParameters_4; }
	inline bool* get_address_of_DisplayParameters_4() { return &___DisplayParameters_4; }
	inline void set_DisplayParameters_4(bool value)
	{
		___DisplayParameters_4 = value;
	}

	inline static int32_t get_offset_of_drawResult_5() { return static_cast<int32_t>(offsetof(ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381, ___drawResult_5)); }
	inline bool get_drawResult_5() const { return ___drawResult_5; }
	inline bool* get_address_of_drawResult_5() { return &___drawResult_5; }
	inline void set_drawResult_5(bool value)
	{
		___drawResult_5 = value;
	}

	inline static int32_t get_offset_of_drawResultIsSet_6() { return static_cast<int32_t>(offsetof(ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381, ___drawResultIsSet_6)); }
	inline bool get_drawResultIsSet_6() const { return ___drawResultIsSet_6; }
	inline bool* get_address_of_drawResultIsSet_6() { return &___drawResultIsSet_6; }
	inline void set_drawResultIsSet_6(bool value)
	{
		___drawResultIsSet_6 = value;
	}
};


// Sirenix.OdinInspector.DetailedInfoBoxAttribute
struct DetailedInfoBoxAttribute_tCBB726C7E8955A4F2261373DF09951C49BB79F97  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.DetailedInfoBoxAttribute::Message
	String_t* ___Message_0;
	// System.String Sirenix.OdinInspector.DetailedInfoBoxAttribute::Details
	String_t* ___Details_1;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.DetailedInfoBoxAttribute::InfoMessageType
	int32_t ___InfoMessageType_2;
	// System.String Sirenix.OdinInspector.DetailedInfoBoxAttribute::VisibleIf
	String_t* ___VisibleIf_3;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_tCBB726C7E8955A4F2261373DF09951C49BB79F97, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Message_0), (void*)value);
	}

	inline static int32_t get_offset_of_Details_1() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_tCBB726C7E8955A4F2261373DF09951C49BB79F97, ___Details_1)); }
	inline String_t* get_Details_1() const { return ___Details_1; }
	inline String_t** get_address_of_Details_1() { return &___Details_1; }
	inline void set_Details_1(String_t* value)
	{
		___Details_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Details_1), (void*)value);
	}

	inline static int32_t get_offset_of_InfoMessageType_2() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_tCBB726C7E8955A4F2261373DF09951C49BB79F97, ___InfoMessageType_2)); }
	inline int32_t get_InfoMessageType_2() const { return ___InfoMessageType_2; }
	inline int32_t* get_address_of_InfoMessageType_2() { return &___InfoMessageType_2; }
	inline void set_InfoMessageType_2(int32_t value)
	{
		___InfoMessageType_2 = value;
	}

	inline static int32_t get_offset_of_VisibleIf_3() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_tCBB726C7E8955A4F2261373DF09951C49BB79F97, ___VisibleIf_3)); }
	inline String_t* get_VisibleIf_3() const { return ___VisibleIf_3; }
	inline String_t** get_address_of_VisibleIf_3() { return &___VisibleIf_3; }
	inline void set_VisibleIf_3(String_t* value)
	{
		___VisibleIf_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VisibleIf_3), (void*)value);
	}
};


// Sirenix.OdinInspector.DictionaryDrawerSettings
struct DictionaryDrawerSettings_t6F42380B0928BCFD6B9B3CFF92D3558518CDD011  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.DictionaryDrawerSettings::KeyLabel
	String_t* ___KeyLabel_0;
	// System.String Sirenix.OdinInspector.DictionaryDrawerSettings::ValueLabel
	String_t* ___ValueLabel_1;
	// Sirenix.OdinInspector.DictionaryDisplayOptions Sirenix.OdinInspector.DictionaryDrawerSettings::DisplayMode
	int32_t ___DisplayMode_2;
	// System.Boolean Sirenix.OdinInspector.DictionaryDrawerSettings::IsReadOnly
	bool ___IsReadOnly_3;

public:
	inline static int32_t get_offset_of_KeyLabel_0() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_t6F42380B0928BCFD6B9B3CFF92D3558518CDD011, ___KeyLabel_0)); }
	inline String_t* get_KeyLabel_0() const { return ___KeyLabel_0; }
	inline String_t** get_address_of_KeyLabel_0() { return &___KeyLabel_0; }
	inline void set_KeyLabel_0(String_t* value)
	{
		___KeyLabel_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___KeyLabel_0), (void*)value);
	}

	inline static int32_t get_offset_of_ValueLabel_1() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_t6F42380B0928BCFD6B9B3CFF92D3558518CDD011, ___ValueLabel_1)); }
	inline String_t* get_ValueLabel_1() const { return ___ValueLabel_1; }
	inline String_t** get_address_of_ValueLabel_1() { return &___ValueLabel_1; }
	inline void set_ValueLabel_1(String_t* value)
	{
		___ValueLabel_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ValueLabel_1), (void*)value);
	}

	inline static int32_t get_offset_of_DisplayMode_2() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_t6F42380B0928BCFD6B9B3CFF92D3558518CDD011, ___DisplayMode_2)); }
	inline int32_t get_DisplayMode_2() const { return ___DisplayMode_2; }
	inline int32_t* get_address_of_DisplayMode_2() { return &___DisplayMode_2; }
	inline void set_DisplayMode_2(int32_t value)
	{
		___DisplayMode_2 = value;
	}

	inline static int32_t get_offset_of_IsReadOnly_3() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_t6F42380B0928BCFD6B9B3CFF92D3558518CDD011, ___IsReadOnly_3)); }
	inline bool get_IsReadOnly_3() const { return ___IsReadOnly_3; }
	inline bool* get_address_of_IsReadOnly_3() { return &___IsReadOnly_3; }
	inline void set_IsReadOnly_3(bool value)
	{
		___IsReadOnly_3 = value;
	}
};


// Sirenix.OdinInspector.InfoBoxAttribute
struct InfoBoxAttribute_t4134D5269F4439BB4423FC022453606650597B3A  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.InfoBoxAttribute::Message
	String_t* ___Message_0;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.InfoBoxAttribute::InfoMessageType
	int32_t ___InfoMessageType_1;
	// System.String Sirenix.OdinInspector.InfoBoxAttribute::VisibleIf
	String_t* ___VisibleIf_2;
	// System.Boolean Sirenix.OdinInspector.InfoBoxAttribute::GUIAlwaysEnabled
	bool ___GUIAlwaysEnabled_3;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t4134D5269F4439BB4423FC022453606650597B3A, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Message_0), (void*)value);
	}

	inline static int32_t get_offset_of_InfoMessageType_1() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t4134D5269F4439BB4423FC022453606650597B3A, ___InfoMessageType_1)); }
	inline int32_t get_InfoMessageType_1() const { return ___InfoMessageType_1; }
	inline int32_t* get_address_of_InfoMessageType_1() { return &___InfoMessageType_1; }
	inline void set_InfoMessageType_1(int32_t value)
	{
		___InfoMessageType_1 = value;
	}

	inline static int32_t get_offset_of_VisibleIf_2() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t4134D5269F4439BB4423FC022453606650597B3A, ___VisibleIf_2)); }
	inline String_t* get_VisibleIf_2() const { return ___VisibleIf_2; }
	inline String_t** get_address_of_VisibleIf_2() { return &___VisibleIf_2; }
	inline void set_VisibleIf_2(String_t* value)
	{
		___VisibleIf_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VisibleIf_2), (void*)value);
	}

	inline static int32_t get_offset_of_GUIAlwaysEnabled_3() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t4134D5269F4439BB4423FC022453606650597B3A, ___GUIAlwaysEnabled_3)); }
	inline bool get_GUIAlwaysEnabled_3() const { return ___GUIAlwaysEnabled_3; }
	inline bool* get_address_of_GUIAlwaysEnabled_3() { return &___GUIAlwaysEnabled_3; }
	inline void set_GUIAlwaysEnabled_3(bool value)
	{
		___GUIAlwaysEnabled_3 = value;
	}
};


// Sirenix.OdinInspector.InlineEditorAttribute
struct InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::expanded
	bool ___expanded_0;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DrawHeader
	bool ___DrawHeader_1;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DrawGUI
	bool ___DrawGUI_2;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DrawPreview
	bool ___DrawPreview_3;
	// System.Single Sirenix.OdinInspector.InlineEditorAttribute::MaxHeight
	float ___MaxHeight_4;
	// System.Single Sirenix.OdinInspector.InlineEditorAttribute::PreviewWidth
	float ___PreviewWidth_5;
	// System.Single Sirenix.OdinInspector.InlineEditorAttribute::PreviewHeight
	float ___PreviewHeight_6;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::IncrementInlineEditorDrawerDepth
	bool ___IncrementInlineEditorDrawerDepth_7;
	// Sirenix.OdinInspector.InlineEditorObjectFieldModes Sirenix.OdinInspector.InlineEditorAttribute::ObjectFieldMode
	int32_t ___ObjectFieldMode_8;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DisableGUIForVCSLockedAssets
	bool ___DisableGUIForVCSLockedAssets_9;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::<ExpandedHasValue>k__BackingField
	bool ___U3CExpandedHasValueU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_expanded_0() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216, ___expanded_0)); }
	inline bool get_expanded_0() const { return ___expanded_0; }
	inline bool* get_address_of_expanded_0() { return &___expanded_0; }
	inline void set_expanded_0(bool value)
	{
		___expanded_0 = value;
	}

	inline static int32_t get_offset_of_DrawHeader_1() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216, ___DrawHeader_1)); }
	inline bool get_DrawHeader_1() const { return ___DrawHeader_1; }
	inline bool* get_address_of_DrawHeader_1() { return &___DrawHeader_1; }
	inline void set_DrawHeader_1(bool value)
	{
		___DrawHeader_1 = value;
	}

	inline static int32_t get_offset_of_DrawGUI_2() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216, ___DrawGUI_2)); }
	inline bool get_DrawGUI_2() const { return ___DrawGUI_2; }
	inline bool* get_address_of_DrawGUI_2() { return &___DrawGUI_2; }
	inline void set_DrawGUI_2(bool value)
	{
		___DrawGUI_2 = value;
	}

	inline static int32_t get_offset_of_DrawPreview_3() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216, ___DrawPreview_3)); }
	inline bool get_DrawPreview_3() const { return ___DrawPreview_3; }
	inline bool* get_address_of_DrawPreview_3() { return &___DrawPreview_3; }
	inline void set_DrawPreview_3(bool value)
	{
		___DrawPreview_3 = value;
	}

	inline static int32_t get_offset_of_MaxHeight_4() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216, ___MaxHeight_4)); }
	inline float get_MaxHeight_4() const { return ___MaxHeight_4; }
	inline float* get_address_of_MaxHeight_4() { return &___MaxHeight_4; }
	inline void set_MaxHeight_4(float value)
	{
		___MaxHeight_4 = value;
	}

	inline static int32_t get_offset_of_PreviewWidth_5() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216, ___PreviewWidth_5)); }
	inline float get_PreviewWidth_5() const { return ___PreviewWidth_5; }
	inline float* get_address_of_PreviewWidth_5() { return &___PreviewWidth_5; }
	inline void set_PreviewWidth_5(float value)
	{
		___PreviewWidth_5 = value;
	}

	inline static int32_t get_offset_of_PreviewHeight_6() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216, ___PreviewHeight_6)); }
	inline float get_PreviewHeight_6() const { return ___PreviewHeight_6; }
	inline float* get_address_of_PreviewHeight_6() { return &___PreviewHeight_6; }
	inline void set_PreviewHeight_6(float value)
	{
		___PreviewHeight_6 = value;
	}

	inline static int32_t get_offset_of_IncrementInlineEditorDrawerDepth_7() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216, ___IncrementInlineEditorDrawerDepth_7)); }
	inline bool get_IncrementInlineEditorDrawerDepth_7() const { return ___IncrementInlineEditorDrawerDepth_7; }
	inline bool* get_address_of_IncrementInlineEditorDrawerDepth_7() { return &___IncrementInlineEditorDrawerDepth_7; }
	inline void set_IncrementInlineEditorDrawerDepth_7(bool value)
	{
		___IncrementInlineEditorDrawerDepth_7 = value;
	}

	inline static int32_t get_offset_of_ObjectFieldMode_8() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216, ___ObjectFieldMode_8)); }
	inline int32_t get_ObjectFieldMode_8() const { return ___ObjectFieldMode_8; }
	inline int32_t* get_address_of_ObjectFieldMode_8() { return &___ObjectFieldMode_8; }
	inline void set_ObjectFieldMode_8(int32_t value)
	{
		___ObjectFieldMode_8 = value;
	}

	inline static int32_t get_offset_of_DisableGUIForVCSLockedAssets_9() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216, ___DisableGUIForVCSLockedAssets_9)); }
	inline bool get_DisableGUIForVCSLockedAssets_9() const { return ___DisableGUIForVCSLockedAssets_9; }
	inline bool* get_address_of_DisableGUIForVCSLockedAssets_9() { return &___DisableGUIForVCSLockedAssets_9; }
	inline void set_DisableGUIForVCSLockedAssets_9(bool value)
	{
		___DisableGUIForVCSLockedAssets_9 = value;
	}

	inline static int32_t get_offset_of_U3CExpandedHasValueU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216, ___U3CExpandedHasValueU3Ek__BackingField_10)); }
	inline bool get_U3CExpandedHasValueU3Ek__BackingField_10() const { return ___U3CExpandedHasValueU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CExpandedHasValueU3Ek__BackingField_10() { return &___U3CExpandedHasValueU3Ek__BackingField_10; }
	inline void set_U3CExpandedHasValueU3Ek__BackingField_10(bool value)
	{
		___U3CExpandedHasValueU3Ek__BackingField_10 = value;
	}
};


// Sirenix.OdinInspector.PreviewFieldAttribute
struct PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// Sirenix.OdinInspector.ObjectFieldAlignment Sirenix.OdinInspector.PreviewFieldAttribute::alignment
	int32_t ___alignment_0;
	// System.Boolean Sirenix.OdinInspector.PreviewFieldAttribute::alignmentHasValue
	bool ___alignmentHasValue_1;
	// System.Single Sirenix.OdinInspector.PreviewFieldAttribute::Height
	float ___Height_2;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_alignmentHasValue_1() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96, ___alignmentHasValue_1)); }
	inline bool get_alignmentHasValue_1() const { return ___alignmentHasValue_1; }
	inline bool* get_address_of_alignmentHasValue_1() { return &___alignmentHasValue_1; }
	inline void set_alignmentHasValue_1(bool value)
	{
		___alignmentHasValue_1 = value;
	}

	inline static int32_t get_offset_of_Height_2() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96, ___Height_2)); }
	inline float get_Height_2() const { return ___Height_2; }
	inline float* get_address_of_Height_2() { return &___Height_2; }
	inline void set_Height_2(float value)
	{
		___Height_2 = value;
	}
};


// Sirenix.OdinInspector.ProgressBarAttribute
struct ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Double Sirenix.OdinInspector.ProgressBarAttribute::Min
	double ___Min_0;
	// System.Double Sirenix.OdinInspector.ProgressBarAttribute::Max
	double ___Max_1;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::MinGetter
	String_t* ___MinGetter_2;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::MaxGetter
	String_t* ___MaxGetter_3;
	// System.Single Sirenix.OdinInspector.ProgressBarAttribute::R
	float ___R_4;
	// System.Single Sirenix.OdinInspector.ProgressBarAttribute::G
	float ___G_5;
	// System.Single Sirenix.OdinInspector.ProgressBarAttribute::B
	float ___B_6;
	// System.Int32 Sirenix.OdinInspector.ProgressBarAttribute::Height
	int32_t ___Height_7;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::ColorGetter
	String_t* ___ColorGetter_8;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::BackgroundColorGetter
	String_t* ___BackgroundColorGetter_9;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::Segmented
	bool ___Segmented_10;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::CustomValueStringGetter
	String_t* ___CustomValueStringGetter_11;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::drawValueLabel
	bool ___drawValueLabel_12;
	// UnityEngine.TextAlignment Sirenix.OdinInspector.ProgressBarAttribute::valueLabelAlignment
	int32_t ___valueLabelAlignment_13;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::<DrawValueLabelHasValue>k__BackingField
	bool ___U3CDrawValueLabelHasValueU3Ek__BackingField_14;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::<ValueLabelAlignmentHasValue>k__BackingField
	bool ___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___Min_0)); }
	inline double get_Min_0() const { return ___Min_0; }
	inline double* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(double value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___Max_1)); }
	inline double get_Max_1() const { return ___Max_1; }
	inline double* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(double value)
	{
		___Max_1 = value;
	}

	inline static int32_t get_offset_of_MinGetter_2() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___MinGetter_2)); }
	inline String_t* get_MinGetter_2() const { return ___MinGetter_2; }
	inline String_t** get_address_of_MinGetter_2() { return &___MinGetter_2; }
	inline void set_MinGetter_2(String_t* value)
	{
		___MinGetter_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MinGetter_2), (void*)value);
	}

	inline static int32_t get_offset_of_MaxGetter_3() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___MaxGetter_3)); }
	inline String_t* get_MaxGetter_3() const { return ___MaxGetter_3; }
	inline String_t** get_address_of_MaxGetter_3() { return &___MaxGetter_3; }
	inline void set_MaxGetter_3(String_t* value)
	{
		___MaxGetter_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MaxGetter_3), (void*)value);
	}

	inline static int32_t get_offset_of_R_4() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___R_4)); }
	inline float get_R_4() const { return ___R_4; }
	inline float* get_address_of_R_4() { return &___R_4; }
	inline void set_R_4(float value)
	{
		___R_4 = value;
	}

	inline static int32_t get_offset_of_G_5() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___G_5)); }
	inline float get_G_5() const { return ___G_5; }
	inline float* get_address_of_G_5() { return &___G_5; }
	inline void set_G_5(float value)
	{
		___G_5 = value;
	}

	inline static int32_t get_offset_of_B_6() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___B_6)); }
	inline float get_B_6() const { return ___B_6; }
	inline float* get_address_of_B_6() { return &___B_6; }
	inline void set_B_6(float value)
	{
		___B_6 = value;
	}

	inline static int32_t get_offset_of_Height_7() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___Height_7)); }
	inline int32_t get_Height_7() const { return ___Height_7; }
	inline int32_t* get_address_of_Height_7() { return &___Height_7; }
	inline void set_Height_7(int32_t value)
	{
		___Height_7 = value;
	}

	inline static int32_t get_offset_of_ColorGetter_8() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___ColorGetter_8)); }
	inline String_t* get_ColorGetter_8() const { return ___ColorGetter_8; }
	inline String_t** get_address_of_ColorGetter_8() { return &___ColorGetter_8; }
	inline void set_ColorGetter_8(String_t* value)
	{
		___ColorGetter_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ColorGetter_8), (void*)value);
	}

	inline static int32_t get_offset_of_BackgroundColorGetter_9() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___BackgroundColorGetter_9)); }
	inline String_t* get_BackgroundColorGetter_9() const { return ___BackgroundColorGetter_9; }
	inline String_t** get_address_of_BackgroundColorGetter_9() { return &___BackgroundColorGetter_9; }
	inline void set_BackgroundColorGetter_9(String_t* value)
	{
		___BackgroundColorGetter_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BackgroundColorGetter_9), (void*)value);
	}

	inline static int32_t get_offset_of_Segmented_10() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___Segmented_10)); }
	inline bool get_Segmented_10() const { return ___Segmented_10; }
	inline bool* get_address_of_Segmented_10() { return &___Segmented_10; }
	inline void set_Segmented_10(bool value)
	{
		___Segmented_10 = value;
	}

	inline static int32_t get_offset_of_CustomValueStringGetter_11() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___CustomValueStringGetter_11)); }
	inline String_t* get_CustomValueStringGetter_11() const { return ___CustomValueStringGetter_11; }
	inline String_t** get_address_of_CustomValueStringGetter_11() { return &___CustomValueStringGetter_11; }
	inline void set_CustomValueStringGetter_11(String_t* value)
	{
		___CustomValueStringGetter_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CustomValueStringGetter_11), (void*)value);
	}

	inline static int32_t get_offset_of_drawValueLabel_12() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___drawValueLabel_12)); }
	inline bool get_drawValueLabel_12() const { return ___drawValueLabel_12; }
	inline bool* get_address_of_drawValueLabel_12() { return &___drawValueLabel_12; }
	inline void set_drawValueLabel_12(bool value)
	{
		___drawValueLabel_12 = value;
	}

	inline static int32_t get_offset_of_valueLabelAlignment_13() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___valueLabelAlignment_13)); }
	inline int32_t get_valueLabelAlignment_13() const { return ___valueLabelAlignment_13; }
	inline int32_t* get_address_of_valueLabelAlignment_13() { return &___valueLabelAlignment_13; }
	inline void set_valueLabelAlignment_13(int32_t value)
	{
		___valueLabelAlignment_13 = value;
	}

	inline static int32_t get_offset_of_U3CDrawValueLabelHasValueU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___U3CDrawValueLabelHasValueU3Ek__BackingField_14)); }
	inline bool get_U3CDrawValueLabelHasValueU3Ek__BackingField_14() const { return ___U3CDrawValueLabelHasValueU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CDrawValueLabelHasValueU3Ek__BackingField_14() { return &___U3CDrawValueLabelHasValueU3Ek__BackingField_14; }
	inline void set_U3CDrawValueLabelHasValueU3Ek__BackingField_14(bool value)
	{
		___U3CDrawValueLabelHasValueU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB, ___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15)); }
	inline bool get_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15() const { return ___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15() { return &___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15; }
	inline void set_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15(bool value)
	{
		___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15 = value;
	}
};


// Sirenix.OdinInspector.RequiredAttribute
struct RequiredAttribute_t233038081C694B76DACAFC3BA8624900426D42B0  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.RequiredAttribute::ErrorMessage
	String_t* ___ErrorMessage_0;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.RequiredAttribute::MessageType
	int32_t ___MessageType_1;

public:
	inline static int32_t get_offset_of_ErrorMessage_0() { return static_cast<int32_t>(offsetof(RequiredAttribute_t233038081C694B76DACAFC3BA8624900426D42B0, ___ErrorMessage_0)); }
	inline String_t* get_ErrorMessage_0() const { return ___ErrorMessage_0; }
	inline String_t** get_address_of_ErrorMessage_0() { return &___ErrorMessage_0; }
	inline void set_ErrorMessage_0(String_t* value)
	{
		___ErrorMessage_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ErrorMessage_0), (void*)value);
	}

	inline static int32_t get_offset_of_MessageType_1() { return static_cast<int32_t>(offsetof(RequiredAttribute_t233038081C694B76DACAFC3BA8624900426D42B0, ___MessageType_1)); }
	inline int32_t get_MessageType_1() const { return ___MessageType_1; }
	inline int32_t* get_address_of_MessageType_1() { return &___MessageType_1; }
	inline void set_MessageType_1(int32_t value)
	{
		___MessageType_1 = value;
	}
};


// Sirenix.OdinInspector.ResponsiveButtonGroupAttribute
struct ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848  : public PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310
{
public:
	// Sirenix.OdinInspector.ButtonSizes Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::DefaultButtonSize
	int32_t ___DefaultButtonSize_6;
	// System.Boolean Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::UniformLayout
	bool ___UniformLayout_7;

public:
	inline static int32_t get_offset_of_DefaultButtonSize_6() { return static_cast<int32_t>(offsetof(ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848, ___DefaultButtonSize_6)); }
	inline int32_t get_DefaultButtonSize_6() const { return ___DefaultButtonSize_6; }
	inline int32_t* get_address_of_DefaultButtonSize_6() { return &___DefaultButtonSize_6; }
	inline void set_DefaultButtonSize_6(int32_t value)
	{
		___DefaultButtonSize_6 = value;
	}

	inline static int32_t get_offset_of_UniformLayout_7() { return static_cast<int32_t>(offsetof(ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848, ___UniformLayout_7)); }
	inline bool get_UniformLayout_7() const { return ___UniformLayout_7; }
	inline bool* get_address_of_UniformLayout_7() { return &___UniformLayout_7; }
	inline void set_UniformLayout_7(bool value)
	{
		___UniformLayout_7 = value;
	}
};


// Sirenix.OdinInspector.SearchableAttribute
struct SearchableAttribute_t36B1B00CA28BBBC11E1E354D4A0F50BAD302BB5A  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Sirenix.OdinInspector.SearchableAttribute::FuzzySearch
	bool ___FuzzySearch_0;
	// Sirenix.OdinInspector.SearchFilterOptions Sirenix.OdinInspector.SearchableAttribute::FilterOptions
	int32_t ___FilterOptions_1;
	// System.Boolean Sirenix.OdinInspector.SearchableAttribute::Recursive
	bool ___Recursive_2;

public:
	inline static int32_t get_offset_of_FuzzySearch_0() { return static_cast<int32_t>(offsetof(SearchableAttribute_t36B1B00CA28BBBC11E1E354D4A0F50BAD302BB5A, ___FuzzySearch_0)); }
	inline bool get_FuzzySearch_0() const { return ___FuzzySearch_0; }
	inline bool* get_address_of_FuzzySearch_0() { return &___FuzzySearch_0; }
	inline void set_FuzzySearch_0(bool value)
	{
		___FuzzySearch_0 = value;
	}

	inline static int32_t get_offset_of_FilterOptions_1() { return static_cast<int32_t>(offsetof(SearchableAttribute_t36B1B00CA28BBBC11E1E354D4A0F50BAD302BB5A, ___FilterOptions_1)); }
	inline int32_t get_FilterOptions_1() const { return ___FilterOptions_1; }
	inline int32_t* get_address_of_FilterOptions_1() { return &___FilterOptions_1; }
	inline void set_FilterOptions_1(int32_t value)
	{
		___FilterOptions_1 = value;
	}

	inline static int32_t get_offset_of_Recursive_2() { return static_cast<int32_t>(offsetof(SearchableAttribute_t36B1B00CA28BBBC11E1E354D4A0F50BAD302BB5A, ___Recursive_2)); }
	inline bool get_Recursive_2() const { return ___Recursive_2; }
	inline bool* get_address_of_Recursive_2() { return &___Recursive_2; }
	inline void set_Recursive_2(bool value)
	{
		___Recursive_2 = value;
	}
};


// Sirenix.OdinInspector.TitleAttribute
struct TitleAttribute_t03734F3839AB42F82F5DD8EA32BD8ECB79F567CC  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.TitleAttribute::Title
	String_t* ___Title_0;
	// System.String Sirenix.OdinInspector.TitleAttribute::Subtitle
	String_t* ___Subtitle_1;
	// System.Boolean Sirenix.OdinInspector.TitleAttribute::Bold
	bool ___Bold_2;
	// System.Boolean Sirenix.OdinInspector.TitleAttribute::HorizontalLine
	bool ___HorizontalLine_3;
	// Sirenix.OdinInspector.TitleAlignments Sirenix.OdinInspector.TitleAttribute::TitleAlignment
	int32_t ___TitleAlignment_4;

public:
	inline static int32_t get_offset_of_Title_0() { return static_cast<int32_t>(offsetof(TitleAttribute_t03734F3839AB42F82F5DD8EA32BD8ECB79F567CC, ___Title_0)); }
	inline String_t* get_Title_0() const { return ___Title_0; }
	inline String_t** get_address_of_Title_0() { return &___Title_0; }
	inline void set_Title_0(String_t* value)
	{
		___Title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Title_0), (void*)value);
	}

	inline static int32_t get_offset_of_Subtitle_1() { return static_cast<int32_t>(offsetof(TitleAttribute_t03734F3839AB42F82F5DD8EA32BD8ECB79F567CC, ___Subtitle_1)); }
	inline String_t* get_Subtitle_1() const { return ___Subtitle_1; }
	inline String_t** get_address_of_Subtitle_1() { return &___Subtitle_1; }
	inline void set_Subtitle_1(String_t* value)
	{
		___Subtitle_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Subtitle_1), (void*)value);
	}

	inline static int32_t get_offset_of_Bold_2() { return static_cast<int32_t>(offsetof(TitleAttribute_t03734F3839AB42F82F5DD8EA32BD8ECB79F567CC, ___Bold_2)); }
	inline bool get_Bold_2() const { return ___Bold_2; }
	inline bool* get_address_of_Bold_2() { return &___Bold_2; }
	inline void set_Bold_2(bool value)
	{
		___Bold_2 = value;
	}

	inline static int32_t get_offset_of_HorizontalLine_3() { return static_cast<int32_t>(offsetof(TitleAttribute_t03734F3839AB42F82F5DD8EA32BD8ECB79F567CC, ___HorizontalLine_3)); }
	inline bool get_HorizontalLine_3() const { return ___HorizontalLine_3; }
	inline bool* get_address_of_HorizontalLine_3() { return &___HorizontalLine_3; }
	inline void set_HorizontalLine_3(bool value)
	{
		___HorizontalLine_3 = value;
	}

	inline static int32_t get_offset_of_TitleAlignment_4() { return static_cast<int32_t>(offsetof(TitleAttribute_t03734F3839AB42F82F5DD8EA32BD8ECB79F567CC, ___TitleAlignment_4)); }
	inline int32_t get_TitleAlignment_4() const { return ___TitleAlignment_4; }
	inline int32_t* get_address_of_TitleAlignment_4() { return &___TitleAlignment_4; }
	inline void set_TitleAlignment_4(int32_t value)
	{
		___TitleAlignment_4 = value;
	}
};


// Sirenix.OdinInspector.TitleGroupAttribute
struct TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A  : public PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310
{
public:
	// System.String Sirenix.OdinInspector.TitleGroupAttribute::Subtitle
	String_t* ___Subtitle_6;
	// Sirenix.OdinInspector.TitleAlignments Sirenix.OdinInspector.TitleGroupAttribute::Alignment
	int32_t ___Alignment_7;
	// System.Boolean Sirenix.OdinInspector.TitleGroupAttribute::HorizontalLine
	bool ___HorizontalLine_8;
	// System.Boolean Sirenix.OdinInspector.TitleGroupAttribute::BoldTitle
	bool ___BoldTitle_9;
	// System.Boolean Sirenix.OdinInspector.TitleGroupAttribute::Indent
	bool ___Indent_10;

public:
	inline static int32_t get_offset_of_Subtitle_6() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A, ___Subtitle_6)); }
	inline String_t* get_Subtitle_6() const { return ___Subtitle_6; }
	inline String_t** get_address_of_Subtitle_6() { return &___Subtitle_6; }
	inline void set_Subtitle_6(String_t* value)
	{
		___Subtitle_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Subtitle_6), (void*)value);
	}

	inline static int32_t get_offset_of_Alignment_7() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A, ___Alignment_7)); }
	inline int32_t get_Alignment_7() const { return ___Alignment_7; }
	inline int32_t* get_address_of_Alignment_7() { return &___Alignment_7; }
	inline void set_Alignment_7(int32_t value)
	{
		___Alignment_7 = value;
	}

	inline static int32_t get_offset_of_HorizontalLine_8() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A, ___HorizontalLine_8)); }
	inline bool get_HorizontalLine_8() const { return ___HorizontalLine_8; }
	inline bool* get_address_of_HorizontalLine_8() { return &___HorizontalLine_8; }
	inline void set_HorizontalLine_8(bool value)
	{
		___HorizontalLine_8 = value;
	}

	inline static int32_t get_offset_of_BoldTitle_9() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A, ___BoldTitle_9)); }
	inline bool get_BoldTitle_9() const { return ___BoldTitle_9; }
	inline bool* get_address_of_BoldTitle_9() { return &___BoldTitle_9; }
	inline void set_BoldTitle_9(bool value)
	{
		___BoldTitle_9 = value;
	}

	inline static int32_t get_offset_of_Indent_10() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A, ___Indent_10)); }
	inline bool get_Indent_10() const { return ___Indent_10; }
	inline bool* get_address_of_Indent_10() { return &___Indent_10; }
	inline void set_Indent_10(bool value)
	{
		___Indent_10 = value;
	}
};


// Sirenix.OdinInspector.ValidateInputAttribute
struct ValidateInputAttribute_tDC0CF1D228B37E27B6AFC85476E9EC04CBFC2E6D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Sirenix.OdinInspector.ValidateInputAttribute::DefaultMessage
	String_t* ___DefaultMessage_0;
	// System.String Sirenix.OdinInspector.ValidateInputAttribute::MemberName
	String_t* ___MemberName_1;
	// System.String Sirenix.OdinInspector.ValidateInputAttribute::Condition
	String_t* ___Condition_2;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.ValidateInputAttribute::MessageType
	int32_t ___MessageType_3;
	// System.Boolean Sirenix.OdinInspector.ValidateInputAttribute::IncludeChildren
	bool ___IncludeChildren_4;
	// System.Boolean Sirenix.OdinInspector.ValidateInputAttribute::ContinuousValidationCheck
	bool ___ContinuousValidationCheck_5;

public:
	inline static int32_t get_offset_of_DefaultMessage_0() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_tDC0CF1D228B37E27B6AFC85476E9EC04CBFC2E6D, ___DefaultMessage_0)); }
	inline String_t* get_DefaultMessage_0() const { return ___DefaultMessage_0; }
	inline String_t** get_address_of_DefaultMessage_0() { return &___DefaultMessage_0; }
	inline void set_DefaultMessage_0(String_t* value)
	{
		___DefaultMessage_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DefaultMessage_0), (void*)value);
	}

	inline static int32_t get_offset_of_MemberName_1() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_tDC0CF1D228B37E27B6AFC85476E9EC04CBFC2E6D, ___MemberName_1)); }
	inline String_t* get_MemberName_1() const { return ___MemberName_1; }
	inline String_t** get_address_of_MemberName_1() { return &___MemberName_1; }
	inline void set_MemberName_1(String_t* value)
	{
		___MemberName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MemberName_1), (void*)value);
	}

	inline static int32_t get_offset_of_Condition_2() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_tDC0CF1D228B37E27B6AFC85476E9EC04CBFC2E6D, ___Condition_2)); }
	inline String_t* get_Condition_2() const { return ___Condition_2; }
	inline String_t** get_address_of_Condition_2() { return &___Condition_2; }
	inline void set_Condition_2(String_t* value)
	{
		___Condition_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Condition_2), (void*)value);
	}

	inline static int32_t get_offset_of_MessageType_3() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_tDC0CF1D228B37E27B6AFC85476E9EC04CBFC2E6D, ___MessageType_3)); }
	inline int32_t get_MessageType_3() const { return ___MessageType_3; }
	inline int32_t* get_address_of_MessageType_3() { return &___MessageType_3; }
	inline void set_MessageType_3(int32_t value)
	{
		___MessageType_3 = value;
	}

	inline static int32_t get_offset_of_IncludeChildren_4() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_tDC0CF1D228B37E27B6AFC85476E9EC04CBFC2E6D, ___IncludeChildren_4)); }
	inline bool get_IncludeChildren_4() const { return ___IncludeChildren_4; }
	inline bool* get_address_of_IncludeChildren_4() { return &___IncludeChildren_4; }
	inline void set_IncludeChildren_4(bool value)
	{
		___IncludeChildren_4 = value;
	}

	inline static int32_t get_offset_of_ContinuousValidationCheck_5() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_tDC0CF1D228B37E27B6AFC85476E9EC04CBFC2E6D, ___ContinuousValidationCheck_5)); }
	inline bool get_ContinuousValidationCheck_5() const { return ___ContinuousValidationCheck_5; }
	inline bool* get_address_of_ContinuousValidationCheck_5() { return &___ContinuousValidationCheck_5; }
	inline void set_ContinuousValidationCheck_5(bool value)
	{
		___ContinuousValidationCheck_5 = value;
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.ArgumentException
struct ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.Func`2<System.String,System.String>
struct Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96  : public MulticastDelegate_t
{
public:

public:
};


// System.NotImplementedException
struct NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.ArgumentNullException
struct ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mE2AF7615AD18E9CD92B1909285F5EC5DA8D180C8_gshared (Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisRuntimeObject_TisRuntimeObject_m93DBD723B5A365BD92FAF21BECDDCAFF67D0CA72_gshared (RuntimeObject* ___source0, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___selector1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Enumerable_ToArray_TisRuntimeObject_m90391AD23AB688BA42D238D4512C858F912D7A67_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m6E336459937EBBC514F001464CC3771240EEBB87_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_mE08D561E86879A26245096C572A8593279383FDB_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mEE468B81D8E7C140F567D10FF7F5894A50EEEA57_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___capacity0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD  List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);

// System.Void System.Attribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0 (Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * __this, const RuntimeMethod* method);
// System.String[] System.String::Split(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* String_Split_m13262358217AD2C119FD1B9733C3C0289D608512 (String_t* __this, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___separator0, const RuntimeMethod* method);
// System.Void System.Func`2<System.String,System.String>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mC276B952F432851DB174123FCF79CFABDF753BB0 (Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mE2AF7615AD18E9CD92B1909285F5EC5DA8D180C8_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.String,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
inline RuntimeObject* Enumerable_Select_TisString_t_TisString_t_m96ED0707DFF4FB4E203C941F8BC60B13F18F5A72 (RuntimeObject* ___source0, Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 *, const RuntimeMethod*))Enumerable_Select_TisRuntimeObject_TisRuntimeObject_m93DBD723B5A365BD92FAF21BECDDCAFF67D0CA72_gshared)(___source0, ___selector1, method);
}
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* Enumerable_ToArray_TisString_t_m1BAD76FB02571EB3CCC5BB00D4D581399F8523FC (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_m90391AD23AB688BA42D238D4512C858F912D7A67_gshared)(___source0, method);
}
// System.String System.String::Join(System.String,System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Join_m49371BED70248F0FCE970CB4F2E39E9A688AAFA4 (String_t* ___separator0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___value1, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.AssetSelectorAttribute/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m8030917C27454A00E147CCBAAB8795473574751B (U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.String System.String::Trim()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Trim_mB52EB7876C7132358B76B7DC95DEACA20722EF4D (String_t* __this, const RuntimeMethod* method);
// System.String System.String::Trim(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Trim_m788DE5AEFDAC40E778745C4DF4AFD45A4BC1007E (String_t* __this, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___trimChars0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A (PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * __this, String_t* ___groupId0, float ___order1, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor(System.String,System.Boolean,System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxGroupAttribute__ctor_m51F35E9FAAE6A6993CECBB636458130D35C88CC0 (BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283 * __this, String_t* ___group0, bool ___showLabel1, bool ___centerLabel2, float ___order3, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ShowInInspectorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7 (ShowInInspectorAttribute_t035C9790B6F7942286B0D6607580AB3393B1115B * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::set_HasDefinedExpanded(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_set_HasDefinedExpanded_mD585B02097F78E9049B2D144D50B503EF882B623_inline (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::get_HasDefinedExpanded()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool FoldoutGroupAttribute_get_HasDefinedExpanded_m3926B57853EA8BB7ACCACCC6740073D46F0C373C_inline (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, const RuntimeMethod* method);
// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::get_Expanded()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool FoldoutGroupAttribute_get_Expanded_m688163003BD99BF4E49564C644275EB8911A236D_inline (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::set_Expanded(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_set_Expanded_m48641F7D01ECE8E41D43960FA1F3FD0CB43976B4 (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// System.String Sirenix.OdinInspector.HideIfGroupAttribute::get_Condition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* HideIfGroupAttribute_get_Condition_mE6857BF4B30ABAC93E337FB918D2CF71AD0B0631 (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.HideIfGroupAttribute::set_Condition(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void HideIfGroupAttribute_set_Condition_m8FB8D4239B1C0E2A8C112A7AE01B5CD4FF400DDA_inline (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229 (String_t* ___value0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyGroupAttribute__ctor_m8F7EF143FEDD12FAC7B22FBAC7BCB4885A09DF88 (PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * __this, String_t* ___groupId0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.HideIfGroupAttribute::set_Animate(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void HideIfGroupAttribute_set_Animate_m4D3CD713242CB20F8E4109EB386B49F2A1231305_inline (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.String,System.Single,System.Int32,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HorizontalGroupAttribute__ctor_mD3D3178ECF3EC429A1C254046F0A5F589451C9C2 (HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5 * __this, String_t* ___group0, float ___width1, int32_t ___marginLeft2, int32_t ___marginRight3, float ___order4, const RuntimeMethod* method);
// System.Single System.Math::Max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Math_Max_m545895C37C1F738BBB653CA1F65E50FF1D197675 (float ___val10, float ___val21, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.InlineEditorAttribute::set_ExpandedHasValue(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void InlineEditorAttribute_set_ExpandedHasValue_m393D00D432705F319C5FCDF2B2571461B781D9C5_inline (InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.NotImplementedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotImplementedException__ctor_m8BEA657E260FC05F0C6D2C43A6E9BC08040F59C4 (NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorModes,Sirenix.OdinInspector.InlineEditorObjectFieldModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineEditorAttribute__ctor_m2C122F43FF3CA6D75809623A950AAA6B2BC3939D (InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216 * __this, int32_t ___inlineEditorMode0, int32_t ___objectFieldMode1, const RuntimeMethod* method);
// System.Int32 System.Math::Max(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Max_mA99E48BB021F2E4B62D4EA9F52EA6928EED618A2 (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::set_Alignment(Sirenix.OdinInspector.ObjectFieldAlignment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreviewFieldAttribute_set_Alignment_mC639ECEAA0F283F44483506E821E205D611634BA (PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabelHasValue(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_DrawValueLabelHasValue_mFE21A81AACD29E509534804C64D7092FC9EC971D_inline (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignmentHasValue(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m2E49A4D0090B1D60036AB25BA0ABAA025D3F25C5_inline (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, bool ___value0, const RuntimeMethod* method);
// System.Int32 System.String::LastIndexOf(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_LastIndexOf_m76C37E3915E802044761572007B8FB0635995F59 (String_t* __this, Il2CppChar ___value0, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline (String_t* __this, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_m2C4AFF5E79DD8BADFD2DFBCF156BF728FBB8E1AE (String_t* __this, int32_t ___startIndex0, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7 (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Single System.Math::Min(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Math_Min_mB77A95F6A1FE3B8E7CD784F20AF4E1C908B5E9CD (float ___val10, float ___val21, const RuntimeMethod* method);
// System.String Sirenix.OdinInspector.ShowIfGroupAttribute::get_Condition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ShowIfGroupAttribute_get_Condition_mBAA18A45D8F96BC8D5E46B3C141673EF52A50E46 (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::set_Condition(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ShowIfGroupAttribute_set_Condition_m7CAF3CF1D9803D698E9011B30EC9EA5536EB8FA9_inline (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::set_Animate(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ShowIfGroupAttribute_set_Animate_m0C5232E32FBF5570CA9EBF341F4A9A26383C87CD_inline (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.String,System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabGroupAttribute__ctor_m3FDCD07DF133DF94BC3015B62269140288334C53 (TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * __this, String_t* ___group0, String_t* ___tab1, bool ___useFixedHeight2, float ___order3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void Sirenix.OdinInspector.TabGroupAttribute::set_Tabs(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TabGroupAttribute_set_Tabs_m40A73E4EC6877859058F36C0D4CEA49C3F4ED537_inline (TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * __this, List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___value0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.String> Sirenix.OdinInspector.TabGroupAttribute::get_Tabs()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * TabGroupAttribute_get_Tabs_mE3395C3B6ACF15D12A58430AA25626B80AD24A4D_inline (TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
inline void List_1_Add_mA348FA1140766465189459D25B01EB179001DE83 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, String_t* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, String_t*, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_mE9FDDA3E872C3CB2DBDC8562E9ABA76CA3124599 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m6E336459937EBBC514F001464CC3771240EEBB87_gshared)(__this, ___collection0, method);
}
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyGroupAttribute_CombineValuesWith_mA9D4370FD93B94EE22C250251D79E7C9ADD9E4D9 (PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.String>::Contains(!0)
inline bool List_1_Contains_mC1D01A0D94C03E4225EEF9D6506D7D91C6976B7B (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, String_t* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, String_t*, const RuntimeMethod*))List_1_Contains_mE08D561E86879A26245096C572A8593279383FDB_gshared)(__this, ___item0, method);
}
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
inline int32_t List_1_get_Count_m4151A68BD4CB1D737213E7595F574987F8C812B4_inline (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Sirenix.OdinInspector.PropertyGroupAttribute>::.ctor(System.Int32)
inline void List_1__ctor_m32270ECBE19DDAA80F0E83559F63FDAD74A5C328 (List_1_tEA3588639D49099355676AB395F7428E02EFB383 * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tEA3588639D49099355676AB395F7428E02EFB383 *, int32_t, const RuntimeMethod*))List_1__ctor_mEE468B81D8E7C140F567D10FF7F5894A50EEEA57_gshared)(__this, ___capacity0, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.String>::GetEnumerator()
inline Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819  List_1_GetEnumerator_m79AA93F4A95A51200A8FC7433950476FD84D83B7 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819  (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
inline String_t* Enumerator_get_Current_m73A2CF304FA1FEDE0FB84D5A90287D398804BCFE_inline (Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819 *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline)(__this, method);
}
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute::.ctor(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabSubGroupAttribute__ctor_m44ED5201B8DDB858B71AC573473AD2D269FEA381 (TabSubGroupAttribute_t163D9E025CE046202E35327D9858BA138474EED6 * __this, String_t* ___groupId0, float ___order1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Sirenix.OdinInspector.PropertyGroupAttribute>::Add(!0)
inline void List_1_Add_m22BEC6E130081220F517BD7875962489A6478DD7 (List_1_tEA3588639D49099355676AB395F7428E02EFB383 * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tEA3588639D49099355676AB395F7428E02EFB383 *, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
inline bool Enumerator_MoveNext_m11D1A253155BD0CBF9B0E59ECEA4F7EEBF86C09F (Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819 *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
inline void Enumerator_Dispose_m6CD2F1158D4A0D24923FBB9479F1A0E987B98F2F (Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819 *, const RuntimeMethod*))Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared)(__this, method);
}
// System.Int32 System.Math::Min(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Min_mC950438198519FB2B0260FCB91220847EE4BB525 (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Single,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleGroupAttribute__ctor_m1E0CEDC90E7C6F85CEE5DBFDDEA3E22CA80321E6 (ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * __this, String_t* ___toggleMemberName0, float ___order1, String_t* ___groupTitle2, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ValueDropdownItem::.ctor(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueDropdownItem__ctor_mE15D14B9282ADADDB949FB86CEA4A53335BF7AF2 (ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * __this, String_t* ___text0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m798542DE19B3F02DC4F4B777BB2E73169F129DE1 (RuntimeObject * ___arg00, const RuntimeMethod* method);
// System.String Sirenix.OdinInspector.ValueDropdownItem::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_ToString_m526C92A7FAA56983098238EEBAB0E726113A1A91 (ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * __this, const RuntimeMethod* method);
// System.String Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetText()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mB8AB4A9382BA84731B96E24AB59C475901FC15E3_inline (ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * __this, const RuntimeMethod* method);
// System.Object Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetValue()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR RuntimeObject * ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m233A24A83540D6FBC8A5593E63BFDA1AA64391E1_inline (ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VerticalGroupAttribute__ctor_mD788EEA9634514D13E321236E9BC3A18186E4DA0 (VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420 * __this, String_t* ___groupId0, float ___order1, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.AssetListAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetListAttribute__ctor_m8247558D8C9BBEA29FA028089F7A0D7D9AC5A5DC (AssetListAttribute_tE254CEED1A60ABA5995D73442A2A7272B477DA55 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		__this->set_AutoPopulate_0((bool)0);
		__this->set_Tags_1((String_t*)NULL);
		__this->set_LayerNames_2((String_t*)NULL);
		__this->set_AssetNamePrefix_3((String_t*)NULL);
		__this->set_CustomFilterMethod_5((String_t*)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.AssetSelectorAttribute::set_Paths(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetSelectorAttribute_set_Paths_m76AA7C29F24AED85D381A5DDA6C2E7008BFBB2FF (AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetSelectorAttribute_set_Paths_m76AA7C29F24AED85D381A5DDA6C2E7008BFBB2FF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * G_B2_0 = NULL;
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* G_B2_1 = NULL;
	AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5 * G_B2_2 = NULL;
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * G_B1_0 = NULL;
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* G_B1_1 = NULL;
	AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5 * G_B1_2 = NULL;
	{
		String_t* L_0 = ___value0;
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_1 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)124));
		NullCheck(L_0);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = String_Split_m13262358217AD2C119FD1B9733C3C0289D608512(L_0, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_il2cpp_TypeInfo_var);
		Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * L_4 = ((U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_il2cpp_TypeInfo_var))->get_U3CU3E9__12_0_1();
		Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * L_5 = L_4;
		G_B1_0 = L_5;
		G_B1_1 = L_3;
		G_B1_2 = __this;
		if (L_5)
		{
			G_B2_0 = L_5;
			G_B2_1 = L_3;
			G_B2_2 = __this;
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_il2cpp_TypeInfo_var);
		U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5 * L_6 = ((U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * L_7 = (Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 *)il2cpp_codegen_object_new(Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96_il2cpp_TypeInfo_var);
		Func_2__ctor_mC276B952F432851DB174123FCF79CFABDF753BB0(L_7, L_6, (intptr_t)((intptr_t)U3CU3Ec_U3Cset_PathsU3Eb__12_0_m8675AB5D0D72FC737F62615297D2D030A0DC8AB1_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_mC276B952F432851DB174123FCF79CFABDF753BB0_RuntimeMethod_var);
		Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * L_8 = L_7;
		((U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_il2cpp_TypeInfo_var))->set_U3CU3E9__12_0_1(L_8);
		G_B2_0 = L_8;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0031:
	{
		RuntimeObject* L_9 = Enumerable_Select_TisString_t_TisString_t_m96ED0707DFF4FB4E203C941F8BC60B13F18F5A72((RuntimeObject*)(RuntimeObject*)G_B2_1, G_B2_0, /*hidden argument*/Enumerable_Select_TisString_t_TisString_t_m96ED0707DFF4FB4E203C941F8BC60B13F18F5A72_RuntimeMethod_var);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_10 = Enumerable_ToArray_TisString_t_m1BAD76FB02571EB3CCC5BB00D4D581399F8523FC(L_9, /*hidden argument*/Enumerable_ToArray_TisString_t_m1BAD76FB02571EB3CCC5BB00D4D581399F8523FC_RuntimeMethod_var);
		NullCheck(G_B2_2);
		G_B2_2->set_SearchInFolders_9(L_10);
		return;
	}
}
// System.String Sirenix.OdinInspector.AssetSelectorAttribute::get_Paths()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AssetSelectorAttribute_get_Paths_mEC8A7AD9115F4A88436030423B8D0D3947E584CA (AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetSelectorAttribute_get_Paths_mEC8A7AD9115F4A88436030423B8D0D3947E584CA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_0 = __this->get_SearchInFolders_9();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = __this->get_SearchInFolders_9();
		String_t* L_2 = String_Join_m49371BED70248F0FCE970CB4F2E39E9A688AAFA4(_stringLiteral5C10B5B2CD673A0616D529AA5234B12EE7153808, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0019:
	{
		return (String_t*)NULL;
	}
}
// System.Void Sirenix.OdinInspector.AssetSelectorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetSelectorAttribute__ctor_mD7B3574705DCEC423BD460B9A92123C8A2160A26 (AssetSelectorAttribute_t80656087B4B0C26358BFFE836F447F90FA880AF5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_IsUniqueList_0((bool)1);
		__this->set_DrawDropdownForListElements_1((bool)1);
		__this->set_ExpandAllMenuItems_4((bool)1);
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.AssetSelectorAttribute/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m898AC2ABD1D3662F4F6DAC7C5C5E904834908795 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m898AC2ABD1D3662F4F6DAC7C5C5E904834908795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5 * L_0 = (U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5 *)il2cpp_codegen_object_new(U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m8030917C27454A00E147CCBAAB8795473574751B(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.AssetSelectorAttribute/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m8030917C27454A00E147CCBAAB8795473574751B (U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Sirenix.OdinInspector.AssetSelectorAttribute/<>c::<set_Paths>b__12_0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* U3CU3Ec_U3Cset_PathsU3Eb__12_0_m8675AB5D0D72FC737F62615297D2D030A0DC8AB1 (U3CU3Ec_t2C5E8806812079D185E143341043DCF9B3CC8BF5 * __this, String_t* ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3Cset_PathsU3Eb__12_0_m8675AB5D0D72FC737F62615297D2D030A0DC8AB1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___x0;
		NullCheck(L_0);
		String_t* L_1 = String_Trim_mB52EB7876C7132358B76B7DC95DEACA20722EF4D(L_0, /*hidden argument*/NULL);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_2 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)2);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_3 = L_2;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)47));
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_4 = L_3;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)92));
		NullCheck(L_1);
		String_t* L_5 = String_Trim_m788DE5AEFDAC40E778745C4DF4AFD45A4BC1007E(L_1, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.AssetsOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetsOnlyAttribute__ctor_m77053C898FD25996672ADF31626685B35BF1F79C (AssetsOnlyAttribute_t2F7CC8E0514CB2767616E03312FD23EF8FA03D63 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor(System.String,System.Boolean,System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxGroupAttribute__ctor_m51F35E9FAAE6A6993CECBB636458130D35C88CC0 (BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283 * __this, String_t* ___group0, bool ___showLabel1, bool ___centerLabel2, float ___order3, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___group0;
		float L_1 = ___order3;
		PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = ___showLabel1;
		__this->set_ShowLabel_6(L_2);
		bool L_3 = ___centerLabel2;
		__this->set_CenterLabel_7(L_3);
		return;
	}
}
// System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxGroupAttribute__ctor_m3735DB49E46EE2E2AE8AE5415D6C9FA0A93233F2 (BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxGroupAttribute__ctor_m3735DB49E46EE2E2AE8AE5415D6C9FA0A93233F2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BoxGroupAttribute__ctor_m51F35E9FAAE6A6993CECBB636458130D35C88CC0(__this, _stringLiteral3D52B3CB7FE05D311C52AD3EA343EAE34ABDEACD, (bool)0, (bool)0, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.BoxGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxGroupAttribute_CombineValuesWith_m79BABBFF9A0B58F8A15369C25F243BFA76128A96 (BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283 * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxGroupAttribute_CombineValuesWith_m79BABBFF9A0B58F8A15369C25F243BFA76128A96_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283 * V_0 = NULL;
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_0 = ___other0;
		V_0 = ((BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283 *)IsInstClass((RuntimeObject*)L_0, BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283_il2cpp_TypeInfo_var));
		bool L_1 = __this->get_ShowLabel_6();
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = L_2->get_ShowLabel_6();
		if (L_3)
		{
			goto IL_0025;
		}
	}

IL_0017:
	{
		__this->set_ShowLabel_6((bool)0);
		BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283 * L_4 = V_0;
		NullCheck(L_4);
		L_4->set_ShowLabel_6((bool)0);
	}

IL_0025:
	{
		bool L_5 = __this->get_CenterLabel_7();
		BoxGroupAttribute_t356BA67FD5641D0B3550FF5635F22B07E319F283 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = L_6->get_CenterLabel_7();
		__this->set_CenterLabel_7((bool)((int32_t)((int32_t)L_5|(int32_t)L_7)));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ButtonAttribute::set_DrawResult(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute_set_DrawResult_mAD70CD6C9145A69B55D15FEFF29A6BA72CCEA8F3 (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_drawResult_5(L_0);
		__this->set_drawResultIsSet_6((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ButtonAttribute::get_DrawResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ButtonAttribute_get_DrawResult_m7F1681E70FF12206C2D4A94195826002882C53BB (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_drawResult_5();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ButtonAttribute::get_DrawResultIsSet()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ButtonAttribute_get_DrawResultIsSet_m708AD98B58E8B96D9FEE46B25439DB6077CA2A7A (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_drawResultIsSet_6();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mF27CD9C19F032DF07E9293DD310F5BE840ED26F8 (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, const RuntimeMethod* method)
{
	{
		__this->set_DisplayParameters_4((bool)1);
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		__this->set_Name_1((String_t*)NULL);
		__this->set_ButtonHeight_0(0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonSizes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mF71AB4EC269B5BD92CA84ED615BDBDF75CCB3111 (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, int32_t ___size0, const RuntimeMethod* method)
{
	{
		__this->set_DisplayParameters_4((bool)1);
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		__this->set_Name_1((String_t*)NULL);
		int32_t L_0 = ___size0;
		__this->set_ButtonHeight_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m0879084C1E2C8C02C21A54F97EC16C7B54483B53 (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, int32_t ___buttonSize0, const RuntimeMethod* method)
{
	{
		__this->set_DisplayParameters_4((bool)1);
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___buttonSize0;
		__this->set_ButtonHeight_0(L_0);
		__this->set_Name_1((String_t*)NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m70C961C47AD907A198820FF74C6DD81A5427B93A (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		__this->set_DisplayParameters_4((bool)1);
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		__this->set_ButtonHeight_0(0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonSizes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mB526E46E3BE5BF1024BB81FF1954A7454BA10E32 (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, String_t* ___name0, int32_t ___buttonSize1, const RuntimeMethod* method)
{
	{
		__this->set_DisplayParameters_4((bool)1);
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		int32_t L_1 = ___buttonSize1;
		__this->set_ButtonHeight_0(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m74C2A9439E312E6658C20984D3BBE10CBB8D941A (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, String_t* ___name0, int32_t ___buttonSize1, const RuntimeMethod* method)
{
	{
		__this->set_DisplayParameters_4((bool)1);
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		int32_t L_1 = ___buttonSize1;
		__this->set_ButtonHeight_0(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mCB7708CF62C859C98DB8112D7D7877EDA2E5898D (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, int32_t ___parameterBtnStyle0, const RuntimeMethod* method)
{
	{
		__this->set_DisplayParameters_4((bool)1);
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		__this->set_Name_1((String_t*)NULL);
		__this->set_ButtonHeight_0(0);
		int32_t L_0 = ___parameterBtnStyle0;
		__this->set_Style_2(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.Int32,Sirenix.OdinInspector.ButtonStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m17A5EAF23490CFFF3D56F7A8FD1AA7E1ED3BCA9C (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, int32_t ___buttonSize0, int32_t ___parameterBtnStyle1, const RuntimeMethod* method)
{
	{
		__this->set_DisplayParameters_4((bool)1);
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___buttonSize0;
		__this->set_ButtonHeight_0(L_0);
		__this->set_Name_1((String_t*)NULL);
		int32_t L_1 = ___parameterBtnStyle1;
		__this->set_Style_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonSizes,Sirenix.OdinInspector.ButtonStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mC8501A7A6F000FBAE5B34D71C67AEFE88E2724D9 (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, int32_t ___size0, int32_t ___parameterBtnStyle1, const RuntimeMethod* method)
{
	{
		__this->set_DisplayParameters_4((bool)1);
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___size0;
		__this->set_ButtonHeight_0(L_0);
		__this->set_Name_1((String_t*)NULL);
		int32_t L_1 = ___parameterBtnStyle1;
		__this->set_Style_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m98049103344F9DE50470BB6C38AF2EA19F590A3F (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, String_t* ___name0, int32_t ___parameterBtnStyle1, const RuntimeMethod* method)
{
	{
		__this->set_DisplayParameters_4((bool)1);
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		__this->set_ButtonHeight_0(0);
		int32_t L_1 = ___parameterBtnStyle1;
		__this->set_Style_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonSizes,Sirenix.OdinInspector.ButtonStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m75DACD3BA27E65DB9A96FD5F64A3DD9A0087A15D (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, String_t* ___name0, int32_t ___buttonSize1, int32_t ___parameterBtnStyle2, const RuntimeMethod* method)
{
	{
		__this->set_DisplayParameters_4((bool)1);
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		int32_t L_1 = ___buttonSize1;
		__this->set_ButtonHeight_0(L_1);
		int32_t L_2 = ___parameterBtnStyle2;
		__this->set_Style_2(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,System.Int32,Sirenix.OdinInspector.ButtonStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mFD9745232184D70BC605975B1817E57DFBF4FD59 (ButtonAttribute_t791FE7C2104D5CBC588184872A393788FA1A2381 * __this, String_t* ___name0, int32_t ___buttonSize1, int32_t ___parameterBtnStyle2, const RuntimeMethod* method)
{
	{
		__this->set_DisplayParameters_4((bool)1);
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		int32_t L_1 = ___buttonSize1;
		__this->set_ButtonHeight_0(L_1);
		int32_t L_2 = ___parameterBtnStyle2;
		__this->set_Style_2(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ButtonGroupAttribute::.ctor(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonGroupAttribute__ctor_m0054EA0E67B1D3755C80EB056F5EBD30F6F19200 (ButtonGroupAttribute_tCDADB0FE0D3E9BFA27DF33D6451869B8EC8E857C * __this, String_t* ___group0, float ___order1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___group0;
		float L_1 = ___order1;
		PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ChildGameObjectsOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChildGameObjectsOnlyAttribute__ctor_m72F82790CBA3BE06BF8A5019D17420040B0C9AC9 (ChildGameObjectsOnlyAttribute_t64A28A6D4B2AAE55BA804CDFC266C0A8681BD920 * __this, const RuntimeMethod* method)
{
	{
		__this->set_IncludeSelf_0((bool)1);
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ColorPaletteAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorPaletteAttribute__ctor_m0FB1C53EA41531CAF7CDF5F24930769F245E154C (ColorPaletteAttribute_t933AEAC8970C941B1074D80041861812D7170DA4 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		__this->set_PaletteName_0((String_t*)NULL);
		__this->set_ShowAlpha_1((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ColorPaletteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorPaletteAttribute__ctor_m781B42126F89B6F56DE709163D0A4D74E5A9BCBE (ColorPaletteAttribute_t933AEAC8970C941B1074D80041861812D7170DA4 * __this, String_t* ___paletteName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___paletteName0;
		__this->set_PaletteName_0(L_0);
		__this->set_ShowAlpha_1((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.CustomContextMenuAttribute::get_MethodName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CustomContextMenuAttribute_get_MethodName_m7D7FC1ECD9F4B0434E85FA639C4B3520C5A0A9A3 (CustomContextMenuAttribute_tB2D58375FEB292B02B5442B5F1282A7796A4B007 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Action_1();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.CustomContextMenuAttribute::set_MethodName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomContextMenuAttribute_set_MethodName_m0DACAC5D5FBB0CCB210649D026953C4907769B4C (CustomContextMenuAttribute_tB2D58375FEB292B02B5442B5F1282A7796A4B007 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_Action_1(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.CustomContextMenuAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomContextMenuAttribute__ctor_mC29FF6E64432B517E79697D8D17715B3F06A0C00 (CustomContextMenuAttribute_tB2D58375FEB292B02B5442B5F1282A7796A4B007 * __this, String_t* ___menuItem0, String_t* ___action1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuItem0;
		__this->set_MenuItem_0(L_0);
		String_t* L_1 = ___action1;
		__this->set_Action_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.CustomValueDrawerAttribute::get_MethodName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CustomValueDrawerAttribute_get_MethodName_mA5767EFD5CF8207C1BE229FD422FC3FC44230CE4 (CustomValueDrawerAttribute_tF14F13C43F3B695A6A04C52ABC0BCED76D8D4278 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Action_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.CustomValueDrawerAttribute::set_MethodName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomValueDrawerAttribute_set_MethodName_mF28068EC65118ECA2A1B38558EC857BD89693878 (CustomValueDrawerAttribute_tF14F13C43F3B695A6A04C52ABC0BCED76D8D4278 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_Action_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.CustomValueDrawerAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomValueDrawerAttribute__ctor_m7B3ECD14426CFC6B76E7239C23B476F684779AE5 (CustomValueDrawerAttribute_tF14F13C43F3B695A6A04C52ABC0BCED76D8D4278 * __this, String_t* ___action0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___action0;
		__this->set_Action_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DelayedPropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DelayedPropertyAttribute__ctor_m489FF80D8A9B462702E606A4DF2BE009D99FF849 (DelayedPropertyAttribute_t63A7B3586325CC6B399DA0D36EBF16D3905EE679 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DetailedInfoBoxAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DetailedInfoBoxAttribute__ctor_m145816EE77ED956C065B36607C1DAEA8FA5BAA3C (DetailedInfoBoxAttribute_tCBB726C7E8955A4F2261373DF09951C49BB79F97 * __this, String_t* ___message0, String_t* ___details1, int32_t ___infoMessageType2, String_t* ___visibleIf3, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set_Message_0(L_0);
		String_t* L_1 = ___details1;
		__this->set_Details_1(L_1);
		int32_t L_2 = ___infoMessageType2;
		__this->set_InfoMessageType_2(L_2);
		String_t* L_3 = ___visibleIf3;
		__this->set_VisibleIf_3(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DictionaryDrawerSettings::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictionaryDrawerSettings__ctor_mCD32097B13EC46AEFE9ED48441540D7C131140D5 (DictionaryDrawerSettings_t6F42380B0928BCFD6B9B3CFF92D3558518CDD011 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictionaryDrawerSettings__ctor_mCD32097B13EC46AEFE9ED48441540D7C131140D5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_KeyLabel_0(_stringLiteralC67DD20EE842986086A0A915DDC2A279490130ED);
		__this->set_ValueLabel_1(_stringLiteral8DCE170DE238B1FEDA2ECD9674EA3CA0D068FBCB);
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableContextMenuAttribute::.ctor(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableContextMenuAttribute__ctor_m3528AA0C5A479BE6ABAF1F4495722FB590D028C2 (DisableContextMenuAttribute_t69DDDBD94727983567A3793C3CFCFA00814E6DC2 * __this, bool ___disableForMember0, bool ___disableCollectionElements1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		bool L_0 = ___disableForMember0;
		__this->set_DisableForMember_0(L_0);
		bool L_1 = ___disableCollectionElements1;
		__this->set_DisableForCollectionElements_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.DisableIfAttribute::get_MemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* DisableIfAttribute_get_MemberName_m1D81D1FA3D91512BB919424574C0EC98F8A0BBCD (DisableIfAttribute_t44366AD5F4EF373AF85E952BEA715D5A40715377 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Condition_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.DisableIfAttribute::set_MemberName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableIfAttribute_set_MemberName_mFEC9F57C525825A0831C81208ED093901CBAD7A7 (DisableIfAttribute_t44366AD5F4EF373AF85E952BEA715D5A40715377 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_Condition_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.DisableIfAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableIfAttribute__ctor_m461AF14E80789329EF1019B33DE25D56CF6CBD18 (DisableIfAttribute_t44366AD5F4EF373AF85E952BEA715D5A40715377 * __this, String_t* ___condition0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___condition0;
		__this->set_Condition_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.DisableIfAttribute::.ctor(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableIfAttribute__ctor_mFC765F2149E0A3CCFEA59CD75B2276B87314B20F (DisableIfAttribute_t44366AD5F4EF373AF85E952BEA715D5A40715377 * __this, String_t* ___condition0, RuntimeObject * ___optionalValue1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___condition0;
		__this->set_Condition_0(L_0);
		RuntimeObject * L_1 = ___optionalValue1;
		__this->set_Value_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInEditorModeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInEditorModeAttribute__ctor_m13F7D48945BD6670A28279DDB616078B9B2F43C6 (DisableInEditorModeAttribute_t07461DBCB20EE922ABC319603E5954C3F00CBA44 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInInlineEditorsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInInlineEditorsAttribute__ctor_mF8B7E6813BD5C9AFBD78A5C49721EB37D56C6560 (DisableInInlineEditorsAttribute_t5E128B9CE96EC7F787672A3C8950E055F4022110 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInNonPrefabsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInNonPrefabsAttribute__ctor_m63D4B41F0E0A99B087DCF8B11E5DBBC427F6451A (DisableInNonPrefabsAttribute_t0D00848E037C1AAAB99D462E6FF885078A300D7E * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInPlayModeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInPlayModeAttribute__ctor_m97B7FB5CCD60F02CEF1099B6743878DA3615A774 (DisableInPlayModeAttribute_t9E36813C99F4DC97259581CB1CE03B0F174B32BA * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInPrefabAssetsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInPrefabAssetsAttribute__ctor_m011CA2F9971FA999EF734041BAB91872317B359B (DisableInPrefabAssetsAttribute_t461A48998B4E32C9C110A5641BF71D57484DF76D * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInPrefabInstancesAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInPrefabInstancesAttribute__ctor_m5E47228B190D987477F41C86ECB66484007FD5EC (DisableInPrefabInstancesAttribute_tBDF4782E7ED0010E287CBE47AB4EFCDC3D2C915C * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInPrefabsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInPrefabsAttribute__ctor_m4E3DA39C7874E60D102C85506B59280968346E3B (DisableInPrefabsAttribute_tC5A57835853CBCBAFF19B5678BEAA7E5A050EE9B * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisplayAsStringAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplayAsStringAttribute__ctor_m6B84964662A8187BE17BD6334934BA2B409A2ACC (DisplayAsStringAttribute_t677D6873B0CED543705D9C9D9C7C02108AE705E9 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		__this->set_Overflow_0((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.DisplayAsStringAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplayAsStringAttribute__ctor_mFD5330EA486F7C1876B7DAD38322173D33A51D7C (DisplayAsStringAttribute_t677D6873B0CED543705D9C9D9C7C02108AE705E9 * __this, bool ___overflow0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		bool L_0 = ___overflow0;
		__this->set_Overflow_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DoNotDrawAsReferenceAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DoNotDrawAsReferenceAttribute__ctor_m09DF620481A8C06F01488885A40FA3B7749ACB11 (DoNotDrawAsReferenceAttribute_tB5A4B9DEAF2766E27A1DB535A8A65C3C37E12C47 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DontApplyToListElementsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DontApplyToListElementsAttribute__ctor_m39E251F2A1BA63B4A48895001AC81CD035D6E113 (DontApplyToListElementsAttribute_tC34D036BA277A188748A5E8D6C5C2FEB90EBF666 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DrawWithUnityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrawWithUnityAttribute__ctor_m255ED9DDDBD0EEA71807B8C6BB304604F1B020A5 (DrawWithUnityAttribute_t3995454B2F87DDEE9CDF30C1E9674EE65F1A5961 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnableForPrefabOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnableForPrefabOnlyAttribute__ctor_m2C520605A683345636EF23DBC8A03AC0E6D2FDAF (EnableForPrefabOnlyAttribute_t097E39FDE22B623C6201BBEEEF3278A093AE0648 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnableGUIAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnableGUIAttribute__ctor_mCC16666621929D16DCBB1A8B4F900EC44EDC9A35 (EnableGUIAttribute_t0A76255CB3C8DADBA1259980D6551ABF6A91E4A2 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.EnableIfAttribute::get_MemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* EnableIfAttribute_get_MemberName_mD2198B861D98311267B4856B0A450CB382C3BC42 (EnableIfAttribute_t28619931329D231822A79B006B44F096DE437E9A * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Condition_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.EnableIfAttribute::set_MemberName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnableIfAttribute_set_MemberName_m5E4EC1A7D06273D9BBA3305FF49DA57009F1123D (EnableIfAttribute_t28619931329D231822A79B006B44F096DE437E9A * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_Condition_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.EnableIfAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnableIfAttribute__ctor_m1F1F838D286FAE9370AD489B884E42E1B73AF976 (EnableIfAttribute_t28619931329D231822A79B006B44F096DE437E9A * __this, String_t* ___condition0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___condition0;
		__this->set_Condition_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.EnableIfAttribute::.ctor(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnableIfAttribute__ctor_m3C1429D1927B76D03B47D67EF295DC6C0614CE39 (EnableIfAttribute_t28619931329D231822A79B006B44F096DE437E9A * __this, String_t* ___condition0, RuntimeObject * ___optionalValue1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___condition0;
		__this->set_Condition_0(L_0);
		RuntimeObject * L_1 = ___optionalValue1;
		__this->set_Value_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnumPagingAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumPagingAttribute__ctor_m749BBBC6A2FC95712729B4DE19C72B8849102F9E (EnumPagingAttribute_t1FEB069556F6126E3D05095600132A589FB0F135 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnumToggleButtonsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumToggleButtonsAttribute__ctor_mE37FDB25B17CE53DA754A3E0F830F396318F34F5 (EnumToggleButtonsAttribute_tCE472D5DA47F333E38C7537FDF0776B73659E2DE * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.FilePathAttribute::get_ReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FilePathAttribute_get_ReadOnly_mE6D353FDC464B3A9EFF55EF78C02C575E41E6C7E (FilePathAttribute_tB6DD8A50FC2A4C8ED0FD8FB5158A2BBB47AC045F * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CReadOnlyU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.FilePathAttribute::set_ReadOnly(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FilePathAttribute_set_ReadOnly_m221DD6A033004FB5626EFF6FA055CADE2B60E780 (FilePathAttribute_tB6DD8A50FC2A4C8ED0FD8FB5158A2BBB47AC045F * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CReadOnlyU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.FilePathAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FilePathAttribute__ctor_m141C277EA439B817D75D6A4378EEBBC854B30DC3 (FilePathAttribute_tB6DD8A50FC2A4C8ED0FD8FB5158A2BBB47AC045F * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.FolderPathAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FolderPathAttribute__ctor_m86B2038DF79EA2F7847EAAA1CC2344A740D8C965 (FolderPathAttribute_t63E1631A434EE753FCD86E7273812B2E89FFDF94 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::get_Expanded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FoldoutGroupAttribute_get_Expanded_m688163003BD99BF4E49564C644275EB8911A236D (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_expanded_6();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::set_Expanded(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_set_Expanded_m48641F7D01ECE8E41D43960FA1F3FD0CB43976B4 (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_expanded_6(L_0);
		FoldoutGroupAttribute_set_HasDefinedExpanded_mD585B02097F78E9049B2D144D50B503EF882B623_inline(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::get_HasDefinedExpanded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FoldoutGroupAttribute_get_HasDefinedExpanded_m3926B57853EA8BB7ACCACCC6740073D46F0C373C (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CHasDefinedExpandedU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::set_HasDefinedExpanded(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_set_HasDefinedExpanded_mD585B02097F78E9049B2D144D50B503EF882B623 (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CHasDefinedExpandedU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::.ctor(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FoldoutGroupAttribute__ctor_m3806B43B9A2E11EC080F54305AC9F5F3E0CFD909 (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, String_t* ___groupName0, float ___order1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupName0;
		float L_1 = ___order1;
		PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::.ctor(System.String,System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FoldoutGroupAttribute__ctor_m8CDE96FF16A2558CA79FDD44704591AE6F2FC2DF (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, String_t* ___groupName0, bool ___expanded1, float ___order2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupName0;
		float L_1 = ___order2;
		PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = ___expanded1;
		__this->set_expanded_6(L_2);
		FoldoutGroupAttribute_set_HasDefinedExpanded_mD585B02097F78E9049B2D144D50B503EF882B623_inline(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_CombineValuesWith_m190C03737485283118946483BC9A40A3256FB6DB (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FoldoutGroupAttribute_CombineValuesWith_m190C03737485283118946483BC9A40A3256FB6DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * V_0 = NULL;
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_0 = ___other0;
		V_0 = ((FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD *)IsInstClass((RuntimeObject*)L_0, FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD_il2cpp_TypeInfo_var));
		FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = FoldoutGroupAttribute_get_HasDefinedExpanded_m3926B57853EA8BB7ACCACCC6740073D46F0C373C_inline(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		FoldoutGroupAttribute_set_HasDefinedExpanded_mD585B02097F78E9049B2D144D50B503EF882B623_inline(__this, (bool)1, /*hidden argument*/NULL);
		FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = FoldoutGroupAttribute_get_Expanded_m688163003BD99BF4E49564C644275EB8911A236D_inline(L_3, /*hidden argument*/NULL);
		FoldoutGroupAttribute_set_Expanded_m48641F7D01ECE8E41D43960FA1F3FD0CB43976B4(__this, L_4, /*hidden argument*/NULL);
	}

IL_0022:
	{
		bool L_5 = FoldoutGroupAttribute_get_HasDefinedExpanded_m3926B57853EA8BB7ACCACCC6740073D46F0C373C_inline(__this, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003d;
		}
	}
	{
		FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * L_6 = V_0;
		NullCheck(L_6);
		FoldoutGroupAttribute_set_HasDefinedExpanded_mD585B02097F78E9049B2D144D50B503EF882B623_inline(L_6, (bool)1, /*hidden argument*/NULL);
		FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * L_7 = V_0;
		bool L_8 = FoldoutGroupAttribute_get_Expanded_m688163003BD99BF4E49564C644275EB8911A236D_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		FoldoutGroupAttribute_set_Expanded_m48641F7D01ECE8E41D43960FA1F3FD0CB43976B4(L_7, L_8, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.GUIColorAttribute::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUIColorAttribute__ctor_mB266561305C367CE45651057D8D4DF36D99FCAD3 (GUIColorAttribute_tDE7A1E234D4277B5079B9921967945DB5CA56B03 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		float L_0 = ___r0;
		float L_1 = ___g1;
		float L_2 = ___b2;
		float L_3 = ___a3;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_Color_0(L_4);
		return;
	}
}
// System.Void Sirenix.OdinInspector.GUIColorAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUIColorAttribute__ctor_mEFBD166806FD6D2338758F5F368AFDF3E8347E91 (GUIColorAttribute_tDE7A1E234D4277B5079B9921967945DB5CA56B03 * __this, String_t* ___getColor0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___getColor0;
		__this->set_GetColor_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideDuplicateReferenceBoxAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideDuplicateReferenceBoxAttribute__ctor_m9003D334EB4C6B39944428C496CA8A0192F04177 (HideDuplicateReferenceBoxAttribute_t91004BA0AB0CD17A308E7FE988D0038FB00150E5 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.HideIfAttribute::get_MemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* HideIfAttribute_get_MemberName_m5C0C4FB895E1A3788F62DDE0A0680920EE3BD8F1 (HideIfAttribute_t011B476D6F3C9156B1886271EFB256C8EC3D76FA * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Condition_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.HideIfAttribute::set_MemberName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfAttribute_set_MemberName_m475241B5D603C50C78DDED174F3DBE823F7A871C (HideIfAttribute_t011B476D6F3C9156B1886271EFB256C8EC3D76FA * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_Condition_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HideIfAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfAttribute__ctor_m6DB6CACEF83AE294D4888CC58DA860DC785E0D01 (HideIfAttribute_t011B476D6F3C9156B1886271EFB256C8EC3D76FA * __this, String_t* ___condition0, bool ___animate1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___condition0;
		__this->set_Condition_0(L_0);
		bool L_1 = ___animate1;
		__this->set_Animate_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HideIfAttribute::.ctor(System.String,System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfAttribute__ctor_m0EF2AB456FB856870BCC79B310E92DB56FC7E3B9 (HideIfAttribute_t011B476D6F3C9156B1886271EFB256C8EC3D76FA * __this, String_t* ___condition0, RuntimeObject * ___optionalValue1, bool ___animate2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___condition0;
		__this->set_Condition_0(L_0);
		RuntimeObject * L_1 = ___optionalValue1;
		__this->set_Value_1(L_1);
		bool L_2 = ___animate2;
		__this->set_Animate_2(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.HideIfGroupAttribute::get_Animate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HideIfGroupAttribute_get_Animate_mF5AA29F3C90F552A9998C8776912B737D66C4DE3 (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = ((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->get_AnimateVisibility_5();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.HideIfGroupAttribute::set_Animate(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfGroupAttribute_set_Animate_m4D3CD713242CB20F8E4109EB386B49F2A1231305 (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->set_AnimateVisibility_5(L_0);
		return;
	}
}
// System.String Sirenix.OdinInspector.HideIfGroupAttribute::get_MemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* HideIfGroupAttribute_get_MemberName_mFEA756BD4855C66E710EE842D93D08B9755EA461 (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = HideIfGroupAttribute_get_Condition_mE6857BF4B30ABAC93E337FB918D2CF71AD0B0631(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.HideIfGroupAttribute::set_MemberName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfGroupAttribute_set_MemberName_m982CF7DDCA552327AE01B1D09E75DF5DD2D5CF27 (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		HideIfGroupAttribute_set_Condition_m8FB8D4239B1C0E2A8C112A7AE01B5CD4FF400DDA_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String Sirenix.OdinInspector.HideIfGroupAttribute::get_Condition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* HideIfGroupAttribute_get_Condition_mE6857BF4B30ABAC93E337FB918D2CF71AD0B0631 (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->get_VisibleIf_4();
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_2 = ((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->get_VisibleIf_4();
		return L_2;
	}

IL_0014:
	{
		String_t* L_3 = ((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->get_GroupName_1();
		return L_3;
	}
}
// System.Void Sirenix.OdinInspector.HideIfGroupAttribute::set_Condition(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfGroupAttribute_set_Condition_m8FB8D4239B1C0E2A8C112A7AE01B5CD4FF400DDA (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->set_VisibleIf_4(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HideIfGroupAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfGroupAttribute__ctor_m4906C608EE64D1E886FA86D29A8DCB4CCD197FD1 (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, String_t* ___path0, bool ___animate1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___path0;
		PropertyGroupAttribute__ctor_m8F7EF143FEDD12FAC7B22FBAC7BCB4885A09DF88(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___animate1;
		HideIfGroupAttribute_set_Animate_m4D3CD713242CB20F8E4109EB386B49F2A1231305_inline(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HideIfGroupAttribute::.ctor(System.String,System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfGroupAttribute__ctor_m3267514613583C7F46E2568CCF1B657EB1A27474 (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, String_t* ___path0, RuntimeObject * ___value1, bool ___animate2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___path0;
		PropertyGroupAttribute__ctor_m8F7EF143FEDD12FAC7B22FBAC7BCB4885A09DF88(__this, L_0, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___value1;
		__this->set_Value_6(L_1);
		bool L_2 = ___animate2;
		HideIfGroupAttribute_set_Animate_m4D3CD713242CB20F8E4109EB386B49F2A1231305_inline(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HideIfGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfGroupAttribute_CombineValuesWith_m75EBFB293E2957D9B6F2C26E79950B90E6ECCBC5 (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HideIfGroupAttribute_CombineValuesWith_m75EBFB293E2957D9B6F2C26E79950B90E6ECCBC5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * V_0 = NULL;
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_0 = ___other0;
		V_0 = ((HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 *)IsInstClass((RuntimeObject*)L_0, HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238_il2cpp_TypeInfo_var));
		RuntimeObject * L_1 = __this->get_Value_6();
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * L_2 = V_0;
		RuntimeObject * L_3 = __this->get_Value_6();
		NullCheck(L_2);
		L_2->set_Value_6(L_3);
	}

IL_001b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInEditorModeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInEditorModeAttribute__ctor_mD8E7DE60394D38D6BE9C83A41EF814D5CD92217D (HideInEditorModeAttribute_t65C98A206B4DC8AC9240C5546D0A6D3827CE9E57 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInInlineEditorsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInlineEditorsAttribute__ctor_m78318BFF82C4AB6286362D19382FC488B157C37A (HideInInlineEditorsAttribute_t177610D85D2123274AD4EA5FE2C8A7DFF7D2240C * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInNonPrefabsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInNonPrefabsAttribute__ctor_m38C345ABE1AD02E5CE773566C53D5628C23D59E0 (HideInNonPrefabsAttribute_tBD5D04C53C1C275F341CD8AD160EFADA158427C0 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInPlayModeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInPlayModeAttribute__ctor_m3BCD0DEAA80C0647A75BDEEF33FAE9D6A4B506A7 (HideInPlayModeAttribute_t627CDE4C0FADA1E6E9197842268B6077ED7004FC * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInPrefabAssetsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInPrefabAssetsAttribute__ctor_m56DF3C5E0803931801A0818BFF80482ECBE185E0 (HideInPrefabAssetsAttribute_t7BB906033FB4DF85FABE31388AF559725E98ED77 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInPrefabInstancesAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInPrefabInstancesAttribute__ctor_m9DCF14D9FC674E03B655300CAAC03356DA89E6C8 (HideInPrefabInstancesAttribute_t7148B73699DA6BE500EF82AE3223E5C89DC4D98F * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInPrefabsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInPrefabsAttribute__ctor_mB976FD8E6F61588DF5FEAAEDB2D84FE377F291BC (HideInPrefabsAttribute_t28D32D1AD69258DF301288D58E42CD5E14FDABAA * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInTablesAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInTablesAttribute__ctor_m9082E7F39E533A3F2279713ED33C8D48ACBACFF8 (HideInTablesAttribute_tCC2FB41AEC83E4CA159714F7238B51FD9AC5D546 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideLabelAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideLabelAttribute__ctor_mB86B50C32B17E33A0A6FAF5AEE634D05E65C10DF (HideLabelAttribute_t451BC6D3C8B4E1C9A9E75D4C1F308C3ED301C71F * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideMonoScriptAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideMonoScriptAttribute__ctor_mFAD292E42B94F432DE0379201BF2F0C7790B7768 (HideMonoScriptAttribute_t8EF9302A9377F51F515B10BB0AB16844636A9599 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideNetworkBehaviourFieldsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideNetworkBehaviourFieldsAttribute__ctor_mD80534B899DAA097D8011A5500EA103C30028950 (HideNetworkBehaviourFieldsAttribute_tCD095386566090284A9A9E3806805FF73B9E83F3 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideReferenceObjectPickerAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideReferenceObjectPickerAttribute__ctor_m30AC48FD898831668E93D8508CB750E1B3C7D415 (HideReferenceObjectPickerAttribute_tE06BCAACEB5B5CA565BA3BC25EA11D7EA22C6F28 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.String,System.Single,System.Int32,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HorizontalGroupAttribute__ctor_mD3D3178ECF3EC429A1C254046F0A5F589451C9C2 (HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5 * __this, String_t* ___group0, float ___width1, int32_t ___marginLeft2, int32_t ___marginRight3, float ___order4, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___group0;
		float L_1 = ___order4;
		PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A(__this, L_0, L_1, /*hidden argument*/NULL);
		float L_2 = ___width1;
		__this->set_Width_6(L_2);
		int32_t L_3 = ___marginLeft2;
		__this->set_MarginLeft_7((((float)((float)L_3))));
		int32_t L_4 = ___marginRight3;
		__this->set_MarginRight_8((((float)((float)L_4))));
		return;
	}
}
// System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.Single,System.Int32,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HorizontalGroupAttribute__ctor_mDE8859B5F8E6ACA485847210F51CE37DDE2531A1 (HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5 * __this, float ___width0, int32_t ___marginLeft1, int32_t ___marginRight2, float ___order3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HorizontalGroupAttribute__ctor_mDE8859B5F8E6ACA485847210F51CE37DDE2531A1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___width0;
		int32_t L_1 = ___marginLeft1;
		int32_t L_2 = ___marginRight2;
		float L_3 = ___order3;
		HorizontalGroupAttribute__ctor_mD3D3178ECF3EC429A1C254046F0A5F589451C9C2(__this, _stringLiteralD2E5419C223893DD079C1E7DFFE254D8D03B6B36, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HorizontalGroupAttribute_CombineValuesWith_m358F04BFE0348EA6FFE6FDF75342FAA1EB65014A (HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5 * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HorizontalGroupAttribute_CombineValuesWith_m358F04BFE0348EA6FFE6FDF75342FAA1EB65014A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5 * G_B1_1 = NULL;
	{
		String_t* L_0 = __this->get_Title_13();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0016;
		}
	}
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_2 = ___other0;
		NullCheck(((HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5 *)IsInstClass((RuntimeObject*)L_2, HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5_il2cpp_TypeInfo_var)));
		String_t* L_3 = ((HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5 *)IsInstClass((RuntimeObject*)L_2, HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5_il2cpp_TypeInfo_var))->get_Title_13();
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0016:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_Title_13(G_B2_0);
		float L_4 = __this->get_LabelWidth_14();
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_5 = ___other0;
		NullCheck(((HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5 *)IsInstClass((RuntimeObject*)L_5, HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5_il2cpp_TypeInfo_var)));
		float L_6 = ((HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5 *)IsInstClass((RuntimeObject*)L_5, HorizontalGroupAttribute_t6EB8B994A876C6BD892C048D968261E4033737F5_il2cpp_TypeInfo_var))->get_LabelWidth_14();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var);
		float L_7 = Math_Max_m545895C37C1F738BBB653CA1F65E50FF1D197675(L_4, L_6, /*hidden argument*/NULL);
		__this->set_LabelWidth_14(L_7);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.IncludeMyAttributesAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IncludeMyAttributesAttribute__ctor_mD6627C9E1A06B23183641DFAD937CBD15577328F (IncludeMyAttributesAttribute_tFCF24E3A971A142A66755F0C43C6B140D1D55327 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.IndentAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndentAttribute__ctor_mDCB458272D695BD2953944B28BBC9F235F0AE6A9 (IndentAttribute_t8DB2B60FF1214619E8901509CE31C77EA2CD6DE8 * __this, int32_t ___indentLevel0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___indentLevel0;
		__this->set_IndentLevel_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.InfoBoxAttribute::.ctor(System.String,Sirenix.OdinInspector.InfoMessageType,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InfoBoxAttribute__ctor_m6BAFAA096773C816F00107526B2D96CAB28441DD (InfoBoxAttribute_t4134D5269F4439BB4423FC022453606650597B3A * __this, String_t* ___message0, int32_t ___infoMessageType1, String_t* ___visibleIfMemberName2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set_Message_0(L_0);
		int32_t L_1 = ___infoMessageType1;
		__this->set_InfoMessageType_1(L_1);
		String_t* L_2 = ___visibleIfMemberName2;
		__this->set_VisibleIf_2(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.InfoBoxAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InfoBoxAttribute__ctor_m0BC51BCFA7915BD71C9BFE2C30FD3A7C28ED423A (InfoBoxAttribute_t4134D5269F4439BB4423FC022453606650597B3A * __this, String_t* ___message0, String_t* ___visibleIfMemberName1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set_Message_0(L_0);
		__this->set_InfoMessageType_1(1);
		String_t* L_1 = ___visibleIfMemberName1;
		__this->set_VisibleIf_2(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.InlineButtonAttribute::get_MemberMethod()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* InlineButtonAttribute_get_MemberMethod_mAD5C9BAF96545FE2DB4034B407E97847E5E0EDF3 (InlineButtonAttribute_t4E0014F9C6D3D55B24D12C714E8E35E4364B1AB6 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Action_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.InlineButtonAttribute::set_MemberMethod(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineButtonAttribute_set_MemberMethod_mCFD34ED6D9B04F9C4112B91AEA84B7546B4B8D24 (InlineButtonAttribute_t4E0014F9C6D3D55B24D12C714E8E35E4364B1AB6 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_Action_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.InlineButtonAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineButtonAttribute__ctor_m01CB0BA9AF4B90E349CDAF936BB0BD9EDA76C512 (InlineButtonAttribute_t4E0014F9C6D3D55B24D12C714E8E35E4364B1AB6 * __this, String_t* ___action0, String_t* ___label1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___action0;
		__this->set_Action_0(L_0);
		String_t* L_1 = ___label1;
		__this->set_Label_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::get_Expanded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InlineEditorAttribute_get_Expanded_m21A55631239C1383A3C08C4DC3C061807A6807EB (InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_expanded_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.InlineEditorAttribute::set_Expanded(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineEditorAttribute_set_Expanded_mFB30F6438023DAD90B5290595289B5C854A788A4 (InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_expanded_0(L_0);
		InlineEditorAttribute_set_ExpandedHasValue_m393D00D432705F319C5FCDF2B2571461B781D9C5_inline(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::get_ExpandedHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InlineEditorAttribute_get_ExpandedHasValue_mFC276BDF5631A5870038BB98C18386F949E01EDF (InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CExpandedHasValueU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.InlineEditorAttribute::set_ExpandedHasValue(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineEditorAttribute_set_ExpandedHasValue_m393D00D432705F319C5FCDF2B2571461B781D9C5 (InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CExpandedHasValueU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorModes,Sirenix.OdinInspector.InlineEditorObjectFieldModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineEditorAttribute__ctor_m2C122F43FF3CA6D75809623A950AAA6B2BC3939D (InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216 * __this, int32_t ___inlineEditorMode0, int32_t ___objectFieldMode1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InlineEditorAttribute__ctor_m2C122F43FF3CA6D75809623A950AAA6B2BC3939D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_PreviewWidth_5((100.0f));
		__this->set_PreviewHeight_6((35.0f));
		__this->set_IncrementInlineEditorDrawerDepth_7((bool)1);
		__this->set_DisableGUIForVCSLockedAssets_9((bool)1);
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___objectFieldMode1;
		__this->set_ObjectFieldMode_8(L_0);
		int32_t L_1 = ___inlineEditorMode0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0051;
			}
			case 1:
			{
				goto IL_0059;
			}
			case 2:
			{
				goto IL_0068;
			}
			case 3:
			{
				goto IL_0077;
			}
			case 4:
			{
				goto IL_0086;
			}
			case 5:
			{
				goto IL_00a0;
			}
		}
	}
	{
		goto IL_00b6;
	}

IL_0051:
	{
		__this->set_DrawGUI_2((bool)1);
		return;
	}

IL_0059:
	{
		__this->set_DrawGUI_2((bool)1);
		__this->set_DrawHeader_1((bool)1);
		return;
	}

IL_0068:
	{
		__this->set_DrawGUI_2((bool)1);
		__this->set_DrawPreview_3((bool)1);
		return;
	}

IL_0077:
	{
		__this->set_expanded_0((bool)1);
		__this->set_DrawPreview_3((bool)1);
		return;
	}

IL_0086:
	{
		__this->set_expanded_0((bool)1);
		__this->set_DrawPreview_3((bool)1);
		__this->set_PreviewHeight_6((170.0f));
		return;
	}

IL_00a0:
	{
		__this->set_DrawGUI_2((bool)1);
		__this->set_DrawHeader_1((bool)1);
		__this->set_DrawPreview_3((bool)1);
		return;
	}

IL_00b6:
	{
		NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4 * L_2 = (NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4 *)il2cpp_codegen_object_new(NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m8BEA657E260FC05F0C6D2C43A6E9BC08040F59C4(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, InlineEditorAttribute__ctor_m2C122F43FF3CA6D75809623A950AAA6B2BC3939D_RuntimeMethod_var);
	}
}
// System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorObjectFieldModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineEditorAttribute__ctor_mE7016B8D252EE799B6E68AB45CEEC1F5755E8509 (InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216 * __this, int32_t ___objectFieldMode0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___objectFieldMode0;
		InlineEditorAttribute__ctor_m2C122F43FF3CA6D75809623A950AAA6B2BC3939D(__this, 0, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.InlinePropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlinePropertyAttribute__ctor_m38FC46B53AAC32D93B97537E0922EB88CDE2675C (InlinePropertyAttribute_t7094D1D53D31ACA38BD2F40D93D24BB00ADFD6FA * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.LabelTextAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LabelTextAttribute__ctor_mE3294102CD8D345ED7DC93D561D14F4611E7A06A (LabelTextAttribute_t05B2F25125BFF01D694EC3DBED7B9B045B16ACC9 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___text0;
		__this->set_Text_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.LabelTextAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LabelTextAttribute__ctor_m3C6152F987F50040E983A67D681E8B5692C9AC18 (LabelTextAttribute_t05B2F25125BFF01D694EC3DBED7B9B045B16ACC9 * __this, String_t* ___text0, bool ___nicifyText1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___text0;
		__this->set_Text_0(L_0);
		bool L_1 = ___nicifyText1;
		__this->set_NicifyText_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.LabelWidthAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LabelWidthAttribute__ctor_m7DCB42E4880AD8CDE03563924EB5AF64BB933BE8 (LabelWidthAttribute_tD46C5A741BE80B8CA6CFC7DAB33E13508A88FF6C * __this, float ___width0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		float L_0 = ___width0;
		__this->set_Width_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowPaging()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowPaging_mF4F4A82094705EF34FC332B5034FF19BAA2780C3 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_paging_12();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowPaging(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_ShowPaging_m7A1AA3022CDB3B19F854B95E437D35437C3ED7FA (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_paging_12(L_0);
		__this->set_pagingHasValue_16((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_DraggableItems()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_DraggableItems_mC99F8EE2C8FEE3CA05418E4A27435C7BC1F4F9CE (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_draggable_13();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_DraggableItems(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_DraggableItems_mCC2F079A739AC071CA9DB84E6E53ED450A82BB39 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_draggable_13(L_0);
		__this->set_draggableHasValue_17((bool)1);
		return;
	}
}
// System.Int32 Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_NumberOfItemsPerPage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ListDrawerSettingsAttribute_get_NumberOfItemsPerPage_mC88191D328036CC550D229C274C049E28EA874BA (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_numberOfItemsPerPage_11();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_NumberOfItemsPerPage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_NumberOfItemsPerPage_m212DEF5B7C88C6C4903064572DA880D74241FDD4 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_numberOfItemsPerPage_11(L_0);
		__this->set_numberOfItemsPerPageHasValue_22((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_IsReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_IsReadOnly_mDA290460ED0767D4D0EBFA60F9B5C2E2862319EC (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isReadOnly_14();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_IsReadOnly(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_IsReadOnly_mC95DBB25014CC6602621E836455FFFBD7F5DC510 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isReadOnly_14(L_0);
		__this->set_isReadOnlyHasValue_18((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowItemCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowItemCount_mB88A50379410C28ED127B58ADE328A3FDF3B446B (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showItemCount_15();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowItemCount(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_ShowItemCount_mF07F2DBCEFB8F88089CC817C5D72202B7E037C0D (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_showItemCount_15(L_0);
		__this->set_showItemCountHasValue_19((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_Expanded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_Expanded_m918C4D4C6AFF9485D88F9CFD870C7216564430B9 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_expanded_20();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_Expanded(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_Expanded_m54D4856BD28E0AF89BF6D075F91E484C08F6A5E2 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_expanded_20(L_0);
		__this->set_expandedHasValue_21((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowIndexLabels()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowIndexLabels_m631D9046E063074DB394730E852914C5710DFF42 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showIndexLabels_23();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowIndexLabels(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_ShowIndexLabels_mC054503B8FF36E2F6412CB89BC90D919A9E3ECD9 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_showIndexLabels_23(L_0);
		__this->set_showIndexLabelsHasValue_24((bool)1);
		return;
	}
}
// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_OnTitleBarGUI()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ListDrawerSettingsAttribute_get_OnTitleBarGUI_mB3DDDC9D3B34DE042FF321FF8025B91D90930299 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_onTitleBarGUI_10();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_OnTitleBarGUI(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_OnTitleBarGUI_m2CEC49FB7F6BF751A2172DD4CAA74027B27DEE22 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_onTitleBarGUI_10(L_0);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_PagingHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_PagingHasValue_m2324C4179F339EDD69C6F7B60FDF1C31F1EBCA68 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_pagingHasValue_16();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowItemCountHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowItemCountHasValue_m8BC569A45F50C6162F694376D2351640D587BA19 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showItemCountHasValue_19();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_NumberOfItemsPerPageHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_NumberOfItemsPerPageHasValue_m91E3D2F6E1370B5439C8A9AB50F1CE91B6AEB7D4 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_numberOfItemsPerPageHasValue_22();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_DraggableHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_DraggableHasValue_m6199435DD60BE83F70C8A062811F20CEF2C37829 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_draggableHasValue_17();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_IsReadOnlyHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_IsReadOnlyHasValue_mF19449F72F043259F929A4D24112000FAF8E85F6 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isReadOnlyHasValue_18();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ExpandedHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ExpandedHasValue_m89E37A4A7BF5206FBDB1D49CD0B3E0F131BA21EE (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_expandedHasValue_21();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowIndexLabelsHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowIndexLabelsHasValue_mE0FFC3BD473061E060471ECC90F5C7C19DD84056 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showIndexLabelsHasValue_24();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute__ctor_m020F9F15913B88F8D060C4D5C1E16E18ECA1C619 (ListDrawerSettingsAttribute_t8B9B565814151050B09459CE51980CBF6818FD1D * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.MaxValueAttribute::.ctor(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaxValueAttribute__ctor_mBC345D2BF67E140D3E243199AC7A970EF5AE8E4A (MaxValueAttribute_t7C88F52D1B04C874C07D6E937F2C83CAED093185 * __this, double ___maxValue0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		double L_0 = ___maxValue0;
		__this->set_MaxValue_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MaxValueAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaxValueAttribute__ctor_mDC89DCEF0E86A2873FE90B65B1B058AF3747B9CC (MaxValueAttribute_t7C88F52D1B04C874C07D6E937F2C83CAED093185 * __this, String_t* ___expression0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___expression0;
		__this->set_Expression_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::get_MinMember()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* MinMaxSliderAttribute_get_MinMember_mE327F8D1415EAFEE019BCD018FBB4462DA236B46 (MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_MinValueGetter_2();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::set_MinMember(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute_set_MinMember_m5E26099BEC8726C15E46128BC6C2B57250FEEA71 (MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_MinValueGetter_2(L_0);
		return;
	}
}
// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::get_MaxMember()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* MinMaxSliderAttribute_get_MaxMember_m29B06F8A83F2BF8A5B46753119CB2D502EDA3C41 (MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_MaxValueGetter_3();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::set_MaxMember(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute_set_MaxMember_m6603B663E5F13F78DEFFC2978E69749C79714083 (MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_MaxValueGetter_3(L_0);
		return;
	}
}
// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::get_MinMaxMember()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* MinMaxSliderAttribute_get_MinMaxMember_m21E19607024528BA48821927215D387356BAB3ED (MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_MinMaxValueGetter_4();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::set_MinMaxMember(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute_set_MinMaxMember_mB42BB3D7E48D263EA75CB69A6EE50CCEE2E5A066 (MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_MinMaxValueGetter_4(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.Single,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m50B38305ECA370A12C393F649B02DE1B5ED6C1C8 (MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461 * __this, float ___minValue0, float ___maxValue1, bool ___showFields2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		float L_0 = ___minValue0;
		__this->set_MinValue_0(L_0);
		float L_1 = ___maxValue1;
		__this->set_MaxValue_1(L_1);
		bool L_2 = ___showFields2;
		__this->set_ShowFields_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m23366474F31036F91A7B90ACC65892C056806C18 (MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461 * __this, String_t* ___minValueGetter0, float ___maxValue1, bool ___showFields2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minValueGetter0;
		__this->set_MinValueGetter_2(L_0);
		float L_1 = ___maxValue1;
		__this->set_MaxValue_1(L_1);
		bool L_2 = ___showFields2;
		__this->set_ShowFields_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.Single,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m94E10DD17D6C52516D7ED2FF4C3F9C3160582DF4 (MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461 * __this, float ___minValue0, String_t* ___maxValueGetter1, bool ___showFields2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		float L_0 = ___minValue0;
		__this->set_MinValue_0(L_0);
		String_t* L_1 = ___maxValueGetter1;
		__this->set_MaxValueGetter_3(L_1);
		bool L_2 = ___showFields2;
		__this->set_ShowFields_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m31DE6EEF1E8D9EAD8FA216FF3709F206091FE5C0 (MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461 * __this, String_t* ___minValueGetter0, String_t* ___maxValueGetter1, bool ___showFields2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minValueGetter0;
		__this->set_MinValueGetter_2(L_0);
		String_t* L_1 = ___maxValueGetter1;
		__this->set_MaxValueGetter_3(L_1);
		bool L_2 = ___showFields2;
		__this->set_ShowFields_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m7FD79AD45A13B4E41D6D8094D863E51CB2234EAE (MinMaxSliderAttribute_t06B8CD9DC00C86A9D48E782A3C1BB06431EC3461 * __this, String_t* ___minMaxValueGetter0, bool ___showFields1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMaxValueGetter0;
		__this->set_MinMaxValueGetter_4(L_0);
		bool L_1 = ___showFields1;
		__this->set_ShowFields_5(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.MinValueAttribute::.ctor(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinValueAttribute__ctor_m9D54B01674E7C6523E8A20BCFC44B7E6ACE03C90 (MinValueAttribute_t69E9653BC5DF176433CF88D0A72F150A73DDAFCD * __this, double ___minValue0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		double L_0 = ___minValue0;
		__this->set_MinValue_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinValueAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinValueAttribute__ctor_m95ECD927C12D1F8F493EDCDB5D20EB8D283AE242 (MinValueAttribute_t69E9653BC5DF176433CF88D0A72F150A73DDAFCD * __this, String_t* ___expression0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___expression0;
		__this->set_Expression_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.MultiLinePropertyAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiLinePropertyAttribute__ctor_mEE7A041CC0A205936F9DA935875F921CE7757D92 (MultiLinePropertyAttribute_t6C8A6AE59171CD7C3D77EE421C7FDD48D846D4CD * __this, int32_t ___lines0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MultiLinePropertyAttribute__ctor_mEE7A041CC0A205936F9DA935875F921CE7757D92_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___lines0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var);
		int32_t L_1 = Math_Max_mA99E48BB021F2E4B62D4EA9F52EA6928EED618A2(1, L_0, /*hidden argument*/NULL);
		__this->set_Lines_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.OdinRegisterAttributeAttribute::.ctor(System.Type,System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OdinRegisterAttributeAttribute__ctor_m6260347940664F402312B72B951F4072A5CBDCFB (OdinRegisterAttributeAttribute_tF3FE293A763B5D61CA1902CF48AAFEAC64543A3F * __this, Type_t * ___attributeType0, String_t* ___category1, String_t* ___description2, bool ___isEnterprise3, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___attributeType0;
		__this->set_AttributeType_0(L_0);
		String_t* L_1 = ___category1;
		__this->set_Categories_1(L_1);
		String_t* L_2 = ___description2;
		__this->set_Description_2(L_2);
		bool L_3 = ___isEnterprise3;
		__this->set_IsEnterprise_4(L_3);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OdinRegisterAttributeAttribute::.ctor(System.Type,System.String,System.String,System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OdinRegisterAttributeAttribute__ctor_m305FAA270A22E7BB4406E17C03ADA1830E174B28 (OdinRegisterAttributeAttribute_tF3FE293A763B5D61CA1902CF48AAFEAC64543A3F * __this, Type_t * ___attributeType0, String_t* ___category1, String_t* ___description2, bool ___isEnterprise3, String_t* ___url4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___attributeType0;
		__this->set_AttributeType_0(L_0);
		String_t* L_1 = ___category1;
		__this->set_Categories_1(L_1);
		String_t* L_2 = ___description2;
		__this->set_Description_2(L_2);
		bool L_3 = ___isEnterprise3;
		__this->set_IsEnterprise_4(L_3);
		String_t* L_4 = ___url4;
		__this->set_DocumentationUrl_3(L_4);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.OnCollectionChangedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnCollectionChangedAttribute__ctor_m982A4122FCD2196709866CB34C8FF8106ACE3A79 (OnCollectionChangedAttribute_tA3CC1CAE8C5A0C81E25B52D82D049EFE8615C73C * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OnCollectionChangedAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnCollectionChangedAttribute__ctor_m343721F74CD7C8FBC42BD3EED73AFB991FDC653E (OnCollectionChangedAttribute_tA3CC1CAE8C5A0C81E25B52D82D049EFE8615C73C * __this, String_t* ___after0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___after0;
		__this->set_After_1(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OnCollectionChangedAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnCollectionChangedAttribute__ctor_m6DB1F00EBB85AE6A0511B8FD9650A2495E8E1C91 (OnCollectionChangedAttribute_tA3CC1CAE8C5A0C81E25B52D82D049EFE8615C73C * __this, String_t* ___before0, String_t* ___after1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___before0;
		__this->set_Before_0(L_0);
		String_t* L_1 = ___after1;
		__this->set_After_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.OnInspectorDisposeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnInspectorDisposeAttribute__ctor_mAD2279BE383A0423F12EF39C4D421C6C3D5ECB08 (OnInspectorDisposeAttribute_t9CF5B6A832469F06C52D7EDC96C3F547A6989BDF * __this, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OnInspectorDisposeAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnInspectorDisposeAttribute__ctor_mE2255EE8D89336EE7BEBBC5EE596101C2A92778E (OnInspectorDisposeAttribute_t9CF5B6A832469F06C52D7EDC96C3F547A6989BDF * __this, String_t* ___action0, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___action0;
		__this->set_Action_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnInspectorGUIAttribute__ctor_m8AE905E81618A1750D139BD7A885A4B7C131689E (OnInspectorGUIAttribute_t82649BAD99432C3B20C66F0126D74AE5FAA2AE55 * __this, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnInspectorGUIAttribute__ctor_m1B66AFEDEC6394DC72DFE0817165C151C878AAC3 (OnInspectorGUIAttribute_t82649BAD99432C3B20C66F0126D74AE5FAA2AE55 * __this, String_t* ___action0, bool ___append1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		bool L_0 = ___append1;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___action0;
		__this->set_Append_1(L_1);
		return;
	}

IL_0011:
	{
		String_t* L_2 = ___action0;
		__this->set_Prepend_0(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnInspectorGUIAttribute__ctor_mA650AA7C5E8F5D1B6E364A9991041E49F3C75730 (OnInspectorGUIAttribute_t82649BAD99432C3B20C66F0126D74AE5FAA2AE55 * __this, String_t* ___prepend0, String_t* ___append1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___prepend0;
		__this->set_Prepend_0(L_0);
		String_t* L_1 = ___append1;
		__this->set_Append_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.OnInspectorInitAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnInspectorInitAttribute__ctor_m8D853E5FF4A7439A5BA4CA59234D58BF7ED76EF2 (OnInspectorInitAttribute_t7564436864B4C2ABC648107C388558D26170EB85 * __this, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OnInspectorInitAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnInspectorInitAttribute__ctor_m74CDD7E081A0A0DF8D451390FBEDD2C7AEEEF373 (OnInspectorInitAttribute_t7564436864B4C2ABC648107C388558D26170EB85 * __this, String_t* ___action0, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___action0;
		__this->set_Action_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.OnStateUpdateAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnStateUpdateAttribute__ctor_mE21A3A0C3A41C2FA529B2E5D2EDE202F8CBDB303 (OnStateUpdateAttribute_tA9908491BF4CB898A9F306ADC408624E9B34A19A * __this, String_t* ___action0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___action0;
		__this->set_Action_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.OnValueChangedAttribute::get_MethodName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* OnValueChangedAttribute_get_MethodName_m2C99633B759FE258D9C44EBE80F4439961D1F3DE (OnValueChangedAttribute_t833863276762F1AA2950B2A58F560B381E73940C * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Action_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.OnValueChangedAttribute::set_MethodName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnValueChangedAttribute_set_MethodName_m4D68854AA62E2D4538912A5B1D94143C9F81D101 (OnValueChangedAttribute_t833863276762F1AA2950B2A58F560B381E73940C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_Action_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OnValueChangedAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnValueChangedAttribute__ctor_mC9217ED5F89B0A1BF293A5E1FF0201746C05AE77 (OnValueChangedAttribute_t833863276762F1AA2950B2A58F560B381E73940C * __this, String_t* ___action0, bool ___includeChildren1, const RuntimeMethod* method)
{
	{
		__this->set_InvokeOnUndoRedo_2((bool)1);
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___action0;
		__this->set_Action_0(L_0);
		bool L_1 = ___includeChildren1;
		__this->set_IncludeChildren_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Sirenix.OdinInspector.ObjectFieldAlignment Sirenix.OdinInspector.PreviewFieldAttribute::get_Alignment()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PreviewFieldAttribute_get_Alignment_mDBED00E78169E0B356F16A8759FF07D67F5974B5 (PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_alignment_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::set_Alignment(Sirenix.OdinInspector.ObjectFieldAlignment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreviewFieldAttribute_set_Alignment_mC639ECEAA0F283F44483506E821E205D611634BA (PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_alignment_0(L_0);
		__this->set_alignmentHasValue_1((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.PreviewFieldAttribute::get_AlignmentHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PreviewFieldAttribute_get_AlignmentHasValue_mE470BFDF84F8A6C264F3DC2A4414FF82B3939B92 (PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_alignmentHasValue_1();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreviewFieldAttribute__ctor_mF4E39532FED54856F331B95BB4BEA2F7022A6BF5 (PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		__this->set_Height_2((0.0f));
		return;
	}
}
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreviewFieldAttribute__ctor_m73A84428DFFD8A8F6AF80475138256C119CDB6E0 (PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96 * __this, float ___height0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		float L_0 = ___height0;
		__this->set_Height_2(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(System.Single,Sirenix.OdinInspector.ObjectFieldAlignment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreviewFieldAttribute__ctor_m9CB7BB82E1251D2398470AF788A2017DE9E98EFA (PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96 * __this, float ___height0, int32_t ___alignment1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		float L_0 = ___height0;
		__this->set_Height_2(L_0);
		int32_t L_1 = ___alignment1;
		PreviewFieldAttribute_set_Alignment_mC639ECEAA0F283F44483506E821E205D611634BA(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(Sirenix.OdinInspector.ObjectFieldAlignment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreviewFieldAttribute__ctor_m42F6284469DF8F7BB78B4DE69BF2BB32EB5FE534 (PreviewFieldAttribute_t0967322B752115C5CBD5E90839DA2C6066E7FA96 * __this, int32_t ___alignment0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___alignment0;
		PreviewFieldAttribute_set_Alignment_mC639ECEAA0F283F44483506E821E205D611634BA(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.ProgressBarAttribute::get_MinMember()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ProgressBarAttribute_get_MinMember_m2CC1ED194ED7A3009E79B1C07A0DE4A39E187ADD (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_MinGetter_2();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_MinMember(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_MinMember_m9A6C0D5E9AE335F63782BEC288D6E17C39178581 (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_MinGetter_2(L_0);
		return;
	}
}
// System.String Sirenix.OdinInspector.ProgressBarAttribute::get_MaxMember()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ProgressBarAttribute_get_MaxMember_m8B59F7BB73FF537EF9292E1C82690A779221C737 (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_MaxGetter_3();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_MaxMember(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_MaxMember_mA6E304FA078A043078F079FD1C05822AEE97D75E (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_MaxGetter_3(L_0);
		return;
	}
}
// System.String Sirenix.OdinInspector.ProgressBarAttribute::get_ColorMember()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ProgressBarAttribute_get_ColorMember_m8BBC82073E0759F54EDA83E001F685C1DDF685FC (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_ColorGetter_8();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ColorMember(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_ColorMember_m9359E4B9A3400F7553BFB3D00D56B4943B603266 (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_ColorGetter_8(L_0);
		return;
	}
}
// System.String Sirenix.OdinInspector.ProgressBarAttribute::get_BackgroundColorMember()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ProgressBarAttribute_get_BackgroundColorMember_m70AC774398545E0FB91CE18331214809C968AFBF (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_BackgroundColorGetter_9();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_BackgroundColorMember(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_BackgroundColorMember_mE873BB83D0F4A0918F2C3AF2494C153E094F5AF4 (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_BackgroundColorGetter_9(L_0);
		return;
	}
}
// System.String Sirenix.OdinInspector.ProgressBarAttribute::get_CustomValueStringMember()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ProgressBarAttribute_get_CustomValueStringMember_m355C89D38750B48F2088523A21E4A3602647F6DE (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_CustomValueStringGetter_11();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_CustomValueStringMember(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_CustomValueStringMember_mD3B3908B9FAA46A37B0A74137E96D01399AEB815 (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_CustomValueStringGetter_11(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.Double,System.Double,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute__ctor_m1AA280ED357596B2D727C77828E5E7135150CC2C (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, double ___min0, double ___max1, float ___r2, float ___g3, float ___b4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		__this->set_Min_0(L_0);
		double L_1 = ___max1;
		__this->set_Max_1(L_1);
		float L_2 = ___r2;
		__this->set_R_4(L_2);
		float L_3 = ___g3;
		__this->set_G_5(L_3);
		float L_4 = ___b4;
		__this->set_B_6(L_4);
		__this->set_Height_7(((int32_t)12));
		__this->set_Segmented_10((bool)0);
		__this->set_drawValueLabel_12((bool)1);
		ProgressBarAttribute_set_DrawValueLabelHasValue_mFE21A81AACD29E509534804C64D7092FC9EC971D_inline(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_valueLabelAlignment_13(1);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m2E49A4D0090B1D60036AB25BA0ABAA025D3F25C5_inline(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.String,System.Double,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute__ctor_m3EBD7FE0C0BE330BB9420358ACD8BF7309F450EB (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, String_t* ___minGetter0, double ___max1, float ___r2, float ___g3, float ___b4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minGetter0;
		__this->set_MinGetter_2(L_0);
		double L_1 = ___max1;
		__this->set_Max_1(L_1);
		float L_2 = ___r2;
		__this->set_R_4(L_2);
		float L_3 = ___g3;
		__this->set_G_5(L_3);
		float L_4 = ___b4;
		__this->set_B_6(L_4);
		__this->set_Height_7(((int32_t)12));
		__this->set_Segmented_10((bool)0);
		__this->set_drawValueLabel_12((bool)1);
		ProgressBarAttribute_set_DrawValueLabelHasValue_mFE21A81AACD29E509534804C64D7092FC9EC971D_inline(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_valueLabelAlignment_13(1);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m2E49A4D0090B1D60036AB25BA0ABAA025D3F25C5_inline(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.Double,System.String,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute__ctor_m05442A34E95B454A385A128774C4E57CBE80ACF4 (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, double ___min0, String_t* ___maxGetter1, float ___r2, float ___g3, float ___b4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		__this->set_Min_0(L_0);
		String_t* L_1 = ___maxGetter1;
		__this->set_MaxGetter_3(L_1);
		float L_2 = ___r2;
		__this->set_R_4(L_2);
		float L_3 = ___g3;
		__this->set_G_5(L_3);
		float L_4 = ___b4;
		__this->set_B_6(L_4);
		__this->set_Height_7(((int32_t)12));
		__this->set_Segmented_10((bool)0);
		__this->set_drawValueLabel_12((bool)1);
		ProgressBarAttribute_set_DrawValueLabelHasValue_mFE21A81AACD29E509534804C64D7092FC9EC971D_inline(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_valueLabelAlignment_13(1);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m2E49A4D0090B1D60036AB25BA0ABAA025D3F25C5_inline(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.String,System.String,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute__ctor_m4247618AFE4481DE37C0DFAD7801E8F26BA69B99 (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, String_t* ___minGetter0, String_t* ___maxGetter1, float ___r2, float ___g3, float ___b4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minGetter0;
		__this->set_MinGetter_2(L_0);
		String_t* L_1 = ___maxGetter1;
		__this->set_MaxGetter_3(L_1);
		float L_2 = ___r2;
		__this->set_R_4(L_2);
		float L_3 = ___g3;
		__this->set_G_5(L_3);
		float L_4 = ___b4;
		__this->set_B_6(L_4);
		__this->set_Height_7(((int32_t)12));
		__this->set_Segmented_10((bool)0);
		__this->set_drawValueLabel_12((bool)1);
		ProgressBarAttribute_set_DrawValueLabelHasValue_mFE21A81AACD29E509534804C64D7092FC9EC971D_inline(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_valueLabelAlignment_13(1);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m2E49A4D0090B1D60036AB25BA0ABAA025D3F25C5_inline(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_DrawValueLabel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ProgressBarAttribute_get_DrawValueLabel_mC0D88C51C77D902A740C75949A6FA5A8553FBFFB (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_drawValueLabel_12();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabel(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_DrawValueLabel_mC55B7F41936E19227A24A8F109FCAC95BB9A068E (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_drawValueLabel_12(L_0);
		ProgressBarAttribute_set_DrawValueLabelHasValue_mFE21A81AACD29E509534804C64D7092FC9EC971D_inline(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_DrawValueLabelHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ProgressBarAttribute_get_DrawValueLabelHasValue_m02DBBB3955C49F325C00C0F297DAB10A808AEE15 (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CDrawValueLabelHasValueU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabelHasValue(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_DrawValueLabelHasValue_mFE21A81AACD29E509534804C64D7092FC9EC971D (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CDrawValueLabelHasValueU3Ek__BackingField_14(L_0);
		return;
	}
}
// UnityEngine.TextAlignment Sirenix.OdinInspector.ProgressBarAttribute::get_ValueLabelAlignment()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ProgressBarAttribute_get_ValueLabelAlignment_mF5ECF715BE3EE10695716E7CB25DBBC2E16E7B0A (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_valueLabelAlignment_13();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignment(UnityEngine.TextAlignment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_ValueLabelAlignment_m35FB4443DEB147A679B29DD278AFBFD3D8244A2B (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_valueLabelAlignment_13(L_0);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m2E49A4D0090B1D60036AB25BA0ABAA025D3F25C5_inline(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_ValueLabelAlignmentHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ProgressBarAttribute_get_ValueLabelAlignmentHasValue_mFD655331D63227292454EB3995BB9B527D642893 (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignmentHasValue(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m2E49A4D0090B1D60036AB25BA0ABAA025D3F25C5 (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15(L_0);
		return;
	}
}
// UnityEngine.Color Sirenix.OdinInspector.ProgressBarAttribute::get_Color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ProgressBarAttribute_get_Color_mD07AE72BEAEFE6EA3175131A3BCF78C4A90516DB (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_R_4();
		float L_1 = __this->get_G_5();
		float L_2 = __this->get_B_6();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C((&L_3), L_0, L_1, L_2, (1.0f), /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A (PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * __this, String_t* ___groupId0, float ___order1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * G_B2_0 = NULL;
	PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * G_B1_0 = NULL;
	PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * G_B3_0 = NULL;
	String_t* G_B4_0 = NULL;
	PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * G_B4_1 = NULL;
	{
		__this->set_HideWhenChildrenAreInvisible_3((bool)1);
		__this->set_AnimateVisibility_5((bool)1);
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___groupId0;
		__this->set_GroupID_0(L_0);
		float L_1 = ___order1;
		__this->set_Order_2(L_1);
		String_t* L_2 = ___groupId0;
		NullCheck(L_2);
		int32_t L_3 = String_LastIndexOf_m76C37E3915E802044761572007B8FB0635995F59(L_2, ((int32_t)47), /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		G_B1_0 = __this;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			G_B2_0 = __this;
			goto IL_0039;
		}
	}
	{
		int32_t L_5 = V_0;
		String_t* L_6 = ___groupId0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline(L_6, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			G_B3_0 = G_B1_0;
			goto IL_003c;
		}
	}

IL_0039:
	{
		String_t* L_8 = ___groupId0;
		G_B4_0 = L_8;
		G_B4_1 = G_B2_0;
		goto IL_0045;
	}

IL_003c:
	{
		String_t* L_9 = ___groupId0;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m2C4AFF5E79DD8BADFD2DFBCF156BF728FBB8E1AE(L_9, ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)), /*hidden argument*/NULL);
		G_B4_0 = L_11;
		G_B4_1 = G_B3_0;
	}

IL_0045:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_GroupName_1(G_B4_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyGroupAttribute__ctor_m8F7EF143FEDD12FAC7B22FBAC7BCB4885A09DF88 (PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * __this, String_t* ___groupId0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupId0;
		PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A(__this, L_0, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// Sirenix.OdinInspector.PropertyGroupAttribute Sirenix.OdinInspector.PropertyGroupAttribute::Combine(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * PropertyGroupAttribute_Combine_mE33D3E9250CECE2DE657D70D5B9041720362481D (PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyGroupAttribute_Combine_mE33D3E9250CECE2DE657D70D5B9041720362481D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, _stringLiteralD0941E68DA8F38151FF86A61FC59F7C5CF9FCAA2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, PropertyGroupAttribute_Combine_mE33D3E9250CECE2DE657D70D5B9041720362481D_RuntimeMethod_var);
	}

IL_000e:
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_2 = ___other0;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60(L_2, /*hidden argument*/NULL);
		Type_t * L_4 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60(__this, /*hidden argument*/NULL);
		if ((((RuntimeObject*)(Type_t *)L_3) == ((RuntimeObject*)(Type_t *)L_4)))
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_5 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_5, _stringLiteral05FC6F56975CF45104FFBC8B64CA1534C38AD0F9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, PropertyGroupAttribute_Combine_mE33D3E9250CECE2DE657D70D5B9041720362481D_RuntimeMethod_var);
	}

IL_0027:
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_6 = ___other0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_GroupID_0();
		String_t* L_8 = __this->get_GroupID_0();
		bool L_9 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_10 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_10, _stringLiteral7BC503D8B9F755E76AE4D6FB13E223FBA124E957, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10, PropertyGroupAttribute_Combine_mE33D3E9250CECE2DE657D70D5B9041720362481D_RuntimeMethod_var);
	}

IL_0045:
	{
		float L_11 = __this->get_Order_2();
		if ((!(((float)L_11) == ((float)(0.0f)))))
		{
			goto IL_0060;
		}
	}
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_12 = ___other0;
		NullCheck(L_12);
		float L_13 = L_12->get_Order_2();
		__this->set_Order_2(L_13);
		goto IL_0084;
	}

IL_0060:
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_14 = ___other0;
		NullCheck(L_14);
		float L_15 = L_14->get_Order_2();
		if ((((float)L_15) == ((float)(0.0f))))
		{
			goto IL_0084;
		}
	}
	{
		float L_16 = __this->get_Order_2();
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_17 = ___other0;
		NullCheck(L_17);
		float L_18 = L_17->get_Order_2();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var);
		float L_19 = Math_Min_mB77A95F6A1FE3B8E7CD784F20AF4E1C908B5E9CD(L_16, L_18, /*hidden argument*/NULL);
		__this->set_Order_2(L_19);
	}

IL_0084:
	{
		bool L_20 = __this->get_HideWhenChildrenAreInvisible_3();
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_21 = ___other0;
		NullCheck(L_21);
		bool L_22 = L_21->get_HideWhenChildrenAreInvisible_3();
		__this->set_HideWhenChildrenAreInvisible_3((bool)((int32_t)((int32_t)L_20&(int32_t)L_22)));
		String_t* L_23 = __this->get_VisibleIf_4();
		if (L_23)
		{
			goto IL_00ab;
		}
	}
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_24 = ___other0;
		NullCheck(L_24);
		String_t* L_25 = L_24->get_VisibleIf_4();
		__this->set_VisibleIf_4(L_25);
	}

IL_00ab:
	{
		bool L_26 = __this->get_AnimateVisibility_5();
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_27 = ___other0;
		NullCheck(L_27);
		bool L_28 = L_27->get_AnimateVisibility_5();
		__this->set_AnimateVisibility_5((bool)((int32_t)((int32_t)L_26&(int32_t)L_28)));
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_29 = ___other0;
		VirtActionInvoker1< PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * >::Invoke(7 /* System.Void Sirenix.OdinInspector.PropertyGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute) */, __this, L_29);
		return __this;
	}
}
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyGroupAttribute_CombineValuesWith_mA9D4370FD93B94EE22C250251D79E7C9ADD9E4D9 (PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertyOrderAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyOrderAttribute__ctor_m0258452EBB74A30BCC712DBB88AA125607B703EA (PropertyOrderAttribute_t142EA22A39E39E022D9DBB6BDFD6649CAD914996 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyOrderAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyOrderAttribute__ctor_mF84FACE1F0E9C501B91344E5D8ED99C41A49732D (PropertyOrderAttribute_t142EA22A39E39E022D9DBB6BDFD6649CAD914996 * __this, float ___order0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		float L_0 = ___order0;
		__this->set_Order_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.PropertyRangeAttribute::get_MinMember()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PropertyRangeAttribute_get_MinMember_m1AE82B2EF2CF8ED9A2180464F63085D1C92ACC32 (PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_MinGetter_2();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::set_MinMember(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyRangeAttribute_set_MinMember_mF0665322F7B5EB30944C1468DE272546D38B1BE7 (PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_MinGetter_2(L_0);
		return;
	}
}
// System.String Sirenix.OdinInspector.PropertyRangeAttribute::get_MaxMember()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PropertyRangeAttribute_get_MaxMember_mBAEEA015901D53E5100A7ACC72DF6A66C3845CC4 (PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_MaxGetter_3();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::set_MaxMember(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyRangeAttribute_set_MaxMember_m91ED22ABEFFEC257A3E8F2AB7EAF91EDA4E0ADBF (PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_MaxGetter_3(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyRangeAttribute__ctor_m46E3E82518F3BD0221513B22406BE53F83109311 (PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * __this, double ___min0, double ___max1, const RuntimeMethod* method)
{
	PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * G_B2_0 = NULL;
	PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * G_B1_0 = NULL;
	double G_B3_0 = 0.0;
	PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * G_B3_1 = NULL;
	PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * G_B5_0 = NULL;
	PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * G_B4_0 = NULL;
	double G_B6_0 = 0.0;
	PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * G_B6_1 = NULL;
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		double L_1 = ___max1;
		G_B1_0 = __this;
		if ((((double)L_0) < ((double)L_1)))
		{
			G_B2_0 = __this;
			goto IL_000e;
		}
	}
	{
		double L_2 = ___max1;
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_000f;
	}

IL_000e:
	{
		double L_3 = ___min0;
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_000f:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_Min_0(G_B3_0);
		double L_4 = ___max1;
		double L_5 = ___min0;
		G_B4_0 = __this;
		if ((((double)L_4) > ((double)L_5)))
		{
			G_B5_0 = __this;
			goto IL_001c;
		}
	}
	{
		double L_6 = ___min0;
		G_B6_0 = L_6;
		G_B6_1 = G_B4_0;
		goto IL_001d;
	}

IL_001c:
	{
		double L_7 = ___max1;
		G_B6_0 = L_7;
		G_B6_1 = G_B5_0;
	}

IL_001d:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_Max_1(G_B6_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.String,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyRangeAttribute__ctor_m69B10A0AF618ADABAE4384D7EF88D30E339AE6DF (PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * __this, String_t* ___minGetter0, double ___max1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minGetter0;
		__this->set_MinGetter_2(L_0);
		double L_1 = ___max1;
		__this->set_Max_1(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.Double,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyRangeAttribute__ctor_m0F5C052C678C9F88EC421C180711890168F82E4B (PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * __this, double ___min0, String_t* ___maxGetter1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		__this->set_Min_0(L_0);
		String_t* L_1 = ___maxGetter1;
		__this->set_MaxGetter_3(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyRangeAttribute__ctor_mB000DC0552D3FADF73419B765DCAFE5B7ED660C6 (PropertyRangeAttribute_tDD0264DCBEB4689FE07E7777AA9D6F82796F682F * __this, String_t* ___minGetter0, String_t* ___maxGetter1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minGetter0;
		__this->set_MinGetter_2(L_0);
		String_t* L_1 = ___maxGetter1;
		__this->set_MaxGetter_3(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertySpaceAttribute__ctor_m776D7EF54F7BA3A1C86CD2BBBFFA9A466CA4B11F (PropertySpaceAttribute_tB1663AC8EFAC09BE967F6C9EA8A4E04FA39B9A47 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		__this->set_SpaceBefore_0((8.0f));
		__this->set_SpaceAfter_1((0.0f));
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertySpaceAttribute__ctor_m767AFE53798D1B36405151C25F85EA97292F33C9 (PropertySpaceAttribute_tB1663AC8EFAC09BE967F6C9EA8A4E04FA39B9A47 * __this, float ___spaceBefore0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		float L_0 = ___spaceBefore0;
		__this->set_SpaceBefore_0(L_0);
		__this->set_SpaceAfter_1((0.0f));
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertySpaceAttribute__ctor_m84DFA94A5E36256F8A3A7D88195F2056226A5B23 (PropertySpaceAttribute_tB1663AC8EFAC09BE967F6C9EA8A4E04FA39B9A47 * __this, float ___spaceBefore0, float ___spaceAfter1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		float L_0 = ___spaceBefore0;
		__this->set_SpaceBefore_0(L_0);
		float L_1 = ___spaceAfter1;
		__this->set_SpaceAfter_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertyTooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyTooltipAttribute__ctor_m61F2100A096BFDDBE09DE5319FFC27D4E5660EC5 (PropertyTooltipAttribute_tB379999F5A87587FF4F62D7F604CC275FCB76DB5 * __this, String_t* ___tooltip0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tooltip0;
		__this->set_Tooltip_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyAttribute__ctor_m18A85A0BB01A704BC63961346547A03264778033 (ReadOnlyAttribute_t993E550B14FD6CBF965160564F37009B72C9F8D4 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequiredAttribute__ctor_mD0472E44DB48D0FAEA21E3727FB7D3C97278F14D (RequiredAttribute_t233038081C694B76DACAFC3BA8624900426D42B0 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		__this->set_MessageType_1(3);
		return;
	}
}
// System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(System.String,Sirenix.OdinInspector.InfoMessageType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequiredAttribute__ctor_m7C9A7315A7C85A1389C15A35348A1C6A02F1E818 (RequiredAttribute_t233038081C694B76DACAFC3BA8624900426D42B0 * __this, String_t* ___errorMessage0, int32_t ___messageType1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___errorMessage0;
		__this->set_ErrorMessage_0(L_0);
		int32_t L_1 = ___messageType1;
		__this->set_MessageType_1(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequiredAttribute__ctor_m356BF7D4C0E2F20CE57E27C5514E94C28C8A149E (RequiredAttribute_t233038081C694B76DACAFC3BA8624900426D42B0 * __this, String_t* ___errorMessage0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___errorMessage0;
		__this->set_ErrorMessage_0(L_0);
		__this->set_MessageType_1(3);
		return;
	}
}
// System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(Sirenix.OdinInspector.InfoMessageType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequiredAttribute__ctor_mFED9DD0CBA9487878469BF33C8DF7D0A219B3CB2 (RequiredAttribute_t233038081C694B76DACAFC3BA8624900426D42B0 * __this, int32_t ___messageType0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___messageType0;
		__this->set_MessageType_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResponsiveButtonGroupAttribute__ctor_m21EF1D295EFCBD00BBF2E6EA35FC10C0786AE86F (ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848 * __this, String_t* ___group0, const RuntimeMethod* method)
{
	{
		__this->set_DefaultButtonSize_6(((int32_t)22));
		String_t* L_0 = ___group0;
		PropertyGroupAttribute__ctor_m8F7EF143FEDD12FAC7B22FBAC7BCB4885A09DF88(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResponsiveButtonGroupAttribute_CombineValuesWith_m0C46A0FFC4BF1DC38EA47EC0D6E68DAB2FD3EDA4 (ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848 * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ResponsiveButtonGroupAttribute_CombineValuesWith_m0C46A0FFC4BF1DC38EA47EC0D6E68DAB2FD3EDA4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848 * V_0 = NULL;
	ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848 * G_B8_0 = NULL;
	ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848 * G_B7_0 = NULL;
	int32_t G_B9_0 = 0;
	ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848 * G_B9_1 = NULL;
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_0 = ___other0;
		V_0 = ((ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848 *)IsInstClass((RuntimeObject*)L_0, ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848_il2cpp_TypeInfo_var));
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_1 = ___other0;
		if (L_1)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_DefaultButtonSize_6();
		if ((((int32_t)L_3) == ((int32_t)((int32_t)22))))
		{
			goto IL_0023;
		}
	}
	{
		ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_DefaultButtonSize_6();
		__this->set_DefaultButtonSize_6(L_5);
		goto IL_0039;
	}

IL_0023:
	{
		int32_t L_6 = __this->get_DefaultButtonSize_6();
		if ((((int32_t)L_6) == ((int32_t)((int32_t)22))))
		{
			goto IL_0039;
		}
	}
	{
		ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848 * L_7 = V_0;
		int32_t L_8 = __this->get_DefaultButtonSize_6();
		NullCheck(L_7);
		L_7->set_DefaultButtonSize_6(L_8);
	}

IL_0039:
	{
		bool L_9 = __this->get_UniformLayout_7();
		G_B7_0 = __this;
		if (L_9)
		{
			G_B8_0 = __this;
			goto IL_004a;
		}
	}
	{
		ResponsiveButtonGroupAttribute_t775BA3BC4B2F7817EB9EFE5496C8B5D5CCCFC848 * L_10 = V_0;
		NullCheck(L_10);
		bool L_11 = L_10->get_UniformLayout_7();
		G_B9_0 = ((int32_t)(L_11));
		G_B9_1 = G_B7_0;
		goto IL_004b;
	}

IL_004a:
	{
		G_B9_0 = 1;
		G_B9_1 = G_B8_0;
	}

IL_004b:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_UniformLayout_7((bool)G_B9_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.SceneObjectsOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneObjectsOnlyAttribute__ctor_mAF5D986331A81C1724B79E23ADEE745EB7EFD9E5 (SceneObjectsOnlyAttribute_tE48AE7C0CE377268490681FAD4EC7108D742DB19 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.SearchableAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SearchableAttribute__ctor_m74A25AAA65FD0FB6893A616BCE3067BFF900AE5C (SearchableAttribute_t36B1B00CA28BBBC11E1E354D4A0F50BAD302BB5A * __this, const RuntimeMethod* method)
{
	{
		__this->set_FuzzySearch_0((bool)1);
		__this->set_FilterOptions_1((-1));
		__this->set_Recursive_2((bool)1);
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowDrawerChainAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowDrawerChainAttribute__ctor_mC28FA3B72111F46DFA3C395F1A4DD86EC43F187F (ShowDrawerChainAttribute_tBF48731E167BC72578A8E8FEC742BAB51E993694 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowForPrefabOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowForPrefabOnlyAttribute__ctor_m243ED5ABA1FDC9E3FED7650C390750C02C75518A (ShowForPrefabOnlyAttribute_t52839F490AF48EB0AD275E18D1F2A81392FC4BC5 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.ShowIfAttribute::get_MemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ShowIfAttribute_get_MemberName_mDF9FD77EE8556F9FC0BE36A70DE6304D8E5A4F29 (ShowIfAttribute_tBC21F2EA58071ED9C4DBA0E5AA6967FF19C543C2 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Condition_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfAttribute::set_MemberName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfAttribute_set_MemberName_mB198C1AA69C874A57B784B11303A4B059C9A981F (ShowIfAttribute_tBC21F2EA58071ED9C4DBA0E5AA6967FF19C543C2 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_Condition_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfAttribute__ctor_m6B88C445A0D6989BFFFF6826FA8EF58611EF84DF (ShowIfAttribute_tBC21F2EA58071ED9C4DBA0E5AA6967FF19C543C2 * __this, String_t* ___condition0, bool ___animate1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___condition0;
		__this->set_Condition_0(L_0);
		bool L_1 = ___animate1;
		__this->set_Animate_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfAttribute::.ctor(System.String,System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfAttribute__ctor_m2939A8F0ED2730D238E2CCE4844FE0B697B3F1D6 (ShowIfAttribute_tBC21F2EA58071ED9C4DBA0E5AA6967FF19C543C2 * __this, String_t* ___condition0, RuntimeObject * ___optionalValue1, bool ___animate2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___condition0;
		__this->set_Condition_0(L_0);
		RuntimeObject * L_1 = ___optionalValue1;
		__this->set_Value_1(L_1);
		bool L_2 = ___animate2;
		__this->set_Animate_2(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.ShowIfGroupAttribute::get_Animate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ShowIfGroupAttribute_get_Animate_mA28D4E294DB07B30D7F199DB20F66A8132674DDC (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = ((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->get_AnimateVisibility_5();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::set_Animate(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfGroupAttribute_set_Animate_m0C5232E32FBF5570CA9EBF341F4A9A26383C87CD (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->set_AnimateVisibility_5(L_0);
		return;
	}
}
// System.String Sirenix.OdinInspector.ShowIfGroupAttribute::get_MemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ShowIfGroupAttribute_get_MemberName_m2EA81E787AF8407FFA0C215D4780EC4FDB75B322 (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ShowIfGroupAttribute_get_Condition_mBAA18A45D8F96BC8D5E46B3C141673EF52A50E46(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::set_MemberName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfGroupAttribute_set_MemberName_m4FFBB055EEEBA05AE6CF9183D34041B7EEB62A32 (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		ShowIfGroupAttribute_set_Condition_m7CAF3CF1D9803D698E9011B30EC9EA5536EB8FA9_inline(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String Sirenix.OdinInspector.ShowIfGroupAttribute::get_Condition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ShowIfGroupAttribute_get_Condition_mBAA18A45D8F96BC8D5E46B3C141673EF52A50E46 (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->get_VisibleIf_4();
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_2 = ((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->get_VisibleIf_4();
		return L_2;
	}

IL_0014:
	{
		String_t* L_3 = ((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->get_GroupName_1();
		return L_3;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::set_Condition(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfGroupAttribute_set_Condition_m7CAF3CF1D9803D698E9011B30EC9EA5536EB8FA9 (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->set_VisibleIf_4(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfGroupAttribute__ctor_mD113EDB716F28754313E2BEB0927B6043981329D (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, String_t* ___path0, bool ___animate1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___path0;
		PropertyGroupAttribute__ctor_m8F7EF143FEDD12FAC7B22FBAC7BCB4885A09DF88(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___animate1;
		ShowIfGroupAttribute_set_Animate_m0C5232E32FBF5570CA9EBF341F4A9A26383C87CD_inline(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::.ctor(System.String,System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfGroupAttribute__ctor_mF398B06CE4C1F42A320290017D7296BAAEA7A312 (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, String_t* ___path0, RuntimeObject * ___value1, bool ___animate2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___path0;
		PropertyGroupAttribute__ctor_m8F7EF143FEDD12FAC7B22FBAC7BCB4885A09DF88(__this, L_0, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___value1;
		__this->set_Value_6(L_1);
		bool L_2 = ___animate2;
		ShowIfGroupAttribute_set_Animate_m0C5232E32FBF5570CA9EBF341F4A9A26383C87CD_inline(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfGroupAttribute_CombineValuesWith_m4D6EAA8F018088F641E92D46A9DD2919BF79CDE0 (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShowIfGroupAttribute_CombineValuesWith_m4D6EAA8F018088F641E92D46A9DD2919BF79CDE0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * V_0 = NULL;
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_0 = ___other0;
		V_0 = ((ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 *)IsInstClass((RuntimeObject*)L_0, ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549_il2cpp_TypeInfo_var));
		RuntimeObject * L_1 = __this->get_Value_6();
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * L_2 = V_0;
		RuntimeObject * L_3 = __this->get_Value_6();
		NullCheck(L_2);
		L_2->set_Value_6(L_3);
	}

IL_001b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowInInlineEditorsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowInInlineEditorsAttribute__ctor_m7A762533C1DDCCEB86F424F6FD7CAD4F2CDACF1B (ShowInInlineEditorsAttribute_t51DF69747A3CE1570890953F2B8536E8082867CD * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowInInspectorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowInInspectorAttribute__ctor_m015E71A39B23797867E22A381E635E29569B65C7 (ShowInInspectorAttribute_t035C9790B6F7942286B0D6607580AB3393B1115B * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m473F5CD90829FF1ADF2BB786906943CB8B80A453 (ShowOdinSerializedPropertiesInInspectorAttribute_t85D03D92F6E429EE8CA403011C588CC9AD651DFE * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowPropertyResolverAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowPropertyResolverAttribute__ctor_mABC8EECEE1320586632257A6237B96F8CF857F4B (ShowPropertyResolverAttribute_t3C66CA8BD48FC58876E4DC2633C0D4B5B660C633 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.SuffixLabelAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuffixLabelAttribute__ctor_mABCF82E4DA146381809AAD4677A35A363CEDF858 (SuffixLabelAttribute_t7B942FEB352DA81BAE4FE6C86A26838563BE3942 * __this, String_t* ___label0, bool ___overlay1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___label0;
		__this->set_Label_0(L_0);
		bool L_1 = ___overlay1;
		__this->set_Overlay_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.SuppressInvalidAttributeErrorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuppressInvalidAttributeErrorAttribute__ctor_mFC93640117AD105D43241199CFF23B41EDAE3545 (SuppressInvalidAttributeErrorAttribute_tD041EA2EC15B10F0A0E7B48E887B652EE6C6C427 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabGroupAttribute__ctor_m44DBA7BC5910270727E85756FB81AEDA69E277FE (TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * __this, String_t* ___tab0, bool ___useFixedHeight1, float ___order2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabGroupAttribute__ctor_m44DBA7BC5910270727E85756FB81AEDA69E277FE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___tab0;
		bool L_1 = ___useFixedHeight1;
		float L_2 = ___order2;
		TabGroupAttribute__ctor_m3FDCD07DF133DF94BC3015B62269140288334C53(__this, _stringLiteralA61CECD51CDCE38AA6025799993B421942C72EC7, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.String,System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabGroupAttribute__ctor_m3FDCD07DF133DF94BC3015B62269140288334C53 (TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * __this, String_t* ___group0, String_t* ___tab1, bool ___useFixedHeight2, float ___order3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabGroupAttribute__ctor_m3FDCD07DF133DF94BC3015B62269140288334C53_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___group0;
		float L_1 = ___order3;
		PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___tab1;
		__this->set_TabName_7(L_2);
		bool L_3 = ___useFixedHeight2;
		__this->set_UseFixedHeight_8(L_3);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_4 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_4, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		TabGroupAttribute_set_Tabs_m40A73E4EC6877859058F36C0D4CEA49C3F4ED537_inline(__this, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___tab1;
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_6 = TabGroupAttribute_get_Tabs_mE3395C3B6ACF15D12A58430AA25626B80AD24A4D_inline(__this, /*hidden argument*/NULL);
		String_t* L_7 = ___tab1;
		NullCheck(L_6);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_6, L_7, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
	}

IL_0031:
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_8 = TabGroupAttribute_get_Tabs_mE3395C3B6ACF15D12A58430AA25626B80AD24A4D_inline(__this, /*hidden argument*/NULL);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_9 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mE9FDDA3E872C3CB2DBDC8562E9ABA76CA3124599(L_9, L_8, /*hidden argument*/List_1__ctor_mE9FDDA3E872C3CB2DBDC8562E9ABA76CA3124599_RuntimeMethod_var);
		TabGroupAttribute_set_Tabs_m40A73E4EC6877859058F36C0D4CEA49C3F4ED537_inline(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Sirenix.OdinInspector.TabGroupAttribute::get_Tabs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * TabGroupAttribute_get_Tabs_mE3395C3B6ACF15D12A58430AA25626B80AD24A4D (TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * __this, const RuntimeMethod* method)
{
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = __this->get_U3CTabsU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.TabGroupAttribute::set_Tabs(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabGroupAttribute_set_Tabs_m40A73E4EC6877859058F36C0D4CEA49C3F4ED537 (TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * __this, List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = ___value0;
		__this->set_U3CTabsU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TabGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabGroupAttribute_CombineValuesWith_m03FB48456237C348D28C3AC330774077BC4AA84E (TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabGroupAttribute_CombineValuesWith_m03FB48456237C348D28C3AC330774077BC4AA84E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * V_0 = NULL;
	TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * G_B3_0 = NULL;
	TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * G_B4_1 = NULL;
	TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * G_B6_0 = NULL;
	TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * G_B7_1 = NULL;
	TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * G_B9_0 = NULL;
	TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * G_B8_0 = NULL;
	int32_t G_B10_0 = 0;
	TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * G_B10_1 = NULL;
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_0 = ___other0;
		PropertyGroupAttribute_CombineValuesWith_mA9D4370FD93B94EE22C250251D79E7C9ADD9E4D9(__this, L_0, /*hidden argument*/NULL);
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_1 = ___other0;
		V_0 = ((TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED *)IsInstClass((RuntimeObject*)L_1, TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED_il2cpp_TypeInfo_var));
		TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_TabName_7();
		if (!L_3)
		{
			goto IL_007f;
		}
	}
	{
		bool L_4 = __this->get_UseFixedHeight_8();
		G_B2_0 = __this;
		if (L_4)
		{
			G_B3_0 = __this;
			goto IL_0027;
		}
	}
	{
		TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = L_5->get_UseFixedHeight_8();
		G_B4_0 = ((int32_t)(L_6));
		G_B4_1 = G_B2_0;
		goto IL_0028;
	}

IL_0027:
	{
		G_B4_0 = 1;
		G_B4_1 = G_B3_0;
	}

IL_0028:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_UseFixedHeight_8((bool)G_B4_0);
		bool L_7 = __this->get_Paddingless_9();
		G_B5_0 = __this;
		if (L_7)
		{
			G_B6_0 = __this;
			goto IL_003e;
		}
	}
	{
		TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = L_8->get_Paddingless_9();
		G_B7_0 = ((int32_t)(L_9));
		G_B7_1 = G_B5_0;
		goto IL_003f;
	}

IL_003e:
	{
		G_B7_0 = 1;
		G_B7_1 = G_B6_0;
	}

IL_003f:
	{
		NullCheck(G_B7_1);
		G_B7_1->set_Paddingless_9((bool)G_B7_0);
		bool L_10 = __this->get_HideTabGroupIfTabGroupOnlyHasOneTab_10();
		G_B8_0 = __this;
		if (L_10)
		{
			G_B9_0 = __this;
			goto IL_0055;
		}
	}
	{
		TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = L_11->get_HideTabGroupIfTabGroupOnlyHasOneTab_10();
		G_B10_0 = ((int32_t)(L_12));
		G_B10_1 = G_B8_0;
		goto IL_0056;
	}

IL_0055:
	{
		G_B10_0 = 1;
		G_B10_1 = G_B9_0;
	}

IL_0056:
	{
		NullCheck(G_B10_1);
		G_B10_1->set_HideTabGroupIfTabGroupOnlyHasOneTab_10((bool)G_B10_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_13 = TabGroupAttribute_get_Tabs_mE3395C3B6ACF15D12A58430AA25626B80AD24A4D_inline(__this, /*hidden argument*/NULL);
		TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = L_14->get_TabName_7();
		NullCheck(L_13);
		bool L_16 = List_1_Contains_mC1D01A0D94C03E4225EEF9D6506D7D91C6976B7B(L_13, L_15, /*hidden argument*/List_1_Contains_mC1D01A0D94C03E4225EEF9D6506D7D91C6976B7B_RuntimeMethod_var);
		if (L_16)
		{
			goto IL_007f;
		}
	}
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_17 = TabGroupAttribute_get_Tabs_mE3395C3B6ACF15D12A58430AA25626B80AD24A4D_inline(__this, /*hidden argument*/NULL);
		TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = L_18->get_TabName_7();
		NullCheck(L_17);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_17, L_19, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
	}

IL_007f:
	{
		return;
	}
}
// System.Collections.Generic.IList`1<Sirenix.OdinInspector.PropertyGroupAttribute> Sirenix.OdinInspector.TabGroupAttribute::Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute.GetSubGroupAttributes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_GetSubGroupAttributes_m86FAA7D41551FA38170739A1C4375482F5F0EAF0 (TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_GetSubGroupAttributes_m86FAA7D41551FA38170739A1C4375482F5F0EAF0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	List_1_tEA3588639D49099355676AB395F7428E02EFB383 * V_1 = NULL;
	Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819  V_2;
	memset((&V_2), 0, sizeof(V_2));
	String_t* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		V_0 = 0;
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = TabGroupAttribute_get_Tabs_mE3395C3B6ACF15D12A58430AA25626B80AD24A4D_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m4151A68BD4CB1D737213E7595F574987F8C812B4_inline(L_0, /*hidden argument*/List_1_get_Count_m4151A68BD4CB1D737213E7595F574987F8C812B4_RuntimeMethod_var);
		List_1_tEA3588639D49099355676AB395F7428E02EFB383 * L_2 = (List_1_tEA3588639D49099355676AB395F7428E02EFB383 *)il2cpp_codegen_object_new(List_1_tEA3588639D49099355676AB395F7428E02EFB383_il2cpp_TypeInfo_var);
		List_1__ctor_m32270ECBE19DDAA80F0E83559F63FDAD74A5C328(L_2, L_1, /*hidden argument*/List_1__ctor_m32270ECBE19DDAA80F0E83559F63FDAD74A5C328_RuntimeMethod_var);
		V_1 = L_2;
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_3 = TabGroupAttribute_get_Tabs_mE3395C3B6ACF15D12A58430AA25626B80AD24A4D_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819  L_4 = List_1_GetEnumerator_m79AA93F4A95A51200A8FC7433950476FD84D83B7(L_3, /*hidden argument*/List_1_GetEnumerator_m79AA93F4A95A51200A8FC7433950476FD84D83B7_RuntimeMethod_var);
		V_2 = L_4;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004b;
		}

IL_0021:
		{
			String_t* L_5 = Enumerator_get_Current_m73A2CF304FA1FEDE0FB84D5A90287D398804BCFE_inline((Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m73A2CF304FA1FEDE0FB84D5A90287D398804BCFE_RuntimeMethod_var);
			V_3 = L_5;
			List_1_tEA3588639D49099355676AB395F7428E02EFB383 * L_6 = V_1;
			String_t* L_7 = ((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->get_GroupID_0();
			String_t* L_8 = V_3;
			String_t* L_9 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_7, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, L_8, /*hidden argument*/NULL);
			int32_t L_10 = V_0;
			int32_t L_11 = L_10;
			V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
			TabSubGroupAttribute_t163D9E025CE046202E35327D9858BA138474EED6 * L_12 = (TabSubGroupAttribute_t163D9E025CE046202E35327D9858BA138474EED6 *)il2cpp_codegen_object_new(TabSubGroupAttribute_t163D9E025CE046202E35327D9858BA138474EED6_il2cpp_TypeInfo_var);
			TabSubGroupAttribute__ctor_m44ED5201B8DDB858B71AC573473AD2D269FEA381(L_12, L_9, (((float)((float)L_11))), /*hidden argument*/NULL);
			NullCheck(L_6);
			List_1_Add_m22BEC6E130081220F517BD7875962489A6478DD7(L_6, L_12, /*hidden argument*/List_1_Add_m22BEC6E130081220F517BD7875962489A6478DD7_RuntimeMethod_var);
		}

IL_004b:
		{
			bool L_13 = Enumerator_MoveNext_m11D1A253155BD0CBF9B0E59ECEA4F7EEBF86C09F((Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m11D1A253155BD0CBF9B0E59ECEA4F7EEBF86C09F_RuntimeMethod_var);
			if (L_13)
			{
				goto IL_0021;
			}
		}

IL_0054:
		{
			IL2CPP_LEAVE(0x64, FINALLY_0056);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0056;
	}

FINALLY_0056:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m6CD2F1158D4A0D24923FBB9479F1A0E987B98F2F((Enumerator_tE2103ED6CFF28595C5618C804ECBBF4C35135819 *)(&V_2), /*hidden argument*/Enumerator_Dispose_m6CD2F1158D4A0D24923FBB9479F1A0E987B98F2F_RuntimeMethod_var);
		IL2CPP_END_FINALLY(86)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(86)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x64, IL_0064)
	}

IL_0064:
	{
		List_1_tEA3588639D49099355676AB395F7428E02EFB383 * L_14 = V_1;
		return L_14;
	}
}
// System.String Sirenix.OdinInspector.TabGroupAttribute::Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute.RepathMemberAttribute(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_RepathMemberAttribute_m85A61D8AD679C3E6A58CAED8A389F21E92B0CE1F (TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___attr0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_RepathMemberAttribute_m85A61D8AD679C3E6A58CAED8A389F21E92B0CE1F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * V_0 = NULL;
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_0 = ___attr0;
		V_0 = ((TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED *)CastclassClass((RuntimeObject*)L_0, TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED_il2cpp_TypeInfo_var));
		String_t* L_1 = ((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->get_GroupID_0();
		TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_TabName_7();
		String_t* L_4 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_1, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute::.ctor(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabSubGroupAttribute__ctor_m44ED5201B8DDB858B71AC573473AD2D269FEA381 (TabSubGroupAttribute_t163D9E025CE046202E35327D9858BA138474EED6 * __this, String_t* ___groupId0, float ___order1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupId0;
		float L_1 = ___order1;
		PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TableColumnWidthAttribute::.ctor(System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TableColumnWidthAttribute__ctor_m19093F5CE091085A12F2DFEE1A59E362E833C50C (TableColumnWidthAttribute_t45DDD415E5FB012D5979A1A163D214EFF48FDB2A * __this, int32_t ___width0, bool ___resizable1, const RuntimeMethod* method)
{
	{
		__this->set_Resizable_1((bool)1);
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		__this->set_Width_0(L_0);
		bool L_1 = ___resizable1;
		__this->set_Resizable_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.TableListAttribute::get_ShowPaging()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TableListAttribute_get_ShowPaging_mAA4CBA1D36E6E33AD266462C1E59D2422E762EE4 (TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showPaging_11();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.TableListAttribute::set_ShowPaging(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TableListAttribute_set_ShowPaging_m70ED81A12B8FBC10D953B5F449CCCECEAA0437CA (TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_showPaging_11(L_0);
		__this->set_showPagingHasValue_10((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.TableListAttribute::get_ShowPagingHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TableListAttribute_get_ShowPagingHasValue_m8E2C05085222E55DECF631D88E8D5B7CFC235940 (TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showPagingHasValue_10();
		return L_0;
	}
}
// System.Int32 Sirenix.OdinInspector.TableListAttribute::get_ScrollViewHeight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TableListAttribute_get_ScrollViewHeight_m8CD7BCAFCF875A2EEDC3B77D27234D748AB0DAAF (TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TableListAttribute_get_ScrollViewHeight_m8CD7BCAFCF875A2EEDC3B77D27234D748AB0DAAF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_MinScrollViewHeight_5();
		int32_t L_1 = __this->get_MaxScrollViewHeight_6();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var);
		int32_t L_2 = Math_Min_mC950438198519FB2B0260FCB91220847EE4BB525(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Sirenix.OdinInspector.TableListAttribute::set_ScrollViewHeight(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TableListAttribute_set_ScrollViewHeight_m28162A9FBF938888F1993A403454A9342ACAE41C (TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = L_0;
		V_0 = L_1;
		__this->set_MaxScrollViewHeight_6(L_1);
		int32_t L_2 = V_0;
		__this->set_MinScrollViewHeight_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TableListAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TableListAttribute__ctor_m74F94D599C39706135C99A2FEC54F490C1541078 (TableListAttribute_t0461426C43C2937A0CD18CE2A97C72B25C71AEA5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_DefaultMinColumnWidth_2(((int32_t)40));
		__this->set_DrawScrollView_4((bool)1);
		__this->set_MinScrollViewHeight_5(((int32_t)350));
		__this->set_CellPadding_9(2);
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TableMatrixAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TableMatrixAttribute__ctor_m5A782F83D5669B3E8BED3227A6A019F04DB88983 (TableMatrixAttribute_t394888A9D58FAF1593363A064B60D84DC8D42B1C * __this, const RuntimeMethod* method)
{
	{
		__this->set_ResizableColumns_1((bool)1);
		__this->set_RespectIndentLevel_9((bool)1);
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TitleAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.TitleAlignments,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TitleAttribute__ctor_m77110B723E6BA8ED4C8EBC8948FC75001867ECD4 (TitleAttribute_t03734F3839AB42F82F5DD8EA32BD8ECB79F567CC * __this, String_t* ___title0, String_t* ___subtitle1, int32_t ___titleAlignment2, bool ___horizontalLine3, bool ___bold4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TitleAttribute__ctor_m77110B723E6BA8ED4C8EBC8948FC75001867ECD4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	TitleAttribute_t03734F3839AB42F82F5DD8EA32BD8ECB79F567CC * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	TitleAttribute_t03734F3839AB42F82F5DD8EA32BD8ECB79F567CC * G_B1_1 = NULL;
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___title0;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0011;
		}
	}
	{
		G_B2_0 = _stringLiteral2BE88CA4242C76E8253AC62474851065032D6833;
		G_B2_1 = G_B1_1;
	}

IL_0011:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_Title_0(G_B2_0);
		String_t* L_2 = ___subtitle1;
		__this->set_Subtitle_1(L_2);
		bool L_3 = ___bold4;
		__this->set_Bold_2(L_3);
		int32_t L_4 = ___titleAlignment2;
		__this->set_TitleAlignment_4(L_4);
		bool L_5 = ___horizontalLine3;
		__this->set_HorizontalLine_3(L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TitleGroupAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.TitleAlignments,System.Boolean,System.Boolean,System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TitleGroupAttribute__ctor_mBBBF50005F89D5F5510014B9B5FBDFAB3934B80D (TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * __this, String_t* ___title0, String_t* ___subtitle1, int32_t ___alignment2, bool ___horizontalLine3, bool ___boldTitle4, bool ___indent5, float ___order6, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___title0;
		float L_1 = ___order6;
		PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___subtitle1;
		__this->set_Subtitle_6(L_2);
		int32_t L_3 = ___alignment2;
		__this->set_Alignment_7(L_3);
		bool L_4 = ___horizontalLine3;
		__this->set_HorizontalLine_8(L_4);
		bool L_5 = ___boldTitle4;
		__this->set_BoldTitle_9(L_5);
		bool L_6 = ___indent5;
		__this->set_Indent_10(L_6);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TitleGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TitleGroupAttribute_CombineValuesWith_mD450E1372EF1FD6291F2E9150EF83E074847A679 (TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TitleGroupAttribute_CombineValuesWith_mD450E1372EF1FD6291F2E9150EF83E074847A679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * V_0 = NULL;
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_0 = ___other0;
		V_0 = ((TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A *)IsInstSealed((RuntimeObject*)L_0, TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A_il2cpp_TypeInfo_var));
		String_t* L_1 = __this->get_Subtitle_6();
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * L_2 = V_0;
		String_t* L_3 = __this->get_Subtitle_6();
		NullCheck(L_2);
		L_2->set_Subtitle_6(L_3);
		goto IL_0029;
	}

IL_001d:
	{
		TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = L_4->get_Subtitle_6();
		__this->set_Subtitle_6(L_5);
	}

IL_0029:
	{
		int32_t L_6 = __this->get_Alignment_7();
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * L_7 = V_0;
		int32_t L_8 = __this->get_Alignment_7();
		NullCheck(L_7);
		L_7->set_Alignment_7(L_8);
		goto IL_004b;
	}

IL_003f:
	{
		TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_Alignment_7();
		__this->set_Alignment_7(L_10);
	}

IL_004b:
	{
		bool L_11 = __this->get_HorizontalLine_8();
		if (L_11)
		{
			goto IL_0061;
		}
	}
	{
		TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * L_12 = V_0;
		bool L_13 = __this->get_HorizontalLine_8();
		NullCheck(L_12);
		L_12->set_HorizontalLine_8(L_13);
		goto IL_006d;
	}

IL_0061:
	{
		TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * L_14 = V_0;
		NullCheck(L_14);
		bool L_15 = L_14->get_HorizontalLine_8();
		__this->set_HorizontalLine_8(L_15);
	}

IL_006d:
	{
		bool L_16 = __this->get_BoldTitle_9();
		if (L_16)
		{
			goto IL_0083;
		}
	}
	{
		TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * L_17 = V_0;
		bool L_18 = __this->get_BoldTitle_9();
		NullCheck(L_17);
		L_17->set_BoldTitle_9(L_18);
		goto IL_008f;
	}

IL_0083:
	{
		TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * L_19 = V_0;
		NullCheck(L_19);
		bool L_20 = L_19->get_BoldTitle_9();
		__this->set_BoldTitle_9(L_20);
	}

IL_008f:
	{
		bool L_21 = __this->get_Indent_10();
		if (!L_21)
		{
			goto IL_00a4;
		}
	}
	{
		TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * L_22 = V_0;
		bool L_23 = __this->get_Indent_10();
		NullCheck(L_22);
		L_22->set_Indent_10(L_23);
		return;
	}

IL_00a4:
	{
		TitleGroupAttribute_t1F5306F593B28932106BA05492CCDCDB7DC2BF1A * L_24 = V_0;
		NullCheck(L_24);
		bool L_25 = L_24->get_Indent_10();
		__this->set_Indent_10(L_25);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ToggleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleAttribute__ctor_m6114BD54D92655906F8FDA3CAE94B5A456F965DF (ToggleAttribute_t4612A2D02256B1199715ACC4C56E742576D3C6B5 * __this, String_t* ___toggleMemberName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___toggleMemberName0;
		__this->set_ToggleMemberName_0(L_0);
		__this->set_CollapseOthersOnExpand_1((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Single,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleGroupAttribute__ctor_m1E0CEDC90E7C6F85CEE5DBFDDEA3E22CA80321E6 (ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * __this, String_t* ___toggleMemberName0, float ___order1, String_t* ___groupTitle2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___toggleMemberName0;
		float L_1 = ___order1;
		PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___groupTitle2;
		__this->set_ToggleGroupTitle_6(L_2);
		__this->set_CollapseOthersOnExpand_7((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleGroupAttribute__ctor_mC14797C18E0030E086743FE9C2369571298D941E (ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * __this, String_t* ___toggleMemberName0, String_t* ___groupTitle1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___toggleMemberName0;
		String_t* L_1 = ___groupTitle1;
		ToggleGroupAttribute__ctor_m1E0CEDC90E7C6F85CEE5DBFDDEA3E22CA80321E6(__this, L_0, (0.0f), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Single,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleGroupAttribute__ctor_m823D2E960C00F760CEE66D90B5BB3935BFB04A77 (ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * __this, String_t* ___toggleMemberName0, float ___order1, String_t* ___groupTitle2, String_t* ___titleStringMemberName3, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___toggleMemberName0;
		float L_1 = ___order1;
		PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___groupTitle2;
		__this->set_ToggleGroupTitle_6(L_2);
		__this->set_CollapseOthersOnExpand_7((bool)1);
		return;
	}
}
// System.String Sirenix.OdinInspector.ToggleGroupAttribute::get_ToggleMemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ToggleGroupAttribute_get_ToggleMemberName_m7117F9DDB7B23565ECF50BB6EC86223E6B896C28 (ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->get_GroupName_1();
		return L_0;
	}
}
// System.String Sirenix.OdinInspector.ToggleGroupAttribute::get_TitleStringMemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ToggleGroupAttribute_get_TitleStringMemberName_mA5A6AD70D2A21733F25D5556553A7C0251AF6FAB (ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleStringMemberNameU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::set_TitleStringMemberName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleGroupAttribute_set_TitleStringMemberName_mD04BE047F8F988B70FC59476ED83C0E34D706C5C (ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleStringMemberNameU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleGroupAttribute_CombineValuesWith_m592F8AF767D119CBD03D064A90A1777E1A0F5C63 (ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToggleGroupAttribute_CombineValuesWith_m592F8AF767D119CBD03D064A90A1777E1A0F5C63_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * V_0 = NULL;
	ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * G_B6_0 = NULL;
	ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * G_B7_1 = NULL;
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_0 = ___other0;
		V_0 = ((ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 *)IsInstSealed((RuntimeObject*)L_0, ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5_il2cpp_TypeInfo_var));
		String_t* L_1 = __this->get_ToggleGroupTitle_6();
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_ToggleGroupTitle_6();
		__this->set_ToggleGroupTitle_6(L_3);
		goto IL_0031;
	}

IL_001d:
	{
		ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = L_4->get_ToggleGroupTitle_6();
		if (L_5)
		{
			goto IL_0031;
		}
	}
	{
		ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * L_6 = V_0;
		String_t* L_7 = __this->get_ToggleGroupTitle_6();
		NullCheck(L_6);
		L_6->set_ToggleGroupTitle_6(L_7);
	}

IL_0031:
	{
		bool L_8 = __this->get_CollapseOthersOnExpand_7();
		G_B5_0 = __this;
		if (L_8)
		{
			G_B6_0 = __this;
			goto IL_0042;
		}
	}
	{
		ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = L_9->get_CollapseOthersOnExpand_7();
		G_B7_0 = ((int32_t)(L_10));
		G_B7_1 = G_B5_0;
		goto IL_0043;
	}

IL_0042:
	{
		G_B7_0 = 1;
		G_B7_1 = G_B6_0;
	}

IL_0043:
	{
		NullCheck(G_B7_1);
		G_B7_1->set_CollapseOthersOnExpand_7((bool)G_B7_0);
		ToggleGroupAttribute_t59456AF5CC6878061E15033DCE8F930CA01038D5 * L_11 = V_0;
		bool L_12 = __this->get_CollapseOthersOnExpand_7();
		NullCheck(L_11);
		L_11->set_CollapseOthersOnExpand_7(L_12);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ToggleLeftAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleLeftAttribute__ctor_mD9999F9B1B10790453F87DE1463164354765B695 (ToggleLeftAttribute_t6E6A2BD826555DA4871D9BE2A6BB0CA822D7D525 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.TypeFilterAttribute::get_MemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TypeFilterAttribute_get_MemberName_m499B40CD5A86143CEE9AFACCA276449001544A67 (TypeFilterAttribute_t367745A090DA5E0ED152768927A09BE02DE708E8 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_FilterGetter_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.TypeFilterAttribute::set_MemberName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TypeFilterAttribute_set_MemberName_m03F6F8703319609E1AFDB7C484DB0567EFC39C1C (TypeFilterAttribute_t367745A090DA5E0ED152768927A09BE02DE708E8 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_FilterGetter_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TypeFilterAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TypeFilterAttribute__ctor_m07F0EF43034B8C390DC753136654962CCCF80061 (TypeFilterAttribute_t367745A090DA5E0ED152768927A09BE02DE708E8 * __this, String_t* ___filterGetter0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___filterGetter0;
		__this->set_FilterGetter_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TypeInfoBoxAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TypeInfoBoxAttribute__ctor_m51D2E2C1E51F7110C5AE9124C84E4FD6C655695D (TypeInfoBoxAttribute_t884BA53D12B62C5C7E9F1AA20525F4D2898A16E8 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set_Message_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.ValidateInputAttribute::get_ContiniousValidationCheck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ValidateInputAttribute_get_ContiniousValidationCheck_m1C740C2A8CB957F6DC3BAB42D99A4D236AE3F4FE (ValidateInputAttribute_tDC0CF1D228B37E27B6AFC85476E9EC04CBFC2E6D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_ContinuousValidationCheck_5();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ValidateInputAttribute::set_ContiniousValidationCheck(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValidateInputAttribute_set_ContiniousValidationCheck_m7081AEBB91BEA03BC0AD1DD6DFD71B57FA882A19 (ValidateInputAttribute_tDC0CF1D228B37E27B6AFC85476E9EC04CBFC2E6D * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_ContinuousValidationCheck_5(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ValidateInputAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValidateInputAttribute__ctor_m3FA8ADE2F0F3327705269A7DD59A90FA30A1FA72 (ValidateInputAttribute_tDC0CF1D228B37E27B6AFC85476E9EC04CBFC2E6D * __this, String_t* ___condition0, String_t* ___defaultMessage1, int32_t ___messageType2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___condition0;
		__this->set_Condition_2(L_0);
		String_t* L_1 = ___defaultMessage1;
		__this->set_DefaultMessage_0(L_1);
		int32_t L_2 = ___messageType2;
		__this->set_MessageType_3(L_2);
		__this->set_IncludeChildren_4((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ValidateInputAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValidateInputAttribute__ctor_mC892E1A76D31818FF5E84A156F41B471F62FFB2C (ValidateInputAttribute_tDC0CF1D228B37E27B6AFC85476E9EC04CBFC2E6D * __this, String_t* ___condition0, String_t* ___message1, int32_t ___messageType2, bool ___rejectedInvalidInput3, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___condition0;
		__this->set_Condition_2(L_0);
		String_t* L_1 = ___message1;
		__this->set_DefaultMessage_0(L_1);
		int32_t L_2 = ___messageType2;
		__this->set_MessageType_3(L_2);
		__this->set_IncludeChildren_4((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.ValueDropdownAttribute::get_MemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ValueDropdownAttribute_get_MemberName_mDDB41A491519276523688B757548FF60EFA21540 (ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_ValuesGetter_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ValueDropdownAttribute::set_MemberName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueDropdownAttribute_set_MemberName_m68682707EC773A1CFE11732A80DC2F915DF0F983 (ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_ValuesGetter_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ValueDropdownAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueDropdownAttribute__ctor_mC8F5D3B5106B55FDAE79C0CCA48CE575C822184E (ValueDropdownAttribute_t0E8DB38AE60AF00F5465609A7049CD375B490478 * __this, String_t* ___valuesGetter0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		__this->set_NumberOfItemsBeforeEnablingSearch_1(((int32_t)10));
		String_t* L_0 = ___valuesGetter0;
		__this->set_ValuesGetter_0(L_0);
		__this->set_DrawDropdownForListElements_3((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Sirenix.OdinInspector.ValueDropdownItem
IL2CPP_EXTERN_C void ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshal_pinvoke(const ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99& unmarshaled, ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshaled_pinvoke& marshaled)
{
	marshaled.___Text_0 = il2cpp_codegen_marshal_string(unmarshaled.get_Text_0());
	if (unmarshaled.get_Value_1() != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_Value_1()))
		{
			marshaled.___Value_1 = il2cpp_codegen_com_query_interface<Il2CppIUnknown>(static_cast<Il2CppComObject*>(unmarshaled.get_Value_1()));
			(marshaled.___Value_1)->AddRef();
		}
		else
		{
			marshaled.___Value_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_Value_1());
		}
	}
	else
	{
		marshaled.___Value_1 = NULL;
	}
}
IL2CPP_EXTERN_C void ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshal_pinvoke_back(const ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshaled_pinvoke& marshaled, ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_Text_0(il2cpp_codegen_marshal_string_result(marshaled.___Text_0));
	if (marshaled.___Value_1 != NULL)
	{
		unmarshaled.set_Value_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<RuntimeObject>(marshaled.___Value_1, Il2CppComObject_il2cpp_TypeInfo_var));

		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_Value_1()))
		{
			il2cpp_codegen_com_cache_queried_interface(static_cast<Il2CppComObject*>(unmarshaled.get_Value_1()), Il2CppIUnknown::IID, marshaled.___Value_1);
		}
	}
	else
	{
		unmarshaled.set_Value_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: Sirenix.OdinInspector.ValueDropdownItem
IL2CPP_EXTERN_C void ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshal_pinvoke_cleanup(ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___Text_0);
	marshaled.___Text_0 = NULL;
	if (marshaled.___Value_1 != NULL)
	{
		(marshaled.___Value_1)->Release();
		marshaled.___Value_1 = NULL;
	}
}
// Conversion methods for marshalling of: Sirenix.OdinInspector.ValueDropdownItem
IL2CPP_EXTERN_C void ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshal_com(const ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99& unmarshaled, ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshaled_com& marshaled)
{
	marshaled.___Text_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Text_0());
	if (unmarshaled.get_Value_1() != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_Value_1()))
		{
			marshaled.___Value_1 = il2cpp_codegen_com_query_interface<Il2CppIUnknown>(static_cast<Il2CppComObject*>(unmarshaled.get_Value_1()));
			(marshaled.___Value_1)->AddRef();
		}
		else
		{
			marshaled.___Value_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_Value_1());
		}
	}
	else
	{
		marshaled.___Value_1 = NULL;
	}
}
IL2CPP_EXTERN_C void ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshal_com_back(const ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshaled_com& marshaled, ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_Text_0(il2cpp_codegen_marshal_bstring_result(marshaled.___Text_0));
	if (marshaled.___Value_1 != NULL)
	{
		unmarshaled.set_Value_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<RuntimeObject>(marshaled.___Value_1, Il2CppComObject_il2cpp_TypeInfo_var));

		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_Value_1()))
		{
			il2cpp_codegen_com_cache_queried_interface(static_cast<Il2CppComObject*>(unmarshaled.get_Value_1()), Il2CppIUnknown::IID, marshaled.___Value_1);
		}
	}
	else
	{
		unmarshaled.set_Value_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: Sirenix.OdinInspector.ValueDropdownItem
IL2CPP_EXTERN_C void ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshal_com_cleanup(ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___Text_0);
	marshaled.___Text_0 = NULL;
	if (marshaled.___Value_1 != NULL)
	{
		(marshaled.___Value_1)->Release();
		marshaled.___Value_1 = NULL;
	}
}
// System.Void Sirenix.OdinInspector.ValueDropdownItem::.ctor(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueDropdownItem__ctor_mE15D14B9282ADADDB949FB86CEA4A53335BF7AF2 (ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * __this, String_t* ___text0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___text0;
		__this->set_Text_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_Value_1(L_1);
		return;
	}
}
IL2CPP_EXTERN_C  void ValueDropdownItem__ctor_mE15D14B9282ADADDB949FB86CEA4A53335BF7AF2_AdjustorThunk (RuntimeObject * __this, String_t* ___text0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * _thisAdjusted = reinterpret_cast<ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 *>(__this + _offset);
	ValueDropdownItem__ctor_mE15D14B9282ADADDB949FB86CEA4A53335BF7AF2(_thisAdjusted, ___text0, ___value1, method);
}
// System.String Sirenix.OdinInspector.ValueDropdownItem::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_ToString_m526C92A7FAA56983098238EEBAB0E726113A1A91 (ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * __this, const RuntimeMethod* method)
{
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = __this->get_Text_0();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0015;
		}
	}
	{
		RuntimeObject * L_2 = __this->get_Value_1();
		String_t* L_3 = String_Concat_m798542DE19B3F02DC4F4B777BB2E73169F129DE1(L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
	}

IL_0015:
	{
		return G_B2_0;
	}
}
IL2CPP_EXTERN_C  String_t* ValueDropdownItem_ToString_m526C92A7FAA56983098238EEBAB0E726113A1A91_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * _thisAdjusted = reinterpret_cast<ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 *>(__this + _offset);
	return ValueDropdownItem_ToString_m526C92A7FAA56983098238EEBAB0E726113A1A91(_thisAdjusted, method);
}
// System.String Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetText()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mB8AB4A9382BA84731B96E24AB59C475901FC15E3 (ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Text_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C  String_t* ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mB8AB4A9382BA84731B96E24AB59C475901FC15E3_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * _thisAdjusted = reinterpret_cast<ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 *>(__this + _offset);
	return ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mB8AB4A9382BA84731B96E24AB59C475901FC15E3_inline(_thisAdjusted, method);
}
// System.Object Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m233A24A83540D6FBC8A5593E63BFDA1AA64391E1 (ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_Value_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C  RuntimeObject * ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m233A24A83540D6FBC8A5593E63BFDA1AA64391E1_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * _thisAdjusted = reinterpret_cast<ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 *>(__this + _offset);
	return ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m233A24A83540D6FBC8A5593E63BFDA1AA64391E1_inline(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VerticalGroupAttribute__ctor_mD788EEA9634514D13E321236E9BC3A18186E4DA0 (VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420 * __this, String_t* ___groupId0, float ___order1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupId0;
		float L_1 = ___order1;
		PropertyGroupAttribute__ctor_m02CD0111A1480FEE0EEEDF5A1F3FBA2648CD266A(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VerticalGroupAttribute__ctor_m655FA45ED58C1BA24F2011314508B7E40FFC69CB (VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420 * __this, float ___order0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VerticalGroupAttribute__ctor_m655FA45ED58C1BA24F2011314508B7E40FFC69CB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___order0;
		VerticalGroupAttribute__ctor_mD788EEA9634514D13E321236E9BC3A18186E4DA0(__this, _stringLiteralC8E20896D265CDCB3B545CCC0D79FBBB6F7F2F3E, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.VerticalGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VerticalGroupAttribute_CombineValuesWith_m983E6DD2C5697D4CCFFA5F58C29C6F93828F06BE (VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420 * __this, PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VerticalGroupAttribute_CombineValuesWith_m983E6DD2C5697D4CCFFA5F58C29C6F93828F06BE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420 * V_0 = NULL;
	{
		PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 * L_0 = ___other0;
		V_0 = ((VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420 *)IsInstClass((RuntimeObject*)L_0, VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420_il2cpp_TypeInfo_var));
		VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420 * L_2 = V_0;
		NullCheck(L_2);
		float L_3 = L_2->get_PaddingTop_6();
		if ((((float)L_3) == ((float)(0.0f))))
		{
			goto IL_0023;
		}
	}
	{
		VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420 * L_4 = V_0;
		NullCheck(L_4);
		float L_5 = L_4->get_PaddingTop_6();
		__this->set_PaddingTop_6(L_5);
	}

IL_0023:
	{
		VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420 * L_6 = V_0;
		NullCheck(L_6);
		float L_7 = L_6->get_PaddingBottom_7();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_003c;
		}
	}
	{
		VerticalGroupAttribute_tC528E057E2D097DE910389B810044BC3D13C3420 * L_8 = V_0;
		NullCheck(L_8);
		float L_9 = L_8->get_PaddingBottom_7();
		__this->set_PaddingBottom_7(L_9);
	}

IL_003c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.WrapAttribute::.ctor(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WrapAttribute__ctor_mBB5B66CD25B9F4AEFA2074CD55D05A6DA788CC77 (WrapAttribute_t1359861280D8003582A215C7DF8F794A72B60437 * __this, double ___min0, double ___max1, const RuntimeMethod* method)
{
	WrapAttribute_t1359861280D8003582A215C7DF8F794A72B60437 * G_B2_0 = NULL;
	WrapAttribute_t1359861280D8003582A215C7DF8F794A72B60437 * G_B1_0 = NULL;
	double G_B3_0 = 0.0;
	WrapAttribute_t1359861280D8003582A215C7DF8F794A72B60437 * G_B3_1 = NULL;
	WrapAttribute_t1359861280D8003582A215C7DF8F794A72B60437 * G_B5_0 = NULL;
	WrapAttribute_t1359861280D8003582A215C7DF8F794A72B60437 * G_B4_0 = NULL;
	double G_B6_0 = 0.0;
	WrapAttribute_t1359861280D8003582A215C7DF8F794A72B60437 * G_B6_1 = NULL;
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		double L_1 = ___max1;
		G_B1_0 = __this;
		if ((((double)L_0) < ((double)L_1)))
		{
			G_B2_0 = __this;
			goto IL_000e;
		}
	}
	{
		double L_2 = ___max1;
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_000f;
	}

IL_000e:
	{
		double L_3 = ___min0;
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_000f:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_Min_0(G_B3_0);
		double L_4 = ___max1;
		double L_5 = ___min0;
		G_B4_0 = __this;
		if ((((double)L_4) > ((double)L_5)))
		{
			G_B5_0 = __this;
			goto IL_001c;
		}
	}
	{
		double L_6 = ___min0;
		G_B6_0 = L_6;
		G_B6_1 = G_B4_0;
		goto IL_001d;
	}

IL_001c:
	{
		double L_7 = ___max1;
		G_B6_0 = L_7;
		G_B6_1 = G_B5_0;
	}

IL_001d:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_Max_1(G_B6_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_set_HasDefinedExpanded_mD585B02097F78E9049B2D144D50B503EF882B623_inline (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CHasDefinedExpandedU3Ek__BackingField_7(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool FoldoutGroupAttribute_get_HasDefinedExpanded_m3926B57853EA8BB7ACCACCC6740073D46F0C373C_inline (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CHasDefinedExpandedU3Ek__BackingField_7();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool FoldoutGroupAttribute_get_Expanded_m688163003BD99BF4E49564C644275EB8911A236D_inline (FoldoutGroupAttribute_tD16245B06D45EB4710C35821DAFF3C0201233BFD * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_expanded_6();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void HideIfGroupAttribute_set_Condition_m8FB8D4239B1C0E2A8C112A7AE01B5CD4FF400DDA_inline (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->set_VisibleIf_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void HideIfGroupAttribute_set_Animate_m4D3CD713242CB20F8E4109EB386B49F2A1231305_inline (HideIfGroupAttribute_t7E9737A76BF9B45A6818F8C08172229CF7043238 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->set_AnimateVisibility_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void InlineEditorAttribute_set_ExpandedHasValue_m393D00D432705F319C5FCDF2B2571461B781D9C5_inline (InlineEditorAttribute_tE91622DDBA938975C2E4B92934FD657427444216 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CExpandedHasValueU3Ek__BackingField_10(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_DrawValueLabelHasValue_mFE21A81AACD29E509534804C64D7092FC9EC971D_inline (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CDrawValueLabelHasValueU3Ek__BackingField_14(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m2E49A4D0090B1D60036AB25BA0ABAA025D3F25C5_inline (ProgressBarAttribute_t71FFAAC53896F4F0E1059529C0A9FC29DA8E9ABB * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ShowIfGroupAttribute_set_Condition_m7CAF3CF1D9803D698E9011B30EC9EA5536EB8FA9_inline (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->set_VisibleIf_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ShowIfGroupAttribute_set_Animate_m0C5232E32FBF5570CA9EBF341F4A9A26383C87CD_inline (ShowIfGroupAttribute_t63C8BDA56730EF3EC938E7F5905DB1785F773549 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		((PropertyGroupAttribute_tCEFEF23C65A1654A50EFF8BD23FC39CFFF38E310 *)__this)->set_AnimateVisibility_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TabGroupAttribute_set_Tabs_m40A73E4EC6877859058F36C0D4CEA49C3F4ED537_inline (TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * __this, List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = ___value0;
		__this->set_U3CTabsU3Ek__BackingField_11(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * TabGroupAttribute_get_Tabs_mE3395C3B6ACF15D12A58430AA25626B80AD24A4D_inline (TabGroupAttribute_tE34148522AB1D1F6104BF9DF4748428ECA46F4ED * __this, const RuntimeMethod* method)
{
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = __this->get_U3CTabsU3Ek__BackingField_11();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mB8AB4A9382BA84731B96E24AB59C475901FC15E3_inline (ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Text_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR RuntimeObject * ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m233A24A83540D6FBC8A5593E63BFDA1AA64391E1_inline (ValueDropdownItem_t62CA4A3A82867431B9098DDFE9613F0CB454CD99 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_Value_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
