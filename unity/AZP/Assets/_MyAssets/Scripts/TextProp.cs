
    using Lean.Gui;
    using TMPro;
    using UnityEngine;

    public class TextProp:MonoBehaviour
    {
        private TMP_Text txt;
        [SerializeField] private TMP_InputField textField;
        
        public void OnOpen()
        {
             var obj = EditContainer.Instance.editTransform.SelectedRect;
            txt = obj.GetComponent<TMP_Text>();
            textField.text=txt.text;
        }

        public void UpdateText(string text){
            txt.text=text;
        }

        public void UpdateAlign(int align){
            txt.alignment=align==0?TextAlignmentOptions.Left:align==1?TextAlignmentOptions.Center:TextAlignmentOptions.Right;
        }

        public void UpdateLayer(bool up)
        {
            var sib=txt.transform.GetSiblingIndex();
            sib=up?(sib<txt.transform.parent.childCount-1?sib+1:0):(sib>0?sib-1:txt.transform.parent.childCount-1);
            txt.transform.SetSiblingIndex(sib);
        }
    }