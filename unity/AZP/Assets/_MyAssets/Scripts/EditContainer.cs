﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Lean.Gui;
using SRF;
using SRF.Components;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.UI;
public class EditContainer : SRSingleton<EditContainer>
{
    [SerializeField] private RectTransform imgPrefab,txtPrefab;
    public EditTransform editTransform;
    public CommandHandler commandHandler;

    

    private void Start()
    {
        //StartCoroutine(LoadTemplate(@"C:/Users/Admin/Desktop/ava anh phong"));
        StartCoroutine(LoadAssetBundle("https://filebin.net/i0lnq0cfo23g5bii/defaultlocalgroup_assets_avaanhphong.bundle"));
    }

    private IEnumerator LoadAssetBundle(string url)
    {
        var uwr=UnityWebRequestAssetBundle.GetAssetBundle(url,0);
        yield return uwr.SendWebRequest();
        var bundle = DownloadHandlerAssetBundle.GetContent(uwr);
        Debug.Log(bundle.GetAllAssetNames()[0]);
        var obj=Instantiate(bundle.LoadAsset<GameObject>(bundle.GetAllAssetNames()[0]),transform);
        yield return new WaitForEndOfFrame();
        var root=obj.GetComponent<RectTransform>();
        GetComponent<RectTransform>().sizeDelta=new Vector2(root.sizeDelta.x,root.sizeDelta.y);
        yield break;
    }
    private float _mouseDownTimeStamp;
    protected virtual void LateUpdate()
    {
        if (Input.GetMouseButtonDown(0)) _mouseDownTimeStamp = Time.time;
        else if (Input.GetMouseButtonUp(0) && Time.time-_mouseDownTimeStamp<0.2f)
        {
            var eventSystem = EventSystem.current;
            if (eventSystem == null) return;
            var selected = eventSystem.currentSelectedGameObject;
            if(selected==null) return;
            if (selected.transform == transform.parent && editTransform.SelectedRect != null)
            {
                GetComponent<Mask>().enabled = true;
                editTransform.SelectedRect = null;
            }
            if (!selected.transform.IsChildOf(transform)) return;
            GetComponent<Mask>().enabled = false;
            editTransform.SelectedRect = (RectTransform) selected.transform;
        }
    }
    public void Export(){
        StartCoroutine(takeScreenShot());
    }
    public IEnumerator takeScreenShot()
    {
        yield return new WaitForEndOfFrame (); // it must be a coroutine 

var rect=GetComponent<RectTransform>().rect;
        var width=(int)rect.width;
        var height=(int)rect.height;

        Vector2 temp = transform.position;
        var startX = Mathf.Max(temp.x - width/2,0);
        var startY = Mathf.Max(temp.y - height/2,0);

        if(startX+width>Screen.width) width=(int)(Screen.width-startX);
        if(startY+height>Screen.height) height=(int)(Screen.height-startY);

        var tex = new Texture2D (width, height, TextureFormat.RGB24, false);
        tex.ReadPixels (new Rect(startX, startY, width, height), 0, 0);
        tex.Apply ();

        // Encode texture into PNG
        var bytes = tex.EncodeToPNG();
        Destroy(tex);

        NativeGallery.SaveImageToGallery(bytes,"AZPics",transform.GetChild(0).name+DateTime.Now.Ticks+".png",( success, path ) => Debug.Log( "Media save result: " + success + " " + path));
    }
}
