using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomImage : Image
{
    private UIFlippable _uiFlippable;
    protected override void Start()
    {
        base.Start();
        alphaHitTestMinimumThreshold = 0.5f;
        _uiFlippable = GetComponent<UIFlippable>();
    }

    public override bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
    {
        if (alphaHitTestMinimumThreshold <= 0)
            return true;

        if (alphaHitTestMinimumThreshold > 1)
            return false;

        if (sprite == null)
            return true;
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, screenPoint, eventCamera, out var local))
            return false;
        var rect = GetPixelAdjustedRect();
        local.x += rectTransform.pivot.x * rect.width;
        local.y += rectTransform.pivot.y * rect.height;
        var spriteRect = sprite.textureRect;
        local = new Vector2(local.x * spriteRect.width / rect.width, local.y * spriteRect.height / rect.height);
        var x = (spriteRect.x + local.x) / sprite.texture.width;
        var y = (spriteRect.y + local.y) / sprite.texture.height;
        if (_uiFlippable.horizontal) x = 1 - x;
        if (_uiFlippable.vertical) y = 1 - y;
        try
        {
            return sprite.texture.GetPixelBilinear(x, y).a >= alphaHitTestMinimumThreshold;
        }
        catch (UnityException e)
        {
            Debug.LogError("Using alphaHitTestMinimumThreshold greater than 0 on Image whose sprite texture cannot be read. " + e.Message + " Also make sure to disable sprite packing for this sprite.", this);
            return true;
        }
    }
}
