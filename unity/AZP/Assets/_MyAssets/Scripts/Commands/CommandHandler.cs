﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using SRF.Components;
using UnityEngine;
using UnityEngine.UI;

public interface ICommand
{
    void Execute();
    void Undo();
}

public class CommandHandler:MonoBehaviour
{
    [SerializeField] private Button btnUndo, btnRedo;
    private readonly List<ICommand> _commandList = new List<ICommand>();
    private int _index;
    private void Start()
    {
        btnUndo.onClick.AddListener(UndoCommand);
        btnRedo.onClick.AddListener(RedoCommand);
        UpdateUI();
    }

    private void UpdateUI()
    {
        btnUndo.gameObject.SetActive(_index > 0);
        btnRedo.gameObject.SetActive(_index < _commandList.Count);
    }

    public void AddCommand(ICommand command)
    {
        if (_index < _commandList.Count)
            _commandList.RemoveRange(_index, _commandList.Count - _index);
        _commandList.Add(command);
        command.Execute();
        _index++;
        UpdateUI();
    }

    private void UndoCommand()
    {
        if (_commandList.Count == 0) return;
        if (_index <= 0) return;
        _commandList[_index - 1].Undo();
        _index--;
        UpdateUI();
    }

    private void RedoCommand()
    {
        if (_commandList.Count == 0) return;
        if (_index >= _commandList.Count) return;
        _index++;
        _commandList[_index - 1].Execute();
        UpdateUI();
    }
}
[Serializable]
public class MoveCommand:ICommand
{
    [SerializeField]
    private Vector4 from;
    [SerializeField]
    private Vector4 to;
    [SerializeField]
    private RectTransform obj;
    public MoveCommand(Vector4 @from, Vector4 to, RectTransform obj)
    {
        this.@from = @from;
        this.to = to;
        this.obj = obj;
    }

    public void Execute()
    {
        obj.SetSizeAndPos(to);
    }

    public void Undo()
    {
        obj.SetSizeAndPos(from);
    }
}