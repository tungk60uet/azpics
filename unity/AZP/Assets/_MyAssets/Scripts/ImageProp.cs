
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using Lean.Gui;
    using UnityEngine;
    using UnityEngine.Networking;
    using UnityEngine.UI;

    public class ImageProp:MonoBehaviour
    {
        private Image img;
        public Slider sat;
        public Slider con;
        public Slider hue;
        public Slider bri;
        public Slider apl;
        private ColorAdjust colorAdjust;
        private UIFlippable uiFlippable;
        private void Awake()
        {
            sat.onValueChanged.AddListener(OnSatChange);
            con.onValueChanged.AddListener(OnConChange);
            hue.onValueChanged.AddListener(OnHueChange);
            bri.onValueChanged.AddListener(OnBriChange);
            apl.onValueChanged.AddListener(OnAlphaChange);
        }

        private void OnDestroy()
        {
            sat.onValueChanged.RemoveListener(OnSatChange);
            con.onValueChanged.RemoveListener(OnConChange);
            hue.onValueChanged.RemoveListener(OnHueChange);
            bri.onValueChanged.RemoveListener(OnBriChange);
            apl.onValueChanged.RemoveListener(OnAlphaChange);
        }

        public void OnOpen()
        {
            var obj = EditContainer.Instance.editTransform.SelectedRect;
            img = obj.GetComponent<Image>();
            uiFlippable = obj.GetComponent<UIFlippable>();
            colorAdjust = obj.GetComponent<ColorAdjust>();
            sat.value = colorAdjust._Saturation;
            con.value = colorAdjust._Contrast;
            hue.value = colorAdjust._HueShift;
            bri.value = colorAdjust._ValueBrightness;
            apl.value = colorAdjust._Alpha;
        }
        public void PickImage(bool isRemove)
        {
            NativeGallery.GetImageFromGallery((path) =>
            {
                if (string.IsNullOrEmpty(path)) return;
                LoadByteToTexture(File.ReadAllBytes(path));
                if (isRemove)
                    StartCoroutine(RemoveBG(path));
            }, "Select a PNG image", "image/png");
        }
        private IEnumerator RemoveBG(string path)
        {
            List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
            formData.Add(new MultipartFormDataSection("size", "auto"));
            formData.Add(new MultipartFormFileSection("image_file", File.ReadAllBytes(path),  Path.GetFileName(path),"image/png"));
            var www = UnityWebRequest.Post("https://api.remove.bg/v1.0/removebg", formData);
            www.SetRequestHeader("X-Api-Key", "t4xF5SZzgqtFwHRwpHoqf3Fu");
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
                LoadByteToTexture(www.downloadHandler.data);
            }
        }

        private void LoadByteToTexture(byte[] data)
        {
            var texture = new Texture2D(2, 2);
            texture.LoadImage(data);
            img.sprite = Sprite.Create(texture,new Rect(0,0,texture.width,texture.height),new Vector2(0.5f,0.5f),100,0,SpriteMeshType.FullRect);
            img.GetComponent<AspectRatioFitter>().aspectRatio = texture.width * 1f / texture.height;
            //onTargetChange?.Invoke(img);
        }
        
        public void OnSatChange(float value)
        {
            colorAdjust._Saturation = value;
        }
        public void OnConChange(float value)
        {
            colorAdjust._Contrast = value;
        }
        public void OnBriChange(float value)
        {
            colorAdjust._ValueBrightness = value;
        }
        public void OnHueChange(float value)
        {
            colorAdjust._HueShift = value;
        }
        public void OnAlphaChange(float value)
        {
            colorAdjust._Alpha = value;
        }

        public void Flip(bool isHorizontal)
        {
            if (isHorizontal) {
                uiFlippable.horizontal = !uiFlippable.horizontal;
            }
            else {
                uiFlippable.vertical = !uiFlippable.vertical;
            }
            uiFlippable.GetComponent<Graphic>().SetVerticesDirty();
        }
        public void UpdateLayer(bool up)
        {
            var sib=img.transform.GetSiblingIndex();
            sib=up?(sib<img.transform.parent.childCount-1?sib+1:0):(sib>0?sib-1:img.transform.parent.childCount-1);
            img.transform.SetSiblingIndex(sib);
        }
    }
