using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Gui;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EditTransform : MonoBehaviour
{
	[SerializeField] private LeanToggle hint;
	[SerializeField] private LeanToggle imgProp;
	[SerializeField] private LeanToggle txtProp;
	
	private RectTransform _selectedRect;
	public RectTransform SelectedRect
	{
		get => _selectedRect;
		set
		{
			_selectedRect = value;
			gameObject.SetActive(_selectedRect != null);
			if (_selectedRect == null)
			{
				hint.TurnOn();
				return;
			}
			
			if(_selectedRect.GetComponent<CustomImage>()!=null){
				imgProp.TurnOn();
				imgProp.GetComponent<ImageProp>().OnOpen();
			}
			else if(_selectedRect.GetComponent<TMP_Text>()!=null) {
				txtProp.TurnOn();
				txtProp.GetComponent<TextProp>().OnOpen();
			}
			else hint.TurnOn();
			
			_leanDrag.Target = _selectedRect;
			foreach (var leanResize in _leanResizes)
				leanResize.Target = _selectedRect;
		}
	}

	private RectTransform rectTransform;
	private RectTransform canvasRectTransform;
	private LeanResize[] _leanResizes;
	private LeanDrag _leanDrag;
	
	private void Awake()
	{
		rectTransform = GetComponent<RectTransform>();
		_leanDrag = GetComponent<LeanDrag>();
		_leanResizes = GetComponentsInChildren<LeanResize>();
		SelectedRect = _selectedRect;
	}

	private Vector4 _sizePosFrom;
	public void OnBegin()
	{
		_sizePosFrom = SelectedRect.ToSizePos();
	}

	public void OnEnd()
	{
		EditContainer.Instance.commandHandler.AddCommand(new MoveCommand(_sizePosFrom, SelectedRect.ToSizePos(), SelectedRect));
	}
	
	protected virtual void LateUpdate()
	{
		UpdateRect(_selectedRect);
	}

	private bool UpdateRect(RectTransform target)
	{
		var camera = Camera.main;

		if (camera != null)
		{
			

			if (canvasRectTransform == null)
			{
				var canvas = GetComponentInParent<Canvas>();

				if (canvas == null)
				{
					throw new System.Exception("Couldn't find attached canvas??");
				}

				canvasRectTransform = canvas.GetComponent<RectTransform>();
			}

			// Calculate viewport/anchor points
			var min = target.rect.min;
			var max = target.rect.max;
			var targetA = target.TransformPoint(min.x, min.y, 0.0f);
			var targetB = target.TransformPoint(max.x, min.y, 0.0f);
			var targetC = target.TransformPoint(min.x, max.y, 0.0f);
			var targetD = target.TransformPoint(max.x, max.y, 0.0f);

			var worldSpace = target.GetComponentInParent<Canvas>().renderMode == RenderMode.WorldSpace;
			var viewportPointA = WorldToViewportPoint(camera, targetA, worldSpace);
			var viewportPointB = WorldToViewportPoint(camera, targetB, worldSpace);
			var viewportPointC = WorldToViewportPoint(camera, targetC, worldSpace);
			var viewportPointD = WorldToViewportPoint(camera, targetD, worldSpace);

			// If outside frustum, hide line out of view
			if (LeanGui.InvaidViewportPoint(camera, viewportPointA) == true ||
			    LeanGui.InvaidViewportPoint(camera, viewportPointB) == true ||
			    LeanGui.InvaidViewportPoint(camera, viewportPointC) == true ||
			    LeanGui.InvaidViewportPoint(camera, viewportPointD) == true)
			{
				viewportPointA = viewportPointB = viewportPointC = viewportPointD = new Vector3(10.0f, 10.0f);
			}

			var minX = Mathf.Min(Mathf.Min(viewportPointA.x, viewportPointB.x),
				Mathf.Min(viewportPointC.x, viewportPointD.x));
			var minY = Mathf.Min(Mathf.Min(viewportPointA.y, viewportPointB.y),
				Mathf.Min(viewportPointC.y, viewportPointD.y));
			var maxX = Mathf.Max(Mathf.Max(viewportPointA.x, viewportPointB.x),
				Mathf.Max(viewportPointC.x, viewportPointD.x));
			var maxY = Mathf.Max(Mathf.Max(viewportPointA.y, viewportPointB.y),
				Mathf.Max(viewportPointC.y, viewportPointD.y));

			// Convert viewport points to canvas points
			var canvasRect = canvasRectTransform.rect;
			var canvasXA = canvasRect.xMin + canvasRect.width * minX;
			var canvasYA = canvasRect.yMin + canvasRect.height * minY;
			var canvasXB = canvasRect.xMin + canvasRect.width * maxX;
			var canvasYB = canvasRect.yMin + canvasRect.height * maxY;

			// Find center, reset anchor, and convert canvas point to world point
			var canvasX = (canvasXA + canvasXB) * 0.5f;
			var canvasY = (canvasYA + canvasYB) * 0.5f;

			rectTransform.anchorMin = rectTransform.anchorMax = Vector2.zero;
			rectTransform.sizeDelta = new Vector2(canvasXB - canvasXA, canvasYB - canvasYA);
			rectTransform.position = canvasRectTransform.TransformPoint(canvasX, canvasY, 0.0f);

			// Get vector between points

			return true;
		}

		return false;
	}

	private static Vector3 WorldToViewportPoint(Camera camera, Vector3 point, bool worldSpace)
	{
		if (worldSpace == false)
		{
			point = RectTransformUtility.WorldToScreenPoint(null, point);
			point.z = 0.5f;

			return camera.ScreenToViewportPoint(point);
		}

		return camera.WorldToViewportPoint(point);
	}
}
