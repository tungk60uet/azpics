﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public static class Ultils
{
    public static void LoadImage(this Image img, string path)
    {
        img.StartCoroutine(LoadImageCor(img, path));
    }

    private static IEnumerator LoadImageCor(Image img, string path)
    {
        var www = UnityWebRequest.Get(path);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(path + '\n' + www.error);
        }
        else
        {
            var texture = new Texture2D(2, 2);
            texture.LoadImage(www.downloadHandler.data);
            img.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f),
                100, 0, SpriteMeshType.FullRect);
            var aspectRatio = img.GetComponent<AspectRatioFitter>();
            if (aspectRatio) aspectRatio.aspectRatio = texture.width * 1f / texture.height;
        }
    }

    public static void SetSizeAndPos(this RectTransform trans, Vector4 posSize)
    {
        trans.anchoredPosition = new Vector2(posSize.x, posSize.y);
        trans.sizeDelta = new Vector2(posSize.z, posSize.w);
    }

    public static Vector4 ToSizePos(this RectTransform trans) => new Vector4(trans.anchoredPosition.x,
        trans.anchoredPosition.y, trans.sizeDelta.x, trans.sizeDelta.y);
}
