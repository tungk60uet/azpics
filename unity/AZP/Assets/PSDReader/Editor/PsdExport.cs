using System.Threading;
using PhotoshopFile;
using UnityEditor;
using UnityEngine;
using System.Text;
using System.Linq;
using System.IO;
using UnityEngine.UI;
using System;
using TMPro;
using UniModules.UniGame.AddressableExtensions.Editor;

public class PsdExport
{
 [MenuItem("Assets/ExportPSD",false,10)]
    public static void Export()
    {
        var path = AssetDatabase.GetAssetPath(Selection.activeObject);   

        var exportPath=Path.GetDirectoryName(path)+"/Export";
        Directory.CreateDirectory(exportPath);
        
        var _psdFile = new PsdFile(path,new LoadContext{Encoding=Encoding.UTF8});
        var CanvasW = (int)_psdFile.BaseLayer.Rect.width;
        var CanvasH =(int)_psdFile.BaseLayer.Rect.height;

        var editContainer=GameObject.FindObjectOfType<EditContainer>();
        if(editContainer.transform.childCount>0) GameObject.DestroyImmediate(editContainer.transform.GetChild(0).gameObject);

        var root=new GameObject(Path.GetFileNameWithoutExtension(path),typeof(RectTransform)).GetComponent<RectTransform>();
        root.SetParent(editContainer.transform);
        root.sizeDelta=new Vector2(CanvasW,CanvasH);
        root.localPosition=Vector3.zero;
        root.localScale=Vector3.one;
        
        var ImgPrefab=AssetDatabase.LoadAssetAtPath<Image>("Assets/_MyAssets/Prefabs/ImgEdit.prefab");
        var TxtPrefab=AssetDatabase.LoadAssetAtPath<TMP_Text>("Assets/_MyAssets/Prefabs/TextEdit.prefab");

        foreach (var TargetLayer in _psdFile.Layers)
        {
            var textInfo=TargetLayer.AdditionalInfo.FirstOrDefault(info=>info is TextLayerInfo) as TextLayerInfo;
            if(textInfo!=null)
            {
                var txt=GameObject.Instantiate(TxtPrefab,root);
                txt.name=TargetLayer.Name;
                txt.text=textInfo.Text;
                var trans=txt.GetComponent<RectTransform>();
                var rect=TargetLayer.Rect;
                trans.sizeDelta=rect.size;
                trans.localPosition=new Vector2(rect.x+rect.width/2-CanvasW/2,-rect.y-rect.height/2+CanvasH/2);
            }else{
                    var pngPath=$"{exportPath}/{TargetLayer.Name }.png";// path.Substring(0,path.LastIndexOf('/')) + "/"++ ".png";
                    var spriteRect=SaveLayer(TargetLayer,pngPath);
                    Image img;
                    if(!TargetLayer.ProtectTrans){
                        img=GameObject.Instantiate(ImgPrefab,root);
                        img.name=TargetLayer.Name;
                    }else{
                        img=new GameObject(TargetLayer.Name,typeof(RectTransform),typeof(Image)).GetComponent<Image>();
                        img.transform.SetParent(root);
                        img.transform.localScale=Vector3.one;
                        img.raycastTarget=false;
                    }
                    img.sprite=spriteRect.Item1;
                    
                    var aspectRatio = img.GetComponent<AspectRatioFitter>();
                    if (aspectRatio) aspectRatio.aspectRatio = spriteRect.Item2.width * 1f / spriteRect.Item2.height;

                    img.SetNativeSize();
                    img.transform.localPosition= new Vector2(spriteRect.Item2.x+spriteRect.Item2.width/2 - CanvasW/2,-spriteRect.Item2.y-spriteRect.Item2.height/2f+CanvasH/2f);
            }
        }
        var prefabSaved= PrefabUtility.SaveAsPrefabAssetAndConnect(root.gameObject, $"{exportPath}/{Path.GetFileNameWithoutExtension(path)}.prefab", InteractionMode.UserAction);
        AddressableHelper.CreateAssetEntry(prefabSaved);
        AssetDatabase.OpenAsset(prefabSaved);
    }
    public static Tuple<Sprite, Rect> SaveLayer(Layer TargetLayer,string path){
        var layerWidth = (int)TargetLayer.Rect.width;
            var layerHeight = (int)TargetLayer.Rect.height;
            var CanvasW = (int)TargetLayer.PsdFile.BaseLayer.Rect.width;
            var CanvasH =(int)TargetLayer.PsdFile.BaseLayer.Rect.height;

            Rect _clippedRect = new Rect(
                    Mathf.Max(0f, TargetLayer.Rect.xMin),
                    Mathf.Max(0f, TargetLayer.Rect.yMin),
                    (CanvasW + layerWidth) - (Mathf.Max(TargetLayer.Rect.xMax, CanvasW) - Mathf.Min(TargetLayer.Rect.xMin, 0f)),
                    (CanvasH + layerHeight) - (Mathf.Max(TargetLayer.Rect.yMax, CanvasH) - Mathf.Min(TargetLayer.Rect.yMin, 0f)));

            Channel _red = (from l in TargetLayer.Channels where l.ID == 0 select l).First();
            Channel _green = (from l in TargetLayer.Channels where l.ID == 1 select l).First();
            Channel _blue = (from l in TargetLayer.Channels where l.ID == 2 select l).First();
            Channel _alpha = TargetLayer.AlphaChannel;

            var _clippedPixels = new Color32[(int)_clippedRect.width * (int)_clippedRect.height];
            var _layerRect=TargetLayer.Rect;
            for (int i = 0; i < CanvasW*CanvasH; ++i)
            {
                var x = i % CanvasW;
                var y = i / CanvasH;
                if (x < _layerRect.xMin || x >= _layerRect.xMax ||
            y < _layerRect.yMin || y >= _layerRect.yMax)
                    {
                        
                    }
                    else
                    {
                        if (x >= _clippedRect.x && x < _clippedRect.xMax && y >= _clippedRect.y && y < _clippedRect.yMax)
                        {
                             var sourcePos = new Vector2Int(x - (int)_layerRect.x, y - (int)_layerRect.y);
                        var sourceIdx = sourcePos.y * (int)_layerRect.width + sourcePos.x;

                        byte r = _red.ImageData[sourceIdx];
                        byte g = _green.ImageData[sourceIdx];
                        byte b = _blue.ImageData[sourceIdx];
                        byte a = (byte)(_alpha == null ? 255 : _alpha.ImageData[sourceIdx]);
                            var clippedX = x - (int)_clippedRect.x;
                            var clippedY = y - (int)_clippedRect.y;
                            var clippedIdx = ((int)_clippedRect.height - 1 - clippedY) * (int)_clippedRect.width + clippedX;
                            _clippedPixels[clippedIdx] = new Color32(r, g, b, a);
                        }
                    }
            }
             var layerTexture = new Texture2D((int)_clippedRect.width,(int)_clippedRect.height, TextureFormat.RGBA32, true);
                layerTexture.SetPixels32(_clippedPixels);
                layerTexture.Apply();
                File.WriteAllBytes(path, layerTexture.EncodeToPNG());
                 AssetDatabase.Refresh();
            AssetDatabase.LoadAssetAtPath<Texture2D>(path);
            TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
            textureImporter.textureType = TextureImporterType.Sprite;
            textureImporter.spriteImportMode = SpriteImportMode.Single;
            textureImporter.spritePivot = new Vector2(0.5f, 0.5f);
            textureImporter.spritePixelsPerUnit = 100;
            textureImporter.isReadable=true;
            textureImporter.crunchedCompression=true;
            textureImporter.compressionQuality=80;
            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
            return new Tuple<Sprite, Rect>(AssetDatabase.LoadAssetAtPath<Sprite>(path),_clippedRect);
    }
}